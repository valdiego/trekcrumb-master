package com.trekcrumb.android.database;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.FileUtil;
import com.trekcrumb.common.exception.TrekcrumbException;

/**
 * Implementation of SQLiteOpenHelper to access the app SQLite database.
 * 
 * The class provides operations:
 * 1. To verify if database exists, and creates new one if it does not exists
 * 2. To upgrades the database if the version number in property file has changed.
 * 3. To open and close database.
 */
public class DatabaseSQLiteOpenHelperImpl extends SQLiteOpenHelper {
    private static final String LOG_TAG = "DatabaseSQLiteOpenHelperImpl";
    
    private static DatabaseSQLiteOpenHelperImpl mSQLiteHelper;
    private SQLiteDatabase mSQLiteDB;
    private Context mContext;
    private String mAppDBPath;
    private boolean mIsCreateDB;
    private boolean mIsUpgradeDB;

    /**
     * Private constructor to force Singleton pattern.
     */
    private DatabaseSQLiteOpenHelperImpl(final Context context) {
        super(context, AndroidConstants.APP_DATABASE_NAME, null, context.getResources().getInteger(R.integer.app_DBVersion));
        mContext = context;
        mAppDBPath = mContext.getDatabasePath(AndroidConstants.APP_DATABASE_NAME).getAbsolutePath();
        reset();
    }
    
    /**
     * Singleton.
     * @param context - Android activity as a context.
     * @return - The single instance of DatabaseSQLiteOpenHelperImpl.
     */
    public static DatabaseSQLiteOpenHelperImpl getInstance(final Context context) {
        if(mSQLiteHelper == null) {
            mSQLiteHelper = new DatabaseSQLiteOpenHelperImpl(context);
        } else {
            mSQLiteHelper.reset();
        }
        return mSQLiteHelper;
    }
    
    /**
     * Initializes application database.
     */
    public void initializeDataBase() throws TrekcrumbException {
        /*
         * The call to getWriteableDatabase() will:
         * 1. If DB does not exist, create an empty DB and then invoke callback onCreate() to 
         *    create tables and fill in data, if any.
         * 2. If DB exists but version number has changed, invoke callback onUpgrade() to update
         *    the database.
         */
        Log.d(LOG_TAG, "initializeDataBase() - Initializing database for app DB: " + mAppDBPath);
        getWritableDatabase();
        if (mIsCreateDB || mIsUpgradeDB) {
            createOrUpgradeDB_ByCopyDB();
        } else {
            Log.d(LOG_TAG, "initializeDataBase() - Database is ready to use.");
        }
    }

    /**
     * A callback method to create a new database and all its tables, and metadata if any.
     * 
     * The system will automatically call this method upon any call to getWritableDatabase()
     * when it finds out that the database in question does not exist.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        mIsCreateDB = true;

        /*
         * WARNING: 
         * We no longer invoke operation to create the DB here. It is because this callback lead
         * to corrupted (invalid) corrupted (invalid) copy-process due to the getWritableDatabase() 
         * call retaining an open database, and this causes copying database to corrupt or incomplete. 
         */
        //createOrUpgradeDB_ByCopyDB();
    }
    
    /**
     * A callback method to upgrade existing database and all its tables, and copy over all data
     * to the new one if any.
     * 
     * The system will automatically call this method upon any call to getWritableDatabase()
     * when it finds out that the app database version has been changed from the last time it was 
     * created/upgraded.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mIsUpgradeDB = true;

        /*
         * WARNING: 
         * We no longer invoke operation to upgrade the DB here. It is because this callback lead
         * to corrupted (invalid) corrupted (invalid) copy-process due to the getWritableDatabase() 
         * call retaining an open database, and this causes copying database to corrupt or incomplete. 
         */
        //createOrUpgradeDB_ByCopyDB();
    }

    public SQLiteDatabase openDatabase() throws TrekcrumbException {
        if(mSQLiteDB == null || !mSQLiteDB.isOpen()) {
            try {
                mSQLiteDB = SQLiteDatabase.openDatabase(mAppDBPath, null, SQLiteDatabase.OPEN_READWRITE);
            } catch(Exception e) {
                mSQLiteDB = null;
                String errMsg = "Failed when trying to open app database: " + e.getMessage();
                Log.e(LOG_TAG, "openDatabase() - " + errMsg, e);
                throw new TrekcrumbException(errMsg);
            }
        }
        return mSQLiteDB;
    }

    public void closeDatabase() {
        super.close();
        if(mSQLiteDB != null) {
            mSQLiteDB.releaseReference();
            mSQLiteDB.close();
            SQLiteDatabase.releaseMemory();
        }
    }
    
    /*
     * Resets singleton instance's main variables to their initial states to avoid
     * left over states/values from previous usages.
     */
    private void reset() {
        mIsCreateDB = false;
        mIsUpgradeDB = false;
        if(mSQLiteDB != null) {
            mSQLiteDB.releaseReference();
            mSQLiteDB.close();
        }
    }

    /*
     * Creates or upgrade app database by hard-copy from source database to the installed location.
     * 
     * This impl. method will re-create new database, and if upgrade is true, it will copy over all data
     * from the old one to the new one, and delete the old database.
     */
    private void createOrUpgradeDB_ByCopyDB() throws TrekcrumbException {
        Log.d(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Starting...");
        
        /*
         * Required: Close SQLiteOpenHelper to commit the created empty database to internal storage.
         * Otherwise, it will keep re-creating the database for every run.
         */
        close();
        
        //If upgrade, backup any existing data on device:
        String upgradeDBPath = null;
        boolean isUpgradeBackupDBSuccess = false;
        if(mIsUpgradeDB) {
            Log.d(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Upgrade DB in action, making upgrade backup DB ...");
            try {
                upgradeDBPath = mContext.getDatabasePath(AndroidConstants.APP_DATABASE_NAME_BACKUP).getAbsolutePath();
                FileUtil.copyFile(mAppDBPath, upgradeDBPath);
                isUpgradeBackupDBSuccess = true;
                Log.d(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Upgrade backup DB completed. Its new path: " + upgradeDBPath);
            } catch(Exception e) {
                isUpgradeBackupDBSuccess = false;
                Log.e(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Failed while making upgrade backup DB: " + e.getMessage(), e);
            } 
        }

        //Copy the new DB from source (install APK) to target (install directory DB):
        Log.d(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Starting to install app DB to target path: " + mAppDBPath);
        InputStream sourceDB = null;
        OutputStream targetDB = null;
        try {
            //Open DB from APK install package:
            sourceDB = mContext.getAssets().open(AndroidConstants.APP_DATABASE_SOURCE_FILENAME);
        } catch(Exception e) {
            sourceDB = null;
            String errMsg = "Failed to open source database file [" 
                            + AndroidConstants.APP_DATABASE_SOURCE_FILENAME + "] to install!";
            Log.e(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - " + errMsg, e);
            throw new TrekcrumbException(errMsg);
        }
        if(sourceDB != null) {
            try {
                targetDB = new FileOutputStream(mAppDBPath);
                FileUtil.copyFile(sourceDB, targetDB);
                Log.i(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Install new app DB Completed.");
            } catch(Exception e) {
                String errMsg = "Error while installing new app database!";
                Log.e(LOG_TAG, errMsg + "createOrUpgradeDB_ByCopyDB() - Error: " + e.getMessage(), e);
                throw new TrekcrumbException(errMsg);
            }
        }
        
        if(mIsUpgradeDB 
                && isUpgradeBackupDBSuccess) {
            upgradeDB(upgradeDBPath);
            
            //Delete upgrade backup DB:
            FileUtil.deleteFile(upgradeDBPath);
        }
        
        /*
         * Required: Access the copied database so SQLiteHelper will cache it and mark it as created.
         */
        getWritableDatabase().close();
        
        Log.d(LOG_TAG, "createOrUpgradeDB_ByCopyDB() - Completed.");
    }
    
    private void upgradeDB(String upgradeDBPath) {
        Log.d(LOG_TAG, "upgradeDB() - Starting to copy upgrade backup data to new DB: \n"
                + "Upgrade Backup DB (source): " + upgradeDBPath + "\n"
                + "App DB (target): " + mAppDBPath);
        SQLiteDatabase appDB = null;
        StringBuilder upgradeQueryBuilder = null;
        try {
            //The queries below must be executed in the exact orders due to FK constraints b/w tables
            appDB = SQLiteDatabase.openDatabase(mAppDBPath, null, SQLiteDatabase.OPEN_READWRITE);
            appDB.execSQL("attach '" + upgradeDBPath + "' as UPGRADE_BACKUP_DB");
            appDB.beginTransaction();

            upgradeQueryBuilder = new StringBuilder();
            upgradeQueryBuilder.append("insert into USER(_id, USERNAME, FULLNAME, SUMMARY, LOCATION, PROFILE_IMAGE_NAME, PROFILE_IMAGE_URL, CREATED, UPDATED)");
            upgradeQueryBuilder.append(" select _id, USERNAME, FULLNAME, SUMMARY, LOCATION, PROFILE_IMAGE_NAME, PROFILE_IMAGE_URL, CREATED, UPDATED from UPGRADE_BACKUP_DB.USER");
            appDB.execSQL(upgradeQueryBuilder.toString());
            
            upgradeQueryBuilder.delete(0, upgradeQueryBuilder.length());
            upgradeQueryBuilder.append("insert into USER_AUTH_TOKEN(_id, USERID, USERNAME, DEVICE_IDENTITY, AUTH_TOKEN, CREATED)");
            upgradeQueryBuilder.append(" select _id, USERID, USERNAME, DEVICE_IDENTITY, AUTH_TOKEN, CREATED from UPGRADE_BACKUP_DB.USER_AUTH_TOKEN");
            appDB.execSQL(upgradeQueryBuilder.toString());
           
            upgradeQueryBuilder.delete(0, upgradeQueryBuilder.length());
            upgradeQueryBuilder.append("insert into TRIP(_id, USERID, NAME, NOTE, LOCATION, STATUS, PUBLISH, PRIVACY, CREATED, UPDATED, COMPLETED)");
            upgradeQueryBuilder.append(" select _id, USERID, NAME, NOTE, LOCATION, STATUS, PUBLISH, PRIVACY, CREATED, UPDATED, COMPLETED from UPGRADE_BACKUP_DB.TRIP");
            appDB.execSQL(upgradeQueryBuilder.toString());
            
            upgradeQueryBuilder.delete(0, upgradeQueryBuilder.length());
            upgradeQueryBuilder.append("insert into PLACE(_id, USERID, TRIPID, LATITUDE, LONGITUDE, LOCATION, NOTE, CREATED, UPDATED)");
            upgradeQueryBuilder.append(" select _id, USERID, TRIPID, LATITUDE, LONGITUDE, LOCATION, NOTE, CREATED, UPDATED from UPGRADE_BACKUP_DB.PLACE");
            appDB.execSQL(upgradeQueryBuilder.toString());

            upgradeQueryBuilder.delete(0, upgradeQueryBuilder.length());
            upgradeQueryBuilder.append("insert into PICTURE(_id, USERID, TRIPID, IMAGE_NAME, IMAGE_URL, IS_COVER_PICTURE, FEATURE_FOR_PLACEID, NOTE, CREATED, UPDATED)");
            upgradeQueryBuilder.append(" select _id, USERID, TRIPID, IMAGE_NAME, IMAGE_URL, IS_COVER_PICTURE, FEATURE_FOR_PLACEID, NOTE, CREATED, UPDATED from UPGRADE_BACKUP_DB.PICTURE");
            appDB.execSQL(upgradeQueryBuilder.toString());
            
            //Mark transaction successful to ensure commit:
            appDB.setTransactionSuccessful();
            Log.d(LOG_TAG, "upgradeDB() - Completed successfully.");
            
        } catch(Exception e) {
            Log.e(LOG_TAG, "upgradeDB() - Failed while copying data from upgrade backup DB to new app DB: " + e.getMessage(), e);
        } finally {
            if(appDB != null) {
                try {
                    //Ensure transaction is closed, or rollback if it is not successful
                    appDB.endTransaction();
                } catch(Exception e2) {}
                try {
                    //Ensure detach
                    appDB.execSQL("detach database UPGRADE_BACKUP_DB");
                } catch(Exception e2) {}
                appDB.releaseReference();
                appDB.close();
            }
            SQLiteDatabase.releaseMemory();
            upgradeQueryBuilder = null;
        }
    }
    
    /*
     * Creates or upgrade app database by executing each SQL statements from the source SQL file.
     */
    @SuppressWarnings("unused")
    private void createOrUpgradeDB_BySQLFile(SQLiteDatabase db) throws TrekcrumbException {
        Log.i(LOG_TAG, "createOrUpgradeDBFromSQLFile() - Starting...");
        InputStream sourceSQLFile = null;
        try {
            sourceSQLFile = mContext.getAssets().open(AndroidConstants.APP_DATABASE_SOURCE_SQL_FILENAME);
        } catch(IOException ioe) {
            String errMsg = "Failed to read app source SQL file!";
            Log.e(LOG_TAG, errMsg);
            ioe.printStackTrace();
            throw new TrekcrumbException(errMsg);
        }

        if(sourceSQLFile != null) {
            try {
                String[] statements = FileUtil.parseSqlFile(sourceSQLFile);
                db.beginTransaction();
                for (String statement : statements) {
                    db.execSQL(statement);
                }
                db.setTransactionSuccessful();
                Log.i(LOG_TAG, "createOrUpgradeDBFromSQLFile() - Completed successfully.");
            } catch (Exception e) {
                String errMsg = "Error while creating app database!";
                Log.e(LOG_TAG, errMsg + " Error: " + e.getMessage());
                e.printStackTrace();
                throw new TrekcrumbException(errMsg);

            } finally {
                db.endTransaction();
                try {
                    sourceSQLFile.close();
                } catch(Exception e) {} //Cant do anything                    
            }       
        }
    }    
}
