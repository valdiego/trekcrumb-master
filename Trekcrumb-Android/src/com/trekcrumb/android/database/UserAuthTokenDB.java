package com.trekcrumb.android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.trekcrumb.common.bean.UserAuthToken;

public class UserAuthTokenDB {
    private static final String TABLE_NAME = "USER_AUTH_TOKEN";

    /**
     * Creates a new record for UserAuthToken.
     * Note: This entry is a clone record that should be created in server first.
     *       All data including _id and CREATED date must come from the server.
     *       
     * @param userAuthToken - The bean containing all required data from the server.
     */
    public static void create(
            final Context context,
            UserAuthToken authTokenToCreate) {
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("_id", authTokenToCreate.getId());
        columnsAndValues.put("USERID", authTokenToCreate.getUserId());
        columnsAndValues.put("USERNAME", authTokenToCreate.getUsername());
        columnsAndValues.put("DEVICE_IDENTITY", authTokenToCreate.getDeviceIdentity());
        columnsAndValues.put("AUTH_TOKEN", authTokenToCreate.getAuthToken());
        columnsAndValues.put("CREATED", authTokenToCreate.getCreated());
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.insertOrThrow(TABLE_NAME, null, columnsAndValues);
                database.setTransactionSuccessful();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }
    
    /**
     * Finds the most currently saved User Auth Token entity. 
     */
    public static UserAuthToken retrieve(final Context context) {
        UserAuthToken authTokenFound = null;
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        Cursor resultCursor = null;
        
        String query = "select * from USER_AUTH_TOKEN";
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                resultCursor = database.rawQuery(query, null);
                if(resultCursor!= null && resultCursor.getCount() > 0) {
                    //Expect only one result:
                    authTokenFound = new UserAuthToken();
                    resultCursor.moveToFirst();
                    authTokenFound.setId(resultCursor.getString(resultCursor.getColumnIndex("_id")));
                    authTokenFound.setUserId(resultCursor.getString(resultCursor.getColumnIndex("USERID")));
                    authTokenFound.setDeviceIdentity(resultCursor.getString(resultCursor.getColumnIndex("DEVICE_IDENTITY")));
                    authTokenFound.setAuthToken(resultCursor.getString(resultCursor.getColumnIndex("AUTH_TOKEN")));
                    authTokenFound.setCreated(resultCursor.getString(resultCursor.getColumnIndex("CREATED")));
                }          
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(resultCursor != null) {
                resultCursor.close();
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
        return authTokenFound;
    }
    
    public static void delete(final Context context) {
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.delete(TABLE_NAME, null, null);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }          
    
}
