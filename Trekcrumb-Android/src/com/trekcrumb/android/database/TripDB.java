package com.trekcrumb.android.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.exception.TrekcrumbException;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

public class TripDB {
    private static final String LOGGER_TAG = "TripDB";
    private static final String TABLE_NAME_TRIP = "TRIP";
    private static final String ORDER_BY_CREATED = "CREATED DESC";
    private static final String ORDER_BY_UPDATED = "UPDATED DESC";
    
    /**
     * Creates a new Trip record into local DB.
     * 
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param tripToCreate - Trip bean containing all its attributes' data for the Trip record.
     *  1. The Trip bean must contain required fields: UserID, created date, updated date and so on.
     *  2. If Trip ID is null, the method will generate a new ID to store. If not null, the Trip
     *     to create is a copy of already created one (from server) and it will be used.
    * @return the new TripID
     * @throws TrekcrumbException If there is any database exception.
     */
    public static String create(
            final Context context, 
            Trip tripToCreate)
            throws Exception {
        String newTripID = tripToCreate.getId();
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("_id", newTripID);
        columnsAndValues.put("USERID", tripToCreate.getUserID());
        columnsAndValues.put("NAME", tripToCreate.getName());
        columnsAndValues.put("NOTE", tripToCreate.getNote());
        columnsAndValues.put("LOCATION", tripToCreate.getLocation());
        columnsAndValues.put("STATUS", tripToCreate.getStatus().getId());
        columnsAndValues.put("PUBLISH", tripToCreate.getPublish().getId());
        columnsAndValues.put("PRIVACY", tripToCreate.getPrivacy().getId());
        columnsAndValues.put("CREATED", tripToCreate.getCreated());
        columnsAndValues.put("UPDATED", tripToCreate.getUpdated());
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.insertOrThrow(TABLE_NAME_TRIP, null, columnsAndValues);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Trip Create error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg, e);
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
        return newTripID;
    }
    
    /**
     * Retrieves a Trip or Trips based on input parameters from local DB.
     * 
     * @param tripToRetrieve - 
     *        1. Must contain the required data according to retrieveBy requirement.
     *           For example, if retrieveBy == TRIP_RETRIEVE_BY_USERID, then tripToRetrieve.getUserID()
     *           must be provided. 
     *        2. In addition, specify tripToRetrieve.getPublish() whether to retrieve only PUBLISH 
     *           or NOT_PUBLISH Trip. If not specified (null), then the method will retrieve both.
     *        3. This shall be NULL if retrieveBy == TRIP_RETRIEVE_BY_WHERE_CLAUSE
     *           
     * @param retrieveBy
     * @param whereClause - Must be provided only if retrieveBy is TRIP_RETRIEVE_BY_WHERECLAUSE.
     *                      Otherwise this is null and ignored.
     * @param whereValue  - Must be provided only if retrieveBy is TRIP_RETRIEVE_BY_WHERECLAUSE.
     *                      Otherwise this is null and ignored.
     * @param orderBy - ORDER_BY_CREATED_DATE or ORDER_BY_UPDATED_DATE. If not provided (null),
     *                  the default is by created date.
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used.
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 (or any negative),
     *                    the method will return all records.
     */
    public static List<Trip> retrieve(
            final Context context,
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            String[] whereValue,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        List<Trip> listOfTrips = null;
        
        //Compose where clause/values:
        StringBuilder whereClauseBld = null;
        ArrayList<String> whereValueList = null;
        if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID) {
            if(tripToRetrieve == null 
                    || ValidationUtil.isEmpty(tripToRetrieve.getUserID())){
                throw new IllegalArgumentException("Trip Retrieve by User id: The UserID must be provided.");
            }
            whereClauseBld = new StringBuilder();
            whereValueList = new ArrayList<String>(1);
            whereClauseBld.append("USERID = ?");
            whereValueList.add(tripToRetrieve.getUserID());
            
        } else if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID) {
            if(tripToRetrieve == null 
                    || ValidationUtil.isEmpty(tripToRetrieve.getId())){
                 throw new IllegalArgumentException("Trip Retrieve by Trip id: The TripID must be provided.");
             }
            whereClauseBld = new StringBuilder();
            whereValueList = new ArrayList<String>(2);
            whereClauseBld.append(" _id = ?");
            whereValueList.add(tripToRetrieve.getId());

        } else if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE) {
            if(ValidationUtil.isEmpty(whereClause)
                    || whereValue == null){
                throw new IllegalArgumentException("Trip Retrieve by where-clause error: where-clause and where-value are required!");
            }
            whereClauseBld = new StringBuilder();
            whereValueList = new ArrayList<String>(1);
            whereClauseBld.append(whereClause);
            for(String value : whereValue) {
                whereValueList.add(value);
            }
        
        } else {
            throw new IllegalArgumentException("Trip Retrieve error: Unsupported operation type " + retrieveBy);
        }
        
        //If publish status is provided:
        if(tripToRetrieve != null && tripToRetrieve.getPublish() != null) {
            whereClauseBld.append(" and PUBLISH = ?");
            whereValueList.add(String.valueOf(tripToRetrieve.getPublish().getId()));
        }
        String whereClauseStr = whereClauseBld.toString();
        String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;
        
        //Compose orderBy:
        String orderByStr = null;
        if(orderBy == OrderByEnum.ORDER_BY_UPDATED_DATE) {
            orderByStr = ORDER_BY_UPDATED;
        } else {
            orderByStr = ORDER_BY_CREATED;
        }
        
        //Compose limit and offset (Note: do NOT include the word 'limit'):
        String limitAndOffsetStr = null;
        StringBuilder limitAndOffsetStrBld = null;
        if(numOfRows > 0) {
            limitAndOffsetStrBld= new StringBuilder();
            if(offset <= 0) {
                limitAndOffsetStrBld.append(0 + ",");
            } else {
                limitAndOffsetStrBld.append(offset + ",");
            }
            limitAndOffsetStrBld.append(numOfRows);
        }
        if(limitAndOffsetStrBld != null) {
            limitAndOffsetStr = limitAndOffsetStrBld.toString();
        }

        //Execute transaction:
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        Cursor resultCursor = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                //Method use:
                //query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limitAndOffsetString)
                resultCursor = database.query(TABLE_NAME_TRIP, null, whereClauseStr, whereValuesStr, null, null, orderByStr, limitAndOffsetStr);
                if(resultCursor != null && resultCursor.getCount() > 0) {
                    listOfTrips = new ArrayList<Trip>();
                    while(resultCursor.moveToNext()) {
                        Trip trip = new Trip();
                        trip.setId(resultCursor.getString(resultCursor.getColumnIndex("_id")));
                        trip.setUserID(resultCursor.getString(resultCursor.getColumnIndex("USERID")));
                        trip.setName(resultCursor.getString(resultCursor.getColumnIndex("NAME")));
                        trip.setNote(resultCursor.getString(resultCursor.getColumnIndex("NOTE")));
                        trip.setLocation(resultCursor.getString(resultCursor.getColumnIndex("LOCATION")));
                        trip.setCreated(resultCursor.getString(resultCursor.getColumnIndex("CREATED")));
                        trip.setUpdated(resultCursor.getString(resultCursor.getColumnIndex("UPDATED")));
                        trip.setCompleted(resultCursor.getString(resultCursor.getColumnIndex("COMPLETED")));
                        
                        int status = resultCursor.getInt(resultCursor.getColumnIndex("STATUS"));
                        if(TripStatusEnum.ACTIVE.getId() == status) {
                            trip.setStatus(TripStatusEnum.ACTIVE);
                        } else {
                            trip.setStatus(TripStatusEnum.COMPLETED);
                        }

                        int publish = resultCursor.getInt(resultCursor.getColumnIndex("PUBLISH"));
                        if(TripPublishEnum.PUBLISH.getId() == publish) {
                            trip.setPublish(TripPublishEnum.PUBLISH);
                        } else {
                            trip.setPublish(TripPublishEnum.NOT_PUBLISH);
                        }

                        int privacy = resultCursor.getInt(resultCursor.getColumnIndex("PRIVACY"));
                        if(TripPrivacyEnum.PRIVATE.getId() == privacy) {
                            trip.setPrivacy(TripPrivacyEnum.PRIVATE);
                        } else if(TripPrivacyEnum.FRIENDS.getId() == privacy) {
                            trip.setPrivacy(TripPrivacyEnum.FRIENDS);
                        } else {
                            trip.setPrivacy(TripPrivacyEnum.PUBLIC);
                        }
                        
                        listOfTrips.add(trip);
                    } //end-while
                } //end-result not empty
            } //end-database not null
        } catch (Exception e) {
            Log.e(LOGGER_TAG, "Trip Retrieve error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if(resultCursor != null) {
                resultCursor.close();
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
        return listOfTrips;
    }
    
    /**
     * Updates a Trip in local DB given the updated Trip bean. 
     * 
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param tripToUpdate - The Trip bean that only contains the attributes to change. 
     *                      If an attribute is NULL, it will not be updated in DB.
     *                      If an attribute is CoreConstants.EMPTY_STRING, it will update as
     *                      an empty String in DB.
     * @throws TrekcrumbException
     */
    public static void update(
            final Context context, 
            Trip tripToUpdate)
            throws Exception {
        if(ValidationUtil.isEmpty(tripToUpdate.getId())
                || ValidationUtil.isEmpty(tripToUpdate.getUpdated())) {
            throw new IllegalArgumentException("Trip Update error: TripID and updated date are required!");
        }
        
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("UPDATED", tripToUpdate.getUpdated());

        if(tripToUpdate.getName() != null) {
            columnsAndValues.put("NAME", tripToUpdate.getName());
        }
        if(tripToUpdate.getStatus() == TripStatusEnum.COMPLETED) {
            //Can only update status from Open to Completed, not the other way:
            columnsAndValues.put("STATUS", tripToUpdate.getStatus().getId());
            columnsAndValues.put("COMPLETED", tripToUpdate.getCompleted());
        }
        if(tripToUpdate.getPrivacy() != null) {
            columnsAndValues.put("PRIVACY", tripToUpdate.getPrivacy().getId());
        }
        if(tripToUpdate.getPublish() != null) {
            columnsAndValues.put("PUBLISH", tripToUpdate.getPublish().getId());
        }
        if(tripToUpdate.getNote() != null) {
            columnsAndValues.put("NOTE", tripToUpdate.getNote());
        }
        if(tripToUpdate.getLocation() != null) {
            columnsAndValues.put("LOCATION", tripToUpdate.getLocation());
        }
        
        String whereClause = "_id = ?";
        String[] whereArgs = {tripToUpdate.getId()};
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.update(TABLE_NAME_TRIP, columnsAndValues, whereClause, whereArgs);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Trip Update error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg, e);
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }    
    
    /**
     * Updates any open (active) Trips to 'COMPLETED' status for a given User.
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param userID - The user whose Trip(s) will be updated to complete.
     * @throws TrekcrumbException
     */
    public static void updateTripStatusCompleted(
            final Context context,
            String userID, 
            TripPublishEnum publishOrNot)
            throws Exception {
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("STATUS", TripStatusEnum.COMPLETED.getId());
        
        String currentDate = DateUtil.getCurrentTimeWithGMTTimezone();
        columnsAndValues.put("UPDATED", currentDate);
        columnsAndValues.put("COMPLETED", currentDate);
        
        StringBuilder whereClauseBld = new StringBuilder();
        ArrayList<String> whereValueList = new ArrayList<String>();
        whereClauseBld.append("USERID = ?");
        whereValueList.add(userID);
        whereClauseBld.append(" and STATUS = ?");
        whereValueList.add(String.valueOf(TripStatusEnum.ACTIVE.getId()));
        if(publishOrNot == TripPublishEnum.NOT_PUBLISH) {
            //Update only not-publish trip(s):
            whereClauseBld.append(" and PUBLISH = ?");
            whereValueList.add(String.valueOf(TripPublishEnum.NOT_PUBLISH.getId()));
            
        } else if(publishOrNot == TripPublishEnum.PUBLISH) {
            //Update only publish trip(s):
            whereClauseBld.append(" and PUBLISH = ?");
            whereValueList.add(String.valueOf(TripPublishEnum.PUBLISH.getId()));
            
        } //Else: Update ALL publish/not-publish
        
        String whereClauseStr = whereClauseBld.toString();
        String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.update(TABLE_NAME_TRIP, columnsAndValues, whereClauseStr, whereValuesStr);
                database.setTransactionSuccessful();
            }
            
        } catch (Exception e) {
            String errMsg = "Trip Update status completed error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg, e);
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }
    
    /**
     * Deletes a Trip or Trips based on the input parameters from local DB.
     * @param tripToDelete
     * @param deleteBy
     * @return Number of deleted items.
     * @throws Exception
     */
    public static int delete(
            final Context context,
            Trip tripToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        int deletedItems = -1;
        
        //Compose where clause/value:
        String whereClause = null;
        ArrayList<String> whereValueList = null;
        if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
            if(tripToDelete == null
                    || ValidationUtil.isEmpty(tripToDelete.getUserID())){
                throw new IllegalArgumentException("Trip Delete by User id: The UserID must be provided!");
            }
            whereClause = "USERID = ?";
            whereValueList = new ArrayList<String>(1);
            whereValueList.add(tripToDelete.getUserID());
            
        } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
            if(tripToDelete == null
                    || ValidationUtil.isEmpty(tripToDelete.getUserID())
                    || ValidationUtil.isEmpty(tripToDelete.getId())){
                throw new IllegalArgumentException("Trip Delete by Trip id: The UserID and TripID must be provided!");
            }
            whereClause = "_id = ? and USERID = ?";
            whereValueList = new ArrayList<String>(2);
            whereValueList.add(tripToDelete.getId());
            whereValueList.add(tripToDelete.getUserID());

        } else {
            throw new IllegalArgumentException("Trip Delete error: Unsupported operation type - " + deleteBy);
        }
        
        String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;

        //Process transaction:
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                deletedItems = database.delete(TABLE_NAME_TRIP, whereClause, whereValuesStr);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Trip Delete error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg, e);
            throw new TrekcrumbException(errMsg);
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
        return deletedItems;
    }
    
    public static int count(
            Context context,
            Trip tripToCount) 
            throws Exception {
        if(tripToCount == null
                || ValidationUtil.isEmpty(tripToCount.getUserID())){
            throw new IllegalArgumentException("Trip Count error: The UserID must be provided!");
        }
        int numOfTrips = -1;
        StringBuilder nativeQueryBld = new StringBuilder();
        nativeQueryBld.append("select count(*) from TRIP where USERID = '" + tripToCount.getUserID() + "'");
        if(tripToCount.getPublish() != null) {
            nativeQueryBld.append(" and PUBLISH = " + tripToCount.getPublish().getId()); 
        }

        //Process transaction:
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        Cursor resultCursor = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                resultCursor = database.rawQuery(nativeQueryBld.toString(), null);
                if(resultCursor != null) {
                    resultCursor.moveToFirst();
                    numOfTrips = resultCursor.getInt(0);
                }
            }
        } catch (Exception e) {
            String errMsg = "Trip Count error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
            throw e;
        } finally {
            if(resultCursor != null) {
                resultCursor.close();
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }        
        return numOfTrips;
    }    

}
