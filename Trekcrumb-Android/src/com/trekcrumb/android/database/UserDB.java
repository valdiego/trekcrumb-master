package com.trekcrumb.android.database;

import java.util.ArrayList;
import java.util.Arrays;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.TrekcrumbException;
import com.trekcrumb.common.utility.ValidationUtil;

public class UserDB {
    private static final String LOGGER_TAG = UserDB.class.getName();
    private static final String TABLE_NAME_USER = "USER";

    /**
     * Finds User given user ID.
     * @return User bean containing the data, or NULL of no matching found.
     */
    public static User retrieve(
            final Context context,
            final User userToRetrieve) {
        if(userToRetrieve == null
                || ValidationUtil.isEmpty(userToRetrieve.getUserId())) {
            throw new IllegalArgumentException("Retrieve User error - UserID is required!");
        }
        User user = null;
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        Cursor resultCursor = null;
        
        String query = "select * from USER where _id = ?";
        String[] args = {userToRetrieve.getUserId()};
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                resultCursor = database.rawQuery(query, args);
                if(resultCursor!= null && resultCursor.getCount() > 0) {
                    //Expect only one unique result:
                    resultCursor.moveToFirst();
                    user = new User();
                    user.setUserId(resultCursor.getString(resultCursor.getColumnIndex("_id")));
                    user.setUsername(resultCursor.getString(resultCursor.getColumnIndex("USERNAME")));
                    user.setFullname(resultCursor.getString(resultCursor.getColumnIndex("FULLNAME")));
                    user.setLocation(resultCursor.getString(resultCursor.getColumnIndex("LOCATION")));
                    user.setSummary(resultCursor.getString(resultCursor.getColumnIndex("SUMMARY")));
                    user.setProfileImageName(resultCursor.getString(resultCursor.getColumnIndex("PROFILE_IMAGE_NAME")));
                    user.setProfileImageURL(resultCursor.getString(resultCursor.getColumnIndex("PROFILE_IMAGE_URL")));
                    user.setCreated(resultCursor.getString(resultCursor.getColumnIndex("CREATED")));
                    user.setUpdated(resultCursor.getString(resultCursor.getColumnIndex("UPDATED")));
                }                
            }
            
        } catch (Exception e) {
            Log.e(LOGGER_TAG, "ERROR: exception when find user from database with message: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if(resultCursor != null) {
                resultCursor.close();
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
        return user;
    }
    
    /**
     * Insert new User record in database.
     * 
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @throws TrekcrumbException If there is any database exception.
     */
    public static void create(
            final Context context, 
            final User userToCreate) {
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("_id", userToCreate.getUserId());
        columnsAndValues.put("USERNAME", userToCreate.getUsername());
        columnsAndValues.put("FULLNAME", userToCreate.getFullname());
        columnsAndValues.put("SUMMARY", userToCreate.getSummary());
        columnsAndValues.put("LOCATION", userToCreate.getLocation());
        columnsAndValues.put("PROFILE_IMAGE_NAME", userToCreate.getProfileImageName());
        columnsAndValues.put("PROFILE_IMAGE_URL", userToCreate.getProfileImageURL());
        columnsAndValues.put("CREATED", userToCreate.getCreated());
        columnsAndValues.put("UPDATED", userToCreate.getUpdated());
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.insertOrThrow(TABLE_NAME_USER, null, columnsAndValues);
                database.setTransactionSuccessful();
            }
            
        } catch (Exception e) {
            String errMsg = "Create User - Failed with message: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }
    
    public static void update(
            final Context context, 
            final User userToUpdate)
            throws TrekcrumbException {
        if(userToUpdate == null 
                || ValidationUtil.isEmpty(userToUpdate.getUserId())) {
            throw new IllegalArgumentException("Update User error - UserID is required!");
        }
        
        ContentValues columnsAndValues = new ContentValues();
        if(userToUpdate.getFullname() != null) {
            columnsAndValues.put("FULLNAME", userToUpdate.getFullname());
        }
        if(userToUpdate.getSummary() != null) {
            columnsAndValues.put("SUMMARY", userToUpdate.getSummary());
        }
        if(userToUpdate.getLocation() != null) {
            columnsAndValues.put("LOCATION", userToUpdate.getLocation());
        }
        if(userToUpdate.getProfileImageName() != null) {
            columnsAndValues.put("PROFILE_IMAGE_NAME", userToUpdate.getProfileImageName());
        }
        if(userToUpdate.getProfileImageURL() != null) {
            columnsAndValues.put("PROFILE_IMAGE_URL", userToUpdate.getProfileImageURL());
        }
        
        if(columnsAndValues.size() == 0) {
            //Nothing to update:
            return;
        } else {
            columnsAndValues.put("UPDATED", userToUpdate.getUpdated());
        }
        
        String whereClause = "_id = ?";
        String[] whereArgs = {userToUpdate.getUserId()};
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.update(TABLE_NAME_USER, columnsAndValues, whereClause, whereArgs);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Update User error - " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
            throw new TrekcrumbException(errMsg);
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }
    
    public static void delete(
            final Context context,
            final User userToDelete,
            ServiceTypeEnum deleteBy) {
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;

        StringBuilder whereClauseBld = null;
        ArrayList<String> whereValueList = null;
        if(deleteBy == ServiceTypeEnum.USER_DELETE_BY_USERID) {
            if(userToDelete == null
                    || ValidationUtil.isEmpty(userToDelete.getUserId())){
                throw new IllegalArgumentException("Delete User by User id: The UserID must be provided!");
            }
            whereClauseBld = new StringBuilder();
            whereValueList = new ArrayList<String>(1);
            whereClauseBld.append("_id = ?");
            whereValueList.add(userToDelete.getUserId());
            
        } else if(deleteBy == ServiceTypeEnum.USER_DELETE_BY_USERNAME) {
            if(userToDelete == null
                    || ValidationUtil.isEmpty(userToDelete.getUsername())){
                throw new IllegalArgumentException("Delete User by Username: The username must be provided!");
            }
            whereClauseBld = new StringBuilder();
            whereValueList = new ArrayList<String>(1);
            whereClauseBld.append("USERNAME = ?");
            whereValueList.add(userToDelete.getUsername());

        } else {
            throw new IllegalArgumentException("Delete User - Unsupported operation type: " + deleteBy);
        }            
        String whereClauseStr = whereClauseBld.toString();
        String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;

        try {

            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.delete(TABLE_NAME_USER, whereClauseStr, whereValuesStr);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Delete User error - " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }        
    
}
