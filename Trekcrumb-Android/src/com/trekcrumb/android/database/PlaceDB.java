package com.trekcrumb.android.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.TrekcrumbException;
import com.trekcrumb.common.utility.ValidationUtil;

public class PlaceDB {
    private static final String LOGGER_TAG = PlaceDB.class.getName();
    private static final String TABLE_NAME_PLACE = "PLACE";
    private static final String ORDER_BY_CREATED = "CREATED ASC";

    /**
     * Insert new Place record in local database.
     * 
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param placeToCreate - Place bean containing all its attributes' data for the Place record.
     *  1. The Place bean must contain required fields: UserID, TripID, created date and so on.
     *  2. If Place ID is null, the method will generate a new ID to store. If not null, the place
     *     to create is a copy of already created one (from server) and it will be used.
     * @return the new PlaceID
     * @throws Exception If there is any database exception.
     */
    public static String create(
            final Context context, 
            Place placeToCreate)
            throws Exception {
        String newPlaceID = placeToCreate.getId();
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("_id", newPlaceID);
        columnsAndValues.put("USERID", placeToCreate.getUserID());
        columnsAndValues.put("TRIPID", placeToCreate.getTripID());
        columnsAndValues.put("LATITUDE", placeToCreate.getLatitude());
        columnsAndValues.put("LONGITUDE", placeToCreate.getLongitude());
        columnsAndValues.put("NOTE", placeToCreate.getNote());
        columnsAndValues.put("LOCATION", placeToCreate.getLocation());
        columnsAndValues.put("CREATED", placeToCreate.getCreated());
        columnsAndValues.put("UPDATED", placeToCreate.getUpdated());
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.insertOrThrow(TABLE_NAME_PLACE, null, columnsAndValues);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Place Create error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
        return newPlaceID;
    }
    
    /**
     * Retrieves a Place or Places based on input parameters from local DB. By default, the list
     * returned is ordered by Created date ascending (the first added will be the first record retrieved).
     * 
     * @return List of Place beans, if any. NULL otherwise.
     */
    public static List<Place> retrieve(
            final Context context,
            Place placeToRetrieve,
            ServiceTypeEnum retrieveBy,
            int offset,
            int numOfRows) 
            throws Exception {
        List<Place> listOfPlaces = null;
        
        //Compose where clause/values:
        StringBuilder whereClauseBld = null;
        ArrayList<String> whereValueList = null;
        if(retrieveBy == ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID) {
            if(placeToRetrieve == null
                    || ValidationUtil.isEmpty(placeToRetrieve.getUserID())
                    || ValidationUtil.isEmpty(placeToRetrieve.getTripID())){
                throw new IllegalArgumentException("Place Retrieve by Trip ID error: The UserID and TripID are required!");
            }
            whereClauseBld = new StringBuilder();
            whereValueList = new ArrayList<String>(2);
            whereClauseBld.append("USERID = ?");
            whereValueList.add(placeToRetrieve.getUserID());
            whereClauseBld.append(" and TRIPID = ?");
            whereValueList.add(placeToRetrieve.getTripID());
            
        } else if(retrieveBy == ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID) {
            if(placeToRetrieve == null 
                    || ValidationUtil.isEmpty(placeToRetrieve.getUserID())
                    || ValidationUtil.isEmpty(placeToRetrieve.getId())){
                 throw new IllegalArgumentException("Place Retrieve by Place ID error: The UserID and PlaceID are required!");
             }
             whereClauseBld = new StringBuilder();
             whereValueList = new ArrayList<String>(2);
             whereClauseBld.append("USERID = ?");
             whereValueList.add(placeToRetrieve.getUserID());

             whereClauseBld.append(" and _id = ?");
             whereValueList.add(placeToRetrieve.getId());
        } else {
            throw new IllegalArgumentException("Place Retrieve error: Unsupported operation type - " + retrieveBy);
        }
        String whereClauseStr = whereClauseBld.toString();
        String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;
        
        //Compose limit:
        /* Note:
         * 1/2014 - NEED to implement offset and numOfRow (see how we did in TripDB.retrieve()
         * 9/2014 - However, for local call we always retrieve ALL pictures during tripEnrichment.
         *          So more likely offset and numOfRows are never used.
         */
        String limitQueryStr = null;

        //Execute transaction:
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        Cursor resultCursor = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                //Method use:
                //query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
                resultCursor = database.query(TABLE_NAME_PLACE, null, whereClauseStr, whereValuesStr, null, null, ORDER_BY_CREATED, limitQueryStr);
                if(resultCursor!= null && resultCursor.getCount() > 0) {
                    listOfPlaces = new ArrayList<Place>();
                    while(resultCursor.moveToNext()) {
                        Place place = new Place(resultCursor.getDouble(resultCursor.getColumnIndex("LATITUDE")), 
                                                resultCursor.getDouble(resultCursor.getColumnIndex("LONGITUDE")));
                        place.setId(resultCursor.getString(resultCursor.getColumnIndex("_id")));
                        place.setUserID(resultCursor.getString(resultCursor.getColumnIndex("USERID")));
                        place.setTripID(resultCursor.getString(resultCursor.getColumnIndex("TRIPID")));
                        place.setLocation(resultCursor.getString(resultCursor.getColumnIndex("LOCATION")));
                        place.setNote(resultCursor.getString(resultCursor.getColumnIndex("NOTE")));
                        place.setCreated(resultCursor.getString(resultCursor.getColumnIndex("CREATED")));
                        place.setUpdated(resultCursor.getString(resultCursor.getColumnIndex("UPDATED")));
                        listOfPlaces.add(place);
                    } //end-while
                } //end-result not empty
            } //end-database not null

        } catch (Exception e) {
            Log.e(LOGGER_TAG, "Place Retrieve error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if(resultCursor != null) {
                resultCursor.close();
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        } 
        return listOfPlaces;
    }    
    
    /**
     * Updates a Place given the updated Place bean.
     * This operation is at least WILL update its updated date (required).
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param placeToUpdate - The Place bean that only contains the attributes to change. 
     *                      If an attribute is NULL, it will not be updated in DB.
     *                      If an attribute is CoreConstants.EMPTY_STRING, it will update as
     *                      an empty String in DB.
     * @throws TrekcrumbException
     */
    public static void update(
            final Context context, 
            Place placeToUpdate)
            throws Exception {
        if(ValidationUtil.isEmpty(placeToUpdate.getId())
                || ValidationUtil.isEmpty(placeToUpdate.getUpdated())) {
            throw new IllegalArgumentException("Place Update error: PlaceID and updated date are required!");
        }
        
        ContentValues columnsAndValues = new ContentValues();
        columnsAndValues.put("UPDATED", placeToUpdate.getUpdated());
        if(placeToUpdate.getNote() != null) {
            columnsAndValues.put("NOTE", placeToUpdate.getNote());
        }
        String whereClause = "_id = ?";
        String[] whereArgs = {placeToUpdate.getId()};
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                database.update(TABLE_NAME_PLACE, columnsAndValues, whereClause, whereArgs);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Place Update error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }
    
    /**
     * Deletes a Place or Places based on the input parameters.
     * @param placeToDelete
     * @param deleteBy
     * @return Number of deleted items.
     * @throws Exception
     */
    public static int delete(
            final Context context,
            Place placeToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        int deletedItems = -1;
        String whereClause = null;
        ArrayList<String> whereValueList = null;
            if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_USERID) {
                if(placeToDelete == null
                        || ValidationUtil.isEmpty(placeToDelete.getUserID())){
                    throw new IllegalArgumentException("Place Delete by User id error: The UserID must be provided!");
                }
                whereClause = "USERID = ?";
                whereValueList = new ArrayList<String>(1);
                whereValueList.add(placeToDelete.getUserID());
                
            } else if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_TRIPID) {
                if(placeToDelete == null
                        || ValidationUtil.isEmpty(placeToDelete.getUserID())
                        || ValidationUtil.isEmpty(placeToDelete.getTripID())){
                    throw new IllegalArgumentException("Place Delete by Trip id error: The UserID and TripID must be provided!");
                }
                whereClause = "TRIPID = ? AND USERID = ?";
                whereValueList = new ArrayList<String>(2);
                whereValueList.add(placeToDelete.getTripID());
                whereValueList.add(placeToDelete.getUserID());
                
            } else if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_PLACEID) {
                if(placeToDelete == null
                        || ValidationUtil.isEmpty(placeToDelete.getUserID())
                        || ValidationUtil.isEmpty(placeToDelete.getId())){
                    throw new IllegalArgumentException("Place Delete by Place id: The UserID and PlaceID must be provided!");
                }
                whereClause = "_id = ? AND USERID = ?";
                whereValueList = new ArrayList<String>(2);
                whereValueList.add(placeToDelete.getId());
                whereValueList.add(placeToDelete.getUserID());
            } else {
                throw new IllegalArgumentException("Place Delete error: Unsupported operation type - " + deleteBy);
            }
            String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;


            DatabaseSQLiteOpenHelperImpl dbHelper = null;
            SQLiteDatabase database = null;
            try {
                dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
                database = dbHelper.openDatabase();
                if(database != null) {
                    database.beginTransaction();
                    deletedItems = database.delete(TABLE_NAME_PLACE, whereClause, whereValuesStr);
                    database.setTransactionSuccessful();
                }
            } catch (Exception e) {
                String errMsg = "Place Delete error: " + e.getMessage();
                Log.e(LOGGER_TAG, errMsg);
                e.printStackTrace();
                throw e;
            } finally {
                if(database != null) {
                    try {
                        //Ensure transaction is commited/closed, or rollback if it is not successful
                        database.endTransaction();
                    } catch(Exception e2) {}
                }
                if(dbHelper != null) {
                    try {
                        dbHelper.closeDatabase();
                    } catch (Exception e) {} //Cant do anything
                }
            }
            return deletedItems;
    }            
    
}
