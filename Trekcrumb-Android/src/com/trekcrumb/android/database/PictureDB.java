package com.trekcrumb.android.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.TrekcrumbException;
import com.trekcrumb.common.utility.ValidationUtil;

public class PictureDB {
    private static final String LOGGER_TAG = "PictureDB";
    private static final String TABLE_NAME_PICTURE = "PICTURE";
    private static final String ORDER_BY_CREATED = "CREATED ASC";

    /**
     * Insert new Picture record in database.
     * 
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param pictureToCreate - Picture bean containing all its attributes' data for the Picture record.
     *  1. The Picture bean must contain required fields: UserID, TripID, created date and so on.
     *  2. If Picture ID is null, the method will generate a new ID to store. If not null, the record
     *     to create is a copy of already created one (from server) and it will be used.
     * @return the new PictureID
     * @throws TrekcrumbException If there is any database exception.
     */
    public static String create(
            final Context context, 
            Picture pictureToCreate)
            throws Exception {
        String newPicID = null;
        if(ValidationUtil.isEmpty(pictureToCreate.getId())) {
            //Freshly new record:
            newPicID = String.valueOf(System.currentTimeMillis());
        } else {
            //A copy record from server:
            newPicID = pictureToCreate.getId();
        }
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        ContentValues columnsAndValues = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                columnsAndValues = new ContentValues();

                if(pictureToCreate.getCoverPicture() == YesNoEnum.YES) {
                    //Update previous cover picture for this Trip, if any, to have 'CoverForTrip = 0':
                    columnsAndValues.put("IS_COVER_PICTURE", YesNoEnum.NO.getId());
                    String whereClause = "TRIPID = ? AND IS_COVER_PICTURE = ?";
                    String[] whereArgs = {pictureToCreate.getTripID(), String.valueOf(YesNoEnum.NO.getId())};
                    database.update(TABLE_NAME_PICTURE, columnsAndValues, whereClause, whereArgs);
                }

                //Compose values:
                columnsAndValues.clear();
                columnsAndValues.put("_id", newPicID);
                columnsAndValues.put("USERID", pictureToCreate.getUserID());
                columnsAndValues.put("TRIPID", pictureToCreate.getTripID());
                columnsAndValues.put("IMAGE_NAME", pictureToCreate.getImageName());
                columnsAndValues.put("IMAGE_URL", pictureToCreate.getImageURL());
                columnsAndValues.put("CREATED", pictureToCreate.getCreated());
                columnsAndValues.put("UPDATED", pictureToCreate.getUpdated());
                columnsAndValues.put("NOTE", pictureToCreate.getNote());
                //columnsAndValues.put("FEATURE_FOR_PLACEID", pictureToCreate.getCoverForPlace());
                
                if(pictureToCreate.getCoverPicture() == YesNoEnum.YES) {
                    columnsAndValues.put("IS_COVER_PICTURE", YesNoEnum.YES.getId());
                } else {
                    columnsAndValues.put("IS_COVER_PICTURE", YesNoEnum.NO.getId());
                }
                
                //Execute and commit:                
                database.insertOrThrow(TABLE_NAME_PICTURE, null, columnsAndValues);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Picture Create error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }        
        return pictureToCreate.getId();
    }
    
    /**
     * Retrieves a Picture or Pictures based on input parameters from local DB. By default, the list
     * returned is ordered by Created date ascending (the first added will be the first record retrieved).
     * 
     * @return List of Place beans, if any. NULL otherwise.
     */
    public static List<Picture> retrieve(
            final Context context,
            Picture pictureToRetrieve,
            ServiceTypeEnum retrieveBy,
            int offset,
            int numOfRows) 
            throws Exception {
        List<Picture> listOfPictures = null;
        
        //Compose where clause/values:
        String whereClause = null;
        ArrayList<String> whereValueList = null;
        if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID) {
            if(pictureToRetrieve == null
                    || ValidationUtil.isEmpty(pictureToRetrieve.getUserID())){
                throw new IllegalArgumentException("Picture Retrieve by User ID error: The UserID is required!");
            }
            whereClause = "USERID = ?";
            whereValueList = new ArrayList<String>(2);
            whereValueList.add(pictureToRetrieve.getUserID());
        } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID) {
            if(pictureToRetrieve == null
                    || ValidationUtil.isEmpty(pictureToRetrieve.getUserID())
                    || ValidationUtil.isEmpty(pictureToRetrieve.getTripID())){
                throw new IllegalArgumentException("Picture Retrieve by Trip ID error: The UserID and TripID are required!");
            }
            whereClause = "USERID = ? and TRIPID = ?";
            whereValueList = new ArrayList<String>(2);
            whereValueList.add(pictureToRetrieve.getUserID());
            whereValueList.add(pictureToRetrieve.getTripID());
            
        } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID) {
            if(pictureToRetrieve == null 
                    || ValidationUtil.isEmpty(pictureToRetrieve.getUserID())
                    || ValidationUtil.isEmpty(pictureToRetrieve.getId())){
                 throw new IllegalArgumentException("Picture Retrieve by Place ID error: The UserID and PictureID are required!");
            }
            whereClause = "_id = ? and USERID = ?";
            whereValueList = new ArrayList<String>(2);
            whereValueList.add(pictureToRetrieve.getId());
            whereValueList.add(pictureToRetrieve.getUserID());

        } else {
            throw new IllegalArgumentException("Picture Retrieve error: Unsupported operation type - " + retrieveBy);
        }
        String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;
        
        //Compose limit:
        /* Note:
         * 1/2014 - NEED to implement offset and numOfRow (see how we did in TripDB.retrieve()
         * 9/2014 - However, for local call we always retrieve ALL pictures during tripEnrichment.
         *          So more likely offset and numOfRows are never used.
         */
        String limitQueryStr = null;

        //Execute transaction:
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        Cursor resultCursor = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                //Method use:
                //query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
                resultCursor = database.query(TABLE_NAME_PICTURE, null, whereClause, whereValuesStr, null, null, ORDER_BY_CREATED, limitQueryStr);
                if(resultCursor!= null && resultCursor.getCount() > 0) {
                    listOfPictures = new ArrayList<Picture>();
                    while(resultCursor.moveToNext()) {
                        Picture picture = new Picture();
                        picture.setId(resultCursor.getString(resultCursor.getColumnIndex("_id")));
                        picture.setUserID(resultCursor.getString(resultCursor.getColumnIndex("USERID")));
                        picture.setTripID(resultCursor.getString(resultCursor.getColumnIndex("TRIPID")));
                        picture.setNote(resultCursor.getString(resultCursor.getColumnIndex("NOTE")));
                        picture.setImageName(resultCursor.getString(resultCursor.getColumnIndex("IMAGE_NAME")));
                        picture.setImageURL(resultCursor.getString(resultCursor.getColumnIndex("IMAGE_URL")));
                        picture.setCreated(resultCursor.getString(resultCursor.getColumnIndex("CREATED")));
                        picture.setUpdated(resultCursor.getString(resultCursor.getColumnIndex("UPDATED")));
                        picture.setFeatureForPlaceID(resultCursor.getString(resultCursor.getColumnIndex("FEATURE_FOR_PLACEID")));
                        
                        if(resultCursor.getInt(resultCursor.getColumnIndex("IS_COVER_PICTURE")) == 1) {
                            picture.setCoverPicture(YesNoEnum.YES);
                        } else {
                            picture.setCoverPicture(YesNoEnum.NO);
                        }
                        
                        listOfPictures.add(picture);
                    } //end-while
                } //end-result not empty
            } //end-database not null

        } catch (Exception e) {
            Log.e(LOGGER_TAG, "Picture Retrieve error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if(resultCursor != null) {
                resultCursor.close();
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        } 
        return listOfPictures;
    }        

    /**
     * Updates a Picture given the updated Picture bean.
     * This operation is at least WILL update its updated date (required).
     * @param context - The hosting context (page) such as Activity or Fragment.
     * @param updatedPlace - The Place bean that only contains the attributes to change. 
     *                      If an attribute is NULL, it will not be updated in DB.
     *                      If an attribute is CoreConstants.EMPTY_STRING, it will update as
     *                      an empty String in DB.
     * @throws TrekcrumbException
     */
    public static void update(
            final Context context, 
            Picture pictureToUpdate)
            throws Exception {
        if(ValidationUtil.isEmpty(pictureToUpdate.getId())
                || ValidationUtil.isEmpty(pictureToUpdate.getUpdated())) {
            throw new IllegalArgumentException("Picture Update error: PictureID and updated date are required!");
        }
        
        DatabaseSQLiteOpenHelperImpl dbHelper = null;
        SQLiteDatabase database = null;
        ContentValues columnsAndValues = null;
        try {
            dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
            database = dbHelper.openDatabase();
            if(database != null) {
                database.beginTransaction();
                columnsAndValues = new ContentValues();

                if(pictureToUpdate.getCoverPicture() == YesNoEnum.YES) {
                    //Update previous Trip cover picture, if any, to have 'main pic for trip = NO':
                    columnsAndValues.put("IS_COVER_PICTURE", YesNoEnum.NO.getId());
                    columnsAndValues.put("UPDATED", pictureToUpdate.getUpdated());
                    String whereClause = "TRIPID = ? AND IS_COVER_PICTURE = ?";
                    String[] whereArgs = {pictureToUpdate.getTripID(), String.valueOf(YesNoEnum.YES.getId())};
                    database.update(TABLE_NAME_PICTURE, columnsAndValues, whereClause, whereArgs);
                }

                //Compose query:
                columnsAndValues.clear();
                columnsAndValues.put("UPDATED", pictureToUpdate.getUpdated());
                if(pictureToUpdate.getNote() != null) {
                    columnsAndValues.put("NOTE", pictureToUpdate.getNote());
                }
                if(pictureToUpdate.getCoverPicture() == YesNoEnum.YES) {
                    columnsAndValues.put("IS_COVER_PICTURE", YesNoEnum.YES.getId());
                } else if(pictureToUpdate.getCoverPicture() == YesNoEnum.NO) {
                    columnsAndValues.put("IS_COVER_PICTURE", YesNoEnum.NO.getId());
                } //else: nothing to update
                
                if(pictureToUpdate.getFeatureForPlaceID() != null) {
                    columnsAndValues.put("FEATURE_FOR_PLACEID", pictureToUpdate.getFeatureForPlaceID());        
                }
                String whereClause = "_id = ?";
                String[] whereArgs = {pictureToUpdate.getId()};
                
                //Execute and commit:
                database.update(TABLE_NAME_PICTURE, columnsAndValues, whereClause, whereArgs);
                database.setTransactionSuccessful();
            }
        } catch (Exception e) {
            String errMsg = "Picture Update error: " + e.getMessage();
            Log.e(LOGGER_TAG, errMsg);
            e.printStackTrace();
            throw e;
        } finally {
            if(database != null) {
                try {
                    //Ensure transaction is commited/closed, or rollback if it is not successful
                    database.endTransaction();
                } catch(Exception e2) {}
            }
            if(dbHelper != null) {
                try {
                    dbHelper.closeDatabase();
                } catch (Exception e) {} //Cant do anything
            }
        }
    }
    
    /**
     * Deletes a Picture or Pictures based on the input parameters.
     * @param pictureToDelete
     * @param deleteBy
     * @return Number of deleted items.
     * @throws Exception
     */
    public static int delete(
            final Context context,
            Picture pictureToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        int deletedItems = -1;
        String whereClause = null;
        ArrayList<String> whereValueList = null;
            if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_USERID) {
                if(pictureToDelete == null
                        || ValidationUtil.isEmpty(pictureToDelete.getUserID())){
                    throw new IllegalArgumentException("Picture Delete by User id: The UserID must be provided!");
                }
                whereClause = "USERID = ?";
                whereValueList = new ArrayList<String>(1);
                whereValueList.add(pictureToDelete.getUserID());
            } else if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID) {
                if(pictureToDelete == null
                        || ValidationUtil.isEmpty(pictureToDelete.getUserID())
                        || ValidationUtil.isEmpty(pictureToDelete.getTripID())){
                    throw new IllegalArgumentException("Picture Delete by Trip id: The UserID and TripID must be provided!");
                }
                whereClause = "USERID = ? and TRIPID = ?";
                whereValueList = new ArrayList<String>(2);
                whereValueList.add(pictureToDelete.getUserID());
                whereValueList.add(pictureToDelete.getTripID());
            } else if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID) {
                if(pictureToDelete == null
                        || ValidationUtil.isEmpty(pictureToDelete.getUserID())
                        || ValidationUtil.isEmpty(pictureToDelete.getTripID())
                        || ValidationUtil.isEmpty(pictureToDelete.getId())){
                    throw new IllegalArgumentException("Picture Delete by Picture id: The UserID and PictureID must be provided!");
                }
                whereClause = "_id = ? and USERID = ?";
                whereValueList = new ArrayList<String>(2);
                whereValueList.add(pictureToDelete.getId());
                whereValueList.add(pictureToDelete.getUserID());
            } else {
                throw new IllegalArgumentException("Picture Delete error: Unsupported operation type - " + deleteBy);
            }
            //String whereClauseStr = whereClauseBld.toString();
            String[] whereValuesStr = Arrays.copyOf(whereValueList.toArray(), whereValueList.toArray().length, String[].class);;

            DatabaseSQLiteOpenHelperImpl dbHelper = null;
            SQLiteDatabase database = null;
            try {
                dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(context);
                database = dbHelper.openDatabase();
                if(database != null) {
                    database.beginTransaction();
                    deletedItems = database.delete(TABLE_NAME_PICTURE, whereClause, whereValuesStr);
                    database.setTransactionSuccessful();
                }
            } catch (Exception e) {
                String errMsg = "Picture Delete error: " + e.getMessage();
                Log.e(LOGGER_TAG, errMsg);
                e.printStackTrace();
                throw e;
            } finally {
                if(database != null) {
                    try {
                        //Ensure transaction is commited/closed, or rollback if it is not successful
                        database.endTransaction();
                    } catch(Exception e2) {}
                }
                if(dbHelper != null) {
                    try {
                        dbHelper.closeDatabase();
                    } catch (Exception e) {} //Cant do anything
                }
            }
            return deletedItems;
    }            
    
}
