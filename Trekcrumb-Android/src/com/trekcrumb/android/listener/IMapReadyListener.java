package com.trekcrumb.android.listener;

import java.io.Serializable;

/**
 * An interface that listen when MapFragment has been created and its GoogleMap is ready, 
 * and can be called via its callback method.
 *
 */
public interface IMapReadyListener extends Serializable {

    /**
     * A callback method to be invoked to let the instance class that implements this 
     * interface know that the GoogleMap instance is ready to use. 
     * 
     * Extending child Activity will have to implement this method ONLY IF they need a 
     * Map fragment and call its getMap() method ensuring the GoogleMap instance
     * has been successfully initialized.
     */
    public void onMapFragmentReadyCallback();
    
}
