package com.trekcrumb.android.listener;

import java.io.Serializable;

/**
 * An interface that listens to ProgressWaitThreadImpl, and can be call when 
 * ProgressWaitThreadImpl finishes its job.
 *
 */
public interface IProgressWaitListener extends Serializable {
    
    /** 
     * Callback method called by ProgressWaitThreadImpl.
     * 
     * Extending child Activity will have to implement this method ONLY IF when the activity 
     * starts this as a new Thread, and need to continue processing after the thread completes 
     * its work.
     */
    public void onProgressCompletedCallback();
}
