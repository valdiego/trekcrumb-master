package com.trekcrumb.android.listener;

import java.io.Serializable;

import android.location.Location;

/**
 * An interface that listens for new update for GPS GeoLocation.
 * 
 */
public interface IGeoLocationUpdateListener extends Serializable {
    
    /**
     * A callback method to be invoked to update GPS location on the instance class
     * that implements this interface. When its callback method is invoked, 
     * the Location is either successfully found or NULL.
     * 
     * @param location - The newly updated Location from LocationListenerImpl, or NULL when
     *                   nothing is found after a timeout.
     */
    public void onGeoLocationUpdateCallback(Location location);
    

}
