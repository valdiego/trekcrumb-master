package com.trekcrumb.android.listener;

import java.io.Serializable;

import com.google.android.gms.maps.model.Marker;

/**
 * An interface that listen when Map's marker is being click and call its callback method.
 *
 */
public interface IMapMarkerClickListener extends Serializable {

    public void onMarkerClickCallback(Marker marker);
    
}
