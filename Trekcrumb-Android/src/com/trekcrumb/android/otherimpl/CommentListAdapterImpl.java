package com.trekcrumb.android.otherimpl;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.trekcrumb.android.R;
import com.trekcrumb.android.layoutholder.CommentListLayoutHolder;
import com.trekcrumb.common.bean.Comment;

/**
 * Custom implementation of Android ArrayAdapter for list of Comment objects.
 * 
 * The adapter modified getView() to control how each row of the list will look (display),
 * their values and their behavior (clickable or not, etc.).
 *
 */
public class CommentListAdapterImpl 
extends ArrayAdapter<Comment> {
    private final Context mContext;
    private final List<Comment> mList;
    private boolean mIsUserLoginProfile;
    
    private final LayoutInflater mInflater;

    public CommentListAdapterImpl(
            Context context, 
            List<Comment> theList, 
            boolean isUserLoginProfile) {
        super(context, R.layout.template_row_comment, theList);
        mContext = context;
        mList = theList;
        mIsUserLoginProfile = isUserLoginProfile;
        
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    /**
     * Returned the intended View containing data values, replacing the default toString().
     * 
     * The system calls this method repeatedly to display the value when iterating thru the list given
     * its position (index) param.
     * @param position - The current index in the list to display
     * @param reusableView - A previous View object that can be re-used for the next indexed view.
     *                      This is Android's attempt to reuse a View object of any row that 
     *                      is out of display once user scrolls thru the list, hence to 
     *                      improve performance.
     * @return The View for the row.
     */
    @Override
    public View getView(int position, View reusableView, ViewGroup viewGroupParent) {
        /*
         * The goal is to reuse resources (view, layout holder) as user scrolls up/down
         * the list for efficiency. Initialize it only when necessary.
         */
        View rowView = null;
        CommentListLayoutHolder layoutHolder = null;
        if(reusableView != null) {
            //Reuse:
            rowView = reusableView;
            layoutHolder = (CommentListLayoutHolder) rowView.getTag();
        } else {
            //New:
            rowView = mInflater.inflate(R.layout.template_row_comment, viewGroupParent, false);
            layoutHolder = new CommentListLayoutHolder(rowView);
            rowView.setTag(layoutHolder);
        }

        //Populate values:
        if(mList != null) {
            Comment comment = mList.get(position);
            layoutHolder.populateLayoutWithValues(mContext, comment, mIsUserLoginProfile);
        }
        
        return rowView;            
    }

}
