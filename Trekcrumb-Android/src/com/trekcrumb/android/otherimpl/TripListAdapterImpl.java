package com.trekcrumb.android.otherimpl;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.trekcrumb.android.R;
import com.trekcrumb.android.layoutholder.TripListLayoutHolder;
import com.trekcrumb.common.bean.Trip;

/**
 * Custom implementation of Android ArrayAdapter for list of Trip objects.
 * 
 * The adapter modified getView() to control how each row of the list will look (display),
 * their values and their behavior (clickable or not, etc.).
 *
 */
public class TripListAdapterImpl extends ArrayAdapter<Trip> {
    
    private final Context mContext;
    private final List<Trip> mList;
    private boolean mIsShowTripSummary;
    private boolean mIsShowTripStatus;
    
    private final LayoutInflater mInflater;

    public TripListAdapterImpl(
            Context context, 
            List<Trip> theList, 
            boolean isShowTripSummary,
            boolean isShowTripStatus) {
        super(context, R.layout.template_row_trip, theList);
        mContext = context;
        mList = theList;
        mIsShowTripSummary = isShowTripSummary;
        mIsShowTripStatus = isShowTripStatus;
        
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    /**
     * Returned the intended View containing data values, replacing the default toString().
     * 
     * The system calls this method repeatedly to display the value when iterating thru the list given
     * its position (index) param.
     * @param position - The current index in the list to display
     * @param reusableView - A previous View object that can be re-used for the next indexed view.
     *                      This is Android's attempt to reuse a View object of any row that 
     *                      is out of display once user scrolls thru the list, hence to 
     *                      improve performance.
     * @return The View for the row.
     */
    @Override
    public View getView(int position, View reusableView, ViewGroup viewGroupParent) {
        /*
         * The goal is to reuse resources (view, layout holder) as user scrolls up/down
         * the list for efficiency. Initialize it only when necessary.
         */
        View rowView = null;
        TripListLayoutHolder layoutHolder = null;
        if(reusableView != null) {
            //Reuse:
            rowView = reusableView;
            layoutHolder = (TripListLayoutHolder) rowView.getTag();
        } else {
            //New:
            rowView = mInflater.inflate(R.layout.template_row_trip, viewGroupParent, false);
            layoutHolder = new TripListLayoutHolder(rowView);
            rowView.setTag(layoutHolder);
        }

        //Populate values:
        if(mList != null) {
            Trip trip = mList.get(position);
            layoutHolder.populateLayoutWithValues(mContext, trip, mIsShowTripSummary, mIsShowTripStatus);
        }
        
        return rowView;            
    }

}
