package com.trekcrumb.android.otherimpl;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;

/**
 * Implementation of OnTouchListener to enable an ImageView to be moved around the screen.
 *
 */
public class ImageMoveOnTouchListenerImpl implements OnTouchListener {
    private int deltaX;
    private int deltaY;

    /**
     * The callback method that handles the touch event to the given View.
     * @param view - The view object where the touch has been dispatched to it
     * @param motionEvent - The object that contains report of the movement (mouse, pen, finger, trackball).
     *        It contains:
     *        a. Action code': Specifies the state change that occurred such as a pointer going down or up
     *        b. A set of axis values (x,y): Describe the position (x,y) and other movement properties such
     *           as pressure, size and orientation.
     */
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        /*
         * Disable the move IF:
         * - The view is not clickable
         * - Its size scale factor is 1 (not being zoom-in/out)
         */
        if(!view.isClickable()) {
            return false;
        } else if(view.getScaleX() == 1 && view.getScaleY() == 1) {
            return false;
        }
        
        //Else:
        view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        int viewOriginalWidth = view.getMeasuredWidth();
        int viewOriginalHeight = view.getMeasuredHeight();
        float viewScale_X = view.getScaleX();
        float viewScale_Y = view.getScaleY();
        int eventOriginalLocation_X = (int) event.getRawX();
        int eventOriginalLocation_Y = (int) event.getRawY();
        
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                //Records how far the pointer (finger down) has moved:
                deltaX = (int) (eventOriginalLocation_X - view.getTranslationX());
                deltaY = (int) (eventOriginalLocation_Y - view.getTranslationY());
                break;
                
            case MotionEvent.ACTION_MOVE:
                //After ACTION_DOWN has stopped, move smoothly the view by the deltas:
                int moveBy_X = eventOriginalLocation_X - deltaX;
                int moveBy_Y = eventOriginalLocation_Y - deltaY;
                
                /*
                float eventFirstPointerCoordinate_X = event.getX();
                float eventFirstPointerCoordinate_Y = event.getY();
                System.out.println("************ ImageMoveOnTouchListenerImpl.onTouch() - ACTION_MOVE: \n"
                        + "(viewOriginalWidth, viewOriginalHeight) = " + viewOriginalWidth + ", " + viewOriginalHeight + "\n"
                        + "Scale factor (x,y) = " + viewScale_X + ", " + viewScale_Y + "\n"
                        + "(eventOriginalLocation_X, eventOriginalLocation_Y) = " + eventOriginalLocation_X + ", " + eventOriginalLocation_Y + "\n"
                        + "(eventFirstPointerCoordinate_X, eventFirstPointerCoordinate_Y) = " + eventFirstPointerCoordinate_X + ", " + eventFirstPointerCoordinate_Y + "\n"
                        + "(deltaX, deltaY) = " + deltaX + ", " + deltaY + "\n"
                        + "Set to translation (x, y) = " + moveBy_X + ", " + moveBy_Y
                        );
                */
                
                //Prevents moving image completely out of its border (original or after scaled) 
                if(Math.abs(moveBy_X) > viewOriginalWidth * viewScale_X
                        || Math.abs(moveBy_Y) > viewOriginalHeight * viewScale_Y) {
                    break;
                }
                
                /*
                 * TODO (10/2014)
                 * Prevents moving image when its borders (left/right, top/bottom) have reached
                 * the edge border of its layout (screen). Hints: See Gallery app.
                 */

                view.setTranslationX(moveBy_X);
                view.setTranslationY(moveBy_Y);
                break;
                
            default:
                //no action
        }

        //Indicate event was handled:
        return true; 
    }
    
   

}
