package com.trekcrumb.android.otherimpl;

import java.io.Serializable;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.trekcrumb.android.listener.IGeoLocationUpdateListener;

/**
 * App's concrete implementation of Android LocationListener.
 * 
 * For usage, this listener impl. must be registered into system LocationManager during a GPS
 * location search by invoking either LocationManager.requestLocationUpdates() or LocationManager.requestSingleUpdate().  
 * When LocationManager detects a GPS location update, the system invokes this impl. callback methods
 * to notify such changes e.g., onLocationChange() or onStatusChange(). 
 * 
 */
@SuppressWarnings("serial")
public class TrekcrumbLocationListenerImpl 
implements LocationListener, Serializable {
    private static final String LOG_TAG = "TrekcrumbLocationListenerImpl";

    private IGeoLocationUpdateListener mGeoLocUpdateListener;

    public TrekcrumbLocationListenerImpl(final IGeoLocationUpdateListener geoLocUpdateListener) {
        super();
        this.mGeoLocUpdateListener = geoLocUpdateListener;
    }

    /**
     * This method is automatically called when a new location is found by the provider
     * (GPS or network provider), passing the new Location. This method is responsible
     * to do what ever needed with the new Location data.
     */
    @Override
    public void onLocationChanged(Location location) {
        if(mGeoLocUpdateListener != null) {
            mGeoLocUpdateListener.onGeoLocationUpdateCallback(location);
            
        } else {
            StringBuilder errMsgBld = new StringBuilder();
            errMsgBld.append("onLocationChanged() - ");
            errMsgBld.append("This LocationListenerImpl [" + this + "] ");
            errMsgBld.append("has NULL IGeoLocationUpdateListener! Cannot notify the hosting activity/fragment that the location [");
            errMsgBld.append(location);
            errMsgBld.append("] is ready!");
            Log.e(LOG_TAG, errMsgBld.toString());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}
    
    public IGeoLocationUpdateListener getGeoLocationUpdateListener() {
        return mGeoLocUpdateListener;
    }
}
