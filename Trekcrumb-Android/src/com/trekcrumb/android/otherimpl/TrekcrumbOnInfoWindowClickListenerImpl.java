package com.trekcrumb.android.otherimpl;

import android.app.Activity;
import android.location.Location;

import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.Marker;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.common.bean.Trip;

/**
 * Custom Map Marker InfoWindow implementation
 * 1. 6/2013: At this time implementation, this is only for Current Location marker infowindow.
 *
 */
public class TrekcrumbOnInfoWindowClickListenerImpl implements OnInfoWindowClickListener {
    private static volatile Activity mHostActivity;
    private Trip mSelectedTrip;
    private Location mCurrentLocation;
    
    public TrekcrumbOnInfoWindowClickListenerImpl(
            final Activity hostActivity,
            Trip selectedTrip,
            Location currentLocation) {
        mHostActivity = hostActivity;
        mSelectedTrip = selectedTrip;
        mCurrentLocation = currentLocation;
    }
    
    /**
     * The custom InfoWindow click. If the marker is the current location marker, do the business logic.
     */
    @Override
    public void onInfoWindowClick(Marker marker) {
        if(marker.getSnippet() != null
                && marker.getSnippet().contains(AndroidConstants.MAP_MARKER_CURRENT_LOC_KEY)) {
            if(mCurrentLocation == null) {
                //Ignore:
                return;
            }
            ActivityUtil.doPlaceCreateActivity(mHostActivity, mSelectedTrip, mCurrentLocation);
        }
        
        //Else: It is regular Places' infowindow = do nothing.
    }

}
