package com.trekcrumb.android.otherimpl;

import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.Marker;
import com.trekcrumb.android.listener.IMapMarkerClickListener;
import com.trekcrumb.android.utility.AndroidConstants;

/**
 * Custom Map Marker implementation.
 *
 */
public class TrekcrumbOnMarkerClickListenerImpl implements OnMarkerClickListener {
    private static volatile IMapMarkerClickListener mMapMarkerClickListener;
    
    public TrekcrumbOnMarkerClickListenerImpl(final IMapMarkerClickListener mapMarkerClickListener) {
        mMapMarkerClickListener = mapMarkerClickListener;
    }
    
    /**
     * The custom marker clickable
     * 1. IF the incoming marker is the current location marker:
     *    - Return FALSE to indicate behave normally per google map DEFAULT
     *    - Center the map on the marker.
     * 2. ELSE: The marker is place markers
     *    - Invoke another callback: IMapMarkerClickListener.OnMarkerClickCallback()
     *    - Return TRUE to indicate DO NOT center the map on the marker.
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        boolean isNOTCenterTheMap = false;
        if(marker.getSnippet() != null
                && marker.getSnippet().contains(AndroidConstants.MAP_MARKER_CURRENT_LOC_KEY)) {
            return isNOTCenterTheMap;
        } else {
            if(mMapMarkerClickListener != null) {
                mMapMarkerClickListener.onMarkerClickCallback(marker);
            }
            isNOTCenterTheMap = true;
            return isNOTCenterTheMap;
        }
    }

}
