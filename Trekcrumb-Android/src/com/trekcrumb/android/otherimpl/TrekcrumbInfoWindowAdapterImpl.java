package com.trekcrumb.android.otherimpl;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.AndroidConstants;

/**
 * Custom Map Marker InfoWindow implementation for Current Location.
 *
 */
public class TrekcrumbInfoWindowAdapterImpl implements InfoWindowAdapter {
    private View mPlaceInfoWindowLayout;
    private View mCurrentLocInfoWindowLayout;

     
    public TrekcrumbInfoWindowAdapterImpl(final Activity hostingActivity) {
        mPlaceInfoWindowLayout = hostingActivity.getLayoutInflater().inflate(R.layout.template_map_infowindow_place, null);
        mCurrentLocInfoWindowLayout = hostingActivity.getLayoutInflater().inflate(R.layout.template_map_infowindow_currentloc, null);
    }

    /**
     * Returns custom contents (layout) for the info window.
     * @return If the marker is the current location marker, return the Current Location marker's infoWindow.
     *         Else, return the Place marker's infoWindow view.
     */
    @Override
    public View getInfoContents(Marker marker) {
        String markerSnippetKey = marker.getSnippet();
        if(markerSnippetKey != null 
                && markerSnippetKey.contains(AndroidConstants.MAP_MARKER_CURRENT_LOC_KEY)) {
            return mCurrentLocInfoWindowLayout;
        } else {
            TextView titleView = (TextView)mPlaceInfoWindowLayout.findViewById(R.id.infoWindowTitle);
            titleView.setText(marker.getTitle());
            return mPlaceInfoWindowLayout;
        }
    }

    @Override
    public View getInfoWindow(Marker marker) {
        //Return NULL because this impl does not change the window view look, so just use the default 
        //window. The goal is to just change the contents.
        return null;
    }

}
