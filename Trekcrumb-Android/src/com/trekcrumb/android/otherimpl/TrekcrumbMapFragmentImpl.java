package com.trekcrumb.android.otherimpl;

import java.io.Serializable;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.MapFragment;
import com.trekcrumb.android.listener.IMapReadyListener;

/**
 * App's MapFragment implementation.
 * 
 * This impl enables any Activity or Fragment to host and add MapFragment
 * programmatically inside the code, and ensure that GoogleMap instance is
 * available upon MapFragment creation. Otherwise, GoogleMap inside the MapFragment
 * could be still NULL.
 * 
 * NOTE: Fragment within Fragment is supported only after Android 4.1.2 (SDK 17) release.
 *
 */
@SuppressWarnings("serial")
public class TrekcrumbMapFragmentImpl extends MapFragment implements Serializable {
    public static final String TAG_FRAGMENT_NAME = TrekcrumbMapFragmentImpl.class.getName();
    private static final String TAG_LOG = "TrekcrumbMapFragmentImpl";

    private volatile IMapReadyListener mMapReadyListener;

    public TrekcrumbMapFragmentImpl() {
        super();
        //Required by Android to re-create the View when device is flipped around
        //between portrait and landscape views whenever we have custom constructor.
    }
    
    public TrekcrumbMapFragmentImpl(final IMapReadyListener listener) {
        super();
        mMapReadyListener = listener;
    }

    public static TrekcrumbMapFragmentImpl newInstance(final IMapReadyListener listener) {
        return new TrekcrumbMapFragmentImpl(listener);
    }
    
    public void refresh(final IMapReadyListener listener) {
        mMapReadyListener = listener;
    }

    /**
     * Called immediately after the hosting activity is created. By then, this fragment 
     * onCreateView() should have completed, which means the fragment View has
     * been successfully instantiated.
     * 
     * More importantly, by then the GoogleMap instance should have been created (not null) 
     * behind the scene by Android system, and the Map size has been determined for each running device.
     */
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Let the listener know the map is ready:
        if(mMapReadyListener != null) {
            mMapReadyListener.onMapFragmentReadyCallback();
            
        } else {
            StringBuilder errMsgBld = new StringBuilder();
            errMsgBld.append("onActivityCreated() - ");
            errMsgBld.append("This MapFragment [" + this + "] ");
            errMsgBld.append("has NULL IMapReadyListener! Cannot notify the hosting activity/fragment that the GoogleMap instance is ready!");
            Log.e(TAG_LOG, errMsgBld.toString());
        }
    }
    
}
