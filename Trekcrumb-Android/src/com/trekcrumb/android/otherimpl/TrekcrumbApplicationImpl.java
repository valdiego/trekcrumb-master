package com.trekcrumb.android.otherimpl;

import android.app.Application;
import android.view.View;

import com.trekcrumb.common.bean.User;

/**
 * App implementation of Application class to hold/maintain global variables (states) for the app 
 * during user 'session' usage across activities, fragments, and so forth.
 * 
 * NOTE 03/2014: 
 * This approach guarantees the global state/variable will be the most up to date whether user
 * goes from page to page, or goes back to previous pages. In contrast, using Bundle does not guarantee
 * the variables be the last up to date when users goes back.
 * 
 * @author Val Triadi
 *
 */
public class TrekcrumbApplicationImpl extends Application {
    private User userLogin;
    private View userImageMenuIcon;
    private boolean isUserAuthenticated;
    private boolean isUserTripSynced;
    
    /*
     * Indicates if app initialization success or fail. We should never reset this value
     * outside app initialization operations. 
     */
    private boolean isAppInitSuccess;

    public void clearUserSession() {
        userLogin = null;
        userImageMenuIcon = null;
        isUserAuthenticated = false;
        isUserTripSynced = false;
    }
    
    public boolean isAppInitSuccess() {
        return isAppInitSuccess;
    }

    public void setAppInitSuccess(boolean isAppInitSuccess) {
        this.isAppInitSuccess = isAppInitSuccess;
    }

    public User getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(User userLogin) {
        this.userLogin = userLogin;
    }

    public boolean isUserAuthenticated() {
        return isUserAuthenticated;
    }

    public void setUserAuthenticated(boolean isUserAuthenticated) {
        this.isUserAuthenticated = isUserAuthenticated;
    }

    public boolean isUserTripSynced() {
        return isUserTripSynced;
    }

    public void setUserTripSynced(boolean isUserTripSynced) {
        this.isUserTripSynced = isUserTripSynced;
    }

    public View getUserImageMenuIcon() {
        return userImageMenuIcon;
    }

    public void setUserImageMenuIcon(View userImageMenuIcon) {
        this.userImageMenuIcon = userImageMenuIcon;
    }


}
