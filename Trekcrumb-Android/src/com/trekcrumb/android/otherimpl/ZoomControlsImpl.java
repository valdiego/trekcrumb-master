package com.trekcrumb.android.otherimpl;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ZoomButton;

import com.trekcrumb.android.R;
import com.trekcrumb.android.iconfont.IconFontDrawableImpl;

/**
 * A sibling copy of Android's android.widget.ZoomControls with custom needs:
 * 1. Custom layout (drawable, orientation, etc.)
 * 2. Custom constant values for maximum zoom-in and minimum zoom-out
 * 3. The logic on how the zoom in/out work on the image
 * 
 */
public class ZoomControlsImpl extends LinearLayout {
    final static float MIN_X = 1;
    final static float MIN_Y = 1;
    final static float MAX_X = 10;
    final static float MAX_Y = 10;

    private final ZoomButton mZoomIn;
    private final ZoomButton mZoomOut;
    private ImageView mImageView;
        
    public ZoomControlsImpl(Context context) {
        this(context, null);
    }

    public ZoomControlsImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
        
        //Custom code below:
        setFocusable(false);
        setOrientation(VERTICAL);
        
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.widget_button_zoomcontrols, 
                         this, // we are the parent
                         true);
        
        mZoomIn = (ZoomButton) findViewById(R.id.zoomInID);
        mZoomOut = (ZoomButton) findViewById(R.id.zoomOutID);
        
        initIcons(context);
    }
    
    /**
     * Sets how fast the zoom events triggered when the user holds down the zoom in/out buttons.
     */
    public void setZoomSpeed(long speed) {
        mZoomIn.setZoomSpeed(speed);
        mZoomOut.setZoomSpeed(speed);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        /* Consume all touch events so they don't get dispatched to the view
         * beneath this view.
         */
        return true;
    }
    
    @Override
    public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(event);
        event.setClassName(ZoomControlsImpl.class.getName());
    }

    @Override
    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo info) {
        super.onInitializeAccessibilityNodeInfo(info);
        info.setClassName(ZoomControlsImpl.class.getName());
    }
    
    /**
     * Sets the TARGET IMAGE where the zoom control will manipulate during the zoom in/out operations.
     * (Note: This is custom code)
     */
    public void setImage(final ImageView image) {
        mImageView = image;
        if(mImageView == null) {
            setIsZoomEnabled(false);
            return;
        }
        
        //Set listeners:
        mZoomIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = mImageView.getScaleX();
                float y = mImageView.getScaleY();
                
                if(x + 1 <= MAX_X || y + 1 <= MAX_Y ) {
                    /*
                     * - Set its scale up a notch at each zoom-in
                     * - Expand to fill the screen
                     */
                    mImageView.setScaleX((float) (x+1));
                    mImageView.setScaleY((float) (y+1));
                    mImageView.setAdjustViewBounds(false);
                }
            }
        });
        
        mZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float x = mImageView.getScaleX();
                float y = mImageView.getScaleY();
                
                if(x - 1 > MIN_X || y -1 > MIN_Y) {
                    /*
                     * - Set its scale down a notch at each zoom-in
                     * - Reset position to its original (in case after move events)
                     */
                    mImageView.setScaleX((float) (x-1));
                    mImageView.setScaleY((float) (y-1));
                    mImageView.setTranslationX(0);
                    mImageView.setTranslationY(0);
                    
                } else if(x - 1 == MIN_X || y -1 == MIN_Y) {
                    /*
                     * - Set to its original scale and ratio
                     * - Reset position to its original (in case after move events)
                     */
                    mImageView.setScaleX(1);
                    mImageView.setScaleY(1);
                    mImageView.setAdjustViewBounds(true);
                    
                    mImageView.setTranslationX(0);
                    mImageView.setTranslationY(0);
                }
            }
        });
    }
    
    /**
     * Sets to disable the zoom in/out widgets.
     * (Note: This is custom code)
     */
    public void setIsZoomEnabled(boolean isEnabled) {
        mZoomIn.setEnabled(isEnabled);
        mZoomOut.setEnabled(isEnabled);
    }
    
    /*
     * ZoomButton is NOT a Button, but a child of ImageView. Hence, we must set image source as drawable.
     */
    private void initIcons(Context context) {
        Drawable iconZoomInIconDrawable = new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_zoomin);
        mZoomIn.setImageDrawable(iconZoomInIconDrawable);

        Drawable iconZoomOutIconDrawable = new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_zoomout);
        mZoomOut.setImageDrawable(iconZoomOutIconDrawable);
    }
}