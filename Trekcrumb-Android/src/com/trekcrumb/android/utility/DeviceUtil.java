package com.trekcrumb.android.utility;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.inputmethod.InputMethodManager;

import com.trekcrumb.android.BuildConfig;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Utility to do any work regarding the device.
 */
public class DeviceUtil {
    private static final String LOGGER_TAG = "DeviceUtil";

    private DeviceUtil() {}

    public static final String DEVICE_WIDTH_KEY = "WIDTH";
    public static final String DEVICE_HEIGHT_KEY = "HEIGHT";

    /**
     * Determine the current running device screen size: Width by Height.
     * The values depend on whether the device is on potrait or landscape position.
     * @return Map - Containing (key,value) of WIDTH and HEIGHT.
     */
    @SuppressWarnings("deprecation")
    public static Map<String, Integer> determineScreenSizes(final Activity runningActivity) {
        int screenWidth = 0;
        int screenHeight = 0;
        Display screenDisplay = runningActivity.getWindowManager().getDefaultDisplay();
        try {
            //These methods available only for SDK => 13:
            Point screenDisplaySize = new Point();
            screenDisplay.getSize(screenDisplaySize);
            screenWidth = screenDisplaySize.x;
            screenHeight = screenDisplaySize.y;
        } catch(Exception nsme) {
            //These methods available only for SDK < 13:
            screenWidth = screenDisplay.getWidth();
            screenHeight = screenDisplay.getHeight();
        }
        Map<String, Integer> sizeMap = new HashMap<String, Integer>();
        sizeMap.put(DEVICE_WIDTH_KEY, Integer.valueOf(screenWidth));
        sizeMap.put(DEVICE_HEIGHT_KEY, Integer.valueOf(screenHeight));
        return sizeMap;
    }
    
    /**
     * Finds current device orientation: Potrait or Landscape.
     * Note: Using API method "getResources().getConfiguration().orientation" sometimes cannot be
     * guaranteed when user device orientation is locked, may return same value all the time.
     * 
     * @return Configuration.ORIENTATION_PORTRAIT or Configuration.ORIENTATION_LANDSCAPE
     */
    public static int determineScreenOrientation(final Activity runningActivity) {
        Map<String, Integer> sizeMap = determineScreenSizes(runningActivity);
        if(sizeMap.get(DEVICE_WIDTH_KEY) <= sizeMap.get(DEVICE_HEIGHT_KEY)){
            return Configuration.ORIENTATION_PORTRAIT;
        }
        
        return Configuration.ORIENTATION_LANDSCAPE;
    }
    
    public static boolean isSDCardAccessible() {
        if(!Environment.MEDIA_MOUNTED.equalsIgnoreCase(Environment.getExternalStorageState())
                || AndroidConstants.DEVICE_SDCARD_DIR_MAIN == null) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Reference why it uses the approach:
     * 1) http://android-developers.blogspot.com/2011/03/identifying-app-installations.html
     * 2) http://stackoverflow.com/questions/2322234/how-to-find-serial-number-of-android-device
     * 
     * @return A String containing this device unique identity: Manufacturer, model, product name
     *         and its serial number.
     */
    public static String determineDeviceIdentity() {
        StringBuilder deviceIdentityBuilder = new StringBuilder();
        deviceIdentityBuilder.append("App ");
        if(!ValidationUtil.isEmpty(Build.MANUFACTURER)
                && !Build.MANUFACTURER.trim().equalsIgnoreCase("unknown")
                && !Build.MANUFACTURER.trim().equalsIgnoreCase("sdk")) {
            deviceIdentityBuilder.append(Build.MANUFACTURER.trim().toUpperCase());
            deviceIdentityBuilder.append(" ");
        }
        if(!ValidationUtil.isEmpty(Build.MODEL)
                && !Build.MODEL.trim().equalsIgnoreCase("unknown")
                && !Build.MODEL.trim().equalsIgnoreCase("sdk")) {
            deviceIdentityBuilder.append(Build.MODEL.trim().toUpperCase());
            deviceIdentityBuilder.append(" ");
        }
        if(!ValidationUtil.isEmpty(Build.PRODUCT)
                && !Build.PRODUCT.trim().equalsIgnoreCase("unknown")
                && !Build.PRODUCT.trim().equalsIgnoreCase("sdk")) {
            deviceIdentityBuilder.append(Build.PRODUCT.trim().toUpperCase());
            deviceIdentityBuilder.append(" ");
        }
        if(!ValidationUtil.isEmpty(Build.SERIAL)
                && !Build.SERIAL.trim().equalsIgnoreCase("unknown")
                && !Build.SERIAL.trim().equalsIgnoreCase("sdk")) {
            deviceIdentityBuilder.append(Build.SERIAL.trim().toUpperCase());
        }
        
        //Alternatively:
        if(deviceIdentityBuilder.length() == 0) {
            try {
                Class<?> systemPropClass = Class.forName("android.os.SystemProperties");
                Method get = systemPropClass.getMethod("get", String.class);
                String serial = (String) get.invoke(systemPropClass, "ro.serialno");
                if(!ValidationUtil.isEmpty(serial)
                        && !Build.SERIAL.trim().equalsIgnoreCase("unknown")
                        && !Build.SERIAL.trim().equalsIgnoreCase("sdk")) {
                    deviceIdentityBuilder.append("App " + serial.toUpperCase());
                }
            } catch (Exception ignored) {
                //none
            }
        }
        
        //Finally, if all fail:
        if(deviceIdentityBuilder.length() == 0) {
            deviceIdentityBuilder.append("App Device Identity Unknown");
            deviceIdentityBuilder.append(" ");
            deviceIdentityBuilder.append(System.currentTimeMillis());
        }
        
        return deviceIdentityBuilder.toString();
    }
    
    /**
     * Verifies if the device has network connectivity.
     * @return TRUE if there is network (internet) connection. FALSE otherwise.
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr = 
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connMgr == null
                || connMgr.getActiveNetworkInfo() == null
                || !connMgr.getActiveNetworkInfo().isConnected()) {
            Log.e(LOGGER_TAG, "isNetworkAvailable() - There is NO active network connection on this device!");
            return false;
        }
        
        return true;
    }
    
    /**
     * Evaluates device GPS enabled or disabled
     * @return TRUE if it is. FALSE otherwise.
     */
    public static boolean isGPSEnabled(Context context) {
        final LocationManager locationMgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if(locationMgr.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        }

        //else:
        Log.e(LOGGER_TAG, "isGPSEnabled() - GPS for this device is NOT enabled!");
        return false;
    }

    public static void hideVirtualKeyboard(Activity hostActivity) {
        if(hostActivity != null
                && hostActivity.getCurrentFocus() != null
                && hostActivity.getCurrentFocus().getWindowToken() != null) {
            IBinder currentWindowToken = hostActivity.getCurrentFocus().getWindowToken();
            InputMethodManager inputManager = (InputMethodManager) hostActivity.getSystemService(Context.INPUT_METHOD_SERVICE); 
            if(inputManager != null) {
                inputManager.hideSoftInputFromWindow(currentWindowToken, InputMethodManager.HIDE_NOT_ALWAYS);
            }
            
        }
    }
    
    /**
     * Android Release Req. PS-P2: 
     * Enables 'StrictMode' during development only i.e., the package is in 'DEBUG' mode, and  
     * notifies all violations including reading/writing to disc and opening network in MAIN THREAD, 
     * as well as any memory and DB leaks. Any violation must be fixed.
     * 
     * Note:
     * 1. Our app does READ directly in main thread to access SQLite DB (UserLogin data) and SD card
     *    (images), and does NOT use async threads thus causing violation. We won't modify this design.
     *    Hence, give StrictMode permission: 'permitDiskReads'.
     * 2. BuildConfig.DEBUG: BuildConfig.java is generated automatically by Android build tools 
     *    (eclipse/ant), and is placed into the gen folder. During development, default is 'true'.
     *    On production (release, signed APK), the value should be 'false'. No need to make any changes.
     * 3. REF:
     *    => https://developer.android.com/reference/android/os/StrictMode.html
     *    => https://android-developers.googleblog.com/2010/12/new-gingerbread-api-strictmode.html 
     *    => https://code.tutsplus.com/tutorials/android-best-practices-strictmode--mobile-7581
     */
    public static void enableStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .permitDiskReads()
                    .penaltyLog() 
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .build());
        }
    }
    
}
