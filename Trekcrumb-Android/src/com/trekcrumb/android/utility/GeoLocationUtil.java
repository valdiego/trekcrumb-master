package com.trekcrumb.android.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.trekcrumb.android.R;
import com.trekcrumb.android.fragment.GPSLocationFragment;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.utility.ValidationUtil;

public class GeoLocationUtil {
    private static final String TAG_LOGGER = "GeoLocationUtil";
    
    private GeoLocationUtil() {}
    
    public static void activateGPSLocation(Activity hostActivity) {
        GPSLocationFragment gpsFragment = new GPSLocationFragment();
        FragmentTransaction fragmentTransaction = hostActivity.getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.gpsLocationSectionID, gpsFragment, GPSLocationFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }
    
    public static void deactivateGPSLocation(Activity hostActivity) {
        GPSLocationFragment gpsFragment = 
                (GPSLocationFragment)hostActivity.getFragmentManager().findFragmentByTag(GPSLocationFragment.TAG_FRAGMENT_NAME);
        if(gpsFragment != null) {
            gpsFragment.cancel();
            
            /*
             * Removes the GPS fragment from the host activity.
             * WARNING: 
             * If activity became stopped/paused due to user switch to other app/sleep - 
             * the 'commit()' operation would throw exception that would be prompted to user view: 
             * "java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState"
             * Hence, catch it and do nothing else.
             */
            try {
                FragmentTransaction fragmentTransaction = hostActivity.getFragmentManager().beginTransaction();
                fragmentTransaction.detach(gpsFragment);
                fragmentTransaction.remove(gpsFragment);
                fragmentTransaction.commit();
            } catch(IllegalStateException ise) {
                Log.e(TAG_LOGGER, "deactivateGPSLocation() - Caught invalid state exception: " + ise.getMessage(), ise);
            }
            
            gpsFragment = null;
        }
    }
    
    /**
     * Gets user most current location.
     * 
     * Behind the scene, Android system will try to get the best provider available given a 
     * specific critaria by this app, and look for the updated Location. When a new location
     * is found, the system will callback LocationListener.onLocationChanged().
     * 
     * @return Location - Returns the last known (detected) Location. This is NOT the most current
     *                    Location, but may be used as the last location available. If no app ever 
     *                    triggers a geo location device, most likely the returned Location will be NULL.
     */
    public static void findCurrentLocation(
            LocationManager locationManager,
            LocationListener locationListener) {
        Log.d(TAG_LOGGER, "findCurrentLocation() - Starting ...");
        if(locationManager == null || locationListener == null) {
            throw new IllegalArgumentException("LocationManager or LocationListener param cannot be NULL!");
        }
        
        Criteria providerCriteria = new Criteria();
        providerCriteria.setAccuracy(Criteria.ACCURACY_FINE);
        providerCriteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH); //(lat,lon) within 100m
        providerCriteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);   //elevation within 100m
        providerCriteria.setBearingAccuracy(Criteria.ACCURACY_LOW);
        providerCriteria.setSpeedAccuracy(Criteria.ACCURACY_LOW);
        String bestEnabledProvider = locationManager.getBestProvider(providerCriteria, true);
        
        Log.d(TAG_LOGGER, "findCurrentLocation() - Sending GPS request...");
        locationManager.requestSingleUpdate(bestEnabledProvider, locationListener, null);

        /*  UNUSED CODE
        locationManager.requestLocationUpdates(
                bestEnabledProvider, 
                MIN_TIME_INTERVAL_BETWEEN_UPDATES, 
                MIN_DISTANCE_INTERVAL_BETWEEN_UPDATES, 
                locationListener);
        */
        
        //Once there is an update, LocationManager will executes callback: LocationListener.onLocationChanged()
    }
    
    /**
     * Constructs a list of LatLng bean based on a Trip's list of Places.
     */
    public static List<LatLng> constructPlacesLatLngList(Trip trip) {
        List<Place> listOfPlaces = trip.getListOfPlaces();
        List<LatLng> placesLatLngList = new ArrayList<LatLng>();
        for(Place place : listOfPlaces) {
            LatLng placeLatLan = new LatLng(place.getLatitude(), place.getLongitude());
            placesLatLngList.add(placeLatLan);
        }
        return placesLatLngList;
    }

    /**
    * Decodes a Geo Location into a human readable City and Country.
    * @param context
    * @param location - Android Location bean containing geoloc data.
    * @return A String of human readable city and country, separated by a comma. This method will not
    *         return a complete address for privacy. When there is an exception during reverse
    *         geo-coding, it returns NULL.
    */
    public static String getCityAndCountry(
          final Context context, 
          final Location location) {
        Log.i(TAG_LOGGER, "getCityAndCountry() - Starting for location: " + location);

        String cityAndCountry = null;
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (Exception e) {
            String errorMsg = "getCityAndCountry() - Failed to get address for ["
                    + location.getLatitude() + "," + location.getLongitude() 
                    + "] with error: " + e.getMessage();
            Log.e(TAG_LOGGER, errorMsg);
            e.printStackTrace();
            return null;
        }

        if (addresses != null && addresses.size() > 0) {
            //Use the first address bean only:
            Address address = addresses.get(0);
            StringBuilder cityAndCountryBld = new StringBuilder();
            if(!ValidationUtil.isEmpty(address.getSubLocality())) {
               cityAndCountryBld.append(address.getSubLocality()).append(", ");
            }
            if(!ValidationUtil.isEmpty(address.getLocality())) {
               cityAndCountryBld.append(address.getLocality()).append(", ");
            }
            if(!ValidationUtil.isEmpty(address.getCountryName())) {
               cityAndCountryBld.append(address.getCountryName());
            }
            cityAndCountry = cityAndCountryBld.toString();
        }
        Log.i(TAG_LOGGER, "getCityAndCountry()() - Address after reverse geo loc: " + cityAndCountry);
        return cityAndCountry;
     }


}
