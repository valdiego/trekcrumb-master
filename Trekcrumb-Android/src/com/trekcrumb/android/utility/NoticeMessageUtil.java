package com.trekcrumb.android.utility;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.trekcrumb.android.R;

public class NoticeMessageUtil {
    private NoticeMessageUtil() {}
    
    /**
     * Displays an info message on a hosting View. 
     * To use this method, the hosting view must contain the merged error and info layout.
     * @param hostView - The host view where the info layout (segment) will be displayed.
     * @param inflater - LayoutInflater
     * @param infoMsg - The info message to display.
     */
    public static void displayInfoMessage(
            final View hostView,
            final LayoutInflater inflater,
            String infoMsg) {
        ViewGroup errorAndInfoLayout = (ViewGroup)hostView.findViewById(R.id.errorAndInfoLayoutID);
        if(errorAndInfoLayout != null) {
            errorAndInfoLayout.setVisibility(View.VISIBLE);
            
            ViewGroup infoLayout = (ViewGroup)hostView.findViewById(R.id.infoLayoutID);
            infoLayout.setVisibility(View.VISIBLE);
            TextView infoTextView = (TextView)infoLayout.findViewById(R.id.infoTextID);
            infoTextView.setText(infoMsg);
            
            //Scroll to top of page:
            ScrollView mainScrollLayout = (ScrollView) hostView.findViewById(R.id.mainScrollLayoutID);
            if(mainScrollLayout != null) {
                mainScrollLayout.fullScroll(ScrollView.FOCUS_UP);
                mainScrollLayout.smoothScrollTo(0,0);
            }
        } //end-if page has error-info layout
    }
    
    /**
     * Removes info message layout from the given hosting View.
     */
    public static void hideInfoMessages(View hostView) {
        ViewGroup errorAndInfoLayout = (ViewGroup)hostView.findViewById(R.id.errorAndInfoLayoutID);
        if(errorAndInfoLayout != null) {
            LinearLayout infoLayout = (LinearLayout) hostView.findViewById(R.id.infoLayoutID);
            infoLayout.setVisibility(View.INVISIBLE);
            infoLayout.setVisibility(LinearLayout.GONE);
        }
    }
    
    /**
     * Displays a list of error messages on a hosting View. 
     * To use this method, the hosting view must contain the merged error and info layout.
     * @param hostView - The host view where the error layout (segment) will be displayed.
     * @param inflater - LayoutInflater
     * @param errorMsgList - The list of error messages.
     */
    public static void displayErrorMessage(
            final View hostView,
            final LayoutInflater inflater,
            List<String> errorMsgList) {
        ViewGroup errorAndInfoLayout = (ViewGroup)hostView.findViewById(R.id.errorAndInfoLayoutID);
        if(errorAndInfoLayout != null) {
            errorAndInfoLayout.setVisibility(View.VISIBLE);

            ViewGroup errorMainLayout = (ViewGroup)hostView.findViewById(R.id.errorLayoutID);
            errorMainLayout.setVisibility(View.VISIBLE);
            
            ViewGroup errorTextLayout = (ViewGroup)errorMainLayout.findViewById(R.id.errorTextLayoutID);
            
            //Clear any previous message:
            errorTextLayout.removeAllViews();
            
            //Add new error messages:
            for(String errorMsg : errorMsgList) {
                TextView errorView = 
                        (TextView) inflater.inflate(
                                R.layout.template_textview_error, null);
                errorView.setText(errorMsg);
                errorTextLayout.addView(errorView);
            }
            
            //Scroll to top of page:
            ScrollView mainScrollLayout = (ScrollView) hostView.findViewById(R.id.mainScrollLayoutID);
            if(mainScrollLayout != null) {
                mainScrollLayout.fullScroll(ScrollView.FOCUS_UP);
                mainScrollLayout.smoothScrollTo(0,0);
            }
        } //end-if page has error-info layout
    }
    
    /**
     * Removes error message layout from the given hosting View.
     */
    public static void hideErrorMessages(View hostView) {
        ViewGroup errorAndInfoLayout = (ViewGroup)hostView.findViewById(R.id.errorAndInfoLayoutID);
        if(errorAndInfoLayout != null) {
            LinearLayout errorMainLayout = (LinearLayout) hostView.findViewById(R.id.errorLayoutID);
            errorMainLayout.setVisibility(View.INVISIBLE);
            errorMainLayout.setVisibility(LinearLayout.GONE);
        }
    }
    
    /**
     * Removes info and error message layout from the given hosting View.
     */
    public static void hideErrorAndInfoMessages(View hostView) {
        ViewGroup errorAndInfoLayout = (ViewGroup)hostView.findViewById(R.id.errorAndInfoLayoutID);
        if(errorAndInfoLayout != null) {
            LinearLayout infoLayout = (LinearLayout) hostView.findViewById(R.id.infoLayoutID);
            infoLayout.setVisibility(View.INVISIBLE);
            infoLayout.setVisibility(LinearLayout.GONE);

            LinearLayout errorMainLayout = (LinearLayout) hostView.findViewById(R.id.errorLayoutID);
            errorMainLayout.setVisibility(View.INVISIBLE);
            errorMainLayout.setVisibility(LinearLayout.GONE);

            errorAndInfoLayout.setVisibility(View.INVISIBLE);
            errorAndInfoLayout.setVisibility(LinearLayout.GONE);
        }
    }
    
    /**
     * Displays a warning message. 
     * To use this method, the hosting view must contain the merged warning layout.
     */
    public static void displayWarningMessage(
            final View hostView,
            final LayoutInflater inflater,
            String infoMsg) {
        LinearLayout warningLayout = (LinearLayout)hostView.findViewById(R.id.warningLayoutID);
        if(warningLayout != null) {
            warningLayout.setVisibility(View.VISIBLE);
            TextView warningTextView = (TextView)warningLayout.findViewById(R.id.warningTextID);
            warningTextView.setText(infoMsg);                    
        }
    }

    /**
     * Hides a warning message. 
     */
    public static void hideWarningMessage(
            final View hostView,
            final LayoutInflater inflater) {
        LinearLayout warningLayout = (LinearLayout)hostView.findViewById(R.id.warningLayoutID);
        if(warningLayout != null) {
            TextView warningTextView = (TextView)warningLayout.findViewById(R.id.warningTextID);
            warningTextView.setText("");                    
            warningLayout.setVisibility(View.INVISIBLE);
            warningLayout.setVisibility(LinearLayout.GONE);
        }
    }
    
    /**
     * Displays a specific message as a main content of the given host view, replacing all other views/structures 
     * inside this host view.
     *  
     * @param hostActivity - The activity where this host view belongs to.
     * @param hostView
     *  The main view where this message will be displayed. 
     *  1. Its root layout must be either a LinearLayout, RelativeLayout or FrameLayout. 
     *  2. It root layout must have ID = 'bodyMainSectionID'
     *  
     * @param message - The message to be displayed.
     */
    public static void displayMessageAsContent(
            Activity hostActivity, 
            ViewGroup hostView,
            String message,
            boolean isTextColorWhite) {
        ViewGroup bodyContentLayout = (ViewGroup) hostView.findViewById(R.id.bodyMainSectionID);
        if(bodyContentLayout != null) {
            bodyContentLayout.removeAllViews();
            
            TextView msgView;
            if(isTextColorWhite) {
                msgView = (TextView) hostActivity.getLayoutInflater().inflate(R.layout.template_textview_info_white, null);
            } else {
                msgView = (TextView) hostActivity.getLayoutInflater().inflate(R.layout.template_textview_info, null);
            }
            msgView.setText(message);
            
            if(bodyContentLayout instanceof LinearLayout) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 10, 10, 10);
                msgView.setLayoutParams(params);
                
            } else if(bodyContentLayout instanceof RelativeLayout) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 10, 10, 10);
                msgView.setLayoutParams(params);
                
            } else if(bodyContentLayout instanceof FrameLayout) {
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 10, 10, 10);
                msgView.setLayoutParams(params);
            }
            
            bodyContentLayout.addView(msgView);
        }
    }

    public static void showToastMessageShort(final Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    public static void showToastMessageLong(final Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
    
    public static void showToastMessageCustom(
            final Context context, 
            String message,
            int durationInSec,
            float horizontalMargin,
            float verticalMargin) {
        Toast toast = new Toast(context);
        toast.setText(message);
        toast.setDuration(durationInSec);
        toast.setGravity(Gravity.TOP, 0, 0);
        if(horizontalMargin >= 0 && verticalMargin >= 0) {
            toast.setMargin(horizontalMargin, verticalMargin);
        }
        
        //TODO: Set a custom view if necessary:
        //toast.setView(view);
        
        toast.show();
    }
    
    public static ProgressDialog showProgressDialog(
            final Context context,
            final LayoutInflater inflater,
            String title,
            String message) {
        ViewGroup dialogLayout = 
                (ViewGroup)inflater.inflate(R.layout.widget_progress_bar_popup, null);
        TextView titleView = (TextView)dialogLayout.findViewById(R.id.progressBarTitleID);
        titleView.setText(title);
        TextView messageView = (TextView)dialogLayout.findViewById(R.id.progressBarMessageID);
        messageView.setText(message);
        
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setContentView(dialogLayout);

        return progressDialog;
    }

    
}
