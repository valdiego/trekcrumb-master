package com.trekcrumb.android.utility;

import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.common.bean.User;


public class SessionUtil {
    
    public static void setAppInitStatus(
            TrekcrumbApplicationImpl appContext,
            boolean isAppInitSuccess) {
        appContext.setAppInitSuccess(isAppInitSuccess);
    }

    public static void setUserLoginDataInSession(
            TrekcrumbApplicationImpl appContext,
            User userLogin,
            boolean isUserAuthenticated,
            boolean isUserTripSynced) {
        appContext.setUserLogin(userLogin);
        appContext.setUserAuthenticated(isUserAuthenticated);
        appContext.setUserTripSynced(isUserTripSynced);
    }

   
}
