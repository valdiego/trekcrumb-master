package com.trekcrumb.android.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import android.util.Log;

/**
 * Utility to deal wth file operation.
 * NOTE:
 * 1. 5/2014 - Unfortunately, we cannot share/use the Common's FileUtil (com.trekcrumb.common.utility.FileUtil)
 *    because it uses new Java ImageIO library, which Android framework did not support and would throw
 *    exception upon app install: 'Could not find method javax.imageio'.
 * 2. Some of the methods here are duplicate of those in Common's FileUtil class.
 *    
 * @author Val Triadi
 *
 */
public class FileUtil {
    
    /**
     * @overwrite parseSqlFile(BufferedReader)
     * @param sqlFilepath - The fullpath and filename of the SQL file.
     */
    public static String[] parseSqlFile(String sqlFilepath) throws IOException {
        return parseSqlFile(new BufferedReader(new FileReader(sqlFilepath)));
    }

    /**
     * @overwrite parseSqlFile(BufferedReader)
     * @param sqlInputStream - InputStream of the SQL file.
     */
    public static String[] parseSqlFile(InputStream sqlInputStream) throws IOException {
        return parseSqlFile(new BufferedReader(new InputStreamReader(sqlInputStream)));
    }

    /**
     * Parses a file containing sql statements into a String array that contains
     * only the sql statements.
     * 
     * Comments and white spaces in the file are ignored. The file must contain well formatted
     * SQL queries and comments, if any. All sql statements must end with a semi-colon ";" to be
     * parsed correctly.
     * 
     * @param sqlFile - BufferedReader of the SQL file that contains sql statements.
     * @return String - An array of single sql statements.
     */
    public static String[] parseSqlFile(BufferedReader sqlFile) throws IOException {
        String line;
        StringBuilder sql = new StringBuilder();
        String multiLineComment = null;

        while ((line = sqlFile.readLine()) != null) {
            line = line.trim();

            //Check for the start of multiline comment
            if (multiLineComment == null) {
                if (line.startsWith("/*")) { //Check for first-type of  multiline comment
                    if (!line.endsWith("*/")) {
                        multiLineComment = "/*";
                    }

                } else if (line.startsWith("{")) { //Check for second-type of multiline comment
                    if (!line.endsWith("}")) {
                        multiLineComment = "{";
                }
                    
                //Append line if line is not empty or a single line comment
                } else if (!line.startsWith("--") && !line.equals("")) {
                    sql.append(line);
                }
                
            //Check for the matching end of multiline comment    
            } else if (multiLineComment.equals("/*")) {
                if (line.endsWith("*/")) {
                    multiLineComment = null;
                }
            } else if (multiLineComment.equals("{")) {
                if (line.endsWith("}")) {
                    multiLineComment = null;
                }
            }
        }
        sqlFile.close();
        return sql.toString().split(";");
    }
    
    /**
     * Copy all content from a Source File into a Target File, overwriting (deleting) anything that the 
     * Target File may have.
     * 
     * @param sourceFile - String specifying the full absolute path of the file to copy from.
     * @param targetFile - String specifying the full absolute path of the file to copy to.
     * @IOException - When there is any exception during processing.
     */
    public static void copyFile(String sourceFile, String targetFile) throws IOException {
        copyFile(new FileInputStream(sourceFile), new FileOutputStream(targetFile));
    }
    
    /**
     * Copy all content from a Source File into a Target File, overwriting anything that the 
     * Target File may have.
     * 
     * @param sourceFile - InputStream for the file to copy from.
     * @param targetFile - OutputStream for the file to copy to.
     * @IOException - When there is any exception during processing.
     */
    public static void copyFile(InputStream sourceFile, OutputStream targetFile) throws IOException {
        byte[] buffer = new byte[1024]; //Copy 1K bytes at a time
        int length;
        try {
            while ((length = sourceFile.read(buffer)) > 0) {
                targetFile.write(buffer, 0, length);
            }
            targetFile.flush();
        
        } finally {
            try {
                sourceFile.close();
            } catch(Exception e) {} //Cant do anything
            try {
                targetFile.close();
            } catch(Exception e) {} //Cant do anything
        }        
    }
    
    /**
     * Copy all content from a Source File into a Target File, overwriting anything that the 
     * Target File may have, using FileChannel API.
     * 
     * @param sourceFile - FileInputStream for the file to copy from.
     * @param targetFile - FileOutputStream for the file to copy to.
     * @IOException - When there is any exception during processing.
     */
    public static void copyFile(FileInputStream sourceFile, FileOutputStream targetFile) throws IOException {
        FileChannel sourceFChannel = null;
        FileChannel targetFChannel = null;
        try {
            sourceFChannel = sourceFile.getChannel();
            targetFChannel = targetFile.getChannel();
            targetFChannel.transferFrom(sourceFChannel, 0, sourceFChannel.size());
        
        } finally {
            if(sourceFChannel != null) {
                try {
                    sourceFChannel.close();
                } catch(Exception e) {} //Cant do anything
            }
            if(targetFChannel != null) {
                try {
                    targetFChannel.close();
                } catch(Exception e) {} //Cant do anything
            }
            if(sourceFile != null) {
                try {
                    sourceFile.close();
                } catch(Exception e) {} //Cant do anything
            }
            if(targetFile != null) {
                try {
                    targetFile.close();
                } catch(Exception e) {} //Cant do anything
            }
        }
    }
    
    /**
     * @See Duplicate: com.trekcrumb.common.utility.FileUtil
     * @param filepath - The absolute file directory (complete, including root dir and filename) of the file to delete.
     */
    public static void deleteFile(String filepath) {
        File fileToDelete = new File(filepath);
        if(!fileToDelete.exists()) {
            Log.d("FileUtil", "deleteFile() - File to delete [" + filepath + "] does NOT exist. Quitting.");
            return;
        }
        
        boolean result = fileToDelete.delete();
        Log.d("FileUtil", "deleteFile() - Delete file [" + filepath + "] success: " + result);
        if(!result) {
            //Fail, try the hard way:
            String deleteCmd = "rm -r " + filepath;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException e) { 
                //Do nothing else
                e.printStackTrace();
            }
            Log.d("FileUtil", "deleteFile() - Re-try delete file [" + filepath + "] success: " + fileToDelete.exists());
        }
    }    

    
}
