package com.trekcrumb.android.utility;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trekcrumb.android.R;
import com.trekcrumb.android.listener.IMapMarkerClickListener;
import com.trekcrumb.android.otherimpl.TrekcrumbInfoWindowAdapterImpl;
import com.trekcrumb.android.otherimpl.TrekcrumbOnInfoWindowClickListenerImpl;
import com.trekcrumb.android.otherimpl.TrekcrumbOnMarkerClickListenerImpl;
import com.trekcrumb.common.bean.Trip;

public class MapUtil {
    //Singleton variables:
    private static BitmapDescriptor mMarkerIconCurrentLoc;
    private static Resources mContextResources; 
    
    private MapUtil() {}
    
    public static void configureMap(
            final Activity hostActivity, 
            GoogleMap map,
            IMapMarkerClickListener mapMarkerClickListener,
            Trip selectedTrip,
            Location currentLocation) {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        UiSettings uiSetting = map.getUiSettings();
        if(uiSetting != null) {
            uiSetting.setAllGesturesEnabled(false);
            uiSetting.setZoomControlsEnabled(true);
            uiSetting.setZoomGesturesEnabled(true);
            uiSetting.setScrollGesturesEnabled(true);
        }
        
        map.setOnMarkerClickListener(new TrekcrumbOnMarkerClickListenerImpl(mapMarkerClickListener));
        map.setInfoWindowAdapter(new TrekcrumbInfoWindowAdapterImpl(hostActivity));

        if(currentLocation != null) {
            map.setOnInfoWindowClickListener(
                    new TrekcrumbOnInfoWindowClickListenerImpl(
                            hostActivity, selectedTrip, currentLocation));
        }
    }

    /**
     * Paints the given Map with marker of places and the line paths from the given Trip.
     * @param map - Map to be painted.
     * @param listOfPlacesLatLng - List of Places (lat,Lng) of a Trip. 
     *               The first Place in the Trip's list is the first added Place, while the last 
     *               Place in the list is indeed the last added Place.
     * @return List of map's Markers in the order they are created, that is in the same order as
     *         the list of places.
     */
    public static List<Marker> paintMapWithPlaces(
            Context context,
            GoogleMap map,
            Trip selectedTrip,
            List<LatLng> listOfPlacesLatLng) {
        if(mContextResources == null) {
            mContextResources = context.getResources();
        }

        int numOfPlaces = listOfPlacesLatLng.size();
        if(numOfPlaces == 0) {
            return null;
        }
        
        int placeIndex = 0;
        List<Marker> listOfMapMarkers = new ArrayList<Marker>();
        for(LatLng placeLatLan : listOfPlacesLatLng) {
            Marker marker = paintMapWithPlacesMarker(context, map, placeLatLan, placeIndex);
            listOfMapMarkers.add(marker);
            placeIndex++;
        }
        
        paintMapWithPolylines(context, map, listOfPlacesLatLng);
        return listOfMapMarkers;
    }
    
    /**
     * Draw a place marker on the map.
     * @param map - The map on which a marker will be drawn.
     * @param placeLatLan - The point (latitude, longitude) where the marker will be placed
     * @param isStart - Whether this is the start place.
     * @param isEnd - Whether this is the end place.
     */
    private static Marker paintMapWithPlacesMarker(
            Context context,
            GoogleMap map, 
            LatLng placeLatLan, 
            int placeIndex) {
        if(mContextResources == null) {
            mContextResources = context.getResources();
        }
        
        MarkerOptions placeMarkerOptions = new MarkerOptions();
        placeMarkerOptions.position(placeLatLan);
        placeMarkerOptions.draggable(false);
        
        /*
         * Snippet and title: 
         * We utilize the marker snippet as a 'custom' placeholder to hold its index value
         * (integer) to be used variedly later to find its associated Place from a list, etc.
         * IF GoogleMap decided to no longer support snippet in future, we then must use title to 
         * hold its index value.
         */
        placeMarkerOptions.snippet(String.valueOf(placeIndex));
        placeMarkerOptions.title(mContextResources.getString(R.string.label_place_number, (placeIndex+1)));
        
        /*
         * Icon:
         * While we keep the placemarker Bitmap object as singleton, but let the BitmapDescriptor 
         * to be new object for each place marker because they should have their own icon objects. 
         * The singleton Bitmap is reusable to produce new BitmapDescriptor. 
         * BitmapDescriptor is what we set as the marker icon.
         */
        placeMarkerOptions.icon(BitmapDescriptorFactory.fromBitmap(SingletonFactory.getMapPlaceMarker(context)));

        return map.addMarker(placeMarkerOptions);
    }    
    
    /*
     * Draws line(s) on the given map between (langitude,longitude) points.
     * See notes on implemented extension methods MapUtil_PolylineExtension.java
     * 
     * @param map - The map on which the lines will be drawn.
     * @param listOfPlacesLatLng - List containing LatLng entities. The lines will be drawn
     *              in the order of these LatLng placed on the List from beginning to the end.
     *              If the list only contains one LatLng, no line will be drawn.
     */
    private static void paintMapWithPolylines(
            Context context, 
            GoogleMap map, 
            List<LatLng> listOfPlacesLatLng) {
        if(mContextResources == null) {
            mContextResources = context.getResources();
        }

        if(listOfPlacesLatLng != null
                && listOfPlacesLatLng.size() > 1) {
            MapUtil_PolylineExtension.constructDashedPolyline_byConstantDashlineNumberGivenDistance(
                    context, map, listOfPlacesLatLng);
        }
    }
    
    /**
     * Adds Current Location's LatLng onto the map.
     * @param map - The map on which a marker will be drawn.
     * @param currentLatLng
     */
    public static Marker paintMapWithCurrentLocation(
            Context context,
            GoogleMap map, 
            LatLng currentLatLng) {
        if(mContextResources == null) {
            mContextResources = context.getResources();
        }

        MarkerOptions currentLocMarkerOptions = new MarkerOptions();
        currentLocMarkerOptions.position(currentLatLng);
        currentLocMarkerOptions.title(mContextResources.getString(R.string.label_place_current));
        currentLocMarkerOptions.snippet(AndroidConstants.MAP_MARKER_CURRENT_LOC_KEY);
        currentLocMarkerOptions.draggable(false);
        currentLocMarkerOptions.anchor(0.5f, 0.5f);
        
        /*
         * Icon:
         * We keep the BitmapDescriptor object as singleton because there should only be ONE
         * current loc marker in a Map, and we can reuse the same object over and over.
         * BitmapDescriptor is what we set as the marker icon.
         */
        if(mMarkerIconCurrentLoc == null) {
            mMarkerIconCurrentLoc = 
                    BitmapDescriptorFactory.fromBitmap(SingletonFactory.getMapCurrentLocMarker(context));
        }
        currentLocMarkerOptions.icon(mMarkerIconCurrentLoc);

        return map.addMarker(currentLocMarkerOptions);
    }
    
    /**
     * Configures the zoom level and the center for the map. 
     * 
     * When CurrentLocation is provided, we simply focus and center the map on its location, and
     * set zoom level to default close-up AndroidConstants.MAP_DEFAULT_ZOOM_CLOSE_UP. Some
     * of existing places may be out of view. 
     * 
     * When no CurrentLocation, we must try to include ALL places within the view boundary, and 
     * set the zoom level accordingly. The calculation is based on all the markers on the map 
     * (that is, the places) such that they are included in the map when a zoom level is determined. 
     * In addition, there are padding to the borders based on the device screen width and height, 
     * and its orientation.
     * 
     * @param map - Map to be painted.
     * @param currentLocation - User current location. When provided, the map will center and zoom
     *        in this point. Other parameters are ignored.
     * @param listOfPlacesLatLng - The list containing all places' points to be included in the 
     *        map view. Ignored if currentLocation parameter is provided.                  
     * @param screenWidth - The width of the screen display in pixels.
     * @param screenHeight - The height of the screen display in pixels.
     * @param deviceOrientation - Configuration.ORIENTATION_PORTRAIT or Configuration.ORIENTATION_LANDSCAPE
     */
    public static void configureMapZoomAndCenter(
            GoogleMap map, 
            Location currentLocation,
            List<LatLng> listOfPlacesLatLng, 
            int screenWidth, 
            int screenHeight,
            int deviceOrientation) {
        if(currentLocation != null) {
            LatLng currentLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    currentLatLng, AndroidConstants.MAP_DEFAULT_ZOOM_CLOSE_UP));
            //That's it!
            return;
        }
        
        //Else:
        int noPadding = 0;
        LatLngBounds.Builder latLngBoundBuilder = new LatLngBounds.Builder();
        for(LatLng latlng : listOfPlacesLatLng) {
            latLngBoundBuilder.include(latlng);
        }
        LatLngBounds boundaries = latLngBoundBuilder.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(
                boundaries, screenWidth, screenHeight, noPadding));
        
        /*
         * After the map camera moves to include all the places, adjust the zoom level.
         * WARNING 06/2015: 
         * On GoogleMap Web API, there is time span for map.fitToBounds() to complete
         * its work, hence we must wait until it finishes before getting its adjusted zoom level.
         * I bet this is similar operation as GoogleMap Android map.moveCamera(). Hence, I suspected 
         * the call below to get adjusted zoom immediately (instead of a callback) could be inaccurate.
         * (DO NOT put thread.sleep because it would delay the entire operation!)
         */
        float zoomLevelAdjusted = map.getCameraPosition().zoom;

        //Adjust zoom level:
        if(20 <= zoomLevelAdjusted && zoomLevelAdjusted <= 21) {
            //Too close, zoom out to default:
            LatLng centerLatLng = listOfPlacesLatLng.get(0);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    centerLatLng, AndroidConstants.MAP_DEFAULT_ZOOM_CLOSE_UP));
        } else {
            if(deviceOrientation == Configuration.ORIENTATION_PORTRAIT) {
                int paddingPixelsForLowerZoomOut = (int) Math.round(0.05 * screenHeight);
                int paddingPixelsForHigherZoomOut = (int) Math.round(0.125 * screenHeight);
                if(zoomLevelAdjusted <= 14) {
                    //Zoom is too close given the markers, give padding to force further zoom out:
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(
                            boundaries, screenWidth, screenHeight, paddingPixelsForHigherZoomOut));
                } else {
                    //Zoom is far enough given the markers, give padding just enough:
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(
                            boundaries, screenWidth, screenHeight, paddingPixelsForLowerZoomOut));
                } 

            } else {
                //Landscape:
                int paddingPixelsForHigherZoomOut = (int) Math.round(0.28 * screenHeight);
                if(zoomLevelAdjusted < 20) {
                    //Zoom everything out:
                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(
                            boundaries, screenWidth, screenHeight, paddingPixelsForHigherZoomOut));
                } 
            }            
        }
    }

}
