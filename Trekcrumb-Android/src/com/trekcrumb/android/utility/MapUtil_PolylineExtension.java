package com.trekcrumb.android.utility;

import java.util.List;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.trekcrumb.android.R;

/**
 * Extension of MapUtil where we keep variety of trial-error methods that we tried to implement
 * 'dashed-polyline', yet not used for various reasons. We keep them as any future references 
 * to troubleshoot the one(s) we used.
 * 
 * Notes (3/2017):
 * 1. GoogleMap V.2 (installed 1/2013) by default draws SOLID lines. 
 *    It does NOT have API to support drawing draw dash/dot polylines. At time of this note, 
 *    there is NO supplement/support API that could extend V.2 to do such. The few options 
 *    were to look for custom impl. OR to upgrade GoogleMap into later release (see note below)
 * 2. GoogleMap V.10.2.1 (2/2017) FINALLY provides API to draw dash/dot polylines using their
 *    new PatternItem class. Any version before this (inclding V.10) does not have it.
 *    HOWEVER, Google had changed the basic GoogleMap framework that we have used such that
 *    some APIs we depended on no longer existed. Example: MapFragment.getMap() has been eliminated.
 *    Moreover, the new version has bunch of other Listeners we must implement. While the 
 *    downloaded tutorial samples seem very straightforward, using this version would require 
 *    us to recode our entire Android Map framework. At this time, we avoided this.
 * 3. Custom code: We found few options to custom code dashed polylines. Note that each of these
 *    impl would multiply number of polylines from 10x to easily 1000x. With the algorithm formula,
 *    it affects performance. See each methods below.
 *            
 * REF:
 * => https://maps-apis.googleblog.com/2017/02/styling-and-custom-data-for-polylines.html
 * => https://developers.google.com/maps/documentation/android-api/polygon-tutorial
 * => http://stackoverflow.com/questions/13721008/how-to-draw-dashed-polyline-with-android-google-map-sdk-v2
 * 
 * @author Val Triadi
 * @since 2/2017
 */
public class MapUtil_PolylineExtension {
    
    /*
     * Constructs dashed-polyline given Zoom value (by EdgarEler). 
     * PRO: Algorithm was very simple and worked perfectly. 
     * CON: 
     * -> All distance will have exact same numbers of dashlines, causing inconsistent line sizes. 
     *    When number of dashlines is small (e.g., 4), it looked nice on close points, but
     *    terrible on long-distance points. Vice-versa, if it is large (e.g., 15), it looked
     *    nice on long-distance points, but crowded on close points.
     * -> The far zoom-out, the less number of dashlines. The closer zoom-in, the more
     *    number of dashlines. 
     * -> Required final zoom value to determine number of dashlines. BUT we painted polylines
     *    before we adjust zoom level in "configureMapZoomAndCenter()", so we always ended up 
     *    with GoogleMap default zoom value = 2; hence number of dashlines is always 4!
     * -> Possibly extending the code to keep recalculating the dashlines as user zoom-in/out 
     *    by implementing some kind of listener. But this approach would be terrible resource-hogging!
     */
    public static void constructDashedPolyline_byZoomValue(
            Context context, 
            GoogleMap map, 
            List<LatLng> listOfPlacesLatLngOrig) {
        double zoom = map.getCameraPosition().zoom;
        double multiplyFactor = zoom * 2;
        
        int numOfLatLng = listOfPlacesLatLngOrig.size() - 1;
        for (int i = 0; i < numOfLatLng; i++) {
            LatLng latLngCurrent = listOfPlacesLatLngOrig.get(i);
            LatLng latLngNext = listOfPlacesLatLngOrig.get(i + 1);

            double difLat = latLngNext.latitude - latLngCurrent.latitude;
            double difLng = latLngNext.longitude - latLngCurrent.longitude;
            double divLat = difLat / multiplyFactor;
            double divLng = difLng / multiplyFactor;
            
            LatLng tmpLatOri = latLngCurrent;
            for(int j = 0; j < multiplyFactor; j++) {
                LatLng loopLatLng = new LatLng(tmpLatOri.latitude + (divLat * 0.25f), tmpLatOri.longitude + (divLng * 0.25f));

                map.addPolyline(new PolylineOptions()
                    .add(loopLatLng)
                    .add(new LatLng(tmpLatOri.latitude + divLat, tmpLatOri.longitude + divLng))
                    .width(AndroidConstants.MAP_POLYLINE_WIDTH)
                    .color(context.getResources().getColor(R.color.color_mapmarker_pathline)));

                tmpLatOri = new LatLng(tmpLatOri.latitude + divLat, tmpLatOri.longitude + divLng);
            }
        }
    }    
    
    /*
     * Constructs dashed-polyline given constant size of Dashline and gaps (by Swati Pardeshi)
     * PRO: You decides the size once, and it produced uniform dashlines and spaces between
     *      them for all place points. Number of dashlines between points vary.
     *      It does not depend on zoom level.
     * CON:
     * -> Algorithm is more complex where we must calculate distance between two points,
     *    then calculate number of dashlines between them. This approach easily multiplies
     *    number of polylines into thousands (out of control)!
     * -> The size is truly scaled to the map. So when zoom-in, dashlines looked nice, even 
     *    in size and gaps (their actual size). BUT when zoom-out, they became blurry wiggled
     *    straight solid lines. Not good!
     * -> Possibly extending the code to keep recalculating the dashlines with the constant
     *    size changes as user zoom-in/out by implementing some kind of listener. 
     *    But this approach would be terrible resource-hogging!
     */
    public static void constructDashedPolyline_byLineSize(
            Context context, 
            GoogleMap map, 
            List<LatLng> listOfPlacesLatLngOrig) {
        //double LINE_SIZE = 0.01; //km
        double LINE_SIZE = 100; //km
        int numOfLatLng = listOfPlacesLatLngOrig.size() - 1;
        boolean isAdded = false;
        for (int i = 0; i < numOfLatLng; i++) {
            LatLng latLngCurrent = listOfPlacesLatLngOrig.get(i);
            LatLng latLngNext = listOfPlacesLatLngOrig.get(i + 1);
            
            //Get distance between current and next point:
            double distance = calculateDistance(latLngCurrent, latLngNext);
            if (distance < LINE_SIZE) {
                /*
                 * Too small to break it into dashed lines. Just add them into the new List given
                 * they haven't been added before - to avoid adding a LatLng twice!
                 */
                if(isAdded) {
                    //Skip!
                    isAdded = false;
                } else {
                    map.addPolyline(new PolylineOptions()
                    .add(latLngCurrent)
                    .add(latLngNext)
                    .width(AndroidConstants.MAP_POLYLINE_WIDTH)
                    .color(context.getResources().getColor(R.color.color_mapmarker_pathline)));

                    isAdded = true;
                }

            } else {
                /*
                 * Break the line between two LatLng into multiple polylines (hence 'dashed'). 
                 */
                //How many divisions to make of this line:
                int countOfDivisions = (int) ((distance/LINE_SIZE));

                //Get difference to add per lat/lng:
                double latDiff = (latLngNext.latitude - latLngCurrent.latitude) / countOfDivisions;
                double lngDiff = (latLngNext.longitude - latLngCurrent.longitude) / countOfDivisions;

                //Last known indicates start point of this new polyline, initialized to current i-th point:
                LatLng latLngDashLastKnown = new LatLng(latLngCurrent.latitude, latLngCurrent.longitude);
                for (int j = 0; j < countOfDivisions; j++) {

                    //Next point is (latLngDashLastKnown + diff), and keep adding these dashed lines up until number of 'countOfDivisions'
                    LatLng latLngDashNext = new LatLng(latLngDashLastKnown.latitude + latDiff, latLngDashLastKnown.longitude + lngDiff);
                    if(isAdded) {
                        //Skip!
                        isAdded = false;
                    } else {
                        map.addPolyline(new PolylineOptions()
                        .add(latLngDashLastKnown)
                        .add(latLngDashNext)
                        .color(context.getResources().getColor(R.color.color_mapmarker_pathline)));
                        isAdded = true;
                    }
                    
                    latLngDashLastKnown = latLngDashNext;
                }
            }
        }
    }
    
    /*
     * Constructs dashed-polyline given constant number of dashlines depending on distance (by ME!)
     * PRO: The impl is sort of combo of two custom solutions above with our own tweak.
     *      We control the number of dashlines we produce, hence avoiding out of control
     *      thousands of new polylines being created. By limiting number of dashlines, we
     *      still achieve our target to show dashed-polylines in our map, yet preventing
     *      resouce-hogging calculations.
     * CON:
     * -> Because it depends on distance between points, we try to have different number 
     *    of dashlines for close and long-distance as constants. These numbers were totally
     *    made up for our liking.
     * -> Each set of distances will have different numbers of dashlines, causing inconsistent 
     *    line sizes. When distance is close, it produces less number of dashlines than
     *    when distance is far away.
     */
    public static void constructDashedPolyline_byConstantDashlineNumberGivenDistance(
            Context context, 
            GoogleMap map, 
            List<LatLng> listOfPlacesLatLngOrig) {
        int numOfLatLng = listOfPlacesLatLngOrig.size() - 1;
        for (int i = 0; i < numOfLatLng; i++) {
            LatLng latLngCurrent = listOfPlacesLatLngOrig.get(i);
            LatLng latLngNext = listOfPlacesLatLngOrig.get(i + 1);
            double distance = calculateDistance(latLngCurrent, latLngNext);
            
            //Limit the number of dahslines between points according to their distance:
            int numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_NONE_NUMOFDASHLINES;
            if(AndroidConstants.MAP_POLYLINE_DISTANCE_NONE_VALUE_KM < distance && distance <= AndroidConstants.MAP_POLYLINE_DISTANCE_VERYSMALL_VALUE_KM) {
                numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_VERYSMALL_NUMOFDASHLINES;
            } else if(distance <= AndroidConstants.MAP_POLYLINE_DISTANCE_SMALL_VALUE_KM) {
                numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_SMALL_NUMOFDASHLINES;
            } else if(distance <= AndroidConstants.MAP_POLYLINE_DISTANCE_MEDIUM_VALUE_KM) {
                numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_MEDIUM_NUMOFDASHLINES;
            } else if(distance <= AndroidConstants.MAP_POLYLINE_DISTANCE_LARGE_VALUE_KM) {
                numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_LARGE_NUMOFDASHLINES;
            } else if(distance <= AndroidConstants.MAP_POLYLINE_DISTANCE_VERYLARGE_VALUE_KM) {
                numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_VERYLARGE_NUMOFDASHLINES;
            } else if(AndroidConstants.MAP_POLYLINE_DISTANCE_VERYLARGE_VALUE_KM < distance) {
                numOfDashlines = AndroidConstants.MAP_POLYLINE_DISTANCE_INFINITE_NUMOFDASHLINES;
            }

            double difLat = latLngNext.latitude - latLngCurrent.latitude;
            double difLng = latLngNext.longitude - latLngCurrent.longitude;
            double divLat = difLat / numOfDashlines;
            double divLng = difLng / numOfDashlines;
            
            LatLng tmpLatOri = latLngCurrent;
            for(int j = 0; j < numOfDashlines; j++) {
                LatLng loopLatLng;
                if(j == 0) {
                    loopLatLng = tmpLatOri;
                } else {
                    loopLatLng = new LatLng(tmpLatOri.latitude + (divLat * 0.25f), tmpLatOri.longitude + (divLng * 0.25f));
                }

                map.addPolyline(new PolylineOptions()
                    .add(loopLatLng)
                    .add(new LatLng(tmpLatOri.latitude + divLat, tmpLatOri.longitude + divLng))
                    .width(AndroidConstants.MAP_POLYLINE_WIDTH)
                    .color(context.getResources().getColor(R.color.color_mapmarker_pathline)));

                tmpLatOri = new LatLng(tmpLatOri.latitude + divLat, tmpLatOri.longitude + divLng);
            }
        }
    }
     
    /*
     * Calculates horizontal distance between two points given their (Lat, Lan) coordinates 
     * (and ignoring their altitude/heights) using Haversine formula.
     * REF: 
     * - https://en.wikipedia.org/wiki/Haversine_formula 
     * - http://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
     * 
     * @returns Distance in Kilometers (km)
     */
    private static double calculateDistance(LatLng latlng1, LatLng latlng2) {
        if ((latlng1.latitude == latlng2.latitude) 
                && (latlng1.longitude == latlng2.longitude)) {
            return 0;
        }

        double lat1 = latlng1.latitude;
        double lon1 = latlng1.longitude;
        double lat2 = latlng2.latitude;
        double lon2 = latlng2.longitude;
        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2)
                   + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                   * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = AndroidConstants.MAP_RADIUS_EARTH_KM * c;
        distance = Math.pow(distance, 2);
        return Math.sqrt(distance);
    }        

}
