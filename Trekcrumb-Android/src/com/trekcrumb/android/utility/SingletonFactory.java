package com.trekcrumb.android.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.trekcrumb.android.R;
import com.trekcrumb.android.iconfont.IconFontDrawableImpl;

/**
 * A util class that generates and maintains singleton variables/objects that, essentially, 
 * once they are generated/instantiated their values become CONSTANTS. 
 * 
 * These singletons are widely used across the Android app and we try to minimize operation 
 * to re-generate them whenever we need them. In addition, some of these singletons read Android 
 * properties for their values, and thus need the app's Context and Resources to access the properties.
 * 
 * @author Val Triadi
 * @since 2/2017
 */
public class SingletonFactory {
    public static int PICTURE_MAX_SIZE_PIXEL_VIEW = -1;
    public static int PICTURE_MAX_SIZE_PIXEL_THUMBNAIL = -1;
    public static int PICTURE_PROFILE_MAX_SIZE_PIXEL_VIEW = -1;
    public static int PICTURE_PROFILE_MAX_SIZE_PIXEL_THUMBNAIL = -1;
    
    private static Bitmap IMAGE_NOT_AVAILABLE;
    private static Bitmap IMAGE_MAP_PLACE_MARKER;
    private static Bitmap IMAGE_MAP_CURRENT_LOCATION_MARKER;
    private static Bitmap IMAGE_USER_PROFILE_DEFAULT;

    private SingletonFactory() {};

    public static int getPictureViewSize(final Context context) {
        if(PICTURE_MAX_SIZE_PIXEL_VIEW == -1) {
            PICTURE_MAX_SIZE_PIXEL_VIEW = (int) context.getResources().getDimension(R.dimen.image_picture_size);
        }
        return PICTURE_MAX_SIZE_PIXEL_VIEW;
    }
    
    public static int getPictureThumbnailViewSize(final Context context) {
        if(PICTURE_MAX_SIZE_PIXEL_THUMBNAIL == -1) {
            PICTURE_MAX_SIZE_PIXEL_THUMBNAIL = (int) context.getResources().getDimension(R.dimen.image_picture_thumbnail_size);
        }
        return PICTURE_MAX_SIZE_PIXEL_THUMBNAIL;
    }
    
    public static int getPictureProfileViewSize(final Context context) {
        if(PICTURE_PROFILE_MAX_SIZE_PIXEL_VIEW == -1) {
            PICTURE_PROFILE_MAX_SIZE_PIXEL_VIEW = (int) context.getResources().getDimension(R.dimen.image_profile_size);
        }
        return PICTURE_PROFILE_MAX_SIZE_PIXEL_VIEW;
    }

    public static int getPictureProfileThumbnailViewSize(final Context context) {
        if(PICTURE_PROFILE_MAX_SIZE_PIXEL_THUMBNAIL == -1) {
            PICTURE_PROFILE_MAX_SIZE_PIXEL_THUMBNAIL = (int) context.getResources().getDimension(R.dimen.image_profile_thumbnail_size);
        }
        return PICTURE_PROFILE_MAX_SIZE_PIXEL_THUMBNAIL;
    }

    public static Bitmap getImageNotAvailable(final Context context) {
        if(IMAGE_NOT_AVAILABLE == null) {
            Log.d("SingletonFactory", "getImageNotAvailable() - Initializing IMAGE_NOT_AVAILABLE.");
            IMAGE_NOT_AVAILABLE = ImageUtil.drawableToBitmap(
                    new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_image_na));
        }
        return IMAGE_NOT_AVAILABLE;
    }
    
    public static Bitmap getMapPlaceMarker(final Context context) {
        if(IMAGE_MAP_PLACE_MARKER == null) {
            Log.d("SingletonFactory", "getPlaceMarker() - Initializing IMAGE_MAP_PLACE_MARKER.");
            IMAGE_MAP_PLACE_MARKER = ImageUtil.drawableToBitmap(
                    new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_mapmarker_place));
        }
        return IMAGE_MAP_PLACE_MARKER;
    }
    
    public static Bitmap getMapCurrentLocMarker(final Context context) {
        if(IMAGE_MAP_CURRENT_LOCATION_MARKER == null) {
            IMAGE_MAP_CURRENT_LOCATION_MARKER = ImageUtil.drawableToBitmap(
                    new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_mapmarker_currentloc));
        }
        return IMAGE_MAP_CURRENT_LOCATION_MARKER;
    }

    public static Bitmap getUserProfileImageDefault(final Context context) {
        if(IMAGE_USER_PROFILE_DEFAULT == null) {
            IMAGE_USER_PROFILE_DEFAULT = ImageUtil.drawableToBitmap(
                    new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_userprofile));
        }
        return IMAGE_USER_PROFILE_DEFAULT;
    }
    
}
