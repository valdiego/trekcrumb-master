package com.trekcrumb.android.utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.trekcrumb.android.asynctask.PictureProfileImageRetrieveAsyncTask;
import com.trekcrumb.android.database.PictureDB;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.utility.ValidationUtil;

public class ImageUtil {
    private static final String LOG_TAG = "ImageUtil";
    
    /**
     * Decodes a Bitmap object into array of bytes.
     * @return The bytes array of the bitmap object.
     */
    public static byte[] decodeBitmapToBytes(Bitmap imageBitmap) {
        Log.d(LOG_TAG, "decodeBitmapToBytes() - Starting ...");
        byte[] imageBytes = null;
        ByteArrayOutputStream bytesOutputStream = null;
        try {
            bytesOutputStream = new ByteArrayOutputStream(imageBitmap.getWidth() * imageBitmap.getHeight());
            imageBitmap.compress(AndroidConstants.PICTURE_FORMAT, AndroidConstants.PICTURE_COMPRESS_QUALITY_100, bytesOutputStream);
            imageBytes = bytesOutputStream.toByteArray();
        } finally {
            if(bytesOutputStream != null) {
                try {
                    bytesOutputStream.close();
                    bytesOutputStream = null;
                } catch(Exception e) { 
                    //O well
                }
            }
        }
        return imageBytes;
    }
    
    /**
     * Decodes a Bitmap object into a new file, which essentially save it into local disk.
     * @param imageFilename - The new name for the file to create
     * @return The absolute location (path) of the created file
     */
    public static String decodeBitmapToFile(final Bitmap imageBitmap, String imageFilename) throws Exception {
        Log.d(LOG_TAG, "decodeBitmapToFile() - Starting ...");
        String imageFilePath = null;
        File imageFile = null;
        FileOutputStream fileOutputStream = null;
        try {
            imageFile = new File(AndroidConstants.APP_LOCAL_DIR_PICTURES + imageFilename);
            fileOutputStream = new FileOutputStream(imageFile);
            imageBitmap.compress(AndroidConstants.PICTURE_FORMAT, AndroidConstants.PICTURE_COMPRESS_QUALITY_100, fileOutputStream);
            fileOutputStream.flush();
            
            imageFilePath = imageFile.getPath();
            Log.d(LOG_TAG, "decodeBitmapToFile() - Completed. New image file created: " + imageFilePath);
        } finally {
            if(fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                    fileOutputStream = null;
                } catch(Exception e) {
                    //O well
                }
            }
        }
        return imageFilePath;
    }
    
    /**
     * Decodes Picture image from either its bytes array OR an image file (given its filepath)into a bitmap object.
     * 
     * @param imageBytes - The bytes array of an image. If this is provided, the param filePath is ignored.
     * @param filePath - The full path of the source image file on local disk.
     * @param maxSizePixels - The maximum size in pixels (either width or height or both) allowed. 
     *                       If the original image size is > than the specified maxSizeBytes, then
     *                       it will be scaled down until either its width or height or both is <= 
     *                       maxSizePixels, and the image aspect ratio is maintained.. 
     *                       Otherwise, its size will not change and returned as is.
     * @param isScaledByPowerTwo - If TRUE, then the image will be scaled down by "the power of 2" (50% each time)
     *                       until it meets the mazSizeBytes requirement. 
     *                       If FALSE, the scale factor will be calculated until the size meets
     *                       the mazSizeBytes requirement. This process could be slower than that of
     *                       power of 2.
     * @param isThumbnail - If this is for thumbnail view, the value must be 'true' and the returned
     *                      dimension is squared CommonConstants.PICTURE_MAX_SIZE_PIXEL_THUMBNAIL. Both
     *                      params 'maxSizePixels' and 'isScaledByPowerTwo' are ignored. Otherwise, value
     *                      must be false.
     *                      
     * @return The Bitmap object of the image.                       
     */
    public static Bitmap decodePictureImageIntoBitmap(
            byte[] imageBytes,
            String filePath,
            int maxSizePixels, 
            boolean isScaledByPowerTwo,
            boolean isThumbnail) {
        Log.d(LOG_TAG, "decodePictureImageIntoBitmap() - Starting ...");
        if(isThumbnail) {
            return decodeImageIntoBitmap(imageBytes, filePath, true, 1, maxSizePixels, maxSizePixels);
        }
        
        //ELSE: Process image with scale factor:
        if(imageBytes == null && filePath == null) {
            Log.e(LOG_TAG, "decodePictureImageIntoBitmap() - ERROR: Both image's byte array and filepath are EMPTY! Returning null bitmap.");
            return null;
        }

        //1. Measure original dimensions:
        BitmapFactory.Options bitmapOptionsSource = new BitmapFactory.Options();
        bitmapOptionsSource.inJustDecodeBounds = true;
        if(imageBytes != null) {
            BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, bitmapOptionsSource);
        } else {
            BitmapFactory.decodeFile(filePath, bitmapOptionsSource);
        }
        if(bitmapOptionsSource.outWidth <= 0
                || bitmapOptionsSource.outHeight <= 0) {
            if(imageBytes != null) {
                Log.e(LOG_TAG, "decodePictureImageIntoBitmap() - ERROR: Invalid input image bytes array! Cannot be decoded into bitmap.");
            } else {
                Log.e(LOG_TAG, "decodePictureImageIntoBitmap() - ERROR: Cannot find file for given filepath [" + filePath + "]");
            }
            return null;
        }
        Log.d(LOG_TAG, "decodePictureImageIntoBitmap() - Original image (W x H) = "
                + bitmapOptionsSource.outWidth + " x " + bitmapOptionsSource.outHeight);

        //2. Calculate the scale factor:
        int scaleFactor = 1;
        if(isScaledByPowerTwo) {
            scaleFactor = scaleImageByPowerTwo(bitmapOptionsSource, maxSizePixels);
        } else {
            scaleFactor = scaleImageByRatio(bitmapOptionsSource, maxSizePixels);
        }
        
        //3. Decode into bitmap:
        return decodeImageIntoBitmap(imageBytes, filePath, false, scaleFactor, -1, -1);
    }    

    /**
     * Decodes a user profile image into a bitmap object, given either its bytes array 
     * OR its filepath (if local). 
     * 
     * This will scale down the image to "SQUARE" dimension and to ignore original image aspect ratio.
     * The size of the bitmap is determined by the parameter isThumbnail.
     * 
     * @param imageBytes - The bytes array of the image to be decoded. If this is provided, the param filePath is ignored.
     * @param filePath - The full path of the source image file on local disk.
     * @param maxSizePixels - The size of the square image's width and length.
     * @return The Bitmap object
     */
    public static Bitmap decodeProfileImageIntoBitmap(
            byte[] imageBytes,
            String filePath,
            int maxSizePixels) {
        return decodeImageIntoBitmap(imageBytes, filePath, true, 1, maxSizePixels, maxSizePixels);
    }
    
    /*
     * @param imageBytes - The bytes array of an image. If this is provided, the param filePath is ignored.
     * @param filePath - The full path of the source image file on local disk.
     * @param isScaleImage - Default is false. If true, the operation will scale down the image by the given 
     *                       (scaleImageWidth x scaleImageLength), and to ignore the scaleFactor paam.
     * @param scaleFactor - Default is 1 that is to return the image's original dimension. If provided
     *                      (must be bigger than 1), the operation will reduce the image dimension
     *                      by 1/scaleFactor. Example, if scaleFactor = 2, the resulting image
     *                      dimensions will be 1/2 of the original. The scaleFactor will be used only
     *                      if isScaleImage == false.
     * @param scaleImageWidth - The width of the result bitmap to returned when isScaleImage == true
     * @param scaleImageLength - The length of the result bitmap to returned when isScaleImage == true
     * @return Decoded image bitmap
     */
    private static Bitmap decodeImageIntoBitmap(
            byte[] imageBytes,
            String filePath,
            boolean isScaleImage,
            int scaleFactor,
            int scaleImageWidth,
            int scaleImageLength) {
        if(imageBytes == null && filePath == null) {
            Log.e(LOG_TAG, "decodeImageIntoBitmap() - ERROR: Either image's byte array or filepath must be provided! Returning null bitmap.");
            return null;
        }
        Log.d(LOG_TAG, "decodeImageIntoBitmap() - Starting ... "
                + "Device memory (heap) maximum [" + AndroidConstants.DEVICE_HEAP_MEMORY_MAX + " KB]"
                + ", device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                );
        
        boolean isDecodeImageByteArray = true;
        if(imageBytes == null) {
            isDecodeImageByteArray = false;
        }
        
        //1. Decode original image:
        /*
         * Warning: These BitmapFactory operations below are prone to OutOfmemory (OOM) error
         * since it would demand and use a lot of heap memory. If the app has been processing 
         * a lot of images, the heap would reach the max. and cause OOM (Android OS places
         * images in heap memory).
         * To avoid this, important to set inPurgeable = true, inInputShareable = true and
         * scale images to smaller size.
         */
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inPreferredConfig = AndroidConstants.PICTURE_IMAGE_PIXEL_CONFIG; 
        bitmapOptions.inJustDecodeBounds = false;
        bitmapOptions.inPurgeable = true;
        bitmapOptions.inInputShareable = true;
        if(!isScaleImage) {
            bitmapOptions.inSampleSize = scaleFactor;
        }
        Bitmap bitmapOrig = null;
        if(isDecodeImageByteArray) {
            bitmapOrig = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, bitmapOptions);
            imageBytes = null; //Release resources
        } else {
            bitmapOrig = BitmapFactory.decodeFile(filePath, bitmapOptions);
        }
        if(bitmapOrig == null) {
            if(isDecodeImageByteArray) {
                Log.e(LOG_TAG, "decodeImageIntoBitmap() - ERROR: Unexpected error when decoding bytes array resulting the bitmap to NULL!");
            } else {
                Log.e(LOG_TAG, "decodeImageIntoBitmap() - ERROR: Cannot find file for filepath [" + filePath + "]");
            }
            return null;
        }
        
        if(!isScaleImage) {
            Log.d(LOG_TAG, "decodeImageIntoBitmap() - Completed decoding image: "
                    + "Scale factor = " + scaleFactor
                    + ", bitmap size (W x H) = " + bitmapOrig.getWidth() + " x " + bitmapOrig.getHeight()
                    + ", bitmap total KB = " + bitmapOrig.getByteCount()/1024
                    + ", device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                    );
            return bitmapOrig;
        }
        
        //ELSE:
        //2. Decode the image again with given scale dimensions:
        /*
         * Warning: The call to createScaledBitmap() will create/return a new bitmap with scaled 
         * from an existing bitmap, when possible. HOWEVER, if the specified width and height are 
         * the same as the current width and height of the source bitmap, the source bitmap is 
         * returned and no new bitmap is created. Therefore, be cautious before calling
         * bitmapOrig.recycle() or bitmapOrig = null (to release resources) because it may 
         * cause app to throw exception if bitmapOrig is returned and used as the result bitmap.
         */
        Bitmap bitmapScaled = Bitmap.createScaledBitmap(bitmapOrig, scaleImageWidth, scaleImageLength, false);
        if(bitmapScaled != bitmapOrig) {
            //Release resource:
            bitmapOrig.recycle();
        }
        Log.d(LOG_TAG, "decodeImageIntoBitmap() - Completed decoding image with scaled dimensions: "
                + "bitmap size (W x H) = " + bitmapScaled.getWidth() + " x " + bitmapScaled.getHeight()
                + ", bitmap total KB = " + bitmapScaled.getByteCount()/1024
                + ", device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                );
        return bitmapScaled;
    }        
    
    /*
     * Calculates scale factor (reduction) of an image to be equal to or less than the required 
     * maximum dimension (maxSizeBytes) that would apply to either the width or the height or both. 
     * This means it keeps image's aspect ratio.
     *
     * When calculating the scale value, it chooses the largest ratio between 
     * (image's original width/maxSizeBytes) vs. (image's original height/maxSizeBytes)
     * to guarantee the final scaled image dimensions are less than or equal to the requirement.
     * 
     * @param bitmapOptionsOrig - BitmapFactory.Options of the original image to be scaled, containing
     *                            data of image original width x height.
     * @param maxSizeBytes - The maximum bytes either the image width or height or both can have.                           
     * @return scale - If > 1, the image will be scaled by that value to a smaller size.
     *                 Example: if scale is '4', then it will scale to 1/4 the width/height of the 
     *                 original, and 1/16 the number of pixels.
     *                 If scale is 1, there is no scaling required. 
     */
    private static int scaleImageByRatio(
            BitmapFactory.Options bitmapOptionsOrig,
            int maxSizeBytes) {
        int scale = 1;
        int heightOrig = bitmapOptionsOrig.outHeight;
        int widthOrig = bitmapOptionsOrig.outWidth;
        if (heightOrig > maxSizeBytes 
                || widthOrig > maxSizeBytes) {
            int widthRatio = Math.round((float) widthOrig / (float) maxSizeBytes );
            int heightRatio = Math.round((float) heightOrig / (float) maxSizeBytes );
            if(widthRatio > heightRatio) {
                scale = widthRatio;
            } else {
                scale = heightRatio;
            }
        }
        return scale;
    }
    
    /*
     * Calculates scale factor (reduction) of an image to be equal to or less than the required 
     * maximum dimension (maxSizeBytes) that would apply to either the width or the height or both. 
     * This means it keeps image's aspect ratio.
     *
     * When calculating the scale value, it will use the power of '2' (reduction by 50%) until
     * the largest dimension is less or equal to maxSizeBytes.
     * (See recommendations on Android's BitmapFactory.Options for why to use power of 2).
     * 
     * @param bitmapOptionsOrig - BitmapFactory.Options of the original image to be scaled, containing
     *                            data of image original width x height.
     * @param maxSizeBytes - The maximum bytes either the image width or height or both can have.                           
     * @return scale - If > 1, the image will be scaled by that value to a smaller size.
     *                 Example: if scale is '4', then it will scale to 1/4 the width/height of the 
     *                 original, and 1/16 the number of pixels.
     *                 If scale is 1, there is no scaling required. 
     */
    private static int scaleImageByPowerTwo(
            BitmapFactory.Options bitmapOptionsOrig,
            int maxSizeBytes) {
        int scale = 1;
        int widthOrig = bitmapOptionsOrig.outWidth;
        int heightOrig = bitmapOptionsOrig.outHeight;
        while (true) {
            if (widthOrig <= maxSizeBytes 
                    && heightOrig <= maxSizeBytes) {
                break;
            }
            widthOrig /= 2;
            heightOrig /= 2;
            scale *= 2;
        }
        return scale;
    }
    
    public static void savePictureToSDCardAndDB(
            Context context,
            Picture pictureToSave,
            Bitmap pictureBitmap) throws Exception {
        //Save picture to SDCard:
        String picFilename = pictureToSave.getImageName();
        String picLocalURL = decodeBitmapToFile(pictureBitmap, picFilename);
        pictureToSave.setImageURL(picLocalURL);
        
        //Save picture to DB:
        PictureDB.create(context, pictureToSave);    
    }

    /**
     * Common method to retrieve user profile image from back-end servers. The operation will
     * launch separate thread to finish the job.
     * 
     * @param context - The hosting activity
     * @param userProfileImageName - The name of the image. If NULL, the entire operation
     *                               will be void.
     * @param userProfileDefaultView - View object where the default icon for user image resides.
     *                                 It would be hidden once the image is available.
     * @param userProfileImageView - View object where the profile image will resides.
     * @param isUserLoginTrip - TRUE if the user is current UserLogin.
     */
    public static void retrieveUserProfileImage(
            Context context, 
            String userProfileImageName,
            View userProfileDefaultView,
            ImageView userProfileImageView,
            boolean isUserLoginData,
            boolean isThumbnail) {
        if(!ValidationUtil.isEmpty(userProfileImageName)) {
            //Call new thread for the work:
             PictureProfileImageRetrieveAsyncTask backendTask = 
                    new PictureProfileImageRetrieveAsyncTask(
                            context,
                            userProfileImageName,
                            userProfileDefaultView,
                            userProfileImageView,
                            null,
                            isUserLoginData,
                            isThumbnail);
            backendTask.execute();
        }
    }

    /**
     * Common method to retrieve user profile image from back-end servers. The operation will
     * launch separate thread to finish the job.
     * 
     * @param context - The hosting activity
     * @param userProfileImageName - The name of the image. If NULL, the entire operation
     *                               will be void.
     */
    public static void retrieveUserProfileImageActionBarMenu(
            Context context, 
            String userProfileImageName,
            MenuItem menuItem) {
        if(!ValidationUtil.isEmpty(userProfileImageName)) {
            //Call new thread for the work:
            PictureProfileImageRetrieveAsyncTask backendTask = 
                    new PictureProfileImageRetrieveAsyncTask(
                            context,
                            userProfileImageName,
                            null,
                            null,
                            menuItem,
                            true,
                            true);
            backendTask.execute();
        }
    }
    
    /**
     * Converts a Drawable object into a Bitmap object.
     * 1. Most of Android regular Drawable (such as those image files under '/drawable' folders and
     *    XML definition with '<bitmap>' root element) is by default a BitmapDrawable object, and we
     *    can easily get their bitmap by invoking its getBitmap() method.
     * 2. Other types of Drawable such as ShapeDrawable or our custom iconfont's IconFontDrawableImpl
     *    are NOT BitmapDrawable, thus getBitmap() method does not exist. Therefore for these types, 
     *    we go deeper to create a Bitmap out of these drawables.
     *    
     * @param drawable - The object to be converted into a Bitmap. It is either a BitmapDrawable type or
     *        others.
     * @return Bitmap conversion of the drawable in question.
     */
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                bitmap = bitmapDrawable.getBitmap();
            }
            return bitmap;
        }

        /*
         * ELSE: Other drawable types
         * 1. For solid color Drawable, the getIntrinsicWidth() and getIntrinsicHieght() will 
         *    return -1. Here, we convert it into a 5x5 pixel bitmap.
         */
        int bitmapWidth = drawable.getIntrinsicWidth();
        int bitmapHeight = drawable.getIntrinsicHeight();
        if(bitmapWidth <= 0 || bitmapHeight <= 0) {
            bitmapWidth = 5;
            bitmapHeight = 5;
        }
        bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }    

}
