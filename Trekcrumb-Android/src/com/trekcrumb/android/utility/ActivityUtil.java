package com.trekcrumb.android.utility;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.trekcrumb.android.activity.CommentListActivity;
import com.trekcrumb.android.activity.HomeActivity;
import com.trekcrumb.android.activity.PictureCreateActivity;
import com.trekcrumb.android.activity.PictureEditActivity;
import com.trekcrumb.android.activity.PlaceCreateActivity;
import com.trekcrumb.android.activity.PlaceEditActivity;
import com.trekcrumb.android.activity.SupportActivity;
import com.trekcrumb.android.activity.TripListActivity;
import com.trekcrumb.android.activity.TripViewActivity;
import com.trekcrumb.android.activity.UserReactivateActivity;
import com.trekcrumb.android.activity.UserViewActivity;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

public class ActivityUtil {
    private ActivityUtil() {}
    
    public static void doHomeActivity(
            final Activity hostingActivityContext,
            boolean isSkipInit,
            boolean isErrorMessage,
            String errorOrInfoMessage) {
        Intent intent = new Intent(hostingActivityContext, HomeActivity.class);
        
        //Clears ALL stack activities prior to this:
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Bundle bundle = new Bundle();
        if(isSkipInit) {
            bundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_HOME_SKIP_INIT, isSkipInit);
        }
        
        if(isErrorMessage) {
            ArrayList<String> listOfErrorMsg = new ArrayList<String>();
            listOfErrorMsg.add(errorOrInfoMessage);
            bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, listOfErrorMsg);
        } else {
            bundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, errorOrInfoMessage);
        }
        
        intent.putExtras(bundle);
        hostingActivityContext.startActivity(intent);
    }    
    
    public static boolean doUserLoginViewActivity(
            final Activity hostingActivityContext,
            boolean isErrorMessage,
            String errorOrInfoMessage) {
        Intent targetIntent = new Intent(hostingActivityContext, UserViewActivity.class);
        
        //Clears any stack activity prior to this ONLY IF this activity has been previously active (in the stack):
        targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        
        Bundle bundle = new Bundle();
        
        User userLogin = ((TrekcrumbApplicationImpl) hostingActivityContext.getApplicationContext()).getUserLogin();
        if(userLogin != null) {
            bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, userLogin.getUserId());
        }
        
        if(!ValidationUtil.isEmpty(errorOrInfoMessage)) {
            if(isErrorMessage) {
                ArrayList<String> listOfErrorMsg = new ArrayList<String>();
                listOfErrorMsg.add(errorOrInfoMessage);
                bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, listOfErrorMsg);
            } else {
                bundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, errorOrInfoMessage);
            }
        }

        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
        return true;
    }

    public static boolean doUserViewActivity(
            final Activity hostingActivityContext,
            String userID) {
        Intent targetIntent = new Intent(hostingActivityContext, UserViewActivity.class);
        
        //Clears any stack activity prior to this ONLY IF this activity has been previously active (in the stack):
        targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, userID);
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
        return true;
    }
    
    public static void doUserReactivateActivity(
            final Activity hostingActivityContext,
            User userDeactivated) {
        Intent intent = new Intent(hostingActivityContext, UserReactivateActivity.class);
        
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_USER_DEACTIVATED, userDeactivated);
        intent.putExtras(bundle);
        
        hostingActivityContext.startActivity(intent);
    }

    /**
     * @param tripListType - Valid values are CommonConstants:
     *                       KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED, 
     *                       KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED and
     *                       KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES. 
     *                       If the value was none of them, then default is KEY_BUNDLE_TRIPS_LIST_TYPE_PUBLISHED.
     */
    public static void doTripListActivity(
            final Activity hostingActivityContext,
            User user,
            int numOfTripsUnpublished,
            String tripListType,
            boolean isAfterDeleteTrip,
            String preConfigInfoMessage) {
        Intent targetIntent = new Intent(hostingActivityContext, TripListActivity.class);;

        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, user.getUserId());
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME, user.getUsername());
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME, user.getFullname());
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE, tripListType);
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PAGINATION_IS_SHOW, true);
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, preConfigInfoMessage);
        
        //Number of total trips:
        if(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED.equalsIgnoreCase(tripListType)) {
            bundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL, numOfTripsUnpublished);
        } else if(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES.equalsIgnoreCase(tripListType)) {
            bundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL, user.getNumOfFavorites());
        } else {
            //Default:
            bundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL, user.getNumOfTotalTrips());
        }
        
        if(isAfterDeleteTrip) {
            //Clears any stack activity prior to this ONLY IF this activity has been previously active (in the stack):
            targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            bundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE, true);
        }
        
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
    }

    /**
     * Starts an activity to display Trip details.
     * 
     * @param hostingActivityContext - The hosting activity where this method is called
     * @param trip - The Trip in question.
     * @param userLogin - The user who owns the app
     * @param userOther - Guest (other) user that this user is looking and whose trip belongs to.
     *                       If the trip is the login user's, then this param must be set to null.
     */
    public static boolean doTripViewActivity(
            final Activity hostingActivityContext,
            String tripID,
            Trip trip,
            boolean isAfterEdit,
            String errorOrInfoMessage, 
            boolean isErrorMessage) {
        Intent targetIntent = new Intent(hostingActivityContext, TripViewActivity.class);
        
        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED_ID, tripID);
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, trip);
        if(!ValidationUtil.isEmpty(errorOrInfoMessage)) {
            if(isErrorMessage) {
                ArrayList<String> listOfErrorMsg = new ArrayList<String>();
                listOfErrorMsg.add(errorOrInfoMessage);
                bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, listOfErrorMsg);
            } else {
                bundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, errorOrInfoMessage);
            }
        }
        targetIntent.putExtras(bundle);
        
        if(isAfterEdit) {
            /*
             * Clears any stack activity prior to this ONLY IF this activity has been previously active (in the stack)
             * NOTE: Important. After trip edit, it will go to Trip View page and any previous Trip View history
             * must be erased so the back button does not bring back the old page with old data.
             */
            targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        
        hostingActivityContext.startActivity(targetIntent);
        return true;
    }    

    /**
     * Open the Place Create activity.
     */
    public static void doPlaceCreateActivity(
            final Activity hostingActivityContext,
            Trip selectedTrip,
            Location currentLocation) {
        Intent targetIntent = new Intent(hostingActivityContext, PlaceCreateActivity.class);
        
        //Prevent the 'create page' from stack activity:
        targetIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, selectedTrip);
        if(currentLocation != null) {
            bundle.putParcelable(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT, currentLocation);
        }
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
    }
    
    /**
     * Open the Place Edit activity.
     */
    public static void doPlaceEditActivity(
            final Activity hostingActivityContext,
            Trip selectedTrip,
            Place selectedPlace,
            int selectedPlaceIndex) {
        Intent targetIntent = new Intent(hostingActivityContext, PlaceEditActivity.class);
        
        //Prevent the 'edit page' from stack activity:
        targetIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, selectedTrip);
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PLACE_SELECTED, selectedPlace);
        bundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, selectedPlaceIndex);
        
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
    }
    
    public static void doPictureCreateActivity(
            final Activity hostingActivityContext,
            Trip selectedTrip) {
        Intent targetIntent = new Intent(hostingActivityContext, PictureCreateActivity.class);
        
        /*
         * NOTE 8/2014: 
         * PictureCreateActivity cannot be declared with "Intent.FLAG_ACTIVITY_NO_HISTORY"
         * because it needs to open a 3rd app: Pic Gallery or Camera, and then must
         * return to the prior PictureCreateActivity form after selection. Declaring 
         * no history would remove it from stack, and prevent onActivityResult() being called
         * by the 3rd app. 
         * If success leads to result page that does 'FLAG_ACTIVITY_CLEAR_TOP', it will erase
         * ALL prior activities from stack anyway. 
         */
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, selectedTrip);
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
    }
    
    public static void doPictureEditActivity(
            final Activity hostingActivityContext,
            Trip trip,
            Picture pictureSelected,
            int pictureSelectedIndex) {
        Intent targetIntent = new Intent(hostingActivityContext, PictureEditActivity.class);
        
        //Prevent the edit page from stack activity:
        targetIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, trip);
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, pictureSelected);
        bundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, pictureSelectedIndex);
        
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
    }
    
    public static void doCommentListByUserActivity(
            final Activity hostingActivityContext,
            User user,
            boolean isAfterDelete,
            String preConfigInfoMessage) {
        Intent targetIntent = new Intent(hostingActivityContext, CommentListActivity.class);;

        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, user.getUserId());
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME, user.getFullname());
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME, user.getUsername());
        bundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL, user.getNumOfComments());
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PAGINATION_IS_SHOW, true);
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, preConfigInfoMessage);

        if(isAfterDelete) {
            //Clears any stack activity prior to this ONLY IF this activity has been previously active (in the stack):
            targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            bundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE, true);
        }
        
        targetIntent.putExtras(bundle);
        hostingActivityContext.startActivity(targetIntent);
    }
    
    public static boolean doSupportFAQActivity(final Activity hostingActivityContext) {
        Intent targetIntent = new Intent(hostingActivityContext, SupportActivity.class);
        
        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE, CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ);
        targetIntent.putExtras(bundle);
        
        hostingActivityContext.startActivity(targetIntent);
        return true;
    }
    
    
}
