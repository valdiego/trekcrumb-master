package com.trekcrumb.android.utility;

import java.lang.reflect.Method;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.activity.HomeActivity;
import com.trekcrumb.android.activity.TripCreateActivity;
import com.trekcrumb.android.activity.TripSearchActivity;
import com.trekcrumb.android.activity.TripSyncActivity;
import com.trekcrumb.android.activity.UserLogoutActivity;
import com.trekcrumb.android.activity.UserSignupActivity;
import com.trekcrumb.android.iconfont.IconFontDrawableImpl;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Util class to manage ActionBar actions.
 *
 */
public class ActionBarUtil {
    private ActionBarUtil() {}
    
    public static boolean createActionBarOptionMenu(
            Context context,
            MenuInflater inflater, 
            Menu menu) {
        inflater.inflate(R.menu.menu_actionbar, menu);
        initIcons(context, menu);
        enableOverflowMenuIcons(menu);
        
        return true;
    }

    /**
     * Readjusts application actionbar menu based on conditions.
     * 1. MenuItem menuHomeID is a bit tricky. It positions BEFORE menuSearchID which is ALWAYS
     *    show up in ActionBar. Since we should not force to have > 2 MenuItems to show up in
     *    ActionBar, we must do the following adjustments:
     *    (a) If no UserLogin, we always show menuHomeID and menuSearchID in that order.
     *    (a) If UserLogin, we keep menuSearchID show up. But we also always show menuUserProfileID, 
     *        so we must hide menuHomeID.
     */
    public static boolean prepareActionBarOptionMenu(Activity hostActivity, Menu menu) {
        boolean isAppInitSuccess = ((TrekcrumbApplicationImpl) hostActivity.getApplicationContext()).isAppInitSuccess();
        if(!isAppInitSuccess) {
            menu.removeItem(R.id.menuSignupID);
        }

        User userLogin = ((TrekcrumbApplicationImpl) hostActivity.getApplicationContext()).getUserLogin();
        if(userLogin == null) {
            //Show:
            MenuItem menuItemHome = menu.findItem(R.id.menuHomeID);
            menuItemHome.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            
            //Remove:
            menu.removeItem(R.id.menuUserProfileID);
            menu.removeItem(R.id.menuTripCreateID);
            menu.removeItem(R.id.menuSyncID);
            menu.removeItem(R.id.menuLogoutID);
            menu.removeItem(R.id.menuAutoLoginRetryID);
        } else {
            //Hide:
            MenuItem menuItemHome = menu.findItem(R.id.menuHomeID);
            menuItemHome.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            
            //Remove:
            menu.removeItem(R.id.menuSignupID);
        }
        
        boolean isUserAuthenticated = ((TrekcrumbApplicationImpl) hostActivity.getApplicationContext()).isUserAuthenticated();
        if(!isUserAuthenticated) {
            menu.removeItem(R.id.menuSyncID);
        } else {
            menu.removeItem(R.id.menuAutoLoginRetryID);
        }
        
        return true;
    }

    /**
     * Executes operations based  application actionbar menu based on conditions.
     */
    public static boolean actionBarOptionItemSelected(Activity hostActivity, MenuItem menuItem) {
        Intent intent;
        switch (menuItem.getItemId()) {
            case R.id.menuHomeID:
                intent = new Intent(hostActivity, HomeActivity.class);
                hostActivity.startActivity(intent);
                return true;

            case R.id.menuSearchID:
                intent = new Intent(hostActivity, TripSearchActivity.class);
                
                Bundle bundle = new Bundle();
                bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PAGINATION_IS_SHOW, true);
                intent.putExtras(bundle);                
                hostActivity.startActivity(intent);
                return true;
                
            case R.id.menuRefreshID:
                if(hostActivity instanceof BaseActivity) {
                    ((BaseActivity)hostActivity).refresh();
                }
                return true;

            case R.id.menuTripCreateID:
                intent = new Intent(hostActivity, TripCreateActivity.class);
                
                //Prevent the activity from stack:
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                hostActivity.startActivity(intent);
                return true;
            
            case R.id.menuUserProfileID:
                /*
                 * Only applicable if UserLogin doesn't have image.
                 * See onUserProfileMenuIconReady()
                 */
                ActivityUtil.doUserLoginViewActivity(hostActivity, false, null);
                return true;
                
            case R.id.menuSyncID:
                intent = new Intent(hostActivity, TripSyncActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                hostActivity.startActivity(intent);
                return true;

            case R.id.menuSignupID:
                intent = new Intent(hostActivity, UserSignupActivity.class);
                
                //Prevent the activity from stack:
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                hostActivity.startActivity(intent);
                return true;
            
            case R.id.menuLogoutID:
                intent = new Intent(hostActivity, UserLogoutActivity.class);
                
                //Prevent the activity from stack:
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                hostActivity.startActivity(intent);
                return true;

            case R.id.menuAutoLoginRetryID:
                SessionUtil.setUserLoginDataInSession(
                        (TrekcrumbApplicationImpl) hostActivity.getApplicationContext(), 
                        null, false, false);
                ActivityUtil.doHomeActivity(hostActivity, false, false, null);
                return true;

            case R.id.menuSupportID:
                ActivityUtil.doSupportFAQActivity(hostActivity);
                return true;

            default:
                //Unknown case
                return false;
        }
    }
    
    /**
     * NOTE:
     * 1. Hack android internal ActionBarImpl class's default attitude that is if the display was
     *    landscape, it would bring all tabs INTO action bar! The key is to call and set its 
     *    method setHasEmbeddedTabs(false). By default, the class always set it to 'true'.
     *    Ref: 
     *    - http://grepcode.com/file_/repository.grepcode.com/java/ext/com.google.android/android/4.0.4_r2.1/com/android/internal/app/ActionBarImpl.java/?v=source
     *    - http://stackoverflow.com/questions/20047686/how-to-display-tabs-below-action-bar
     *    - [INSTALLDIR]\Android\SDK\sources\android-16\com\android\internal\app\ActionBarImpl.java
     *    
     * 2. If the framework uses different ActionBar impl (such as AndroidSherlock's ActionBarWrapper),
     *    this would NOT work! Thus, the wrapper must be extended and look how to call 
     *    its ActionBar.setHasEmbeddedTabs(false).
     */
    public static void disableEmbeddedTabs(ActionBar actionBar) {
        boolean isEnableEmbeddedTab = false;
        try {
            Method setHasEmbeddedTabsMethod = actionBar.getClass().getDeclaredMethod("setHasEmbeddedTabs", boolean.class);
            setHasEmbeddedTabsMethod.setAccessible(true);
            setHasEmbeddedTabsMethod.invoke(actionBar, isEnableEmbeddedTab);
        } catch(NoSuchMethodException nsme) {
            Log.e("ActionBarUtil", "Attempt to disable embedding tabs into ActionBar ERROR: The ActionBar concrete implementation does NOT have method[setHasEmbeddedTabs]!");
        } catch (Exception e) {
            Log.e("ActionBarUtil", "Attempt to disable embedding tabs into ActionBar ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    private static void initIcons(Context context, Menu menu) {
        initUserProfileMenuIcon(context, menu);
       
        MenuItem menuItemHome = menu.findItem(R.id.menuHomeID);
        menuItemHome.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_home));
        
        MenuItem menuItemSearch = menu.findItem(R.id.menuSearchID);
        menuItemSearch.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_search));
        
        MenuItem menuItemRefresh = menu.findItem(R.id.menuRefreshID);
        menuItemRefresh.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_refresh));

        MenuItem menuItemTripCreate = menu.findItem(R.id.menuTripCreateID);
        menuItemTripCreate.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_tripcreate));

        MenuItem menuItemTripSync = menu.findItem(R.id.menuSyncID);
        menuItemTripSync.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_tripsync));

        MenuItem menuItemAutoLoginRetry = menu.findItem(R.id.menuAutoLoginRetryID);
        menuItemAutoLoginRetry.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_autologinretry));

        MenuItem menuItemSignup = menu.findItem(R.id.menuSignupID);
        menuItemSignup.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_login));

        MenuItem menuItemLogout = menu.findItem(R.id.menuLogoutID);
        menuItemLogout.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_logout));
        
        MenuItem menuSupport = menu.findItem(R.id.menuSupportID);
        menuSupport.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_support));
    }
    
    private static void initUserProfileMenuIcon(Context context, Menu menu) {
        MenuItem menuItemUserProfile = menu.findItem(R.id.menuUserProfileID);
        menuItemUserProfile.setIcon(new IconFontDrawableImpl(context, R.drawable.drawable_iconfont_userprofile));
        
        User userLogin = ((TrekcrumbApplicationImpl) context.getApplicationContext()).getUserLogin();
        if(userLogin != null && !ValidationUtil.isEmpty(userLogin.getProfileImageName())) {
            View userImageView = ((TrekcrumbApplicationImpl) context.getApplicationContext()).getUserImageMenuIcon();
            if(userImageView != null) {
                menuItemUserProfile.setActionView(userImageView);
            } else {
                ImageUtil.retrieveUserProfileImageActionBarMenu(context, userLogin.getProfileImageName(), menuItemUserProfile);
            }
        }
    }
    
    /**
     * Callback method when UserLogin's profile image is ready, to be called by an async operation.
     * See more details in layout/template_menuitem_user_image.xml.
     * 
     * @param menuItemUserProfile - Target MenuItem to be populated with the image
     * @param imageBitmap - User profile image
     */
    public static void onUserProfileMenuIconReady(
            final Context context, 
            MenuItem menuItemUserProfile, 
            Bitmap imageBitmap) {
        if(context != null && context instanceof Activity) {
            View userImageView = ((Activity)context).getLayoutInflater().inflate(R.layout.template_menuitem_user_image, null);
            ((ImageView)userImageView.findViewById(R.id.imageViewID)).setImageBitmap(imageBitmap);
            userImageView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityUtil.doUserLoginViewActivity((Activity)context, false, null);
                        }
                    }
            );
            
            //Store for in-session usage:
            ((TrekcrumbApplicationImpl) context.getApplicationContext()).setUserImageMenuIcon(userImageView);
            
            //Populate icon:
            menuItemUserProfile.setActionView(userImageView);
        }
    }

   /**
    * NOTE:
    * 1. A hack method to enforce the ActionBar Overflow Menu to display their icons. By default,
    *    overflow menus do not show icons, but only if they are showing in ActionBar. If the hacking
    *    attempt failed, keep going without any icons to show.
    * 2. Must be invoked during Activity's onCreateOptionsMenu() or onMenuOpened() callback.
    * 
    * @see drawable-mdpi/a_read_me.txt
    */
   private static void enableOverflowMenuIcons(Menu menu) {
       if(menu.getClass().getSimpleName().equals("MenuBuilder")){
           try {
               Method methodFound = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
               if(methodFound != null) {
                   methodFound.setAccessible(true);
                   methodFound.invoke(menu, true);
               }
           } catch(Exception e) {
               /*
                * It is possible for newer Android OS versions, the targeted method 'setOptionalIconsVisible'
                * is no longer supported.
                */
               Log.e("ActionBarUtil", "enableOverflowMenuIcons() - Attempt to enable Overflow Menu icons FAILED: " + e.getMessage(), e);
           }
       }
   }    
   

    
}
