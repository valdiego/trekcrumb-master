package com.trekcrumb.android.utility;

import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.os.Environment;

public class AndroidConstants {
    //Device specifics:
    public static final String DEVICE_SDCARD_DIR_MAIN;
    public static final String DEVICE_IDENTITY;
    public static final int DEVICE_HEAP_MEMORY_MAX;
    static {
        if(Environment.getExternalStorageDirectory() == null) {
            DEVICE_SDCARD_DIR_MAIN = null;
        } else {
            DEVICE_SDCARD_DIR_MAIN = Environment.getExternalStorageDirectory().toString();
            /*
             * TODO: Is this better way to get the default path?
             * if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
             *         || !isExternalStorageRemovable()) {
             *     String cachePath = getExternalCacheDir(context).getPath();
             * } else {
             *     String cachePath = context.getCacheDir().getPath();
             * }
             * 
             */
     
        }
        DEVICE_IDENTITY = DeviceUtil.determineDeviceIdentity();
        DEVICE_HEAP_MEMORY_MAX = (int) (Runtime.getRuntime().maxMemory() / 1024);
    }
    
    //App Specifics: 
    public static final String APP_NAME = "com.trekcrumb.android";
    public static final String APP_ANDROID_ACCOUNT_KEY = "AIzaSyAj_K93rojLdPnVMcxm-0_wdPbHa-euuWc";
    public static final String APP_LOCAL_DIR_PICTURES = DEVICE_SDCARD_DIR_MAIN + "/Trekcrumb/TrekcrumbPics/";
    public static final String APP_LOCAL_DIR_CACHE = DEVICE_SDCARD_DIR_MAIN + "/Trekcrumb/TrekcrumbCaches/";
    
    //Icon Fonts:
    public static final String APP_ICONFONT_TFF_FILENAME = "fontawesome-webfont.ttf";
    public static final String APP_ICONFONT_TFF_FILEFULLPATH = "/assets/" + APP_ICONFONT_TFF_FILENAME;

    //Database:
    public static final String APP_DATABASE_NAME = "TrekcrumbDB.sqlite";
    public static final String APP_DATABASE_NAME_BACKUP = "TrekcrumbDB_UpgradeBackup.sqlite";
    public static final String APP_DATABASE_SOURCE_FILENAME = "TrekcrumbDB.sqlite";
    //public static final String APP_DATABASE_SOURCE_SQL_FILENAME = "trekcrumbDBSql.sql";
    public static final String APP_DATABASE_SOURCE_SQL_FILENAME = "trekcrumbDBSql_OneLine.sql";
    
    //Maps (distance values are in KM):
    public static final int MAP_DEFAULT_ZOOM_CLOSE_UP = 17;
    //public static final int MAP_POLYLINE_WIDTH = 7; //pixels
    public static final int MAP_POLYLINE_WIDTH = 5; //pixels
    public static final String MAP_MARKER_CURRENT_LOC_KEY = "CURRENT_LOC";
    public static final int MAP_RADIUS_EARTH_KM = 6371; 
    public static final int MAP_POLYLINE_DISTANCE_NONE_VALUE_KM = 1;
    public static final int MAP_POLYLINE_DISTANCE_VERYSMALL_VALUE_KM = 5;
    public static final int MAP_POLYLINE_DISTANCE_SMALL_VALUE_KM = 10;
    public static final int MAP_POLYLINE_DISTANCE_MEDIUM_VALUE_KM = 25;
    public static final int MAP_POLYLINE_DISTANCE_LARGE_VALUE_KM = 100;
    public static final int MAP_POLYLINE_DISTANCE_VERYLARGE_VALUE_KM = 500;
    public static final int MAP_POLYLINE_DISTANCE_NONE_NUMOFDASHLINES = 1;
    public static final int MAP_POLYLINE_DISTANCE_VERYSMALL_NUMOFDASHLINES = 2;
    public static final int MAP_POLYLINE_DISTANCE_SMALL_NUMOFDASHLINES = 3;
    public static final int MAP_POLYLINE_DISTANCE_MEDIUM_NUMOFDASHLINES = 5;
    public static final int MAP_POLYLINE_DISTANCE_LARGE_NUMOFDASHLINES = 7;
    public static final int MAP_POLYLINE_DISTANCE_VERYLARGE_NUMOFDASHLINES = 10;
    public static final int MAP_POLYLINE_DISTANCE_INFINITE_NUMOFDASHLINES = 15;

    /*
     * Pictures
     * Note: Other picture constants are in CommonConstants.java and SingletonFactory.java
     */
    public static final CompressFormat PICTURE_FORMAT = CompressFormat.JPEG;
    public static final Config PICTURE_IMAGE_PIXEL_CONFIG = Config.RGB_565; //less quality than default ARGB_8888.
                                                                            //stores only 2 bytes/px instead of 4 bytes/px
    public static final int PICTURE_COMPRESS_QUALITY_100 = 100;
    public static final int PICTURE_COMPRESS_QUALITY_75 = 75;
    public static final int PICTURE_DOWNLOAD_THREAD_MAX = 3;
    
}
