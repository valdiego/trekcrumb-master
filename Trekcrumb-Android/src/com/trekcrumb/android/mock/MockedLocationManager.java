package com.trekcrumb.android.mock;

import android.app.Activity;
import android.location.Location;
import android.util.Log;

import com.trekcrumb.android.listener.IGeoLocationUpdateListener;

/**
 * A dummy LocationManager to return a dummy current GPS location.
 * 
 * This mocked class is a thread such that a calling code (an activity or a utility) can invoke its
 * start(), and then proceed to trigger a ProgressWaitThread per business as usual to wait for the result.
 * When the mocked class is done in its dummy delay, it returns a dummy location.
 * 
 * @author Val Triadi
 *
 */
public class MockedLocationManager extends Thread {
    private static final String LOG_TAG = "MockedLocationManager";
    
    private Activity mActivity;
    private IGeoLocationUpdateListener mGeoLocationUpdateListener;
    
    /** 
     * Creates a new thread of MockedLocationManager.
     */
    public MockedLocationManager(
            final Activity activity,
            final IGeoLocationUpdateListener listener) {
        super("MockedLocationManager");
        mActivity = activity;
        mGeoLocationUpdateListener = listener;
    }

    @Override
    public void run() {
        Log.i(LOG_TAG, "run() - Starting to get ***MOCKED*** GPS current location ... ");
        try {
            Thread.sleep(2000);
        } catch(Exception e) { 
            //None
        }
        
        Log.i(LOG_TAG, "run() - Returning ***MOCKED*** GPS current location ... ");
        final Location mockedLocation = MockedResources.getLocation();
        if(mActivity != null) {
            //Must run on the calling MAIN UI thread:
            mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    if(mGeoLocationUpdateListener != null) {
                        mGeoLocationUpdateListener.onGeoLocationUpdateCallback(mockedLocation);
                        
                    } else {
                        StringBuilder errMsgBld = new StringBuilder();
                        errMsgBld.append("run() - ");
                        errMsgBld.append("This LocationManager [" + this + "] ");
                        errMsgBld.append("has NULL IGeoLocationUpdateListener! Cannot notify the hosting activity/fragment that the location [");
                        errMsgBld.append(mockedLocation);
                        errMsgBld.append("] is ready!");
                        Log.e(LOG_TAG, errMsgBld.toString());
                    }
                }
            });
        }
    }

}
