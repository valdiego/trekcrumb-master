package com.trekcrumb.android.mock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.location.Location;

import com.trekcrumb.android.listener.IGeoLocationUpdateListener;
import com.trekcrumb.common.bean.MessageBoard;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripStatusEnum;

@SuppressWarnings({ "deprecation" })
public class MockedResources {
    
    public static User getUser() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("bbark2034");
        user.setFullname("Bark Badabing");
        //TODO: user.setProfileIcon(profileIcon);
        user.setLocation("Southern California, USA");
        user.setSummary("Little town cruise enthusiasts. Love share little nice stories along the way. Or just lazy weekends with my pup Dodo. That it is 140 chars.");
        return user;
    }
    
    public static Trip getTrip(final User user) {
        Trip trip = new Trip();
        trip.setId("1");
        trip.setName("Trek for Sunday on Beachy Beach");
        trip.setStatus(TripStatusEnum.ACTIVE);
        trip.setPrivacy(TripPrivacyEnum.PUBLIC);
        trip.setNote("Having good time sunday afternoon for a short hike along the trek. We hiked not far from ocean front. Watched sunset afterwards. 140 chars.");
        
        Calendar cal = Calendar.getInstance();
        trip.setCreated(cal.getTime().toLocaleString());
        trip.setCompleted(cal.getTime().toLocaleString());
        trip.setUpdated(cal.getTime().toLocaleString());
        
        List<Place> placesList = new ArrayList<Place>();
        placesList.add(getPlace());
        trip.setListOfPlaces(placesList);
        
        return trip;
    }    
    
    public static List<Trip> getTrips() {
        List<Trip> tripsList = new ArrayList<Trip>();
        for(int i = 0; i < 3; i++) {
            Trip trip = new Trip();
            trip.setId(String.valueOf(i));
            trip.setName("Trek for the Day of " + trip.getId());
            if(i==0) {
                trip.setStatus(TripStatusEnum.ACTIVE);
            } else {
                trip.setStatus(TripStatusEnum.COMPLETED);
            }
            trip.setPrivacy(TripPrivacyEnum.PUBLIC);
            trip.setNote("Having good time on day " + i + " afternoon.");
            
            Calendar cal = Calendar.getInstance();
            trip.setCreated(cal.getTime().toLocaleString());
            trip.setCompleted(cal.getTime().toLocaleString());
            trip.setUpdated(cal.getTime().toLocaleString());
            
            trip.setListOfPlaces(getPlaces());
            tripsList.add(trip);
        }
        return tripsList;
    }    
    
    private static Place getPlace() {
        Place place = new Place();
        place.setId("1");
        Location location = getLocation();
        place.setLatitude(location.getLatitude());
        place.setLongitude(location.getLongitude());
        return place;
    }

    private static List<Place> getPlaces() {
        List<Place> placesList = new ArrayList<Place>();
        double[] longitude = {32.76243, 32.76166, 32.75718, 32.75142, 32.74866, 32.74839};
        double[] latitude = {-117.14818, -117.14629, -117.15129, -117.15112, -117.15513, -117.16260};
        for(int i = 0; i < 6; i++) {
            Place place = new Place();
            place.setId(String.valueOf(i));
            place.setLatitude(latitude[i]);
            place.setLongitude(longitude[i]);
            place.setNote("Im here on this place " + i);
            placesList.add(place);
        }
        return placesList;
    }
    
    public static List<MessageBoard> getMessageBoards() {
        List<MessageBoard> listOfMessageBoards = new ArrayList<MessageBoard>();
        Calendar cal = Calendar.getInstance();
        String[] messagesArray = {
                "You created new trip: Enjoying Summer on La Jolla Beach 12/12/2012",
                "You created new trip: Hiking Mt. Vernon and see Snowww ...!",
                "User Abelgie added comment on your trip: Enjoying Summer on La Jolla Beach 12/12/2012",
                "User SoAndSo added comment on your trip: Enjoying Summer on La Jolla Beach 12/12/2012",
                "You added comment on Abelgie trip: Sunbathing on Carribean Bitches Beach!",
                "User SoAndSo favorited your trip: Stumbled Upon Rocky Mountain"
        };
        for(int i=0; i<messagesArray.length; i++) {
            MessageBoard msg = new MessageBoard();
            msg.setId(String.valueOf(i));
            //msg.setUserID("TODO");
            msg.setMessage(messagesArray[i]);
            msg.setCreatedDate(cal.getTime());
            listOfMessageBoards.add(msg);
        }
        return listOfMessageBoards;
    }
    
    public static void findCurrentLocation(
            final Activity hostActivity, 
            final IGeoLocationUpdateListener listener) {
        MockedLocationManager locationManagerMocked = new MockedLocationManager(hostActivity, listener);
        locationManagerMocked.start();
    }
    
    public static Location getLocation() {
        Location location = new Location("DumbButSmartProvider");
        location.setLatitude(32.76243);
        location.setLongitude(-117.14818);
        //location.setAltitude(todo);
        return location;
    }
    
}
