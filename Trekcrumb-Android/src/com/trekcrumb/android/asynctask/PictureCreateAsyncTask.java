package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.business.PictureManager;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.TripPublishEnum;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class PictureCreateAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private BaseActivity mHostActivity;
    private Picture mPictureToCreate;
    private boolean mIsLoadByCamera;
    private TripPublishEnum mIsPublishOrNot;
    
    public PictureCreateAsyncTask(
            final BaseActivity hostingActivity,
            final Picture pictureToCreate,
            boolean isNewPicByCamera,
            TripPublishEnum isPublishOrNot) {
        super();
        mHostActivity = hostingActivity;
        mPictureToCreate = pictureToCreate;
        mIsLoadByCamera = isNewPicByCamera;
        mIsPublishOrNot = isPublishOrNot;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        return PictureManager.create(
                mHostActivity, mPictureToCreate, mIsLoadByCamera, mIsPublishOrNot);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        mHostActivity.onAsyncBackendTaskCompleted(result);
    }
}        
