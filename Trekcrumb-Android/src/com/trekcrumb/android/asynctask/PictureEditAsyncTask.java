package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.business.PictureManager;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPublishEnum;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class PictureEditAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private BaseActivity mHostActivity;
    private Picture mPictureToUpdate;
    private boolean mIsDeletePicture;
    private TripPublishEnum mTripPublishStatus;
    
    public PictureEditAsyncTask(
            BaseActivity hostingActivity,
            Picture pictureToUpdate,
            boolean isDeletePic,
            TripPublishEnum tripPublishStatus) {
        super();
        mHostActivity = hostingActivity;
        mPictureToUpdate = pictureToUpdate;
        mTripPublishStatus = tripPublishStatus;
        mIsDeletePicture = isDeletePic;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        if(mIsDeletePicture) {
            return PictureManager.delete(
                    mHostActivity, 
                    mPictureToUpdate, 
                    ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID,
                    mTripPublishStatus);
        } else {
            return PictureManager.update(mHostActivity, mPictureToUpdate, mTripPublishStatus);
        }
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        mHostActivity.onAsyncBackendTaskCompleted(result);
    }
}        
