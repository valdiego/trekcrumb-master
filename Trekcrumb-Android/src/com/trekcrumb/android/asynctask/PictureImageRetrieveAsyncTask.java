package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.activity.PictureEditActivity;
import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.business.PictureManager;
import com.trekcrumb.android.fragment.BaseFragment;
import com.trekcrumb.android.fragment.PictureCoverFragment;
import com.trekcrumb.android.fragment.PictureDetailFragment;
import com.trekcrumb.android.fragment.PictureGalleryFragment;
import com.trekcrumb.common.bean.Picture;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class PictureImageRetrieveAsyncTask extends AsyncTask<Void, Void, ImageCacheBean> {
    private BaseActivity mHostActivity;
    private BaseFragment mHostFragment;
    private boolean mIsUserLoginTrip;
    private Picture mPictureToRead;
    private boolean mIsThumbnail;
    private int mThumbnailImageIndex;
    
    public PictureImageRetrieveAsyncTask(
            final BaseActivity hostActivity,
            final Picture pictureToRead,
            boolean isUserLoginTrip,
            boolean isThumbnail,
            int imageIndex) {
        super();
        mHostActivity = hostActivity;
        initProperties(pictureToRead, isUserLoginTrip, isThumbnail, imageIndex);
    }

    public PictureImageRetrieveAsyncTask(
            final BaseFragment hostingFragment,
            final Picture pictureToRead,
            boolean isUserLoginTrip,
            boolean isThumbnail,
            int imageIndex) {
        super();
        mHostFragment = hostingFragment;
        initProperties(pictureToRead, isUserLoginTrip, isThumbnail, imageIndex);
    }
    
    private void initProperties(
            final Picture pictureToRead,
            boolean isUserLoginTrip,
            boolean isThumbnail,
            int imageIndex) {
        mPictureToRead = pictureToRead;
        mIsUserLoginTrip = isUserLoginTrip;
        mIsThumbnail = isThumbnail;
        mThumbnailImageIndex = imageIndex;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ImageCacheBean doInBackground(Void... none) {
        if(mHostActivity != null) {
            return PictureManager.retrieveImage(
                    mHostActivity, mPictureToRead, mIsUserLoginTrip, mIsThumbnail);
        }
        
        return PictureManager.retrieveImage(
                mHostFragment.getActivity(), mPictureToRead, mIsUserLoginTrip, mIsThumbnail);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ImageCacheBean result) {
        if(mHostActivity != null) {
            if(mHostActivity instanceof PictureEditActivity) {
                ((PictureEditActivity)mHostActivity).onPictureImageDetailReadyCallback(result);
            }
        }
        
        //Else:
        if(mHostFragment instanceof PictureDetailFragment) {
            ((PictureDetailFragment)mHostFragment).onPictureImageDetailReadyCallback(result);
        
        } else if(mHostFragment instanceof PictureGalleryFragment) {
            ((PictureGalleryFragment)mHostFragment).onPictureImageGalleryReadyCallback(result, mThumbnailImageIndex);
        
        } else if(mHostFragment instanceof PictureCoverFragment) {
            ((PictureCoverFragment)mHostFragment).onPictureCoverReadyCallback(result);
        }
    }
    
}        
