package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.business.UserManager;
import com.trekcrumb.android.fragment.BaseFragment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class UserCreateAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseActivity mHostActivity;
    private final BaseFragment mHostFragment;
    private final User mUserToCreate;
    private final UserAuthToken mUserAuthTokenToCreate;
    
    public UserCreateAsyncTask(
            BaseActivity hostingActivity,
            BaseFragment hostingFragment,
            User userToCreate,
            UserAuthToken userAuthTokenToCreate) {
        super();
        mHostActivity = hostingActivity;
        mHostFragment = hostingFragment;
        mUserToCreate = userToCreate;
        mUserAuthTokenToCreate = userAuthTokenToCreate;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        return UserManager.userCreateOnServer(mHostActivity, mUserToCreate, mUserAuthTokenToCreate);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        if(mHostFragment != null) {
            mHostFragment.onAsyncBackendTaskCompleted(result);
        } else {
            mHostActivity.onAsyncBackendTaskCompleted(result);
        }
    }
}        
