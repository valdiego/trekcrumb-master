package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.business.PlaceManager;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPublishEnum;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class PlaceEditAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseActivity mHostActivity;
    private final Place mPlaceToUpdate;
    private boolean mIsDelete;
    private TripPublishEnum mTripPublishStatus;
    
    public PlaceEditAsyncTask(
            BaseActivity hostingActivity,
            Place updatedPlaceBean,
            boolean isDelete,
            TripPublishEnum tripPublishStatus) {
        super();
        mHostActivity = hostingActivity;
        mPlaceToUpdate = updatedPlaceBean;
        mIsDelete = isDelete;
        mTripPublishStatus = tripPublishStatus;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        if(mIsDelete) {
            return PlaceManager.delete(
                    mHostActivity, mPlaceToUpdate, 
                    ServiceTypeEnum.PLACE_DELETE_BY_PLACEID, mTripPublishStatus);
        } else {
            return PlaceManager.update(
                    mHostActivity, mPlaceToUpdate, mTripPublishStatus);
        }
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        mHostActivity.onAsyncBackendTaskCompleted(result);
    }
}        
