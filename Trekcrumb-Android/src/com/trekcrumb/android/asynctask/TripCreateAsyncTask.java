package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class TripCreateAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseActivity mHostActivity;
    private final Trip mTripToCreate;
    
    public TripCreateAsyncTask(
            BaseActivity hostingActivity,
            Trip newTrip) {
        super();
        mHostActivity = hostingActivity;
        mTripToCreate = newTrip;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        return TripManager.create(mHostActivity, mTripToCreate);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        mHostActivity.onAsyncBackendTaskCompleted(result);
    }
}        
