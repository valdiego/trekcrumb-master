package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.business.UserManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class UserEditAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseActivity mHostActivity;
    private final User mUserToUpdate;
    private boolean mIsDeactivateUser;
    private boolean mIsReactivateUser;
    private boolean mIsNewPicByCamera;
    
    public UserEditAsyncTask(
            BaseActivity hostingActivity,
            User user,
            boolean isDeactivateUser,
            boolean isReactivateUser,
            boolean isNewPicByCamera) {
        super();
        mHostActivity = hostingActivity;
        mUserToUpdate = user;
        mIsDeactivateUser = isDeactivateUser;
        mIsReactivateUser = isReactivateUser;
        mIsNewPicByCamera = isNewPicByCamera;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        if(mIsDeactivateUser) {
            return UserManager.userDeactivate(mHostActivity, mUserToUpdate);
            
        } else if(mIsReactivateUser) {
            return UserManager.userReactivate(mHostActivity, mUserToUpdate);
            
        } else {
            return UserManager.userUpdate(mHostActivity, mUserToUpdate, mIsNewPicByCamera);
        }
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        mHostActivity.onAsyncBackendTaskCompleted(result);
    }
}        
