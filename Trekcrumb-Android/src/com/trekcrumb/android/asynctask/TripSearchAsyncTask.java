package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseListActivity;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.TripSearchCriteria;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class TripSearchAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseListActivity mHostActivity;
    private TripSearchCriteria mSearchCriteria;
    private String mCacheKey;
    
    public TripSearchAsyncTask(
            BaseListActivity hostingActivity,
            TripSearchCriteria searchCriteria,
            String cacheKey) {
        super();
        mHostActivity = hostingActivity;
        mSearchCriteria = searchCriteria;
        mCacheKey = cacheKey;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        return TripManager.search(mSearchCriteria);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        mHostActivity.onAsyncBackendTaskCompleted(result, mCacheKey);
    }
}        
