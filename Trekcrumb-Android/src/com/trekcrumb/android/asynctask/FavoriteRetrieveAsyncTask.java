package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.activity.BaseListActivity;
import com.trekcrumb.android.business.FavoriteManager;
import com.trekcrumb.android.fragment.BaseFragment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class FavoriteRetrieveAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseActivity mHostActivity;
    private final BaseFragment mHostFragment;
    private Trip mTrip;
    private User mUser;
    private ServiceTypeEnum mRetrieveBy;
    private int mOffset;
    private String mCacheKey;

    public FavoriteRetrieveAsyncTask(
            BaseActivity hostingActivity,
            BaseFragment hostingFragment,
            Trip trip,
            User user,
            ServiceTypeEnum retrieveBy,
            int offset,
            String cacheKey) {
        super();
        mHostActivity = hostingActivity;
        mHostFragment = hostingFragment;
        mTrip = trip;
        mUser = user;
        mRetrieveBy = retrieveBy;
        mOffset = offset;
        mCacheKey = cacheKey;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        if(mHostFragment != null) {
            return FavoriteManager.retrieve(mHostFragment.getActivity(), mTrip, mUser, mRetrieveBy, mOffset);
        } else {
            return FavoriteManager.retrieve(mHostActivity, mTrip, mUser, mRetrieveBy, mOffset);
        }
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        if(mHostFragment != null) {
            mHostFragment.onAsyncBackendTaskCompleted(result);
            
        } else if(mHostActivity instanceof BaseListActivity) {
            ((BaseListActivity)mHostActivity).onAsyncBackendTaskCompleted(result, mCacheKey);
            
        } else {
            mHostActivity.onAsyncBackendTaskCompleted(result);
        }
    }
}        
