package com.trekcrumb.android.asynctask;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.trekcrumb.android.business.PictureManager;
import com.trekcrumb.android.utility.ActionBarUtil;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class PictureProfileImageRetrieveAsyncTask extends AsyncTask<Void, Void, Bitmap> {
    private Context mContext;
    private String mProfileImageName;
    private View mProfileImageDefaultView;
    private ImageView mProfileImageView;
    private MenuItem mProfileMenuItem;    
    
    private boolean mIsUserLoginData;
    private boolean mIsThumbnail;
    
    public PictureProfileImageRetrieveAsyncTask(
            final Context context,
            String profileImageName,
            final View userProfileDefaultView,
            final ImageView profileImageView,
            final MenuItem menuItem,
            boolean isUserLoginData,
            boolean isThumbnail) {
        super();
        mContext = context;
        
        mProfileImageName = profileImageName;
        mProfileImageDefaultView = userProfileDefaultView;
        mProfileImageView = profileImageView;
        mProfileMenuItem = menuItem;
        
        mIsUserLoginData = isUserLoginData;
        mIsThumbnail = isThumbnail;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected Bitmap doInBackground(Void... none) {
        return PictureManager.retrieveProfileImage(
                mContext, mProfileImageName, mIsUserLoginData, mIsThumbnail);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(Bitmap result) {
        if(result != null) {
            if(mProfileImageDefaultView != null) {
                mProfileImageDefaultView.setVisibility(View.GONE);
            }
            
            if(mProfileImageView != null) {
                //Populate target ImageView:
                mProfileImageView.setVisibility(View.VISIBLE);
                mProfileImageView.setImageBitmap(result);
                
            } else if(mProfileMenuItem != null) {
                ActionBarUtil.onUserProfileMenuIconReady(mContext, mProfileMenuItem, result);
            }
        }
    }
}        
