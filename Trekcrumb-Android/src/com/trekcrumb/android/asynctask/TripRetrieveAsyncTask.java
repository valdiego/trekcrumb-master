package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.activity.BaseListActivity;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class TripRetrieveAsyncTask extends AsyncTask<Void, Void, ServiceResponse> {
    private final BaseActivity mHostActivity;
    private Trip mTrip;
    private ServiceTypeEnum mRetrieveBy;
    private boolean mIsUserLoginProfile;
    private int mOffset;
    private int mNumOfRows;
    private OrderByEnum mOrderBy;
    private String mCacheKey;

    public TripRetrieveAsyncTask(
            BaseActivity hostingActivity,
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            boolean isUserLoginProfile,
            int offset,
            int numOfRows,
            OrderByEnum orderBy,
            String cacheKey) {
        super();
        
        mHostActivity = hostingActivity;
        mTrip = tripToRetrieve;
        mRetrieveBy = retrieveBy;
        mIsUserLoginProfile = isUserLoginProfile;
        mOffset = offset;
        mNumOfRows = numOfRows;
        if(orderBy != null) {
            mOrderBy = orderBy;
        } else {
            mOrderBy = OrderByEnum.ORDER_BY_CREATED_DATE;
        }
        mCacheKey = cacheKey;
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    @Override
    protected ServiceResponse doInBackground(Void... none) {
        return TripManager.retrieve(mHostActivity, mTrip, mIsUserLoginProfile, mRetrieveBy, mOrderBy, mOffset, mNumOfRows);
    }

    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    @Override
    protected void onPostExecute(ServiceResponse result) {
        if(mHostActivity instanceof BaseListActivity) {
            ((BaseListActivity) mHostActivity).onAsyncBackendTaskCompleted(result, mCacheKey);
        } else {
            mHostActivity.onAsyncBackendTaskCompleted(result);
        }
    }
}        
