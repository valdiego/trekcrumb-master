package com.trekcrumb.android.asynctask;

import android.os.AsyncTask;

/**
 * Concrete implementation must define: AsyncTask<Params, Progress, Result>
 * Params = The type of the parameters sent to the task upon execution
 * Progress = The type of the progress units published during the background computation
 * Result = The type of the result of the background computation
 * 
 */
public class TemplateAsyncTask extends AsyncTask<String, Integer, Boolean> {

    /**
     * 1st step: invoked on the UI thread immediately after the task is executed on the UI
     * thread. This may include initialization.
     */
    @Override
    protected void onPreExecute() {
    }
    
    /**
     * 2nd step: Invoked on the background thread immediately after onPreExecute() finishes.
     * @param map - This must be the type of 'Params' on class definition.
     * @return Return type must be the type of 'Result' on classs definiton.
     */
    protected Boolean doInBackground(String... inputParamArray) {
        
        return null;
    }
    
    /**
     * Invoked if any call to publishProgress(Progress).
     * @param progress - This must be the type of 'Progress' on class definition.
     */
    protected void onProgressUpdate(Integer progress) {
        //Not implemented
    }
    
    /**
     * Last step: Invoked on the UI thread after the background computation finishes.
     */
    protected void onPostExecute(Boolean result) {
        //Not implemented
    }
    


}
