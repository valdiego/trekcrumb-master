package com.trekcrumb.android.iconfont;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View.BaseSavedState;

public class IconFontDrawableSavedState extends BaseSavedState {
    
    public static final Parcelable.Creator<IconFontDrawableSavedState> CREATOR = new Parcelable.Creator<IconFontDrawableSavedState>() {
        public IconFontDrawableSavedState createFromParcel(Parcel in) {
            return new IconFontDrawableSavedState(in);
        }

        public IconFontDrawableSavedState[] newArray(int size) {
            return new IconFontDrawableSavedState[size];
        }
    };

    private String mText;
    private int mTextColor;
    private float mTextSize;

    public IconFontDrawableSavedState(Parcelable parcelable) {
        super(parcelable);
    }

    public IconFontDrawableSavedState(Parcel parcel) {
        super(parcel);

        mText = parcel.readString();
        mTextSize = parcel.readFloat();
        mTextColor = parcel.readInt();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        super.writeToParcel(parcel, flags);
    }
    
    public void setSavedStateProperties(String text, float textSize, int textColor) {
        mText = text;
        mTextSize = textSize;
        mTextColor = textColor;
    }

    public String getText() {
        return mText;
    }

    public int getTextColor() {
        return mTextColor;
    }

    public float getTextSize() {
        return mTextSize;
    }


}