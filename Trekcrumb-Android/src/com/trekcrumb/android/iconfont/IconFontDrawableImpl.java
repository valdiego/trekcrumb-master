package com.trekcrumb.android.iconfont;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.AbsSavedState;

import com.trekcrumb.android.R;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Custom implementation of Drawable that enables an 'Icon Font' as a drawable object.
 * 
 * Drawable Properties: Characteristics that must be set either programatically via setProperties() 
 * or in drawable XML definition. If not set, then default values will be used.
 * 
 * @author Val Triadi
 * @since 02/2017
 * @see /drawable-mdpi/a_read_me.txt
 */
public class IconFontDrawableImpl extends Drawable {
    private static final String LOG_TAG = "IconFontDrawableImpl";
    private static final String ICONFONT_DRAWABLE_XML_ROOT_ELEMENT = "iconfont";
    
    //The iconfont symbol to draw
    private String mText;

    //Paint used for the iconfont text and holds most drawing primitives
    private TextPaint mTextPaint;

    //Container representing the text bounds (x,y) to be reported to widgets
    private Rect mTextBoundRectangle;
    
    //Layout is used to measure and draw the text
    private StaticLayout mTextLayout;
    
    /*
     * Default constructor. Hidden.
     * Note:
     * 1. DO NOT set mTextPaint.setTextAlign() to anything because it would ruin the alignment!
     * 2. Important to set the Typeface making the iconfont work.
     */
    private IconFontDrawableImpl(Context context) {
        mTextBoundRectangle = new Rect();
        mTextPaint = new TextPaint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setDither(true);
        mTextPaint.setTypeface(IconFontTypefaceHolder.getTypeface(context.getAssets()));
        mTextPaint.density = context.getResources().getDisplayMetrics().density;
    }
    
    /**
     * Constructs a new IconFontDrawableImpl with explicit input parameters, without any XML definition.
     */
    public IconFontDrawableImpl(Context context, String text, float textSize, int textColor) {
        this(context);
        setProperties(text, textSize, textColor);
    }

    /**
     * Constructs a new IconFontDrawableImpl with drawable XML defnitions whose valid root element
     * must be '<iconfont>'. The XML should have all the properties required. Otherwise, default
     * values would be assigned or you can programatically call its setter() methods.
     * 
     * @param context
     * @param xmlID - The R' drawable XML definition ID
     */
    public IconFontDrawableImpl(Context context, int xmlID) {
        this(context);

        //Parse drawable XML definition:
        XmlResourceParser xmlParser = null;
        try {
            xmlParser = context.getResources().getXml(xmlID);
            if (xmlParser == null) {
                Log.e(LOG_TAG, "IconFontDrawableImpl() - ERROR: Failure to parse XML schema - [xmlID] NOT FOUND! Assign IconFont properties to default.");
                setPropertiesDefault();
                return;
            }

            int elementType;
            while ((elementType = xmlParser.next()) != XmlPullParser.END_DOCUMENT) {
                if (elementType != XmlPullParser.START_TAG) {
                    continue;
                }
                
                String rootElementName = xmlParser.getName();
                if (ICONFONT_DRAWABLE_XML_ROOT_ELEMENT.equals(rootElementName)) {
                    setPropertiesFromXML(context, xmlParser);
                } else {
                    Log.e(LOG_TAG, "IconFontDrawableImpl() - ERROR: Invalid root element name from drawable XML definition: [" + rootElementName + "]! Assign IconFont properties to default.");
                    setPropertiesDefault();
                }
            }
            
        } catch(Exception e) {
            Log.e(LOG_TAG, "IconFontDrawableImpl() - ERROR: Unexpected exception while parsing XML definition! Assign IconFont properties to default.");
            e.printStackTrace();
            setPropertiesDefault();
        } finally {
            xmlParser = null;
        }
    }   
    
    public Parcelable onSaveInstanceState() {
        IconFontDrawableSavedState savedState = new IconFontDrawableSavedState(AbsSavedState.EMPTY_STATE);
        savedState.setSavedStateProperties(mText, mTextPaint.getTextSize(), mTextPaint.getColor());
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof IconFontDrawableSavedState) {
            IconFontDrawableSavedState savedState = (IconFontDrawableSavedState) parcelable;
            setProperties(savedState.getText(), savedState.getTextSize(), savedState.getTextColor());
        }
    }
    
    /*
     * Set drawable properties with default values.
     */
    private void setPropertiesDefault() {
        setProperties(null, -1, Color.BLACK);
    }
    
    /*
     * Set drawable properties from XML definition's AttributeSet. If some properties are not
     * defined, default value would be assigned.
     */
    private void setPropertiesFromXML(Context context, final XmlResourceParser xmlParser) {
        AttributeSet attributes = Xml.asAttributeSet(xmlParser);
        TypedArray typedArray = context.obtainStyledAttributes(attributes, R.styleable.IconFontDrawableImpl);
        if (typedArray == null) {
            Log.e(LOG_TAG, "IconFontDrawableImpl() - ERROR: Failed reading or NO drawable attributes found in XML definition! Assign IconFont properties to default!");
            setPropertiesDefault();
            return;
        }

        //Else: 
        try {
            String text = typedArray.getString(R.styleable.IconFontDrawableImpl_text);
            float textSize = typedArray.getDimension(R.styleable.IconFontDrawableImpl_textSize, 12f);

            int color = Color.BLACK;
            ColorStateList colorStateList = typedArray.getColorStateList(R.styleable.IconFontDrawableImpl_textColor);
            if (colorStateList != null) {
                color = colorStateList.getColorForState(getState(), Color.BLACK);
            }
            
            setProperties(text, textSize, color);
        } finally {
            //TypedArray objects are a shared resource and must be recycled after use.
            typedArray.recycle();
        }
    }
    
    /*
     * Sets the drawable properties with valid values. If property value is invalid (see below),
     * it will be set with default values.
     * 
     * @param text - If empty, set to default value.
     * @param textSize - If value is <= 0, set to default size.
     * @param textColor - The int value (negative/positive) of android.graphics.Color. 
     *                    A value of -1 is Color.WHITE, 0 is Color.TRANSPARENT and -16777216 is Color.BLACK
    */
    private void setProperties(String text, float textSize, int textColor) {
        if(!ValidationUtil.isEmpty(text)) {
            mText = text;
        } else {
            mText = "X";
        }
        
        if(textSize <= 0) {
            textSize = 12f;
        }
        mTextPaint.setTextSize(textSize);
        
        mTextPaint.setColor(textColor);
        
        updateBounds();
    }
    
    /*
     * Takes measurements of the current contents and apply the correct bounds when possible.
     */
    private void updateBounds() {
        double desired = Math.ceil( Layout.getDesiredWidth(mText, mTextPaint) );
        mTextLayout = new StaticLayout(mText, mTextPaint, (int) desired,
                                       Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
        mTextBoundRectangle.set(0, 0, mTextLayout.getWidth(), mTextLayout.getHeight());

        //Update bound:
        setBounds(mTextBoundRectangle);
        
        //Force a redrawn
        invalidateSelf();
    }    
    
    /**
     * Draws the drawable onto the display. This is invoked by the engine everytime the drawable
     * to show at a page (display).
     */
    @Override
    public void draw(Canvas canvas) {
        final Rect bounds = getBounds();
        int count = canvas.save();
        canvas.translate(bounds.left, bounds.top);
        mTextLayout.draw(canvas);
        canvas.restoreToCount(count);
    }

/*************************************************************************************************
 * The following SETTER/GETTER are required either to implement parent class' abstract  methods,
 * or because the engine would invoke them during processing.
 **************************************************************************************************/
    /**
     * Required for engine processing.
     * Updates the internal bounds by copying the input's coordinates.
     */
    @Override
    protected void onBoundsChange(Rect bounds) {
        mTextBoundRectangle.set(bounds);
    }
    
    /**
     * Required for engine processing.
     * @Return True if the state change has caused the appearance of the Drawable to change 
     * (that is, it needs to be drawn), else False if it looks the same and there is no 
     * need to redraw it since its last state.
     */
    @Override
    protected boolean onStateChange(int[] state) {
        return false;
    }

    /**
     * Required for engine processing.
     * @Return True if this drawable changes its appearance, which is based on its text color 
     *         list set. False otherwise.
     */
    @Override
    public boolean isStateful() {
        return false;
    }
    
    /**
     * Required for engine processing.
     * @return Returns the vertical bounds of the measured underlying drawable object.
     *         Returns-1 if none (it has no intrinsic height, such as with a solid color).
     */
    @Override
    public int getIntrinsicHeight() {
        if (mTextBoundRectangle.isEmpty()) {
            return -1;
        } else {
            return (mTextBoundRectangle.bottom - mTextBoundRectangle.top);
        }
    }

    /**
     * Required for engine processing.
     * @return Returns the horizontal bounds of the measured underlying drawable object.
     *         Returns-1 if none (it has no intrinsic width, such as with a solid color).
     *         
     */
    @Override
    public int getIntrinsicWidth() {
        if (mTextBoundRectangle.isEmpty()) {
            return -1;
        } else {
            return (mTextBoundRectangle.right - mTextBoundRectangle.left);
        }
    }    
    
    /**
     * Required. Implements parent abstract method.
     */
    @Override
    public void setColorFilter(ColorFilter cf) {
        mTextPaint.setColorFilter(cf);
        invalidateSelf();
    }

    /**
     * Required. Implements parent abstract method.
     */
    @Override
    public void setAlpha(int alpha) {
        mTextPaint.setAlpha(alpha);
        invalidateSelf();
    }

    /**
     * Required. Implements parent abstract method.
     */
    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    
}
