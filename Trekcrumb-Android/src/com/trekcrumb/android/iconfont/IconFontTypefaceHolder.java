package com.trekcrumb.android.iconfont;

import java.io.File;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import com.trekcrumb.android.utility.AndroidConstants;

/**
 * Singleton.
 * A static storage for Typeface instance loaded from .ttf file, and to prevent multiple
 * instances init-ed and running.
 * @author Val Triadi
 * @see /drawable-mdpi/a_read_me.txt
 */
public class IconFontTypefaceHolder {
    private static final String LOG_TAG = "IconFontTypefaceHolder";
    
    private static Typeface mTypeface;

    public static Typeface getTypeface(AssetManager assetManager) {
        if (mTypeface == null) {
            init(assetManager);
            if(mTypeface == null) {
                String errMsg = "IconFont Typeface FAILED to initialize!";
                Log.e(LOG_TAG, "getTypeface() - ERROR: " + errMsg);
                throw new IllegalStateException(errMsg);
            }
        }

        return mTypeface;
    }
    
    /*
     * Initializes the static Typeface variable so it is ready to use globally. If the variable
     * has been initialized before, skip it.
     * 
     * @param assetManager - Android AssetManager
     */
    private static void init(AssetManager assetManager) {
        if(assetManager == null && AndroidConstants.APP_ICONFONT_TFF_FILEFULLPATH == null) {
            String errMsg = "Either AssetManager or TFF fullpath must be provided to initialize TypeFace!";
            Log.e(LOG_TAG, "getTypeface() - ERROR: " + errMsg);
            throw new IllegalArgumentException(errMsg);
        }
        
        try {
            if(assetManager != null) {
                //Init via AssetManager and TFF filename:
                mTypeface = Typeface.createFromAsset(assetManager, AndroidConstants.APP_ICONFONT_TFF_FILENAME);
            
            } else {
                //Init via direct TFF fullpath:
                File tffFile = new File(AndroidConstants.APP_ICONFONT_TFF_FILEFULLPATH);
                mTypeface = Typeface.createFromFile(tffFile);
            }
            
        } catch(Exception e) {
            mTypeface = null;
            Log.e(LOG_TAG, "init() - ERROR: " + e.getMessage(), e);
        }
    }

}
