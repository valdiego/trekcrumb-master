package com.trekcrumb.android.iconfont;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * 
 * @author Val Triadi
 * @Since 02/2017
 * @see /drawable-mdpi/a_read_me.txt
 */
public class IconFontButtonImpl extends Button {
    
    public IconFontButtonImpl(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public IconFontButtonImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public IconFontButtonImpl(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        setTypeface(IconFontTypefaceHolder.getTypeface(context.getAssets()));
    }

}

