package com.trekcrumb.android.activity;
                                                 
import java.util.List;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PlaceEditAsyncTask;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * 5/21/2013:
 * Place edit was used to be dialog fragment, however Android performed badly where the 
 * look-feel was not consistent, and the entered data on form was not persisted by default
 * by the system during fragment destroyed/re-created. Hence, it is now an activity.
 */
public class PlaceEditActivity extends BaseActivity {
    private Trip mTrip;
    private Place mPlaceSelected;
    private int mPlaceSelectedIndex;
    private boolean mIsDelete;
    private CheckBox mViewDeleteChk;
    private ViewGroup mViewDataTable;


    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_place_edit);
        cleanErrorAndInfoMessages();

        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mTrip = (Trip)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mPlaceSelected = (Place)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_PLACE_SELECTED);
                mPlaceSelectedIndex = getIntent().getExtras().getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            }
            
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mPlaceSelected = (Place)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_PLACE_SELECTED);
            if(savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED) >= 0) {
                mPlaceSelectedIndex = (Integer)savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            } else {
                mPlaceSelectedIndex = 0;
            }
            mIsDelete = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
        }
        
        init();

    }
    
    /**
     * Callback method called by the system before putting the fragment into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PLACE_SELECTED, mPlaceSelected);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPlaceSelectedIndex);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsDelete);
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonSaveID:
                save();
                break;
                
            case R.id.buttonCancelID:
                finish();
                break;
                
            case R.id.deleteCheckBoxID:
                if(mViewDeleteChk == null) {
                    mViewDeleteChk = (CheckBox)findViewById(R.id.deleteCheckBoxID);
                }
                if(mViewDeleteChk.isChecked()) {
                    mIsDelete = true;
                    NoticeMessageUtil.displayWarningMessage(
                            findViewById(android.R.id.content), 
                            getLayoutInflater(), 
                            getResources().getString(R.string.info_common_deleteWarning));
                    mViewDataTable.setVisibility(View.INVISIBLE);

                } else {
                    mIsDelete = false;
                    NoticeMessageUtil.hideWarningMessage(
                            findViewById(android.R.id.content), getLayoutInflater());
                    mViewDataTable.setVisibility(View.VISIBLE);
                }
                break;

            default:
                //Nothing
                
        }
    }
    
    
    private void init() {
        if(mTrip == null || mPlaceSelected == null) {
            populateNoData();
            return;
        } else if(!isUserLoginTrip(mTrip)) {
            populateNoData();
            return;
        } else if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
            if(!mIsUserAuthenticated || !mIsUserTripSynced) {
                populateNoData();
                return;
            }
        }
            
        mViewDataTable = (ViewGroup)findViewById(R.id.tableDataID);
        if(mIsDelete) {
            NoticeMessageUtil.displayWarningMessage(
                    findViewById(android.R.id.content), 
                    getLayoutInflater(), 
                    getResources().getString(R.string.info_common_deleteWarning));
            mViewDataTable.setVisibility(View.INVISIBLE);

        } else {
            NoticeMessageUtil.hideWarningMessage(findViewById(android.R.id.content), getLayoutInflater());
            mViewDataTable.setVisibility(View.VISIBLE);
        }

        populateFormWithData();
    }
    
    private void populateNoData() {
        ViewGroup bodyContentLayout = (ViewGroup)findViewById(R.id.bodyMainSectionID);
        bodyContentLayout.removeAllViews();
        
        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
        evaluateErrorAndInfo();
    }
    
    
    private void populateFormWithData() {
        mViewDeleteChk = (CheckBox)findViewById(R.id.deleteCheckBoxID);

        TextView tripNameView = (TextView)findViewById(R.id.tripNameValueID);
        tripNameView.setText(mTrip.getName());
        
        TextView placelocationView = (TextView)findViewById(R.id.placelocationValueID);
        placelocationView.setText(mPlaceSelected.getLocation());
        
        TextView placeNoteView = (TextView)findViewById(R.id.placeNoteValueID);
        placeNoteView.setText(mPlaceSelected.getNote());
        
        TextView placeNumOfTotal = (TextView)findViewById(R.id.numOfTotalID);
        String numberOfTotalLabel = String.format(
                getResources().getString(R.string.label_common_numberOfTotal), 
                mPlaceSelectedIndex + 1,
                mTrip.getNumOfPlaces()); 
        placeNumOfTotal.setText(numberOfTotalLabel);
    }

    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();
        
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_place_edit), 
                        getResources().getString(R.string.info_common_pleaseWait));

        //Call back-end:
        Place placeToUpdate = composePlaceToUpdate();
        PlaceEditAsyncTask backendTask = new PlaceEditAsyncTask(this, placeToUpdate, mIsDelete, mTrip.getPublish());
        backendTask.execute();
    }
    
    private Place composePlaceToUpdate() {
        Place placeToUpdate = new Place(mPlaceSelected.getLatitude(), mPlaceSelected.getLongitude());
        placeToUpdate.setId(mPlaceSelected.getId());
        placeToUpdate.setUserID(mUserLogin.getUserId());
        placeToUpdate.setTripID(mTrip.getId());
        if(!mIsDelete) {
            CharSequence noteChars = ((TextView)findViewById(R.id.placeNoteValueID)).getText();
            if(noteChars != null) {
                placeToUpdate.setNote(String.valueOf(noteChars));
            } else {
                //User can edit note to empty:
                placeToUpdate.setNote(CommonConstants.STRING_VALUE_EMPTY);
            }
        }
        return placeToUpdate;
    }
    
    /**
     * Callback method to be invoked by PlaceEditAsyncTask when completed.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            /*
             * Note 9/2014: 
             * Ensure memory cache is clean of any traces of prevous records prior to the edit
             * (e.g., trips, images). This to enforce the 'back' button to reload data
             * again from back-end, not from cache, to ensure up-to-date data. 
             */
            CacheMemoryManager.cleanAll();

            List<Place> listOfUpdatedPlaces = result.getListOfPlaces();
            mTrip.setListOfPlaces(listOfUpdatedPlaces);
            if(mIsDelete) {
                mPlaceSelectedIndex = 0;
            }
            
            if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
                //Readjust userLogin sync data for this session:
                //TODO - How? At this moment, the device has updated date (in DB) later than the 
                //current userLogin.getLatestUpdatedTrip. (but the same on server DB)
            }

            ActivityUtil.doTripViewActivity(this, null, mTrip, true, getResources().getString(R.string.info_common_saveSuccess), false);
        }
        return isSuccess;
    }    
    

}
