package com.trekcrumb.android.activity;
                                                 
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.TripSyncAsyncTask;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.utility.CommonConstants;

public class TripSyncActivity 
extends BaseActivity {
    
    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_trip_sync);
        evaluateErrorAndInfo();
    }
    
    /**
     * Callback to take action when any menu on ActionBar is selected.
     * Overrides to disable 'create trip' icon menu item from this activity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.menuRefreshID
                || menuItem.getItemId() == R.id.menuSyncID) {
            //Disabled, do nothing:
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonSaveID:
                syncNow();
                break;
                
            default:
                //Nothing
        }
    }
    
    private void syncNow() {
        cleanErrorAndInfoMessages();
        
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_trip_sync), 
                        getResources().getString(R.string.info_common_pleaseWait));
        TripSyncAsyncTask backendTask = 
                new TripSyncAsyncTask(this, mUserLogin);
        backendTask.execute();
    }    
    
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            mIsUserTripSynced = TripManager.evaluateTripRecordsUpToDate(this, mUserLogin);
            ((TrekcrumbApplicationImpl) getApplicationContext()).setUserTripSynced(mIsUserTripSynced);
            ActivityUtil.doTripListActivity(
                    this, mUserLogin, 0,
                    CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED,
                    true, getResources().getString(R.string.info_trip_sync_success));

        }
        return isSuccess;
        
    }        
}
