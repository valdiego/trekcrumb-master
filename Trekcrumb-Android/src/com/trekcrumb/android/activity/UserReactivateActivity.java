package com.trekcrumb.android.activity;
                                                 
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.UserEditAsyncTask;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.DateUtil;

public class UserReactivateActivity 
extends BaseActivity {
    private static final String LOG_TAG = "UserReactivateActivity";

    private User mUserToReactivate;

    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_user_reactivate);
        cleanErrorAndInfoMessages();

        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mUserToReactivate = (User)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_USER_DEACTIVATED);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mUserToReactivate = (User)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_USER_DEACTIVATED);
        }
        
        init();
    }
    
    /**
     * Callback when to save any current dynamic data (state) before the activity
     * is destroyed; typically due to device flip.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_USER_DEACTIVATED, mUserToReactivate);
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonReactivateID:
                reactivate();
                break;
                
            default:
                //Nothing
        }
    }

    
    private void init() {
        if(mUserToReactivate == null) {
            populateNoData();
            evaluateErrorAndInfo();
        } else {
            populateValues();
            evaluateErrorAndInfo();
        }
    }
    
    private void populateNoData() {
        ViewGroup bodyLayout = (ViewGroup) findViewById(R.id.mainScrollLayoutID);
        bodyLayout.removeAllViews();
        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
    }
    
    private void populateValues() {
        TextView greetingView = (TextView)findViewById(R.id.reactvateGreeting_1_LabelID);
        if(mUserToReactivate != null) {
            String updatedDateStr = 
                    DateUtil.formatDate(mUserToReactivate.getUpdated(), 
                                        DateFormatEnum.GMT_STANDARD_LONG, 
                                        DateFormatEnum.LOCALE_UI_WITH_TIME);
            greetingView.setText(
                    getResources().getString(R.string.info_user_reactivate_greeting_1, 
                            mUserToReactivate.getUsername(),
                            updatedDateStr));
        } else {
            greetingView.setText(getResources().getString(R.string.error_user_illegalAccess));
        }
    }        
    
    private void reactivate() {
        cleanErrorAndInfoMessages();
        if(mUserToReactivate == null) {
            return;
        }

        User userToUpdate = new User();
        userToUpdate.setUserId(mUserToReactivate.getUserId());
        
        //Call back-end (must be async-task due to network op.)
        mProgressDialog = NoticeMessageUtil.showProgressDialog(
                this, 
                getLayoutInflater(), 
                getResources().getString(R.string.label_user_login), 
                getResources().getString(R.string.info_common_pleaseWait));
        UserEditAsyncTask backendTask = 
                new UserEditAsyncTask(this, userToUpdate, false, true, false);
        backendTask.execute();
    }
    
    /**
     * Callback by the back-end refresh business logic once it is done.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            if(result.getUser() == null) {
                Log.e(LOG_TAG, "Remote Server error: Login is successful, BUT returned user data is empty!");
                isSuccess = false;
                ((TrekcrumbApplicationImpl) getApplicationContext()).clearUserSession();
                addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_LoginFailed));
                evaluateErrorAndInfo();
                return isSuccess;
            }
            
            mUserLogin = result.getUser();
            SessionUtil.setUserLoginDataInSession(
                    (TrekcrumbApplicationImpl) getApplicationContext(),
                    mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(this, mUserLogin)); 
            String successGreeting = 
                    getResources().getString(R.string.info_user_reactivate_success, mUserLogin.getFullname());
            ActivityUtil.doHomeActivity(this, true, false, successGreeting);
        }
        return isSuccess;
    }    
    
        
}
