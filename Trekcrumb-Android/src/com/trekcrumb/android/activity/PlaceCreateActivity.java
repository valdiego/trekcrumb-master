package com.trekcrumb.android.activity;
                                                 
import android.location.Location;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PlaceCreateAsyncTask;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.GeoLocationUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Note:
 * 1. There are two ways to add a Place to a Trip: From Trip Detail where we will launch GPS current
 *    location search, and from Map Detail where GPS current location has been found, hence it 
 *    would be passed on in the incoming Intent's extra Bundle.
 * 2. 5/2013: Place create was used to be dialog fragment, however Android performed badly where the 
 *    look-feel was not consistent, and the entered data on form was not persisted by default
 *    by the system during fragment destroyed/re-created. Hence, it is now an activity.
 * 3. 2/2017: Place Create form was discarded! The op to add new place should be automatically done
 *    once the location is detected, without asking user confirmation nor entering note. User may
 *    choose to edit the new Place afterwards to add note. The purpose was to quick and prompt
 *    adding the Place with less interuptions.
 */
public class PlaceCreateActivity 
extends BaseActivity {
    private Trip mTrip;
    private Location mCurrentLocation;
    private boolean mIsGPSLocSearchInProgress;

    /**
     * @see BaseActivity.onCreate()
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_place_create);
        cleanErrorAndInfoMessages();

        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mTrip = (Trip)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mCurrentLocation = (Location)getIntent().getExtras().getParcelable(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mCurrentLocation = (Location)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT);
            mIsGPSLocSearchInProgress = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_LOCATION_IS_GPS_SEARCHING);
        }
        
        init();
    }
    
    /**
     * @see BaseActivity.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putParcelable(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT, mCurrentLocation);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_LOCATION_IS_GPS_SEARCHING, mIsGPSLocSearchInProgress);
    }
    
    /**
     * Callback to take action when any menu on ActionBar is selected.
     * Overrides to disable 'create trip' icon menu item from this activity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.menuTripCreateID) {
            //Disabled, do nothing:
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonCancelID:
                GeoLocationUtil.deactivateGPSLocation(this);
                finish();
                break;
                
            default:
                //Nothing
        }
    }    
    
    private void init() {
        //Hide Progress Bar:
        LinearLayout progressBarLayout = (LinearLayout) findViewById(R.id.progressBarLayoutID);
        if(progressBarLayout != null) {
            progressBarLayout.setVisibility(View.GONE);
        }
        
        if(mTrip == null) {
            populateNoData(getResources().getString(R.string.error_user_illegalAccess));
            return;
        } else if(!isUserLoginTrip(mTrip)) {
            populateNoData(getResources().getString(R.string.error_user_illegalAccess));
            return;
        } else if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
            if(!mIsUserAuthenticated || !mIsUserTripSynced) {
                populateNoData(getResources().getString(R.string.error_user_illegalAccess));
                return;
            }
        }
        
        if(mCurrentLocation == null) {
            if(mIsGPSLocSearchInProgress) {
                /*
                 * Due to app stop/resume framework mechanism, android will re-add any fragment
                 * automatically and re-start them. Therefore, if mIsGPSLocationSearch == true,
                 * it indicates GPSLocationFragment already exists and active in this context.
                 * Skip adding anymore fragment to avoid duplicate GPSLocationFragment running.
                 */
                return; //Just wait for result
            
            } else {
                mIsGPSLocSearchInProgress = true;
                GeoLocationUtil.activateGPSLocation(this);
            }
        } else {
            //Current location is already available: 
            mIsGPSLocSearchInProgress = false;
            GeoLocationUtil.deactivateGPSLocation(this);
            savePlace();
        }        
    }
    
    private void populateNoData(String errorMsg) {
        GeoLocationUtil.deactivateGPSLocation(this);
        addErrorOrInfoMessages(true, errorMsg);
        evaluateErrorAndInfo();
    }
    
    /**
     * Callback method when GPSLocationFragment completes searching the current location.
     */
    public void onGPSLocationReadyCallback(Location location, String errorMsg) {
        mIsGPSLocSearchInProgress = false;
        GeoLocationUtil.deactivateGPSLocation(this);

        if(location == null) {
            populateNoData(errorMsg);
        } else {
            mCurrentLocation = location;
            savePlace();
        }
    }
    
    private void savePlace() {
        cleanErrorAndInfoMessages();
        
        //Hide 'Cancel' button
        LinearLayout buttonCancelLayout = (LinearLayout) findViewById(R.id.buttonCancelLayoutID);
        if(buttonCancelLayout != null) {
            buttonCancelLayout.setVisibility(View.INVISIBLE);
            buttonCancelLayout.setVisibility(LinearLayout.GONE);
        }
        
        //Show Progress Bar:
        LinearLayout progressBarLayout = (LinearLayout) findViewById(R.id.progressBarLayoutID);
        if(progressBarLayout != null) {
            TextView progressBarText = (TextView) progressBarLayout.findViewById(R.id.progressBarLabelID);
            progressBarText.setText(getResources().getString(R.string.info_place_create_greeting));
            progressBarLayout.setVisibility(View.VISIBLE);
        }
        
        //Compose request:
        Place placeToCreate = new Place(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        placeToCreate.setUserID(mUserLogin.getUserId());
        placeToCreate.setTripID(mTrip.getId());
        placeToCreate.setLocation(GeoLocationUtil.getCityAndCountry(this, mCurrentLocation));
        
        PlaceCreateAsyncTask backendTask = new PlaceCreateAsyncTask(this, placeToCreate, mTrip.getPublish());
        backendTask.execute();
    }
    
    /**
     * Callback method to be invoked by PlaceCreateAsyncTask when completed.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            if(result.getListOfPlaces() == null
                    || result.getListOfPlaces().size() == 0) {
                addErrorOrInfoMessages(true, getResources().getString(R.string.error_place_create_successButNoPlaceFound));
                evaluateErrorAndInfo();
                
                //Hide Progress Bar:
                LinearLayout progressBarLayout = (LinearLayout) findViewById(R.id.progressBarLayoutID);
                if(progressBarLayout != null) {
                    progressBarLayout.setVisibility(View.GONE);
                }

            } else {
                //Success
                Place placeCreated = result.getListOfPlaces().get(0);
                mTrip.addPlace(placeCreated);
                
                if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
                    //Readjust userLogin sync data for this session:
                    mTrip.setUpdated(placeCreated.getCreated());
                    mUserLogin.setLastUpdatedTrip(mTrip);
                    SessionUtil.setUserLoginDataInSession((TrekcrumbApplicationImpl) getApplicationContext(),
                            mUserLogin, true, true);
                }
                
                //Go to result page:
                ActivityUtil.doTripViewActivity(this, null, mTrip, true, getResources().getString(R.string.info_place_create_success), false);

            }
        }
        return isSuccess;
    }    
    
}
