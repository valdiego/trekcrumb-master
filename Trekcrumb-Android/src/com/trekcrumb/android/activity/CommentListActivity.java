package com.trekcrumb.android.activity;
                                                 
import java.util.ArrayList;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.CommentRetrieveAsyncTask;
import com.trekcrumb.android.bean.ListCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.fragment.CommentListByUserFragment;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Comment List page.
 * 1. Issues and Resolutions
 *    (1) Issue: Fragments Auto-Create
 *    This activity contains the list fragment that is NOT defined by default inside its layout
 *    (which is generic), but added programatically in the code here. 
 *    When created from raw, we simply add the Fragment new. However, after STOP/PAUSE state, 
 *    Android engine preserves these fragments in FragmentManager, and 
 *    re-init them automatically along with their preserved states/variables, if any. 
 *    Commonly, we want to REUSE these 'sticky' fragments so we do not lose their preserved 
 *    states/variables; instead of remove/replace them with new fragment instances. 
 *    Some fragment could be too costly if we remove/replace it after stop/pause and auto-create.

 * @author Val Triadi
 *
 */
public class CommentListActivity extends BaseListActivity {
    private String mUserID;
    private String mUserFullname;
    private String mUsername;
    private boolean mIsUserLoginProfile;
    private ArrayList<Comment> mListOfComments;
    private String mCacheKeyCurrent;
    private boolean mIsAfterDelete;
    
    //Views:
    private View mProgressBarSectionView;
    private View mListFragmentSection; 

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.page_generic_list);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                CacheMemoryManager.cleanAll();

                Bundle intentBundleExtra = getIntent().getExtras();
                mUserID = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERID);
                mUserFullname = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME);
                mUsername = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME);
                mNumOfTotalRecords = intentBundleExtra.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL);
                mIsAfterDelete = intentBundleExtra.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE);
                mInfoMsg = getIntent().getExtras().getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mUserID = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERID);
            mUserFullname = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME);
            mUsername = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME);
            mIsAfterDelete = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE);

            mListOfComments = (ArrayList<Comment>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
        }
        
        mIsUserLoginProfile = isUserLoginProfile(mUserID);
        init();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, mUserID);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME, mUserFullname);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME, mUsername);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE, mIsAfterDelete);
        
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfComments);
    }
    
    /**
     * Method to be invoked by clickable menus on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);
        
        switch (view.getId()) {
            case R.id.menuGoPreviousID:
                cleanErrorAndInfoMessages();
                retrieveData();
                break;
            
            case R.id.menuGoNextID:
                cleanErrorAndInfoMessages();
                retrieveData();
                break;

            default:
                //Nothing
        }
    }
    
    @Override
    public void refresh() {
        if(ValidationUtil.isEmpty(mUserID)) {
            populateNoData();
            return;
        }
        if(mIsRefreshInProgress) {
            //Disallow multiple refreshes
            return;
        }

        /*
         * Else
         * (Be aware, a lot is going on at super.refresh()!
         */
        mIsRefreshInProgress = true;
        super.refresh();
        retrieveData();
    }    
    
    private void init() {
        if(ValidationUtil.isEmpty(mUserID)) {
            populateNoData();
            return;
        }
        
        cleanErrorAndInfoMessages();
        initPropertiesAndLayout();
        initPagination();
        initTitle();

        if(mListOfComments == null) {
            retrieveData();
        } else {
            //List of comments may not be null possibly due to pause/resume.
            mProgressBarSectionView.setVisibility(View.GONE);
            initListFragment();
        }
        
        if(mInfoMsg != null) {
            evaluateErrorAndInfo();
        }
    }
    
    private void populateNoData() {
        cleanErrorAndInfoMessages();
        
        View bodyMainSection = findViewById(R.id.bodyMainSectionID);
        bodyMainSection.setVisibility(View.INVISIBLE);

        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
        evaluateErrorAndInfo();
    }
    
    private void initPropertiesAndLayout() {
        //ProgressBar
        mProgressBarSectionView = findViewById(R.id.progressBarLayoutID);
        TextView progressBarText = (TextView)mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_common_pleaseWait);
        
        mListFragmentSection = findViewById(R.id.listFragmentSectionID);
    }
    
    private void initTitle() {
        TextView titlePage = (TextView) findViewById(R.id.titleID);
        StringBuilder titleStrBld = new StringBuilder();
        titleStrBld.append(mUserFullname);
        titleStrBld.append(" >> ");
        titleStrBld.append(getResources().getText(R.string.label_comment));
        titlePage.setText(titleStrBld.toString());
    }
    
    private void initListFragment() {
        CommentListByUserFragment listFragment = 
                (CommentListByUserFragment)getFragmentManager().findFragmentByTag(CommentListByUserFragment.TAG_FRAGMENT_NAME);
        if(listFragment != null) {
            //(1) Issue: Fragments Auto-Create (See Javadoc)
            return;
        }
        
        //Else:
        listFragment = new CommentListByUserFragment();
        Bundle dataBundle = new Bundle();
        dataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfComments);
        dataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE, mIsUserLoginProfile);
        listFragment.setArguments(dataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.listFragmentSectionID, listFragment, CommentListByUserFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }
    
    private void refreshListFragment() {
        CommentListByUserFragment listFragment = 
                (CommentListByUserFragment)getFragmentManager().findFragmentByTag(CommentListByUserFragment.TAG_FRAGMENT_NAME);
        if(listFragment == null) {
            initListFragment();
        
        } else {
            listFragment.refresh(mListOfComments);
        }
    }    
    
    /*
     * Retrieve the list either from back-end repository (local device or remote server), or
     * from cache.
     * Note:
     * 1. Cache management: Any seen List will be placed into CacheMemory so that any 
     *    'PREVIOUS" menu click (or 'back' navigation) would have the data ready.
     *    However, it does not search and put into the cache for the next List. 
     *    User must navigate first before it is placed in the cache.
     * 2. Since CacheMemory for all lists is static and used accross list activities, 
     *    it is important the key to be unique between activities so we avoid mixed-up list data 
     *    returned by the get from cache methods.
     */
    private void retrieveData() {
        //Current page:
        mCacheKeyCurrent = CommentListByUserFragment.TAG_FRAGMENT_NAME + mUserID + mPageCurrent;
        ListCacheBean listCacheBeanCurrent = CacheMemoryManager.getListFromCache(mCacheKeyCurrent);
        if(listCacheBeanCurrent == null) {
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            mListFragmentSection.setVisibility(View.GONE);

            //Setup cache:
            ListCacheBean cacheBean = new ListCacheBean();
            cacheBean.setKey(mCacheKeyCurrent);
            cacheBean.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
            CacheMemoryManager.addListToCache(mCacheKeyCurrent, cacheBean);

            //Call back-end ops:
            callAsyncBackendTask();

        } else if(listCacheBeanCurrent.getServiceStatus() == ServiceStatusEnum.STATUS_WAITING) {
            //Continue waiting (until onAsyncBackendTaskCompleted() is called)
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            mListFragmentSection.setVisibility(View.GONE);
            
        } else {
            //List is in the cache! Good and ready:
            mListOfComments = (ArrayList<Comment>) listCacheBeanCurrent.getValueCommentList();
            refreshListFragment();

            mProgressBarSectionView.setVisibility(View.GONE);
            mListFragmentSection.setVisibility(View.VISIBLE);
            mIsRefreshInProgress = false;
        }
    }
    
    private void callAsyncBackendTask() {
        Comment comment = new Comment();
        comment.setUsername(mUsername);
        CommentRetrieveAsyncTask backendTask = new CommentRetrieveAsyncTask(
                this, null, comment, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, mOffset, mCacheKeyCurrent);
        
        backendTask.execute();
    }

    /**
     * @see BaseActivity.onAsyncBackendTaskCompleted()
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result, String cacheKey) {
        /*
         * First, let's preserve display any carry-over info message (e.g., scenario after delete etc.)
         * just because cleanErrorAndInfoMessages() may be invoked down the line when evaluating
         * the result.
         */
        String reservedCarryOverInfoMsg = null;
        if(mInfoMsg != null) {
            reservedCarryOverInfoMsg = mInfoMsg;
        }
        
        //Reset refresh flag regardless the ops was due to refresh or not
        mIsRefreshInProgress = false;

        boolean isSuccess = super.onAsyncBackendTaskCompleted(result, cacheKey);
        
        //Manage display only if it is for current page:
        if(cacheKey.equalsIgnoreCase(mCacheKeyCurrent)) {
            mProgressBarSectionView.setVisibility(View.GONE);
            mListFragmentSection.setVisibility(View.VISIBLE);
            if(isSuccess) {
                mListOfComments = (ArrayList<Comment>) result.getListOfComments();
                refreshListFragment();
                updatePagination();
             }
        }
        
        /*
         * Then add back any info message to display (e.g., scenario after delete etc.)
         */
        if(reservedCarryOverInfoMsg != null) {
            addErrorOrInfoMessages(false, reservedCarryOverInfoMsg);
            evaluateErrorAndInfo();
        }
        
        return isSuccess;
    }    
    
    @Override
    public void onBackPressed() {
        if(mIsAfterDelete) {
            /* After 2.0: 
             * Defect fix: Overrides such that back button will go to a safe page, instead of accidentally
             * going back to Trip Detail page that has been deleted.
             */
            ActivityUtil.doUserLoginViewActivity(this, false, null);
            
        } else {
            //Business as usual
            super.onBackPressed();
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            /* Before 2.0: 
             * Defect fix: Overrides such that back button will go to a safe page, instead of accidentally
             * going back to Trip Detail page that has been deleted.
             */
            onBackPressed();
            return true;
        }
        
        //Else: Business as usual
        return super.onKeyDown(keyCode, event);
    }
    
}
