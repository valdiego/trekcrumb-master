package com.trekcrumb.android.activity;
                                                 
import java.util.ArrayList;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.TripSearchAsyncTask;
import com.trekcrumb.android.bean.ListCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.fragment.TripListFragment;
import com.trekcrumb.android.iconfont.IconFontTypefaceHolder;
import com.trekcrumb.android.utility.DeviceUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

public class TripSearchActivity 
extends BaseListActivity {
    private static final String BUNDLE_SEARCH_CRITERIA_CRUMBS = "BUNDLE_SEARCH_CRITERIA_CRUMBS";
    private static final String CACHE_KEY_PREFIX = "TripSearchActivity";
    
    private ArrayList<Trip> mListOfTrips;
    private boolean mIsShowEditMenus;
    private String mSearchCriteriaCrumbs;
    private String mCacheKeyCurrent;

    //Views:
    private TripListFragment mTripListFragment;
    private View mProgressBarSectionView;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_trip_list_search);
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            CacheMemoryManager.cleanAll();
            
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mListOfTrips = (ArrayList<Trip>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
            mIsShowEditMenus = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
            mSearchCriteriaCrumbs = savedInstanceState.getString(BUNDLE_SEARCH_CRITERIA_CRUMBS);
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfTrips);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsShowEditMenus);
        outStateBundle.putString(BUNDLE_SEARCH_CRITERIA_CRUMBS, mSearchCriteriaCrumbs);
    }
    
    /**
     * Method to be invoked by clickable menus (the menu icons and their container/layout) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.menuShowEditSectionID:
                mIsShowEditMenus = true;
                doShowEditSection();
                break;
                
            case R.id.menuHideEditSectionID:
                mIsShowEditMenus = false;
                doHideEditSection();
                break;
                
            case R.id.buttonSearchID:
                refresh();
                break;

            case R.id.menuClearID:
                clearSearchCriteria();
                break;
                
            case R.id.infoIconID:
                mSearchCriteriaCrumbs = null;
                break;
                
            case R.id.menuGoPreviousID:
                cleanErrorAndInfoMessages();
                retrieveTrips();
                break;
            
            case R.id.menuGoNextID:
                cleanErrorAndInfoMessages();
                retrieveTrips();
                break;

            default:
                //Nothing
        }
    }
    
    @Override
    public void refresh() {
        if(mIsRefreshInProgress) {
            //Disallow multiple refreshes
            return;
        }

        
        /*
         * Else
         * (Be aware, a lot is going on at super.refresh()!
         */
        mIsRefreshInProgress = true;
        mNumOfTotalRecords = 0;
        super.refresh();
        
        mIsShowEditMenus = false;
        mSearchCriteriaCrumbs = null;
        DeviceUtil.hideVirtualKeyboard(this);
        doHideEditSection();
        retrieveTrips();
    }
    
    private void init() {
        cleanErrorAndInfoMessages();
        initViews();
        initIcons();
        initLayout();
        initPagination();

        if(mListOfTrips != null && mListOfTrips.size() > 0) {
            /*
             * During initiation, only update List fragment if List of trips is not null (possibly due to pause/resume)
             * Note 7/2014: 
             * In case of pause/resume, the fragment would automatically reload itself
             * with all data saved on onSaveInstanceState(). Thus, this refresh() call may trigger
             * the same operation twice.
             */
            updateFragments();
        }

        if(mListOfTrips == null) {
            //Fresh init (possibly)
            retrieveTrips();
        }
        
        if(!ValidationUtil.isEmpty(mSearchCriteriaCrumbs)) {
            //Not null due to pause/resume:
            addErrorOrInfoMessages(
                    false, 
                    getResources().getString(R.string.label_trip_search_criteria_breadcrumbs, mSearchCriteriaCrumbs));
            evaluateErrorAndInfo();
        }
    }
    
    private void initViews() {
        mProgressBarSectionView = findViewById(R.id.progressBarLayoutID);
        TextView progressBarText = (TextView)mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_common_pleaseWait);
        
        mTripListFragment = (TripListFragment) getFragmentManager().findFragmentById(R.id.tripListFragmentID);
    }
    
    /*
     * Initializes menu icon using IconFont web technology by overriding the View (TextView/Button)'s
     * Typeface.
     * @see /drawable-mdpi/a_read_me.txt
     */
    private void initIcons() {
        Button menuShowEditSection = (Button) findViewById(R.id.menuShowEditSectionID);
        if(menuShowEditSection != null) {
            menuShowEditSection.setTypeface(IconFontTypefaceHolder.getTypeface(getAssets()));
        }
    }

    private void initLayout() {
        if(mListOfTrips == null) {
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.hide(mTripListFragment);
            fragmentTransaction.commit();

            doHideEditSection();
            return;
        }
        
        //Else: List of trips may not null possibly due to pause/resume.
        mProgressBarSectionView.setVisibility(View.GONE);
        updateFragments();

        if(mIsShowEditMenus) {
            doShowEditSection();
        } else {
            doHideEditSection();
        }
    }

    private void updateFragments() {
        if(mTripListFragment == null) {
            mTripListFragment = (TripListFragment) getFragmentManager().findFragmentById(R.id.tripListFragmentID);
        }

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.show(mTripListFragment);
        fragmentTransaction.commit();
        mTripListFragment.refresh(mListOfTrips, true, false);
    }
    
    private TripSearchCriteria composeSearchCriteria() {
        TripSearchCriteria tripSearchCriteria = new TripSearchCriteria();
        tripSearchCriteria.setOffset(mOffset);
        tripSearchCriteria.setNumOfRows(CommonConstants.RECORDS_NUMBER_MAX);
        
        TextView tripKeywordValue = (TextView)findViewById(R.id.tripNameValueID);
        if(tripKeywordValue != null 
                && !ValidationUtil.isEmpty(String.valueOf(tripKeywordValue.getText()))) {
            tripSearchCriteria.setTripKeyword(String.valueOf(tripKeywordValue.getText()));
        }
        
        TextView userKeywordValue = (TextView)findViewById(R.id.usernameValueID);
        if(userKeywordValue != null 
                && !ValidationUtil.isEmpty(String.valueOf(userKeywordValue.getText()))) {
            tripSearchCriteria.setUserKeyword(String.valueOf(userKeywordValue.getText()));
        }
        
        RadioGroup tripStatusRadioGroup = (RadioGroup)findViewById(R.id.statusRadioGroupID);
        int radioChecked = tripStatusRadioGroup.getCheckedRadioButtonId();
        if(radioChecked == R.id.statusActiveID) {
            tripSearchCriteria.setStatus(TripStatusEnum.ACTIVE);
        } else if(radioChecked == R.id.statusCompletedID) {
            tripSearchCriteria.setStatus(TripStatusEnum.COMPLETED);
        } else {
            tripSearchCriteria.setStatus(null);
        }
        
        return tripSearchCriteria;
    }
    
    private void composeSearchCrumbs(TripSearchCriteria searchCriteria) {
        StringBuilder searchCriteriaCrumbsBld = null;

        if(!ValidationUtil.isEmpty(searchCriteria.getTripKeyword())) {
            searchCriteriaCrumbsBld = new StringBuilder();
            searchCriteriaCrumbsBld.append("Search results for");
            searchCriteriaCrumbsBld.append(" Trek");
            searchCriteriaCrumbsBld.append(" \"" + searchCriteria.getTripKeyword() + "\"");
        }
        
        if(!ValidationUtil.isEmpty(searchCriteria.getUserKeyword())) {
            if(searchCriteriaCrumbsBld == null) {
                searchCriteriaCrumbsBld = new StringBuilder();
                searchCriteriaCrumbsBld.append("Search results for");
            } else {
                searchCriteriaCrumbsBld.append(" and");
            }
            searchCriteriaCrumbsBld.append(" User");
            searchCriteriaCrumbsBld.append(" \"" + searchCriteria.getUserKeyword() + "\"");
        }
        
        if(searchCriteria.getStatus() != null) {
            if(searchCriteriaCrumbsBld == null) {
                searchCriteriaCrumbsBld = new StringBuilder();
                searchCriteriaCrumbsBld.append("Search results for");
            } else {
                searchCriteriaCrumbsBld.append(" and");
            }
            searchCriteriaCrumbsBld.append(" Status");
            searchCriteriaCrumbsBld.append(" \"" + searchCriteria.getStatus().getValue() + "\"");
        }
        
        if(searchCriteriaCrumbsBld != null) {
            mSearchCriteriaCrumbs = searchCriteriaCrumbsBld.toString();
        } else {
            mSearchCriteriaCrumbs = null;
        }
    }
    
    private void clearSearchCriteria() {
        TextView tripKeywordValue = (TextView)findViewById(R.id.tripNameValueID);
        tripKeywordValue.setText(null);
        
        TextView userKeywordValue = (TextView)findViewById(R.id.usernameValueID);
        userKeywordValue.setText(null);
        
        RadioButton statusALLRadioButton = (RadioButton)findViewById(R.id.statusAllID);
        statusALLRadioButton.setChecked(true);
    }

    /*
     * Retrieve list of Trips.
     * Note:
     * 1. Cache management: Any seen List of Trips will be placed into CacheMemory so that the user
     *    can always navigate BACK to the previously seen pages. It does not, however, search and put
     *    into the cache for the next List. User must navigate first before it is placed in the cache.
     * 2. Since CacheMemory for list of trips is static and used accross list activities, 
     *    it is important the key to be unique between activities so we avoid mixed-up list data 
     *    returned from the getter methods.
     *    
     */
    private void retrieveTrips() {
        TripSearchCriteria tripSearchCriteria = composeSearchCriteria();
        composeSearchCrumbs(tripSearchCriteria);

        //Current page:
        mCacheKeyCurrent = CACHE_KEY_PREFIX + mPageCurrent;
        ListCacheBean listCacheBeanCurrent = CacheMemoryManager.getListFromCache(mCacheKeyCurrent);
        if(listCacheBeanCurrent == null) {
            //Trigger backend ops:
            ListCacheBean cacheBean = new ListCacheBean();
            cacheBean.setKey(mCacheKeyCurrent);
            cacheBean.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
            CacheMemoryManager.addListToCache(mCacheKeyCurrent, cacheBean);

            //Hide list fragment:
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.hide(mTripListFragment);
            fragmentTransaction.commit();

            //Show progress bar:
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            
            //Call back-end:
            TripSearchAsyncTask backendTask = new TripSearchAsyncTask(this, tripSearchCriteria, mCacheKeyCurrent);
            backendTask.execute();

        } else if(listCacheBeanCurrent.getServiceStatus() == ServiceStatusEnum.STATUS_WAITING) {
            //Continue waiting
            if(mTripListFragment != null && mTripListFragment.isVisible()) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.hide(mTripListFragment);
                fragmentTransaction.commit();
            }
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            
        } else {
            //TripList good and ready:
            cleanErrorAndInfoMessages();
            mIsRefreshInProgress = false;
            mProgressBarSectionView.setVisibility(View.GONE);
            mListOfTrips = (ArrayList<Trip>) listCacheBeanCurrent.getValueTripList();
            updateFragments();
        }
    }
    
    /**
     * Callback by the back-end refresh business logic once it is done.
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result, String cacheKey) {
        //Reset refresh flag regardless the ops was due to refresh or not:
        mIsRefreshInProgress = false;

        //Parent classes will handle success/fail scenarios.
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result, cacheKey);
        
        //If it is for current page, display it (success or fail):
        if(cacheKey.equalsIgnoreCase(mCacheKeyCurrent)) {
            mProgressBarSectionView.setVisibility(View.GONE);
            if(mSearchCriteriaCrumbs != null) {
                addErrorOrInfoMessages(
                        false, 
                        getResources().getString(R.string.label_trip_search_criteria_breadcrumbs, mSearchCriteriaCrumbs));
                evaluateErrorAndInfo();
            }
            
            if(isSuccess) {
                mListOfTrips = (ArrayList<Trip>)result.getListOfTrips();
                mNumOfTotalRecords = result.getNumOfRecords();
                updateFragments();
                updatePagination();
            }
        }
        return isSuccess;
    }
    
    /*
     * Show menu edit section.
     */
    private void doShowEditSection() {
        cleanErrorAndInfoMessages();
        
        View editLayout = findViewById(R.id.tripSearchFormLayoutID);
        if(editLayout != null) {
            editLayout.setVisibility(View.VISIBLE);
        }
        View menuShowEditSection = findViewById(R.id.menuShowEditSectionID);
        if(menuShowEditSection != null) {
            menuShowEditSection.setVisibility(View.GONE);
        }
    }
    
    /*
     * Hides menu edit section.
     */
    private void doHideEditSection() {
        View editLayout = findViewById(R.id.tripSearchFormLayoutID);
        if(editLayout != null) {
            editLayout.setVisibility(View.GONE);
        }
        View menuShowEditSection = findViewById(R.id.menuShowEditSectionID);
        if(menuShowEditSection != null) {
            menuShowEditSection.setVisibility(View.VISIBLE);
        }
    }    
    
    
}
