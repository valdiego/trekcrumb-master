package com.trekcrumb.android.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.trekcrumb.android.R;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActionBarUtil;
import com.trekcrumb.android.utility.DeviceUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Base activity providing common methods for its children activities that do the same business logic. 
 *
 */
public abstract class BaseActivity extends Activity {
    protected User mUserLogin;
    protected ArrayList<String> mListOfErrorMsg;
    protected String mInfoMsg;
    protected boolean mIsUserAuthenticated;
    protected boolean mIsUserTripSynced;
    protected ProgressDialog mProgressDialog;
    protected boolean mIsRefreshInProgress;

    /**
     * Callback method called by the system when the activity is created from scratch, or from
     * paused/stopped state (due to device flip etc.). Typically, this is where we 
     * initialize essential components/variables to retain when the activity is paused/stopped/resumed.
     * 1. If it is from scratch, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //Get Session objects/values:
        mUserLogin = ((TrekcrumbApplicationImpl) getApplicationContext()).getUserLogin();
        mIsUserAuthenticated = ((TrekcrumbApplicationImpl) getApplicationContext()).isUserAuthenticated();
        mIsUserTripSynced = ((TrekcrumbApplicationImpl) getApplicationContext()).isUserTripSynced();
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                Bundle intentBundleExtra = getIntent().getExtras();
                mListOfErrorMsg = (ArrayList<String>)intentBundleExtra.getSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
                mInfoMsg = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            }

        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mListOfErrorMsg = (ArrayList<String>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
            mInfoMsg = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
        }
        
        hideKeyboard();
    }
    
    /**
     * Callback method called by the system before putting the fragment into paused/stopped state
     * (typically due to device flip or other app running). Persists current variable values 
     * so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, mListOfErrorMsg);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, mInfoMsg);
    }
    
    /**
     * Callback to create ActionBar Menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return ActionBarUtil.createActionBarOptionMenu(this, getMenuInflater(), menu);
    }
    
    /**
     * Callback right after ActionBar Menu is created. Override to dynamically modify 
     * the content of ActionBar Menu.
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        ActionBarUtil.prepareActionBarOptionMenu(this, menu);
        return true;
    }
    
    /**
     * Callback to take action when any menu on ActionBar is selected.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return ActionBarUtil.actionBarOptionItemSelected(this, menuItem);
    }
    
    /**
     * Method to be invoked by clickable menus on the page. 
     * Children classes must override this method to implement other clickable menus.
     * @param view - The View object being clicked.
     */
    public void onClickMenuButton(View view) {
        switch (view.getId()) {
            case R.id.infoIconID:
                cleanErrorAndInfoMessages();
                break;
            
            case R.id.errorIconID:
                cleanErrorAndInfoMessages();
                break;

            default:
                //Nothing
        }
    }        
    
    public void cleanErrorAndInfoMessages() {
        mListOfErrorMsg = null;
        mInfoMsg = null;
        NoticeMessageUtil.hideErrorAndInfoMessages(findViewById(android.R.id.content));
    }

    public void addErrorOrInfoMessages(boolean isError, String msg) {
        if(isError) {
            if(mListOfErrorMsg == null) {
                mListOfErrorMsg = new ArrayList<String>();
            }
            mListOfErrorMsg.add(msg);
        } else {
            mInfoMsg = msg;
        }
    }

    public void evaluateErrorAndInfo() {
        DeviceUtil.hideVirtualKeyboard(this);

        if(mListOfErrorMsg != null && mListOfErrorMsg.size() > 0) {
            NoticeMessageUtil.displayErrorMessage(
                    findViewById(android.R.id.content),
                    getLayoutInflater(),
                    mListOfErrorMsg);
        } else {
            NoticeMessageUtil.hideErrorMessages(findViewById(android.R.id.content));
        }
        
        if(!ValidationUtil.isEmpty(mInfoMsg)) {
            NoticeMessageUtil.displayInfoMessage(
                    findViewById(android.R.id.content),
                    getLayoutInflater(),
                    mInfoMsg);
        } else {
            NoticeMessageUtil.hideInfoMessages(findViewById(android.R.id.content));
        }
    }

    /**
     * Callback method to be invoked when Activity's back-end asynchronous task
     * has been completed. Children classes would need to implement concrete
     * logic when needed.
     */
    public void onAsyncBackendTaskCompleted(boolean isSuccess) {
        if (mProgressDialog != null) {
            try {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            } catch(Exception e) {
                //Android throws exception if device flipped during progress bar:
                Log.e("BaseActivity.onAsyncBackendTaskCompleted()", 
                      "ERROR: During progress dialog dismissal: " + e.getMessage());
            }
        }
    }

    /**
     * Callback method to be invoked when Activity's back-end asynchronous task
     * has been completed. Children classes would need to implement concrete
     * logic when needed.
     */
    public boolean onAsyncBackendTaskCompleted(final ServiceResponse result) {
        onAsyncBackendTaskCompleted(true);
        cleanErrorAndInfoMessages();

        boolean isResultSuccess = true;
        if(result == null) {
            isResultSuccess = false;
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_commonError));
            
        } else if(!result.isSuccess()) {
            isResultSuccess = false;
            String errMsg = null;
            if(result.getServiceError() != null) {
                ServiceErrorEnum errorEnum = result.getServiceError().getErrorEnum();
                if(errorEnum == ServiceErrorEnum.SYSTEM_NETWORK_NOT_AVAILABLE_ERROR) {
                    errMsg = getResources().getString(R.string.error_system_device_networkNotAvailable);
                    
                } else if(errorEnum == ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR) {
                    errMsg = getResources().getString(R.string.error_system_server_NotAvailable);
                    
                } else if(errorEnum == ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR) {
                    errMsg = getResources().getString(R.string.error_user_LoginFailed);
                    
                } else if(errorEnum == ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR) {
                    errMsg = getResources().getString(R.string.error_user_LoginFailed_deactivated);
                    
                } else if(!ValidationUtil.isEmpty(result.getServiceError().getErrorText())) {
                    errMsg = result.getServiceError().getErrorText();
                }
            }
            
            if(errMsg == null) {
                errMsg = getResources().getString(R.string.error_system_commonError);
            }
            addErrorOrInfoMessages(true, errMsg);
        }
        
        if(!isResultSuccess) {
            evaluateErrorAndInfo();
        }
        return isResultSuccess;
    }
    
    public void evaluateUserLoginData() {
        //Refresh to get Session objects/values:
        mUserLogin = ((TrekcrumbApplicationImpl) getApplicationContext()).getUserLogin();
        mIsUserAuthenticated = ((TrekcrumbApplicationImpl) getApplicationContext()).isUserAuthenticated();
        mIsUserTripSynced = ((TrekcrumbApplicationImpl) getApplicationContext()).isUserTripSynced();

        if(!mIsUserAuthenticated) {
            addErrorOrInfoMessages(
                    false, 
                    getResources().getString(R.string.info_user_not_authenticated));
        } else if(!mIsUserTripSynced) {
            addErrorOrInfoMessages(
                    false, 
                    getResources().getString(R.string.error_trip_recordsOutOfDate));
        }
    }

    public boolean isUserLoginTrip(Trip trip) {
        if(trip == null) {
            return false;
        }
        
        if(mUserLogin == null
                || !mUserLogin.getUserId().equalsIgnoreCase(trip.getUserID())) {
            return false;
        }
        return true;
    }
    
    public boolean isUserLoginProfile(String userID) {
        if(mUserLogin == null
                || !mUserLogin.getUserId().equalsIgnoreCase(userID)) {
            return false;
        }
        return true;
    }
    
    /**
     * Children classes, whenever applicable, would need to implement the concrete logic.
     */
    public void refresh() {
        Log.w("Trekcrumb curent page (activity)", "refresh() - Not implemented.");
    }

    /**
     * Provides access to ProgressDialog instance to other components (non-children such as Fragments) 
     * that otherwise does not have access to. This is helpful when a Fragment triggers a async op
     * and needs a ProgressDialog to show up.
     */
    public void setProgressDialog(ProgressDialog progressDialog) {
        if(progressDialog != null) {
            mProgressDialog = progressDialog;
        }
    }
    
    /**
     * Disables any auto-open virtual keyboard, especially for a TextEdit view.
     */
    public void hideKeyboard() {
        //Default: Hide keyboard to begin with
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        /*
         * Focus: The code above won't work if there is any editable view (EditText, etc) on focus
         * which opens virtual keyboard to close the keyboard. Here, we retrieve the current view
         * on focus, unfocus it then close the keyboard.
         */
        View editableViewOnFocus = getCurrentFocus();
        if (editableViewOnFocus != null) {  
            editableViewOnFocus.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if(imm != null) {
                imm.hideSoftInputFromWindow(editableViewOnFocus.getWindowToken(), 0);
            }
        }
    }
        
}
