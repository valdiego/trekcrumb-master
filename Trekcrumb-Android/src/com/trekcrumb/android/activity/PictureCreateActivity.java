package com.trekcrumb.android.activity;
                                                 
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PictureCreateAsyncTask;
import com.trekcrumb.android.fragment.PictureSelectFragment;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;

public class PictureCreateActivity 
extends BaseActivity {
    
    private Trip mTrip;
    private String mPicToCreateURL;
    private boolean mIsLoadByCamera;
    
    //Views:
    private PictureSelectFragment mPictureSelectFragment;

    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_picture_create);
        cleanErrorAndInfoMessages();

        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                if(getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED) != null) {
                    mTrip = (Trip)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                }
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
        }

        init();
    }
    
    /**
     * Callback method called by the system before putting the fragment into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
    }
    
    private void init() {
        if(mTrip == null) {
            populateNoData();
            return;
        } else if(!isUserLoginTrip(mTrip)) {
            populateNoData();
            return;
        } else if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
            if(!mIsUserAuthenticated || !mIsUserTripSynced) {
                populateNoData();
                return;
            }
        }

        //Else:
        initLayout();
    }
    
    private void populateNoData() {
        ViewGroup bodyContentLayout = (ViewGroup)findViewById(R.id.bodyMainSectionID);
        bodyContentLayout.removeAllViews();
        
        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
        evaluateErrorAndInfo();
    }
    
    private void initLayout() {
        TextView tripNameView = (TextView)findViewById(R.id.tripNameValueID);
        tripNameView.setText(mTrip.getName());
        
        if(mPictureSelectFragment == null) {
            mPictureSelectFragment = (PictureSelectFragment) getFragmentManager().findFragmentById(R.id.pictureSelectFragmentID);
        }
        mPictureSelectFragment.refresh(mTrip.getId());

    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonSaveID:
                save();
                break;
                
            case R.id.buttonCancelID:
                if(mPictureSelectFragment != null) {
                    mPictureSelectFragment.cleanTemporaryPicFile();
                }
                finish();
                break;
                
            default:
                //Nothing
        }
    }

    /*
     * Saves the uploaded picture.
     */
    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();

        //Validate:
        if(mPictureSelectFragment != null) {
            mPicToCreateURL = mPictureSelectFragment.getPictureToCreateURL();
            mIsLoadByCamera = mPictureSelectFragment.isLoadByCamera();
        }
        if(mPicToCreateURL == null) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_picture_invalidParam_picEmpty));
            evaluateErrorAndInfo();
            return;
        }
        
        Picture picToCreate = composeNewPictureBean();
        
        //Back-end:
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_picture_create), 
                        getResources().getString(R.string.info_common_pleaseWait));
        PictureCreateAsyncTask backendTask = 
                new PictureCreateAsyncTask(this, picToCreate, mIsLoadByCamera, mTrip.getPublish());
        backendTask.execute();
    }
    
    private Picture composeNewPictureBean() {
        Picture newPicBean = new Picture();
        newPicBean.setTripID(mTrip.getId());
        newPicBean.setUserID(mTrip.getUserID());
        newPicBean.setImageURL(mPicToCreateURL);

        CharSequence notes = ((TextView)findViewById(R.id.noteValueID)).getText();
        if(notes != null) {
            newPicBean.setNote(String.valueOf(notes));
        }
        
        CheckBox coverPictureCheckBox = (CheckBox)findViewById(R.id.coverPictureCheckID);
        if(coverPictureCheckBox.isChecked()) {
            newPicBean.setCoverPicture(YesNoEnum.YES);
        }
        
        return newPicBean;
    }
    
    @Override
    public void onBackPressed() {
        if(mIsLoadByCamera) {
            /* After 2.0: 
             * Defect fix: Overrides such that back button will clean up any pic taken by camera
             */
            if(mPictureSelectFragment != null) {
                mPictureSelectFragment.cleanTemporaryPicFile();
            }
            super.onBackPressed();
            
        } else {
            //Business as usual
            super.onBackPressed();
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            /* Before 2.0: 
             * Defect fix: Overrides such that back button will clean up any pic taken by camera
             */
            onBackPressed();
            return true;
        }
        //Business as usual
        return super.onKeyDown(keyCode, event);
    }
    
    /**
     * Callback method to be invoked by PictureCreateAsyncTask when completed.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            if(result.getListOfPictures() == null
                    || result.getListOfPictures().size() == 0) {
                addErrorOrInfoMessages(true, getResources().getString(R.string.error_picture_create_successButNoPicFound));
                evaluateErrorAndInfo();
            } else {
                //Success:
                Picture picCreated = result.getListOfPictures().get(0);
                mTrip.addPicture(picCreated);
                
                if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
                    //Readjust userLogin sync data for this session:
                    mTrip.setUpdated(picCreated.getCreated());
                    mUserLogin.setLastUpdatedTrip(mTrip);
                    SessionUtil.setUserLoginDataInSession((TrekcrumbApplicationImpl) getApplicationContext(),
                            mUserLogin, true, true);
                }

                ActivityUtil.doTripViewActivity(this, null, mTrip, true, getResources().getString(R.string.info_picture_create_success), false);
            }
        }
        return isSuccess;
    }
    


}
