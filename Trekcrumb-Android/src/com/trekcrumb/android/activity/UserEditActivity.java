package com.trekcrumb.android.activity;
                                                 
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.UserEditAsyncTask;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.fragment.PictureSelectFragment;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.android.utility.SingletonFactory;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

public class UserEditActivity 
extends BaseActivity {
    private static final String TAG_LOGGER = "UserEditActivity";

    private String mNewFullname;
    private String mNewLocation;
    private String mNewSummary;
    private boolean mIsDeactivateUser;
    
    //Profile pic:
    private String mProfileImgSourceURL;
    private boolean mIsProfileImgByCamera;
    private PictureSelectFragment mPictureSelectFragment;


    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_user_edit);
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            NoticeMessageUtil.hideWarningMessage(findViewById(android.R.id.content), getLayoutInflater());
        } else  {
            //Re-created from paused/stopped; check for previous data bundle:
            mIsDeactivateUser = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
            if(mIsDeactivateUser) {
                NoticeMessageUtil.displayWarningMessage(
                        findViewById(android.R.id.content), 
                        getLayoutInflater(), 
                        getResources().getString(R.string.info_user_deactivate_warning));
            } else {
                NoticeMessageUtil.hideWarningMessage(findViewById(android.R.id.content), getLayoutInflater());
            }
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the fragment into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsDeactivateUser);
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        if(view.getId() == R.id.buttonCancelID) {
            finish();
            return;
        }
        
        //Do these only if userLogin is valid:
        if(mUserLogin != null
                && mIsUserAuthenticated) {
            switch (view.getId()) {
                case R.id.buttonSaveID:
                    save();
                    break;
                    
                case R.id.deactivateCheckBoxID:
                    CheckBox deleteCheckBox = (CheckBox)findViewById(R.id.deactivateCheckBoxID);
                    if(deleteCheckBox.isChecked()) {
                        mIsDeactivateUser = true;
                        NoticeMessageUtil.displayWarningMessage(
                                findViewById(android.R.id.content), 
                                getLayoutInflater(), 
                                getResources().getString(R.string.info_user_deactivate_warning));
                    } else {
                        mIsDeactivateUser = false;
                        NoticeMessageUtil.hideWarningMessage(
                                findViewById(android.R.id.content), 
                                getLayoutInflater());
                    }
                    break;
                    
                default:
                    //Nothing
            }                
        }
    }
    
    private void init() {
        cleanErrorAndInfoMessages();
        if(mUserLogin == null
                || !mIsUserAuthenticated) {
            populateNoData();
            return;
        }
        
        //Else:
        if(mPictureSelectFragment == null) {
            mPictureSelectFragment = (PictureSelectFragment) getFragmentManager().findFragmentById(R.id.pictureSelectFragmentID);
        }
        mPictureSelectFragment.refresh(mUserLogin.getUserId());
        
        populateValues();
        evaluateErrorAndInfo();
    }
    
    private void populateNoData() {
        ViewGroup bodyLayout = (ViewGroup) findViewById(R.id.mainScrollLayoutID);
        bodyLayout.removeAllViews();
        if(mUserLogin == null) {
            addErrorOrInfoMessages(
                    true, 
                    getResources().getString(R.string.error_user_illegalAccess));
        } else if(!mIsUserAuthenticated) {
            addErrorOrInfoMessages(
                    true, 
                    getResources().getString(R.string.error_user_autoLogin_NetworkError));
        }
        evaluateErrorAndInfo();

    }

    private void populateValues() {
        if(mUserLogin == null) {
            ViewGroup userProfileBody = (ViewGroup) findViewById(R.id.mainScrollLayoutID);
            userProfileBody.removeAllViews();
            addErrorOrInfoMessages(
                    true, 
                    getResources().getString(R.string.error_user_illegalAccess));
            return;
        }
        
        //Else:
        TextView fullnameView = (TextView)findViewById(R.id.fullnameValueID);
        TextView locationView = (TextView)findViewById(R.id.locationValueID);
        TextView summaryView = (TextView)findViewById(R.id.summaryValueID);

        fullnameView.setText(mUserLogin.getFullname());
        locationView.setText(mUserLogin.getLocation());
        summaryView.setText(mUserLogin.getSummary());

        //Existing profile pic:
        if(!ValidationUtil.isEmpty(mUserLogin.getProfileImageName())) {
            Bitmap userProfileImgBitmap = ImageUtil.decodePictureImageIntoBitmap(
                    null,
                    AndroidConstants.APP_LOCAL_DIR_PICTURES + mUserLogin.getProfileImageName(),
                    SingletonFactory.getPictureProfileViewSize(this),
                    true,
                    false);
            if(userProfileImgBitmap == null) {
                Log.e(TAG_LOGGER, "populateValues() - ERROR: Cannot find image file current user profile image: " 
                        + mUserLogin.getProfileImageName());
            
            } else {
                ImageView picReviewView = (ImageView) findViewById(R.id.picReviewID);
                if(picReviewView != null) {
                    picReviewView.setImageBitmap(userProfileImgBitmap);
                }
            }

        }
    }        
    
    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();
        
        if(!mIsDeactivateUser) {
            if(!validateForm()) {
                evaluateErrorAndInfo();
                return;
            }
        }
        User userToUpdate = composeRequest();
        
        mProgressDialog = NoticeMessageUtil.showProgressDialog(
                this, 
                getLayoutInflater(), 
                getResources().getString(R.string.label_user_edit), 
                getResources().getString(R.string.info_common_pleaseWait));
        
        UserEditAsyncTask backendTask = 
                new UserEditAsyncTask(this, userToUpdate, mIsDeactivateUser, false, mIsProfileImgByCamera);
        backendTask.execute();
    }
    
    private boolean validateForm() {
        if(((TextView)findViewById(R.id.fullnameValueID)).getText() == null
                || ValidationUtil.isEmpty(String.valueOf(((TextView)findViewById(R.id.fullnameValueID)).getText()))) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_invalidParam_fullnameEmpty));
            return false;
        }
        mNewFullname = String.valueOf(((TextView)findViewById(R.id.fullnameValueID)).getText());
        mNewLocation = String.valueOf(((TextView)findViewById(R.id.locationValueID)).getText());
        mNewSummary = String.valueOf(((TextView)findViewById(R.id.summaryValueID)).getText());
        
        if(mNewFullname.equalsIgnoreCase(mUserLogin.getFullname())) {
            mNewFullname = null;
        }
        if(ValidationUtil.isEmpty(mNewLocation)
                || mNewLocation.equalsIgnoreCase(mUserLogin.getLocation())) {
            mNewLocation = null;
        }
        if(ValidationUtil.isEmpty(mNewSummary)
                || mNewSummary.equalsIgnoreCase(mUserLogin.getSummary())) {
            mNewSummary = null;
        }
        return true;
    }
    
    private User composeRequest() {
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserLogin.getUserId());
        if(!mIsDeactivateUser) {
            userToUpdate.setFullname(mNewFullname);
            userToUpdate.setLocation(mNewLocation);
            userToUpdate.setSummary(mNewSummary);
            
            //Profile pic:
            if(mPictureSelectFragment != null) {
                mProfileImgSourceURL = mPictureSelectFragment.getPictureToCreateURL();
                mIsProfileImgByCamera = mPictureSelectFragment.isLoadByCamera();
                
                if(!ValidationUtil.isEmpty(mProfileImgSourceURL)) {
                    userToUpdate.setProfileImageURL(mProfileImgSourceURL);
                }
            }

        }
        return userToUpdate;
    }
    
    /**
     * Callback by the back-end refresh business logic once it is done.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        super.onAsyncBackendTaskCompleted(true);
        boolean isSuccess = evaluateResponse(result);
        if(isSuccess) {
            if(mIsDeactivateUser) {
                mUserLogin = null;
                ((TrekcrumbApplicationImpl) getApplicationContext()).clearUserSession();
                String successMsg = getResources().getString(R.string.info_user_deactivate_success);
                ActivityUtil.doHomeActivity(this, true, false, successMsg);
                
            } else {
                if(result.getUser() != null) {
                    mUserLogin = result.getUser();
                    SessionUtil.setUserLoginDataInSession(
                            (TrekcrumbApplicationImpl) getApplicationContext(),
                            mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(this, mUserLogin));
                    
                    if(!ValidationUtil.isEmpty(mProfileImgSourceURL)) {
                        /*
                         * If profile image is updated, clear the stored image MenuIcon from session,
                         * and let the next activity retrieve the new image.
                         */
                        ((TrekcrumbApplicationImpl) getApplicationContext()).setUserImageMenuIcon(null);
                    }
                }
                String successMsg = getResources().getString(R.string.info_common_saveSuccess);
                ActivityUtil.doUserLoginViewActivity(this, false, successMsg);
            }
            
        } else {
            evaluateErrorAndInfo();
        }
        return isSuccess;
    }    
    
    private boolean evaluateResponse(ServiceResponse result) {
        boolean isSuccess = true;
        if(result == null) {
            isSuccess = false;
            addErrorOrInfoMessages(
                    true, getResources().getString(R.string.error_system_commonError));
        }
        if(!result.isSuccess()) {
            isSuccess = false;
            if(result.getServiceError() != null) {
                if(result.getServiceError().getErrorEnum() == ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR) {
                    addErrorOrInfoMessages(
                            true, getResources().getString(R.string.error_user_duplicateUser));
                } else {
                    addErrorOrInfoMessages(
                            true, result.getServiceError().getErrorText());
                }
            } else {
                addErrorOrInfoMessages(
                        true, getResources().getString(R.string.error_system_commonError));
            }
        }
        return isSuccess;
    }
    

}
