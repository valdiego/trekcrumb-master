package com.trekcrumb.android.activity;
                                                 
import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.TripRetrieveAsyncTask;
import com.trekcrumb.android.fragment.BaseFragment;
import com.trekcrumb.android.fragment.MapDetailFragment;
import com.trekcrumb.android.fragment.PictureDetailFragment;
import com.trekcrumb.android.fragment.TripDetailFragment;
import com.trekcrumb.android.utility.ActionBarUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Represents page to view all details of a Trip: Trip Detail, Map, Picture Gallery, etc.
 *
 */
public class TripViewActivity 
extends BaseActivity 
implements TabListener {
    private static final int NUM_OF_PAGES = 3;
    
    private String mTripID;
    private Trip mTrip;
    private ArrayList<BaseFragment> mListOfFragments;
    private int mTabIndexSelected = -1;
    private boolean mIsUserLoginTrip;
    private boolean mIsTabInitCompleted;
    
    /**
     * A caller (previous activity) may pass in info/error messages when opening this page. This
     * should have been handled in BaseActivity.onCreate() to display them.
     *  
     * @See BaseActivity.onCreate()
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_trip_view);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mTrip = (Trip)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                if(mTrip == null) {
                    mTripID = getIntent().getExtras().getString(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED_ID);
                }
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            if(mTrip == null) {
                mTripID = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED_ID);
            }
            mTabIndexSelected = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     * 
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED_ID, mTripID);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mTabIndexSelected);
    }
    
    private void init() {
        mIsTabInitCompleted = false;
        
        if(mTrip == null) {
            findViewById(R.id.tripViewBodyLoadingID).setVisibility(View.VISIBLE);
            findViewById(R.id.tripViewBodyTabsID).setVisibility(View.GONE);
            retrieveData();
        
        } else {
            findViewById(R.id.tripViewBodyLoadingID).setVisibility(View.GONE);
            findViewById(R.id.tripViewBodyTabsID).setVisibility(View.VISIBLE);
            mTripID = mTrip.getId();
            mIsUserLoginTrip = isUserLoginTrip(mTrip);
            initTabs();
            mIsTabInitCompleted = true;
        }
    }
    
    private void populateNoData(String errMsg) {
        ViewGroup tripViewBodyLoading = (ViewGroup) findViewById(R.id.tripViewBodyLoadingID);
        tripViewBodyLoading.setVisibility(View.VISIBLE);
        findViewById(R.id.tripViewBodyTabsID).setVisibility(View.GONE);

        TextView errorView = (TextView) getLayoutInflater().inflate(R.layout.template_textview_error, null);
        errorView.setText(errMsg);
        tripViewBodyLoading.addView(errorView);
    }
    
    private void initTabs() {
        final ActionBar actionBar = getActionBar();
        for(int i = 0; i < NUM_OF_PAGES; i++) {
            Tab tab = actionBar.newTab();
            tab.setTabListener(this);
            if(i == 0) {
                tab.setText(getResources().getString(R.string.label_trip_detail));
            } else if(i == 1) {
                tab.setText(getResources().getString(R.string.label_place));
            } else if(i == 2) {
                tab.setText(getResources().getString(R.string.label_picture));
            } else {
                tab.setText(getResources().getString(R.string.info_common_notAvailable));
            }
            actionBar.addTab(tab);
        }

        //Set Tab contents (fragments):
        if(mTabIndexSelected < 0) {
            mTabIndexSelected = 0;
        }
        if(mListOfFragments == null) {
            mListOfFragments = populateTabs();
        }

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS); //This setting triggers call to onTabSelected():
        actionBar.setSelectedNavigationItem(mTabIndexSelected); //This setting triggers call to onTabSelected():
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false); //Do not display app title
        
        //Hack some of ActionBar default behaviors:
        ActionBarUtil.disableEmbeddedTabs(actionBar);       
    }

    /*
     * NOTE:
     * Once the page's tab fragments are constructed, we cannot save them into onSaveInstanceState(bundle)
     * because apparently Fragments are not marshalable. If we try, android will throw error:
     * "RuntimeException: Parcel: unable to marshal value [fragment classname]"
     */
    private ArrayList<BaseFragment> populateTabs() {
        ArrayList<BaseFragment> listOfFragments = new ArrayList<BaseFragment>(NUM_OF_PAGES);
        
        TripDetailFragment tripDetailFragment = new TripDetailFragment();
        Bundle tripDetailDataBundle = new Bundle();
        tripDetailDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        tripDetailDataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, mIsUserLoginTrip);
        if(mInfoMsg != null) {
            tripDetailDataBundle.putString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, mInfoMsg);
        }
        tripDetailFragment.setArguments(tripDetailDataBundle);
        listOfFragments.add(tripDetailFragment);
        
        MapDetailFragment placeDetailFragment = new MapDetailFragment();
        Bundle placeDetailDataBundle = new Bundle();
        placeDetailDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        placeDetailDataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, mIsUserLoginTrip);
        placeDetailFragment.setArguments(placeDetailDataBundle);
        listOfFragments.add(placeDetailFragment);

        PictureDetailFragment pictureDetailFragment = new PictureDetailFragment();
        Bundle picDetailDataBundle = new Bundle();
        picDetailDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        picDetailDataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, mIsUserLoginTrip);
        pictureDetailFragment.setArguments(picDetailDataBundle);
        listOfFragments.add(pictureDetailFragment);
        
        return listOfFragments;
    }
    
    /**
     * Overrides TabListener.onTabSelected()
     */
    @Override
    public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {
        Fragment fragmentSelected = null;
        cleanErrorAndInfoMessages();
        
        /*
         * Step 1: Reassign the preserved value of previously selected tab index ONLY IF 
         * the user tap the tab after all init() operation is completed. Do not do this during 
         * the init() where tab.getPosition() is always 0
         */
        if(mIsTabInitCompleted) {
            if (tab.getPosition() > mListOfFragments.size()) {
                //Prevent ArrayOutofBound error:
                mTabIndexSelected = 0;
            } else {
                mTabIndexSelected = tab.getPosition();
            }
        }
        fragmentSelected = mListOfFragments.get(mTabIndexSelected);
        String selectedFragmentTagName = fragmentSelected.getClass().getName();
        
        /*
         * Step-2: Check to see if the activity already has an attached fragment the same as
         * the newly selected fragment.
         * 1. If freshly created from raw, this should be NULL. 
         * 2. But if re-created from paused/stopped, there is a previously selected fragment
         *    and we need to retrieve it and use it in order to get its preserved state (bundle). 
         *    Otherwise, it will replace the preserved fragment with the same fragment (due to replace())
         *    thus deleting all the historic bundle.
         */
        Fragment previousSelectedFragment = getFragmentManager().findFragmentByTag(selectedFragmentTagName);
        if(previousSelectedFragment != null) {
            if(previousSelectedFragment.isDetached()) {
                fragmentTransaction.attach(previousSelectedFragment);
            }
            if(!previousSelectedFragment.isVisible()) {
                fragmentTransaction.show(previousSelectedFragment);
            }

        } else {
            fragmentTransaction.replace(R.id.tripViewBodyTabsID, fragmentSelected, selectedFragmentTagName);
        }
    }

    /**
     * Overrides TabListener.onTabUnselected()
     */
    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
        //Not implemented
    }

    /**
     * Overrides TabListener.onTabReselected()
     */
    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
        //Not implemented
    }
    
    /**
     * Refresh Trip View page.
     */
    @Override
    public void refresh() {
        if(mIsRefreshInProgress) {
            //Disallow multiple refreshes
            return;
        }
        
        //Else:
        mIsRefreshInProgress = true;
        cleanErrorAndInfoMessages();
        
        if(mIsTabInitCompleted) {
            /*
             * Page tabs structure have been initiated:
             * 1. Reset to the 1st Tab.
             * 2. Move page to the 1st tab by calling getActionBar().setSelectedNavigationItem().
             *    This will trigger call to onTabSelected() where the 1st Tab will be displayed, 
             *    and its fragment is attached to the parent Activity. Consequently, the other tabs' 
             *    fragments would be detached and they will not have any parent activity at all 
             *    (getActivity() = NULL) until they are selected. 
             *    Becareful on implementing/invoke these detached fragments' refresh() method.
             * 3. Note: 
             *    We MUST do these reset here before we call retrieveData() and before we
             *    refresh all the fragments later in onAsyncBackendTaskCompleted(). Otherwise,
             *    any refresh call while user inside MapDetail tab or PictureDetail tab would 
             *    throw fatal exception where their layout ContentView is NULL!
             */
            mTabIndexSelected = 0;
            getActionBar().setSelectedNavigationItem(mTabIndexSelected);
        }
        
        retrieveData();
    }
    
    private void retrieveData() {
        //Open 'background disabling' pop-up window
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_trip_detail), 
                        getResources().getString(R.string.info_common_pleaseWait));

        //Retrieve selected Trip
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(mTripID);
        TripRetrieveAsyncTask backendTask = new TripRetrieveAsyncTask(
                this, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, 
                mIsUserLoginTrip, 0, 1, null, null);
        backendTask.execute();
    }
    
    /**
     * This callback may be invoked due to the following:
     * 1. RetrieveData() operation because the incoming Trip is empty, at which it invokes
     *    async back-end component to retrieve Trip
     * 2. Refresh() operation at which it invokes async back-end component to retrieve Trip,
     *    regardless if the original incoming Trip is empty (tabs' fragments are not initialized)
     *    or not empty (tabs' fragments have been initialized)
     * 3. Its fragments various async calls such as retrieving user data, comments, favorites, etc.
     *    and expect NO Trip entity in their response (beware!).   
     * 
     * @see BaseActivity.onAsyncBackendTaskCompleted()
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        
        if(mIsRefreshInProgress) {
            //Do the following only if the callback was due to this page's refresh()
            mIsRefreshInProgress = false;
            if(isSuccess) {
                if(result.getListOfTrips() == null 
                        || result.getListOfTrips().size() == 0) {
                    isSuccess = false;
                    String errMsg = getResources().getString(R.string.error_trip_notFound);
                    addErrorOrInfoMessages(true, errMsg);
                    
                    if(!mIsTabInitCompleted) {
                        //Page tabs structure has never been initiated
                        populateNoData(errMsg);
                    }
                    
                    evaluateErrorAndInfo();
        
                } else {
                    mTrip = result.getListOfTrips().get(0);
                    if(!mIsTabInitCompleted) {
                        //Page tabs structure has never been initiated. Start over:
                        init();
                    } else {
                        //Page tabs structure have been initiated, refresh all of its tabs' fragments
                        for(BaseFragment tabFragment : mListOfFragments) {
                            Bundle tabFragmentDataBundle = tabFragment.getArguments();
                            tabFragmentDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
                            tabFragment.refresh(mTrip);
                        }
                    }
                }
            }
        
        } else if(!mIsTabInitCompleted) {
            //Do the following only if the callback was due to retrieveTrip since incoming trip is empty
            if(isSuccess) {
                if(result.getListOfTrips() == null 
                        || result.getListOfTrips().size() == 0) {
                    isSuccess = false;
                    String errMsg = getResources().getString(R.string.error_trip_notFound);
                    addErrorOrInfoMessages(true, errMsg);
                    populateNoData(errMsg);
                    evaluateErrorAndInfo();
        
                } else {
                    mTrip = result.getListOfTrips().get(0);
                    init();
                }            
            }
        }

        return isSuccess;
    }
    
}
