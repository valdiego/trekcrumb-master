package com.trekcrumb.android.activity;
                                                 
import java.util.List;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PictureEditAsyncTask;
import com.trekcrumb.android.asynctask.PictureImageRetrieveAsyncTask;
import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;

public class PictureEditActivity 
extends BaseActivity {
    private static final String TAG_LOGGER = "PictureEditActivity";
    
    private Trip mTrip;
    private Picture mPictureSelected;
    private int mPictureSelectedIndex;
    private Bitmap mPictureSelectedPreviewBMP;
    private boolean mIsDeletePicture;
    private ViewGroup mViewDataTable;
    
    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_picture_edit);
        cleanErrorAndInfoMessages();

        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mTrip = (Trip)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mPictureSelected = (Picture)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED);
                mPictureSelectedIndex = getIntent().getExtras().getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mPictureSelected = (Picture)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED);
            mPictureSelectedIndex = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            mIsDeletePicture = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
            mPictureSelectedPreviewBMP = (Bitmap)savedInstanceState.getParcelable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED_BLOB);
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the fragment into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, mPictureSelected);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPictureSelectedIndex);
        outStateBundle.putParcelable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED_BLOB, mPictureSelectedPreviewBMP);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsDeletePicture);
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.deleteCheckBoxID:
                CheckBox deleteCheckBox = (CheckBox) findViewById(R.id.deleteCheckBoxID);
                if(deleteCheckBox.isChecked()) {
                    mIsDeletePicture = true;
                    NoticeMessageUtil.displayWarningMessage(
                            findViewById(android.R.id.content), 
                            getLayoutInflater(), 
                            getResources().getString(R.string.info_common_deleteWarning));
                    mViewDataTable.setVisibility(View.INVISIBLE);
                    
                } else {
                    mIsDeletePicture = false;
                    NoticeMessageUtil.hideWarningMessage(
                            findViewById(android.R.id.content), getLayoutInflater());
                    mViewDataTable.setVisibility(View.VISIBLE);
                }
                break;
            
            case R.id.buttonSaveID:
                save();
                break;
                
            case R.id.buttonCancelID:
                finish();
                break;
    
            default:
                //Nothing
        }
    }
    
    private void init() {
        if(mTrip == null || mPictureSelected == null) {
            populateNoData();
            return;
        } else if(!isUserLoginTrip(mTrip)) {
            populateNoData();
            return;
        } else if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
            if(!mIsUserAuthenticated || !mIsUserTripSynced) {
                populateNoData();
                return;
            }
        }
        
        //Else:
        mViewDataTable = (ViewGroup)findViewById(R.id.tableDataID);
        if(mIsDeletePicture) {
            NoticeMessageUtil.displayWarningMessage(
                    findViewById(android.R.id.content), 
                    getLayoutInflater(), 
                    getResources().getString(R.string.info_common_deleteWarning));
            mViewDataTable.setVisibility(View.INVISIBLE);

        } else {
            NoticeMessageUtil.hideWarningMessage(findViewById(android.R.id.content), getLayoutInflater());
            mViewDataTable.setVisibility(View.VISIBLE);
        }

        populateFormWithData();
    }
    
    private void populateNoData() {
        ViewGroup bodyContentLayout = (ViewGroup)findViewById(R.id.bodyMainSectionID);
        bodyContentLayout.removeAllViews();
        
        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
        evaluateErrorAndInfo();
    }
    
    private void populateFormWithData() {
        TextView tripNameView = (TextView)findViewById(R.id.tripNameValueID);
        tripNameView.setText(mTrip.getName());
        
        TextView numOfTotalView = (TextView)findViewById(R.id.numOfTotalID);
        String numberOfTotalLabel = String.format(
                getResources().getString(R.string.label_common_numberOfTotal), 
                mPictureSelectedIndex + 1,
                mTrip.getNumOfPictures()); 
        numOfTotalView.setText(numberOfTotalLabel);
        
        TextView noteView = (TextView)findViewById(R.id.noteValueID);
        noteView.setText(mPictureSelected.getNote());
        
        if(mPictureSelected.getCoverPicture() == YesNoEnum.YES) {
            CheckBox coverPicCjeckbox = (CheckBox)findViewById(R.id.coverPictureCheckID);
            coverPicCjeckbox.setChecked(true);
        }
        
        populateImageToEdit();
    }
    
    /*
     * Android ImageView's can only hold maximum 2048x2048 image, and we must use setImageBitmap().
     * Cannot use ImageView.setImageURI() because we cannot guarantee how big the source image is.
     */
    private void populateImageToEdit() {
        ImageCacheBean imgResult = CacheMemoryManager.getImageFromCache(this, mPictureSelected.getImageName());
        if(imgResult == null) {
            //Not in cache:
            ImageCacheBean result = new ImageCacheBean();
            result.setKey(mPictureSelected.getImageName());
            result.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
            CacheMemoryManager.addImageToCache(this, mPictureSelected.getImageName(), result);

            PictureImageRetrieveAsyncTask backendTask = 
                    new PictureImageRetrieveAsyncTask(this, mPictureSelected, true, false, -1);
            backendTask.execute();

        } else if(imgResult.getServiceStatus() == ServiceStatusEnum.STATUS_WAITING) {
            //In cache but still loading. Continue waiting
            
        } else {
            //Image ready:
            ImageView mImg = (ImageView) findViewById(R.id.picImageID);
            mImg.setImageBitmap(imgResult.getValue());
            
            View mImgErrorText = findViewById(R.id.picImageErrorTextID);
            mImgErrorText.setVisibility(View.GONE);
        }
    }
    
    /*
     * Callback when back-end components complete retrieving image from cache or repository.
     * It does the following:
     * 1. Check if the returned result is good and not null
     * 2. Only if the result status is success, then put into cache. Otherwise do not, and remove it
     *    if it was in the cache previously.
     * 3. Display it good or bad.
     */
    public void onPictureImageDetailReadyCallback(ImageCacheBean imgResult) {
        if(imgResult != null) {
            if(imgResult.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS) {
                CacheMemoryManager.addImageToCache(this, imgResult.getKey(), imgResult);
                ImageView mImg = (ImageView) findViewById(R.id.picImageID);
                mImg.setImageBitmap(imgResult.getValue());
                
                View mImgErrorText = findViewById(R.id.picImageErrorTextID);
                mImgErrorText.setVisibility(View.GONE);
                
            } else {
                Log.e(TAG_LOGGER, "onPictureImageDetailReadyCallback() - Returned result has ERROR: " + imgResult);
                CacheMemoryManager.removeImageFromCache(this, imgResult.getKey());
                
                ImageView mImg = (ImageView) findViewById(R.id.picImageID);
                mImg.setImageBitmap(imgResult.getValue());
                
                TextView mImgErrorText = (TextView)findViewById(R.id.picImageErrorTextID);
                mImgErrorText.setVisibility(View.VISIBLE);
                mImgErrorText.setText(imgResult.getErrorMsg());
            }
        }
    }
    
    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();
        
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_picture_edit), 
                        getResources().getString(R.string.info_common_pleaseWait));

        //Call back-end:
        Picture picToUpdate = composePictureToEditBean();
        PictureEditAsyncTask backendTask = 
                new PictureEditAsyncTask(this, picToUpdate, mIsDeletePicture, mTrip.getPublish());
        backendTask.execute();
    }    
    
    /*
     * NOTE: Picture image is not editable.
     */
    private Picture composePictureToEditBean() {
        Picture picToUpdate = new Picture();
        picToUpdate.setId(mPictureSelected.getId());
        picToUpdate.setTripID(mTrip.getId());
        picToUpdate.setUserID(mUserLogin.getUserId());
        if(mIsDeletePicture) {
            picToUpdate.setImageName(mPictureSelected.getImageName());
            
        } else {
            CharSequence noteChars = ((TextView)findViewById(R.id.noteValueID)).getText();
            if(noteChars != null) {
                picToUpdate.setNote(String.valueOf(noteChars));
            } else {
                //User can edit note to empty:
                picToUpdate.setNote(CommonConstants.STRING_VALUE_EMPTY);
            }
            
            CheckBox coverPictureCheckBox = (CheckBox)findViewById(R.id.coverPictureCheckID);
            if(coverPictureCheckBox.isChecked()) {
                picToUpdate.setCoverPicture(YesNoEnum.YES);
            } else {
                picToUpdate.setCoverPicture(YesNoEnum.NO);
            }
        }
        return picToUpdate;
    }
    
    /**
     * Callback method to be invoked by PictureEditAsyncTask when completed.
     */
    @Override
    public boolean  onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            /*
             * Note 9/2014: 
             * Ensure memory cache is clean of any traces of prevous records prior to the edit
             * (e.g., trips, images). This to enforce the 'back' button to reload data
             * again from back-end, not from cache, to ensure up-to-date data. 
             */
            CacheMemoryManager.cleanAll();

            List<Picture> listOfUpdatedPictures = result.getListOfPictures();
            mTrip.setListOfPictures(listOfUpdatedPictures);
            
            if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
                //Readjust userLogin sync data for this session:
                //TODO - How? At this moment, the device has updated date later than the userLogin.getLatestUpdatedTrip.
            }

            ActivityUtil.doTripViewActivity(this, null, mTrip, true, getResources().getString(R.string.info_common_saveSuccess), false);
        }
        return isSuccess;
    }    

}
