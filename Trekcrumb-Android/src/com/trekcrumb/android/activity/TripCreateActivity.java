package com.trekcrumb.android.activity;
                                                 
import android.location.Location;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.TripCreateAsyncTask;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.GeoLocationUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

public class TripCreateActivity 
extends BaseActivity {

    private Location mCurrentLocation;
    private String mCurrentLocationString;
    private boolean mIsGPSLocationSearch;
    
    //Views:
    private TextView mWarningStatusText;
    private TextView mWarningPrivacyText;
    private TextView mWarningPublishText;

    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_trip_create);
        cleanErrorAndInfoMessages();
        
        if(savedInstanceState == null) {
            //Freshly created from raw: Nothing to do
        } else {
            //Re-created from paused/stopped; check for previous data:
            mCurrentLocation = (Location)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT);
            mCurrentLocationString = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT_STRING);
            mIsGPSLocationSearch = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_LOCATION_IS_GPS_SEARCHING);
        }
        
        init();
    }
    
    /**
     * Callback to take action when any menu on ActionBar is selected.
     * Overrides to disable 'create trip' icon menu item from this activity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.menuTripCreateID) {
            //Disabled, do nothing:
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putParcelable(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT, mCurrentLocation);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT_STRING, mCurrentLocationString);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_LOCATION_IS_GPS_SEARCHING, mIsGPSLocationSearch);
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonSaveID:
                save();
                break;
                
            case R.id.buttonCancelID:
                if(mCurrentLocation == null) {
                    //Canceling GPS lookup:
                    GeoLocationUtil.deactivateGPSLocation(this);
                }
                finish();
                break;
                
            case R.id.publishedID:
                if(mWarningPublishText != null) {
                    mWarningPublishText.setText(getResources().getString(R.string.info_trip_published_warning));
                }
                break;
                
            case R.id.notPublishedID:
                if(mWarningPublishText != null) {
                    mWarningPublishText.setText(getResources().getString(R.string.info_trip_notPublished_warning));
                }
                break;

            case R.id.publicID:
                if(mWarningPrivacyText != null) {
                    mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_public_warning));
                }
                break;
                
            case R.id.privateID:
                if(mWarningPrivacyText != null) {
                    mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_private_warning));
                }
                break;

            case R.id.refreshTextViewID:
                refresh();
                break;
                
            default:
                //Nothing
        }
    }
    
    @Override
    public void refresh() {
        /*
         * Enabling multiple refreshes call; essentially canceling whatever is happening at the moment,
         * including any current CurrentLocation and enforcing new search for current location
         */
        setContentView(R.layout.page_trip_create);
        cleanErrorAndInfoMessages();
        findViewById(R.id.refreshTextViewID).setVisibility(View.GONE);
        mCurrentLocation = null;
        mCurrentLocationString = null;
        mIsGPSLocationSearch = false;
        GeoLocationUtil.deactivateGPSLocation(this);
        init();
    }

    private void init() {
        if(mCurrentLocation == null) {
            if(mIsGPSLocationSearch) {
                /*
                 * Due to app stop/resume framework mechanism, android will re-add any fragment
                 * automatically and re-start them. Therefore, if mIsGPSLocationSearch == true,
                 * it indicates GPSLocationFragment already exists and active in this context.
                 * Skip adding anymore fragment to avoid duplicate.
                 */
                return; //Just wait for result
            
            } else {
                //Starts GPS lookup:
                mIsGPSLocationSearch = true;
                GeoLocationUtil.activateGPSLocation(this);
            }
            
        } else {
            //GPS location search has been done and we have the location, so use it
            mIsGPSLocationSearch = false;
            GeoLocationUtil.deactivateGPSLocation(this);
            if(mCurrentLocationString == null) {
                mCurrentLocationString = GeoLocationUtil.getCityAndCountry(this, mCurrentLocation);
            }
            enableCreateNewTrip();
        }
    }
    
    /**
     * Callback method when GPSLocationFragment completes searching the current location.
     */
    public void onGPSLocationReadyCallback(Location location, String errorMsg) {
        mIsGPSLocationSearch = false;
        GeoLocationUtil.deactivateGPSLocation(this);
        
        if(location == null) {
            disableCreateNewTrip(errorMsg);
            findViewById(R.id.refreshTextViewID).setVisibility(View.VISIBLE);
            
        } else {
            findViewById(R.id.refreshTextViewID).setVisibility(View.GONE);
            
            mCurrentLocation = location;
            mCurrentLocationString = GeoLocationUtil.getCityAndCountry(this, mCurrentLocation);
            enableCreateNewTrip();
        }
    }
    

    private void disableCreateNewTrip(String errorMsg) {
        addErrorOrInfoMessages(true, errorMsg);
        evaluateErrorAndInfo();
    }
    
    private void enableCreateNewTrip() {
        cleanErrorAndInfoMessages();
        
        //Check user eligibility:
        if(!mIsUserAuthenticated) {
            addErrorOrInfoMessages(
                    false, 
                    getResources().getString(R.string.info_trip_published_userNotAuthenticated));
        } else if(!mIsUserTripSynced) {
            addErrorOrInfoMessages(
                    false, 
                    getResources().getString(R.string.info_trip_published_recordsOutOfDate));
        }
        
        initPropertiesAndLayout();
        populateValues();
        evaluateErrorAndInfo();
    }
    
    private void initPropertiesAndLayout() {
        //Remove original layout contents:
        LinearLayout buttonCancelLayout = (LinearLayout)findViewById(R.id.buttonCancelLayoutID);
        if(buttonCancelLayout != null) {
            //Physically delete cancelButton to avoid duplicate cancelButtonIDs:
            buttonCancelLayout.removeView(buttonCancelLayout.findViewById(R.id.buttonCancelID));
            buttonCancelLayout.setVisibility(LinearLayout.GONE);
        }
        
        //Add form layouts:
        LinearLayout tripCreateBodyLayout = (LinearLayout)findViewById(R.id.bodyMainSectionID);
        getLayoutInflater().inflate(R.layout.merge_form_trip_create_or_edit, tripCreateBodyLayout, true);
        
        //View properties
        View warningStatusLayout = findViewById(R.id.warningStatusLayoutID);
        mWarningStatusText = (TextView) warningStatusLayout.findViewById(R.id.warningTextID);
        
        View warningPrivacyLayout = findViewById(R.id.warningPrivacyLayoutID);
        mWarningPrivacyText = (TextView) warningPrivacyLayout.findViewById(R.id.warningTextID);
        
        View warningPublishLayout = findViewById(R.id.warningPublishLayoutID);
        mWarningPublishText = (TextView) warningPublishLayout.findViewById(R.id.warningTextID);
        
        //Hide or remove sections from form:
        View statusCompletedBtn = findViewById(R.id.statusCompletedID);
        statusCompletedBtn.setVisibility(LinearLayout.GONE);
        
        View deleteLayout = findViewById(R.id.deleteLayoutID);
        deleteLayout.setVisibility(LinearLayout.GONE);
    }
    
    /*
     * Populate default values/selections.
     */
    private void populateValues() {
        //Location:
        TextView locationView = (TextView)findViewById(R.id.tripLocationID);
        if(locationView != null) {
            locationView.setText(mCurrentLocationString);
        }

        //Status options:
        ((RadioButton) findViewById(R.id.statusActiveID)).setChecked(true);
        mWarningStatusText.setText(getResources().getString(R.string.info_trip_create_greeting));
        
        //Privacy options:
        ((RadioButton) findViewById(R.id.publicID)).setChecked(true);;
        mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_public_warning));

        //Publish options:
        if(!mIsUserAuthenticated || !mIsUserTripSynced) {
            ((RadioButton) findViewById(R.id.publishedID)).setVisibility(LinearLayout.GONE);
            ((RadioButton) findViewById(R.id.notPublishedID)).setChecked(true);
            mWarningPublishText.setText(getResources().getString(R.string.info_trip_notPublished_warning));

        } else {
            ((RadioButton) findViewById(R.id.publishedID)).setChecked(true);
            mWarningPublishText.setText(getResources().getString(R.string.info_trip_published_warning));
        }
    }
    
    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();
        
        if(!validateForm()) {
            evaluateErrorAndInfo();
            return;
        }
        
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_trip_create), 
                        getResources().getString(R.string.info_common_pleaseWait));

        Trip newTripBean = composeNewTrip();
        TripCreateAsyncTask backendTask = new TripCreateAsyncTask(this, newTripBean);
        backendTask.execute();
    }
    
    private boolean validateForm() {
        CharSequence tripNameValue = ((TextView)findViewById(R.id.tripNameID)).getText();
        if(tripNameValue == null
                || ValidationUtil.isEmpty(String.valueOf(tripNameValue))) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_trip_invalidParam_nameEmpty));
            return false;
        }
        if(mCurrentLocation == null) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_trip_invalidParam_currentLocEmpty));
            return false;
        }
        return true;
    }
    
    /*
     * Composed the Trip bean with values from the layout form.
     */
    private Trip composeNewTrip() {
        Trip tripToCreate = new Trip();
        tripToCreate.setUserID(mUserLogin.getUserId());
        tripToCreate.setStatus(TripStatusEnum.ACTIVE);
        tripToCreate.setName(String.valueOf(((TextView)findViewById(R.id.tripNameID)).getText()));
        tripToCreate.setLocation(mCurrentLocationString);
        
        CharSequence notes = ((TextView)findViewById(R.id.TripNoteID)).getText();
        if(notes != null) {
            tripToCreate.setNote(String.valueOf(notes));
        }
        
        RadioGroup publishRadioGroup = (RadioGroup)findViewById(R.id.publishRadioGroupID);
        int whichPublishIsChecked = publishRadioGroup.getCheckedRadioButtonId();
        switch (whichPublishIsChecked) {
            case R.id.publishedID: 
                tripToCreate.setPublish(TripPublishEnum.PUBLISH);
                break;
            case R.id.notPublishedID: 
                tripToCreate.setPublish(TripPublishEnum.NOT_PUBLISH);
                break;
        }
        
        RadioGroup privacyRadioGroup = (RadioGroup)findViewById(R.id.privacyRadioGroupID);
        int whichPrivacyIsChecked = privacyRadioGroup.getCheckedRadioButtonId();
        switch (whichPrivacyIsChecked) {
            case R.id.publicID: 
                tripToCreate.setPrivacy(TripPrivacyEnum.PUBLIC);
                break;
            case R.id.privateID: 
                tripToCreate.setPrivacy(TripPrivacyEnum.PRIVATE);
                break;
        }
        
        Place startPlaceToCreate = new Place(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        startPlaceToCreate.setUserID(mUserLogin.getUserId());
        startPlaceToCreate.setLocation(mCurrentLocationString);
        tripToCreate.addPlace(startPlaceToCreate);

        return tripToCreate;
    }    

    /**
     * Callback method to be invoked by TripCreateAsyncTask when completed.
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            if(result.getListOfTrips() == null
                    || result.getListOfTrips().size() == 0) {
                addErrorOrInfoMessages(true, getResources().getString(R.string.error_trip_create_successButNoTripFound));
                evaluateErrorAndInfo();
            } else {
                //Success
                Trip tripCreated = result.getListOfTrips().get(0);
                
                if(tripCreated.getPublish() == TripPublishEnum.PUBLISH) {
                    //Readjust userLogin sync data for this session:
                    mUserLogin.setLastCreatedTrip(tripCreated);
                    mUserLogin.setLastUpdatedTrip(tripCreated);
                    SessionUtil.setUserLoginDataInSession((TrekcrumbApplicationImpl) getApplicationContext(),
                                                           mUserLogin, true, true);
                }
                
                //Go to result page:
                ActivityUtil.doTripViewActivity(this, null, tripCreated, true, getResources().getString(R.string.info_trip_create_success), false);
            }
        }

        return isSuccess;
    }
    
    

}
