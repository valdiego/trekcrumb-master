package com.trekcrumb.android.activity;
                                                 
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.TripEditAsyncTask;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

public class TripEditActivity extends BaseActivity {
    private final static String IS_TRIP_DELETE = "IS_TRIP_DELETE";
    private final static String IS_TRIP_PUBLISH = "IS_TRIP_PUBLISH";
    
    private Trip mTripSelected;
    private TripPublishEnum mPublishOrigValue;
    private boolean mIsDeleteTrip;
    private boolean mIsPublishTrip;
    
    //Views:
    private View mWarningStatusLayout;
    private TextView mWarningPrivacyText;
    private TextView mWarningPublishText;
     private View mWarningDeleteLayout;
     private CheckBox mDeleteChkbox;
    private ViewGroup mViewDataTable;

    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_trip_edit);
        cleanErrorAndInfoMessages();

        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null 
                    && getIntent().getExtras() != null) {
                mTripSelected = (Trip)getIntent().getExtras().getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            }

        } else {
            //Re-created from paused/stopped; check for previous data:
            mTripSelected = (Trip)savedInstanceState.get(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mIsDeleteTrip = savedInstanceState.getBoolean(IS_TRIP_DELETE);
            mIsPublishTrip = savedInstanceState.getBoolean(IS_TRIP_PUBLISH);
        } 
        
        init();
    }

    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTripSelected);
        outStateBundle.putBoolean(IS_TRIP_DELETE, mIsDeleteTrip);
        outStateBundle.putBoolean(IS_TRIP_PUBLISH, mIsPublishTrip);
    }
    
    /**
     * Method to be invoked by clickable menus (icons) on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonSaveID:
                save();
                break;
                
            case R.id.buttonCancelID:
                finish();
                break;
                
            case R.id.statusActiveID:
                if(mWarningStatusLayout != null) {
                    mWarningStatusLayout.setVisibility(View.GONE);
                }
                break;

            case R.id.statusCompletedID:
                if(mWarningStatusLayout != null) {
                    mWarningStatusLayout.setVisibility(View.VISIBLE);
                }
                break;
                
            case R.id.publicID:
                if(mWarningPrivacyText != null) {
                    mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_public_warning));
                }
                break;
            
            case R.id.privateID:
                if(mWarningPrivacyText != null) {
                    mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_private_warning));
                }
                break;

            case R.id.publishedID:
                if(mWarningPublishText != null) {
                    mWarningPublishText.setText(getResources().getString(R.string.info_trip_published_warning));
                }
                break;
                
            case R.id.notPublishedID:
                if(mWarningPublishText != null) {
                    mWarningPublishText.setText(getResources().getString(R.string.info_trip_notPublished_warning));
                }
                break;

            case R.id.deleteCheckBoxID:
                if(mDeleteChkbox != null) {
                    if(mDeleteChkbox.isChecked()) {
                        mIsDeleteTrip = true;
                        doHandleDeleteCheckbox();
                    } else {
                        mIsDeleteTrip = false;
                        doHandleDeleteCheckbox();
                    }
                }
                break;

            default:
                //Nothing
        }
    }

    private void init() {
        if(mTripSelected == null) {
            populateNoData();
            return;
        } else if(!isUserLoginTrip(mTripSelected)) {
            populateNoData();
            return;
        } else if(mTripSelected.getPublish() == TripPublishEnum.PUBLISH) {
            if(!mIsUserAuthenticated || !mIsUserTripSynced) {
                populateNoData();
                return;
            }
        } else if(mTripSelected.getPublish() == TripPublishEnum.NOT_PUBLISH) {
            if(!mIsUserAuthenticated) {
                addErrorOrInfoMessages(
                        false, 
                        getResources().getString(R.string.info_trip_published_userNotAuthenticated));
                findViewById(R.id.publishedID).setVisibility(View.GONE);
            } else if(!mIsUserTripSynced) {
                addErrorOrInfoMessages(
                        false, 
                        getResources().getString(R.string.info_trip_published_recordsOutOfDate));
                findViewById(R.id.publishedID).setVisibility(View.GONE);
            }
        }
            
        initPropertiesAndLayout();
        populateValues();
        doHandleDeleteCheckbox();
        evaluateErrorAndInfo();
    }
    
    private void populateNoData() {
        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
        ViewGroup formLayout = (ViewGroup)findViewById(R.id.mainFormLayoutID);
        formLayout.removeAllViews();
    }
    
    private void initPropertiesAndLayout() {
        mPublishOrigValue = mTripSelected.getPublish();
        
        //View properties
        mViewDataTable = (ViewGroup)findViewById(R.id.tableDataID);
        
        mWarningStatusLayout = findViewById(R.id.warningStatusLayoutID);
        ((TextView) mWarningStatusLayout.findViewById(R.id.warningTextID)).setText(
                getResources().getString(R.string.info_trip_completed_warning));
        
        View warningPrivacyLayout = findViewById(R.id.warningPrivacyLayoutID);
        mWarningPrivacyText = (TextView) warningPrivacyLayout.findViewById(R.id.warningTextID);
        
        View warningPublishLayout = findViewById(R.id.warningPublishLayoutID);
        mWarningPublishText = (TextView) warningPublishLayout.findViewById(R.id.warningTextID);

        mDeleteChkbox = (CheckBox)findViewById(R.id.deleteCheckBoxID);
        mWarningDeleteLayout = findViewById(R.id.warningDeleteLayoutID);
        ((TextView) mWarningDeleteLayout.findViewById(R.id.warningTextID)).setText(
                getResources().getString(R.string.info_trip_delete_greeting));

        //Hide or remove sections from form:

    }    
    
    public void populateValues() {
        ((EditText) findViewById(R.id.tripNameID)).setText(mTripSelected.getName());
        ((EditText) findViewById(R.id.TripNoteID)).setText(mTripSelected.getNote());
        ((TextView) findViewById(R.id.tripLocationID)).setText(mTripSelected.getLocation());
        
        //Status
        if(mTripSelected.getStatus() == TripStatusEnum.ACTIVE) {
            ((RadioButton) findViewById(R.id.statusActiveID)).setChecked(true);
            mWarningStatusLayout.setVisibility(View.GONE);
        } else {
            //If status COMPLETED, disable any change to this field:
            ViewGroup statusRow = (ViewGroup)findViewById(R.id.statusRowID);
            if(statusRow != null) {
                statusRow.setVisibility(LinearLayout.GONE);
            }
            mWarningStatusLayout.setVisibility(View.GONE);
        }
        
        //Privacy
        if(mTripSelected.getPrivacy() == TripPrivacyEnum.PUBLIC) {
            ((RadioButton) findViewById(R.id.publicID)).setChecked(true);
            mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_public_warning));
        } else {
            ((RadioButton) findViewById(R.id.privateID)).setChecked(true);
            mWarningPrivacyText.setText(getResources().getString(R.string.info_trip_private_warning));
        }
        
        //Publish
        if(mTripSelected.getPublish() == TripPublishEnum.NOT_PUBLISH) {
            ((RadioButton) findViewById(R.id.notPublishedID)).setChecked(true);
            mWarningPublishText.setText(getResources().getString(R.string.info_trip_notPublished_warning));
        } else {
            //If published, disable any change to this field:
            ViewGroup publishRow = (ViewGroup)findViewById(R.id.publishRowID);
            if(publishRow != null) {
                publishRow.setVisibility(LinearLayout.GONE);
            }
            (findViewById(R.id.warningPublishLayoutID)).setVisibility(LinearLayout.GONE);
        }
    }    
    
    private void doHandleDeleteCheckbox() {
        if(mIsDeleteTrip) {
            mViewDataTable.setVisibility(View.GONE);
            mWarningDeleteLayout.setVisibility(View.VISIBLE);
        } else {
            mViewDataTable.setVisibility(View.VISIBLE);
            mWarningDeleteLayout.setVisibility(View.GONE);
        }
    }
    
    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();
        
        if(!mIsDeleteTrip) {
            if(!validateForm()) {
                evaluateErrorAndInfo();
                return;
            }
        }
        
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_trip_edit), 
                        getResources().getString(R.string.info_common_pleaseWait));

        Trip tripToUpdate = composeUpdatedTrip();
        TripEditAsyncTask backendTask = 
                new TripEditAsyncTask(this, tripToUpdate, mTripSelected.getPublish(), mIsDeleteTrip);
        backendTask.execute();
    }
    
    private boolean validateForm() {
        CharSequence tripNameValue = ((TextView)findViewById(R.id.tripNameID)).getText();
        if(tripNameValue == null
                || ValidationUtil.isEmpty(String.valueOf(tripNameValue))) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_trip_invalidParam_nameEmpty));
            return false;
        }
        return true;
    }    
    
    /*
     * Composed the Trip bean with values from the layout form.
     */
    private Trip composeUpdatedTrip() {
        Trip tripToUpdate = new Trip();
        tripToUpdate.setId(mTripSelected.getId());
        tripToUpdate.setUserID(mUserLogin.getUserId());
        
        //To delete:
        if(mIsDeleteTrip) {
            return tripToUpdate;
        }
        
        //To publish:
        if(mTripSelected.getPublish() == TripPublishEnum.NOT_PUBLISH) {
            //Only check if user change status from NOT_PUBLISH to PUBLISH:
            RadioGroup publishRadioGroup = (RadioGroup)findViewById(R.id.publishRadioGroupID);
            int whichPublishIsChecked = publishRadioGroup.getCheckedRadioButtonId();
            if(whichPublishIsChecked == R.id.publishedID) {
                mIsPublishTrip = true;
                tripToUpdate = mTripSelected.clone();
                tripToUpdate.setPublish(TripPublishEnum.PUBLISH);
                //Then let it capture other changes below.
            }
        }

        //Regular update:
        tripToUpdate.setName(String.valueOf(((TextView)findViewById(R.id.tripNameID)).getText()));

        CharSequence notes = ((TextView)findViewById(R.id.TripNoteID)).getText();
        if(notes != null) {
            tripToUpdate.setNote(String.valueOf(notes));
        } else {
            //User can update note to empty:
            tripToUpdate.setNote(CommonConstants.STRING_VALUE_EMPTY);
        }

        RadioGroup privacyRadioGroup = (RadioGroup)findViewById(R.id.privacyRadioGroupID);
        int whichPrivacyIsChecked = privacyRadioGroup.getCheckedRadioButtonId();
        switch (whichPrivacyIsChecked) {
          case R.id.publicID: 
              tripToUpdate.setPrivacy(TripPrivacyEnum.PUBLIC);
              break;
          case R.id.privateID: 
              tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
              break;
        }

        if(mTripSelected.getStatus() == TripStatusEnum.ACTIVE) {
            //Only check if user change status from ACTIVE to COMPLETED:
            RadioGroup statusRadioGroup = (RadioGroup)findViewById(R.id.statusRadioGroupID);
            int whichStatusIsChecked = statusRadioGroup.getCheckedRadioButtonId();
            if(whichStatusIsChecked == R.id.statusCompletedID) {
                tripToUpdate.setStatus(TripStatusEnum.COMPLETED);
            }
        }

        return tripToUpdate;
    }
    
    /**
     * Callback method to be invoked by backend AsyncTask when completed.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            /*
             * Note 9/2014: 
             * Ensure memory cache is clean of any traces of prevous records prior to the edit
             * (e.g., trips, images). This to enforce the 'back' button to reload data
             * again from back-end, not from cache, to ensure up-to-date data. 
             */
            CacheMemoryManager.cleanAll();
            
            if(mIsDeleteTrip) {
                //Result after delete a trip:
                if(mPublishOrigValue == TripPublishEnum.PUBLISH) {
                    //Readjust userLogin sync data for this session:
                    if(result.getUser() != null) {
                        mUserLogin = result.getUser();
                        SessionUtil.setUserLoginDataInSession(
                                (TrekcrumbApplicationImpl) getApplicationContext(), 
                                mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(this, mUserLogin)); 
                    }
                }
                
                if(mPublishOrigValue == TripPublishEnum.PUBLISH) {
                    ActivityUtil.doTripListActivity(
                            this, mUserLogin, 0,
                            CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED,
                            true, getResources().getString(R.string.info_trip_delete_success));
                    
                } else {
                    //Non-Published Trips:
                    int tripNonPublishCountNum = 0;
                    Trip tripToCount = new Trip();
                    tripToCount.setUserID(mUserLogin.getUserId());
                    tripToCount.setPublish(TripPublishEnum.NOT_PUBLISH);
                    ServiceResponse tripNonPublishCountResult = TripManager.count(this, tripToCount, true);
                    if(tripNonPublishCountResult.isSuccess()) {
                        tripNonPublishCountNum = result.getNumOfRecords();
                    }
                    ActivityUtil.doTripListActivity(
                            this, mUserLogin, tripNonPublishCountNum,
                            CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED,
                            true, getResources().getString(R.string.info_trip_delete_success));
                }
                
            } else {
                //Non-delete result:
                if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                    //Error:
                    addErrorOrInfoMessages(true, getResources().getString(R.string.error_trip_notFound));
                    evaluateErrorAndInfo();
                } else {
                    mTripSelected = result.getListOfTrips().get(0);
                    
                    //Readjust userLogin sync data for this session:
                    if(mIsPublishTrip) {
                        //Result after publish a trip:
                        if(result.getUser() != null) {
                            mUserLogin = result.getUser();
                            SessionUtil.setUserLoginDataInSession(
                                    (TrekcrumbApplicationImpl) getApplicationContext(),
                                    mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(this, mUserLogin)); 
                        }
                    } else {
                        //Result after regular update:
                        if(mPublishOrigValue == TripPublishEnum.PUBLISH) {
                            //Readjust userLogin sync data for this session:
                            mUserLogin.setLastUpdatedTrip(mTripSelected);
                            SessionUtil.setUserLoginDataInSession(
                                    (TrekcrumbApplicationImpl) getApplicationContext(),
                                    mUserLogin, true, true); 
                        }
                    }
                    
                    ActivityUtil.doTripViewActivity(this, null, mTripSelected, true, getResources().getString(R.string.info_common_saveSuccess), false);
                }
            }

        }
        return isSuccess;
    }    
}
