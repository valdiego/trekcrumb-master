package com.trekcrumb.android.activity;
                                                 
import java.util.ArrayList;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.FavoriteRetrieveAsyncTask;
import com.trekcrumb.android.asynctask.TripRetrieveAsyncTask;
import com.trekcrumb.android.bean.ListCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.fragment.TripListFragment;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * One generic Trip List class for accommodating all: 
 * To process and display different types of Trip List including UserLogin's published trips, 
 * unpublished trips, favorite trips and UserOther's trips.
 * 
 */
public class TripListActivity 
extends BaseListActivity {
    private String mUserID;
    private String mUserFullname;
    private String mUsername;
    private boolean mIsUserLoginProfile;
    private ArrayList<Trip> mListOfTrips;
    private boolean mIsAfterDelete;
    private String mCacheKeyCurrent;
    
    /*
     * This activity and its layout try to accomodate different types of Trip List: Published,
     * Unpublished, Favorites, etc. The param below is how to differentiate them.
     */
    private String mTripListType;

    //Views:
    private View mProgressBarSectionView;
    private View mListFragmentSection; 

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.page_generic_list);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                CacheMemoryManager.cleanAll();

                Bundle intentBundleExtra = getIntent().getExtras();
                mUserID = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERID);
                mUserFullname = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME);
                mUsername = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME);
                mTripListType = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE);
                mNumOfTotalRecords = intentBundleExtra.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL);
                mIsAfterDelete = intentBundleExtra.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE);
                mInfoMsg = getIntent().getExtras().getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mUserID = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERID);
            mUserFullname = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME);
            mUsername = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME);
            mTripListType = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE);
            mIsAfterDelete = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE);

            mListOfTrips = (ArrayList<Trip>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
        }
        
        mIsUserLoginProfile = isUserLoginProfile(mUserID);
        init();
        
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, mUserID);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_FULLNAME, mUserFullname);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERNAME, mUsername);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE, mTripListType);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfTrips);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_AFTER_DELETE, mIsAfterDelete);
    }
    
    /**
     * Method to be invoked by clickable menus on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);
        
        switch (view.getId()) {
            case R.id.menuGoPreviousID:
                cleanErrorAndInfoMessages();
                retrieveData();
                break;
            
            case R.id.menuGoNextID:
                cleanErrorAndInfoMessages();
                retrieveData();
                break;

            default:
                //Nothing
        }
    }
    
    @Override
    public void refresh() {
        if(ValidationUtil.isEmpty(mUserID)) {
            populateNoData();
            return;
        }
        if(mIsRefreshInProgress) {
            //Disallow multiple refreshes
            return;
        }

        /*
         * Else
         * (Be aware, a lot is going on at super.refresh()!
         */
        mIsRefreshInProgress = true;
        super.refresh();
        retrieveData();
    }    
    
    private void init() {
        if(ValidationUtil.isEmpty(mUserID)) {
            populateNoData();
            return;
        }
        
        cleanErrorAndInfoMessages();
        initPropertiesAndLayout();
        initPagination();
        initTitle();

        boolean isRetrieveDataOk = true;
        if(mListOfTrips == null) {
            isRetrieveDataOk = retrieveData();
        } else {
            //List of trips may not be null possibly due to pause/resume.
            mProgressBarSectionView.setVisibility(View.GONE);
            initListFragment();
        }
        
        if(isRetrieveDataOk) {
            if(mInfoMsg != null) {
                evaluateErrorAndInfo();
            }
        }
    }
    
    private void populateNoData() {
        cleanErrorAndInfoMessages();
        
        View bodyMainSection = findViewById(R.id.bodyMainSectionID);
        bodyMainSection.setVisibility(View.INVISIBLE);

        addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_illegalAccess));
        evaluateErrorAndInfo();
    }
    
    private void initPropertiesAndLayout() {
        //ProgressBar
        mProgressBarSectionView = findViewById(R.id.progressBarLayoutID);
        TextView progressBarText = (TextView)mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_common_pleaseWait);
        
        //Trip Unpub greeting/warning:
        if(mTripListType.equalsIgnoreCase(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED)) {
            TextView warningText = (TextView) findViewById(R.id.warningTextID);
            warningText.setText(R.string.info_trip_list_notpublished_greeting);
            View warningLayout = findViewById(R.id.warningLayoutID);
            warningLayout.setVisibility(View.VISIBLE);
        } else {
            View warningLayout = findViewById(R.id.warningLayoutID);
            warningLayout.setVisibility(View.GONE);
        }
        
        mListFragmentSection = findViewById(R.id.listFragmentSectionID);
    }
    
    private void initTitle() {
        TextView titlePage = (TextView) findViewById(R.id.titleID);
        StringBuilder titleStrBld = new StringBuilder();
        titleStrBld.append(mUserFullname);
        titleStrBld.append(" >> ");
        if(mTripListType.equalsIgnoreCase(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED)) {
            titleStrBld.append(getResources().getText(R.string.label_trip_list));
        } else if(mTripListType.equalsIgnoreCase(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED)) {
            titleStrBld.append(getResources().getText(R.string.label_trip_list_notPublished));
        } else if(mTripListType.equalsIgnoreCase(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES)) {
            titleStrBld.append(getResources().getText(R.string.label_favorite));
        }            
        titlePage.setText(titleStrBld.toString());
    }
    
    private void initListFragment() {
        TripListFragment listFragment = 
                (TripListFragment)getFragmentManager().findFragmentByTag(TripListFragment.TAG_FRAGMENT_NAME);
        if(listFragment != null) {
            //(1) Issue: Fragments Auto-Create (See Javadoc)
            return;
        }
        
        //Else:
        listFragment = new TripListFragment();
        Bundle dataBundle = new Bundle();
        dataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfTrips);
        dataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_SUMMARY, true);
        if(mIsUserLoginProfile) {
            dataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, true);
            dataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS, true);
        } else {
            dataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, false);
            dataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS, false);
        }
        listFragment.setArguments(dataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.listFragmentSectionID, listFragment, TripListFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }
    
    private void refreshListFragment() {
        TripListFragment listFragment = 
                (TripListFragment) getFragmentManager().findFragmentByTag(TripListFragment.TAG_FRAGMENT_NAME);
        if(listFragment == null) {
            initListFragment();
        
        } else {
            if(mIsUserLoginProfile) {
                listFragment.refresh(mListOfTrips, true, true);
            } else {
                listFragment.refresh(mListOfTrips, true, false);
            }
        }
    }    
    
    /*
     * Retrieve list of Trips either from back-end repository (local device or remote server), or
     * from cache.
     * Note:
     * 1. Cache management: Any seen List of Trips will be placed into CacheMemory so that any 
     *    'PREVIOUS" menu click (or 'back' navigation) would have the data ready.
     *    However, it does not search and put into the cache for the next List. 
     *    User must navigate first before it is placed in the cache.
     * 2. Since CacheMemory for list of trips is static and used accross list activities, 
     *    it is important the key to be unique between activities so we avoid mixed-up list data 
     *    returned by the get from cache methods.
     */
    private boolean retrieveData() {
        //Current page:
        mCacheKeyCurrent = mTripListType + mUserID + mPageCurrent;
        ListCacheBean listCacheBeanCurrent = CacheMemoryManager.getListFromCache(mCacheKeyCurrent);
        if(listCacheBeanCurrent == null) {
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            mListFragmentSection.setVisibility(View.GONE);

            //Setup cache:
            ListCacheBean cacheBean = new ListCacheBean();
            cacheBean.setKey(mCacheKeyCurrent);
            cacheBean.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
            CacheMemoryManager.addListToCache(mCacheKeyCurrent, cacheBean);

            //Call back-end ops:
            return callAsyncBackendTask();

        } else if(listCacheBeanCurrent.getServiceStatus() == ServiceStatusEnum.STATUS_WAITING) {
            //Continue waiting (until onAsyncBackendTaskCompleted() is called)
            mProgressBarSectionView.setVisibility(View.VISIBLE);
            mListFragmentSection.setVisibility(View.GONE);
            
        } else {
            //TripList is in the cache! Good and ready:
            mListOfTrips = (ArrayList<Trip>)listCacheBeanCurrent.getValueTripList();
            refreshListFragment();
            
            mProgressBarSectionView.setVisibility(View.GONE);
            mListFragmentSection.setVisibility(View.VISIBLE);
            mIsRefreshInProgress = false;
        }
        
        return true;
    }
    
    private boolean callAsyncBackendTask() {
        if(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED.equalsIgnoreCase(mTripListType)
                || CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED.equalsIgnoreCase(mTripListType)) {
            //Trip List (publish or unpublish):
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(mUserID);
            if(mTripListType.equalsIgnoreCase(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED)) {
                tripToRetrieve.setPublish(TripPublishEnum.NOT_PUBLISH);
            } else {
                tripToRetrieve.setPublish(TripPublishEnum.PUBLISH);
            }
            TripRetrieveAsyncTask backendTask = new TripRetrieveAsyncTask(
                    this, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, 
                    mIsUserLoginProfile, mOffset, CommonConstants.RECORDS_NUMBER_MAX, 
                    OrderByEnum.ORDER_BY_CREATED_DATE,
                    mCacheKeyCurrent);
            backendTask.execute();
            return true;
        
        } else if(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES.equalsIgnoreCase(mTripListType)) {
            //Favorite List:
            User user = new User();
            user.setUsername(mUsername);
            FavoriteRetrieveAsyncTask backendTask = new FavoriteRetrieveAsyncTask(
                    this, null, null, user, ServiceTypeEnum.FAVORITE_RETRIEVE_BY_USER, 
                    mOffset, mCacheKeyCurrent);
            backendTask.execute();
            return true;
            
        } else {
            String errMsg = "Invalid Trip List type to retrieve/display! Cannot continue";
            Log.e("TripListActivity", errMsg);
            addErrorOrInfoMessages(true, errMsg);
            evaluateErrorAndInfo();
            return false;
        }
    }

    /**
     * @see BaseActivity.onAsyncBackendTaskCompleted()
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result, String cacheKey) {
        /*
         * First, let's preserve display any carry-over info message (e.g., scenario after delete etc.)
         * just because cleanErrorAndInfoMessages() may be invoked down the line when evaluating
         * the result.
         */
        String reservedCarryOverInfoMsg = null;
        if(mInfoMsg != null) {
            reservedCarryOverInfoMsg = mInfoMsg;
        }
        
        //Reset refresh flag regardless the ops was due to refresh or not:
        mIsRefreshInProgress = false;

        boolean isSuccess = super.onAsyncBackendTaskCompleted(result, cacheKey);
        
        //Manage display only if it is for current page:
        if(cacheKey.equalsIgnoreCase(mCacheKeyCurrent)) {
            mProgressBarSectionView.setVisibility(View.GONE);
            mListFragmentSection.setVisibility(View.VISIBLE);
            if(isSuccess) {
                mListOfTrips = (ArrayList<Trip>)result.getListOfTrips();
                refreshListFragment();
                updatePagination();
             }
        }
        
        /*
         * Then add back any info message to display (e.g., scenario after delete etc.)
         */
        if(reservedCarryOverInfoMsg != null) {
            addErrorOrInfoMessages(false, reservedCarryOverInfoMsg);
            evaluateErrorAndInfo();
        }
        
        return isSuccess;
    }    
    
    @Override
    public void onBackPressed() {
        if(mIsAfterDelete) {
            /* After 2.0: 
             * Defect fix: Overrides such that back button will go to a safe page, instead of accidentally
             * going back to Trip Detail page that has been deleted.
             */
            ActivityUtil.doUserLoginViewActivity(this, false, null);
            
        } else {
            //Business as usual
            super.onBackPressed();
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            /* Before 2.0: 
             * Defect fix: Overrides such that back button will go to a safe page, instead of accidentally
             * going back to Trip Detail page that has been deleted.
             */
            onBackPressed();
            return true;
        }
        
        //Else: Business as usual
        return super.onKeyDown(keyCode, event);
    }
    
}
