package com.trekcrumb.android.activity;
                                                 
import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.trekcrumb.android.R;
import com.trekcrumb.android.fragment.BaseFragment;
import com.trekcrumb.android.fragment.UserCreateFragment;
import com.trekcrumb.android.fragment.UserForgetFragment;
import com.trekcrumb.android.fragment.UserLoginFragment;
import com.trekcrumb.android.utility.ActionBarUtil;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Represents page to options: User Login, User Create, User Forget Account, etc.
 *
 */
public class UserSignupActivity 
extends BaseActivity 
implements TabListener {
    private static final int NUM_OF_PAGES = 3;
    
    private ArrayList<BaseFragment> mListOfFragments;
    private int mTabIndexSelected = -1;
    private boolean mIsInitCompleted;
    
    /**
     * A caller (previous activity) may pass in info/error messages when opening this page. This
     * should have been handled in BaseActivity.onCreate() to display them.
     *  
     * @See BaseActivity.onCreate()
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_user_signup);
        if(savedInstanceState != null) {
            //Re-created from paused/stopped; check for previous data bundle:
            mTabIndexSelected = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     * 
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mTabIndexSelected);
    }
    
    /**
     * Callback to take action when any menu on ActionBar is selected.
     * Overrides to disable some menu item(s) from this activity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.menuSignupID) {
            //Disabled, do nothing:
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }
    
    private void init() {
        mIsInitCompleted = false;
        initTabs();
        mIsInitCompleted = true;
    }
    
    private void initTabs() {
        final ActionBar actionBar = getActionBar();
        for(int i = 0; i < NUM_OF_PAGES; i++) {
            Tab tab = actionBar.newTab();
            tab.setTabListener(this);
            if(i == 0) {
                tab.setText(getResources().getString(R.string.label_common_login));
            } else if(i == 1) {
                tab.setText(getResources().getString(R.string.label_common_join));
            } else if(i == 2) {
                tab.setText(getResources().getString(R.string.label_common_forget));
            } else {
                tab.setText(getResources().getString(R.string.info_common_notAvailable));
            }
            actionBar.addTab(tab);
        }

        //Set Tab contents (fragments):
        if(mListOfFragments == null) {
            mListOfFragments = populateTabContents();
        }
        if(mTabIndexSelected < 0) {
            mTabIndexSelected = 0;
        }

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS); //This setting triggers call to onTabSelected():
        actionBar.setSelectedNavigationItem(mTabIndexSelected); //This setting triggers call to onTabSelected():
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false); //Do not display app title
        
        //Hack some of ActionBar default behaviors:
        ActionBarUtil.disableEmbeddedTabs(actionBar);       
    }

    /*
     * NOTE:
     * Once the page's tab fragments are constructed, we cannot save them into onSaveInstanceState(bundle)
     * because apparently Fragments are not marshalable. If we try, android will throw error:
     * "RuntimeException: Parcel: unable to marshal value [fragment classname]"
     */
    private ArrayList<BaseFragment> populateTabContents() {
        ArrayList<BaseFragment> listOfFragments = new ArrayList<BaseFragment>(NUM_OF_PAGES);
        
        UserLoginFragment userLoginFragment = new UserLoginFragment();
        listOfFragments.add(userLoginFragment);
        
        UserCreateFragment userCreateFragment = new UserCreateFragment();
        listOfFragments.add(userCreateFragment);
        
        UserForgetFragment userForgetFragment = new UserForgetFragment();
        listOfFragments.add(userForgetFragment);

        return listOfFragments;
    }
    
    /**
     * Overrides TabListener.onTabSelected()
     */
    @Override
    public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {
        Fragment fragmentSelected = null;
        
        /*
         * Clean any error/info from previous tab
         */
        cleanErrorAndInfoMessages();
        
        /*
         * Hide any open virtual keyboard (for TextEdit)
         * REF: http://stackoverflow.com/questions/1109022/close-hide-the-android-soft-keyboard#17789187
         */
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        
        /*
         * Step 1: Reassign the preserved value of previously selected tab index ONLY IF 
         * the user tap the tab after all init() operation is completed. Do not do this during 
         * the init() where tab.getPosition() is always 0
         */
        if(mIsInitCompleted) {
            if (tab.getPosition() > mListOfFragments.size()) {
                //Prevent ArrayOutofBound error:
                mTabIndexSelected = 0;
            } else {
                mTabIndexSelected = tab.getPosition();
            }
        }
        fragmentSelected = mListOfFragments.get(mTabIndexSelected);
        String selectedFragmentTagName = fragmentSelected.getClass().getName();
        
        /*
         * Step-2: Check to see if the activity already has an attached fragment the same as
         * the newly selected fragment.
         * 1. If freshly created from raw, this should be NULL. 
         * 2. But if re-created from paused/stopped, there is a previously selected fragment
         *    and we need to retrieve it and use it in order to get its preserved state (bundle). 
         *    Otherwise, it will replace the preserved fragment with the same fragment (due to replace())
         *    thus deleting all the historic bundle.
         */
        Fragment previousSelectedFragment = getFragmentManager().findFragmentByTag(selectedFragmentTagName);
        if(previousSelectedFragment != null) {
            if(previousSelectedFragment.isDetached()) {
                fragmentTransaction.attach(previousSelectedFragment);
            }
            if(!previousSelectedFragment.isVisible()) {
                fragmentTransaction.show(previousSelectedFragment);
            }

        } else {
            fragmentTransaction.replace(R.id.tabBodyContentID, fragmentSelected, selectedFragmentTagName);
        }
    }

    /**
     * Overrides TabListener.onTabUnselected()
     */
    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
        //Not implemented
    }

    /**
     * Overrides TabListener.onTabReselected()
     */
    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
        //Not implemented
    }
    
}
