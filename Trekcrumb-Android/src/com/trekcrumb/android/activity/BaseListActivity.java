package com.trekcrumb.android.activity;
                                                 
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.bean.ListCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;

public abstract class BaseListActivity 
extends BaseActivity {
    
    //Pagination:
    protected boolean mIsShowPagination;
    protected int mOffset;
    protected int mPageCurrent;
    protected int mPagesTotal;
    protected int mNumOfTotalRecords;
    protected TextView mNumOfPagesView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            //Freshly created from raw:
            //None
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mIsShowPagination = getIntent().getExtras().getBoolean(CommonConstants.KEY_SESSIONOBJ_PAGINATION_IS_SHOW);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mIsShowPagination = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_PAGINATION_IS_SHOW);
            
            mOffset = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET);
            mPageCurrent = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_PAGE_CURRENT);
            mPagesTotal = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_PAGES_TOTAL);
            mNumOfTotalRecords = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL);
        }
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_PAGINATION_IS_SHOW, mIsShowPagination);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET, mOffset);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_PAGE_CURRENT, mPageCurrent);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_PAGES_TOTAL, mPagesTotal);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL, mNumOfTotalRecords);
    }
    
    /**
     * Method to be invoked by clickable menus on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);
        
        switch (view.getId()) {
            case R.id.menuGoPreviousID:
                if(mPageCurrent == 1 
                    || mOffset == 0 
                    || mOffset < CommonConstants.RECORDS_NUMBER_MAX) {
                    break;
                }
                mPageCurrent = mPageCurrent - 1;
                mOffset = mOffset - CommonConstants.RECORDS_NUMBER_MAX;
                updatePagination();
                break;
            
            case R.id.menuGoNextID:
                if(mPageCurrent == mPagesTotal) {
                    break;
                }
                mPageCurrent = mPageCurrent + 1;
                mOffset = mOffset + CommonConstants.RECORDS_NUMBER_MAX;
                updatePagination();
                break;

            default:
                //Nothing
        }
    }
    
    public void initPagination() {
        if(!mIsShowPagination) {
            View paginationSection = findViewById(R.id.paginationSectionID);
            if(paginationSection != null) {
                paginationSection.setVisibility(View.GONE);
            }
            return;
        }
        
        //Else:
        if(mPageCurrent < 1) {
            mPageCurrent = 1;
        }
        if(mPagesTotal < 1) {
            mPagesTotal = 1;
        }
        if(mOffset < 0) {
            mOffset = 0;
        }
        if(mNumOfTotalRecords < 0) {
            mNumOfTotalRecords = 0;
        }
        if(mNumOfPagesView == null) {
            mNumOfPagesView = (TextView)findViewById(R.id.numOfPagesID);
        }
        
        updatePagination();
    }
    
    public void updatePagination() {
        //Populate page number:
        if(mNumOfTotalRecords > CommonConstants.RECORDS_NUMBER_MAX ) {
            //Round up into int:
            mPagesTotal = mNumOfTotalRecords / CommonConstants.RECORDS_NUMBER_MAX 
                          + (mNumOfTotalRecords % CommonConstants.RECORDS_NUMBER_MAX == 0? 0 : 1);
        }
        if(mPagesTotal == 1) {
            mNumOfPagesView.setText(getResources().getString(R.string.label_common_pageOne));
        } else {
            String numOfPagesStr = String.format(
                    getResources().getString(R.string.label_common_pageOfTotal),  mPageCurrent, mPagesTotal); 
            mNumOfPagesView.setText(numOfPagesStr);
        }
        
        //Init 'Previous' menu:
        View menuPrevious = findViewById(R.id.menuGoPreviousID);
        if(mOffset == 0) {
            menuPrevious.setVisibility(View.GONE);
        } else {
            menuPrevious.setVisibility(View.VISIBLE);
        }
        
        //Init 'Next' menu:
        View menuNext = findViewById(R.id.menuGoNextID);
        if(mOffset + CommonConstants.RECORDS_NUMBER_MAX >= mNumOfTotalRecords) {
            menuNext.setVisibility(View.GONE);
        } else {
            menuNext.setVisibility(View.VISIBLE);
        }
    }
    
    /**
     * Callback by the back-end business logic once it is done.
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result, String cacheKey) {
        //Parent class will handle success/fail scenarios.
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);

        /*
         * Manage caching:
         * Put to memory cache only when it  is success. Note that CacheMemoryManager handles proper
         * adding/removing cache to avoid duplicates. If no success, remove from cache (if it is there). 
         * This will force the List pages to retry to retrieve the list, instead of stuck with the failed one.
         */
        if(!isSuccess) {
            CacheMemoryManager.removeListFromCache(cacheKey);

        } else {
            boolean isResultListExist = false;
            
            ListCacheBean cacheBean = new ListCacheBean();
            cacheBean.setKey(cacheKey);
            cacheBean.setServiceStatus(ServiceStatusEnum.STATUS_SUCCESS);
            if(result.getListOfTrips() != null && result.getListOfTrips().size() > 0) {
                isResultListExist = true;
                cacheBean.setValueTripList(result.getListOfTrips());
            } else if(result.getListOfComments() != null && result.getListOfComments().size() > 0) {
                isResultListExist = true;
                cacheBean.setValueCommentList(result.getListOfComments());
            }
            
            if(isResultListExist) {
                CacheMemoryManager.addListToCache(cacheKey, cacheBean);
            } else {
                CacheMemoryManager.removeListFromCache(cacheKey);
            }
        }
        
        return isSuccess;
    }    
    
    /**
     * Refreshes the List by clearing all records from current cache/memory, and initializes
     * pagination back to the beginnning.
     * 
     * NOTE:
     * The refresh() does not reset mNumOfTotalRecords = 0. This is because some children lists
     * needs the value to be in place (static) even after refresh. For other child (such as
     * TripSearch page), it may need to reset the mNumOfTotalRecords on its own.
     */
    @Override
    public void refresh() {
        cleanErrorAndInfoMessages();
        CacheMemoryManager.cleanAll();
        
        mOffset = 0;
        mPageCurrent = 1;
        mPagesTotal = 1;
        
        initPagination();
    }
    
}
