package com.trekcrumb.android.activity;
                                                 
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.UserAuthTokenDeleteAsyncTask;
import com.trekcrumb.android.asynctask.UserLogoutAsyncTask;
import com.trekcrumb.android.business.CacheDiskManager;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.utility.CommonConstants;

public class UserLogoutActivity 
extends BaseActivity {
    private boolean mIsDeleteStoredData;

    /**
     * Callback method called by the system when the activity is created from raw, or from
     * paused/stopped state (due to device flip etc.).
     * 1. If it is from raw, the Bundle is sure empty, and the activity will attempt to read
     *    any incoming data from Intent.
     * 2. If it is due to restart, the Bundle may have some preserved data so the activity will
     *    try to read data out of the Bundle and re-store the class variables/states accordingly.
     * 
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_user_logout);
        evaluateErrorAndInfo();

        if(savedInstanceState == null) {
            //Freshly created from raw:
        } else  {
            //Re-created from paused/stopped; check for previous data bundle:
            mIsDeleteStoredData = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
        }
        
        if(mIsDeleteStoredData) {
            NoticeMessageUtil.displayWarningMessage(
                    findViewById(android.R.id.content), 
                    getLayoutInflater(), 
                    getResources().getString(R.string.info_user_logout_deleteStoredData_warning));
        } else {
            NoticeMessageUtil.hideWarningMessage(findViewById(android.R.id.content), getLayoutInflater());
        }

    }
    
    /**
     * Callback method called by the system before putting the fragment into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsDeleteStoredData);
    }
    
    /**
     * Callback to take action when any menu on ActionBar is selected.
     * Overrides to disable 'login' menu item from this activity.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.menuLogoutID) {
            //Disabled, do nothing:
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }
    
    /**
     * Method to be invoked by clickable menus on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);

        switch (view.getId()) {
            case R.id.buttonLogoutID:
                logout();
                break;
            case R.id.deleteCheckBoxID:
                CheckBox deleteCheckBox = (CheckBox)view;
                if(deleteCheckBox.isChecked()) {
                    mIsDeleteStoredData = true;
                    NoticeMessageUtil.displayWarningMessage(
                            findViewById(android.R.id.content), 
                            getLayoutInflater(), 
                            getResources().getString(R.string.info_user_logout_deleteStoredData_warning));

                } else {
                    mIsDeleteStoredData = false;
                    NoticeMessageUtil.hideWarningMessage(findViewById(android.R.id.content), getLayoutInflater());
                }
                break;
                
            default:
                //Nothing
        }
    }
        
    private void logout() {
        cleanErrorAndInfoMessages();

        mProgressDialog = NoticeMessageUtil.showProgressDialog(
                this, 
                getLayoutInflater(), 
                getResources().getString(R.string.label_user_logout), 
                getResources().getString(R.string.info_user_logout_pleaseWait));
        
        //Step 1: 
        //Logs out user from local device and remove user's stored data (no interaction with remote server)
        UserLogoutAsyncTask backendTask = new UserLogoutAsyncTask(this, mUserLogin, mIsDeleteStoredData);
        backendTask.execute();
    }    
    
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        
        /*
         * Step 2:
         * Once user is logged out, launch separate thread to delete any relevant data on remote
         * server (the app doesn't need to wait), and continue to confirm logout.
         */
        if(isSuccess) {
            //Remote server:
            if(result.getUser() != null && result.getUser().getUserAuthToken() != null) {
                UserAuthToken userAuthTokenToDelete = result.getUser().getUserAuthToken();
                UserAuthTokenDeleteAsyncTask backendTask = 
                        new UserAuthTokenDeleteAsyncTask(this, userAuthTokenToDelete);
                backendTask.execute();
            }
            
            //Clean session:
            mUserLogin = null;
            ((TrekcrumbApplicationImpl) getApplicationContext()).clearUserSession();
            
            /*
             * Clean ALL caches.
             * 
             * Android Release Req. PS-P2:
             * 1. See DeviceUtil.enableStrictMode() for details. 
             * 2. In this step, we must read/write disk via CacheDiskManager from main thread, 
             *    instead of using async threads; it causes StrictMode to fail due to violation. 
             *    To ignore the violation, we temporarily gave permissions to read/write here:
             *    'permitDiskReads' and 'permitDiskWrites'
             *    THEN, reset the StrictMode policy back afterwards.
             */
            StrictMode.ThreadPolicy validStrictMode = StrictMode.getThreadPolicy();
            if(validStrictMode != null) {
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(validStrictMode)
                .permitDiskWrites()
                .permitDiskReads()
                .build());
            }

            CacheMemoryManager.cleanAll();
            CacheDiskManager.cleanAndClose(this);
            
            if(validStrictMode != null) {
                StrictMode.setThreadPolicy(validStrictMode);
            }

            ActivityUtil.doHomeActivity(this, true, false, null);
        } else {
            evaluateErrorAndInfo();
        }
        return isSuccess;
    }        
}
