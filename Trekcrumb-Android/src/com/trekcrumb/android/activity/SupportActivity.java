package com.trekcrumb.android.activity;
                                                 
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.common.utility.CommonConstants;

public class SupportActivity 
extends BaseActivity {
    private String mSupportType;
    private CharSequence mTermsContent;
    private CharSequence mPrivacyContent;
    private CharSequence mFAQContent;
    private TextView mTermsTextview;
    private TextView mPrivacyTextview;
    private TextView mFAQTextview;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_support);
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mSupportType = getIntent().getExtras().getString(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mSupportType = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE);
            mTermsContent = savedInstanceState.getCharSequence(CommonConstants.KEY_SESSIONOBJ_SUPPORT_CONTENT_TERMS);
            mPrivacyContent = savedInstanceState.getCharSequence(CommonConstants.KEY_SESSIONOBJ_SUPPORT_CONTENT_PRIVACY);
            mFAQContent = savedInstanceState.getCharSequence(CommonConstants.KEY_SESSIONOBJ_SUPPORT_CONTENT_FAQ);
        }
        
        init();
    }
    
    /**
     *  See BaseActivity.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE, mSupportType);
        outStateBundle.putCharSequence(CommonConstants.KEY_SESSIONOBJ_SUPPORT_CONTENT_TERMS, mTermsContent);
        outStateBundle.putCharSequence(CommonConstants.KEY_SESSIONOBJ_SUPPORT_CONTENT_PRIVACY, mPrivacyContent);
        outStateBundle.putCharSequence(CommonConstants.KEY_SESSIONOBJ_SUPPORT_CONTENT_FAQ, mFAQContent);
    }
    
    /**
     * Method to be invoked by clickable menus on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);
        
        switch (view.getId()) {
            case R.id.supportContactUsSubmitID:
                doContactUsViaWeb();
                break;
                
            default:
                //Nothing
        }
    }
        
    public void refresh(String supportType) {
        if(supportType != null) {
            mSupportType = supportType;
        }
        
        init();
    }
    
    private void init() {
        initTitle();
        initBody();
    }
    
    private void initTitle() {
        TextView titleView = (TextView) findViewById(R.id.titleID);
        if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_TERMS.equalsIgnoreCase(mSupportType)) {
            titleView.setText(getResources().getText(R.string.label_support_terms_long));
        } else if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_PRIVACY.equalsIgnoreCase(mSupportType)) {
            titleView.setText(getResources().getText(R.string.label_support_privacy_long));
        } else if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ.equalsIgnoreCase(mSupportType)) {
            titleView.setText(getResources().getText(R.string.label_support_faq_long));
        } else if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_CONTACTUS.equalsIgnoreCase(mSupportType)) {
            titleView.setText(getResources().getText(R.string.label_support_contactUs));
        }        
    }
    
    /*
     * Try best not to reload the file content nor set the text on the view repeatedly when 
     * user browses from menu to menu while is still in the same SupportActivity page. These
     * processes are costly.
     */
    private void initBody() {
        if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_TERMS.equalsIgnoreCase(mSupportType)) {
            findViewById(R.id.supportTermsLayoutID).setVisibility(View.VISIBLE);
            findViewById(R.id.supportPrivacyLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportFAQLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportContactUsLayoutID).setVisibility(View.GONE);
            
            if(mTermsContent == null) {
                mTermsContent = getResources().getText(R.string.support_terms);
            }
            if(mTermsTextview == null) {
                mTermsTextview = (TextView) findViewById(R.id.supportTermsContentID);
            }
            if(mTermsTextview.getText() == null || mTermsTextview.getText().length() == 0) {
                mTermsTextview.setText(mTermsContent);
            }
        
        } else if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_PRIVACY.equalsIgnoreCase(mSupportType)) {
            findViewById(R.id.supportTermsLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportPrivacyLayoutID).setVisibility(View.VISIBLE);
            findViewById(R.id.supportFAQLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportContactUsLayoutID).setVisibility(View.GONE);

            if(mPrivacyContent == null) {
                mPrivacyContent = getResources().getText(R.string.support_privacy);
            }
            if(mPrivacyTextview == null) {
                mPrivacyTextview = (TextView) findViewById(R.id.supportPrivacyContentID);
            }
            if(mPrivacyTextview.getText() == null || mPrivacyTextview.getText().length() == 0) {
                mPrivacyTextview.setText(mPrivacyContent);
            }
            
        } else if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ.equalsIgnoreCase(mSupportType)) {
            findViewById(R.id.supportTermsLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportPrivacyLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportFAQLayoutID).setVisibility(View.VISIBLE);
            findViewById(R.id.supportContactUsLayoutID).setVisibility(View.GONE);

            if(mFAQContent == null) {
                mFAQContent = getResources().getText(R.string.support_faq);
            }
            if(mFAQTextview == null) {
                mFAQTextview = (TextView) findViewById(R.id.supportFAQContentID);
            }
            if(mFAQTextview.getText() == null || mFAQTextview.getText().length() == 0) {
                mFAQTextview.setText(mFAQContent);
            }

        } else if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_CONTACTUS.equalsIgnoreCase(mSupportType)) {
            findViewById(R.id.supportTermsLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportPrivacyLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportFAQLayoutID).setVisibility(View.GONE);
            findViewById(R.id.supportContactUsLayoutID).setVisibility(View.VISIBLE);
        }
    }
    
    private void doContactUsViaWeb() {
        String contactUsURLStr = getResources().getString(R.string.webapp_support_contactus);
        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(contactUsURLStr));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
