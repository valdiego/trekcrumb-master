package com.trekcrumb.android.activity;
                                                 
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.UserRetrieveAsyncTask;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.fragment.UserDetailFragment;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Represents User Profile page.
 *
 */
public class UserViewActivity 
extends BaseActivity {
    private String mUserID;
    private boolean mIsUserLoginProfile;
    private User mUserOther;
    private boolean mIsShowEditMenus;
    
    //Fragments:
    private UserDetailFragment mUserDetailFragment;
    
    //Views:
    private View mBodyMainSection;
    private Button mMenuPageShow;
    private Button mMenuPageHide;
    private View mMenuPageContent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_user_view);
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                Bundle intentBundleExtra = getIntent().getExtras();
                mUserID = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERID);
            }

        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mUserID = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_USER_USERID);
            mUserOther = (User)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
            mIsShowEditMenus = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
        }
        
        mIsUserLoginProfile = isUserLoginProfile(mUserID);
        init();
    }
    
    /**
     *  See BaseActivity.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_USER_USERID, mUserID);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED, mUserOther);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsShowEditMenus);
    }
    
    /**
     * Method to be invoked by clickable menus on the page.
     * @param view - The View object being clicked.
     */
    @Override
    public void onClickMenuButton(View view) {
        super.onClickMenuButton(view);
        
        switch (view.getId()) {
            case R.id.menuPageShowID:
                mIsShowEditMenus = true;
                doShowMenuPageContent();
                break;
                
            case R.id.menuPageHideID:
                mIsShowEditMenus = false;
                doHideMenuPageContent();
                break;
            
            case R.id.menuEditID:
                if(mIsUserLoginProfile) {
                    doUserEditActivity();
                }
                break;
                
            default:
                //Nothing
        }
    }
    
    /**
     * Refresh data.
     */
    @Override
    public void refresh() {
        if(mIsRefreshInProgress) {
            //Disallow multiple refreshes
            return;
        }
        
        //Else:
        mIsRefreshInProgress = true;
        cleanErrorAndInfoMessages();
        
        if(ValidationUtil.isEmpty(mUserID)) {
            populateNoData();
            return;
        }
        
        if(mIsUserLoginProfile && !mIsUserAuthenticated) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.info_user_not_authenticated));
            evaluateErrorAndInfo();
            return;
        }
        
        //Call remote servers for fresh data:
        mProgressDialog = 
                NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_common_refresh), 
                        getResources().getString(R.string.info_common_pleaseWait));
        UserRetrieveAsyncTask backendTask = new UserRetrieveAsyncTask(this, mUserID, mIsUserLoginProfile);
        backendTask.execute();
    }
    
    private void init() {
        if(mIsUserLoginProfile && mUserLogin == null) {
            populateNoData();
            return;
        } else if(ValidationUtil.isEmpty(mUserID)) {
            populateNoData();
            return;
        }

        cleanErrorAndInfoMessages();
        initPropertiesAndLayout();
        retrieveData();
        doHideMenuPageContent();
        evaluateErrorAndInfo();
    }
    
    private void populateNoData() {
        cleanErrorAndInfoMessages();
        
        View progressBarSection = findViewById(R.id.progressBarLayoutID);
        progressBarSection.setVisibility(View.GONE);
        
        View bodyMainSection = findViewById(R.id.bodyMainSectionID);
        bodyMainSection.setVisibility(View.INVISIBLE);

        addErrorOrInfoMessages(
                true, 
                getResources().getString(R.string.error_user_illegalAccess));
        
        evaluateErrorAndInfo();
    }
    
    private void initPropertiesAndLayout() {
        mUserDetailFragment = (UserDetailFragment) getFragmentManager().findFragmentById(R.id.userDetailFragmentID);
        
        //Views variables:
        mBodyMainSection = findViewById(R.id.bodyMainSectionID);
        mMenuPageShow = (Button) findViewById(R.id.menuPageShowID);
        mMenuPageHide = (Button) findViewById(R.id.menuPageHideID);
        mMenuPageContent = findViewById(R.id.menuPageContentID);
        if(!mIsUserLoginProfile || !mIsUserAuthenticated) {
            mMenuPageShow.setVisibility(View.GONE);
            mMenuPageHide.setVisibility(View.GONE);
            mMenuPageContent.setVisibility(View.GONE);
            mMenuPageShow = null;
            mMenuPageHide = null;
            mMenuPageContent = null;
        }
    }
    
    private void retrieveData() {
        if(mIsUserLoginProfile) {
            //userLogin profile and the bean should be ready (from Session):
            mBodyMainSection.setVisibility(View.VISIBLE);
            evaluateUserLoginData();
            populateFragments();
            populateTitle();

        } else {
            //userOther's profile:
            if(mUserOther != null) {
                //userOther profile and the bean is ready:
                mBodyMainSection.setVisibility(View.VISIBLE);
                populateFragments();
                populateTitle();
                
            } else {
                //Retrieve from back-end servers:
                mBodyMainSection.setVisibility(View.INVISIBLE);

                //Call remote servers for fresh data:
                mProgressDialog = 
                        NoticeMessageUtil.showProgressDialog(
                                this, 
                                getLayoutInflater(), 
                                getResources().getString(R.string.label_user_profile), 
                                getResources().getString(R.string.info_common_pleaseWait));
                
                UserRetrieveAsyncTask backendTask = new UserRetrieveAsyncTask(this, mUserID, mIsUserLoginProfile);
                backendTask.execute();
            }
        }        
    }
    
    /**
     * @see BaseActivity.onAsyncBackendTaskCompleted()
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        //Reset refresh flag regardless the ops was due to refresh or not:
        mIsRefreshInProgress = false;
        
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            if(mIsUserLoginProfile) {
                mUserLogin = result.getListOfUsers().get(0);
                if(mUserLogin == null) {
                    addErrorOrInfoMessages(
                            true, 
                            getResources().getString(R.string.error_user_profile_empty));
                    evaluateErrorAndInfo();
                    return false;
                }
                
                //Else:
                SessionUtil.setUserLoginDataInSession(
                        (TrekcrumbApplicationImpl) getApplicationContext(),
                        mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(this, mUserLogin)); 
                
            } else {
                mUserOther = result.getListOfUsers().get(0);
                if(mUserOther == null) {
                    addErrorOrInfoMessages(
                            true, 
                            getResources().getString(R.string.error_user_profile_empty));
                    evaluateErrorAndInfo();
                    return false;
                }
            }
            
            populateFragments();
            populateTitle();
        }
        
        return isSuccess;
    }        
    
    private void populateFragments() {
        /*
         * Note 7/2014:
         * In case of pause/resume, the fragment would automatically reload itself
         * with all data saved on onSaveInstanceState(). Thus, this refresh() call may trigger
         * the same operation twice.
         */
        if(mIsUserLoginProfile) {
            mUserDetailFragment.refresh(true, mUserLogin, null);
        } else {
            //userOther exits possibly due to pause/resume. Only refresh when it is not null.
            if(mUserOther != null) {
                mBodyMainSection.setVisibility(View.VISIBLE);
                mUserDetailFragment.refresh(false, null, mUserOther);
            }
        }
    }
    
    private void populateTitle() {
        TextView titlePage = (TextView) findViewById(R.id.titleID);
        if(mIsUserLoginProfile) {
            titlePage.setText(mUserLogin.getFullname());
        } else {
            if(mUserOther != null) {
                titlePage.setText(mUserOther.getFullname());
            }
        }
    }

    private void doShowMenuPageContent() {
        if(!mIsUserLoginProfile || !mIsUserAuthenticated) {
            return;
        }
        
        mMenuPageShow.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.VISIBLE);
    }
    
    private void doHideMenuPageContent() {
        if(!mIsUserLoginProfile || !mIsUserAuthenticated) {
            return;
        }

        mMenuPageShow.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.GONE);
    }
    
    private void doUserEditActivity() {
        /*
         * NOTE 8/2014: 
         * UserEditActivity cannot be declared with "Intent.FLAG_ACTIVITY_NO_HISTORY"
         * because it needs to open a 3rd app: Pic Gallery or Camera, and then must
         * return to the prior UserEditActivity form after selection. Declaring 
         * no history would remove it from stack, and prevent onActivityResult() being called
         * by the 3rd app. 
         * If success leads to result page that does 'FLAG_ACTIVITY_CLEAR_TOP', it will erase
         *  ALL prior activities from stack anyway. 
         */
        Intent targetIntent = new Intent(this, UserEditActivity.class);
        startActivity(targetIntent);
    }    
}
