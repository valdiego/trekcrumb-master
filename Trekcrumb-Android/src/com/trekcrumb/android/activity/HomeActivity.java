package com.trekcrumb.android.activity;
                                                 
import java.io.File;
import java.util.ArrayList;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.UserAuthTokenAuthenticateAsyncTask;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.business.UserManager;
import com.trekcrumb.android.database.DatabaseSQLiteOpenHelperImpl;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.DeviceUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.exception.TrekcrumbException;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * The MAIN application activity where it initiates all components necessary to start the app.
 * 
 */
public class HomeActivity extends BaseActivity {
    private static final String LOG_TAG = "HomeActivity";
    
    /*
     * mIsKsipInit: Determines if to initialize system components (DB, connectivity, etc.)
     * or to skip it all (in case of android app pause/stop events.
     */
    private boolean mIsSkipInit;
    
    /**
     *  See BaseActivity.onCreate()
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_home);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                mIsSkipInit = getIntent().getExtras().getBoolean(CommonConstants.KEY_SESSIONOBJ_HOME_SKIP_INIT);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mIsSkipInit = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_HOME_SKIP_INIT);
        }

        //Android Release Req. PS-P2
        DeviceUtil.enableStrictMode();
        
        init(savedInstanceState);
    }

    /**
     *  See BaseActivity.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsSkipInit);
    }
    
    @Override
    public void refresh() {
        /*
         * TODO (02/2017): It could be incorrect to invoke init() for a refresh - in particular
         * it would force user reauthentication routine. 
         * Instead, when we will implement it in future, it should only about retrieving any necessary 
         * records (trips, pictures, places, etc.) for gallery fragments. 
         */
        mIsSkipInit = false;
        init(null);
    }
    
    private void init(Bundle savedInstanceState) {
        cleanErrorAndInfoMessages();
        initiatePreErrorOrInfoMsg(savedInstanceState);

        if(!mIsSkipInit) {
            /*
             * Android Release Req. PS-P2:
             * 1. See DeviceUtil.enableStrictMode() for details. 
             * 2. In this crucial steps, we must read/write SQLLite and SD Drive from main thread, 
             *    instead of using async threads; it causes StrictMode to fail due to violation. 
             *    To ignore the violation, we temporarily gave permissions to read/write here:
             *    'permitDiskReads' and 'permitDiskWrites'
             *    THEN, reset the StrictMode policy back afterwards.
             */
            StrictMode.ThreadPolicy validStrictMode = StrictMode.getThreadPolicy();
            if(validStrictMode != null) {
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(validStrictMode)
                .permitDiskWrites()
                .permitDiskReads()
                .build());
            }

            boolean isSuccess = initiateDatabase();
            if(isSuccess) {
                isSuccess = initiateAppDataFolders();
            }
            if(isSuccess) {
                initiateUser();
            }
            SessionUtil.setAppInitStatus((TrekcrumbApplicationImpl) getApplicationContext(), isSuccess);

            if(validStrictMode != null) {
                StrictMode.setThreadPolicy(validStrictMode);
            }
        }
        
        evaluateErrorAndInfo();
    }
    
    @SuppressWarnings("unchecked")
    private void initiatePreErrorOrInfoMsg(Bundle savedInstanceState) {
        if(savedInstanceState == null) {
            //Freshly created from raw:
            if(getIntent() != null
                    && getIntent().getExtras() != null) {
                Bundle intentBundleExtra = getIntent().getExtras();
                mListOfErrorMsg = (ArrayList<String>)intentBundleExtra.getSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
                mInfoMsg = intentBundleExtra.getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mListOfErrorMsg = (ArrayList<String>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
            mInfoMsg = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
        }
    }
    
    /*
     * Initializes app database when necessary.
     */
    private boolean initiateDatabase() {
        DatabaseSQLiteOpenHelperImpl dbHelper = DatabaseSQLiteOpenHelperImpl.getInstance(this);
        try {
            dbHelper.initializeDataBase();
            return true;
            
        } catch(TrekcrumbException e) {
            Log.e(LOG_TAG, "initiateDatabase() - ERROR: " + e.getMessage());
            e.printStackTrace();
            handleInitFailed( getResources().getString(R.string.error_system_appDB_CreateOrLoadFail));
            return false;
            
        } finally {
            try {
                dbHelper.closeDatabase();
            } catch (Exception e) {} //Cant do anything
        }
    }
    
    private boolean initiateAppDataFolders() {
        if(!DeviceUtil.isSDCardAccessible()) {
            Log.e(LOG_TAG, "initiateAppDataFolders() - ERROR: Device SD card is NOT accessible or properly mounted!");
            handleInitFailed( getResources().getString(R.string.error_system_device_SDCardNotAvailable));
            return false;

        } else {
            if (!new File(AndroidConstants.APP_LOCAL_DIR_PICTURES).exists()) {
                Log.d(LOG_TAG, "initiateAppDataFolders() - Making new app directory: " + AndroidConstants.APP_LOCAL_DIR_PICTURES);
                new File(AndroidConstants.APP_LOCAL_DIR_PICTURES).mkdirs();
            }
            if (!new File(AndroidConstants.APP_LOCAL_DIR_CACHE).exists()) {
                Log.d(LOG_TAG, "initiateAppDataFolders() - Making new app directory: " + AndroidConstants.APP_LOCAL_DIR_CACHE);
                new File(AndroidConstants.APP_LOCAL_DIR_CACHE).mkdirs();
            }
        }
        return true;
    }
    
    /*
     * Finds login user:
     * 1. Check userLogin from Session
     * 2. If none, check to find stored UserAuthToken
     *    2.1. If none found, there is no user and open page as guest
     *    2.2. If found, attempt to authenticate the stored user.
     */
    private void initiateUser() {
        mUserLogin = ((TrekcrumbApplicationImpl) getApplicationContext()).getUserLogin();

        if(mUserLogin == null) {
            UserAuthToken userAuthToken = UserManager.userAuthTokenRetrieve(this);
            if(userAuthToken != null) {
                //Call back-end (must be async-task due to network op.)
                mProgressDialog = NoticeMessageUtil.showProgressDialog(
                        this, 
                        getLayoutInflater(), 
                        getResources().getString(R.string.label_user_login), 
                        getResources().getString(R.string.info_userauthToken_pleaseWaitAutoLogin));

                UserAuthTokenAuthenticateAsyncTask backendTask = 
                        new UserAuthTokenAuthenticateAsyncTask(this, userAuthToken);
                backendTask.execute();
            }
        }
    }
    
    /**
     * Callback by the back-end async task once it is done.
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        super.onAsyncBackendTaskCompleted(true);
        
        if(result == null) {
            mUserLogin = null;
            SessionUtil.setUserLoginDataInSession(
                   (TrekcrumbApplicationImpl) getApplicationContext(), mUserLogin, false, false);
            ActivityUtil.doHomeActivity(this, true, true, getResources().getString(R.string.error_user_autoLogin_SystemError));
            return false;
        }
        
        boolean isSuccess = result.isSuccess();
        if(!isSuccess) {
            //Failed:
            String errMsg = null;
            if(result.getServiceError() == null) {
                mUserLogin = null;
                errMsg = getResources().getString(R.string.error_user_autoLogin_SystemError);
            } else {
                ServiceErrorEnum errorEnum = result.getServiceError().getErrorEnum();
                if(errorEnum == ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR) {
                    mUserLogin = null;
                    errMsg = getResources().getString(R.string.error_user_autoLogin_AuthFailed);
                    
                } else if(errorEnum == ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR) {
                    mUserLogin = null;
                    errMsg = getResources().getString(R.string.error_user_LoginFailed_deactivated);
                    
                } else if(errorEnum == ServiceErrorEnum.SYSTEM_NETWORK_NOT_AVAILABLE_ERROR
                        || errorEnum == ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR) {
                    mUserLogin = result.getUser();
                    errMsg = getResources().getString(R.string.error_user_autoLogin_NetworkError);
                    
                } else {
                    mUserLogin = null;
                    errMsg = getResources().getString(R.string.error_user_autoLogin_SystemError);
                }
            }
            
            SessionUtil.setUserLoginDataInSession(
                    (TrekcrumbApplicationImpl) getApplicationContext(), mUserLogin, false, false);
            ActivityUtil.doHomeActivity(this, true, true, errMsg);

        } else {
            //Success:
            mUserLogin = result.getUser();
            SessionUtil.setUserLoginDataInSession(
                    (TrekcrumbApplicationImpl) getApplicationContext(),
                    mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(this, mUserLogin)); 
            ActivityUtil.doHomeActivity(this, true, false, null);
        }
        
        return isSuccess;
    }
    
    private void handleInitFailed(String errMsg) {
        SessionUtil.setAppInitStatus((TrekcrumbApplicationImpl) getApplicationContext(), false);
        ActivityUtil.doHomeActivity(this, true, true, errMsg);
    }
    
}
