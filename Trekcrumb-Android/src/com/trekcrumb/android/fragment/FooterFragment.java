package com.trekcrumb.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.SupportActivity;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.utility.CommonConstants;

public class FooterFragment 
extends BaseFragment {
    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_footer, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        init();
    }
    
    private void init() {
        setupClickables();
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        View menuTerms = mFragmentRootView.findViewById(R.id.supportTermsID);
        menuTerms.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doSupportActivity(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_TERMS);
                    }
                }); 
        
        View menuPrivacy = mFragmentRootView.findViewById(R.id.supportPrivacyID);
        menuPrivacy.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doSupportActivity(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_PRIVACY);
                    }
                }); 
        
        View menuFAQ = mFragmentRootView.findViewById(R.id.supportFAQID);
        menuFAQ.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doSupportActivity(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ);
                    }
                }); 

        View menuContactUs = mFragmentRootView.findViewById(R.id.supportContactUsID);
        menuContactUs.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doSupportActivity(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_CONTACTUS);
                    }
                }); 

    }
    
    private void doSupportActivity(String supportType) {
        if(getActivity() instanceof SupportActivity) {
            ((SupportActivity) getActivity()).refresh(supportType);
            return;
        }

        if(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ.equalsIgnoreCase(supportType)) {
            ActivityUtil.doSupportFAQActivity(getActivity());
            return;
        }
        
        //Else:
        Intent intent = new Intent(getActivity(), SupportActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(CommonConstants.KEY_SESSIONOBJ_SUPPORT_TYPE, supportType);
        intent.putExtras(bundle);        
        getActivity().startActivity(intent);
    }
    
    
}
