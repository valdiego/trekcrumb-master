package com.trekcrumb.android.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Base class for app fragments providing common variables, methods and business logics.
 *
 */
public abstract class BaseFragment extends Fragment {
    protected View mFragmentRootView;
    protected User mUserLogin; 
    protected boolean mIsUserLoginTrip;
    protected boolean mIsUserLoginProfile;
    protected boolean mIsUserAuthenticated;
    protected boolean mIsUserTripSynced;

    /**
     *  Callback when after fragment is created, its view initialized and the 
     *  hosting (parent) activity is created. It should contain any final initialization or
     *  re-populate (re-store) any dynamic data in place.     
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        //Get Session objects/values:
        mUserLogin = ((TrekcrumbApplicationImpl) getActivity().getApplicationContext()).getUserLogin();
        mIsUserAuthenticated = ((TrekcrumbApplicationImpl) getActivity().getApplicationContext()).isUserAuthenticated();
        mIsUserTripSynced = ((TrekcrumbApplicationImpl) getActivity().getApplicationContext()).isUserTripSynced();
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mIsUserLoginTrip = dataBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED);
                mIsUserLoginProfile = dataBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE);
            }

        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mIsUserLoginTrip = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED);
            mIsUserLoginProfile = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE);
        }
    }
    
    /**
     * Callback when to save any current dynamic data (state) before pause, so 
     * it can later be reconstructed in a new instance when the fragment is restarted.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, mIsUserLoginTrip);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE, mIsUserLoginProfile);
    }
    
    /**
     * Wrapper method to fragment's parent BaseActivity.cleanErrorAndInfoMessages()
     */
    public void cleanErrorAndInfoMessages() {
        ((BaseActivity) getActivity()).cleanErrorAndInfoMessages();
    }

    /**
     * Wrapper method to fragment's host BaseActivity.addErrorOrInfoMessages()
     */
    public void addErrorOrInfoMessages(boolean isError, String msg) {
        ((BaseActivity) getActivity()).addErrorOrInfoMessages(isError, msg);
    }

    /**
     * Wrapper method to fragment's host BaseActivity.evaluateErrorAndInfo()
     */
    public void evaluateErrorAndInfo() {
        ((BaseActivity) getActivity()).evaluateErrorAndInfo();
    }
    
    /**
     * Wrapper method to fragment's host BaseActivity.hideKeyboard()
     */
    public void hideKeyboard() {
        ((BaseActivity) getActivity()).hideKeyboard();
    }
    
    /**
     * Wrapper method to fragment's host BaseActivity.onAsyncBackendTaskCompleted()
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        return ((BaseActivity) getActivity()).onAsyncBackendTaskCompleted(result);
    }    

    /**
     * Children classes, whenever applicable, would need to implement the concrete logic.
     */
    public void refresh(Trip trip) {
        Log.w("Trekcrumb curent fragment", "refresh() - Not implemented.");
    }
    

}
