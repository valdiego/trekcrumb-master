package com.trekcrumb.android.fragment;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.TripEditActivity;
import com.trekcrumb.android.layoutholder.TripDetailLayoutHolder;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Trip Detail fragment
 * 1. Represents the page for all detail contents about the Trip, excluding Map and Picture Detail. 
 *    Not all contents are linear; some of them are also sub-Fragments.
 * 
 * 2. Issues and Resolutions
 *    (1) Issue: Fragments within Fragment Auto-Create
 *    This fragment contains sub-fragments such as PictureCoverFragment, PictureGalleryFragment,
 *    PlaceListByTripFragment, etc. which all of them are added programatically and not defined
 *    in the layout. When created from raw, we simply add new Fragments to each of these. However,
 *    after STOP/PAUSE state, Android engine preserves these fragments in FragmentManager, and 
 *    re-init them automatically along with their preserved states/variables, if any. 
 *    Commonly, we want to REUSE these 'sticky' fragments so we do not lose their preserved 
 *    states/variables; instead of remove/replace them with new fragment instances. 
 *    Some fragment such as PictureCoverFragment would be too costly if we remove/replace it after
 *    stop/pause and auto-create because it involves loading picture image ops.
 *    Exception applies to specific fragment(s) as we describe below.
 *    
 *    (2) Issue: Fragment within Fragment already 'resumed' during Tab swap
 *    When parent Activity consists of Tabs containing this Fragment, user can hop from Tab to Tab.
 *    Unlike stop/pause state, the 'Tab Hopping' event could lead to FALSE FLAG whern stored fragments
 *    retrieved from FragmentManager has 'isResumed()' returned TRUE. This false flag caused Android
 *    engine not to re-init these Fragments, anc cause them failed to show up at the page. 
 *    To resolve this issue, we invoke FragmentTransaction's "detach() and attach()" the fragment 
 *    in question, a series of action that is similar to "add()" ops, thus forcing Android to 
 *    init these fragments and ensure they show up properly.
 *    
 *    (3) Issue: Fragment variables LOST during detach/attach ops
 *    Unlike stop/pause state where we could preserve fragment variables on 'onSaveInstanceState()',
 *    Tab Hopping with detach/attach combo would not have any access to that saved bundle (it is NULL),
 *    thus we would LOSE any fragment variable(s) we need to preserve when the fragment is re-attach.
 *    Example: offset value, index selected value, etc. We need to avoid this.
 *    To resolve this issue, we must know that one thing that will never be destroyed is fragment's 
 *    ARGUMENT Bundle that we set during its first initialization: Fragment.setArguments(Bundle).
 *    Therefore, in EACH fragment in question, we must put any variable and its updated value
 *    into the argument so that variable will not be lost after detach/attach ops.
 *    
 * 3. PictureCoverFragment
 *    => See their javadoc for their own issues and resolutions.
 * 
 * 4. PictureGalleryFragment
 *    => See its javadoc for its own issues and resolutions.
 *    => Note on addressing (2) Issue: Fragment within Fragment already 'resumed' during Tab swap:
 *       The old fragment was attached to the old instance of fragment layout (i.e., 'picGallerySectionID' layout). 
 *       Since Android framework re-init all fragments after every tab hopping, it recreates a new
 *       picGallerySectionID layout instance, and the old one is removed. Thus while the fragment was
 *       "resumed", it won't show up. When we resolve the issue by invoking "detach() and attach()", 
 *       be aware that the ops could throw exception during re-attach: 
 *       "The specified child already has a parent. You must call removeView() on the child's parent first".
 *       This is because inside gallery fragment code, we re-add existing ImageView objects into the
 *       fragment, but these ImageViews are still attached to the previous picGallerySectionID 
 *       layout, thus the exception.
 *
 * @author Val Triadi
 * @since 6/2014, 2/2017
 */
public class TripDetailFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "TripDetailFragment";
    public static final String TAG_FRAGMENT_NAME = TripDetailFragment.class.getName();
    
    private Trip mTrip;
    private String mInfoMsg;
    private boolean mIsShowMenuPageContent;
    
    //Views
    private Button mMenuPageShow;
    private Button mMenuPageHide;
    private View mMenuPageContent;
    private Button mMenuShare;
    private Button mMenuTripEdit;
    private Button mMenuPictureAdd;
    private Button mMenuPlaceAdd;
    
    //Views for Scrolling:
    private ScrollView mMainScrollLayout;  
    private View mFavoriteListSection;  
    private View mCommentListSection;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_trip_detail, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mTrip = (Trip)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mInfoMsg = dataBundle.getString(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mIsShowMenuPageContent = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
        }
        
        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, mIsShowMenuPageContent);
    }
    
    @Override
    public void refresh(Trip trip) {
        mTrip = trip;
        
        //Ensure the fragment is attached to an activity before relaunch the init()
        if(getActivity() != null) {
            init();
        }
    }
    
    private void init() {
        if(mTrip == null) {
            populateNoData();
            return;
            
        } else if(mIsUserLoginTrip) {
            if(mTrip.getPublish() == TripPublishEnum.PUBLISH) {
                //Only give these warnings if trip is published:
                if(!mIsUserAuthenticated) {
                    addErrorOrInfoMessages(false, getResources().getString(R.string.info_user_not_authenticated));
                } else if(!mIsUserTripSynced) {
                    addErrorOrInfoMessages(false, getResources().getString(R.string.error_trip_recordsOutOfDate));
                }
            } else {
                /*
                 * Display any carry-over info message, if any.
                 * Note: this infoMsg shall only be displayed once, and not be saved into onSaveInstanceState()
                 */
                if(!ValidationUtil.isEmpty(mInfoMsg)) {
                    addErrorOrInfoMessages(false, mInfoMsg);
                }
            }
        }

        initPropertiesAndLayout();
        setupClickables();
        populateDataValues();
        initPictureCover();
        initPictureGallery();
        initPlaceList();
        initFavoriteList();
        initCommentList();
        evaluateErrorAndInfo();
    }
    
    private void populateNoData() {
        String errMsg = getResources().getString(R.string.error_trip_notFound);
        addErrorOrInfoMessages(true, errMsg);
        evaluateErrorAndInfo();
        
        ViewGroup bodyContentLayout = (ViewGroup) mFragmentRootView.findViewById(R.id.bodyMainSectionID);
        bodyContentLayout.removeAllViews();
        TextView errorView = 
                (TextView) getActivity().getLayoutInflater().inflate(R.layout.template_textview_error, null);
        errorView.setText(errMsg);
        bodyContentLayout.addView(errorView);
    }
    
    /*
     * Dynamically initialize and modify layout due to specific conditions.
     */
    private void initPropertiesAndLayout() {
        //Menu Views:
        mMenuPageShow = (Button) mFragmentRootView.findViewById(R.id.menuPageShowID);
        mMenuPageHide = (Button) mFragmentRootView.findViewById(R.id.menuPageHideID);
        mMenuPageContent = mFragmentRootView.findViewById(R.id.menuPageContentID);
        
        if(mIsUserLoginTrip) {
            if(mTrip.getPublish() == TripPublishEnum.PUBLISH
                    && (!mIsUserAuthenticated || !mIsUserTripSynced)) {
                doHideMenuForUserLoginTrip();
            } else {
                mMenuTripEdit = (Button) mFragmentRootView.findViewById(R.id.menuTripEditID);
                mMenuPictureAdd = (Button) mFragmentRootView.findViewById(R.id.menuPictureAddID);
                mMenuPlaceAdd = (Button) mFragmentRootView.findViewById(R.id.menuPlaceAddID);
                if(mTrip.getStatus() != TripStatusEnum.ACTIVE) {
                    mMenuPlaceAdd.setVisibility(View.GONE);
                    mMenuPlaceAdd = null;
                }
            }
            
            //TODO (4/2017): Hide ALL MenuPage until we implement Comment Add, Favorite Add and Share
            //Meanwhile, only do this for userLoginTrip 
            doHideMenuPageContent();

        } else {
            doHideMenuForUserLoginTrip();

            //TODO (4/2017): Hide ALL MenuPage until we implement Comment Add, Favorite Add and Share
            mMenuPageShow.setVisibility(View.GONE);
            mMenuPageHide.setVisibility(View.GONE);
            mMenuPageContent.setVisibility(View.GONE);
        }
    }

    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        View tripUserSection = mFragmentRootView.findViewById(R.id.tripUserSectionClickableID);
        if(tripUserSection != null) {
            tripUserSection.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            ActivityUtil.doUserViewActivity(getActivity(), mTrip.getUserID());
                        }
                    }); 
        }
        
        View menuPlaceSummary = mFragmentRootView.findViewById(R.id.placeIconActiveID);
        if(menuPlaceSummary != null) {
            menuPlaceSummary.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            getActivity().getActionBar().setSelectedNavigationItem(1);
                        }
                    }); 
        }
        
        View menuPictureSummary = mFragmentRootView.findViewById(R.id.picIconActiveID);
        if(menuPictureSummary != null) {
            menuPictureSummary.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            getActivity().getActionBar().setSelectedNavigationItem(2);
                        }
                    }); 
        }
        
        View menuFavoriteSummary = mFragmentRootView.findViewById(R.id.faveIconActiveID);
        if(menuFavoriteSummary != null) {
            menuFavoriteSummary.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doScrollToFavorites();
                        }
                    }); 
        }

        View menuCommentSummary = mFragmentRootView.findViewById(R.id.commentIconActiveID);
        if(menuCommentSummary != null) {
            menuCommentSummary.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doScrollToComments();
                        }
                    }); 
        }
        
        mMenuPageShow.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mIsShowMenuPageContent = true;
                        doShowMenuPageContent();
                    }
                }); 
        
        mMenuPageHide.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mIsShowMenuPageContent = false;
                        doHideMenuPageContent();
                    }
                });
        
        if(mMenuShare != null) {
            mMenuShare.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            //TODO
                        }
                    }); 
        }

        if(mMenuTripEdit != null) {
            mMenuTripEdit.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doTripEditActivity();
                        }
                    }); 
        }
        if(mMenuPictureAdd != null) {
            mMenuPictureAdd.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            ActivityUtil.doPictureCreateActivity(getActivity(), mTrip);
                        }
                    }); 
        }
        if(mMenuPlaceAdd != null) {
            mMenuPlaceAdd.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            ActivityUtil.doPlaceCreateActivity(getActivity(), mTrip, null);
                        }
                    }); 
        }
    }
    
    private void populateDataValues() {
        TripDetailLayoutHolder layoutHolder = new TripDetailLayoutHolder(mFragmentRootView);
        layoutHolder.populateLayoutWithValues(getActivity(), mTrip, mIsUserLoginTrip);
    }
    
    private void initPictureCover() {
        ViewGroup picCoverSection = (ViewGroup) mFragmentRootView.findViewById(R.id.picCoverSectionID);
        PictureCoverFragment picCoverFragment = 
                (PictureCoverFragment)getFragmentManager().findFragmentByTag(PictureCoverFragment.TAG_FRAGMENT_NAME);
        if(mTrip.getNumOfPictures() == 0) {
            picCoverSection.setVisibility(View.GONE);

            if(picCoverFragment == null) {
                //Done, nothing else to do:
                return;
            } else {
                //Trip with empty pictures should not have PictureCover fragment at all! Remove it: 
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(picCoverFragment);
                fragmentTransaction.remove(picCoverFragment);
                fragmentTransaction.commit();
                picCoverFragment = null;                
                return;
            }
        }
        
        //Else:
        picCoverSection.setVisibility(View.VISIBLE);
        if(picCoverFragment != null) {
            //(1) Issue: Fragments within Fragment Auto-Create
            if(picCoverFragment.isResumed()) {
                //(2) Issue: Fragment within Fragment already 'resumed' during Tab swap (See Javadoc)
                detachAndAttachFragment(picCoverFragment);
            }
            return;
        }
        
        //Else:
        picCoverFragment = new PictureCoverFragment();
        Bundle picCoverDataBundle = new Bundle();
        picCoverDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        picCoverDataBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, 0);
        picCoverDataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, mIsUserLoginTrip);
        picCoverFragment.setArguments(picCoverDataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.picCoverSectionID, picCoverFragment, PictureCoverFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }            
    
    private void initPictureGallery() {
        ViewGroup picGallerySection = (ViewGroup) mFragmentRootView.findViewById(R.id.picGallerySectionID);
        PictureGalleryFragment picGalleryFragment = 
                (PictureGalleryFragment)getFragmentManager().findFragmentByTag(PictureGalleryFragment.TAG_FRAGMENT_NAME);
        if(mTrip.getNumOfPictures() < 2) {
            //Remove picture gallery
            if(picGalleryFragment != null) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(picGalleryFragment);
                fragmentTransaction.remove(picGalleryFragment);
                fragmentTransaction.commit();
                picGalleryFragment = null;                
            }
            picGallerySection.setVisibility(View.GONE);
            return;
        }
        
        //Else:
        picGallerySection.setVisibility(View.VISIBLE);
        if(picGalleryFragment != null) {
            //(1) Issue: Fragments within Fragment Auto-Create
            if(picGalleryFragment.isResumed()) {
                //(2) Issue: Fragment within Fragment already 'resumed' during Tab swap (See Javadoc)
                detachAndAttachFragment(picGalleryFragment);
            }
            return;
        }
        
        //Else:
        picGalleryFragment = new PictureGalleryFragment();
        Bundle picGalleryDataBundle = new Bundle();
        picGalleryDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        picGalleryDataBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, 0);
        picGalleryDataBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED, mIsUserLoginTrip);
        picGalleryFragment.setArguments(picGalleryDataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.picGallerySectionID, picGalleryFragment, PictureGalleryFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }    

    private void initPlaceList() {
        ViewGroup placeListSection = (ViewGroup) mFragmentRootView.findViewById(R.id.placeListSectionID);
        PlaceListByTripFragment placeListFragment = 
                (PlaceListByTripFragment)getFragmentManager().findFragmentByTag(PlaceListByTripFragment.TAG_FRAGMENT_NAME);
        if(mTrip.getNumOfPlaces() == 0) {
            placeListSection.setVisibility(View.GONE);
            
            if(placeListFragment == null) {
                //Done, nothing else to do:
                return;
            } else {
                //Trip with empty places should not have PlaceList fragment at all! Remove it: 
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(placeListFragment);
                fragmentTransaction.remove(placeListFragment);
                fragmentTransaction.commit();
                placeListFragment = null;
                return;
            }
        }
        
        //Else:
        placeListSection.setVisibility(View.VISIBLE);        
        if(placeListFragment != null) {
            //(1) Issue: Fragments within Fragment Auto-Create (See Javadoc)
            if(placeListFragment.isResumed()) {
                //(2) Issue: Fragment within Fragment already 'resumed' during Tab swap (See Javadoc)
                detachAndAttachFragment(placeListFragment);
            }
            return;
        }

        //Else:
        placeListFragment = new PlaceListByTripFragment();
        Bundle placeNoteDataBundle = new Bundle();
        placeNoteDataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        placeListFragment.setArguments(placeNoteDataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.placeListSectionID, placeListFragment, PlaceListByTripFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }    
        
    private void initCommentList() {
        CommentListByTripFragment commentListFragment = 
                (CommentListByTripFragment)getFragmentManager().findFragmentByTag(CommentListByTripFragment.TAG_FRAGMENT_NAME);
        if(commentListFragment != null) {
            //(1) Issue: Fragments within Fragment Auto-Create (See Javadoc)
            if(commentListFragment.isResumed()) {
                //(2) Issue: Fragment within Fragment already 'resumed' during Tab swap (See Javadoc)
                detachAndAttachFragment(commentListFragment);
            }
            return;
        }
        
        //Else:
        commentListFragment = new CommentListByTripFragment();
        Bundle dataBundle = new Bundle();
        dataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        commentListFragment.setArguments(dataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.commentListSectionID, commentListFragment, CommentListByTripFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }
    
    private void initFavoriteList() {
        FavoriteListByTripFragment favetListFragment = 
                (FavoriteListByTripFragment)getFragmentManager().findFragmentByTag(FavoriteListByTripFragment.TAG_FRAGMENT_NAME);
        if(favetListFragment != null) {
            //(1) Issue: Fragments within Fragment Auto-Create (See Javadoc)
            if(favetListFragment.isResumed()) {
                //(2) Issue: Fragment within Fragment already 'resumed' during Tab swap (See Javadoc)
                detachAndAttachFragment(favetListFragment);
            }
            return;
        }
        
        //Else:
        favetListFragment = new FavoriteListByTripFragment();
        Bundle dataBundle = new Bundle();
        dataBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        favetListFragment.setArguments(dataBundle);

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.favoriteListSectionID, favetListFragment, FavoriteListByTripFragment.TAG_FRAGMENT_NAME);
        fragmentTransaction.commit();
    }

    private void detachAndAttachFragment(BaseFragment fragment) {
        //(3) Issue: Fragment variables LOST during detach/attach ops (See Javadoc)
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.detach(fragment);
        fragmentTransaction.attach(fragment);
        fragmentTransaction.commit();
    }

    private void doShowMenuPageContent() {
        mMenuPageShow.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.VISIBLE);
    }
    
    private void doHideMenuPageContent() {
        mMenuPageShow.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.GONE);
    }
    
    private void doHideMenuForUserLoginTrip() {
        //No edit menus:
        View menuForUserLoginTrip = mFragmentRootView.findViewById(R.id.menuForUserLoginTripID);
        menuForUserLoginTrip.setVisibility(View.GONE);
    }
    
    private void doScrollToFavorites() {
        //Populate and open:
        FavoriteListByTripFragment favetListFragment = 
                (FavoriteListByTripFragment)getFragmentManager().findFragmentByTag(FavoriteListByTripFragment.TAG_FRAGMENT_NAME);
        if(favetListFragment != null
                && !favetListFragment.isDetached()
                && favetListFragment.isResumed()) {
            favetListFragment.doShowContent();
        }

        /*
         * Then scroll there. Note that we don't initialize the target views until after user triggers
         * this call.
         */
        if(mMainScrollLayout == null) {
            mMainScrollLayout = (ScrollView) mFragmentRootView.findViewById(R.id.mainScrollLayoutID);  
        }
        if(mFavoriteListSection == null) {
            mFavoriteListSection = mFragmentRootView.findViewById(R.id.favoriteListSectionID);  
        }
        mMainScrollLayout.smoothScrollTo(0, mFavoriteListSection.getTop());
    }

    private void doScrollToComments() {
        //Populate and open:
        CommentListByTripFragment commentListFragment = 
                (CommentListByTripFragment)getFragmentManager().findFragmentByTag(CommentListByTripFragment.TAG_FRAGMENT_NAME);
        if(commentListFragment != null
                && !commentListFragment.isDetached()
                && commentListFragment.isResumed()) {
            commentListFragment.doShowContent();
        }

        /*
         * Then scroll there. Note that we don't initialize the target views until after user triggers
         * this call.
         */
        if(mMainScrollLayout == null) {
            mMainScrollLayout = (ScrollView) mFragmentRootView.findViewById(R.id.mainScrollLayoutID);  
        }
        if(mCommentListSection == null) {
            mCommentListSection = mFragmentRootView.findViewById(R.id.commentListSectionID);  
        }
        mMainScrollLayout.smoothScrollTo(0, mCommentListSection.getTop());
    }

    private void doTripEditActivity() {
        Intent targetIntent = new Intent(getActivity(), TripEditActivity.class);
        
        //Prevent the edit page from stack activity:
        targetIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        
        Bundle bundle = new Bundle();
        bundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        targetIntent.putExtras(bundle);
        startActivity(targetIntent);
    }
    
    
}

