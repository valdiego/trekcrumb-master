package com.trekcrumb.android.fragment;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.PlaceCreateActivity;
import com.trekcrumb.android.activity.TripCreateActivity;
import com.trekcrumb.android.listener.IGeoLocationUpdateListener;
import com.trekcrumb.android.listener.IProgressWaitListener;
import com.trekcrumb.android.otherimpl.TrekcrumbLocationListenerImpl;
import com.trekcrumb.android.thread.ProgressWaitThread;
import com.trekcrumb.android.utility.DeviceUtil;
import com.trekcrumb.android.utility.GeoLocationUtil;
import com.trekcrumb.common.utility.CommonConstants;

@SuppressWarnings("serial")
public class GPSLocationFragment 
extends BaseFragment 
implements IGeoLocationUpdateListener, IProgressWaitListener {
    public static final String TAG_FRAGMENT_NAME = GPSLocationFragment.class.getName();
    private static final String TAG_LOGGER = "GPSLocationFragment";

    private TrekcrumbLocationListenerImpl mLocationListener;
    private ProgressWaitThread mThreadToWaitForLocation;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_gps_location, parentViewGroup, false);
        return mFragmentRootView;
    }

    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initGPSLocation();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped/destroyed 
     * state. Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        
        /*
         * We do not preserve the instances of mLocationListener and mThreadToWaitForLocation during
         * context stop/resume. Doing this, we enforce to always fire new GPS location search
         * everytime the app is stop/resume (device flip, etc.) to guarantee the hosting context
         * (activity or another fragment) receives the onGeoLocationUpdateCallback() call with
         * its location result.
         */
    }
    
    /**
     * Callback method called by the system when putting the activity to STOP state (stopped).
     */
    @Override
    public void onStop() {
        super.onStop();
        
        /*
         * Android Release Req. FN-51: 
         * 1. FN-S1: The app is in the background it should not leave any services running - 
         *    including network connection, GPS lookup, bluetooth connnection, etc
         *    unless it is part of its core function (such as Radio app, etc.). Hence, to pass this we
         *    must invoke our cancel() when the app is put in background.
         *    
         * 2. Understand activity/fragment lifecycle and the differences between onPause(), onStop() and 
         *    onDestroyView()!
         *    ==> onPause(): When app main view is obstructed but still visible by a pop-up dialog box, etc.
         *    ==> onStop(): When user returns to "device HOME" view
         *    ==> onDestroyView(): When user opens another app
         *    
         * 3. Warning: Calling cancel() here causes us to ALWAYS cancel during device orientation flip-flop.
         *    But the good news is, since this fragment does not preserve anything (see onSaveInstanceState()),
         *    we always re-initialize the GPS lookup op.   
         */
        cancel();
    }
    
    private void initGPSLocation() {
        if(!DeviceUtil.isGPSEnabled(getActivity())) {
            doHideProgressBar();
            notifyHostContext(null, R.string.error_system_device_GPSNotAvailable);
            return;
            
        } else {
            doShowProgressBar();
            
            mThreadToWaitForLocation = 
                    new ProgressWaitThread(getActivity(), this, CommonConstants.PROGRESS_WAIT_TIME_30_SECONDS);
            mThreadToWaitForLocation.start();

            //REAL-CODE:
            mLocationListener = new TrekcrumbLocationListenerImpl(this);
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            GeoLocationUtil.findCurrentLocation(locationManager, mLocationListener);
            //END REAL-CODE
            
            //MOCKED CODE:
            //MockedResources.findCurrentLocation(getActivity(), this);
            //END MOCKED CODE
 
            /*
             * When done, backend code will callback to either 
             * onUserGeoLocationChangeCallback() or onProgressCompletedCallback():
             */
        }       
    }
    
    /**
     * Callback method.
     * @see IProgressWaitListener
     */
    @Override
    public void onProgressCompletedCallback() {
        onGeoLocationUpdateCallback(null);
    }
    
    /** 
     * Callback method by LocationManager via IGeoLocationUpdateListener when the GPS completes
     * its location search.
     * @see IGeoLocationUpdateListener
     */
    @Override
    public void onGeoLocationUpdateCallback(Location location) {
        cancel();
        doHideProgressBar();
        notifyHostContext(location, -1);
    }
    
    private void notifyHostContext(Location location, int errorMsgID) {
        if(getActivity() == null || getFragmentManager() == null) {
            /*
             * Note: It is possible when the callback was made by the backend thread, the hosting
             * activity has become null or incomplete due to user navigates to other view (activity or fragments)
             */
            Log.e(TAG_LOGGER, "notifyHostContext() - ERROR! This fragment is NOT attached to any Activity or parent Fragment! Abandoned the result.");
            return;
            
        } else {
            String errorMsg = null;
            if(location == null) {
                if(errorMsgID > 0) {
                    errorMsg = getResources().getString(errorMsgID);
                } else {
                    errorMsg = getResources().getString(R.string.error_system_device_CurrentLocationNotAvailable);
                }
            }

            /*
             * Call specific callback methods to specific hosting parent (an activity or a parent fragment)
             * to report the location result. 
             */
            if(getActivity() instanceof TripCreateActivity) {
                ((TripCreateActivity)getActivity()).onGPSLocationReadyCallback(location, errorMsg);
                
            } else if(getActivity() instanceof PlaceCreateActivity) {
                ((PlaceCreateActivity)getActivity()).onGPSLocationReadyCallback(location, errorMsg);
                
            } else {
                MapDetailFragment mapDetailFragment = 
                        (MapDetailFragment)getFragmentManager().findFragmentByTag(MapDetailFragment.TAG_FRAGMENT_NAME);
                if(mapDetailFragment != null) {
                    mapDetailFragment.onGPSLocationReadyCallback(location, errorMsg);
                } else {
                    Log.e(TAG_LOGGER, "notifyHostContext() - ERROR! Hosting parent of this fragment is UNKNOWN! Abandoned the result.");
                }
            }
        }
    }
    
    public void cancel() {
        Log.w(TAG_LOGGER, "cancel() - GPS lookup is being canceled!");

        //Shut down GeoLocation update request:
        if(mLocationListener != null) {
            if(getActivity() != null
                    && getActivity().getSystemService(Context.LOCATION_SERVICE) != null) {
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                locationManager.removeUpdates(mLocationListener);
            }
            mLocationListener = null;
        }
        
        //Shut down waiting thread:
        if(mThreadToWaitForLocation != null) {
            mThreadToWaitForLocation.interrupt();
            mThreadToWaitForLocation = null;
        }
    }
    
    private void doShowProgressBar() {
        LinearLayout progressBarLayout = 
                (LinearLayout) mFragmentRootView.findViewById(R.id.gpsLocProgressBarLayoutID);
        if(progressBarLayout != null) {
            progressBarLayout.setVisibility(View.VISIBLE);
        }
    }
    
    private void doHideProgressBar() {
        LinearLayout progressBarLayout = 
                (LinearLayout) mFragmentRootView.findViewById(R.id.gpsLocProgressBarLayoutID);
        if(progressBarLayout != null) {
            progressBarLayout.setVisibility(LinearLayout.GONE);
        }    
    }

}

