package com.trekcrumb.android.fragment;

import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PictureImageRetrieveAsyncTask;
import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.utility.SingletonFactory;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Picture Gallery fragment
 * 1. Represents a gallery of picture images' thumbnails where user can scroll/browse horizontally
 *    left/right to view images. We could have used HorizontalScroll layout, but we chose PageViewer.
 * 2. PageViewer
 *    2.1. Typical usage and default design of ViewPager was to occupy the entire page, and display
 *         one content at a time. Here, however, we accomodate the ViewPager as part of this fragment
 *         and therefore can be a part of content within a page. Furthermore, each swipe (PagerViewer)
 *         would contain one or MORE contents (images) depending the width of the area (Fragment).
 *    2.2. When declaring/initiating PageViewer layout, we MUST set explicitly the height either 
 *         in XML layout or in code programatically. We cannot use 'wrap_content' or '0dp' because 
 *         ViewPager will not show up.
 *         (REF: http://stackoverflow.com/questions/29152674/viewpager-not-showing-inside-recyclerview-row/31953464#31953464)
 *
 * 3. PagerAdapter
 *    3.1. In order to use PageViewer, we must implement a PageAdapter, hence GalleryImagesPagerAdapterImpl class.
 *    3.2. This PageAdapter impl is the engine for ViewPager, doing heavy workload to display each
 *         PageViewer, to retrieve and populate those images, etc.
 *
 * 4. ViewPagerPageChangeListenerImpl
 *    (REF: http://stackoverflow.com/questions/8117523/how-do-you-get-the-current-page-number-of-a-viewpager-for-android)

 * 5. Images
 *    5.1. The images' View must be ImageView objects, cannot be TextView/Button, because they
 *         will be programatically populated with the actual image bitmaps.
                  
   6. Issues and Resolutions
 *    (1) Issue: Parent Width dimension
 *    We NEED the container (parent) view's width as part of initialization to do some math 
 *    calculations. However, any call of view.getWidth()/getMeaseuredWidth() would 
 *    return either 0 or -1 during Activity/Fragment creation when the XML layout does not specify 
 *    explicit value in dp/px, or its content is empty. The actual value would only be calculated 
 *    by the System once all the View objects got created and laid out. To resolve this, we would
 *    do the actual initialization by launching a runnable listener, AFTER a post().
 *    (REF: http://stackoverflow.com/questions/3591784/getwidth-and-getheight-of-view-returns-0)
 *
 *    (2) Issue: ScrollView vs. PageViewer scrolling conflicts
 *    IF parent layout is a ScrollView, there will be conflicts/fights for user swipe 
 *    between the ScrollView and PageViewer: The PageViewer could only work if user swipe it
 *    exactly horizontal straight; but even this is not guaranteed and most often ScrollView
 *    won. Hence, we disable ScrollView (parent) swipe on touch listener whenever user touches
 *    PageViewer section. 
 *    (REF: http://stackoverflow.com/questions/8381697/viewpager-inside-a-scrollview-does-not-scroll-correclty)
 * 
 *    (3) Issue: Fragment(s) auto-created but getActivity() NULL
 *    After pause/stop (due to device flip. etc.), Android system would automatically recreate this 
 *    fragment TWICE with possibility the fragment either not attached to an activity (or what) 
 *    such that getActivity() would returned NULL. This caused either IllegalStateException or NPE 
 *    in this fragment when trying to access resources. We tried our best to avoid such cases in 
 *    the container parent Activity/Fragment (See TripDetailFragment.java javadoc regarding PictureGalleryFragment). 
 *    In particular, inside post() runnable method, getActivity() is NULL on the 1st autocreation! 
 *    Not sure exactly the root cause. SAD! So we must place double check with IF statements.
 * 
 *    (4) Issue: Fragment(s) sticky auto-created but dimensions ZEROs
 *    After we viewed a page with this fragment, then we swap tabs to view either PictureDetail 
 *    fragment or Map fragment, and then we flip the device: Android system found the previous
 *    instance of this fragment even though we were not viewing it (hence, sticky!) and tried to 
 *    initialize it. This sticky fragment instance was not on display, and has ZERO width dimension 
 *    that caused math exception ERROR when we tried to do math calculations.
 *    To resolve this, we quit initialization whenever fragment width is zero.
 *
 *    (5) Issue: Image size vs. Number of images per PageViewer
 *    As described above, each PageViewer may contain 1 or more images based on calculation of 
 *    its area, that is the fragment width. BUT, while the calculation was correct, the PageViewer 
 *    often has difficulty to fit in all the images in the given width and cause inconsistent
 *    image sizes (shrunk, expanded, etc.). To resolve this, we forced to set original image layout
 *    width/height to ZERO, then reset them in the code to match the expected width/height of 
 *    the image to show. Note:
 *    (5.1) Setting the original dimensions to zero is optional but recommended to save resources. 
 *    (5.2) Android system returns 'fragment.getWidth()' value after including any paddings/margins.
 * 
 *    (6) Issue: Image as ImageView vs. ViewGroup/Layout
 *    Initially we had the entire image layout (merge_body_picture_thumbnail.xml) as a 
 *    simple ImageView. But with the issue #5 above, this became hard to resolve. So we have
 *    a FrameLayout to contain the ImageView, and reconfigure the FrameLayout as we wish. Also,
 *    it benefits by setting the inside ImageView's scaleType="fitXY" so the image would expand
 *    freely to fill in container dimension.
 * 
 *    (7) Issue: Fragment variables LOST during detach/attach ops
 *    See TripDetailFragment.java javadoc for details of this issue.
 *    
 * 7. Assets:
 * -> layout/fragment_picture_gallery.xml
 * -> layout/fragment_picture_gallery_viewpager.xml
 * -> layout/template_row_picture_image.xml
 * 
 * @author Val Triadi
 * @since 6/2013, 3/2017
 *
 */
public class PictureGalleryFragment 
extends BaseFragment {
    public static final String TAG_FRAGMENT_NAME = PictureGalleryFragment.class.getName();
    private static final String TAG_LOGGER = "PictureGalleryFragment";
    
    private Trip mTrip;
    private List<Picture> mPictureList;
    private int mPictureTotalNum;
    private int mPictureSelectedIndex = 0;
    private int mViewPagerTotalNum;
    private int mViewPagerSelectedIndex = 0;
    private Hashtable<Integer, ViewGroup> mLoadedImagesTable;
    private int mFragmentTotalWidth;
    
    //(7) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
    Bundle mFragmentArgumentBundle;
    
    //Views:
    private View mProgressBarSectionView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        mFragmentRootView = inflater.inflate(R.layout.fragment_picture_gallery, parentViewGroup, false);
        return mFragmentRootView;
    }

    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            mFragmentArgumentBundle = getArguments();
            if(mFragmentArgumentBundle != null) {
                mTrip = (Trip) mFragmentArgumentBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mPictureSelectedIndex = mFragmentArgumentBundle.getInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED);
                mViewPagerSelectedIndex = mFragmentArgumentBundle.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mPictureSelectedIndex = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED);
            mViewPagerSelectedIndex = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            
            //(7) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
            mFragmentArgumentBundle = getArguments();
        }
        
        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, mPictureSelectedIndex);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mViewPagerSelectedIndex);
    }
    
    public void refresh(Trip trip) {
        mTrip = trip;
        mPictureSelectedIndex = 0;
        mViewPagerSelectedIndex = 0;
        
        //(7) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
        if(mFragmentArgumentBundle != null) {
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, mPictureSelectedIndex);
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mViewPagerSelectedIndex);
        }
        
        init();
    }
    
    private void init() {
        //(3) Issue: Fragment(s) auto-created but getActivity() NULL (See Javadoc)
        if(getActivity() == null) {
            return;
        }
        
        if(mTrip == null
                || mTrip.getNumOfPictures() < 1) {
            return;
        }
        
        mProgressBarSectionView = mFragmentRootView.findViewById(R.id.picGalleryProgressBarLayoutID);
        TextView progressBarText = (TextView) mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_picture_imageLoading);

        //(1) Issue: Parent Width dimension (see Javadoc)
        mFragmentRootView.post(new Runnable() {
            @Override
            public void run() {
                //(3) Issue: Fragment(s) auto-created but getActivity() NULL (See Javadoc)
                if(getActivity() == null) {
                    return;
                }
                
                //(4) Issue: Fragment(s) sticky auto-created but dimensions ZEROs (See Javadoc)
                mFragmentTotalWidth = mFragmentRootView.getWidth();
                if(mFragmentTotalWidth == 0) {
                    Log.e(TAG_LOGGER, "init() - ERROR: Required Fragment width is ZERO! Skipping all initialization.");
                    return;
                }

                initProperties();
                initViewPager();
            }
        });
    }
    
    private void initProperties() {
        mPictureList = mTrip.getListOfPictures();
        mPictureTotalNum = mPictureList.size();
        
        if(mPictureSelectedIndex < 0
                || mPictureSelectedIndex > (mPictureTotalNum - 1)) {
            //Avoid ArrayOutOfBound error
            mPictureSelectedIndex = 0;
        }
        
        /*
         * We only care to highlight PictureCover (if any) in Gallery if mPictureSelectedIndex is ZERO
         * because we do not want to override any mPictureSelectedIndex value from bundle!
         */
        if(mPictureSelectedIndex == 0) {
            int loopIndex  = 0;
            for(Picture picture : mPictureList) {
                if(picture.getCoverPicture() == YesNoEnum.YES) {
                    mPictureSelectedIndex = loopIndex;
                    
                    //(7) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
                    if(mFragmentArgumentBundle != null) {
                        mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, mPictureSelectedIndex);
                    }
                    
                    break;
                }
                loopIndex++;
            }
        }
        
        if(mLoadedImagesTable == null) {
            mLoadedImagesTable = new Hashtable<Integer, ViewGroup>();
        }
    }
    
    private void initViewPager() {
        ViewPager viewPager = (ViewPager) mFragmentRootView.findViewById(R.id.picGalleryViewPagerID);
        if(viewPager == null) {
            Log.e(TAG_LOGGER, "initViewPager() - ERROR: Required ViewPager component is missing from main layout!");
            return;
        }
        
        //(2) Issue: ScrollView vs. PageViewer scrolling conflicts (See Javadoc)
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        viewPager.setOnPageChangeListener(new SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mFragmentRootView.getParent().requestDisallowInterceptTouchEvent(true);
            }
        });
        
        //Calculate number of images per ViewPager (the smallest or round DOWN of the division calculation)
        float divisionResultFloat = (float)mFragmentTotalWidth / SingletonFactory.getPictureThumbnailViewSize(getActivity());
        int numOfImagesPerViewPager = (int) Math.floor(divisionResultFloat);

        //Calculate number of total ViewPagers (round UP of the division calculation):
        mViewPagerTotalNum = mPictureTotalNum / numOfImagesPerViewPager 
                             + (mPictureTotalNum % numOfImagesPerViewPager == 0? 0 : 1);   
        
        //Set PageChangeListener impl
        viewPager.setOnPageChangeListener(new ViewPagerPageChangeListenerImpl());
        
        //Set current ViewPager to display:
        viewPager.setCurrentItem(mViewPagerSelectedIndex, false);

        //Set PageAdapter impl
        mProgressBarSectionView.setVisibility(View.GONE);
        GalleryImagesPagerAdapterImpl pagerAdapter = new GalleryImagesPagerAdapterImpl(numOfImagesPerViewPager);
        viewPager.setAdapter(pagerAdapter);
        
        /*Log.d(TAG_LOGGER, "initViewPager() - Completed. containerWidth [" 
                + mFragmentTotalWidth + "], picImageThumbnailSize [" + SingletonFactory.getPictureThumbnailViewSize(getActivity())
                + "], divisionResult [" + divisionResultFloat + "], numOfImagesPerViewPager [" 
                + numOfImagesPerViewPager + "] and numOfTotalViewPager: " + mViewPagerTotalNum);*/
    }
    
    private void updatePictureGallerySelected(int indexSelected, View imgViewSelected) {
        if(mPictureSelectedIndex == indexSelected) {
            //No need to update:
            return;
        }

        mPictureSelectedIndex = indexSelected;
        
        //(7) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
        if(mFragmentArgumentBundle != null) {
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED, mPictureSelectedIndex);
        }
        
        //Update picture gallery thumbnails look-n-feel:
        Collection<ViewGroup> imageViewCollection = mLoadedImagesTable.values();
        for(ViewGroup imgView : imageViewCollection) {
            imgView.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
        }
        imgViewSelected.setBackgroundColor(getActivity().getResources().getColor(R.color.color_active));

        //Update picture cover (if found):
         PictureCoverFragment picCoverFragment = 
                (PictureCoverFragment) getFragmentManager().findFragmentByTag(PictureCoverFragment.TAG_FRAGMENT_NAME);
        if(picCoverFragment != null) {
            picCoverFragment.updatePictureCover(indexSelected);
        }
    }
    
    public void onPictureImageGalleryReadyCallback(ImageCacheBean imgResult, int imageIndex) {
        if(imgResult == null) {
            Log.e(TAG_LOGGER, "onPictureImageGalleryReadyCallback() - ERROR: Returned result is NULL for image index [" + imageIndex + "]!");
        } else if(imgResult.getServiceStatus() != ServiceStatusEnum.STATUS_SUCCESS) {
            Log.e(TAG_LOGGER, "onPictureImageGalleryReadyCallback() - ERROR: Returned result failed image index [" + imageIndex + "]! " + imgResult);
        } else if(imgResult.getValue() == null) {
            Log.e(TAG_LOGGER, "onPictureImageGalleryReadyCallback() - ERROR: Returned result success BUT had NULL bitmap for image index [" + imageIndex + "]!");
        } else {
            /*
             * Update ImageView in the list and in the display.
             * Only bother if it is a success (Remember: image has been pre-populated)
             */
            ViewGroup imgViewToDisplay = mLoadedImagesTable.get(imageIndex);
            if(imgViewToDisplay != null) {
                ((ImageView) imgViewToDisplay.findViewById(R.id.imageViewID)).setImageBitmap(imgResult.getValue());
            } //Else; The view to display is already out of view (destroyed)
        }
    }
    
    /**
     * GalleryImagesPagerAdapter Inner class.
     * Concrete implementation of the abstract PagerAdapter that is responsible to fill the 
     * ViewPager with the sequence of views to display.
     * 
     * Must implement:
     * - instantiateItem()
     * - destroyItem()
     * - getCount()
     * - isViewFromObject()
     */    
    private class GalleryImagesPagerAdapterImpl extends PagerAdapter {
        private int numOfImagesPerViewPager;
        
        public GalleryImagesPagerAdapterImpl(int numOfImagesPerViewPager) {
            super();
            this.numOfImagesPerViewPager = numOfImagesPerViewPager;
        }
        
        /**
         * The system calls this to compose the next object view for the (n)th position. 
         * 
         * The operation is that the system will always maintain 3 object views (default) at the same time 
         * on the view so that you can slide to (n-1)th or (n+1)th while the (n)th object is on the display.
         * Example:
         * a. At the start view, the system call with position = 0 to display 0th object, and also
         *    immediately call with position = 1 so user can slide a peek to view the 1st object.
         *    
         * b. At the 1st object view, 1st object is already available. The system call with 
         *    position = 2, so that the 2nd object is ready to be slided while 0th object is still 
         *    available in memory so you can slide to the previous view.
         * 
         * c. At the 2nd object view, the system call with position = 3 (as long as the getCount() is not
         *    exceeded, while maintaining 1st and 2nd object. The 0th object is distroyed with the
         *    destroyItem() invoked. 
         *    
         *  Therefore, the 'position' value is always the next object or (n+1)th.
         *  
         * CUSTOM NOTE:
         * Our ViewPager layout is to display 3 items (images) per object view. So for 3 objects
         * (n-1, n, n+1), we maintain 9 images (max) in memory.
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //Create a new ViewPager layout for the position:
            ViewGroup viewPagerLayout = 
                    (ViewGroup)getActivity().getLayoutInflater().inflate(
                            R.layout.fragment_picture_gallery_viewpager, container, false);

            //Populate images into the ViewPager:
            int imageIndex = position * numOfImagesPerViewPager;
            for(int i = 0; i < numOfImagesPerViewPager; i++) {
                if((imageIndex + i) > (mPictureTotalNum - 1)) {
                    //Exceed array bound (array index start at 0):
                    break;
                }
                
                //(6) Issue: Image as ImageView vs. ViewGroup/Layout (See Javadoc)
                ViewGroup imageView = 
                        (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.template_row_picture_image, null);
                
                //(5) Issue: Image size vs. Number of images per PageViewer (See Javadoc)
                FrameLayout.LayoutParams layoutParam = 
                        new FrameLayout.LayoutParams(
                                SingletonFactory.getPictureThumbnailViewSize(getActivity()), 
                                SingletonFactory.getPictureThumbnailViewSize(getActivity())); 
                imageView.setLayoutParams(layoutParam);

                viewPagerLayout.addView(imageView);
                processImageToDisplay(getActivity(), imageIndex + i, imageView);
            }

            ((ViewPager)container).addView(viewPagerLayout, 0);
            return viewPagerLayout;
        }
        
        /**
         * Returns number of expected ViewPagers.
         * 
         * CUSTOM NOTE:
         * We do not return the list size (number of total pictures) as typically done. 
         * Since each ViewPager display n number of pictures at once, then this method should return
         * number of total expected ViewPager.
         */
        @Override
        public int getCount() {
            return mViewPagerTotalNum;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (View) object;
        }

        /**
         * The systems calls this to destroy the object view that is out of view, outside the 3
         * displays it maintains: (n-1)th, (n)th and (n+1)th. 
         * 
         * CUSTOM NOTE:
         * In addition, we must adjust our Table Map of loaded images accordingly to remove its values (images) 
         * that belong to the object view in question (given its position index). By doing this, we
         * maintain the Table Map to only contain maximum (numOfImagesPerViewPager x 3) images.
         * 
         * @position - The index of the object view to be destroyed
         * @object - The entity to be destroyed or removed from the container
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            //Remove images from the Table Map:
            int imageIndex = position * numOfImagesPerViewPager;
            for(int i = 0; i < numOfImagesPerViewPager; i++) {
                mLoadedImagesTable.remove(imageIndex + i);
            }

            //Default:
            container.removeView((View)object);
        }
        
        private void processImageToDisplay(final Context context, final int imageIndex, final ViewGroup imageView) {
            /*
             * Pre-populate the image view before we try to retrieve its real image
             * Also: (6) Issue: Image as ImageView vs. ViewGroup/Layout (See Javadoc)
             */
            ((ImageView) imageView.findViewById(R.id.imageViewID)).setImageBitmap(SingletonFactory.getImageNotAvailable(context));
            ((TextView) imageView.findViewById(R.id.imageIndexID)).setText("" + (imageIndex + 1));

            if(imageIndex == mPictureSelectedIndex) {
                imageView.setBackgroundColor(context.getResources().getColor(R.color.color_active));
            } else {
                imageView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updatePictureGallerySelected(imageIndex, imageView);
                }
            });
            
            mLoadedImagesTable.put(imageIndex, imageView);

            //Get image from repository:
            Picture picToRead = mPictureList.get(imageIndex);
            PictureImageRetrieveAsyncTask backendTask = 
                    new PictureImageRetrieveAsyncTask(PictureGalleryFragment.this, picToRead, mIsUserLoginTrip, true, imageIndex);
            backendTask.execute();
        }
    }
    
    /**
     * ViewPagerPageChangeListener Inner class.
     * Simple overrides default ViewPager.SimpleOnPageChangeListener where anytime user swipe
     * between ViewPagers, we capture the current position, and update the latest selected 
     * ViewPager index.
     */    
    private class ViewPagerPageChangeListenerImpl extends SimpleOnPageChangeListener{
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            
            mViewPagerSelectedIndex = position;
            
            //(7) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
            if(mFragmentArgumentBundle != null) {
                mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mViewPagerSelectedIndex);
            }
        }
    }    
    
}

