package com.trekcrumb.android.fragment;

import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.FavoriteRetrieveAsyncTask;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A fragment for Trip's List of Favorites. 
 * 1. This fragment is NOT an extension of Android's ListFragment (which is a commonly use to display List),
 *    but our custom BaseFragment. It operates in raw mode where we would populate rows manually in code. 
 * 
 * 2. Issues and Resolutions
 *    (1) Issue: Fragment variables LOST during detach/attach ops
 *    See TripDetailFragment.java javadoc for details of this issue.
 * 
 * @author Val Triadi
 * @since 04/2017
 */
public class FavoriteListByTripFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "FavoriteListByTripFragment";
    public static final String TAG_FRAGMENT_NAME = FavoriteListByTripFragment.class.getName();
    
    private Trip mTrip;
    private int mNumOfTotalFavorites;
    private int mOffset = 0;
    
    //(1) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
    Bundle mFragmentArgumentBundle;
    
    //Views
    private View mContentContainer;
    private ViewGroup mListRowsContainer;
    private View mMenuContentShow;
    private View mMenuContentHide;
    private View mProgressBarSectionView;
    private View mMenuMore;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_favorite_by_trip, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            mFragmentArgumentBundle = getArguments();
            if(mFragmentArgumentBundle != null) {
                mTrip = (Trip) mFragmentArgumentBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mOffset = mFragmentArgumentBundle.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mOffset = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET);
            
            //(1) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
            mFragmentArgumentBundle = getArguments();
        }
        
        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET, mOffset);
    }
    
    @Override
    public void refresh(Trip trip) {
        mTrip = trip;
        mOffset = 0;
        
        //Ensure the fragment is attached to an activity before relaunch the init()
        if(getActivity() != null) {
            init();
        }
    }
    
    private void init() {
        if(mTrip == null) {
            return;
        }
        
        initPropertiesAndLayout();
        setupClickables();
        
        if(mNumOfTotalFavorites == 0) {
            populateNoData();
            return;
        }

        //Else:
        populateDataValues();
    }
    
    private void initPropertiesAndLayout() {
        mNumOfTotalFavorites = mTrip.getNumOfFavorites();
        
        mMenuContentShow = mFragmentRootView.findViewById(R.id.menuContentShowID);
        mMenuContentHide = mFragmentRootView.findViewById(R.id.menuContentHideID);
        mContentContainer = (ViewGroup)mFragmentRootView.findViewById(R.id.contentContainerID);
        mListRowsContainer = (ViewGroup) mFragmentRootView.findViewById(R.id.listRowsContainerID);
        mMenuMore = mFragmentRootView.findViewById(R.id.favoriteMenuMoreID);
        
        //Progress bar:
        mProgressBarSectionView = mFragmentRootView.findViewById(R.id.favoriteProgressBarID);
        TextView progressBarText = (TextView) mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_common_pleaseWait);
        
        //By default, hide these:
        mProgressBarSectionView.setVisibility(View.GONE);
        mMenuMore.setVisibility(View.GONE);
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        mMenuContentShow.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doShowContent();
                    }
                }); 
        
        mMenuContentHide.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doHideContent();
                    }
                });
        
        mMenuMore.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        retrieveFavorites();
                    }
                });
    }
    
    private void populateNoData() {
        ((TextView)mFragmentRootView.findViewById(R.id.numOfFavoritesID)).setText("0");
        ((TextView)mFragmentRootView.findViewById(R.id.favoriteOnTripInfoID)).setText(
                getResources().getText(R.string.info_favorite_empty));
        doHideContent();
    }
    
    private void populateDataValues() {
        ((TextView)mFragmentRootView.findViewById(R.id.numOfFavoritesID)).setText("" + mNumOfTotalFavorites);
        ((TextView)mFragmentRootView.findViewById(R.id.favoriteOnTripInfoID)).setText(
                getResources().getText(R.string.label_favorite_list_users));
        
        /*
         * During init(), we only populate and display the list when it is handily available
         * inside the Trip object. If not, no action taken until user triggers 'doShowContent()'.
         * If user never reads the list content, we save ourselves from useless server call.
         * See doShowContent().
         */
        List<User> favoriteUsers = mTrip.getListOfFavoriteUsers();
        if(favoriteUsers != null && favoriteUsers.size() > 0) {
            displayList(favoriteUsers);
        }
        
        //By default - hide it
        doHideContent();
    }
    
    /*
     * TODO Flexbox-Layout (4/2017): 
     * The following method is working and preferrable BUT we cannot use it until we implement Flexbox-Layout.
     * The layout 'listRowsContainerID' orientation must be set to 'horizontal'.
     * Note:
     * The list row is using template_row_picture_image, the same template as PictureGalleryFragment.
     * See its Javadoc for the following issue and resolution:
     * (5) Issue: Image size vs. Number of images per PageViewer
     * (6) Issue: Image as ImageView vs. ViewGroup/Layout
     */
    /*
    private void displayList(List<User> favoriteUsers) {
        mListRowsContainer.setVisibility(View.VISIBLE);
        
        for(User user : favoriteUsers) {
            final String userID = user.getUserId();
            
            //Clone and populate row template:
            ViewGroup row = 
                    (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.template_row_picture_image, null);
            row.findViewById(R.id.imageIndexID).setVisibility(View.GONE);
            
            FrameLayout.LayoutParams layoutParam = 
                    new FrameLayout.LayoutParams(
                            SingletonFactory.getPictureThumbnailViewSize(getActivity()), 
                            SingletonFactory.getPictureThumbnailViewSize(getActivity())); 
            row.setLayoutParams(layoutParam);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityUtil.doUserViewActivity(getActivity(), userID);
                }
            });

            //Handle user image:
            ImageView userImageView = (ImageView)row.findViewById(R.id.imageViewID);
            userImageView.setImageBitmap(SingletonFactory.getImageNotAvailable(getActivity()));
            if(!ValidationUtil.isEmpty(user.getProfileImageName())) {
                ImageUtil.retrieveUserProfileImage(
                        getActivity(), user.getProfileImageName(), null, userImageView, false, true);
            }

            //Add row to the list:
            mListRowsContainer.addView(row);
        }
        
        doShowOrHideMenuMore();
    }
    */
    
    /*
     * TODO Flexbox-Layout (4/2017): 
     * The following method is temporary display until we implement Flexbox-Layout.
     */
    private void displayList(List<User> favoriteUsers) {
        mListRowsContainer.setVisibility(View.VISIBLE);
        
        for(User user : favoriteUsers) {
            final String userID = user.getUserId();
            
            //Clone and populate row template:
            ViewGroup row = 
                    (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.widget_user_summary, null);
            row.setClickable(true);
            row.setBackgroundResource(R.drawable.drawable_statelist_clickable_menu_transparent);
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityUtil.doUserViewActivity(getActivity(), userID);
                }
            });

            ((TextView)row.findViewById(R.id.fullnameID)).setText(user.getFullname());
            ((TextView)row.findViewById(R.id.usernameID)).setText(user.getUsername().toLowerCase());

            //Handle user image:
            Button userProfileDefaultView = (Button) row.findViewById(R.id.profileImageDefaultID);
            ImageView userProfileImageView = (ImageView) row.findViewById(R.id.profileImageID);
            if(ValidationUtil.isEmpty(user.getProfileImageName())) {
                userProfileDefaultView.setVisibility(View.VISIBLE);
                userProfileImageView.setVisibility(View.GONE);
            } else {
                ImageUtil.retrieveUserProfileImage(
                        getActivity(), user.getProfileImageName(), userProfileDefaultView, userProfileImageView, false, true);
            }

            //Add row to the list:
            mListRowsContainer.addView(row);
        }
        
        doShowOrHideMenuMore();
    }
    
    private void retrieveFavorites() {
        mProgressBarSectionView.setVisibility(View.VISIBLE);
        mMenuMore.setVisibility(View.GONE);
        
        Trip trip = new Trip();
        trip.setId(mTrip.getId());
        FavoriteRetrieveAsyncTask backendTask = new FavoriteRetrieveAsyncTask(
                null, this, trip, null, ServiceTypeEnum.FAVORITE_RETRIEVE_BY_TRIP, 
                mOffset, null);
        backendTask.execute();
    }
    
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(!isSuccess) {
            mProgressBarSectionView.setVisibility(View.GONE);
            doShowOrHideMenuMore();
            return isSuccess;
        }
        
        //Success:
        List<User> listOfFavoriteUsers = result.getListOfUsers();
        mOffset = mOffset + CommonConstants.RECORDS_NUMBER_MAX;
        
        //(1) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
        if(mFragmentArgumentBundle != null) {
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET, mOffset);
        }
        
        if(listOfFavoriteUsers == null || listOfFavoriteUsers.size() == 0) {
            //Possible some users removed their favorites right before we retrieve them:
            Log.e(TAG_LOGGER, "onAsyncBackendTaskCompleted() - ERROR: Returned result success BUT had NULL List of Favorite Users for offset [" + mOffset + "]!");
            mProgressBarSectionView.setVisibility(View.GONE);
            doShowOrHideMenuMore();
        }
        
        for(User user : listOfFavoriteUsers) {
            mTrip.addFavoriteUser(user);
        }
        mProgressBarSectionView.setVisibility(View.GONE);
        displayList(listOfFavoriteUsers);
        return isSuccess;
    }
    
    /**
     * Populates and shows the list contents.
     */
    public void doShowContent() {
        mMenuContentShow.setVisibility(View.GONE);
        mMenuContentHide.setVisibility(View.VISIBLE);
        mContentContainer.setVisibility(View.VISIBLE);
        
        /*
         * Only when mOffset is ZERO, we need to make sure the list is populated after init().
         * But that only occurred if the list is handily available inside Trip object. If 
         * the list is still empty, we call server to retrieve the list.
         */
        if(mOffset == 0 && mNumOfTotalFavorites > 0) {
            if(mListRowsContainer == null) {
                mListRowsContainer = (ViewGroup) mFragmentRootView.findViewById(R.id.listRowsContainerID);
            }
            if(mListRowsContainer.getChildCount() == 0) {
                retrieveFavorites();
            }
        }
    }

    private void doHideContent() {
        mMenuContentShow.setVisibility(View.VISIBLE);
        mMenuContentHide.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.GONE);
    }
    
    private void doShowOrHideMenuMore() {
        if((mOffset + 1) < mNumOfTotalFavorites) {
            mMenuMore.setVisibility(View.VISIBLE);
        } else {
            mMenuMore.setVisibility(View.GONE);
        }
    }
    
    
    
}
