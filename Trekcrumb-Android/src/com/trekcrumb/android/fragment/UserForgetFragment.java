package com.trekcrumb.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.asynctask.UserForgetAsyncTask;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.ValidationUtil;

public class UserForgetFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "UserForgetFragment";
    public static final String TAG_FRAGMENT_NAME = UserForgetFragment.class.getName();
    
    private BaseActivity mHostActivity;
    private String mEmail;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_user_forget, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        init();
    }
    
    private void init() {
        mHostActivity = (BaseActivity)getActivity();
        
        Button menuSubmit = (Button)mFragmentRootView.findViewById(R.id.buttonSaveID);
        menuSubmit.setText(getResources().getText(R.string.label_common_submit));
        
        setupClickables();
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        View menuSubmit = mFragmentRootView.findViewById(R.id.buttonSaveID);
        menuSubmit.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        submit();
                    }
                }); 
        
        View menuCancel = mFragmentRootView.findViewById(R.id.buttonCancelID);
        menuCancel.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mHostActivity.finish();
                    }
                }); 
    }
    
    private void submit() {
        cleanErrorAndInfoMessages();
        hideKeyboard();

        if(!validateForm()) {
            evaluateErrorAndInfo();
            return;
        }
        
        //Call back-end (must be async-task due to network op.)
        mHostActivity.setProgressDialog(
                NoticeMessageUtil.showProgressDialog(
                    mHostActivity, 
                    mHostActivity.getLayoutInflater(), 
                    getResources().getString(R.string.label_user_my_forgetAccount), 
                    getResources().getString(R.string.info_common_pleaseWait)));

        User userForget = new User();
        userForget.setEmail(mEmail);

        UserForgetAsyncTask backendTask = 
                new UserForgetAsyncTask(mHostActivity, this, userForget);
        backendTask.execute();
    }
    
    private boolean validateForm() {
        boolean isValid = true;
        try {
            mEmail = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.emailValueID)).getText());
        } catch(NullPointerException npe) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_invalidParam_loginAllRequired));
            return false;
        }
        try {
            ValidationUtil.validateEmail(mEmail, true);
        } catch(IllegalArgumentException iae) {
            addErrorOrInfoMessages(true, iae.getMessage());
            isValid = false;
        }
        return isValid;
    }
    
    /**
     * Callback by the back-end refresh business logic once it is done.
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        if(getActivity() == null) {
            return false;
        }
        
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(isSuccess) {
            View successWarningLayout = mFragmentRootView.findViewById(R.id.userForgetSuccessWarningID);
            if(successWarningLayout != null) {
                ((TextView)successWarningLayout.findViewById(R.id.warningTextID)).
                    setText(getResources().getText(R.string.info_user_forget_successConfirm));
                
                successWarningLayout.setVisibility(View.VISIBLE);
            }
        }

        return isSuccess;
    }
    
    
}
