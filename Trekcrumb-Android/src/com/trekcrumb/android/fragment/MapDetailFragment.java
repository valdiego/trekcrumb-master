package com.trekcrumb.android.fragment;

import java.util.List;
import java.util.Map;

import android.app.FragmentTransaction;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.trekcrumb.android.R;
import com.trekcrumb.android.layoutholder.PlaceDetailLayoutHolder;
import com.trekcrumb.android.listener.IMapMarkerClickListener;
import com.trekcrumb.android.listener.IMapReadyListener;
import com.trekcrumb.android.otherimpl.TrekcrumbMapFragmentImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.DeviceUtil;
import com.trekcrumb.android.utility.GeoLocationUtil;
import com.trekcrumb.android.utility.MapUtil;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;

@SuppressWarnings("serial")
public class MapDetailFragment 
extends BaseFragment 
implements IMapReadyListener, IMapMarkerClickListener {
    public static final String TAG_FRAGMENT_NAME = MapDetailFragment.class.getName();
    private static final String TAG_LOG = "MapDetailFragment";

    private Trip mTrip;

    private boolean mIsPlaceEmpty;
    private int mPlaceSelectedIndex;
    private Location mCurrentLocation;
    private List<Marker> mListOfMapMarkers;
    private Marker mMarkerCurrentLocation;
    private boolean mIsShowDetail;
    private boolean mIsGPSLocationSearchRunning;

    //Place Details:
    private View mPlaceDetailOnMapLayout;
    private PlaceDetailLayoutHolder mplaceDetailLayoutHolder;
    private Button mMenuNext;
    private Button mMenuPrevious;
    private Button mMenuPageShow;
    private Button mMenuPageHide;
    private View mMenuPageContent;
    private Button mMenuPlaceEdit;
    private View mMenuCurrentLocation;
    
    /* class variable not to be included in serialize */
    private transient FrameLayout mMapLayout;

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_map_detail, parentViewGroup, false);
        mMapLayout = (FrameLayout) mFragmentRootView.findViewById(R.id.mapLayoutID);
        
        mPlaceDetailOnMapLayout = getActivity().getLayoutInflater().inflate(R.layout.fragment_place_detail_on_map, null);
        mplaceDetailLayoutHolder = new PlaceDetailLayoutHolder(mPlaceDetailOnMapLayout);
        
        return mFragmentRootView;
    }

    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceBundle) {
        super.onActivityCreated(savedInstanceBundle);
        if(savedInstanceBundle == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mTrip = (Trip)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                
                //Default to always show details initially:
                mIsShowDetail = true;
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);

            mPlaceSelectedIndex = savedInstanceBundle.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            mCurrentLocation = (Location)savedInstanceBundle.getParcelable(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT);
            mIsShowDetail = savedInstanceBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_SHOW_DETAIL);
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped/destroyed 
     * state. Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPlaceSelectedIndex);
        outStateBundle.putParcelable(CommonConstants.KEY_SESSIONOBJ_LOCATION_CURRENT, mCurrentLocation);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_SHOW_DETAIL, mIsShowDetail);
    }
    
    public void refresh(Trip trip) {
        mTrip = trip;
        
        //Ensure the fragment is attached to an activity before relaunch the init()
        if(getActivity() != null) {
            //Kill any GPS location search, if any:
            GeoLocationUtil.deactivateGPSLocation(getActivity());
            
            init();
        }
    }
    
    private void init() {
        cleanErrorAndInfoMessages();
        
        if(mTrip == null) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_trip_notFound));
            displayNoMap();
            evaluateErrorAndInfo();
            return;
        }
        
        //Else:
        mIsGPSLocationSearchRunning = false;
        mMarkerCurrentLocation = null;
        mCurrentLocation = null;
        
        mIsPlaceEmpty = false;
        if(mTrip.getListOfPlaces() == null 
                || mTrip.getListOfPlaces().size() == 0) {
            mIsPlaceEmpty = true;
            addErrorOrInfoMessages(true, getResources().getString(R.string.info_trip_emptyMarkedPlaces));
        }
        
        if(mPlaceSelectedIndex < 0) {
            mPlaceSelectedIndex = 0;
        }
        initMapFragment();
        evaluateErrorAndInfo();
    }
    
    private void displayNoMap() {
        mMapLayout.removeAllViews();
        TextView infoView = 
                (TextView) getActivity().getLayoutInflater().inflate(R.layout.template_textview_info, null);
        infoView.setText(R.string.info_trip_noMapDisplay);
        mMapLayout.addView(infoView);
    }
    
    private void initMapFragment() {
        if(!mIsUserLoginTrip) {
            if(mIsPlaceEmpty) {
                displayNoMap();
            } else {
                addMapFragment();
            }
            return;
        }
        
        //UserLogin's trip:
        if(mTrip.getStatus() == TripStatusEnum.COMPLETED) {
            if(mIsPlaceEmpty) {
                displayNoMap();
                return;
            } else {
                addMapFragment();
                return;
            }
        }
        
        //Else: Trip is ACTIVE and belongs to userLogin:
        if(mIsPlaceEmpty 
                && !DeviceUtil.isGPSEnabled(getActivity())) {
            displayNoMap();
        } else {
            //Place may be empty or not, display the map anyway:
            addMapFragment();
        }
    }    
    
    /*
     * Adds MapFragemnt dynamically onto the hosting view. 
     * NOTE:
     * 1. There is a gap time to wait for Google map service to return the fragment. Hence,
     *    the callback: onMapFragmentReadyCallback
     *    
     * 2. Be aware that Android allows to add multiple fragment (with same tags) on the same activity,
     *    causing them to sit on top of each other. Therefore:
     *    IF MapFragment already exists on page:
     *    1.1. If MapFragment.getMap() is not null: It is valid, re-use it
     *    1.2. Else: It is invalid MapFragment, may due to system onDestroy() that places it 
     *               on backstack etc. Delete this MapFragment
     *    Else: Add MapFragment fresh and new
     *    
     * 3. Be aware: "Sticky issue". Even tho it is as if we add MapFragment into another fragment
     *    (MapDetailFragment), it is actually added to the activity and keep alive
     *    in FragmentManager. Even if its parent fragment is detached or its activity destroyed, 
     *    the MapFragment still exist and attached to the previous activity (that may become NULL).
     *    This requires careful handling during onStop()/onDestroy().
     */
    private void addMapFragment() {
        TrekcrumbMapFragmentImpl mapFragment = 
                (TrekcrumbMapFragmentImpl)getFragmentManager().findFragmentByTag(TrekcrumbMapFragmentImpl.TAG_FRAGMENT_NAME);
        if(mapFragment != null) {
            if(mapFragment.getMap() != null) {
                Log.d(TAG_LOG, "addMapFragment() - MapFragment is NOT NULL and it has getMap() ready! Let's re-use it!");
                /*
                 * Note: Ready to use.
                 * 1. Since this is MapFragment within fragment, it is possible MapFragment.isVisible() = false.
                 *    If so, it requires us to detach and then re-attach the map fragment. Otherwise it will show empty. 
                 *    If this was activity, there is no need to do such step.
                 * 2. Fragment attach() will trigger the same callback as the add() method
                 * 3. Reset MapFragment's IMapReadyListener and parent activity to this instance 
                 *    to anticipate its previous listener/activity has been destroyed (null) during
                 *    onStop() call (phone flip, other app opens, etc.) 
                 * 4. The attach() call will do a background work, and when done it callbacks method: 
                 *    onMapFragmentReadyCallback()
                 */
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(mapFragment);
                mapFragment.refresh(this);
                fragmentTransaction.attach(mapFragment);
                fragmentTransaction.commit();
                return;
                
            } else {
                //Invalid left-over, remove it:
                Log.w(TAG_LOG, "addMapFragment() - MapFragment is NOT NULL but its getMap() is EMPTY! Let's remove this sticky fragment.");
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.detach(mapFragment);
                fragmentTransaction.remove(mapFragment);
                fragmentTransaction.commit();
                mapFragment = null;
            }
        }

        /*
         * Add new Map fragment:
         * 1. The add() call will do a background work, and when done it callbacks method: 
         *    onMapFragmentReadyCallback()
         */
        //Add new Map fragment:
        Log.d(TAG_LOG, "addMapFragment() - Adding new Map ...");
        try {
            mapFragment = TrekcrumbMapFragmentImpl.newInstance(this);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.mapLayoutID, mapFragment, TrekcrumbMapFragmentImpl.TAG_FRAGMENT_NAME);
            fragmentTransaction.commit();

        } catch(Exception e) {
            String errMsg = "Failure when initializing Map with error: " + e.getMessage();
            Log.e(TAG_LOG, errMsg);
            e.printStackTrace();
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_googleMap_serviceNotAvailable));
        } 
    }
    
    /**
     * Callback method.
     * @see IMapReadyListener.
     */
    @Override
    public void onMapFragmentReadyCallback() {
        if(getActivity() == null || getFragmentManager() == null) {
            /*
             * Note: It is possible when the callback was made by the backend thread, the hosting
             * activity has become null or incomplete due to user navigate to other view (activity or fragments)
             */
            Log.e(TAG_LOG, "onMapFragmentReadyCallback() - ERROR! This fragment is NOT attached to any activity as getActivity() is NULL! Abandoned.");
            return;
        }
        
        TrekcrumbMapFragmentImpl mapFragment = 
                (TrekcrumbMapFragmentImpl)getFragmentManager().findFragmentByTag(TrekcrumbMapFragmentImpl.TAG_FRAGMENT_NAME);
        if(mapFragment == null
                || mapFragment.getMap() == null) {
            //Show error:
            Log.e(TAG_LOG, "onMapFragmentReadyCallback() - Upon callback, Google Map returned NULL map!");
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_googleMap_serviceNotAvailable));
            evaluateErrorAndInfo();
            return;
        }
        
        //Map is ready, let's configure it:
        GoogleMap theMap = mapFragment.getMap();
        theMap.clear();
        MapUtil.configureMap(getActivity(), theMap, this, mTrip, mCurrentLocation);

        //Places on map:
        List<LatLng> placesLatLngList = null;
        mListOfMapMarkers = null;
        if(!mIsPlaceEmpty) {
            placesLatLngList = GeoLocationUtil.constructPlacesLatLngList(mTrip);
            mListOfMapMarkers = MapUtil.paintMapWithPlaces(getActivity(), theMap, mTrip, placesLatLngList);
        }

        //Current location on map:
        if(mCurrentLocation != null) {
            LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            mMarkerCurrentLocation = MapUtil.paintMapWithCurrentLocation(getActivity(), theMap, currentLatLng);
       }
        
        //Show marker infoWindow:
        if(mMarkerCurrentLocation != null) {
            mMarkerCurrentLocation.showInfoWindow();
        } else if(mListOfMapMarkers != null) {
            //The 1st Place by default
            mListOfMapMarkers.get(0).showInfoWindow();
        }

        //Zoom and center the map appropriately:
        if(mCurrentLocation != null) {
            MapUtil.configureMapZoomAndCenter(theMap, mCurrentLocation, null, -1, -1, -1);
            
        } else if(placesLatLngList != null){
            Map<String, Integer> sizeMap = DeviceUtil.determineScreenSizes(getActivity());
            MapUtil.configureMapZoomAndCenter(
                    theMap, 
                    null,
                    placesLatLngList, 
                    ((Integer)sizeMap.get(DeviceUtil.DEVICE_WIDTH_KEY)).intValue(), 
                    ((Integer)sizeMap.get(DeviceUtil.DEVICE_HEIGHT_KEY)).intValue(),
                    getResources().getConfiguration().orientation);
        }

        /*
         * Decorate with Place Detail view as frame:
         * 1. The detail view must be added AFTER the MapFragment is drawn in order to have
         *    it on top of the map view.
         * 2. If the layout already has detail view (from previous load), remove it to avoid
         *    exception. Then re-added.
         * 3. If MapFragment is re-loaded but we did not re-add the detail view, it still exists
         *    but will be hidden behind the MapFragment.
         */
        if(!mIsPlaceEmpty) {
            View existingPlaceDetailOnMapLayout = mMapLayout.findViewById(R.id.placeDetailOnMapID);
            if(existingPlaceDetailOnMapLayout != null) {
                mMapLayout.removeView(existingPlaceDetailOnMapLayout);
            }
            mMapLayout.addView(mPlaceDetailOnMapLayout);
            initPlaceDetails();
        }
        
        evaluateErrorAndInfo();
    }
    
    private void initGPSLocation() {
        if(!mIsUserLoginTrip) {
            //userOther's Trip, nothing to do:
            return;
        }
        if(mTrip.getStatus() == TripStatusEnum.COMPLETED) {
            //Trip is completed, nothing to do:
            return;
        }
        if(mTrip.getPublish() == TripPublishEnum.PUBLISH
                && (!mIsUserAuthenticated || !mIsUserTripSynced)) {
            //Unauthorized, disable it:
            return;
        }
        if(mIsGPSLocationSearchRunning) {
            //GPS search is currently running, prevent multiple GPS search thread:
            return;
        }

        //ELSE: Trip is ACTIVE, belongs to userLogin and GPS is not yet running:
        cleanErrorAndInfoMessages();
        mIsGPSLocationSearchRunning = true;
        mCurrentLocation = null;
        if(mMarkerCurrentLocation != null) {
            mMarkerCurrentLocation.hideInfoWindow();
            mMarkerCurrentLocation.remove();
        }
        GeoLocationUtil.activateGPSLocation(getActivity());
    }
    
    /**
     * Callback method when GPSLocationFragment completes searching the current location.
     */
    public void onGPSLocationReadyCallback(Location location, String errorMsg) {
        Log.d(TAG_LOG, "onGPSLocationReadyCallback() - Received GPS current location result!");

        mIsGPSLocationSearchRunning = false;
        GeoLocationUtil.deactivateGPSLocation(getActivity());
        
        if(location == null) {
            addErrorOrInfoMessages(true, errorMsg);
            evaluateErrorAndInfo();
            
        } else {
            mCurrentLocation = location;
            if(getActivity() == null || getFragmentManager() == null) {
                /*
                 * Note: It is possible when the callback was made by the backend thread, the hosting
                 * activity has become null or incomplete due to user navigates to other view (activity or fragments)
                 */
                Log.e(TAG_LOG, "onGPSLocationReadyCallback() - ERROR! This fragment is NOT attached to any Activity! Abandoned the result.");
                return;
            }

            if(getFragmentManager().findFragmentByTag(TrekcrumbMapFragmentImpl.TAG_FRAGMENT_NAME) == null) {
                //MapFragment had never been added to the page, so initiate it:
                addMapFragment();
            } else {
                //There is already map on page, update it:
                onMapFragmentReadyCallback();
            }
            
            //Close menu-page:
            doHideMenuPageContent();
        }
    }        
    
    /**
     * Callback method
     * @see IMapMarkerClickListener.OnMarkerClickCallback()
     */
    public void onMarkerClickCallback(Marker marker) {
        //Get place index from marker snippet:
        int selectedPlaceIndex = -1;
        if(marker.getSnippet() != null) {
            try {
                selectedPlaceIndex = Integer.parseInt(marker.getSnippet());
            } catch(NumberFormatException nfe) {
                selectedPlaceIndex = -1;
            }
        }
        
        if(selectedPlaceIndex >= 0) {
            mPlaceSelectedIndex = selectedPlaceIndex;
            mIsShowDetail = true;
            populatePlaceDetails();
            doShowMenuPageContent();
        }
    }

    /*
     * Note:
     * 1. The detail view (R.layout.fragment_place_detail_on_map) and the layouthandler are
     *    defined as class variables and initiated at onViewCreated() step is to avoid to do 
     *    such everytime populatePlaceDetails() is invoked, particularly useful when user stays
     *    at the page and browsing thru the details or click on the mark.
     * 2. But nothing can prevent the view instance to be destroyed during phone flip or tab
     *    navigation, hence forcing the initiation to re-occur during each onViewCreated() calls.
     * 3. We must add the detail view ONLY AFTER the MapFragment is loaded successfully to be
     *    on top of the map (otherwise, it will be behind and hidden). 
     */
    private void initPlaceDetails() {
        if(mIsPlaceEmpty) {
            return;
        }
        
        initPropertiesAndLayout();
        setupClickables();
        populatePlaceDetails();
        
        //To show or hide it:
        if(mIsShowDetail) {
            doShowMenuPageContent();
        } else {
            doHideMenuPageContent();
        }
    }
    
    private void initPropertiesAndLayout() {
        mMenuNext = (Button) mFragmentRootView.findViewById(R.id.menuGoNextID);
        mMenuPrevious = (Button) mFragmentRootView.findViewById(R.id.menuGoPreviousID);
        if(mTrip.getNumOfPlaces() < 2) {
            mMenuNext.setVisibility(View.GONE);
            mMenuPrevious.setVisibility(View.GONE);
            mMenuNext = null;
            mMenuPrevious = null;
        } else {
            doShowOrHideMenuPagination();
        }
        
        mMenuPageShow = (Button) mFragmentRootView.findViewById(R.id.menuPageShowID);
        mMenuPageHide = (Button) mFragmentRootView.findViewById(R.id.menuPageHideID);
        mMenuPageContent = mFragmentRootView.findViewById(R.id.menuPageContentID);
        
        //UserLogin only section:
        if(mIsUserLoginTrip) {
            if(mTrip.getPublish() == TripPublishEnum.NOT_PUBLISH) {
                mMenuPlaceEdit = (Button) mFragmentRootView.findViewById(R.id.menuPlaceEditID);
                if(mTrip.getStatus() == TripStatusEnum.ACTIVE) {
                    
                }
            } else {
                if(!mIsUserAuthenticated || !mIsUserTripSynced) {
                    doHideMenuForUserLoginTrip();
                } else {
                    mMenuPlaceEdit = (Button) mFragmentRootView.findViewById(R.id.menuPlaceEditID);
                }
            }
            
            //Current Location:
            mMenuCurrentLocation = mFragmentRootView.findViewById(R.id.menuCurrentLocationID);
            if(mTrip.getStatus() != TripStatusEnum.ACTIVE) {
                mMenuCurrentLocation.setVisibility(View.GONE);
                mMenuCurrentLocation = null;
            }
            
        } else {
            //userOther's trip:
            doHideMenuForUserLoginTrip();
        }
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * NOTE: 
     * Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity. Tragic.
     */
    private void setupClickables() {
        if(mMenuNext != null) {
            mMenuNext.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doNext();
                        }
                    }); 
        }
        
        if(mMenuPrevious != null) {
            mMenuPrevious.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doPrevious();
                        }
                    }); 
        }
        
        mMenuPageShow.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mIsShowDetail = true;
                        doShowMenuPageContent();
                    }
                }); 

        mMenuPageHide.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mIsShowDetail = false;
                        doHideMenuPageContent();
                    }
                }); 

        if(mMenuPlaceEdit != null) {
            mMenuPlaceEdit.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            Place placeSelected =  mTrip.getListOfPlaces().get(mPlaceSelectedIndex);
                            ActivityUtil.doPlaceEditActivity(getActivity(), mTrip, placeSelected, mPlaceSelectedIndex);
                        }
                    }); 
        }
        
        if(mMenuCurrentLocation != null) {
            mMenuCurrentLocation.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            initGPSLocation();
                        }
                    }); 
        }
        
    }
    
    private void populatePlaceDetails() {
        Place placeSelected =  mTrip.getListOfPlaces().get(mPlaceSelectedIndex);
        mplaceDetailLayoutHolder.populateLayoutWithValues(
                mTrip, placeSelected, mPlaceSelectedIndex, 
                getResources().getString(R.string.label_common_numberOfTotal));        
    }
    
    private void doNext() {
        if(mPlaceSelectedIndex >= (mTrip.getNumOfPlaces() - 1)) {
            //Invalid call, no more 'Next'
            return;
        }
        
        //normal:
        mPlaceSelectedIndex++;
        doUpdatePlaceDetailsAndMapMarkers();
        doShowOrHideMenuPagination();
    }
    
    private void doPrevious() {
        if(mPlaceSelectedIndex == 0) {
            //Invalid call, no more 'Previous'
            return;
        } 
        
        //normal:
        mPlaceSelectedIndex--;
        doUpdatePlaceDetailsAndMapMarkers();
        doShowOrHideMenuPagination();
    }    
    
    private void doShowOrHideMenuPagination() {
        if(mPlaceSelectedIndex == 0) {
            mMenuPrevious.setVisibility(View.GONE);
            mMenuNext.setVisibility(View.VISIBLE);
            
        } else if(mPlaceSelectedIndex == (mTrip.getNumOfPlaces() - 1)) {
            mMenuPrevious.setVisibility(View.VISIBLE);
            mMenuNext.setVisibility(View.GONE);
            
        } else {
            mMenuPrevious.setVisibility(View.VISIBLE);
            mMenuNext.setVisibility(View.VISIBLE);
        }
    }
    
    private void doShowMenuPageContent() {
        mMenuPageShow.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.VISIBLE);
    }
    
    private void doHideMenuPageContent() {
        mMenuPageShow.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.GONE);
    }

    private void doHideMenuForUserLoginTrip() {
        //No edit menus:
        View menuForUserLoginTrip = mFragmentRootView.findViewById(R.id.menuForUserLoginTripID);
        menuForUserLoginTrip.setVisibility(View.GONE);
        
        View menuSectionSeparator = mFragmentRootView.findViewById(R.id.menuSectionSeparatorID);
        menuSectionSeparator.setVisibility(View.GONE);
    }
    
    private void doUpdatePlaceDetailsAndMapMarkers() {
        populatePlaceDetails();
        
        //Change map center:
        Place placeSelected =  mTrip.getListOfPlaces().get(mPlaceSelectedIndex);
        LatLng placeLatLan = new LatLng(placeSelected.getLatitude(), placeSelected.getLongitude());
        TrekcrumbMapFragmentImpl mapFragment = 
                (TrekcrumbMapFragmentImpl)getFragmentManager().findFragmentByTag(TrekcrumbMapFragmentImpl.TAG_FRAGMENT_NAME);
        if(mapFragment == null) {
            return;
        }
        GoogleMap theMap = mapFragment.getMap();
        if(theMap == null) {
            return;
        }
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(placeLatLan);
        theMap.moveCamera(cameraUpdate);
        
        //Show marker infoWindow:
        try {
            Marker marker = mListOfMapMarkers.get(mPlaceSelectedIndex);
            marker.showInfoWindow();
        } catch(IndexOutOfBoundsException iobe) {
            //If list is not in-synch with place index, ignore this feature:
            Log.e(TAG_LOG, "doUpdatePlaceDetailsAndMapMarkers() - ERROR! Index out of bound while trying to get Marker from the list given index [" + mPlaceSelectedIndex + "]!");
        }
    }
    
    
    
}

