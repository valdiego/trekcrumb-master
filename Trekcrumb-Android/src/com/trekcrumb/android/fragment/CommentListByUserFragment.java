package com.trekcrumb.android.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.otherimpl.CommentListAdapterImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * A fragment for User's List of Comments. 
 * 1. This fragment is an extension of Android's ListFragment which is a commonly use to display List. 
 *    Consequently, we must implement Android's ArrayAdapter and a LayoutHolder classes to support it.
 * 
 * @author Val Triadi
 * @since 04/2017
 */
public class CommentListByUserFragment 
extends ListFragment {
    public static final String TAG_FRAGMENT_NAME = CommentListByUserFragment.class.getName();

    private View mFragmentRootView;
    private ArrayList<Comment> mListOfComments;
    private int mSelectedIndex = 0;
    private boolean mIsUserLoginProfile;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView =  inflater.inflate(R.layout.fragment_generic_list, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  Callback when after fragment is created, its view initialized and the 
     *  hosting (parent) activity is created. It should contain any final initialization.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mListOfComments = (ArrayList<Comment>)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
                mIsUserLoginProfile = dataBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mListOfComments = (ArrayList<Comment>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
            mIsUserLoginProfile = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE);
            mSelectedIndex = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
        }
        
        init();
    }

    /**
     * Callback when to save any current dynamic data (state) before pause, so 
     * it can later be reconstructed in a new instance when the fragment is restarted.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfComments);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE, mIsUserLoginProfile);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mSelectedIndex);
    }
     
    public void refresh(List<Comment> comments) {
        mListOfComments = (ArrayList<Comment>)comments;
        init();
    }    

    private void init() {
        //Avoid NPE from android ListAdapter:
        if(mListOfComments == null) {
            mListOfComments = new ArrayList<Comment>();
        }
        
        //Populate List and its adapter:
        CommentListAdapterImpl listAdapter = 
                new CommentListAdapterImpl(getActivity(), mListOfComments, mIsUserLoginProfile);
        setListAdapter(listAdapter);
    }
    
    /**
     * Callback when an item in the list is selected. 
     */
    @Override
    public void onListItemClick(ListView listView, View view, int selectedIndex, long id) {
        mSelectedIndex = selectedIndex;
        Comment commentSelected = mListOfComments.get(selectedIndex);
        ActivityUtil.doTripViewActivity(
                getActivity(), commentSelected.getTripID(), null, false, null, false);
    }
    

}
