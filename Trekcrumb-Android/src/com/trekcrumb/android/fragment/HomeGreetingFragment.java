package com.trekcrumb.android.fragment;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.trekcrumb.android.R;

/**
 * Fragment to represent welcome greetings to user.
 *
 */
public class HomeGreetingFragment extends Fragment {
    private static final String BUNDLE_DATA_CURRENT_GREETING = "com.trekcrumb.BUNDLE_DATA_CURRENT_GREETING";

    private View mView;
    private TextView mHomeGreetingTextView;
    private String mHomeGreetingTextMsg; 
    
    /**
     * Callback when creating the fragment from scratch.
     * It initialize essential components to retain when the fragment is paused/stopped/resumed.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_home_greeting, parentViewGroup, false);
        return mView;
    }
    
    /**
     *  Callback when after fragment is created, its view initialized and the 
     *  hosting (parent) activity is created. It should contain any final initialization or
     *  re-populate (re-store) any dynamic data in place.     
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        //Restore previous state/data (if any):
        if (savedInstanceState != null
                && savedInstanceState.get(BUNDLE_DATA_CURRENT_GREETING) != null) {
            mHomeGreetingTextMsg = (String)savedInstanceState.get(BUNDLE_DATA_CURRENT_GREETING);
        }
        
        init();
    }
    
    /**
     * Callback when to save any current dynamic data (state) before pause, so 
     * it can later be reconstructed in a new instance when the fragment is restarted.
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(mHomeGreetingTextMsg != null) {
            savedInstanceState.putSerializable(BUNDLE_DATA_CURRENT_GREETING, mHomeGreetingTextMsg);
        }
    }
    
    private void init() {
        initPropertiesAndLayout();
    }
    
    /*
     * Dynamically initialize and modify layout due to specific conditions.
     */
    private void initPropertiesAndLayout() {
        if(mView != null) {
            if(mHomeGreetingTextView == null) {
                mHomeGreetingTextView = (TextView) mView.findViewById(R.id.homeGreetingTextViewID);
            }
        }
        
        if(mHomeGreetingTextMsg == null) {
            mHomeGreetingTextMsg = getResources().getString(R.string.info_home_greeting_welcome);
        }
        mHomeGreetingTextView.setText(mHomeGreetingTextMsg);
        
        //Adjust TextView greeting programatically:
        int screenOrientation = getResources().getConfiguration().orientation;
        if(screenOrientation != Configuration.ORIENTATION_LANDSCAPE) {
            //Potraits or square (!):
            LayoutParams params = (LayoutParams) mHomeGreetingTextView.getLayoutParams();
            params.setMargins(20, 0, 20, 0);
            mHomeGreetingTextView.setLayoutParams(params);
            
        } else { 
            /* Landscape: 
             * -> Adjust TextView to populate only 50% of main layout ("mView") width.
             * -> It is NOT SIMPLE to get either device screen dimensions or current layout dimensions.
             *    Most direct attempts "getWidth()" or "getMeasureWidth()" will return ZERO!
             *    In order to get a mere main layout width/height, we must invoke layout listener,
             *    and retrieve the value when it is ready.
             * -> Ref: http://stackoverflow.com/questions/21926644/get-height-and-width-of-a-layout-programatically
            */
            ViewTreeObserver vto = mView.getViewTreeObserver(); 
            vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() { 
                @SuppressWarnings("deprecation")
                @Override 
                public void onGlobalLayout() { 
                    mView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    mHomeGreetingTextView.setWidth(mView.getMeasuredWidth()/2);
                } 
            });
        }
    }

}
