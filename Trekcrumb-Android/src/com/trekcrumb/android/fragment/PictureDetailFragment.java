package com.trekcrumb.android.fragment;

import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PictureImageRetrieveAsyncTask;
import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.layoutholder.PictureDetailLayoutHolder;
import com.trekcrumb.android.otherimpl.ImageMoveOnTouchListenerImpl;
import com.trekcrumb.android.otherimpl.ZoomControlsImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;

public class PictureDetailFragment 
extends BaseFragment {
    public static final String TAG_FRAGMENT_NAME = PictureDetailFragment.class.getName();
    private static final String TAG_LOG = "PictureDetailFragment";
    
    //Main properties
    private Trip mTrip;
    private List<Picture> mListOfPictures;
    private Picture mPictureSelected;
    private int mPictureSelectedIndex;
    private int mNumOfPictures;
    private boolean mIsShowDetail;
    
    //Views
    private PictureDetailLayoutHolder mLayoutHolder;
    private View mProgressBarImgLoadingSection;
    private View mImgSection;
    private ImageView mImgCurrent;
    private TextView mImgErrorText;
    private Button mMenuPrevious;
    private Button mMenuNext;
    private ZoomControlsImpl mZoomControl;
    private Button mMenuPageShow;
    private Button mMenuPageHide;
    private View mMenuPageContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        mFragmentRootView = inflater.inflate(R.layout.fragment_picture_detail, parentViewGroup, false);
        return mFragmentRootView;
    }

    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceBundle) {
        super.onActivityCreated(savedInstanceBundle);
        
        if(savedInstanceBundle == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mTrip = (Trip)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mPictureSelectedIndex = dataBundle.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
                
                //Default to always show details initially:
                mIsShowDetail = true;
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mPictureSelectedIndex = savedInstanceBundle.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            mIsShowDetail = savedInstanceBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_IS_SHOW_DETAIL);
        }
        
        if(mPictureSelectedIndex < 0) {
            mPictureSelectedIndex = 0;
        }
        
        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPictureSelectedIndex);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_IS_SHOW_DETAIL, mIsShowDetail);
    }
    
    public void refresh(Trip tripRefreshed) {
        mTrip = tripRefreshed;

        //Ensure the fragment is attached to an activity before relaunch the init()
        if(getActivity() != null) {
            init();
        }
    }
    
    /**
     * Updates picture details and image given the new picture index.
     */
    public void updatePictureDetailAndImage(int selectedIndex) {
        mPictureSelectedIndex = selectedIndex;
        mPictureSelected = mListOfPictures.get(mPictureSelectedIndex);
        mLayoutHolder.populateLayoutWithValues(
                getActivity(),
                mTrip, mPictureSelected, mPictureSelectedIndex,
                getResources().getString(R.string.label_common_numberOfTotal),
                mIsUserLoginTrip);
        
        populatePictureImages(false, false);
    }

    private void init() {
        cleanErrorAndInfoMessages();
        if(mTrip == null) {
            populateNoData(getResources().getString(R.string.error_trip_notFound));
            return;
        } else if(mTrip.getNumOfPictures() == 0) {
            populateNoData(getResources().getString(R.string.info_common_noData));
            return;
        }

        //Else:
        initPropertiesAndLayout();
        setupClickables();
        populatePictureDetails();
        populatePictureImages(true, true);
    }
    
    private void populateNoData(String errorMsg) {
        NoticeMessageUtil.displayMessageAsContent(getActivity(), (ViewGroup)mFragmentRootView, errorMsg, true);
    }
    
    private void initPropertiesAndLayout() {
        mListOfPictures = mTrip.getListOfPictures();
        mNumOfPictures = mListOfPictures.size();
        if(mPictureSelectedIndex > (mNumOfPictures-1)) {
            //Avoid ArrayOutOfBound error
            mPictureSelectedIndex = 0;
        }
        mPictureSelected = mListOfPictures.get(mPictureSelectedIndex);
        mLayoutHolder = new PictureDetailLayoutHolder(mFragmentRootView);

        //ProgressBar:
        mProgressBarImgLoadingSection = mFragmentRootView.findViewById(R.id.progressBarLayoutID);
        TextView progressBarText = (TextView) mProgressBarImgLoadingSection.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_picture_imageLoading);
        progressBarText.setTextColor(getResources().getColor(R.color.color_white));
        
        //Views variables:
        mImgSection = mFragmentRootView.findViewById(R.id.picImageLayoutID);
        mImgCurrent = (ImageView) mFragmentRootView.findViewById(R.id.picImageID);
        mImgCurrent.setOnTouchListener(new ImageMoveOnTouchListenerImpl());
        mImgErrorText = (TextView)mFragmentRootView.findViewById(R.id.picImageErrorTextID);
        mZoomControl = (ZoomControlsImpl) mFragmentRootView.findViewById(R.id.zoomControlID);
        mMenuPrevious = (Button)mFragmentRootView.findViewById(R.id.menuGoPreviousID);
        mMenuNext = (Button)mFragmentRootView.findViewById(R.id.menuGoNextID);
        if(mTrip.getNumOfPictures() < 2) {
            mMenuNext.setVisibility(View.GONE);
            mMenuPrevious.setVisibility(View.GONE);
            mMenuNext = null;
            mMenuPrevious = null;
        }

        mMenuPageShow = (Button) mFragmentRootView.findViewById(R.id.menuPageShowID);
        mMenuPageHide = (Button) mFragmentRootView.findViewById(R.id.menuPageHideID);
        mMenuPageContent = mFragmentRootView.findViewById(R.id.menuPageContentID);

        //UserLogin only section:
        if(mIsUserLoginTrip) {
            if(mTrip.getPublish() == TripPublishEnum.PUBLISH
                    && (!mIsUserAuthenticated || !mIsUserTripSynced)) {
                //Trip is not editable:
                doHideMenuForUserLoginTrip();
            }
        } else {
            //userOther's trip:
            doHideMenuForUserLoginTrip();
        }
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity. Tragic.
     */
    private void setupClickables() {
        if(mMenuNext != null) {
            mMenuNext.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doNext();
                        }
                    }); 
        }
        
        if(mMenuPrevious != null) {
            mMenuPrevious.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            doPrevious();
                        }
                    }); 
        }
        
        mMenuPageShow.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mIsShowDetail = true;
                        doShowMenuPageContent();
                    }
                }); 
        
        mMenuPageHide.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mIsShowDetail = false;
                        doHideMenuPageContent();
                    }
                }); 

        Button menuPicEdit = (Button) mFragmentRootView.findViewById(R.id.menuPicEditID);
        if(menuPicEdit != null) {
            menuPicEdit.setOnClickListener(
                    new View.OnClickListener() {
                        public void onClick(View v) {
                            ActivityUtil.doPictureEditActivity(
                                    getActivity(), mTrip, mPictureSelected, mPictureSelectedIndex);
                        }
                    }); 
        }
    }    
    
    /*
     * Populate picture details and whether to show or hide it. 
     */
    private void populatePictureDetails() {
        mLayoutHolder.populateLayoutWithValues(
                getActivity(),
                mTrip, mPictureSelected, mPictureSelectedIndex,
                getResources().getString(R.string.label_common_numberOfTotal),
                mIsUserLoginTrip);
        
        if(mIsShowDetail) {
            doShowMenuPageContent();
        } else {
            doHideMenuPageContent();
        }
    }
    
    private void doShowMenuPageContent() {
        mMenuPageShow.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.VISIBLE);
        
        doHideZoomControls();
    }
    
    private void doHideMenuPageContent() {
        mMenuPageShow.setVisibility(View.VISIBLE);
        mMenuPageContent.setVisibility(View.GONE);
        mMenuPageHide.setVisibility(View.GONE);
        
        doShowZoomControls();
    }
    
    private void doNext() {
        if(mPictureSelectedIndex >= (mNumOfPictures - 1)) {
            //Invalid call, no more 'Next'
            return;
        }
        mPictureSelectedIndex++;
        mPictureSelected = mListOfPictures.get(mPictureSelectedIndex);
        populatePictureDetails();
        populatePictureImages(true, true);
    }
    
    private void doPrevious() {
        if(mPictureSelectedIndex == 0) {
            //Invalid call, no more 'Previous'
            return;
        }
        
        mPictureSelectedIndex--;
        mPictureSelected = mListOfPictures.get(mPictureSelectedIndex);
        populatePictureDetails();
        populatePictureImages(true, true);
    }
    
    private void doShowZoomControls() {
        mZoomControl.setVisibility(View.VISIBLE);
        mZoomControl.setImage(mImgCurrent);
        mZoomControl.setIsZoomEnabled(true);
    }
    
    private void doHideZoomControls() {
        mZoomControl.setImage(null);
        mZoomControl.setVisibility(View.GONE);
    }
    
    private void doHideMenuForUserLoginTrip() {
        //No edit menus:
        View menuForUserLoginTrip = mFragmentRootView.findViewById(R.id.menuForUserLoginTripID);
        menuForUserLoginTrip.setVisibility(View.GONE);
        
        View menuSectionSeparator = mFragmentRootView.findViewById(R.id.menuSectionSeparatorID);
        menuSectionSeparator.setVisibility(View.GONE);
    }
    
    private void populatePictureImages(
            boolean isPopulateNextPicture,
            boolean isPopulatePreviousPicture) {
        //Disable these buttons by default:
        if(mMenuNext != null) {
            mMenuNext.setVisibility(View.GONE);
        }
        if(mMenuPrevious != null) {
            mMenuPrevious.setVisibility(View.GONE);
        }
        doHideZoomControls();

        //Current image:
        ImageCacheBean imgResultCurrent = CacheMemoryManager.getImageFromCache(getActivity(), mPictureSelected.getImageName());
        if(imgResultCurrent == null) {
            //Not in cache:
            mImgSection.setVisibility(View.GONE);
            mProgressBarImgLoadingSection.setVisibility(View.VISIBLE);

            ImageCacheBean result = new ImageCacheBean();
            result.setKey(mPictureSelected.getImageName());
            result.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
            CacheMemoryManager.addImageToCache(getActivity(), mPictureSelected.getImageName(), result);

            PictureImageRetrieveAsyncTask backendTask = 
                    new PictureImageRetrieveAsyncTask(this, mPictureSelected, mIsUserLoginTrip, false, -1);
            backendTask.execute();

        } else if(imgResultCurrent.getServiceStatus() == ServiceStatusEnum.STATUS_WAITING) {
            //In cache but still loading. Continue waiting
            mImgSection.setVisibility(View.GONE);
            mProgressBarImgLoadingSection.setVisibility(View.VISIBLE);
            
        } else {
            //Image ready:
            displayImageCurrent(imgResultCurrent);
        }
        
        //Previous image:
        if(mPictureSelectedIndex > 0) {
            if(isPopulatePreviousPicture) {
                if(mMenuPrevious != null) {
                    mMenuPrevious.setVisibility(View.VISIBLE);
                }

                Picture picturePrevious = mListOfPictures.get(mPictureSelectedIndex - 1);
                if(CacheMemoryManager.getImageFromCache(getActivity(), picturePrevious.getImageName()) == null) {
                    ImageCacheBean result = new ImageCacheBean();
                    result.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
                    CacheMemoryManager.addImageToCache(getActivity(), picturePrevious.getImageName(), result);

                    PictureImageRetrieveAsyncTask backendTask = 
                            new PictureImageRetrieveAsyncTask(this, picturePrevious, mIsUserLoginTrip, false, -1);
                    backendTask.execute();
                }
            }
        } //Else: No more 'Previous'
        
        //Next image:
        if(mPictureSelectedIndex < (mNumOfPictures - 1)) {
            if(isPopulateNextPicture) {
                if(mMenuNext != null) {
                    mMenuNext.setVisibility(View.VISIBLE);
                }

                Picture pictureNext = mListOfPictures.get(mPictureSelectedIndex + 1);
                if(CacheMemoryManager.getImageFromCache(getActivity(), pictureNext.getImageName()) == null) {
                    ImageCacheBean result = new ImageCacheBean();
                    result.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
                    CacheMemoryManager.addImageToCache(getActivity(), pictureNext.getImageName(), result);

                    PictureImageRetrieveAsyncTask backendTask = 
                            new PictureImageRetrieveAsyncTask(this, pictureNext, mIsUserLoginTrip, false, -1);
                    backendTask.execute();
                }
            }
        } //Else: No more 'Next'
    }
    
    /*
     * Callback when back-end components complete retrieving image from cache or repository.
     * It does the following:
     * 1. Check if the returned result is good and not null
     * 2. Only if the result status is success, then put into cache. Otherwise do not, and remove it
     *    if it was in the cache previously.
     * 3. If this is for current image, display it.
     */
    public void onPictureImageDetailReadyCallback(ImageCacheBean imageResult) {
        Log.d(TAG_LOG, "onPictureImageDetailReadyCallback() - Callback invoked for image result: " + imageResult);
        
        //TODO: What if result == NULL?

        if(imageResult.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS) {
            CacheMemoryManager.addImageToCache(getActivity(), imageResult.getKey(), imageResult);
        } else {
            CacheMemoryManager.removeImageFromCache(getActivity(), imageResult.getKey());
        }

        if(mPictureSelected.getImageName().equalsIgnoreCase(imageResult.getKey())) {
            displayImageCurrent(imageResult);
        }
    }
    
    private void displayImageCurrent(ImageCacheBean imageResult) {
        mProgressBarImgLoadingSection.setVisibility(View.GONE);
        mImgSection.setVisibility(View.VISIBLE);
        
        mImgCurrent.setImageBitmap(imageResult.getValue());
        if(imageResult.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS) {
            resetImageView(true);
            
            if(!mIsShowDetail) {
                doShowZoomControls();
            }

            mImgErrorText.setVisibility(View.GONE);
            
        } else {
            resetImageView(false);
            doHideZoomControls();
            mImgErrorText.setVisibility(View.VISIBLE);
            mImgErrorText.setText(imageResult.getErrorMsg());
        }
    }
    
    /*
     * 1. Reset any previous click/move event to its original position (x,y)
     * 2. Reset any previous scaling (zoom in/out) to its original scale
     * 3. Set AdjustViewBounds to 'true' to maintain image ratio
     * 3. If good image:
     *    - Expand image layout and view to fill entire screen thus enabling zoom-in to fill
     *      entire screen, and move from corner to corner.
     *    - Set clickable to 'true' to enable ImageMoveOnTouchListener
     *    - DO NOT set image setScaleType = ScaleType.FIT_XY, since this will stretch image to 
     *      fill screen disregarding its ratio.
     *      
     * 4. Else, bad image (image n/a):
     *    - Shrink image layout and view to be enough to wrap content
     *    - Set clickable to 'false' to disable ImageMoveOnTouchListener
     *    - DO NOT set image setScaleType = ScaleType.CENTER, since this will cut off left/right edge of image
     */
    @SuppressWarnings("deprecation")
    private void resetImageView(boolean isGoodImage) {
        //Reset (general):
        mImgCurrent.setTranslationX(0);
        mImgCurrent.setTranslationY(0);
        mImgCurrent.setScaleX(1);
        mImgCurrent.setScaleY(1);
        mImgCurrent.setAdjustViewBounds(true);
        
        if(isGoodImage) {
            mImgSection.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
            mImgSection.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;;
            
            mImgCurrent.getLayoutParams().height = ViewGroup.LayoutParams.FILL_PARENT;
            mImgCurrent.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            mImgCurrent.setClickable(true);
            
        } else {
            mImgSection.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImgSection.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;;

            mImgCurrent.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImgCurrent.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImgCurrent.setClickable(false);
        }
    }

}

