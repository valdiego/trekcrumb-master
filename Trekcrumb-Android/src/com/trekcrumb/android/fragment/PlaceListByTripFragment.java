package com.trekcrumb.android.fragment;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A fragment for List of Places (details and notes) given a Trip. 
 * Note: 
 * This fragment ideally placed within a page such as Trip View/Detail. 
 * For Place Detail within Map, see MapDetailFragment.java.
 * 
 * @author Val Triadi
 * @since 04/2017
 */
public class PlaceListByTripFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "PlaceDetailFragment";
    public static final String TAG_FRAGMENT_NAME = PlaceListByTripFragment.class.getName();
    
    private Trip mTrip;
    
    //Views
    private View mMenuContentShow;
    private View mMenuContentHide;
    private ViewGroup mContentContainer;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_place_list_by_trip, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mTrip = (Trip)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
        }
        
        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
    }
    
    @Override
    public void refresh(Trip trip) {
        mTrip = trip;
        
        //Ensure the fragment is attached to an activity before relaunch the init()
        if(getActivity() != null) {
            init();
        }
    }
    
    private void init() {
        if(mTrip == null || mTrip.getNumOfPlaces() == 0) {
            return;
        }

        mMenuContentShow = mFragmentRootView.findViewById(R.id.menuContentShowID);
        mMenuContentHide = mFragmentRootView.findViewById(R.id.menuContentHideID);
        mContentContainer = (ViewGroup)mFragmentRootView.findViewById(R.id.contentContainerID);

        setupClickables();
        boolean isPlaceWithNoteExist = populateDataValues();
        if(isPlaceWithNoteExist) {
            mFragmentRootView.findViewById(R.id.placeDetailOnTripNoNoteInfoID).setVisibility(View.GONE);
            doShowContent();
        } else {
            mFragmentRootView.findViewById(R.id.placeDetailOnTripNoNoteInfoID).setVisibility(View.VISIBLE);
            doHideContent();
        }
    }
    
    private boolean populateDataValues() {
        List<Place> places = mTrip.getListOfPlaces();
        boolean isPlaceWithNoteExist = false;
        int placeIndexToDisplay = 1;
        for(Place place : places) {
            if(!ValidationUtil.isEmpty(place.getNote())) { 
                if(!isPlaceWithNoteExist) {
                    isPlaceWithNoteExist = true;
                }
                
                //Clone and populate row template:
                ViewGroup row = 
                        (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.template_row_place_by_trip, null);
                ((TextView)row.findViewById(R.id.placeIndexID)).setText("" + placeIndexToDisplay);
                ((TextView)row.findViewById(R.id.placeLocationID)).setText(place.getLocation());
                ((TextView)row.findViewById(R.id.createdDateID)).setText(
                        DateUtil.formatDate(place.getCreated(), DateFormatEnum.GMT_STANDARD_LONG, DateFormatEnum.LOCALE_UI_WITH_TIME));
                ((TextView)row.findViewById(R.id.placeNoteID)).setText(place.getNote());
                
                //Add row to the list:
                mContentContainer.addView(row);
            } 
            
            placeIndexToDisplay++;
        }
        
        return isPlaceWithNoteExist;
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        mMenuContentShow.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doShowContent();
                    }
                }); 
        
        mMenuContentHide.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doHideContent();
                    }
                });
    }
    
    private void doShowContent() {
        mMenuContentShow.setVisibility(View.GONE);
        mMenuContentHide.setVisibility(View.VISIBLE);
        mContentContainer.setVisibility(View.VISIBLE);
    }

    private void doHideContent() {
        mMenuContentShow.setVisibility(View.VISIBLE);
        mMenuContentHide.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.GONE);
    }
    
}
