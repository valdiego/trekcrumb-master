package com.trekcrumb.android.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.otherimpl.TripListAdapterImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Fragment to represent a list of Trips.
 * 1. This fragment is an extension of Android's ListFragment which is a commonly use to display List. 
 *    Consequently, we must implement Android's ArrayAdapter and a LayoutHolder classes to support it.
 *
 */
public class TripListFragment extends ListFragment {
    public static final String TAG_FRAGMENT_NAME = TripListFragment.class.getName();

    private View mFragmentRootView;
    private ArrayList<Trip> mListOfTrips;
    private int mSelectedTripIndex = 0;
    private boolean mIsShowTripSummary;
    private boolean mIsShowTripStatus;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView =  inflater.inflate(R.layout.fragment_generic_list, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  Callback when after fragment is created, its view initialized and the 
     *  hosting (parent) activity is created. It should contain any final initialization.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mListOfTrips = (ArrayList<Trip>)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
                mIsShowTripSummary = dataBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_SUMMARY);
                mIsShowTripStatus = dataBundle.getBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mListOfTrips = (ArrayList<Trip>)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT);
            mIsShowTripSummary = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_SUMMARY);
            mIsShowTripStatus = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS);
            
            mSelectedTripIndex = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
        }
        
        init();
    }

    /**
     * Callback when to save any current dynamic data (state) before pause, so 
     * it can later be reconstructed in a new instance when the fragment is restarted.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_LIST_CURRENT, mListOfTrips);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mSelectedTripIndex);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_SUMMARY, mIsShowTripSummary);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS, mIsShowTripSummary);
    }
     
    public void refresh(
            List<Trip> trips,
            boolean isShowSummary,
            boolean isShowTripStatus) {
        mListOfTrips = (ArrayList<Trip>)trips;
        mIsShowTripSummary = isShowSummary;
        mIsShowTripStatus = isShowTripStatus;
        init();
    }    

    private void init() {
        //Avoid NPE from android ListAdapter:
        if(mListOfTrips == null) {
            mListOfTrips = new ArrayList<Trip>();
        }
        
        //Populate List and its adapter:
        TripListAdapterImpl listAdapter = 
                new TripListAdapterImpl(getActivity(), mListOfTrips, mIsShowTripSummary, mIsShowTripStatus);
        setListAdapter(listAdapter);
    }
    
    /**
     * Callback when an item in the list is selected. 
     */
    @Override
    public void onListItemClick(ListView listView, View view, int selectedIndex, long id) {
        Trip selectedTrip = mListOfTrips.get(selectedIndex);
        mSelectedTripIndex = selectedIndex;
        ActivityUtil.doTripViewActivity(getActivity(), null, selectedTrip, false, null, false);
    }
    

}
