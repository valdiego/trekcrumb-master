package com.trekcrumb.android.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.asynctask.UserCreateAsyncTask;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.ValidationUtil;

public class UserCreateFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "UserCreateFragment";
    public static final String TAG_FRAGMENT_NAME = UserCreateFragment.class.getName();
    
    private BaseActivity mHostActivity;
    private String mNewUsername;
    private String mNewEmail;
    private String mNewPassword;
    private String mNewFullname;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_user_create, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        init();
    }
    
    private void init() {
        mHostActivity = (BaseActivity)getActivity();
        
        TextView termsAndPrivacyAgreementGreetingView = 
                (TextView)mFragmentRootView.findViewById(R.id.termsAndPrivacyAgreementGreetingID);
        String termsAndPrivacyAgreementGreetingStr = 
                String.format(getResources().getString(R.string.info_user_create_read_terms_and_privacy),  
                        "Terms", 
                        "Privacy"); 
        termsAndPrivacyAgreementGreetingView.setText(termsAndPrivacyAgreementGreetingStr);

        setupClickables();
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        View menuSave = mFragmentRootView.findViewById(R.id.buttonSaveID);
        menuSave.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        save();
                    }
                }); 
        
        View menuCancel = mFragmentRootView.findViewById(R.id.buttonCancelID);
        menuCancel.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mHostActivity.finish();
                    }
                }); 
        
        View menuDoNotAcceptAgreement = mFragmentRootView.findViewById(R.id.doNotAcceptAgreementID);
        menuDoNotAcceptAgreement.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mFragmentRootView.findViewById(R.id.buttonCancelAndSaveLayoutID).setVisibility(View.GONE);
                    }
                }); 

        View menuDoAcceptAgreement = mFragmentRootView.findViewById(R.id.doAcceptAgreementID);
        menuDoAcceptAgreement.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mFragmentRootView.findViewById(R.id.buttonCancelAndSaveLayoutID).setVisibility(View.VISIBLE);
                    }
                }); 
    }
    
    private void save() {
        cleanErrorAndInfoMessages();
        hideKeyboard();

        if(!validateForm()) {
            evaluateErrorAndInfo();
            return;
        }
        
        //Call back-end (must be async-task due to network op.)
        mHostActivity.setProgressDialog(NoticeMessageUtil.showProgressDialog(
                mHostActivity, 
                mHostActivity.getLayoutInflater(), 
                getResources().getString(R.string.label_user_create), 
                getResources().getString(R.string.info_common_pleaseWait)));

        User userToCreate = new User();
        userToCreate.setUsername(mNewUsername);
        userToCreate.setEmail(mNewEmail);
        userToCreate.setPassword(mNewPassword);
        userToCreate.setFullname(mNewFullname);
        
        //Defaults:
        userToCreate.setLocation(getResources().getString(R.string.info_user_locationEmpty));
        userToCreate.setSummary(getResources().getString(R.string.info_user_summaryEmpty));

        UserAuthToken userAuthTokenToCreate = new UserAuthToken();
        userAuthTokenToCreate.setDeviceIdentity(AndroidConstants.DEVICE_IDENTITY);

        UserCreateAsyncTask backendTask = 
                new UserCreateAsyncTask(mHostActivity, this, userToCreate, userAuthTokenToCreate);
        backendTask.execute();
    }    
    
    private boolean validateForm() {
        boolean isValid = true;
        try {
            mNewUsername = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.usernameValueID)).getText());
            mNewEmail = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.emailValueID)).getText());
            mNewPassword = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.passwordValueID)).getText());
            mNewFullname = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.fullnameValueID)).getText());
        } catch(NullPointerException npe) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_invalidParam_allRequired));
            return false;
        }
        
        if(ValidationUtil.isEmpty(mNewUsername)
                || ValidationUtil.isEmpty(mNewEmail)
                || ValidationUtil.isEmpty(mNewPassword)
                || ValidationUtil.isEmpty(mNewFullname)) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_invalidParam_allRequired));
            return false;
        }
        
        try {
            ValidationUtil.validateUsername(mNewUsername, true);
        } catch(IllegalArgumentException iae) {
            addErrorOrInfoMessages(true, iae.getMessage());
            isValid = false;
        }
        try {
            ValidationUtil.validateEmail(mNewEmail, true);
        } catch(IllegalArgumentException iae) {
            addErrorOrInfoMessages(true, iae.getMessage());
            isValid = false;
        }
        try {
            ValidationUtil.validatePassword(mNewPassword, true);
        } catch(IllegalArgumentException iae) {
            addErrorOrInfoMessages(true, iae.getMessage());
            isValid = false;
        }
        return isValid;
    }
    
    /**
     * Callback by the back-end refresh business logic once it is done.
     */
    @Override
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        if(getActivity() == null) {
            return false;
        }

        mHostActivity.onAsyncBackendTaskCompleted(true);
        
        if(result == null) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_commonError));
        } else if(!result.isSuccess()) {
            if(result.getServiceError() != null) {
                if(result.getServiceError().getErrorEnum() == ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR) {
                    addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_duplicateUser));
                } else {
                    addErrorOrInfoMessages(true, result.getServiceError().getErrorText());
                }
            } else {
                addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_commonError));
            }
        
        } else {
            if(result.getUser() != null) {
                mUserLogin = result.getUser();
                SessionUtil.setUserLoginDataInSession(
                        (TrekcrumbApplicationImpl) mHostActivity.getApplicationContext(),
                        mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(mHostActivity, mUserLogin)); 
                String greetingMsg = getResources().getString(R.string.info_user_create_success);
                ActivityUtil.doHomeActivity(mHostActivity, true, false, greetingMsg);
                
            } else {
                String greetingMsg = getResources().getString(R.string.info_user_create_success_NeedLogin);
                ActivityUtil.doHomeActivity(mHostActivity, true, false, greetingMsg);
            }
        } 
        
        evaluateErrorAndInfo();
        return true;
    }        
}
