package com.trekcrumb.android.fragment;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.activity.BaseActivity;
import com.trekcrumb.android.asynctask.UserLoginAsyncTask;
import com.trekcrumb.android.business.CacheDiskManager;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.otherimpl.TrekcrumbApplicationImpl;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.NoticeMessageUtil;
import com.trekcrumb.android.utility.SessionUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.ValidationUtil;

public class UserLoginFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "UserLoginFragment";
    public static final String TAG_FRAGMENT_NAME = UserLoginFragment.class.getName();
    
    private BaseActivity mHostActivity;
    private String mUsername;
    private String mPassword;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_user_login, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        init();
    }
    
    private void init() {
        mHostActivity = (BaseActivity)getActivity();
        
        Button menuLogin = (Button)mFragmentRootView.findViewById(R.id.buttonSaveID);
        menuLogin.setText(getResources().getText(R.string.label_common_login));
        
        setupClickables();
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        View menuLogin = mFragmentRootView.findViewById(R.id.buttonSaveID);
        menuLogin.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        login();
                    }
                }); 
        
        View menuCancel = mFragmentRootView.findViewById(R.id.buttonCancelID);
        menuCancel.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mHostActivity.finish();
                    }
                }); 
    }
    
    private void login() {
        cleanErrorAndInfoMessages();
        hideKeyboard();
        
        if(!validateForm()) {
            evaluateErrorAndInfo();
            return;
        }
        
        //Call back-end (must be async-task due to network op.)
        mHostActivity.setProgressDialog(
                NoticeMessageUtil.showProgressDialog(
                    mHostActivity, 
                    mHostActivity.getLayoutInflater(), 
                    getResources().getString(R.string.label_user_login), 
                    getResources().getString(R.string.info_common_pleaseWait)));

        User userToLogin = new User();
        userToLogin.setUsername(mUsername);
        userToLogin.setPassword(mPassword);
        
        UserAuthToken userAuthTokenToCreate = new UserAuthToken();
        userAuthTokenToCreate.setDeviceIdentity(AndroidConstants.DEVICE_IDENTITY);

        UserLoginAsyncTask backendTask = 
                new UserLoginAsyncTask(mHostActivity, this, userToLogin, userAuthTokenToCreate);
        backendTask.execute();
    }
    
    private boolean validateForm() {
        boolean isValid = true;
        try {
            mUsername = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.usernameValueID)).getText());
            mPassword = String.valueOf(((TextView)mFragmentRootView.findViewById(R.id.passwordValueID)).getText());
        } catch(NullPointerException npe) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_invalidParam_loginAllRequired));
            return false;
        }
        if(ValidationUtil.isEmpty(mUsername)
                || ValidationUtil.isEmpty(mPassword)) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_invalidParam_loginAllRequired));
            return false;
        }
        return isValid;
    }
    
    /**
     * Callback by the back-end refresh business logic once it is done.
     */
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        if(getActivity() == null) {
            return false;
        }
        
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(!isSuccess) {
            if(result != null
                    && result.getServiceError() != null
                    && result.getServiceError().getErrorEnum() == ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR
                    && result.getUser() != null
                    && !ValidationUtil.isEmpty(result.getUser().getUserId())) {
                ActivityUtil.doUserReactivateActivity(mHostActivity, result.getUser());
            }
            return isSuccess;
        }
        
        //Success:
        if(result.getUser() == null) {
            Log.e(TAG_LOGGER, "Remote Server error: Login is successful, BUT returned user data is empty!");
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_user_LoginFailed));
            evaluateErrorAndInfo();
            return false;

        } else {
            mUserLogin = result.getUser();
            SessionUtil.setUserLoginDataInSession(
                    (TrekcrumbApplicationImpl) mHostActivity.getApplicationContext(),
                    mUserLogin, true, TripManager.evaluateTripRecordsUpToDate(mHostActivity, mUserLogin)); 
            
            /*
             * Refreshes ALL caches:
             * 
             * Android Release Req. PS-P2:
             * 1. See DeviceUtil.enableStrictMode() for details. 
             * 2. In this step, we must read/write disk via CacheDiskManager from main thread, 
             *    instead of using async threads; it causes StrictMode to fail due to violation. 
             *    To ignore the violation, we temporarily gave permissions to read/write here:
             *    'permitDiskReads' and 'permitDiskWrites'
             *    THEN, reset the StrictMode policy back afterwards.
             */
            StrictMode.ThreadPolicy validStrictMode = StrictMode.getThreadPolicy();
            if(validStrictMode != null) {
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(validStrictMode)
                .permitDiskWrites()
                .permitDiskReads()
                .build());
            }
            
            CacheMemoryManager.cleanAll();
            CacheDiskManager.cleanAndClose(mHostActivity);

            if(validStrictMode != null) {
                StrictMode.setThreadPolicy(validStrictMode);
            }

            ActivityUtil.doHomeActivity(mHostActivity, true, false, null);
        }
        return isSuccess;
    }
    
    
}
