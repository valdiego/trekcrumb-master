package com.trekcrumb.android.fragment;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.FileUtil;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.android.utility.SingletonFactory;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;

public class PictureSelectFragment 
extends BaseFragment {
    private static final String TAG_LOGGER = "PictureSelectFragment";
    private static final String KEY_BUNDLE_PREFIX_ID = "KEY_BUNDLE_PREFIX_ID";
    private static final int ACTIVITY_RESULT_CODE_SELECT_PICTURE_VIA_GALLERY = 101;
    private static final int ACTIVITY_RESULT_CODE_SELECT_PICTURE_VIA_CAMERA = 102;

    private View mFragmentView;
    private String mPicToCreateURL;
    private boolean mIsLoadByCamera;
    private Bitmap mPicToCreatePreviewBMP;
    private String mPicIDPrefix;
    
    //Views:
    private ImageView mPicReviewView;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentView = inflater.inflate(R.layout.fragment_picture_select, parentViewGroup, false);
        return mFragmentView;
    }
    
    /**
     *  Callback when after fragment is created, its view initialized and the 
     *  hosting (parent) activity is created. It should contain any final initialization.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw: None
            
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mPicToCreateURL = savedInstanceState.getString(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED_URL);
            mIsLoadByCamera = savedInstanceState.getBoolean(CommonConstants.KEY_SESSIONOBJ_PICTURE_IS_BY_CAMERA);
            mPicToCreatePreviewBMP = (Bitmap)savedInstanceState.getParcelable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED_BLOB);
            mPicIDPrefix = savedInstanceState.getString(KEY_BUNDLE_PREFIX_ID);
        }
        
        init();
    }
    
    /**
     * Callback method called by the system before putting the activity into paused/stopped state.
     * Persists current variable values so they will not lost.
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putString(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED_URL, mPicToCreateURL);
        outStateBundle.putBoolean(CommonConstants.KEY_SESSIONOBJ_PICTURE_IS_BY_CAMERA, mIsLoadByCamera);
        outStateBundle.putParcelable(CommonConstants.KEY_SESSIONOBJ_PICTURE_SELECTED_BLOB, mPicToCreatePreviewBMP);
        outStateBundle.putString(KEY_BUNDLE_PREFIX_ID, mPicIDPrefix);
    }
    
    public String getPictureToCreateURL() {
        return mPicToCreateURL;
    }
    
    public boolean isLoadByCamera() {
        return mIsLoadByCamera;
    }
    
    public void refresh(String picIDPrefix) {
        mPicIDPrefix = picIDPrefix;

        cleanTemporaryPicFile();
        mIsLoadByCamera = false;
        mPicToCreateURL = null;
        mPicToCreatePreviewBMP = null;
    }
    
    /**
     * Cleans up temporary picture file, if any, taken by Camera.
     * 
     * Usage examples: In the case user cancel to create after taking new picture, or switch option
     * between Camera and Gallery, or switch between new pictures with Camera, or user click
     * 'BACK' button.
     */
    public void cleanTemporaryPicFile() {
        if(mIsLoadByCamera && mPicToCreateURL != null) {
            FileUtil.deleteFile(mPicToCreateURL);
            mPicToCreateURL = null;
            mPicToCreatePreviewBMP = null;
            
            if(mPicReviewView == null) {
                mPicReviewView = (ImageView) mFragmentView.findViewById(R.id.picReviewID);
            }
            mPicReviewView.setImageDrawable(null);
        }
    }
    
    private void init() {
        mPicReviewView = (ImageView) mFragmentView.findViewById(R.id.picReviewID);
        
        if(mPicToCreatePreviewBMP != null) {
            mPicReviewView.setImageBitmap(mPicToCreatePreviewBMP);
        }

        setupClickables();
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity. Tragic.
     */
    private void setupClickables() {
        View menuUploadPicFromGallery = mFragmentView.findViewById(R.id.uploadPicFromGalleryID);
        menuUploadPicFromGallery.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View v) {
                    doLaunchGalleryActivity();
                }
            });
        
        View menuUploadPicFromCamera = mFragmentView.findViewById(R.id.uploadPicFromCameraID);
        menuUploadPicFromCamera.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View v) {
                    doLaunchCameraActivity();
                }
            });
    }    

    /*
     * Launch an activity to open available Gallery applications on the device to load picture. 
     * Upon success, the system will callback onActivityResult().
     */
    private void doLaunchGalleryActivity() {
        cleanTemporaryPicFile();
        mIsLoadByCamera = false;
        mPicToCreatePreviewBMP = null;
        
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                ACTIVITY_RESULT_CODE_SELECT_PICTURE_VIA_GALLERY);
    }
    
    /*
     * Launch an activity to open available Camera applications on the device to load picture. 
     * It does the following:
     * 1. Validate if the device has built-in camera and camera app
     * 2. Validate if the device supports the Intent to capture image
     * 3. Create a temporary file to where the camera will save the new image into.
     *    NOTE 6/2013: The camera returns only a thumbnail image unless this temp. file path is provided.
     * 4. Trigger camera app. activity
     * 
     * Upon success, the system will callback onActivityResult().
     */
    private void doLaunchCameraActivity() {
        boolean isDeviceHasCamera = getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
        if(!isDeviceHasCamera) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_device_CameraNotAvailable));
            evaluateErrorAndInfo();
            return;
        }

        Intent intent = new Intent(); 
        intent.setAction(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        List<ResolveInfo> availableIntentList = 
                getActivity().getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if(availableIntentList == null || availableIntentList.size() == 0) {
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_system_device_CameraNotAvailable));
            evaluateErrorAndInfo();
            return;
        }
        
        mIsLoadByCamera = true;
        mPicToCreatePreviewBMP = null;
        
        //Create (temporary) file to which the taken image will be saved by Camera:
        cleanTemporaryPicFile();
        File newPicFile = new File(
                AndroidConstants.APP_LOCAL_DIR_CACHE + "/" + CommonUtil.generatePictureFilename(mPicIDPrefix));
        mPicToCreateURL = newPicFile.getAbsolutePath();
        Uri picFileUri = Uri.fromFile(newPicFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picFileUri);
        startActivityForResult(intent, ACTIVITY_RESULT_CODE_SELECT_PICTURE_VIA_CAMERA); 
    }
    
    /**
     * Callback method called by the system after startActivityForResult() is completed.
     * @see android.app.Fragment#onActivityResult(int, int, Intent)
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
        Log.d(TAG_LOGGER, "onActivityResult() - Starting with request code [" 
                + requestCode + "] and result code [" + resultCode + "]");
        
        cleanErrorAndInfoMessages();
        if(resultCode == Activity.RESULT_CANCELED) {
            cleanTemporaryPicFile();
            return;
        }
        if(resultCode != Activity.RESULT_OK) {
            Log.e(TAG_LOGGER, "onActivityResult() - ERROR: Failed to upload picture. Result code: " + resultCode);
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_picture_uploadFailed));
            evaluateErrorAndInfo();
            return;
        }
        if(requestCode == ACTIVITY_RESULT_CODE_SELECT_PICTURE_VIA_GALLERY) {
            doUploadPictureResultViaGallery(intentData);
        } else if(requestCode == ACTIVITY_RESULT_CODE_SELECT_PICTURE_VIA_CAMERA) {
            doUploadPictureResulViaCamera(intentData);
        } else {
            Log.e(TAG_LOGGER, "onActivityResult() - ERROR: Unknown returned result code: " + resultCode);
        }
    }
    
    private void doUploadPictureResultViaGallery(Intent intentData) {
        if(intentData == null || intentData.getData() == null) {
            Log.e(TAG_LOGGER, "doUploadPictureResultViaGallery() - ERROR: Failed to upload picture. Result Intent data is NULL or contain empty data!");
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_picture_uploadFromGalleryFailed));
            evaluateErrorAndInfo();
            return;
            
        } else {
            //Get the selected image from Gallery source path:
            Uri selectedPicURI = intentData.getData();
            String[] filePathQueryColumns = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(selectedPicURI, filePathQueryColumns, null, null, null);
            if(cursor != null) {
                try {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathQueryColumns[0]);
                    mPicToCreateURL = cursor.getString(columnIndex);
                } finally {
                    cursor.close();
                }
            }
            
            if(mPicToCreateURL == null) {
                Log.e(TAG_LOGGER, "doUploadPictureResultViaGallery() - ERROR: Failed to upload picture. Returned file URL path is NULL!");
                addErrorOrInfoMessages(true, getResources().getString(R.string.error_picture_uploadFromGalleryFailed));
                evaluateErrorAndInfo();
                return;
            }
            
            displayPicToCreatePreview();
        }
    }
    
    /*
     * NOTE 6/2013:
     * The camera image result could be varied among devices, unfortunately. 
     * Some devices may return the taken image in the incoming Intent.getExtras() and some do not (null). 
     * However, the Intent.getExtras() only contains the thumbnail snapshot of the actual image. 
     * The actual image itself would only be saved if we specify where to save it: 'mPicToCreateURL'.
     */
    private void doUploadPictureResulViaCamera(Intent intentData) {
        //Validate the image exists:
        File newImageTaken = new File(mPicToCreateURL);
        if(!newImageTaken.exists()) {
            Log.e(TAG_LOGGER, "doUploadPictureResulViaCamera() - ERROR: Failed to find the new picture file at specified folder!");
            addErrorOrInfoMessages(true, getResources().getString(R.string.error_picture_uploadFromCameraFailed));
            evaluateErrorAndInfo();
            return;
        }
        
        displayPicToCreatePreview();
    }
    
    /*
     * Android ImageView's can only hold maximum 2048x2048 image, and we must use setImageBitmap().
     * Cannot use ImageView.setImageURI() because we cannot guarantee how big the source image is.
     */
    private void displayPicToCreatePreview() {
        mPicToCreatePreviewBMP = ImageUtil.decodePictureImageIntoBitmap(
                null, 
                mPicToCreateURL, 
                SingletonFactory.getPictureViewSize(getActivity()), 
                true, false);
        ImageView picReviewView = (ImageView) mFragmentView.findViewById(R.id.picReviewID);
        picReviewView.setImageBitmap(mPicToCreatePreviewBMP);
        
        //Note: Alternatively use ImageView.setImageURI(), but only works if image size <= 2048x2048
        /*
        ImageView picReviewView = (ImageView) findViewById(R.id.picReviewID);
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.path(mPicToCreateURL);
        picReviewView.setImageURI(uriBuilder.build());
        */
    }
    
}
