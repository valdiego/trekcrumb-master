package com.trekcrumb.android.fragment;

import java.util.List;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.CommentRetrieveAsyncTask;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A fragment for Trip's List of Comments. 
 * 1. This fragment is NOT an extension of Android's ListFragment (which is a commonly use to display List),
 *    but our custom BaseFragment. It operates in raw mode where we would populate rows manually in code. 
 * 
 * 2. Issues and Resolutions
 *    (1) Issue: Fragment variables LOST during detach/attach ops
 *        See TripDetailFragment.java javadoc for details of this issue.
 * 
 * @author Val Triadi
 * @since 04/2017
 */
public class CommentListByTripFragment 
extends BaseFragment {
    public static final String TAG_LOGGER = "CommentListByTripFragment";
    public static final String TAG_FRAGMENT_NAME = CommentListByTripFragment.class.getName();
    
    private Trip mTrip;
    private int mNumOfTotalComments;
    private int mOffset = 0;
    
    //(1) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
    Bundle mFragmentArgumentBundle;
    
    //Views
    private View mContentContainer;
    private ViewGroup mListRowsContainer;
    private View mMenuContentShow;
    private View mMenuContentHide;
    private View mProgressBarSectionView;
    private View mMenuMore;

    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_comment_by_trip, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            mFragmentArgumentBundle = getArguments();
            if(mFragmentArgumentBundle != null) {
                mTrip = (Trip) mFragmentArgumentBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mOffset = mFragmentArgumentBundle.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mOffset = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET);
            
            //(1) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
            mFragmentArgumentBundle = getArguments();
        }
        
        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET, mOffset);
    }
    
    @Override
    public void refresh(Trip trip) {
        mTrip = trip;
        mOffset = 0;
        
        //Ensure the fragment is attached to an activity before relaunch the init()
        if(getActivity() != null) {
            init();
        }
    }
    
    private void init() {
        if(mTrip == null) {
            return;
        }
        
        initPropertiesAndLayout();
        setupClickables();
        
        if(mNumOfTotalComments == 0) {
            populateNoData();
            return;
        }

        //Else:
        populateDataValues();
    }
    
    private void initPropertiesAndLayout() {
        mNumOfTotalComments = mTrip.getNumOfComments();
        
        mMenuContentShow = mFragmentRootView.findViewById(R.id.menuContentShowID);
        mMenuContentHide = mFragmentRootView.findViewById(R.id.menuContentHideID);
        mContentContainer = (ViewGroup)mFragmentRootView.findViewById(R.id.contentContainerID);
        mListRowsContainer = (ViewGroup) mFragmentRootView.findViewById(R.id.listRowsContainerID);
        mMenuMore = mFragmentRootView.findViewById(R.id.commentMenuMoreID);
        
        //Progress bar:
        mProgressBarSectionView = mFragmentRootView.findViewById(R.id.commentProgressBarID);
        TextView progressBarText = (TextView) mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_common_pleaseWait);
        
        //By default, hide these:
        mProgressBarSectionView.setVisibility(View.GONE);
        mMenuMore.setVisibility(View.GONE);
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        mMenuContentShow.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doShowContent();
                    }
                }); 
        
        mMenuContentHide.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        doHideContent();
                    }
                });
        
        mMenuMore.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        retrieveComments();
                    }
                });
    }
    
    private void populateNoData() {
        ((TextView)mFragmentRootView.findViewById(R.id.numOfCommentsID)).setText("0");
        mFragmentRootView.findViewById(R.id.commentOnTripEmptyInfoID).setVisibility(View.VISIBLE);
        doHideContent();
    }
    
    private void populateDataValues() {
        ((TextView)mFragmentRootView.findViewById(R.id.numOfCommentsID)).setText("" + mNumOfTotalComments);
        mFragmentRootView.findViewById(R.id.commentOnTripEmptyInfoID).setVisibility(View.GONE);
        
        /*
         * During init(), we only populate and display the list when it is handily available
         * inside the Trip object. If not, no action taken until user triggers 'doShowContent()'.
         * If user never reads the list content, we save ourselves from useless server call.
         * See doShowContent().
         */
        List<Comment> comments = mTrip.getListOfComments();
        if(comments != null && comments.size() > 0) {
            displayList(comments);
        }
        
        //By default - hide it
        doHideContent();
    }
    
    private void displayList(List<Comment> comments) {
        mListRowsContainer.setVisibility(View.VISIBLE);
        
        for(Comment comment : comments) {
            //Clone and populate row template:
            ViewGroup row = 
                    (ViewGroup)getActivity().getLayoutInflater().inflate(R.layout.template_row_comment, null);
            row.findViewById(R.id.tripSectionID).setVisibility(View.GONE);
            ((TextView)row.findViewById(R.id.commentNoteID)).setText(comment.getNote());
            ((TextView)row.findViewById(R.id.createdDateID)).setText(
                    DateUtil.formatDate(comment.getCreated(), 
                            DateFormatEnum.GMT_STANDARD_LONG, 
                            DateFormatEnum.LOCALE_UI_WITH_TIME));

            //User summary:
            ((TextView)row.findViewById(R.id.fullnameID)).setText(comment.getUserFullname());
            ((TextView)row.findViewById(R.id.usernameID)).setText(comment.getUsername().toLowerCase());

            Button userProfileDefaultView = (Button) row.findViewById(R.id.profileImageDefaultID);
            ImageView userProfileImageView = (ImageView) row.findViewById(R.id.profileImageID);
            if(ValidationUtil.isEmpty(comment.getUserImageName())) {
                userProfileDefaultView.setVisibility(View.VISIBLE);
                userProfileImageView.setVisibility(View.GONE);
            } else {
                ImageUtil.retrieveUserProfileImage(
                        getActivity(), comment.getUserImageName(), userProfileDefaultView, userProfileImageView, 
                        false, true);
            }
            
            //Add row to the list:
            mListRowsContainer.addView(row);
        }
        
        doShowOrHideMenuMore();
    }
    
    private void retrieveComments() {
        mProgressBarSectionView.setVisibility(View.VISIBLE);
        mMenuMore.setVisibility(View.GONE);
        
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setTripID(mTrip.getId());
        CommentRetrieveAsyncTask backendTask = new CommentRetrieveAsyncTask(
                null, this, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, mOffset, null);
        backendTask.execute();
    }
    
    public boolean onAsyncBackendTaskCompleted(ServiceResponse result) {
        boolean isSuccess = super.onAsyncBackendTaskCompleted(result);
        if(!isSuccess) {
            mProgressBarSectionView.setVisibility(View.GONE);
            doShowOrHideMenuMore();
            return isSuccess;
        }
        
        //Success:
        List<Comment> listOfComments = result.getListOfComments();
        mOffset = mOffset + CommonConstants.RECORDS_NUMBER_MAX;
        
        //(1) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
        if(mFragmentArgumentBundle != null) {
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET, mOffset);
        }
        
        if(listOfComments == null || listOfComments.size() == 0) {
            //Possible some users deleted their comments right before we retrieve them:
            Log.e(TAG_LOGGER, "onAsyncBackendTaskCompleted() - ERROR: Returned result success BUT had NULL List of Comments for offset [" + mOffset + "]!");
            mProgressBarSectionView.setVisibility(View.GONE);
            doShowOrHideMenuMore();
        }
        
        for(Comment comment : listOfComments) {
            mTrip.addComment(comment);
        }
        mProgressBarSectionView.setVisibility(View.GONE);
        displayList(listOfComments);
        return isSuccess;
    }
    
    /**
     * Populates and shows the list contents.
     */
    public void doShowContent() {
        mMenuContentShow.setVisibility(View.GONE);
        mMenuContentHide.setVisibility(View.VISIBLE);
        mContentContainer.setVisibility(View.VISIBLE);
        
        /*
         * Only when mOffset is ZERO, we need to make sure the list is populated after init().
         * But that only occurred if the list is handily available inside Trip object. If 
         * the list is still empty, we call server to retrieve the list.
         */
        if(mOffset == 0 && mNumOfTotalComments > 0) {
            if(mListRowsContainer == null) {
                mListRowsContainer = (ViewGroup) mFragmentRootView.findViewById(R.id.listRowsContainerID);
            }
            if(mListRowsContainer.getChildCount() == 0) {
                retrieveComments();
            }
        }
    }

    private void doHideContent() {
        mMenuContentShow.setVisibility(View.VISIBLE);
        mMenuContentHide.setVisibility(View.GONE);
        mContentContainer.setVisibility(View.GONE);
    }
    
    private void doShowOrHideMenuMore() {
        if((mOffset + 1) < mNumOfTotalComments) {
            mMenuMore.setVisibility(View.VISIBLE);
        } else {
            mMenuMore.setVisibility(View.GONE);
        }
    }
    
    
    
}
