package com.trekcrumb.android.fragment;

import java.util.List;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.asynctask.PictureImageRetrieveAsyncTask;
import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.business.CacheMemoryManager;
import com.trekcrumb.android.utility.SingletonFactory;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Picture Cover fragment
 * 1. Represents a cover image of a Trip
 * 2. Issues and Resolutions
 *    (1) Issue: Parent Width dimension
 *    We NEED the container (parent) view's width as part of initialization to do some math 
 *    calculations. However, any call of view.getWidth()/getMeaseuredWidth() would 
 *    return either 0 or -1 during Activity/Fragment creation when the XML layout does not specify 
 *    explicit value in dp/px, or its content is empty. The actual value would only be calculated 
 *    by the System once all the View objects got created and laid out. To resolve this, we would
 *    do the actual initialization by launching a runnable listener, AFTER a post().
 *    (REF: http://stackoverflow.com/questions/3591784/getwidth-and-getheight-of-view-returns-0)
 * 
 *    (3) Issue: Fragment(s) auto-created but getActivity() NULL
 *    After pause/stop (due to device flip. etc.), Android system would automatically recreate this 
 *    fragment with possibility the fragment not attached to an activity (why?) 
 *    so that getActivity() would returned NULL. This caused either IllegalStateException or NPE 
 *    in this fragment when trying to access resources. 
 *    
 *    (4) Issue: Fragment(s) sticky auto-created but dimensions ZEROs
 *    After we viewed a page with this fragment, then we swap tabs to view either PictureDetail 
 *    fragment or Map fragment, and then we flip the device: Android system found the previous
 *    instance of this fragment even though we were not viewing it (hence, sticky!) and tried to 
 *    initialize it. This sticky fragment instance was not on display, and has ZERO width dimension 
 *    that caused math exception ERROR when we tried to do math calculations.
 *    To resolve this, we quit initialization whenever fragment width is zero.
 *    
 *    (5) Issue: Image Bitmap to fill entire area width
 *    Unlike PictureDetail Fragment where each image must fill entire area's width and height (one
 *    page screen), we wanted PictureCover Fragmwent image to cover ONLY the area width BUT maintain
 *    its ratio proprotionally. Since the page is scrollable, the height is FLEXIBLE (wrap_content)
 *    and can be adjusted to whatever value it is. For the bitmap's ImageView, this is 
 *    as easy as setting 'layout_width=fill_parent'. By default, its setting 'scaleType=fitCenter' 
 *    which means any Bitmap will expand/shrink until either it fully fill X-axis (width) or Y-axis 
 *    (height). The issue is with the Bitmap! 
 *    When Bitmap's width pixel >= ImageView size, it would shrink and fill the ImageView
 *    width nicely. BUT when Bitmap's width < ImageView's width: it will NOT expand as
 *    we wished because its height has fully filled the Y-axis (height) which is 'wrap_content', then
 *    stopped expanding. This resulted in the image to FAIL to fill the entire area width. 
 *    We could not choose 'scaleType=fitXY' because while Bitmap would fill entire width/height, 
 *    it lost the image ratio. One thing we can is to set 'scaleType=centerCrop' where it would 
 *    force the image to expand until its wide fill the ImageView's width while maintaining ratio. 
 *    BUT it cropped out the height, so big portion of the Image disappeared.  
 *    To resolve this, we set: 'scaleType=centerCrop' and 'adjustViewBounds=true' AND MANUALLY
 *    calculate/adjust ImageView's height so that it is enough to contain the entire Bitmap's height
 *    and will not crop it out of view.
 *    
 *    (6) Issue: Fragment variables LOST during detach/attach ops
 *    See TripDetailFragment.java javadoc for details of this issue.
 *    
 * @author Val Triadi
 * @since 3/2017
 */
public class PictureCoverFragment 
extends BaseFragment {
    public static final String TAG_FRAGMENT_NAME = PictureCoverFragment.class.getName();
    private static final String TAG_LOG = "PictureCoverFragment";

    //Main properties
    private Trip mTrip;
    private List<Picture> mListOfPictures;
    private Picture mPictureSelected;
    private int mPictureSelectedIndex;
    private int mNumOfPictures;
    private int mFragmentTotalWidth;
    
    //(6) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
    Bundle mFragmentArgumentBundle;

    //Views
    private View mProgressBarSectionView;
    private ImageView mImgView;
    private TextView mImgErrorText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        mFragmentRootView = inflater.inflate(R.layout.fragment_picture_cover, parentViewGroup, false);
        return mFragmentRootView;
    }

    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        if(savedInstanceState == null) {
            //Freshly created from raw:
            mFragmentArgumentBundle = getArguments();
            if(mFragmentArgumentBundle != null) {
                mTrip = (Trip) mFragmentArgumentBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
                mPictureSelectedIndex = mFragmentArgumentBundle.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mTrip = (Trip)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
            mPictureSelectedIndex = savedInstanceState.getInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED);
            
            //(6) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
            mFragmentArgumentBundle = getArguments();
        }

        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, mTrip);
        outStateBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPictureSelectedIndex);
    }
    
    /**
     * Updates picture details and image given the new picture index.
     */
    public void updatePictureCover(int selectedIndex) {
        //(2) Issue: Fragment(s) auto-created but getActivity() NULL (See Javadoc)
        if(getActivity() == null) {
            return;
        }

        if(mPictureSelectedIndex == selectedIndex) {
            //No need to update:
            return;
        }
        
        mPictureSelectedIndex = selectedIndex;
        
        //(6) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
        if(mFragmentArgumentBundle != null) {
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPictureSelectedIndex);
        }
        
        mPictureSelected = mListOfPictures.get(mPictureSelectedIndex);
        populatePictureDetails();
        populatePictureImage();
    }

    public void refresh(Trip tripRefreshed) {
        mTrip = tripRefreshed;
        mPictureSelectedIndex = 0;
        
        //(6) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
        if(mFragmentArgumentBundle != null) {
            mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPictureSelectedIndex);
        }
        
        init();
    }
    
    private void init() {
        //(3) Issue: Fragment(s) auto-created but getActivity() NULL (See Javadoc)
        if(getActivity() == null) {
            return;
        }
        
        if(mTrip == null || mTrip.getNumOfPictures() == 0) {
            return;
        }
        
        //Else:
        mProgressBarSectionView = mFragmentRootView.findViewById(R.id.picCoverProgressBarLayoutID);
        TextView progressBarText = (TextView) mProgressBarSectionView.findViewById(R.id.progressBarLabelID);
        progressBarText.setText(R.string.info_picture_imageLoading);

        //(1) Issue: Parent Width dimension (see Javadoc)
        mFragmentRootView.post(new Runnable() {
            @Override
            public void run() {
                //(3) Issue: Fragment(s) auto-created but getActivity() NULL (See Javadoc)
                if(getActivity() == null) {
                    return;
                }
                
                //(4) Issue: Fragment(s) sticky auto-created but dimensions ZEROs (See Javadoc)
                mFragmentTotalWidth = mFragmentRootView.getWidth();
                if(mFragmentTotalWidth == 0) {
                    Log.e(TAG_LOG, "init() - ERROR: Required Fragment width is ZERO! Skipping all initialization!");
                    return;
                }
                
                initPropertiesAndLayout();
                populatePictureDetails();
                populatePictureImage();
            }
        });
    }
    
    private void initPropertiesAndLayout() {
        if(mFragmentTotalWidth > SingletonFactory.getPictureViewSize(getActivity())) {
            mFragmentTotalWidth = SingletonFactory.getPictureViewSize(getActivity());
        }

        mListOfPictures = mTrip.getListOfPictures();
        mNumOfPictures = mListOfPictures.size();
        if(mPictureSelectedIndex < 0 || mPictureSelectedIndex > (mNumOfPictures-1)) {
            //Avoid ArrayOutOfBound error
            mPictureSelectedIndex = 0;
        }
        
        /*
         * We only care to display PictureCover (if any) if mPictureSelectedIndex is ZERO
         * because we do not want to override any mPictureSelectedIndex value from bundle!
         */
        if(mPictureSelectedIndex == 0) {
            int loopIndex  = 0;
            for(Picture picture : mListOfPictures) {
                if(picture.getCoverPicture() == YesNoEnum.YES) {
                    mPictureSelectedIndex = loopIndex;
                    
                    //(6) Issue: Fragment variables LOST during detach/attach ops (See javadoc)
                    if(mFragmentArgumentBundle != null) {
                        mFragmentArgumentBundle.putInt(CommonConstants.KEY_SESSIONOBJ_INDEX_SELECTED, mPictureSelectedIndex);
                    }

                    mPictureSelected = picture;
                    break;
                }
                loopIndex++;
            }
        }
        
        if(mPictureSelected == null) {
            mPictureSelected = mListOfPictures.get(mPictureSelectedIndex);
        }

        mImgView = (ImageView) mFragmentRootView.findViewById(R.id.picCoverImageID);
        mImgErrorText = (TextView)mFragmentRootView.findViewById(R.id.picCoverImageErrorTextID);
    }
    
    private void populatePictureDetails() {
        TextView picNumberOfTotal = (TextView) mFragmentRootView.findViewById(R.id.picCoverIndexID);
        String numberOfTotalStr = String.format(
                        getResources().getString(R.string.label_common_numberOfTotal),  
                        (mPictureSelectedIndex + 1),
                        mNumOfPictures); 
        picNumberOfTotal.setText(numberOfTotalStr);
        
        TextView picNote = (TextView) mFragmentRootView.findViewById(R.id.picCoverNoteID);
        if(!ValidationUtil.isEmpty(mPictureSelected.getNote())) {
            picNote.setText(mPictureSelected.getNote());
        } else {
            picNote.setText(null);   
        }
    }
    
    private void populatePictureImage() {
        mProgressBarSectionView.setVisibility(View.VISIBLE);
        mImgView.setImageBitmap(null);
        mImgView.setVisibility(View.GONE);
        mImgErrorText.setText(null);
        mImgErrorText.setVisibility(View.GONE);
        
        //Current image:
        String cacheKey = mPictureSelected.getImageName();
        ImageCacheBean imgResultCurrent = CacheMemoryManager.getImageFromCache(getActivity(), cacheKey);
        if(imgResultCurrent == null) {
            //Not in cache, retrieve it:
            ImageCacheBean result = new ImageCacheBean();
            result.setKey(cacheKey);
            result.setServiceStatus(ServiceStatusEnum.STATUS_WAITING);
            CacheMemoryManager.addImageToCache(getActivity(), cacheKey, result);

            PictureImageRetrieveAsyncTask backendTask = 
                    new PictureImageRetrieveAsyncTask(this, mPictureSelected, mIsUserLoginTrip, false, -1);
            backendTask.execute();

        } else if(imgResultCurrent.getServiceStatus() == ServiceStatusEnum.STATUS_WAITING) {
            //Continue waiting ...
            
        } else {
            //Image ready:
            displayImageCurrent(imgResultCurrent);
        }
    }
    
    public void onPictureCoverReadyCallback(ImageCacheBean imgResult) {
        if(imgResult != null) {
            String cacheKey = imgResult.getKey();
            if(imgResult.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS) {
                CacheMemoryManager.addImageToCache(getActivity(), cacheKey, imgResult);
            } else {
                Log.e(TAG_LOG, "onPictureCoverReadyCallback() - ERROR: Returned result failed! " + imgResult);
                CacheMemoryManager.removeImageFromCache(getActivity(), cacheKey);
            }
            displayImageCurrent(imgResult);
        }
    }
        
    @SuppressWarnings("deprecation")
    private void displayImageCurrent(ImageCacheBean imageResult) {
        Bitmap imageBitmap = imageResult.getValue();
        mProgressBarSectionView.setVisibility(View.GONE);

        if(imageResult.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS) {
            //(5) Issue: Image Bitmap to fill entire area width
            mImgView.setVisibility(View.VISIBLE);
            mImgView.getLayoutParams().width = ViewGroup.LayoutParams.FILL_PARENT;
            
            /*
             * Calculates and adjusts ImageView height. We need the calculation in 'float' that is
             * to keep the decimals, then we will round it UP so that the new height can be larger
             * when necessary than what the Bitmap needs so it will not crop out any height pixels.
             */
            float heightFloat = ((float)mFragmentTotalWidth / imageBitmap.getWidth()) * imageBitmap.getHeight();
            mImgView.getLayoutParams().height = (int) Math.ceil(heightFloat);
            mImgView.setImageBitmap(imageBitmap);
            
            mImgErrorText.setVisibility(View.GONE);
            
        } else {
            mImgView.setVisibility(View.VISIBLE);
            mImgView.getLayoutParams().width = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImgView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
            mImgView.setImageBitmap(imageBitmap);

            mImgErrorText.setVisibility(View.VISIBLE);
            mImgErrorText.setText(imageResult.getErrorMsg());
        }
        /*
        Log.d(TAG_LOG, "displayImageCurrent() - AFTER Dimensions mImgCurrent width x height [" 
                + mImgView.getWidth() + " x " + mImgView.getHeight() 
                + "], bitmmap width x height [" + imageBitmap.getWidth() 
                + " x " + imageBitmap.getHeight() + "]");*/
    }
    

}

