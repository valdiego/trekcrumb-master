package com.trekcrumb.android.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trekcrumb.android.R;
import com.trekcrumb.android.business.TripManager;
import com.trekcrumb.android.layoutholder.UserDetailLayoutHolder;
import com.trekcrumb.android.utility.ActivityUtil;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;

/**
 * Fragment to represent User detail profile.
 *
 */
public class UserDetailFragment 
extends BaseFragment {
    private static final String LOGGER_TAG = "UserDetailFragment";

    private User mUserOther;
    private int mNumOfTripsUnpublished;
    private UserDetailLayoutHolder mLayoutHolder;
    private View mMenuUserTripsUnpub;
    
    /**
     * Callback when it's time for the fragment to draw its user interface for the first time. 
     * @return A View entity of the fragment to draw a UI.
     *         Null, if the if the fragment does not provide a UI.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parentViewGroup, Bundle savedInstanceState) {
        super.onCreateView(inflater, parentViewGroup, savedInstanceState);
        mFragmentRootView = inflater.inflate(R.layout.fragment_user_detail, parentViewGroup, false);
        return mFragmentRootView;
    }
    
    /**
     *  See BaseFragment.onActivityCreated()
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            //Freshly created from raw:
            Bundle dataBundle = getArguments();
            if(dataBundle != null) {
                mUserOther = (User)dataBundle.getSerializable(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
            }
        } else {
            //Re-created from paused/stopped; check for previous data bundle:
            mUserOther = (User)savedInstanceState.getSerializable(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
        }

        init();
    }
    
    /**
     *  See BaseFragment.onSaveInstanceState()
     */
    @Override
    public void onSaveInstanceState(Bundle outStateBundle) {
        super.onSaveInstanceState(outStateBundle);
        outStateBundle.putSerializable(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED, mUserOther);
    }
    
    public void refresh(
            boolean isUserLoginProfile,
            User userLogin, 
            User userOther) {
        mIsUserLoginProfile = isUserLoginProfile;
        mUserLogin = userLogin;
        mUserOther = userOther;
        init();
    }

    private void init() {
        //Warning: This logic is tricky, so careful when try to refactor it!
        if(mIsUserLoginProfile) {
            if(mUserLogin == null) {
                populateNoData();
                return;
            }
        } else {
            //Only when mIsUserLoginProfile is false:
            if(mUserOther == null) {
                populateNoData();
                return;
            }
        }
        
        //Else:
        initPropertiesAndLayout();
        setupClickables();
        populateDataValues();
    }
    
    private void populateNoData() {
        View bodyMainSection = mFragmentRootView.findViewById(R.id.bodyMainSectionID);
        bodyMainSection.setVisibility(View.GONE);
    }
    
    /*
     * Dynamically initialize and modify layout due to specific conditions.
     */
    private void initPropertiesAndLayout() {
        mLayoutHolder = new UserDetailLayoutHolder(mFragmentRootView);
        
        mMenuUserTripsUnpub = mFragmentRootView.findViewById(R.id.menuUserTripsUnpubID);
        if(!mIsUserLoginProfile) {
            mMenuUserTripsUnpub.setVisibility(View.GONE);
            mMenuUserTripsUnpub = null;
        }
    }
    
    /*
     * Functional menu or buttons (save, cancel, etc.).
     * 
     * NOTE: Unfortunately, implementation of callable onClick() method for each menu in Fragment
     * is not easily called without the exact same onClick() method being implemented in its
     * hosting activity.
     */
    private void setupClickables() {
        View menuUserTrips = mFragmentRootView.findViewById(R.id.menuUserTripsID);
        menuUserTrips.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View v) {
                    if(mIsUserLoginProfile) {
                        ActivityUtil.doTripListActivity(getActivity(), mUserLogin, 0,
                                CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED, false, null);
                    } else {
                        ActivityUtil.doTripListActivity(getActivity(), mUserOther, 0,
                                CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED, false, null);
                    }
                }
            }); 
        
        if(mMenuUserTripsUnpub != null) {
            mMenuUserTripsUnpub.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        ActivityUtil.doTripListActivity(getActivity(), mUserLogin, mNumOfTripsUnpublished,
                                CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED, false, null);
                    }
                }); 
        }
        
        View menuUserFavorites = mFragmentRootView.findViewById(R.id.menuUserFavesID);
        menuUserFavorites.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View v) {
                    if(mIsUserLoginProfile) {
                        ActivityUtil.doTripListActivity(getActivity(), mUserLogin, 0,
                                CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES, false, null);
                    } else {
                        ActivityUtil.doTripListActivity(getActivity(), mUserOther, 0,
                                CommonConstants.KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES, false, null);
                    }
                }
            }); 
        
        View menuUserComments = mFragmentRootView.findViewById(R.id.menuUserCommentsID);
        menuUserComments.setOnClickListener(
            new View.OnClickListener() {
                public void onClick(View v) {
                    if(mIsUserLoginProfile) {
                        ActivityUtil.doCommentListByUserActivity(getActivity(), mUserLogin, false, null);
                    } else {
                        ActivityUtil.doCommentListByUserActivity(getActivity(), mUserOther, false, null);
                    }
                }
            });
        
        //TODO: User Pictures
    }    
    
    private void populateDataValues() {
        View bodyMainSection = mFragmentRootView.findViewById(R.id.bodyMainSectionID);
        bodyMainSection.setVisibility(View.VISIBLE);
        
        if(mIsUserLoginProfile) {
            getUserLoginDataSummary();
            mLayoutHolder.populateLayoutWithValues(getActivity(), mUserLogin, true, mNumOfTripsUnpublished);
        
        } else {
            mLayoutHolder.populateLayoutWithValues(getActivity(), mUserOther, false, 0);
        }
    }
    
    /*
     * For userLogin, always look data saved LOCALLY and overwrite data from remote servers.
     * Exceptions: User's favorites and comments MUST BE from remote servers.
     */
    private void getUserLoginDataSummary() {
        ServiceResponse result = null;
        
        //Published trips:
        Trip tripToCount = new Trip();
        tripToCount.setUserID(mUserLogin.getUserId());
        tripToCount.setPublish(TripPublishEnum.PUBLISH);
        result = TripManager.count(getActivity(), tripToCount, true);
        if(result.isSuccess()) {
            mUserLogin.setNumOfTotalTrips(result.getNumOfRecords());
        } else {
            Log.e(LOGGER_TAG, ".getUserLoginDataSummary() - ERROR while locally counting UserLogin's Trip Published: " + result.getServiceError());
        }
        
        //Unpublished trips:
        tripToCount.setPublish(TripPublishEnum.NOT_PUBLISH);
        result = TripManager.count(getActivity(), tripToCount, true);
        if(result.isSuccess()) {
            mNumOfTripsUnpublished = result.getNumOfRecords();
        } else {
            Log.e(LOGGER_TAG, ".getUserLoginDataSummary() - ERROR while locally counting UserLogin's Trip Non-Published: " + result.getServiceError());
            mNumOfTripsUnpublished = 0;
        }

        //TODO: User Pictures (locally)
    }
    
}
