package com.trekcrumb.android.layoutholder;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A class to represent one row of user comment list layout and hold all the fields found from 
 * the View (via findViewById) that is recycleable (reuse) for the next row. 
 * 
 * This is an approach to avoid calling the findViewBById() for each rows in the list since it is 
 * expensive operation.
 *
 */
public class CommentListLayoutHolder {
    //Comment
    private TextView commentCreatedDateTextView;
    private TextView commentNoteTextView;
    
    //Trip
    private TextView tripNameTextView;
    
    //User
    private TextView userFullnameTextView;
    private TextView usernameTextView;
    private Button userProfileDefaultView;
    private ImageView userProfileImageView;
    
    public CommentListLayoutHolder(View view) {
        if(view == null) {
            throw new IllegalArgumentException("Invalid View layout: NULL!");
        }
        
        tripNameTextView = (TextView) view.findViewById(R.id.tripNameID);
        
        commentCreatedDateTextView = (TextView) view.findViewById(R.id.createdDateID);
        commentNoteTextView = (TextView) view.findViewById(R.id.commentNoteID);
        
        userFullnameTextView = (TextView) view.findViewById(R.id.fullnameID);
        usernameTextView = (TextView) view.findViewById(R.id.usernameID);
        userProfileDefaultView = (Button) view.findViewById(R.id.profileImageDefaultID);
        userProfileImageView = (ImageView) view.findViewById(R.id.profileImageID);
    }
    
    /**
     * Since the row template is reused/recycleable, it is important to reset all the values to their
     * actual or default values to avoid the previous row's values get carried over.
     * 
     * When setting font typeface from BOLD back to NORMAL (remove previous styling), becareful
     * the previous row TextView's typeface may turn up FAKE BOLD (or italic) if the previous
     * typeface does not have all the style we provided.
     * (REF: http://stackoverflow.com/questions/6200533/set-textview-style-bold-or-italic)
     * 
     * Additionally, we are using icon-font technology and "Button" views for the icons which
     * require special attention for its focusable/clickable params. See the XML layout 'list_row_trip'
     * for extensive notes.
     */
    public void populateLayoutWithValues(
            final Context context,
            final Comment comment,
            boolean isUserLoginProfile) {
        if(comment == null) {
            return;
        }
        
        tripNameTextView.setText(comment.getTripName());
        commentCreatedDateTextView.setText(
                DateUtil.formatDate(comment.getCreated(), 
                        DateFormatEnum.GMT_STANDARD_LONG, 
                        DateFormatEnum.LOCALE_UI_WITH_TIME));
        commentNoteTextView.setText(comment.getNote());

        //User data:
        userFullnameTextView.setText(comment.getUserFullname());
        usernameTextView.setText(comment.getUsername().toLowerCase());

        retrieveProfileImage(context, comment);
    }
    
    private void retrieveProfileImage(Context context, Comment comment) {
        String profileImageName = comment.getUserImageName();
        if(ValidationUtil.isEmpty(profileImageName)) {
            /*
             * Since imageView (layoutholder) is recycled (reused): if no user profile image, 
             * we must replace whatever it has before with the default ('na image'). 
             * O/w, it would display image that belongs to previous row.
             */
            userProfileDefaultView.setVisibility(View.VISIBLE);
            userProfileImageView.setImageDrawable(null);
            userProfileImageView.setVisibility(View.GONE);
            return;
        }
        
        //Else: Retrieve profile image on separate thread
        ImageUtil.retrieveUserProfileImage(
                context, profileImageName, userProfileDefaultView, userProfileImageView, 
                false, true);
    }
    
}    
