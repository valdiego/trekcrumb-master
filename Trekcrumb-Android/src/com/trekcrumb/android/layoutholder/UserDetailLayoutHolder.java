package com.trekcrumb.android.layoutholder;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

public class UserDetailLayoutHolder {
    private View mRootView;
    
    private TextView fullnameView;
    private TextView usernameView;
    private TextView summaryView;
    private TextView locationView;
    private TextView createdDateView;
    private ImageView profileImageView;
    private Button profileImageDefaultView;
    
    private TextView tripNumberView;
    private TextView tripUnpubNumberView;
    private TextView picNumberView;
    private TextView faveNumberView;
    private TextView commentNumberView;
    
    public UserDetailLayoutHolder(final View rootView) {
        if(rootView == null) {
            throw new IllegalArgumentException("Invalid View layout: NULL!");
        }
        mRootView = rootView;
        
        fullnameView = (TextView) mRootView.findViewById(R.id.fullnameID);
        usernameView = (TextView) mRootView.findViewById(R.id.usernameID);
        createdDateView = (TextView) mRootView.findViewById(R.id.memberSinceID);
        locationView = (TextView) mRootView.findViewById(R.id.locationID);
        summaryView = (TextView) mRootView.findViewById(R.id.summaryID);
        profileImageView = (ImageView) mRootView.findViewById(R.id.profileImageID);
        profileImageDefaultView = (Button) mRootView.findViewById(R.id.profileImageDefaultID);
        
        tripNumberView = (TextView) mRootView.findViewById(R.id.tripNumberID);
        tripUnpubNumberView = (TextView) mRootView.findViewById(R.id.tripUnpubNumberID);
        picNumberView = (TextView) mRootView.findViewById(R.id.picNumberID);
        faveNumberView = (TextView) mRootView.findViewById(R.id.faveNumberID);
        commentNumberView = (TextView) mRootView.findViewById(R.id.commentNumberID);
    }
    
    public void populateLayoutWithValues(
            final Context context,
            final User user,
            boolean isUserLoginProfile,
            int numOfTripsUnpublished) {
        //Details:
        fullnameView.setText(user.getFullname());
        if(user.getUsername() != null) {
            usernameView.setText(user.getUsername().toLowerCase());
        } else {
            usernameView.setText("");
        }
        locationView.setText(user.getLocation());
        summaryView.setText(user.getSummary());
        String createdDate = 
                DateUtil.formatDate(user.getCreated(), 
                                    DateFormatEnum.GMT_STANDARD_LONG, 
                                    DateFormatEnum.LOCALE_UI_WITHOUT_TIME);
        createdDateView.setText("Joined " + createdDate);
        
        //Summary row:
        tripNumberView.setText(String.valueOf(user.getNumOfTotalTrips()));
        //TODO 8/2017: picNumberView.setText(String.valueOf(user.getNumOfTotalPictures()));
        faveNumberView.setText(String.valueOf(user.getNumOfFavorites()));
        commentNumberView.setText(String.valueOf(user.getNumOfComments()));
        if(isUserLoginProfile) {
            tripUnpubNumberView.setText(String.valueOf(numOfTripsUnpublished));
        }
        
        //Profile image:
        if(!ValidationUtil.isEmpty(user.getProfileImageName())) {
            ImageUtil.retrieveUserProfileImage(
                    context, user.getProfileImageName(), profileImageDefaultView, profileImageView,
                    isUserLoginProfile, false);
        }
    }    

}    
