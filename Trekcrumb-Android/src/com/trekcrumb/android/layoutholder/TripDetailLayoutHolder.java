package com.trekcrumb.android.layoutholder;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.DateUtil;

/**
 * A class to represent Trip Detail layout and hold all the fields found from the View (via findViewById).
 * 
 */
public class TripDetailLayoutHolder {
    private View mRootView;
    
    private TextView fullnameTextView;
    private TextView usernameTextView;
    private Button userProfileDefaultView;
    private ImageView userProfileImageView;
    
    private TextView tripNameView;
    private TextView locationView;
    private TextView tripNoteView;
    private TextView statusView;
    private TextView privacyView;
    private TextView publishView;
    private TextView createdDateView;
    
    private Button placeSummaryIconPassive;
    private Button placeSummaryIconActive;
    private TextView placeSummaryTxt;
    private Button picSummaryIconPassive;
    private Button picSummaryIconActive;
    private TextView picSummaryTxt;
    private Button faveSummaryIconPassive;
    private Button faveSummaryIconActive;
    private TextView faveSummaryTxt;
    private Button commentSummaryIconPassive;
    private Button commentSummaryIconActive;
    private TextView commentSummaryTxt;
    
    public TripDetailLayoutHolder(final View rootView) {
        if(rootView == null) {
            throw new IllegalArgumentException("Invalid View layout: NULL!");
        }
        mRootView = rootView;
        
        fullnameTextView = (TextView) mRootView.findViewById(R.id.fullnameID);
        usernameTextView = (TextView) mRootView.findViewById(R.id.usernameID);
        userProfileDefaultView = (Button) mRootView.findViewById(R.id.profileImageDefaultID);
        userProfileImageView =(ImageView) mRootView.findViewById(R.id.profileImageID);
        
        tripNameView = (TextView) mRootView.findViewById(R.id.tripNameID);
        locationView = (TextView) mRootView.findViewById(R.id.locationID);
        tripNoteView = (TextView) mRootView.findViewById(R.id.tripNoteID);
        statusView = (TextView) mRootView.findViewById(R.id.statusID);
        privacyView = (TextView) mRootView.findViewById(R.id.privacyID);
        publishView = (TextView) mRootView.findViewById(R.id.publishID);
        createdDateView = (TextView) mRootView.findViewById(R.id.createdDateID);
        
        placeSummaryIconPassive = (Button) mRootView.findViewById(R.id.placeIconPassiveID);
        placeSummaryIconActive = (Button) mRootView.findViewById(R.id.placeIconActiveID);
        placeSummaryTxt = (TextView) mRootView.findViewById(R.id.placeNumberID);
        picSummaryIconPassive = (Button) mRootView.findViewById(R.id.picIconPassiveID);
        picSummaryIconActive = (Button) mRootView.findViewById(R.id.picIconActiveID);
        picSummaryTxt = (TextView) mRootView.findViewById(R.id.picNumberID);
        faveSummaryIconPassive = (Button) mRootView.findViewById(R.id.faveIconPassiveID);
        faveSummaryIconActive = (Button) mRootView.findViewById(R.id.faveIconActiveID);
        faveSummaryTxt = (TextView) mRootView.findViewById(R.id.faveNumberID);
        commentSummaryIconPassive = (Button) mRootView.findViewById(R.id.commentIconPassiveID);
        commentSummaryIconActive = (Button) mRootView.findViewById(R.id.commentIconActiveID);
        commentSummaryTxt = (TextView) mRootView.findViewById(R.id.commentNumberID);
    }
    
    /**
     * Populates the layout with current/updated values.
     * WARNING: DO NOT include any init step in this operation because any init step should only
     *          be done ONCE, and not repeated everytime we update the values. This is true for 
     *          layout with rows or pagination.
     */
    public void populateLayoutWithValues(
            final Context context,
            final Trip trip,
            boolean isUserLoginTrip) {
        if(trip == null) {
            return;
        }

        //Trip details:
        tripNameView.setText(trip.getName());
        locationView.setText(trip.getLocation());
        tripNoteView.setText(trip.getNote());
        String createdDate = DateUtil.formatDate(trip.getCreated(), 
                                                 DateFormatEnum.GMT_STANDARD_LONG, 
                                                 DateFormatEnum.LOCALE_UI_WITH_TIME);
        createdDateView.setText(createdDate);

        //Trip Statuses section:
        if(trip.getStatus() == TripStatusEnum.ACTIVE) {
            statusView.setVisibility(View.VISIBLE);
        } else {
            statusView.setVisibility(View.GONE);
        }
        
        if(isUserLoginTrip) {
            if(trip.getPrivacy() == TripPrivacyEnum.PRIVATE) {
                privacyView.setVisibility(View.VISIBLE);
            } else {
                privacyView.setVisibility(View.GONE);
            }
            
            if(trip.getPublish() == TripPublishEnum.NOT_PUBLISH) {
                publishView.setVisibility(View.VISIBLE);
            } else {
                publishView.setVisibility(View.GONE);
            }

        } else {
            privacyView.setVisibility(View.GONE);
            publishView.setVisibility(View.GONE);
        }
        
        //Trip Summary section:
        if(trip.getNumOfPlaces() > 0) {
            placeSummaryIconPassive.setVisibility(View.GONE);
            placeSummaryIconActive.setVisibility(View.VISIBLE);
            placeSummaryTxt.setText(String.valueOf(trip.getNumOfPlaces()));
            placeSummaryTxt.setTypeface(placeSummaryTxt.getTypeface(), Typeface.BOLD);
        } else {
            placeSummaryIconPassive.setVisibility(View.VISIBLE);
            placeSummaryIconActive.setVisibility(View.GONE);
            placeSummaryTxt.setText("0");
            placeSummaryTxt.setTypeface(placeSummaryTxt.getTypeface(), Typeface.NORMAL);
        }
        
        if(trip.getNumOfPictures() > 0) {
            picSummaryIconPassive.setVisibility(View.GONE);
            picSummaryIconActive.setVisibility(View.VISIBLE);
            picSummaryTxt.setText(String.valueOf(trip.getNumOfPictures()));
            picSummaryTxt.setTypeface(picSummaryTxt.getTypeface(), Typeface.BOLD);
        } else {
            picSummaryIconPassive.setVisibility(View.VISIBLE);
            picSummaryIconActive.setVisibility(View.GONE);
            picSummaryTxt.setText("0");            
            picSummaryTxt.setTypeface(picSummaryTxt.getTypeface(), Typeface.NORMAL);
        }
        
        if(trip.getNumOfFavorites() > 0) {
            faveSummaryIconPassive.setVisibility(View.GONE);
            faveSummaryIconActive.setVisibility(View.VISIBLE);
            faveSummaryTxt.setText(String.valueOf(trip.getNumOfFavorites()));
            faveSummaryTxt.setTypeface(faveSummaryTxt.getTypeface(), Typeface.BOLD);
        } else {
            faveSummaryIconPassive.setVisibility(View.VISIBLE);
            faveSummaryIconActive.setVisibility(View.GONE);
            faveSummaryTxt.setText("0");            
            faveSummaryTxt.setTypeface(faveSummaryTxt.getTypeface(), Typeface.NORMAL);
        }
        
        if(trip.getNumOfComments() > 0) {
            commentSummaryIconPassive.setVisibility(View.GONE);
            commentSummaryIconActive.setVisibility(View.VISIBLE);
            commentSummaryTxt.setText(String.valueOf(trip.getNumOfComments()));
            commentSummaryTxt.setTypeface(commentSummaryTxt.getTypeface(), Typeface.BOLD);
        } else {
            commentSummaryIconPassive.setVisibility(View.VISIBLE);
            commentSummaryIconActive.setVisibility(View.GONE);
            commentSummaryTxt.setText("0");            
            commentSummaryTxt.setTypeface(commentSummaryTxt.getTypeface(), Typeface.NORMAL);
        }        
        
        //User details:
        fullnameTextView.setText(trip.getUserFullname());
        if(trip.getUsername() != null) {
            usernameTextView.setText(trip.getUsername().toLowerCase());
        } else {
            usernameTextView.setText("");
        }
        ImageUtil.retrieveUserProfileImage(
                context, trip.getUserImageName(), userProfileDefaultView, userProfileImageView, 
                isUserLoginTrip, true);
    }
    
}    
