package com.trekcrumb.android.layoutholder;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A class to represent one row of user trip list layout and hold all the fields found from 
 * the View (via findViewById) that is recycleable (reuse) for the next row. 
 * 
 * This is an approach to avoid calling the findViewBById() for each rows in the list since it is 
 * expensive operation.
 *
 */
public class TripListLayoutHolder {
    private TextView tripNameTextView;
    private TextView tripCreatedDateTextView;
    private TextView tripStatusTextView;
    private TextView privacyTextView;
    private TextView publishTextView;
    private TextView tripLocationTextView;
    
    private View summarySectionLayout;
    private Button placeSummaryIconPassive;
    private Button placeSummaryIconActive;
    private TextView placeSummaryTxt;
    private Button picSummaryIconPassive;
    private Button picSummaryIconActive;
    private TextView picSummaryTxt;
    private Button faveSummaryIconPassive;
    private Button faveSummaryIconActive;
    private TextView faveSummaryTxt;
    private Button commentSummaryIconPassive;
    private Button commentSummaryIconActive;
    private TextView commentSummaryTxt;
    
    private TextView userFullnameTextView;
    private TextView usernameTextView;
    private Button userProfileDefaultView;
    private ImageView userProfileImageView;
    
    public TripListLayoutHolder(View view) {
        if(view == null) {
            throw new IllegalArgumentException("Invalid View layout: NULL!");
        }

        tripNameTextView = (TextView) view.findViewById(R.id.tripNameID);
        tripCreatedDateTextView = (TextView) view.findViewById(R.id.createdDateID);
        tripLocationTextView = (TextView) view.findViewById(R.id.locationID);
        tripStatusTextView = (TextView) view.findViewById(R.id.statusID);
        privacyTextView = (TextView) view.findViewById(R.id.privacyID);
        publishTextView = (TextView) view.findViewById(R.id.publishID);
        
        summarySectionLayout = view.findViewById(R.id.summarySectionID);
        placeSummaryIconPassive = (Button) view.findViewById(R.id.placeIconPassiveID);
        placeSummaryIconActive = (Button) view.findViewById(R.id.placeIconActiveID);
        placeSummaryTxt = (TextView) view.findViewById(R.id.placeNumberID);
        picSummaryIconPassive = (Button) view.findViewById(R.id.picIconPassiveID);
        picSummaryIconActive = (Button) view.findViewById(R.id.picIconActiveID);
        picSummaryTxt = (TextView) view.findViewById(R.id.picNumberID);
        faveSummaryIconPassive = (Button) view.findViewById(R.id.faveIconPassiveID);
        faveSummaryIconActive = (Button) view.findViewById(R.id.faveIconActiveID);
        faveSummaryTxt = (TextView) view.findViewById(R.id.faveNumberID);
        commentSummaryIconPassive = (Button) view.findViewById(R.id.commentIconPassiveID);
        commentSummaryIconActive = (Button) view.findViewById(R.id.commentIconActiveID);
        commentSummaryTxt = (TextView) view.findViewById(R.id.commentNumberID);
        
        userFullnameTextView = (TextView) view.findViewById(R.id.fullnameID);
        usernameTextView = (TextView) view.findViewById(R.id.usernameID);
        userProfileDefaultView = (Button) view.findViewById(R.id.profileImageDefaultID);
        userProfileImageView = (ImageView) view.findViewById(R.id.profileImageID);
    }
    
    /**
     * Since the row template is reused/recycleable, it is important to reset all the values to their
     * actual or default values to avoid the previous row's values get carried over.
     * 
     * When setting font typeface from BOLD back to NORMAL (remove previous styling), becareful
     * the previous row TextView's typeface may turn up FAKE BOLD (or italic) if the previous
     * typeface does not have all the style we provided.
     * (REF: http://stackoverflow.com/questions/6200533/set-textview-style-bold-or-italic)
     * 
     * Additionally, we are using icon-font technology and "Button" views for the icons which
     * require special attention for its focusable/clickable params. See the XML layout 'list_row_trip'
     * for extensive notes.
     */
    public void populateLayoutWithValues(
            final Context context,
            final Trip trip,
            boolean isShowTripSummary, 
            boolean isShowTripStatus) {
        if(trip == null) {
            return;
        }
        //Trip data:
        tripNameTextView.setText(trip.getName());
        tripLocationTextView.setText(trip.getLocation());
        String createdDate = DateUtil.formatDate(trip.getCreated(), 
                DateFormatEnum.GMT_STANDARD_LONG, 
                DateFormatEnum.LOCALE_UI_WITHOUT_TIME);
        tripCreatedDateTextView.setText(createdDate);

        //Trip Statuses section:
        if(trip.getStatus() == TripStatusEnum.ACTIVE) {
            tripStatusTextView.setVisibility(View.VISIBLE);
        } else {
            tripStatusTextView.setVisibility(View.GONE);
        }
        
        if(isShowTripStatus) {
            if(trip.getPrivacy() == TripPrivacyEnum.PRIVATE) {
                privacyTextView.setVisibility(View.VISIBLE);
            } else {
                privacyTextView.setVisibility(View.GONE);
            }
            
            if(trip.getPublish() == TripPublishEnum.NOT_PUBLISH) {
                publishTextView.setVisibility(View.VISIBLE);
            } else {
                publishTextView.setVisibility(View.GONE);
            }

        } else {
            privacyTextView.setVisibility(View.GONE);
            publishTextView.setVisibility(View.GONE);
        }
        
        //Trip Summary section:
        if(isShowTripSummary) {
            if(trip.getNumOfPlaces() > 0) {
                placeSummaryIconPassive.setVisibility(View.GONE);
                placeSummaryIconActive.setVisibility(View.VISIBLE);
                placeSummaryTxt.setText(String.valueOf(trip.getNumOfPlaces()));
                placeSummaryTxt.setTypeface(placeSummaryTxt.getTypeface(), Typeface.BOLD);
            } else {
                placeSummaryIconPassive.setVisibility(View.VISIBLE);
                placeSummaryIconActive.setVisibility(View.GONE);
                placeSummaryTxt.setText("0");
                /*
                 * Get rid of 'BOLD' font
                 * We cannot do this: placeSummaryTxt.setTypeface(placeSummaryTxt.getTypeface(), Typeface.NORMAL)
                 * But must re-create the Typeface from scratch (See Javadoc note above)
                 */
                placeSummaryTxt.setTypeface(Typeface.create(placeSummaryTxt.getTypeface(), Typeface.NORMAL));
            }
            
            if(trip.getNumOfPictures() > 0) {
                picSummaryIconPassive.setVisibility(View.GONE);
                picSummaryIconActive.setVisibility(View.VISIBLE);
                picSummaryTxt.setText(String.valueOf(trip.getNumOfPictures()));
                picSummaryTxt.setTypeface(picSummaryTxt.getTypeface(), Typeface.BOLD);
            } else {
                picSummaryIconPassive.setVisibility(View.VISIBLE);
                picSummaryIconActive.setVisibility(View.GONE);
                picSummaryTxt.setText("0");            
                picSummaryTxt.setTypeface(Typeface.create(picSummaryTxt.getTypeface(), Typeface.NORMAL));
            }
            
            if(trip.getNumOfFavorites() > 0) {
                faveSummaryIconPassive.setVisibility(View.GONE);
                faveSummaryIconActive.setVisibility(View.VISIBLE);
                faveSummaryTxt.setText(String.valueOf(trip.getNumOfFavorites()));
                faveSummaryTxt.setTypeface(faveSummaryTxt.getTypeface(), Typeface.BOLD);
            } else {
                faveSummaryIconPassive.setVisibility(View.VISIBLE);
                faveSummaryIconActive.setVisibility(View.GONE);
                faveSummaryTxt.setText("0");            
                faveSummaryTxt.setTypeface(Typeface.create(faveSummaryTxt.getTypeface(), Typeface.NORMAL));
            }
            
            if(trip.getNumOfComments() > 0) {
                commentSummaryIconPassive.setVisibility(View.GONE);
                commentSummaryIconActive.setVisibility(View.VISIBLE);
                commentSummaryTxt.setText(String.valueOf(trip.getNumOfComments()));
                commentSummaryTxt.setTypeface(commentSummaryTxt.getTypeface(), Typeface.BOLD);
            } else {
                commentSummaryIconPassive.setVisibility(View.VISIBLE);
                commentSummaryIconActive.setVisibility(View.GONE);
                commentSummaryTxt.setText("0");            
                commentSummaryTxt.setTypeface(Typeface.create(commentSummaryTxt.getTypeface(), Typeface.NORMAL));
            }
            
        } else {
            if(summarySectionLayout != null) {
                summarySectionLayout.setVisibility(View.GONE);
            }
        }
        
        //User data:
        userFullnameTextView.setText(trip.getUserFullname());
        if(trip.getUsername() != null) {
            usernameTextView.setText(trip.getUsername().toLowerCase());
        } else {
            usernameTextView.setText("");
        }
        retrieveProfileImage(context, trip);
    }
    
    private void retrieveProfileImage(Context context, Trip trip) {
        String profileImageName = trip.getUserImageName();
        if(ValidationUtil.isEmpty(profileImageName)) {
            /*
             * Since imageView (layoutholder) is recycled (reused): if no user profile image, 
             * we must replace whatever it has before with the default ('na image'). 
             * O/w, it would display image that belongs to previous row.
             */
            userProfileDefaultView.setVisibility(View.VISIBLE);
            userProfileImageView.setImageDrawable(null);
            userProfileImageView.setVisibility(View.GONE);
            return;
        }
        
        //Else: Retrieve profile image on separate thread
        ImageUtil.retrieveUserProfileImage(
                context, profileImageName, userProfileDefaultView, userProfileImageView, 
                false, true);
    }
    
    

}    
