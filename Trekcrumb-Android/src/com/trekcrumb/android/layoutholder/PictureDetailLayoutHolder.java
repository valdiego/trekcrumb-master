package com.trekcrumb.android.layoutholder;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A class to represent picture detail layout and hold all the fields found from the View (via findViewById).
 * 
 * This is an approach to avoid calling the findViewById() for each time a layout being re-populate
 * with new data. The findViewById() operation is expensive operation.
 *
 */
public class PictureDetailLayoutHolder {
    private TextView fullnameTextView;
    private TextView usernameTextView;
    private Button userProfileDefaultView;
    private ImageView userProfileImageView;
    
    private TextView tripNameView;
    private TextView picNoteView;
    private TextView picNumberOfTotalView;
    
    /**
     * Initializes the picture layout.
     */
    public PictureDetailLayoutHolder(final View view) {
        if(view == null) {
            throw new IllegalArgumentException("Invalid View layout: NULL!");
        }
        
        fullnameTextView = (TextView) view.findViewById(R.id.fullnameID);
        usernameTextView = (TextView) view.findViewById(R.id.usernameID);
        userProfileDefaultView = (Button) view.findViewById(R.id.profileImageDefaultID);
        userProfileImageView =(ImageView) view.findViewById(R.id.profileImageID);

        tripNameView = (TextView) view.findViewById(R.id.tripNameID);
        picNoteView = (TextView) view.findViewById(R.id.picNoteID);
        picNumberOfTotalView = (TextView) view.findViewById(R.id.numberOfTotalID);
    }

    /**
     * Populates the layout with current/updated values.
     * WARNING: DO NOT include any init step in this operation because any init step should only
     *          be done ONCE, and not repeated everytime we update the values. This is true for 
     *          layout with rows or pagination.
     *          
     * @param context
     * @param trip - The trip where the current picture belongs to
     * @param picture - The picture in question
     * @param pictureIndex - The index number of the picture in the List
     * @param numOfPicturesLabel
     * @param isUserLoginTrip
     */
    public void populateLayoutWithValues(
            final Context context,
            final Trip trip,
            final Picture picture,
            int pictureIndex,
            String numOfPicturesLabel,
            boolean isUserLoginTrip) {
        if(trip == null) {
            return;
        }
        
        tripNameView.setText(trip.getName());
        picNoteView.setText(picture.getNote());

        int numOfPictures = trip.getNumOfPictures();
        String numberOfTotalStr = 
                String.format(numOfPicturesLabel,  
                              pictureIndex + 1,
                              numOfPictures); 
        picNumberOfTotalView.setText(numberOfTotalStr);
        
        //User details:
        fullnameTextView.setText(trip.getUserFullname());
        fullnameTextView.setTextColor(context.getResources().getColor(R.color.color_white));
        if(trip.getUsername() != null) {
            usernameTextView.setText(trip.getUsername().toLowerCase());
            usernameTextView.setTextColor(context.getResources().getColor(R.color.color_white));
        } else {
            usernameTextView.setText("");
        }
        
        //Profile image:
        if(!ValidationUtil.isEmpty(trip.getUserImageName())) {
            ImageUtil.retrieveUserProfileImage(
                    context, trip.getUserImageName(), userProfileDefaultView, userProfileImageView,
                    isUserLoginTrip, true);
        } else {
            userProfileDefaultView.setVisibility(View.VISIBLE);
            userProfileDefaultView.setTextColor(context.getResources().getColor(R.color.color_white));
            userProfileImageView.setVisibility(View.GONE);
        }
    }
    
    
}    
