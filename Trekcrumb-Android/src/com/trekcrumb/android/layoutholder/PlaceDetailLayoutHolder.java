package com.trekcrumb.android.layoutholder;

import java.io.Serializable;

import android.view.View;
import android.widget.TextView;

import com.trekcrumb.android.R;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.utility.DateUtil;

/**
 * A class to represent place detail layout and hold all the fields found from the View (via findViewById).
 * 
 * This is an approach to avoid calling the findViewById() for each time a layout being re-populate
 * with new data. The findViewById() operation is expensive operation.
 *
 */
@SuppressWarnings("serial")
public class PlaceDetailLayoutHolder implements Serializable  {
    private TextView placeLocationView;
    private TextView placeNoteView;
    private TextView createdDateView;
    private TextView numberOfTotalView;
    
    /**
     * Initializes the place detail layout.
     */
    public PlaceDetailLayoutHolder(final View view) {
        if(view == null) {
            throw new IllegalArgumentException("Invalid View layout: NULL!");
        }
        placeLocationView = (TextView) view.findViewById(R.id.placeLocationID);
        placeNoteView = (TextView) view.findViewById(R.id.placeNoteID);
        createdDateView = (TextView) view.findViewById(R.id.createdDateID);
        numberOfTotalView = (TextView) view.findViewById(R.id.numberOfTotalID);
    }

    /**
     * Populates the layout with current/updated values.
     * WARNING: DO NOT include any init step in this operation because any init step should only
     *          be done ONCE, and not repeated everytime we update the values. This is true for 
     *          layout with rows or pagination.
     *          
     * @param context
     * @param trip - The trip where the current picture belongs to
     * @param place - The place in question
     * @param placeIndex - The index number of the place in the List
     * @param numOfPlacesLabel
     */    
    public void populateLayoutWithValues(
            Trip trip,
            Place place,
            int placeIndex,
            String numOfPlacesLabel) {
        placeLocationView.setText(place.getLocation());
        placeNoteView.setText(place.getNote());
        
        int numOfPlaces = trip.getNumOfPlaces();
        String numberOfTotalStr = String.format(numOfPlacesLabel, placeIndex + 1, numOfPlaces); 
        numberOfTotalView.setText(numberOfTotalStr);
        
        String createdDate = DateUtil.formatDate(place.getCreated(), 
                DateFormatEnum.GMT_STANDARD_LONG, 
                DateFormatEnum.LOCALE_UI_WITH_TIME);
        createdDateView.setText(createdDate);
    }
    
}    
