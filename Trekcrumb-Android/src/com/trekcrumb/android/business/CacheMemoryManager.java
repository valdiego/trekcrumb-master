package com.trekcrumb.android.business;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.util.Log;
import android.util.LruCache;

import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.bean.ListCacheBean;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.SingletonFactory;

/**
 * A manager and wrapper class to access LruCache. It handles memory caching of objects (such as 
 * images, List, etc.) including create, put into, and get from the cache.
 * 
 * @see android.util.LruCache
 * 
 * @author Val Triadi
 */
public class CacheMemoryManager {
    private static final String TAG_LOGGER = "CacheMemoryManager";

    private static LruCache<String, ImageCacheBean> mMemoryCacheImages;
    private static LruCache<String, ListCacheBean> mMemoryCacheLists;

    public static void cleanAll() {
        mMemoryCacheImages = null;
        mMemoryCacheLists = null;
    }
    
    public static void initCacheForImages(final Context context) {
        if(mMemoryCacheImages == null) {
            Log.d(TAG_LOGGER, "initCacheForImages() - Starting ..."
                    + " Device memory (heap) maximum [" + AndroidConstants.DEVICE_HEAP_MEMORY_MAX + " KB]"
                    + ", device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                    );
            
            //There can only be one memory cache exists at a time
            if(mMemoryCacheLists != null) {
                mMemoryCacheLists.evictAll();
                mMemoryCacheLists = null;
            }

            int cacheSize = calculateCacheSizeForImages(context, AndroidConstants.PICTURE_IMAGE_PIXEL_CONFIG);
            mMemoryCacheImages = new LruCache<String, ImageCacheBean>(cacheSize) {
                @Override
                protected int sizeOf(String key, ImageCacheBean value) {
                    // The cache size will be measured in kilobytes rather than number of items.
                    if(value != null && value.getValue() != null) {
                        return value.getValue().getByteCount() / 1024;
                    } else {
                        return 0;
                    }
                }
            };
            
            Log.d(TAG_LOGGER, "initCacheForImages() - Completed."
                    + "Device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                    + ", image memory cache reserved size [" + mMemoryCacheImages.maxSize() + " KB]"
                    + ", image cache used size [" + mMemoryCacheImages.size() + " KB]"
                    );
        }
    }
    
    public static void initCacheForList() {
        if(mMemoryCacheLists == null) {
            Log.d(TAG_LOGGER, "initCacheForList() - Starting ..."
                    + " Device memory (heap) maximum [" + AndroidConstants.DEVICE_HEAP_MEMORY_MAX + " KB]"
                    + ", device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                    );
            
            //There can only be one memory cache exists at a time
            if(mMemoryCacheImages != null) {
                mMemoryCacheImages.evictAll();
                mMemoryCacheImages = null;
            }

            int cacheSize = 10; //number of maximum items
            mMemoryCacheLists = new LruCache<String, ListCacheBean>(cacheSize);
            Log.d(TAG_LOGGER, "mMemoryCacheLists() - Completed."
                    + "Device memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                    + ", Cache reserved size (number): " + mMemoryCacheLists.maxSize()
                    + ", Cache used size (number): " + mMemoryCacheLists.size()
                    );
        }
    }

    /**
     * Adds a bitmap to memory cache.
     * @param key - Unique identifier for the object to store
     * @param value - The image object to store
     */
    public static void addImageToCache(final Context context, String key, ImageCacheBean value) {
        if (key == null || value == null) {
            Log.e(TAG_LOGGER, "addImageToCache() - Invalid input: either 'key' or 'value' cannot be NULL!");
            return;
        }
 
        initCacheForImages(context);
        if(mMemoryCacheImages.get(key) != null) {
            //Remove to avoid unwanted duplicate: 
            synchronized (mMemoryCacheImages) {
                mMemoryCacheImages.remove(key);
                mMemoryCacheImages.put(key, value);
            }
        } else {
            synchronized (mMemoryCacheImages) {
                mMemoryCacheImages.put(key, value);
            }
        }

        Log.d(TAG_LOGGER, "addImageToCache() - Completed for key [" + key + "] and value [" + value 
                + "]. Memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                + ", image cache reserved size [" + mMemoryCacheImages.maxSize() + " KB]"
                + ", image cache used size [" + mMemoryCacheImages.size() + " KB]"
                );
    }
 
    /**
     * Gets image from memory cache given its key.
     *
     * @param key Unique identifier for which item to get
     * @return The ImageRetrieveResult if found in cache, null otherwise
     */
    public static ImageCacheBean getImageFromCache(final Context context, String key) {
        initCacheForImages(context);
        return mMemoryCacheImages.get(key);
    }
    
    public static void removeImageFromCache(final Context context, String key) {
        initCacheForImages(context);
        synchronized (mMemoryCacheImages) {
            mMemoryCacheImages.remove(key);
        }
    }
    
    public static void addListToCache(String key, ListCacheBean value) {
        if (key == null || value == null) {
            Log.e(TAG_LOGGER, "addListToCache() - Invalid input: either 'key' or 'value' cannot be NULL!");
            return;
        }
 
        initCacheForList();
        if(mMemoryCacheLists.get(key) != null) {
            //Remove to avoid unwanted duplicate: 
            synchronized (mMemoryCacheLists) {
                mMemoryCacheLists.remove(key);
                mMemoryCacheLists.put(key, value);
            }
        } else {
            synchronized (mMemoryCacheLists) {
                mMemoryCacheLists.put(key, value);
            }
        }

        Log.d(TAG_LOGGER, "addListToCache() - Completed for key [" + key + "] and value [" + value 
                + "]. Memory (heap) currently allocated [" + (int) (Runtime.getRuntime().totalMemory() / 1024) + " KB]"
                + ", Cache reserved size (number): " + mMemoryCacheLists.maxSize()
                + ", Cache used size (number): " + mMemoryCacheLists.size()
                );
    }
 
    public static ListCacheBean getListFromCache(String key) {
        initCacheForList();
        return mMemoryCacheLists.get(key);
    }
    
    public static void removeListFromCache(String key) {
        initCacheForList();
        synchronized (mMemoryCacheLists) {
            mMemoryCacheLists.remove(key);
        }
    }    
    
    /*
     * Calculates appropriate cache size in KB to be reserved for images caching mechanism.
     * Note: 
     * 1. The implementation will only allow maximum THREE (3) images stored with maximum size
     *    of 'picture image for view' for each. 
     * 2. For the app, the quality of images are stored and displayed as Bitmap.Config.RGB_565, which
     *    stores 2 bytes per pixel.
     * 3. Memory sizes:
     *    a. Device maximum heap size (Runtime.maxMemory) = is the device's maximum memory before it runs out of memory error
     *    b. Device allocated heap size (Runtime.totalMemory) = is the currently allocated memory that the OS
     *       dynamically increases/reduces per demand. When this size is too close or exceeding
     *       maxMemory, the OutOfMemory error will be thrown. 
     *    c. App appropriate maximum heap size (ActivityManager.getMemoryClass) = is the heap size
     *       that OS calculates that an app should use. The number is typically very small
     *       such as 16 bytes or 32 bytes (compared to 16MB of maxMemmory, for instance). This is
     *       not useful for Cache memory.
     * 
     * @return The size (KB) of caching to be reserved.
     */
    private static int calculateCacheSizeForImages(final Context context, Config config) {
        int numOfBytesPerPixel = 1;
        if (config == Config.ARGB_8888) {
            numOfBytesPerPixel = 4;
        } else if (config == Config.RGB_565) {
            numOfBytesPerPixel = 2;
        } else if (config == Config.ARGB_4444) {
            numOfBytesPerPixel = 2;
        } else if (config == Config.ALPHA_8) {
            numOfBytesPerPixel = 1;
        }

        int reservedSize = 0;
        int memoryAllocated = (int) (Runtime.getRuntime().totalMemory() / 1024);
        int imgSizeToStore = SingletonFactory.getPictureViewSize(context);
        
        //For cache, we will store 3 images at a time:
        int wantedSize = 3 * (imgSizeToStore * imgSizeToStore * numOfBytesPerPixel) / 1024;
        if(memoryAllocated > wantedSize) {
            reservedSize = wantedSize;
        } else {
            //Reserve 1/8th of maximum memory:
            reservedSize = AndroidConstants.DEVICE_HEAP_MEMORY_MAX/8;
        }
        return reservedSize;
    }
    

}
