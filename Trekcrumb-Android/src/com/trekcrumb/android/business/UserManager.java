package com.trekcrumb.android.business;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.trekcrumb.android.database.TripDB;
import com.trekcrumb.android.database.UserAuthTokenDB;
import com.trekcrumb.android.database.UserDB;
import com.trekcrumb.android.utility.FileUtil;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.UserAuthTokenWSClient;
import com.trekcrumb.webserviceclient.UserWSClient;

/**
 * Manager to care about User and UserAuthToken entities.
 * 
 * @author Val Triadi
 *
 */
public class UserManager {
    private static final String LOG_TAG = "UserManager";

    /**
     * Retrieves stored UserAuthToken data from local repository.
     */
    public static UserAuthToken userAuthTokenRetrieve(final Context context) {
        return UserAuthTokenDB.retrieve(context);
    }    
    
    /**
     * Authenticate the given UserAuthToken against remote server.
     * 1. Rainy Day cases:
     *    1.1. If Server returns null, return as is
     *    1.2. Else: Server returns failure
     *         1.2.1. If the error is 'AUTHENTICATION_FAILED_ERROR':
     *                1.2.1.1. Delete UserAuthToken on local device entry since it is invalid
     *                1.2.1.2. Delete User record on local device
     *                
     *         1.2.2. Else if the error is due to network failure or server not reachable:
     *                1.2.2.1. Retrieve stored User record locally
     *                1.2.2.2. Return the user and let app runs locally only
     *                
     *         1.2.3. Else: return as is with user record NULL
     */
    public static ServiceResponse userAuthTokenAuthenticate(
            final Context context,
            UserAuthToken userAuthToken) {
        UserAuthToken userAuthTokenToVerify = new UserAuthToken();
        userAuthTokenToVerify.setUserId(userAuthToken.getUserId());
        userAuthTokenToVerify.setAuthToken(userAuthToken.getAuthToken());
        ServiceResponse authResult = UserAuthTokenWSClient.authenticate(userAuthTokenToVerify);
        
        //If failed, intercept to do some business logic here:
        if(authResult == null) {
            return authResult;
        }
        if(!authResult.isSuccess()
                && authResult.getServiceError() != null) {
            Log.e(LOG_TAG, "UserTokenAuth authentication error: " 
                    + authResult.getServiceError().getErrorEnum()
                    + ", " + authResult.getServiceError().getErrorText());

            ServiceErrorEnum errorEnum = authResult.getServiceError().getErrorEnum();
            if( errorEnum == ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR) {
                UserAuthTokenDB.delete(context);
                
                User userToDelete = new User();
                userToDelete.setUserId(userAuthToken.getUserId());
                UserDB.delete(context, userToDelete, ServiceTypeEnum.USER_DELETE_BY_USERID);
                return authResult;
            }
            
            if(errorEnum == ServiceErrorEnum.SYSTEM_NETWORK_NOT_AVAILABLE_ERROR
                    || errorEnum == ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR) {
                User userToRetrieve = new User();
                userToRetrieve.setUserId(userAuthToken.getUserId());
                User foundUserLocally = UserDB.retrieve(context, userToRetrieve);
                if(foundUserLocally != null) {
                    authResult.setUser(foundUserLocally);
                }
                return authResult;
            }
        }
        
        return authResult;
    }
    
    /**
     * Removes UserAuthToken from remote server.
     */
    public static ServiceResponse userAuthTokenDeleteOnServer(
            final Context context,
            UserAuthToken userAuthTokenToDelete) {
        if(userAuthTokenToDelete == null
                || ValidationUtil.isEmpty(userAuthTokenToDelete.getId())) {
            String errMsg = "UserAuthToken Delete error: UserAuthTokenId is required!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
        }
        
        return UserAuthTokenWSClient.delete(userAuthTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
    }

    /**
     * Removes UserAuthToken from local device.
     */
    public static void userAuthTokenDeleteOnDevice(
            final Context context,
            UserAuthToken userAuthTokenToDelete) {
        UserAuthTokenDB.delete(context);
    }
        
    /**
     * Creates User and User Auth Token to remote server.
     */
    public static ServiceResponse userCreateOnServer(
            final Context context, 
            User userToCreate,
            UserAuthToken userAuthTokenToCreate) {
        ServiceResponse result = UserWSClient.userCreate(userToCreate, userAuthTokenToCreate);
        if(result == null 
                || !result.isSuccess()
                || result.getUser() == null) {
            return result;
        }
        
        //Now store it locally:
        userCreateOnDevice(context, result.getUser(), result.getUser().getUserAuthToken());
        return result;
    }    
    
    /**
     * Creates User and User Auth Token data into local device.
     * 
     * 1. Find the given user in User table
     * 1.1. If found: User has been stored before; update the user with new data from server
     * 1.2. Else: Insert the new user (who is currently logged in) into User table
     *      
     * 1.3. Delete all previous UserAuthToken records, if any
     * 1.4. Insert the new UserAuthToken into UserAuthToken table
     */
    public static void userCreateOnDevice(
            final Context context, 
            User userToCreate,
            UserAuthToken userAuthTokenToCreate) {
        User userFoundLocally = UserDB.retrieve(context, userToCreate);
        if(userFoundLocally != null) {
            try {
                UserDB.update(context, userToCreate);
            } catch(Exception e) {
                //Wont do anyting.
            }
        } else {
            UserDB.create(context, userToCreate);
        }
        
        if(userAuthTokenToCreate != null) {
            UserAuthTokenDB.delete(context);
            UserAuthTokenDB.create(context, userAuthTokenToCreate);
        }
    }
    
    public static ServiceResponse userLogin(
            final Context context,
            User userToLogin,
            UserAuthToken userAuthTokenToCreate) {
        ServiceResponse result = UserWSClient.userLogin(userToLogin, userAuthTokenToCreate);
        if(result == null 
                || !result.isSuccess()
                || result.getUser() == null) {
            return result;
        }
        
        userCreateOnDevice(context, result.getUser(), result.getUser().getUserAuthToken());
        return result;
    }    
    
    public static ServiceResponse userLogout(
            final Context context,
            User userToLogout,
            boolean isDeleteStoredData) {
        ServiceResponse result = null;
        if(userToLogout == null 
                || ValidationUtil.isEmpty(userToLogout.getUserId())) {
            String errMsg = "User Logout error: UserID is required!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    errMsg);
        }
        String userID = userToLogout.getUserId();
        
        //1. Delete stored UserAuthToken:
        UserAuthToken userAuthTokenStored = userAuthTokenRetrieve(context);
        if(userAuthTokenStored != null) {
            userAuthTokenDeleteOnDevice(context, userAuthTokenStored);
        }

        //2. Delete user's stored data:
        if(isDeleteStoredData) {
            deleteUserDataOnLocal(context, userID);
        }

        result = CommonUtil.composeServiceResponseSuccess(null);
        if(userAuthTokenStored != null) {
            userToLogout.setUserAuthToken(userAuthTokenStored);
            result.setUser(userToLogout);
        }
        return result;
    }
    
    /**
     * Updates user profile and setting.
     */
    public static ServiceResponse userUpdate(
            final Context context,
            User userToUpdate,
            boolean isNewPicByCamera) {
        
        //Profile picture, if any:
        String profileImgSourceURL = null;
        Bitmap userProfileImageBitmap = null;
        if(!ValidationUtil.isEmpty(userToUpdate.getProfileImageURL())) {
            profileImgSourceURL = userToUpdate.getProfileImageURL();
            userProfileImageBitmap = ImageUtil.decodePictureImageIntoBitmap(
                    null, profileImgSourceURL, CommonConstants.PICTURE_PROFILE_MAX_SIZE_PIXEL_SAVE, false, false);
            if(userProfileImageBitmap == null) {
                Log.e(LOG_TAG, "User Update error: Cannot update user profile image due to NO file found given source location: " + profileImgSourceURL);
                //userToUpdate.setProfileImageName(null);
                userToUpdate.setProfileImageURL(null);
            } else {
                //Server requires image bytes; it will generate new image name:
                byte[] imgBytes = ImageUtil.decodeBitmapToBytes(userProfileImageBitmap);
                userToUpdate.setProfileImageBytes(imgBytes);
            }
        }

        //Remote server:
        ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
        if(result == null 
                || !result.isSuccess()
                || result.getUser() == null) {
            return result;
        }
        
        //Local device:
        User updatedUser = result.getUser();
        updatedUser = updateUserDataOnLocal(
                context,
                updatedUser,
                userProfileImageBitmap,
                isNewPicByCamera,
                profileImgSourceURL);
        
        return result;
    }
    
    /**
     * Deactivates user and delete all user data from local device.
     */
    public static ServiceResponse userDeactivate(
            final Context context,
            User userToUpdate) {
        String userID = userToUpdate.getUserId();
        ServiceResponse svcResponse = UserWSClient.userDeActivate(userToUpdate);
        if(svcResponse != null) {
            if(svcResponse.isSuccess()) {
                //Check stored user and delete all:
                UserAuthToken userAuthTokenToDelete = new UserAuthToken();
                userAuthTokenToDelete.setUserId(userID);
                userAuthTokenDeleteOnDevice(context, userAuthTokenToDelete);
                
                deleteUserDataOnLocal(context, userID);
            }
        }
        return svcResponse;
    }    

    /**
     * Deactivates user and delete all user data from local device.
     */
    public static ServiceResponse userReactivate(
            final Context context,
            User userToUpdate) {
        return UserWSClient.userReActivate(userToUpdate);
    }
    
    /**
     * Retrieves User profile data (user login or user other) from server.
     * @param userToRetrieve - Contains the UserID to retrieve.
     * @param isUserLoginProfile - If true, the profile being retrieved is for the current
     *                             User login, and may have specific further treatments.
     */
    public static ServiceResponse userRetrieve(
            final Context context,
            User userToRetrieve,
            boolean isUserLoginProfile) {
        ServiceResponse result = null;
        if(userToRetrieve == null 
                || ValidationUtil.isEmpty(userToRetrieve.getUserId())) {
            String errMsg = "User Retrieve error: Missing userID to retrieve in input!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_RETRIEVE_BY_USERID, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    errMsg);
        }
        
        result = UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, 0, 1);
        if(result == null || !result.isSuccess()) {
            return result;
        }
        
        if(result.getListOfUsers() == null 
                || result.getListOfUsers().size() == 0
                || result.getListOfUsers().get(0) == null) {
            String errMsg = "User Retrieve error: Given userID is no longer found in the system!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_RETRIEVE_BY_USERID, 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, 
                    errMsg);
        }
        
        if(isUserLoginProfile) {
            //This is userLogin refresh, so update the stored data with new data
            User userLoginRefreshed = result.getListOfUsers().get(0);
            try {
                UserDB.update(context, userLoginRefreshed);
            } catch(Exception e) {
                //Wont do anyting.
            }
        }
        
        return result;
    }
    
    /**
     * Submits User data (email, etc.) to the system in order to reset his/her password.
     * @param userForget - Contains email to process.
     */
    public static ServiceResponse userForget(User userForget) {
        if(userForget == null 
                || ValidationUtil.isEmpty(userForget.getEmail())) {
            String errMsg = "User Password Forget error: Missing user email!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_PASSWORD_FORGET, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    errMsg);
        }
        
        return UserWSClient.userPasswordForget(userForget);
    }    
    
    private static User updateUserDataOnLocal(
            Context context,
            User updatedUser,
            Bitmap userProfileImageBitmap,
            boolean isNewPicByCamera,
            String imgSourceURL) {
        Log.d(LOG_TAG, "updateUserDataOnLocal() - Starting ... ");
        try {
            //If update profile image:
            if(userProfileImageBitmap != null) {
                if(!ValidationUtil.isEmpty(updatedUser.getProfileImageName())) {
                    String picLocalURL = ImageUtil.decodeBitmapToFile(
                            userProfileImageBitmap, 
                            updatedUser.getProfileImageName());
                    updatedUser.setProfileImageURL(picLocalURL);
                    
                    //Delete previous profile image locally, if any:
                    //TODO
                    
                } //Else: Cannot save it locally!
                
                //Release resources to avoid "failed binder transaction" (out of memory) error:
                userProfileImageBitmap = null;
                
                //Delete temporary picture by camera, if any:
                if(isNewPicByCamera) {
                    FileUtil.deleteFile(imgSourceURL);
                }
            }
            
            //Update user data:
            UserDB.update(context, updatedUser);
        } catch(Exception e) {
            //Cant do anything but log it:
            Log.e(LOG_TAG, "updateUserDataOnLocal() - Error: " + e.getMessage(), e);
            e.printStackTrace();
        }
        
        return updatedUser;
    }    
    
    private static void deleteUserDataOnLocal(
            final Context context,
            String userID) {
        try {
            Place placeToDelete = new Place();
            placeToDelete.setUserID(userID);
            PlaceManager.delete(context, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_USERID, TripPublishEnum.NOT_PUBLISH);
            
            Picture pictureToDelete = new Picture();
            pictureToDelete.setUserID(userID);
            PictureManager.delete(context, pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_USERID, TripPublishEnum.NOT_PUBLISH);
            
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(userID);
            TripDB.delete(context, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID);

            User userToDelete = new User();
            userToDelete.setUserId(userID);
            UserDB.delete(context, userToDelete, ServiceTypeEnum.USER_DELETE_BY_USERID);
        } catch(Exception e) {
            //Ignore
            String errMsg = "User Logout error: While trying to delete stored user data - " + e.getMessage();
            Log.e(LOG_TAG, errMsg);
            e.printStackTrace();
        }            
    }
    
    /*
    private static void userProfilePictureCreate(Context context, User user) {
        if(user.getProfileImageURL() == null) {
            return;
        }
        
        Log.d(LOG_TAG, "userProfilePictureCreate() - Starting ... ");
        try {
            //Get the source image first and put into memory:
            String imgSourceURL = user.getProfileImageURL();
            Bitmap imgBitmapToSave = 
                    ImageUtil.decodeFileIntoBitmap(imgSourceURL, CommonConstants.PICTURE_PROFILE_MAX_SIZE_PIXEL_SAVE, false);
            if(imgBitmapToSave == null) {
                Log.e(LOG_TAG, "userProfilePictureCreate() - ERROR: No image found given source location: " + imgSourceURL);
                return;
            }
            byte[] imgBytes = ImageUtil.decodeBitmapToBytes(imgBitmapToSave);

            //Compose FS request:
            FileServerRequest requestToWrite = new FileServerRequest();
            requestToWrite.setFileMenu(FileServerMenuEnum.WRITE_PROFILE_PICTURE);
            //requestToWrite.setFilename(picImageName);
            requestToWrite.setFileBytes(imgBytes);

            //Send:
            FileServerResponse response = FileServerClient.writeFile(requestToWrite);
            if(!response.isSuccess()) {
                if(response.getServiceError() != null) {
                    Log.e(LOG_TAG, "userProfilePictureCreate() - ERROR: " + response.getServiceError().getErrorText());
                } else {
                    Log.e(LOG_TAG, "userProfilePictureCreate() - ERROR: The reason is unknown from server.");
                }
                return;
            }
            Log.d(LOG_TAG, "userProfilePictureCreate() - Completed successfully.");
            
        } catch(Exception e) {
            Log.e(LOG_TAG, "userProfilePictureCreate() - ERROR!", e);
        }
    }    
    */    
}
