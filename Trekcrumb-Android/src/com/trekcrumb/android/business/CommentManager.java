package com.trekcrumb.android.business;

import android.content.Context;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.CommentWSClient;

public class CommentManager {

    public static ServiceResponse retrieve(
            Context context,
            Comment commentToRetrieve,
            ServiceTypeEnum retrieveBy,
            int offset) {
        //Validation:
        if(commentToRetrieve == null) {
            return CommonUtil.composeServiceResponseError(
                    retrieveBy,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Comment Retrieve error: Missing required Comment to retrieve!");
        }
        if(retrieveBy == ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID) {
            if(ValidationUtil.isEmpty(commentToRetrieve.getTripID())) {
                return CommonUtil.composeServiceResponseError(
                        retrieveBy,
                        ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                        "Comment Retrieve error: Missing required TripID!");
            }
            
        } else if(retrieveBy == ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME) {
            if(ValidationUtil.isEmpty(commentToRetrieve.getUsername())) {
                return CommonUtil.composeServiceResponseError(
                        retrieveBy,
                        ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                        "Comment Retrieve error: Missing required Username!");
            }
            
        } else {
            return CommonUtil.composeServiceResponseError(
                    retrieveBy,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Comment Retrieve error: Unknown retrieve by type [" + retrieveBy + "]!");
        }
        
        //All is good:
        ServiceResponse svcResponse = null;
        if(retrieveBy == ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID) {
            svcResponse = CommentWSClient.retrieve(commentToRetrieve, retrieveBy, null, offset, CommonConstants.RECORDS_NUMBER_MAX);
        } else {
            svcResponse = CommentWSClient.retrieve(commentToRetrieve, retrieveBy, OrderByEnum.ORDER_BY_CREATED_DATE, offset, CommonConstants.RECORDS_NUMBER_MAX);
        }
        return svcResponse;
    }
    
}
