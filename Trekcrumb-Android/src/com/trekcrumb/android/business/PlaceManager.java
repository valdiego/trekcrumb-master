package com.trekcrumb.android.business;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.trekcrumb.android.database.PlaceDB;
import com.trekcrumb.android.database.TripDB;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.PlaceWSClient;

public class PlaceManager {
    private static final String LOG_TAG = "PlaceManager";

    private PlaceManager() {}
    
    /**
     * Creates a new Place for the Trip on either remote or local DB based on tripPublishStatus param.
     * 
     * Note:
     * 1. This operation is mostly for adding a new Place to an existing Trip
     * 2. To create a Place as part of sync() operation (that is previously created/updated on 
     *    server), TripManager.sync() and PlaceManager.sync()

     * @param placeToUpdate - Contains the TripID where this Place belongs to, and all other data.
     * @param tripPublishStatus - If NOT_PUBLISH, then it saves on local DB only. If PUBLISH, it will
     *                            saves on remote server then copy the record into local DB.
     */
    public static ServiceResponse create(
            Context context, 
            Place placeToCreate,
            TripPublishEnum tripPublishStatus) {
        ServiceResponse result = null;
        if(placeToCreate == null) {
            return CommonUtil.composeServiceResponseError(
                    null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Place Create error: Missing required Place data to create!");
        }

        if(tripPublishStatus == TripPublishEnum.PUBLISH) {
            //Remote server:
            result = PlaceWSClient.create(placeToCreate);
            if(!result.isSuccess()
                    || result.getListOfPlaces() == null
                    || result.getListOfPlaces().size() == 0) {
                return result;
            }
            
            placeToCreate = result.getListOfPlaces().get(0);
        }
        
        //Local device: 
        return createOnLocal(context, placeToCreate);
    }
    
    /**
     * Sync Places as part of Trip sync operation by saving the Place(s) "as is" from server to
     * local DB.
     */
    public static ServiceResponse sync(
            Context context, 
            List<Place> listOfPlacesToSync) {
        if(listOfPlacesToSync == null
                || listOfPlacesToSync.size() == 0) {
            return CommonUtil.composeServiceResponseSuccess(null);
        }

        try {
            for(Place place : listOfPlacesToSync) {
                PlaceDB.create(context, place);
            }
        } catch(IllegalArgumentException iae) {
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Place Sync error: " + e.getMessage());
        }
        return CommonUtil.composeServiceResponseSuccess(null);
    }
    

    /**
     * Finds Places from device local DB.
     */
    public static ServiceResponse retrieve(    
            Context context,
            Place placeToRetrieve,
            ServiceTypeEnum retrieveBy,
            int offset,
            int numOfRows) {
        ServiceResponse result = null;
        try {
            List<Place> listOfPlaces = PlaceDB.retrieve(context, placeToRetrieve, retrieveBy, offset, numOfRows);
            result = CommonUtil.composeServiceResponseSuccess(retrieveBy);
            result.setListOfPlaces(listOfPlaces);
            
        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, iae.getMessage());
            result = CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            result = CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, "Place Retrieve error: " + e.getMessage());
        }
        return result;        
    }
    
    /**
     * Updates Place on either remote or local DB based on tripPublishStatus param.
     * @param placeToUpdate - Contains the PlaceID and all other data to update.
     * @param tripPublishStatus - If NOT_PUBLISH, then it saves on local DB only. If PUBLISH, it will
     *                            saves on remote server then copy the record into local DB.
     */
    public static ServiceResponse update(
            Context context, 
            Place placeToUpdate,
            TripPublishEnum tripPublishStatus) {
        if(placeToUpdate == null
                || ValidationUtil.isEmpty(placeToUpdate.getUserID())
                || ValidationUtil.isEmpty(placeToUpdate.getTripID())
                || ValidationUtil.isEmpty(placeToUpdate.getId())) {
            return CommonUtil.composeServiceResponseError(
                    null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Place Update error: Missing required data: User ID, Trip ID and Place ID!");
        }
        
        if(tripPublishStatus == TripPublishEnum.PUBLISH) {
            //Remote server:
            ServiceResponse result = PlaceWSClient.update(placeToUpdate);
            if(result == null || !result.isSuccess()) {
                return result;
            }
            
            if(result.getListOfPlaces() != null && result.getListOfPlaces().size() > 0) {
                placeToUpdate = result.getListOfPlaces().get(0);
            }
        }
        
        //Local device:
        return updateOnLocal(context, placeToUpdate);
    }
    
    /**
     * Deletes Place on either remote or local DB based on tripPublishStatus param.
     * @param placeToUpdate - Contains required data to identify the Place to delete.
     * @param tripPublishStatus
     *        1. If NOT_PUBLISH, it deletes on local DB only
     *        2. If PUBLISH, it will delete on remote server, then delete the record from local DB.
     *           By default, this only applies for ServiceTypeEnum.PLACE_DELETE_BY_PLACEID only
     *           because other type of deletes are handled by server component implicitly.
     */
    public static ServiceResponse delete(
            Context context, 
            Place placeToDelete,
            ServiceTypeEnum deleteBy,
            TripPublishEnum tripPublishStatus) {
        if(placeToDelete == null
            || ValidationUtil.isEmpty(placeToDelete.getUserID())
            || ValidationUtil.isEmpty(placeToDelete.getTripID())) {
            return CommonUtil.composeServiceResponseError(
                    null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Place Delete error: Missing required data: User ID, Trip ID and/or Place ID!");
        }
        //Set input with defaults:
        if(ValidationUtil.isEmpty(placeToDelete.getUpdated())) {
            placeToDelete.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        }

        if(tripPublishStatus == TripPublishEnum.PUBLISH) {
            //Remote server:
            //NOTE: Only apply for delete by placeID. If delete by USERID or TRIPID,
            //      the server will take care of its own.
            ServiceResponse result = 
                    PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            if(!result.isSuccess()) {
                return result;
            }
        }
        
        //Local device:
        return deleteOnLocal(context, placeToDelete, deleteBy);
    }

    private static ServiceResponse createOnLocal(
            Context context, 
            Place placeToCreate) {
        ServiceResponse result = null;
        try {
            if(ValidationUtil.isEmpty(placeToCreate.getId())) {
                //It is new fresh record, set defaults:
                placeToCreate.setId(CommonUtil.generateID(placeToCreate.getTripID()));
                placeToCreate.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
                placeToCreate.setUpdated(placeToCreate.getCreated());
                
            } //Else: A copy from server
            
            //Execute:
            PlaceDB.create(context, placeToCreate);
            
            //Update Trip:
            Trip tripToUpdate =  new Trip();
            tripToUpdate.setId(placeToCreate.getTripID());
            tripToUpdate.setUpdated(placeToCreate.getCreated());
            TripDB.update(context, tripToUpdate);
            
            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(null);
            List<Place> listOfCreatedPlaces = new ArrayList<Place>();
            listOfCreatedPlaces.add(placeToCreate);
            result.setListOfPlaces(listOfCreatedPlaces);
            
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Place Create error: " + e.getMessage());
        }
        return result;
    }
    
    private static ServiceResponse updateOnLocal(
            Context context, 
            Place placeToUpdate) {
        ServiceResponse result = null;
        
        //Set input with defaults:
        if(ValidationUtil.isEmpty(placeToUpdate.getUpdated())) {
            placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        }

        try {
            //Execute:
            PlaceDB.update(context, placeToUpdate);
            
            //Update Trip:
            Trip tripToUpdate =  new Trip();
            tripToUpdate.setId(placeToUpdate.getTripID());
            tripToUpdate.setUpdated(placeToUpdate.getUpdated());
            TripDB.update(context, tripToUpdate);
            
            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(null);
            List<Place> listOfPlaces = 
                    PlaceDB.retrieve(context, placeToUpdate, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, 0, -1); 
            if(listOfPlaces == null || listOfPlaces.size() == 0) {
                Log.e(LOG_TAG, "Place Update error: After update, cannot find any Place for TripID [" + placeToUpdate.getTripID() + "]");
            } 
            result.setListOfPlaces(listOfPlaces);
           
        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, iae.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Place Update error: " + e.getMessage());
        }
        return result;
    }
    
    /*
     * Deletes places on local device.
     * 2. If deleteBy USERID or TRIPID, there is NO need to update Trip's UPDATED date
     * 3. if deleteBy USERID or TRIPID, there is NO need to return the latest list of places
     */
    private static ServiceResponse deleteOnLocal(
            Context context, 
            Place placeToDelete, 
            ServiceTypeEnum deleteBy) {
        ServiceResponse result = null;
        try {
            //Execute:
            PlaceDB.delete(context, placeToDelete, deleteBy);
            
            //Update Trip:
            if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_PLACEID) {
                Trip tripToUpdate =  new Trip();
                tripToUpdate.setId(placeToDelete.getTripID());
                tripToUpdate.setUpdated(placeToDelete.getUpdated());
                TripDB.update(context, tripToUpdate);
            }
            
            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(deleteBy);
            if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_PLACEID) {
                List<Place> listOfPlaces = 
                        PlaceDB.retrieve(context, placeToDelete, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, 0, -1); 
                result.setListOfPlaces(listOfPlaces);
            }
            
        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, iae.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Place Delete error: " + e.getMessage());
        }
        return result;
    }
}
