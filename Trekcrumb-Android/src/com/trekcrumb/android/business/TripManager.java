package com.trekcrumb.android.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.trekcrumb.android.database.PlaceDB;
import com.trekcrumb.android.database.TripDB;
import com.trekcrumb.android.database.UserDB;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.TripWSClient;

public class TripManager {
    private static final String LOG_TAG = "TripManager";

    private TripManager() {}
    
    /**
     * Creates a new Trip.
     * 
     * By default, the trip is created for the logged in user. This method handles both published 
     * and not published new trip.
     * 1. Published
     * 1.1. Connect and create the Trip to Server
     * 1.2. Updates any existing 'ACTIVE' Trips on local device: published and unpublished Trips.
     * 1.3. Copy the newly created trip into device's local repository along with any server data
     *      (Trip ID, created/updated dates, etc.).
     * 
     * 2. Unpublished
     * 2.1. Updates any existing 'ACTIVE' Trips on local device: unpublish Trips ONLY
     * 2.2. Create the new trip on device's local repository only.
     */
    public static ServiceResponse create(
            final Context context,
            Trip tripToCreate) {
        if(tripToCreate == null
                || ValidationUtil.isEmpty(tripToCreate.getUserID())
                || ValidationUtil.isEmpty(tripToCreate.getName())
                || tripToCreate.getPrivacy() == null
                || tripToCreate.getStatus() == null) {
            String errMsg = "Trip Create error: UserID, Trip name, status and privacy are required!";
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,errMsg);
        }
        
        ServiceResponse result = null;
        try {
            if(tripToCreate.getPublish() == TripPublishEnum.PUBLISH) {
                //Remote server:
                ServiceResponse serverResult = TripWSClient.create(tripToCreate);
                if(serverResult == null
                        || !serverResult.isSuccess()
                        || serverResult.getListOfTrips() == null
                        || serverResult.getListOfTrips().size() == 0) {
                    return serverResult;
                }
                //Replaced the Trip to create bean with that from the server:
                tripToCreate = serverResult.getListOfTrips().get(0);
            }
            
            //Update any existing active Trip locally:
            if(tripToCreate.getPublish() == TripPublishEnum.PUBLISH) {
                TripDB.updateTripStatusCompleted(context, tripToCreate.getUserID(), null);
            } else {
                TripDB.updateTripStatusCompleted(context, tripToCreate.getUserID(), TripPublishEnum.NOT_PUBLISH);
            }
            
            //Local device:
            result = createOnLocal(context, tripToCreate, false);
            
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    /**
     * Publishes a Trip.
     * 
     * NOTE:
     * 1. By default, the trip belongs to the logged in user.
     * 2. This method, in theory, is to update Trip's publish status from not publish to publish.
     *    However, the method involves creating new trip to remote server, and update the existing
     *    trip in device's local DB accordingly. The publish includes Trip's Places and Pictures
     * 3. To publish Pictures, see PictureManager.publish() 
     * 
     */
    public static ServiceResponse publish(
            final Context context,
            Trip tripToPublish) {
        if(tripToPublish == null
                || ValidationUtil.isEmpty(tripToPublish.getUserID())
                || ValidationUtil.isEmpty(tripToPublish.getId())
                || ValidationUtil.isEmpty(tripToPublish.getName())
                || tripToPublish.getPrivacy() == null
                || tripToPublish.getStatus() == null
                || ValidationUtil.isEmpty(tripToPublish.getCreated())
                || ValidationUtil.isEmpty(tripToPublish.getUpdated())) {
            String errMsg = "Trip Publish error: User ID, Trip ID, Trip name, status, privacy, created and updated dates are all required!";
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,errMsg);
        }

        ServiceResponse tripPublishResult = null;
        tripPublishResult = TripWSClient.publish(tripToPublish);
        if(tripPublishResult == null
                || !tripPublishResult.isSuccess()
                || tripPublishResult.getListOfTrips() == null
                || tripPublishResult.getListOfTrips().size() == 0
                || tripPublishResult.getListOfTrips().get(0) == null) {
            return tripPublishResult;
        }
        
        //Publish trek Pictures (if any):
        ServiceResponse picPublishResult = PictureManager.publish(context, tripToPublish.getListOfPictures());
        if(picPublishResult == null
                || !picPublishResult.isSuccess()) {
            return picPublishResult;
        }
        
        //Now update local record with any data changes (status active or completed, etc.) from server:
        Trip tripPublished = tripPublishResult.getListOfTrips().get(0);
        try {
            String userID = tripPublished.getUserID();
            if(tripPublished.getStatus() == TripStatusEnum.ACTIVE) {
                /*
                 * From the server, it is the latest ACTIVE. Hence, update any 'active published'
                 * trip on this local device. Let any unpublished trip active because that this trip) 
                 */
                TripDB.updateTripStatusCompleted(context, userID, TripPublishEnum.PUBLISH);
            } else {
                /*
                 * The result published trip is not active or no longer active. But we need to ensure
                 * any local 'active' trip if created before this published trip, it must be marked
                 * as 'completed'.
                 */
                String whereClause = "USERID = ? and STATUS = ?";
                String[] whereValue = {userID, String.valueOf(TripStatusEnum.ACTIVE.getId())};
                List<Trip> listOfCurrentActiveTrips = TripDB.retrieve(
                        context, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE,
                        whereClause, whereValue, OrderByEnum.ORDER_BY_CREATED_DATE, 0, 1);
                if(listOfCurrentActiveTrips != null 
                        && listOfCurrentActiveTrips.size() > 0 
                        && listOfCurrentActiveTrips.get(0) != null) {
                    Trip tripCurrentActive = listOfCurrentActiveTrips.get(0);
                    Date tripCurrentActiveCreatedDate = DateUtil.formatDate(tripCurrentActive.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
                    Date tripPublishedCreatedDate = DateUtil.formatDate(tripPublished.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
                    if(tripPublishedCreatedDate.after(tripCurrentActiveCreatedDate)) {
                        TripDB.updateTripStatusCompleted(context, userID, null);
                    }
                }
            }
            
            TripDB.update(context, tripPublished);
            
        } catch(IllegalArgumentException iae) {
            tripPublishResult = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            tripPublishResult = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return tripPublishResult;
    }    
    
    /**
     * Sync user's Trip records with the latest updates from server. 
     * The sync will only copy those trip(s) whose updated date is after local device's 
     * last updated trip.
     */
    public static ServiceResponse sync(
            Context context,
            User userLogin) {
        if(userLogin == null) {
            return CommonUtil.composeServiceResponseError(
                    null, ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, 
                    "Trip Sync error: Illegal Access missing user login data!");
        }
        if(evaluateTripRecordsUpToDate(context, userLogin)) {
            //Nothing to sync:
            return CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.TRIP_RETRIEVE_SYNC);
        }
        
        //Else:
        ServiceResponse result = null;
        try {
            //Get required data:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(userLogin.getUserId());
            tripToRetrieve.setPublish(TripPublishEnum.PUBLISH);
            List<Trip> lastTripUpdatedOnLocal = TripDB.retrieve(
                    context, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID,
                    null, null, OrderByEnum.ORDER_BY_UPDATED_DATE, 0, 1);
            if(lastTripUpdatedOnLocal != null
                    && lastTripUpdatedOnLocal.size() > 0) {
                tripToRetrieve.setUpdated(lastTripUpdatedOnLocal.get(0).getUpdated());
            }
            
            //Call Remote Server:
            result = TripWSClient.retrieveSync(tripToRetrieve);
            if(result == null 
                    || !result.isSuccess() 
                    || result.getListOfTrips() == null
                    || result.getListOfTrips().size() == 0) {
                return result;
            }
            Log.d(LOG_TAG, "Trip Sync SUCCESS: Number of synced Trips: " + result.getListOfTrips().size());

            //Process to update local DB with the latest one:
            List<Trip> listOfTripSynced =  result.getListOfTrips();
            for(Trip trip : listOfTripSynced) {
                deleteOnLocal(context, trip);
                createOnLocal(context, trip, true);
            }
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
        
    /**
     * Retrieves a Trip or list of Trips based on input parameters.
     * 
     * The returned Trip(s) has all the details (places, pictures, etc.) in it. 
     * This method handles trips for both for userLogin and userOther.
     */
    public static ServiceResponse retrieve(
            Context context,
            Trip tripToRetrieve,
            boolean isUserLoginProfile,
            ServiceTypeEnum retrieveBy,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) {
        ServiceResponse result = null;
        if(isUserLoginProfile) {
            //UserLogin - Retrieve from local device (always):
            try {
                List<Trip> listOfTrips = 
                        TripDB.retrieve(context, tripToRetrieve, retrieveBy, 
                                        null, null, orderBy, offset, numOfRows);
                tripEnrichmentOnLocal(context, listOfTrips);
                
                result = new ServiceResponse();
                result.setSuccess(true);
                result.setListOfTrips(listOfTrips);
                
            } catch(IllegalArgumentException iae) {
                result = CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
            } catch(Exception e) {
                e.printStackTrace();
                result = CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
            }
            return result;
            
        } else {
            //userOther - Only retrieve published and public trips from server
            tripToRetrieve.setPublish(TripPublishEnum.PUBLISH);
            tripToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
            return TripWSClient.retrieve(tripToRetrieve, retrieveBy, offset, numOfRows);
        }
    }
    
    /**
     * Counts total number of Trips given a User.
     * 
     * NOTE: This method has not been implemented for UserOther's trips. Expand it when necessary.
     */
    public static ServiceResponse count(
            Context context,
            Trip tripToCount,
            boolean isUserLoginProfile) {
        ServiceResponse result = null;

        //Validate:
        if(tripToCount == null 
                || tripToCount.getUserID() == null) {
            String errMsg = "Trip Count error: UserID is required!";
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
        }
        if(isUserLoginProfile
                && tripToCount.getPublish() == null) {
            String errMsg = "Trip Count error: UserLogin requires Trip's publish value!";
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
        }
        
        if(isUserLoginProfile) {
            //Count locally:
            try {
                int numOfTrips = TripDB.count(context, tripToCount);
                result = CommonUtil.composeServiceResponseSuccess(null);
                result.setNumOfRecords(numOfTrips);
            } catch(Exception e) {
                String errMsg = "Trip Count error: " + e.getMessage();
                Log.e(LOG_TAG, errMsg);
                e.printStackTrace();
                result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
            }
        }
        
        return result;
    }
    
    /**
     * Updates a Trip.
     * 
     * By default, the trip belongs to the logged in user.
     * This method handles both published and not published new trip.
     * 1. If published, it will update the Trip record on server first, then update the one on local DB.
     * 2. If not published, it will update the trip on device's local DB.
     */
    public static ServiceResponse update(
            final Context context,
            Trip tripToUpdate,
            TripPublishEnum publishOrNot) {
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        if(publishOrNot == TripPublishEnum.PUBLISH) {
            //Remote server:
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            if(result == null || !result.isSuccess()) {
                return result;
            }
            
            if(result.getListOfTrips() != null && result.getListOfTrips().size() > 0) {
                tripToUpdate = result.getListOfTrips().get(0);
            }
        }
        
        //Local device:
        return updateOnLocal(context, tripToUpdate);
    }
    
    /**
     * Deletes a Trip.
     * 
     * By default, the trip belongs to the logged in user.
     * This method handles both published and not published new trip.
     * 1. If published, it will delete the Trip record on server first, then delete the one on local DB.
     * 2. If not published, it will delete the trip on device's local DB.
     */
    public static ServiceResponse delete(
            final Context context,
            Trip tripToDelete,
            TripPublishEnum publishOrNot) {
        if(publishOrNot == TripPublishEnum.PUBLISH) {
            //Remote server:
            ServiceResponse serverResult = 
                    TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            if(serverResult == null
                    || !serverResult.isSuccess()) {
                return serverResult;
            }
        } 
        
        //Local device:
        return deleteOnLocal(context, tripToDelete);
    }
    
    /**
     * Determines if trip records on local device is up to date with server's for the given 
     * user login.
     * 
     * @return
     * 1. TRUE if server doesn't have any records (published trips)
     * 2. TRUE if local device last updated trip is the same (or after) the server
     * 3. FALSE if local device has no record of last updated trip from server
     * 4. FALSE if server's last updated trip is after local device's last updated trip
     */
    public static boolean evaluateTripRecordsUpToDate(
            Context context,
            User userLogin) {
        Log.d(LOG_TAG, "evaluateTripRecordsUpToDate() - Starting ...");
        String serverLastTripUpdatedDateStr = null;
        String localLastTripUpdatedDateStr = null;
        try {
            //Check server:
            if(userLogin.getLastUpdatedTrip() == null
                    || ValidationUtil.isEmpty(userLogin.getLastUpdatedTrip().getUpdated())) {
                Log.d(LOG_TAG, "There is NO trip records for the user on server. No need to sync.");
                return true;
            }
            serverLastTripUpdatedDateStr = userLogin.getLastUpdatedTrip().getUpdated();
            Log.d(LOG_TAG, "Server last updated Trip [" + userLogin.getLastUpdatedTrip().getName() + "] with UPDATED: " + serverLastTripUpdatedDateStr);
            
            //Check local:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(userLogin.getUserId());
            tripToRetrieve.setPublish(TripPublishEnum.PUBLISH);
            List<Trip> lastTripUpdatedOnLocal = TripDB.retrieve(
                    context, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID,
                    null, null, OrderByEnum.ORDER_BY_UPDATED_DATE, 0, 1);
            if(lastTripUpdatedOnLocal == null
                    || lastTripUpdatedOnLocal.get(0) == null
                    || ValidationUtil.isEmpty(lastTripUpdatedOnLocal.get(0).getUpdated())) {
                Log.e(LOG_TAG, "User trips records NOT up to date: Trip records on local device is EMPTY. Need to sync ALL trip records.");
                return false;
            }
            localLastTripUpdatedDateStr = lastTripUpdatedOnLocal.get(0).getUpdated(); 
            Log.d(LOG_TAG, "Local last updated Trip [" + lastTripUpdatedOnLocal.get(0).getName() + "] with UPDATED: " + localLastTripUpdatedDateStr);
            
            //Compare:
            Date serverTripUpdatedDate = DateUtil.formatDate(serverLastTripUpdatedDateStr, DateFormatEnum.GMT_STANDARD_LONG);
            Date localTripUpdatedDate = DateUtil.formatDate(localLastTripUpdatedDateStr, DateFormatEnum.GMT_STANDARD_LONG);
            if(serverTripUpdatedDate.after(localTripUpdatedDate)) {
                Log.e(LOG_TAG, "User trips records NOT up to date: Server record is AFTER (later than) local record. Need to sync the latest ones.");
                return false;
            } else {
                Log.d(LOG_TAG, "evaluateTripRecordsUpToDate() - Completed. serverLastTripUpdatedDate is BEFORE or SAME AS localLastTripUpdatedDate.");
                return true;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * Retrieves list of Trips based on search criteria.
     * 
     * This operation will return every Trips on the server database, and does not depends on 
     * User ID thus its difference with the retrieve() method.
     * 
     * @return ServiceResponse that contains the list of Trips.
     */
    public static ServiceResponse search(TripSearchCriteria searchCriteria) {
        return TripWSClient.search(searchCriteria);
    }    
    
    /*
     * Enriches each Trip entitiy in the given list with its components (Places, Pictures, ...)
     * from local repository for the given UserLogin.
     */
    private static void tripEnrichmentOnLocal(
            Context context,
            List<Trip> listOfTripsToEnrich) {
        

        if(listOfTripsToEnrich != null && listOfTripsToEnrich.size() > 0) {
            try {
                String userID = listOfTripsToEnrich.get(0).getUserID();
                User userToRetrieve = new User();
                userToRetrieve.setUserId(userID);
                User userLogin = UserDB.retrieve(context, userToRetrieve);

                Place placeToRetrieve = new Place();
                placeToRetrieve.setUserID(userID);
                
                Picture pictureToRetrieve = new Picture();
                pictureToRetrieve.setUserID(userID);
                
                for(Trip trip : listOfTripsToEnrich) {
                    if(userLogin != null) {
                        trip.setUserFullname(userLogin.getFullname());
                        trip.setUsername(userLogin.getUsername());
                        trip.setUserImageName(userLogin.getProfileImageName());
                    }
                    
                    placeToRetrieve.setTripID(trip.getId());
                    ServiceResponse retrievePlaceResult =  
                            PlaceManager.retrieve(context, placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, 0, -1);
                    if(retrievePlaceResult != null && retrievePlaceResult.isSuccess()) {
                        trip.setListOfPlaces(retrievePlaceResult.getListOfPlaces());
                    }
                
                    pictureToRetrieve.setTripID(trip.getId());
                    ServiceResponse retrievePicResult = 
                            PictureManager.retrieve(context, pictureToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, 0, -1);
                    if(retrievePicResult != null && retrievePicResult.isSuccess()) {
                        trip.setListOfPictures(retrievePicResult.getListOfPictures());
                    }
               }
                
            } catch(IllegalArgumentException iae) {
                iae.printStackTrace();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    private static ServiceResponse createOnLocal(
            Context context, 
            Trip tripToCreate,
            boolean isFromSync) {
        ServiceResponse result = null;
        try {
            boolean isFreshRecord = false;
            String tripID = tripToCreate.getId();
            if(ValidationUtil.isEmpty(tripID)) {
                isFreshRecord = true;
                
                //Set input with defaults:
                tripID = CommonUtil.generateID(tripToCreate.getUserID());
                tripToCreate.setId(tripID);
                tripToCreate.setStatus(TripStatusEnum.ACTIVE);
                tripToCreate.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
                tripToCreate.setUpdated(tripToCreate.getCreated());

            } //Else: A copy from server.

            //Execute:
            TripDB.create(context, tripToCreate);
            
            //Create Place(s):
            if(isFromSync) {
                PlaceManager.sync(context, tripToCreate.getListOfPlaces());
            } else {
                //Create places on local:
                if(tripToCreate.getListOfPlaces() != null
                        && tripToCreate.getListOfPlaces().size() > 0) {
                    List<Place> listOfPlaces = tripToCreate.getListOfPlaces();
                    for(Place placeToCreate : listOfPlaces) {
                        //If new fresh Trip, set required fields: 
                        if(isFreshRecord) {
                            placeToCreate.setUserID(tripToCreate.getUserID());
                            placeToCreate.setTripID(tripID);
                            placeToCreate.setId(CommonUtil.generateID(tripID));
                            placeToCreate.setCreated(tripToCreate.getCreated());
                            placeToCreate.setUpdated(tripToCreate.getCreated());
                        }
                        
                        //Call direct to DB, and not manager to avoid unwanted logic:
                        PlaceDB.create(context, placeToCreate);
                    }
                }
            }
            
            //Create Picture(s)
            if(isFromSync) {
                PictureManager.sync(context, tripToCreate.getListOfPictures());
            } else {
                //Note (7/2014): 
                //Current solution does not enable adding new picture at the same time as
                //creating new Trip
            }
            
            //Enrichment user data (here is for local only since the server would do the same):
            if(tripToCreate.getPublish() == TripPublishEnum.NOT_PUBLISH) {
                User userToRetrieve = new User();
                userToRetrieve.setUserId(tripToCreate.getUserID());
                User userLogin = UserDB.retrieve(context, userToRetrieve);
                if(userLogin != null) {
                    tripToCreate.setUserFullname(userLogin.getFullname());
                    tripToCreate.setUsername(userLogin.getUsername());
                    tripToCreate.setUserImageName(userLogin.getProfileImageName());
                }
            }

            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.TRIP_CREATE);
            List<Trip> listOfTrips = new ArrayList<Trip>();
            listOfTrips.add(tripToCreate);
            result.setListOfTrips(listOfTrips);        
            
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    private static ServiceResponse updateOnLocal(
            Context context, 
            Trip tripToUpdate) {
        ServiceResponse result = null;
        try {
            //Set input with defaults:
            String updatedDate = tripToUpdate.getUpdated();
            if(ValidationUtil.isEmpty(updatedDate)) {
                updatedDate = DateUtil.getCurrentTimeWithGMTTimezone();
                tripToUpdate.setUpdated(updatedDate);
            }
            if(tripToUpdate.getStatus() == TripStatusEnum.COMPLETED) {
                if(ValidationUtil.isEmpty(tripToUpdate.getCompleted())) {
                    tripToUpdate.setCompleted(updatedDate);
                }
            }
            
            //Execute:
            TripDB.update(context, tripToUpdate);
            
            //Retrieve it back:
            List<Trip> tripUpdated = 
                    TripDB.retrieve(context, tripToUpdate, 
                                    ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID,
                                    null, null, null, 0, 1); 
            if(tripUpdated == null || tripUpdated.size() == 0) {
                result = CommonUtil.composeServiceResponseError(
                        ServiceTypeEnum.TRIP_UPDATE, 
                        ServiceErrorEnum.TRIP_SERVICE_TRIP_NOT_FOUND_ERROR, 
                        "Trip Update error: Cannot find trip after update!");
            } else {
                tripEnrichmentOnLocal(context, tripUpdated);
                result = CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.TRIP_UPDATE);
                result.setListOfTrips(tripUpdated);
            }
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_UPDATE, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_UPDATE, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    private static ServiceResponse deleteOnLocal(
            Context context, 
            Trip tripToDelete) {
        Log.d(LOG_TAG, "deleteOnLocal() - Starting ... ");
        ServiceResponse result = null;
        try {
            //Delete pictures:
            Picture pictureToDelete = new Picture();
            pictureToDelete.setUserID(tripToDelete.getUserID());
            pictureToDelete.setTripID(tripToDelete.getId());
            result = PictureManager.delete(context, pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID, TripPublishEnum.NOT_PUBLISH);
            if(!result.isSuccess()) {
                return result;
            }
            
            //Delete places:
            Place placeToDelete = new Place();
            placeToDelete.setUserID(tripToDelete.getUserID());
            placeToDelete.setTripID(tripToDelete.getId());
            result = PlaceManager.delete(context, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_TRIPID, TripPublishEnum.NOT_PUBLISH);
            if(!result.isSuccess()) {
                return result;
            }
                
            TripDB.delete(context, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            result = CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            Log.d(LOG_TAG, "deleteOnLocal() - Completed.");
            
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_DELETE_BY_TRIPID, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_DELETE_BY_TRIPID, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }        
        return result;
    }
    
}
