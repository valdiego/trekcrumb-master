package com.trekcrumb.android.business;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.trekcrumb.android.otherimpl.DiskLruCache;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.SingletonFactory;
 
/**
 * A manager and wrapper class to access DiskLruCache. It handles disk caching of objects (such as 
 * bitmaps) including create, open, add to, get from, and clear the cache.
 * 
 * @see com.trekcrumb.android.otherimpl.DiskLruCache
 * 
 * @author Val Triadi
 */
public class CacheDiskManager {
    private static final String TAG_LOGGER = "CacheDiskManager";
    private static final int DISK_CACHE_INDEX = 0;
    private static final Object DISK_CACHE_LOCK = new Object();

    //These static variables are shared with multiple threads
    private static DiskLruCache mDiskLruCache;
    private static File mCacheDirectory;
    private static boolean mDiskCacheStarting = true;

    /**
     * Either initializes the CacheDisk if it has not been done so, or open it if it is closed.
     */
    public static void init(final Context context) {
        synchronized (DISK_CACHE_LOCK) {
            if(mDiskLruCache == null || mDiskLruCache.isClosed()) {
                Log.d(TAG_LOGGER, "init() - Disk cache is either NULL or CLOSED. Initialize it ...");
                if(mCacheDirectory == null) {
                    mCacheDirectory = new File(AndroidConstants.APP_LOCAL_DIR_CACHE);
                }
                if(!mCacheDirectory.exists()) {
                    mCacheDirectory.mkdirs();
                }
                try {
                    int cacheSize = calculateCacheSize(context, AndroidConstants.PICTURE_IMAGE_PIXEL_CONFIG);
                    mDiskLruCache = DiskLruCache.open(mCacheDirectory, 1, 1, cacheSize);
                    Log.d(TAG_LOGGER, "init() - Disk cache successfully initialized and READY!"
                            + " Directory [" + mCacheDirectory.getAbsolutePath() 
                            + "], allocated maximum size (bytes) [" + mDiskLruCache.getMaxSize() 
                            + "], total used size (bytes) [" + mDiskLruCache.size() + "]");
                } catch(Exception e) {
                    mDiskLruCache = null;
                    Log.e(TAG_LOGGER, "init() - ERROR while trying to initialize or open the disk cache!", e);
                }
            }
            
            mDiskCacheStarting = false;
            DISK_CACHE_LOCK.notifyAll(); // Wake any waiting threads
        }
    }
    
    /**
     * Adds a bitmap to disk cache.
     * @param key - Unique identifier for the bitmap to store
     * @param value - The bitmap to store
     */
    public static void add(final Context context, String key, Bitmap value) {
        if (key == null || value == null) {
            Log.e(TAG_LOGGER, "add() - Invalid input: either 'key' or 'value' cannot be NULL!");
            return;
        }
 
        init(context);
        synchronized (DISK_CACHE_LOCK) {
            if(mDiskLruCache != null) {
                key = key.toLowerCase();
                Log.d(TAG_LOGGER, "add() - Starting for key [" + key + "] with the value size (bytes): " + value.getByteCount());
                DiskLruCache.Snapshot snapshot = null;
                OutputStream outStream = null;
                try {
                    snapshot = mDiskLruCache.get(key);
                    if(snapshot == null) {
                        final DiskLruCache.Editor editor = mDiskLruCache.edit(key);
                        if (editor != null) {
                            outStream = editor.newOutputStream(DISK_CACHE_INDEX);
                            boolean isSuccess = value.compress(AndroidConstants.PICTURE_FORMAT, AndroidConstants.PICTURE_COMPRESS_QUALITY_75, outStream);
                            if(isSuccess) {
                                outStream.flush();
                                mDiskLruCache.flush();
                                editor.commit();
                            }
                        }
                    } else {
                        snapshot.getInputStream(DISK_CACHE_INDEX).close();
                    }
                    Log.d(TAG_LOGGER, "add() - Completed for key [" + key + "]. Total disk cache used (bytes): " + mDiskLruCache.size());

                } catch (Exception e) {
                    Log.e(TAG_LOGGER, "add() - ERROR during operation with message: " + e.getMessage(), e);
                } finally {
                    if(outStream != null) {
                        try {
                            outStream.close();
                        } catch (IOException e) { 
                            //Do nothing
                        }
                    }
                    if(snapshot != null ) {
                        snapshot.close();
                    }
                }
            }
        }
    }
 
    /**
     * Gets bitmap from disk cache.
     *
     * @param key Unique identifier for which item to get
     * @return The bitmap if found in cache, null otherwise
     */
    public static Bitmap get(final Context context, String key) {
        Bitmap bitmap = null;
        
        init(context);
        synchronized (DISK_CACHE_LOCK) {
            while (mDiskCacheStarting) {
                try {
                    DISK_CACHE_LOCK.wait();
                } catch (InterruptedException e) {}
            }
            
            if (mDiskLruCache != null) {
                key = key.toLowerCase();
                InputStream inStream = null;
                try {
                    final DiskLruCache.Snapshot snapshot = mDiskLruCache.get(key);
                    if (snapshot != null) {
                        inStream = snapshot.getInputStream(DISK_CACHE_INDEX);
                        if (inStream != null) {
                            bitmap = BitmapFactory.decodeStream(inStream);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG_LOGGER, "get() - ERROR during operation with message: " + e.getMessage(), e);
                } finally {
                    if(inStream != null) {
                        try {
                            inStream.close();
                        } catch (IOException e) { 
                            //Do nothing
                        }
                    }
                }
            }
            
            return bitmap;
        }
    }
 
    /**
     * Flushes the disk cache.
     */
    public static void flush() {
        synchronized (DISK_CACHE_LOCK) {
            if (mDiskLruCache != null) {
                try {
                    mDiskLruCache.flush();
                } catch (IOException e) {
                    Log.w(TAG_LOGGER, "flush() - ERROR during operation with message: " + e.getMessage());
                }
            }
        }
    }
 
    /**
     * Clears disk cache from all entities, and refreshes to a clean state.
     */
    public static void clean(final Context context) {
        synchronized (DISK_CACHE_LOCK) {
            if(mDiskLruCache != null) {
                if(mDiskLruCache.isClosed()) {
                    //Open access:
                    init(context);
                }
                
                //Clean and refresh:
                try {
                    mDiskLruCache.delete();
                } catch (IOException e) {
                    Log.w(TAG_LOGGER, "clean() - ERROR while cleaning disk cache with message: " + e.getMessage());
                }
                mDiskLruCache = null;
                mDiskCacheStarting = true;
                init(context);
            }
        }
    }
    
    /**
     * Closes the disk cache.
     */
    public static void close() {
        synchronized (DISK_CACHE_LOCK) {
            if (mDiskLruCache != null
                    && !mDiskLruCache.isClosed()) {
                try {
                    mDiskLruCache.close();
                    mDiskLruCache = null;
                } catch (IOException e) {
                    Log.w(TAG_LOGGER, "close() - ERROR while closing disk cache with message: " + e.getMessage(), e);
                }
            }
        }
    }
    
    public static void cleanAndClose(final Context context) {
        clean(context);
        close();
    }
    
    /*
     * Calculates appropriate cache size in bytes to be reserved for the disk cache.
     * image gallery caching mechanism.
     * Note: 
     * 1. The implementation will only allow maximum 20 images stored at a time with maximum size
     *    of 'picture image thumbnail for view' for each.
     * 2. The calculation depends on the config parameter which specifies the quality of the bitmap image
     *    (number of bytes per pixel)
     * 
     * @return The byte size of caching to be reserved.
     */
    private static int calculateCacheSize(final Context context, Config config) {
        int numOfBytesPerPixel = 1;
        if (config == Config.ARGB_8888) {
            numOfBytesPerPixel = 4;
        } else if (config == Config.RGB_565) {
            numOfBytesPerPixel = 2;
        } else if (config == Config.ARGB_4444) {
            numOfBytesPerPixel = 2;
        } else if (config == Config.ALPHA_8) {
            numOfBytesPerPixel = 1;
        }
        
        //TODO: Calculate device available disk space and put into considerstion ...
        
        //For cache, we will store 20 of regular images at a time:
        int imgSizeToStore = SingletonFactory.getPictureThumbnailViewSize(context);
        int cacheSize = 20 * (imgSizeToStore * imgSizeToStore * numOfBytesPerPixel);
        return cacheSize;
    }
    
}



