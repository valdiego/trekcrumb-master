package com.trekcrumb.android.business;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.trekcrumb.android.R;
import com.trekcrumb.android.bean.ImageCacheBean;
import com.trekcrumb.android.database.PictureDB;
import com.trekcrumb.android.database.TripDB;
import com.trekcrumb.android.thread.PictureDownloadThreadPool;
import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.android.utility.DeviceUtil;
import com.trekcrumb.android.utility.FileUtil;
import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.android.utility.SingletonFactory;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceStatusEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.PictureWSClient;
import com.trekcrumb.webserviceclient.UserWSClient;

public class PictureManager {
    private static final String LOG_TAG = "PictureManager";
    
    private PictureManager() {};
    
    /**
     * Creates a new Picture for the Trip.
     * NOTE:
     * 1. 6/2013: The operation will store only image name, NOT store image bytes (as BLOB) 
     *    into the DB because it becomes too big for Android when it retrieves the BLOB because the SQLite get() 
     *    API has maximum data 1MB. Therefore, the solution is to store the image bytes into filesystem (SD card).
     *
     * 2. The param pictureToCreate must contain imageURL which is the local location of the image. 
     *    The data imageURL usage is only used during local processing only, and is abandoned during
     *    client/server service interactions.
     * 
     * @param tripSelected - If the Trip is NOT_PUBLISH, then it saves on local DB only. If PUBLISH, it will
     *                       saves on remote server then copy the record into local DB.
     * @param pictureToCreate - Contains the TripID where this Picture belongs to, and all other data.
     * 
     */
    public static ServiceResponse create(
            Context context,
            Picture pictureToCreate,
            boolean isNewPicByCamera,
            TripPublishEnum isPublishOrNot) {
        if(pictureToCreate == null
                || ValidationUtil.isEmpty(pictureToCreate.getUserID())
                || ValidationUtil.isEmpty(pictureToCreate.getTripID())
                || pictureToCreate.getImageURL() == null) {
            String errMsg = "Picture Create error: Picture bean to contain User ID, Trip ID and the source image URL is required!";
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,errMsg);
        }
        
        if(!DeviceUtil.isSDCardAccessible()) {
            String errMsg = "Picture Create error: Device external SD card is not accessible or not properly mounted!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, errMsg);
        }
        
        //Get the source image first and put into memory:
        String imgSourceURL = pictureToCreate.getImageURL();
        Bitmap imgBitmapToSave = 
                ImageUtil.decodePictureImageIntoBitmap(null, imgSourceURL, CommonConstants.PICTURE_MAX_SIZE_PIXEL_SAVE, false, false);
        if(imgBitmapToSave == null) {
            String errMsg = "No image found given source location: " + imgSourceURL;
            Log.e(LOG_TAG, "Picture Create error: " + errMsg);
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
        }
        
        if(isPublishOrNot == TripPublishEnum.PUBLISH) {
            //Remote server:
            byte[] imgBytes = null;
            try {
                imgBytes = ImageUtil.decodeBitmapToBytes(imgBitmapToSave);
                pictureToCreate.setImageBytes(imgBytes);
                
                ServiceResponse wsResult = PictureWSClient.create(pictureToCreate); 
                if(wsResult == null
                        || !wsResult.isSuccess()
                        || wsResult.getListOfPictures() == null 
                        || wsResult.getListOfPictures().size() == 0) {
                    return wsResult;
                }

                Picture picCreated = wsResult.getListOfPictures().get(0);
                
                //Set picture to create bean with server's default values:
                pictureToCreate.setId(picCreated.getId());
                pictureToCreate.setCreated(picCreated.getCreated());
                pictureToCreate.setUpdated(picCreated.getUpdated());
                pictureToCreate.setImageName(picCreated.getImageName());

            } catch(Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
            } finally {
                //Release resources to avoid "failed binder transaction" (out of memory) error:
                imgBytes = null;
                pictureToCreate.setImageBytes(null);
            }
        }
        
        //Local device:
        return createOnLocal(context, pictureToCreate, imgBitmapToSave, isNewPicByCamera, imgSourceURL);
    }
    
    /**
     * Sync Pictures from remote server as part of Trip sync operation.
     * NOTE: 
     * 1. 5/2014: During Trip sync(), if the returned synced Trip has pictures, the remote server
     *    will only transfer the image name and/or source URL without the actual image bytes to 
     *    avoid too big of data transfer and network bootleneck. This operation, therefore, 
     *    is to separately download each of pictures and save them into local device.
     * 
     * @param listOfPicturesToSync - List of Pictures from Server that contains image name and/or URL 
     *                               to download and then save into device.
     */
    public static ServiceResponse sync(
            Context context,
            List<Picture> listOfPicturesToSync) {
        if(listOfPicturesToSync == null
                || listOfPicturesToSync.size() == 0) {
            return CommonUtil.composeServiceResponseSuccess(null);
        }
        Log.d(LOG_TAG, "sync() - Starting ... ");
        
        if(!DeviceUtil.isSDCardAccessible()) {
            String errMsg = "Picture Sync error: Device external SD card is not accessible or properly mounted!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, errMsg);
        }

        try {
            for(Picture pictureToSync : listOfPicturesToSync) {
                ImageUtil.savePictureToSDCardAndDB(
                        context, pictureToSync, SingletonFactory.getImageNotAvailable(context));
                
                //Launch of separate thread to download the real picture:
                PictureDownloadThreadPool threadPool = PictureDownloadThreadPool.getInstance();
                threadPool.serve(pictureToSync);
            }
            Log.d(LOG_TAG, "sync() - Completed.");
        } catch(IllegalArgumentException iae) {
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Picture Sync error: " + e.getMessage());
        }
        return CommonUtil.composeServiceResponseSuccess(null);
    }
    
    /**
     * Publish Pictures from local device to remote server as part of Trip publish operation.
     * 
     * @param listOfPicturesToPublish - List of Pictures from Server that contains image name and/or URL 
     *                               to download and then save into device.
     */
    public static ServiceResponse publish(
            Context context,
            List<Picture> listOfPicturesToPublish) {
        if(listOfPicturesToPublish == null
                || listOfPicturesToPublish.size() == 0) {
            return CommonUtil.composeServiceResponseSuccess(null);
        }
        Log.d(LOG_TAG, "publish() - Starting ... ");
        
        Bitmap picBitmap = null;
        byte[] picBytes = null;
        ServiceResponse svcResponse = null;
        try {
            for(Picture pictureToPublish : listOfPicturesToPublish) {
                Log.d(LOG_TAG, "publish() - Uploading picture image to server: " + pictureToPublish.getImageName());
                picBitmap = ImageUtil.decodePictureImageIntoBitmap(
                        null, 
                        AndroidConstants.APP_LOCAL_DIR_PICTURES + pictureToPublish.getImageName(), 
                        CommonConstants.PICTURE_MAX_SIZE_PIXEL_SAVE, false, false);
                picBytes = ImageUtil.decodeBitmapToBytes(picBitmap);
                pictureToPublish.setImageBytes(picBytes);
                svcResponse = PictureWSClient.imageUpload(pictureToPublish);
                if(!svcResponse.isSuccess()) {
                    return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                            "Picture Publish error: " + svcResponse.getServiceError());
                }
                Log.d(LOG_TAG, "publish() - Upload image file successful!");
            }
            Log.d(LOG_TAG, "publish() - ALL Completed.");
        } catch(IllegalArgumentException iae) {
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Picture Publish error: " + e.getMessage());
        } finally {
            //Release resources:
            picBytes = null;
            picBitmap = null;
        }
        return CommonUtil.composeServiceResponseSuccess(null);
    }    
    
    /**
     * Retrieves Pictures from device local DB.
     * 
     * NOTE:
     * 1. 6/10/2013: The management will return picture bean with imageURL, instead of imageBytes. 
     * There is a limit on Android ImageView, Bundle, etc. to only pass objects <= 1MB around. 
     * Anything bigger, you will start getting error: 'Failed Binder Transaction'
     */
    public static ServiceResponse retrieve(
            Context context,
            Picture pictureToRetrieve,
            ServiceTypeEnum retrieveBy,
            int offset,
            int numOfRows) {
        ServiceResponse result = null;
        
        if(!DeviceUtil.isSDCardAccessible()) {
            String errMsg = "Picture Retrieve error: Device external SD card is not accessible or properly mounted!";
            Log.e(LOG_TAG, errMsg);
            return CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, errMsg);
        }

        try {
            List<Picture> listOfPictures =  PictureDB.retrieve(context, pictureToRetrieve, retrieveBy, offset, numOfRows);
            result = CommonUtil.composeServiceResponseSuccess(retrieveBy);
            result.setListOfPictures(listOfPictures);
            
        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, iae.getMessage());
            result = CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            result = CommonUtil.composeServiceResponseError(retrieveBy, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    public static ServiceResponse update(
            Context context,
            Picture pictureToUpdate,
            TripPublishEnum tripPublishStatus) {
        if(pictureToUpdate == null
                || ValidationUtil.isEmpty(pictureToUpdate.getUserID())
                || ValidationUtil.isEmpty(pictureToUpdate.getTripID())
                || ValidationUtil.isEmpty(pictureToUpdate.getId())) {
            return CommonUtil.composeServiceResponseError(
                    null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Picture Update error: Missing required data: User ID, Trip ID and Picture ID!");
        }
        
        if(tripPublishStatus == TripPublishEnum.PUBLISH) {
            //Remote Server:
            ServiceResponse wsResult = PictureWSClient.update(pictureToUpdate); 
            if(wsResult == null || !wsResult.isSuccess()) {
                return wsResult;
            }
            
            if(wsResult.getListOfPictures() != null && wsResult.getListOfPictures().size() > 0) {
                //Just get the updated date (imageURL is different b/w local vs. remote)
                Picture pictureUpdatedOnServer = wsResult.getListOfPictures().get(0);
                pictureToUpdate.setUpdated(pictureUpdatedOnServer.getUpdated());
            }
        }

        //Local device:
        return updateOnLocal(context, pictureToUpdate);
    }
    
    /**
     * Deletes a Picture or Pictures depending the input params.
     * @param pictureToDelete - Picture bean containing data for the entity to delete. It
     *                          must contains pictureID, TripID and imageURL.
     * @param tripPublishStatus - If NOT_PUBLISH, then it deletes on local DB only. If PUBLISH, it will
     *                            delete on remote server then delete the record into local DB.
     */
    public static ServiceResponse delete(
            Context context, 
            Picture pictureToDelete,
            ServiceTypeEnum deleteBy,
            TripPublishEnum tripPublishStatus) {
        //Set input with defaults:
        if(ValidationUtil.isEmpty(pictureToDelete.getUpdated())) {
            pictureToDelete.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        }

        if(tripPublishStatus == TripPublishEnum.PUBLISH) {
            //Remote server
            //NOTE: Only apply for delete by pictureID. If delete by USERID or TRIPID,
            //      the server will take care of its own.
            ServiceResponse wsResult = 
                    PictureWSClient.delete(pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID); 
            if(wsResult == null
                    || !wsResult.isSuccess()) {
                return wsResult;
            }
        }
        
        //Local device:
        return deleteOnLocal(context, pictureToDelete, deleteBy);
    }
    
    /**
     * Retrieves Picture's image. The image is either the original image, or its thumbnail format 
     * (e.g., for gallery use etc.).
     * 
     * @param pictureToRead - Contains the image name to retrieve.
     * @param isUserLoginTrip
     * @param isThumbnail - The value is 'true' if this is for thumbnail image which is smaller size
     *                      than the original size. Otherwise, it must be false.
     * 
     * @return ImageCacheBean containing indication if operation is success or fail, and the 
     *         bitmap content.
     */
    public static ImageCacheBean retrieveImage(
            Context context,
            final Picture pictureToRead,
            boolean isUserLoginTrip,
            boolean isThumbnail) {
        Log.d(LOG_TAG, "retrieveImage() - Starting for Picture's imageName: " + pictureToRead.getImageName());
        ImageCacheBean result = null;
        String imgName = pictureToRead.getImageName();
        
        if(isThumbnail) {
            //Retrieve image as thumbnail:
            Bitmap imgBitmap = null;
            if(isUserLoginTrip) {
                //UserLogin: Read from regular stored thumbnail images on local device:
                imgBitmap = ImageUtil.decodePictureImageIntoBitmap(
                        null, AndroidConstants.APP_LOCAL_DIR_PICTURES + imgName, 
                        SingletonFactory.getPictureThumbnailViewSize(context), 
                        false, true);
            } else {
                //UserOther: Read from disk cache:
                imgBitmap = CacheDiskManager.get(context, imgName);
            }
            if(imgBitmap != null) {
                //Image is found:
                Log.d(LOG_TAG, "retrieveImage() - Found stored bitmap for image name [" + imgName + "]");
                result = new ImageCacheBean();
                result.setServiceStatus(ServiceStatusEnum.STATUS_SUCCESS);
                result.setKey(imgName);
                result.setValue(imgBitmap);
                return result;
            }
            
            //Else:
            result = retrieveImageFromServer(context, pictureToRead, true);
            if(!isUserLoginTrip
                    && result.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS) {
                //Put into disk cache (only for UserOther trips and a success):
                CacheDiskManager.add(context, imgName, result.getValue());
            }
            return result;
        } //END-IF isThumbnail
        
        //ELSE: Retrieve image as regular (not thumbnail):
        if(isUserLoginTrip) {
            //UserLogin: Read from stored images on local device, if any:
            Bitmap imgBitmap = ImageUtil.decodePictureImageIntoBitmap(
                    null, AndroidConstants.APP_LOCAL_DIR_PICTURES + imgName,
                    SingletonFactory.getPictureViewSize(context), 
                    true, false);
            if(imgBitmap != null) {
                result = new ImageCacheBean();
                result.setServiceStatus(ServiceStatusEnum.STATUS_SUCCESS);
                result.setKey(imgName);
                result.setValue(imgBitmap);
                return result;
            }
        }
        
        /*
         * Else: UserLogin does not have the image in question so must read from server, OR
         * this is for UserOther which always read from server:
         */
        result = retrieveImageFromServer(context, pictureToRead, false);
        if(result.getServiceStatus() == ServiceStatusEnum.STATUS_SUCCESS
                && isUserLoginTrip) {
            //UserLogin (only): Save to SDcard:
            try {
                ImageUtil.decodeBitmapToFile(result.getValue(), imgName);
            } catch(Exception e) {
                Log.e(LOG_TAG, "retrieveImage() - ERROR! Downloading image file for picture [" 
                        + imgName + "] was successful, but saving into local drive had error: " + e.getMessage());
                e.printStackTrace();
            }
        }

        return result;
    }    

    /**
     * Retrieves user profile image.
     * 
     * @param profileImageName - The name of image file to retrieve
     * @param isUserLoginProfile - Whether this is for user login profile ('true'), or
     *                             other user profile ('false').
     * @param isThumbnail - True if to display as a thumbnail (e.g., on list page) and the returned
     *                      bitmap will be thumbnail sized. False if to display as a detail image 
     *                      (e.g., on user detail page etc.) where the returned bitmap will be
     *                      regular sized.
     *                      
     * @return The profile image in bitmap. NULL if image not found, or there is error in the 
     *         operation.
     */
    public static Bitmap retrieveProfileImage(
            Context context,
            String profileImageName,
            boolean isUserLoginProfile,
            boolean isThumbnail) {
        Bitmap imgBitmap = null;
        
        String filename = null;
        int imgSize = -1;
        if(isThumbnail) {
            filename = profileImageName + CommonConstants.PICTURE_FILENAME_SUFFIX_THUMBNAIL;
            imgSize = SingletonFactory.getPictureProfileThumbnailViewSize(context);
        } else {
            filename = profileImageName;
            imgSize = SingletonFactory.getPictureProfileViewSize(context);
        }
        
        if(isUserLoginProfile) {
            /*
             * UserLogin: 
             * 1. Find image from app user local repository
             * 1.1. IF found: return it
             * 1.2. Else: Retrieve it from server. Upon success, save it to app user local repository
             */
            imgBitmap = ImageUtil.decodeProfileImageIntoBitmap(
                    null, AndroidConstants.APP_LOCAL_DIR_PICTURES +  filename, imgSize);
            if(imgBitmap == null) {
                imgBitmap = retrieveProfileImageFromServer(context, profileImageName, imgSize);
                if(imgBitmap != null) {
                    //Save to SDcard (only for userLogin's):
                    try {
                        ImageUtil.decodeBitmapToFile(imgBitmap, filename);
                    } catch(Exception e) {
                        //Just log it:
                        Log.e(LOG_TAG, "retrieveUserProfileImage() - ERROR! Downloading user profile image [" 
                                + profileImageName + "] was successful, but saving into local drive had error: " + e.getMessage(), e);
                    }
                }
            }
            return imgBitmap;

        } else {
            /*
             * UserOther: 
             * 1. Find image from cache disk repository
             * 1.1. IF found: return it
             * 1.2. Else: Retrieve it from server. Upon success, save it to cache disk
             */
            imgBitmap = CacheDiskManager.get(context, filename);
            if(imgBitmap == null) {
                imgBitmap = retrieveProfileImageFromServer(context, profileImageName, imgSize);
                if(imgBitmap != null) {
                    //Put into disk cache (only for non userLogin trips):
                    CacheDiskManager.add(context, filename, imgBitmap);
                }
            }
            return imgBitmap;
        }
    }

    private static ServiceResponse createOnLocal(
            Context context,
            Picture pictureToCreate,
            Bitmap imgBitmap,
            boolean isNewPicByCamera,
            String imgSourceURL) {
        ServiceResponse result = null;
        try {
            if(ValidationUtil.isEmpty(pictureToCreate.getId())) {
                //It is new fresh record:
                pictureToCreate.setId(CommonUtil.generateID(pictureToCreate.getTripID()));
                pictureToCreate.setImageName(CommonUtil.generatePictureFilename(pictureToCreate.getTripID()));
                pictureToCreate.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
                pictureToCreate.setUpdated(pictureToCreate.getCreated());

            } //Else: A copy from server.
            
            ImageUtil.savePictureToSDCardAndDB(context, pictureToCreate, imgBitmap);
            
            //Release resources to avoid "failed binder transaction" (out of memory) error:
            imgBitmap = null;
            
            //Delete temporary picture by camera:
            if(isNewPicByCamera) {
                FileUtil.deleteFile(imgSourceURL);
            }
            
            //Update Trip:
            Trip tripToUpdate =  new Trip();
            tripToUpdate.setId(pictureToCreate.getTripID());
            tripToUpdate.setUpdated(pictureToCreate.getCreated());
            TripDB.update(context, tripToUpdate);
 
            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(null);
            List<Picture> listOfCreatedPics = new ArrayList<Picture>();
            listOfCreatedPics.add(pictureToCreate);
            result.setListOfPictures(listOfCreatedPics);

        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, "Picture Create error: " + iae.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, "Picture Create error: " + e.getMessage());
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    private static ServiceResponse updateOnLocal(
            Context context, 
            Picture pictureToUpdate) {
        ServiceResponse result = null;
        
        //Set input with defaults:
        if(ValidationUtil.isEmpty(pictureToUpdate.getUpdated())) {
            pictureToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        }

        try {
            PictureDB.update(context, pictureToUpdate);
            
            //Update Trip:
            Trip tripToUpdate =  new Trip();
            tripToUpdate.setId(pictureToUpdate.getTripID());
            tripToUpdate.setUpdated(pictureToUpdate.getUpdated());
            TripDB.update(context, tripToUpdate);
            
            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(null);
            List<Picture> listOfPics = 
                    PictureDB.retrieve(context, pictureToUpdate, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, 0, -1); 
            if(listOfPics == null || listOfPics.size() == 0) {
                Log.e(LOG_TAG, "Picture Update error: After update, cannot find any Picture for TripID [" + pictureToUpdate.getTripID() + "]");
            } 
            result.setListOfPictures(listOfPics);

        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, iae.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            result = CommonUtil.composeServiceResponseError(null, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    /*
     * Deletes pictures on local device.
     * 1. If there are pictures to delete, the operation will remove records from DB and from directory files
     * 2. If deleteBy USERID or TRIPID, there is NO need to update Trip's UPDATED date
     * 3. if deleteBy USERID or TRIPID, there is NO need to return the latest list of pictures
     */
    private static ServiceResponse deleteOnLocal(
            Context context, 
            Picture pictureToDelete,
            ServiceTypeEnum deleteBy) {
        ServiceResponse result = null;
        List<Picture> listOfPicturesToDelete = null;
        try {
            
            if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_USERID) {
                listOfPicturesToDelete = PictureDB.retrieve(context, pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID, 0, -1);
            } else if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID) {
               listOfPicturesToDelete = PictureDB.retrieve(context, pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, 0, -1);
            } else {
                listOfPicturesToDelete = PictureDB.retrieve(context, pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, 0, -1);
            }
            if(listOfPicturesToDelete == null || listOfPicturesToDelete.size() == 0) {
                //Nothing to delete:
                return CommonUtil.composeServiceResponseSuccess(deleteBy);
            }
            
            //Delete on DB:
            PictureDB.delete(context, pictureToDelete, deleteBy);
            
            //Delete on device SD card:
            for(Picture picture : listOfPicturesToDelete) {
                FileUtil.deleteFile(AndroidConstants.APP_LOCAL_DIR_PICTURES + picture.getImageName());
            }
            
            //Update Trip:
            if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID) {
                Trip tripToUpdate =  new Trip();
                tripToUpdate.setId(pictureToDelete.getTripID());
                tripToUpdate.setUpdated(pictureToDelete.getUpdated());
                TripDB.update(context, tripToUpdate);
            }

            //Compose response:
            result = CommonUtil.composeServiceResponseSuccess(deleteBy);
            if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID) {
                List<Picture> listOfPictures = 
                        PictureDB.retrieve(context, pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, 0, -1); 
                result.setListOfPictures(listOfPictures);
            }
            
        } catch(IllegalArgumentException iae) {
            Log.e(LOG_TAG, iae.getMessage());
            result = CommonUtil.composeServiceResponseError(deleteBy, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            Log.e(LOG_TAG, e.getMessage());
            result = CommonUtil.composeServiceResponseError(deleteBy, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    private static ImageCacheBean retrieveImageFromServer(
            Context context,
            Picture pictureToRead,
            boolean isForImageThumbnail) {
        Log.d(LOG_TAG, "retrieveImageFromServer() - Starting downloading Picture image: " + pictureToRead.getImageName());
        ImageCacheBean result = null;
        byte[] imgBytes = null;
        ServiceResponse svcResponse = null;
        try {
            svcResponse = PictureWSClient.imageDownload(pictureToRead);
            if(!svcResponse.isSuccess()) {
                //Handle failures:
                Log.e(LOG_TAG, "retrieveImageFromServer() - ERROR: " + svcResponse.getServiceError());
                result = new ImageCacheBean();
                result.setServiceStatus(ServiceStatusEnum.STATUS_FAIL);
                result.setKey(pictureToRead.getImageName());
                result.setValue(SingletonFactory.getImageNotAvailable(context));
                
                if(svcResponse.getServiceError() != null) {
                    if(svcResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.SYSTEM_NETWORK_NOT_AVAILABLE_ERROR) {
                        result.setErrorMsg(context.getResources().getString(R.string.error_system_network_NotAvailable));
                    } else if(svcResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR) {
                        result.setErrorMsg(context.getResources().getString(R.string.error_system_server_NotAvailable));
                    } else if(svcResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND) {
                        result.setErrorMsg(context.getResources().getString(R.string.error_picture_image_notFound));
                    } else {
                        result.setErrorMsg(context.getResources().getString(R.string.error_system_commonError));
                    }
                } else {
                    result.setErrorMsg(context.getResources().getString(R.string.error_system_commonError));
                }
                return result;
                
            }
            if(svcResponse.getListOfPictures() == null
                    || svcResponse.getListOfPictures().get(0) == null
                    || svcResponse.getListOfPictures().get(0).getImageBytes() == null) {
                Log.e(LOG_TAG, "retrieveImageFromServer() - ERROR: Image file NOT FOUND on server!");
                result = new ImageCacheBean();
                result.setServiceStatus(ServiceStatusEnum.STATUS_FAIL);
                result.setKey(pictureToRead.getImageName());
                result.setErrorMsg(context.getResources().getString(R.string.error_picture_image_notFound));
                result.setValue(SingletonFactory.getImageNotAvailable(context));
                return result;
            }

            //Success:
            result = new ImageCacheBean();
            result.setServiceStatus(ServiceStatusEnum.STATUS_SUCCESS);
            result.setKey(pictureToRead.getImageName());

            imgBytes = svcResponse.getListOfPictures().get(0).getImageBytes();
            Bitmap imgBitmap = null;
            if(isForImageThumbnail) {
                imgBitmap = ImageUtil.decodePictureImageIntoBitmap(
                        imgBytes, null,
                        SingletonFactory.getPictureThumbnailViewSize(context), 
                        true, isForImageThumbnail);
            } else {
                imgBitmap = ImageUtil.decodePictureImageIntoBitmap(
                        imgBytes, null,
                        SingletonFactory.getPictureViewSize(context), 
                        true, isForImageThumbnail);
            }
            result.setValue(imgBitmap);
            return result;            
        } finally {
            //Cleanup resources:
            imgBytes = null;
            svcResponse = null;
        }
    }        

    private static Bitmap retrieveProfileImageFromServer(
            Context context,
            String profileImageName,
            int imgSize) {
        Log.d(LOG_TAG, "retrieveProfileImageFromServer() - Starting for profile imageName: " + profileImageName);
        ServiceResponse svcResponse = null;
        byte[] imgBytes = null;
        try {
            User userWithImageToDownload = new User();
            userWithImageToDownload.setProfileImageName(profileImageName);
            svcResponse = UserWSClient.userProfileImageDownload(userWithImageToDownload);
            if(!svcResponse.isSuccess()) {
                Log.e(LOG_TAG, "retrieveProfileImageFromServer() - ERROR: " + svcResponse.getServiceError());
                return null;
            }
            if(svcResponse.getListOfUsers() == null
                    || svcResponse.getListOfUsers().get(0) == null
                    || svcResponse.getListOfUsers().get(0).getProfileImageBytes() == null) {
                Log.e(LOG_TAG, "retrieveProfileImageFromServer() - ERROR: Image file NOT FOUND on server!");
                return null;
            }
    
            //Success:
            imgBytes = svcResponse.getListOfUsers().get(0).getProfileImageBytes();
            return ImageUtil.decodeProfileImageIntoBitmap(imgBytes, null, imgSize);
        } finally {
            //Cleanup resources:
            imgBytes = null;
            svcResponse = null;
        }
    }


}
