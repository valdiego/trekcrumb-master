package com.trekcrumb.android.business;

import android.content.Context;

import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.FavoriteWSClient;

public class FavoriteManager {

    public static ServiceResponse retrieve(
            Context context,
            Trip trip,
            User user,
            ServiceTypeEnum retrieveBy,
            int offset) {
        //Validation:
        if(retrieveBy == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_TRIP) {
            if(trip == null
                    || ValidationUtil.isEmpty(trip.getId())) {
                return CommonUtil.composeServiceResponseError(
                        retrieveBy,
                        ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                        "Favorite Retrieve by Trip error: Missing required TripID!");
            }
        } else if(retrieveBy == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_USER) {
            if(user == null
                    || ValidationUtil.isEmpty(user.getUsername())) {
                return CommonUtil.composeServiceResponseError(
                        retrieveBy,
                        ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                        "Favorite Retrieve by User error: Missing required Username!");
            }
        } else {
            return CommonUtil.composeServiceResponseError(
                    retrieveBy,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Favorite Retrieve error: Unknown retrieve by type [" + retrieveBy + "]!");
        }
        
        //All is good:
        ServiceResponse svcResponse = null;
        if(retrieveBy == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_TRIP) {
            svcResponse = FavoriteWSClient.retrieveByTrip(trip, null, offset, CommonConstants.RECORDS_NUMBER_MAX);
        } else {
            svcResponse = FavoriteWSClient.retrieveByUser(user, null, offset, CommonConstants.RECORDS_NUMBER_MAX);
        }
        return svcResponse;
    }
    
}
