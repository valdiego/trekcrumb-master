package com.trekcrumb.android.bean;

import java.io.Serializable;

import android.graphics.Bitmap;

import com.trekcrumb.common.enums.ServiceStatusEnum;

public class ImageCacheBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String key;
    private Bitmap value;
    private ServiceStatusEnum serviceStatus;
    private String errorMsg;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("ImageRetrieveResult: ");
        strBld.append("key [" + key + "], ");
        strBld.append("value [" + value + "]");
        strBld.append(serviceStatus + ", ");
        strBld.append("errorMsg [" + errorMsg + "], ");
        return strBld.toString();
    }    
    
    public ServiceStatusEnum getServiceStatus() {
        return serviceStatus;
    }
    public void setServiceStatus(ServiceStatusEnum serviceStatus) {
        this.serviceStatus = serviceStatus;
    }
    public String getErrorMsg() {
        return errorMsg;
    }
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    public Bitmap getValue() {
        return value;
    }
    public void setValue(Bitmap value) {
        this.value = value;
    }
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    
    
}
