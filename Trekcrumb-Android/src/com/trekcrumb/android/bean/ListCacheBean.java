package com.trekcrumb.android.bean;

import java.io.Serializable;
import java.util.List;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.ServiceStatusEnum;

public class ListCacheBean implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String key;
    private ServiceStatusEnum serviceStatus;
    private String errorMsg;
    private List<Trip> valueTripList;
    private List<Comment> valueCommentList;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("ListCacheBean: ");
        strBld.append("key [" + key + "], ");
        strBld.append(serviceStatus + ", ");
        strBld.append("errorMsg [" + errorMsg + "], ");
        return strBld.toString();
    }    
    
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public ServiceStatusEnum getServiceStatus() {
        return serviceStatus;
    }
    public void setServiceStatus(ServiceStatusEnum serviceStatus) {
        this.serviceStatus = serviceStatus;
    }
    public String getErrorMsg() {
        return errorMsg;
    }
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<Trip> getValueTripList() {
        return valueTripList;
    }

    public void setValueTripList(List<Trip> valueTripList) {
        this.valueTripList = valueTripList;
    }

    public List<Comment> getValueCommentList() {
        return valueCommentList;
    }

    public void setValueCommentList(List<Comment> valueCommentList) {
        this.valueCommentList = valueCommentList;
    }


    
    
}
