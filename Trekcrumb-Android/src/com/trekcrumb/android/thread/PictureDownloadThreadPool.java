package com.trekcrumb.android.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.trekcrumb.android.utility.AndroidConstants;
import com.trekcrumb.common.bean.Picture;

/**
 * A singleton class to manage thread pool when it launch PictureDownloadRunnable class as separate threads
 * so that the number of threads that are running are manageable.
 * 
 * The maximum number of running PictureDownloadRunnable threads are set in AndroidCommon.PICTURE_DOWNLOAD_THREAD_MAX.
 * If the numbers of pictures to download are bigger than this, they will be put in a List to wait for
 * their turn.
 * 
 * @author Val Triadi
 * @since 5/2014
 */
public class PictureDownloadThreadPool {
    private static PictureDownloadThreadPool mThreadPool;
    private static ExecutorService mExecutorSvc;

    private PictureDownloadThreadPool() { 
        mExecutorSvc = Executors.newFixedThreadPool(AndroidConstants.PICTURE_DOWNLOAD_THREAD_MAX);
    }
    
    public static PictureDownloadThreadPool getInstance() {
        if(mThreadPool == null) {
            mThreadPool = new PictureDownloadThreadPool();
        }
        return mThreadPool;
    }
    
    public void serve(Picture pictureToDownload) {
        Thread thread = new PictureDownloadThread(pictureToDownload);
        mExecutorSvc.execute(thread);
    }

}
