package com.trekcrumb.android.thread;

import android.graphics.Bitmap;
import android.util.Log;

import com.trekcrumb.android.utility.ImageUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.webserviceclient.PictureWSClient;

/**
 * A worker class - to download image file as a separate thread.
 * @author Val Triadi
 * @since 5/2014
 *
 */
public class PictureDownloadThread extends Thread {
    private static final String LOG_TAG = "PictureDownloadThread";
    private Picture mPictureToDownload;
    
    public PictureDownloadThread(final Picture pictureToDownload){
        mPictureToDownload = pictureToDownload;
    }

    @Override
    public void run() {
        Log.d(LOG_TAG, "run() - Starting downloading Picture image: " + mPictureToDownload.getImageName());
        ServiceResponse svcResponse = PictureWSClient.imageDownload(mPictureToDownload);
        if(!svcResponse.isSuccess()) {
            Log.e(LOG_TAG, "run() - ERROR while downloading Picture image [" 
                    + mPictureToDownload.getImageName() + "] with error: " + svcResponse.getServiceError());
            return;
        }
        
        if(svcResponse.getListOfPictures() == null
                || svcResponse.getListOfPictures().get(0) == null
                || svcResponse.getListOfPictures().get(0).getImageBytes() == null) {
            Log.e(LOG_TAG, "run() - ERROR while downloading Picture image [" 
                    + mPictureToDownload.getImageName() + "] with error: NOT FOUND!");
            return;
        }
        
        //Save to SDcard:
        byte[] imgBytes = svcResponse.getListOfPictures().get(0).getImageBytes();
        Bitmap imgBitmap = ImageUtil.decodePictureImageIntoBitmap(
                imgBytes, null, CommonConstants.PICTURE_MAX_SIZE_PIXEL_SAVE, true, false);
        try {
            ImageUtil.decodeBitmapToFile(imgBitmap, mPictureToDownload.getImageName());
            Log.d(LOG_TAG, "run() - COMPLETED downloading and saving image file for picture: " + mPictureToDownload.getImageName());
            
        } catch(Exception e) {
            Log.e(LOG_TAG, "run() - Downloading image file for picture [" 
                    + mPictureToDownload.getImageName() 
                    + "] was successful, but saving into local drive had error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            //Release resources:
            imgBytes = null;
            imgBitmap = null;
        }
    }
}
