package com.trekcrumb.android.thread;

import java.io.Serializable;

import android.app.Activity;
import android.util.Log;

import com.trekcrumb.android.listener.IProgressWaitListener;

/**
 * A Thread implementation to provide separate (secondary) thread to sleep while waiting 
 * for the main app thread to get GeoLocation update. 
 * When the thread's sleep time expired, it would call the main thread and process with failure.
 * When the thread's sleep is interupted before expired, it is a good sign and do nothing.
 * 
 * This thread can be apply from a running Activity, or a fragment.
 *
 */
@SuppressWarnings("serial")
public class ProgressWaitThread extends Thread implements Serializable {
    private static final String TAG_LOGGER = "ProgressWaitThread";
    
    private Activity mHostActivity;
    private IProgressWaitListener mProgressWaitListener;
    private int mWaitingTime;
    
    public ProgressWaitThread(
            final Activity activity, 
            final IProgressWaitListener progressWaitListener,
            int waitingTime) {
        if(activity == null
                || progressWaitListener == null) {
            throw new IllegalArgumentException("Parameters host activity and IProgressWaitListener are required!");
        }
        mHostActivity = activity;
        mProgressWaitListener = progressWaitListener;
        mWaitingTime = waitingTime;
    }
    
    public void run() {
        try {
            Log.d(TAG_LOGGER, "Start waiting and sleeping for [" + mWaitingTime + "] ...");
            sleep(mWaitingTime);
            
            Log.i(TAG_LOGGER, "Waiting time has expired! Wake up and continue...");
            //Must run on the calling MAIN UI thread:
            mHostActivity.runOnUiThread(new Runnable() {
                public void run() {
                    mProgressWaitListener.onProgressCompletedCallback();
                }
            });
        } catch (InterruptedException e) {
            //Do nothing.
            Log.d(TAG_LOGGER, "Thread works is interrupted for some reason.");
        }
    }
    
}
