<resources></resources>
<!-- REFERENCE ONLY (NOT USED):
     1. Contains parent style lineage for app's custom styles that are overriding them. Used only for
        reference. A lot of effort to collect these.
     2. Must have empty <resources /> tag to pass Android build scheme.
     3. Parent references:
        -> [ANDROID_SDK_INSTALL_DIR]\SDK\platforms\android-16\data\res\values\themes.xml
        -> [ANDROID_SDK_INSTALL_DIR]\SDK\platforms\android-16\data\res\values\styles.xml


    *** ACTIONBAR ***
    <style name="trekcrumbActionBarStyle" parent="android:style/Widget.Holo.ActionBar">
        <item name="android:displayOptions">useLogo|showHome</item>
        <item name="android:background">@color/color_active_dark</item>
        <item name="android:icon">@drawable/icon_applogo_2</item>
        <item name="android:divider">@null</item>
        <item name="android:gravity">center</item>
        <item name="android:layout_gravity">center</item>
        <item name="android:adjustViewBounds">true</item>
        <item name="android:scaleType">fitCenter</item>
        <item name="android:itemPadding">0dip</item>
    </style>
    <style name="Widget.Holo.ActionBar" parent="Widget.ActionBar">
        <item name="android:titleTextStyle">@android:style/TextAppearance.Holo.Widget.ActionBar.Title</item>
        <item name="android:subtitleTextStyle">@android:style/TextAppearance.Holo.Widget.ActionBar.Subtitle</item>
        <item name="android:background">@android:drawable/ab_transparent_dark_holo</item>
        <item name="android:backgroundStacked">@android:drawable/ab_stacked_transparent_dark_holo</item>
        <item name="android:backgroundSplit">@android:drawable/ab_bottom_transparent_dark_holo</item>
        <item name="android:divider">?android:attr/dividerVertical</item>
        <item name="android:progressBarStyle">@android:style/Widget.Holo.ProgressBar.Horizontal</item>
        <item name="android:indeterminateProgressStyle">@android:style/Widget.Holo.ProgressBar</item>
        <item name="android:progressBarPadding">32dip</item>
        <item name="android:itemPadding">8dip</item>
    </style>
    <style name="Widget.ActionBar">
        <item name="android:background">@android:drawable/action_bar_background</item>
        <item name="android:displayOptions">useLogo|showHome|showTitle</item>
        <item name="android:divider">@android:drawable/action_bar_divider</item>
        <item name="android:height">?android:attr/actionBarSize</item>
        <item name="android:paddingLeft">0dip</item>
        <item name="android:paddingTop">0dip</item>
        <item name="android:paddingRight">0dip</item>
        <item name="android:paddingBottom">0dip</item>
        <item name="android:titleTextStyle">@android:style/TextAppearance.Widget.ActionBar.Title</item>
        <item name="android:subtitleTextStyle">@android:style/TextAppearance.Widget.ActionBar.Subtitle</item>
        <item name="android:progressBarStyle">@android:style/Widget.ProgressBar.Horizontal</item>
        <item name="android:indeterminateProgressStyle">@android:style/Widget.ProgressBar.Small</item>
        <item name="android:homeLayout">@android:layout/action_bar_home</item>
    </style>
    <style name="Widget">
        <item name="android:textAppearance">?textAppearance</item>
    </style>
    
    *** ACTION BAR ICONS STYLE ***
    <style name="trekcrumbActionBarButtonStyle" parent="android:style/Widget.Holo.ActionButton">
        <item name="android:background">@drawable/drawable_statelist_clickable_actionbar_icon</item>
    </style>
    <style name="Widget.Holo.ActionButton" parent="Widget.ActionButton">
        <item name="android:minWidth">@android:dimen/action_button_min_width</item>
        <item name="android:gravity">center</item>
        <item name="android:paddingLeft">12dip</item>
        <item name="android:paddingRight">12dip</item>
        <item name="android:scaleType">center</item>
        <item name="android:maxLines">2</item>
    </style>
    <style name="Widget.ActionButton">
        <item name="android:background">?android:attr/actionBarItemBackground</item>
        <item name="android:paddingLeft">12dip</item>
        <item name="android:paddingRight">12dip</item>
        <item name="android:minWidth">@android:dimen/action_button_min_width</item>
        <item name="android:minHeight">?android:attr/actionBarSize</item>
        <item name="android:gravity">center</item>
        <item name="android:maxLines">2</item>
    </style>
    <style name="Widget">
        <item name="android:textAppearance">?textAppearance</item>
    </style>

    *** ACTION BAR OVERFLOW MENU BUTTON STYLE ***
    <style name="trekcrumbActionBarOverflowButtonStyle" parent="android:style/Widget.Holo.ActionButton.Overflow">
        <item name="android:background">@color/color_black</item>
        <item name="android:src">@drawable/any_custom_icon_here</item>
    </style>
    <style name="Widget.Holo.ActionButton.Overflow">
        <item name="android:src">@android:drawable/ic_menu_moreoverflow_holo_dark</item>
        <item name="android:background">?android:attr/actionBarItemBackground</item>
        <item name="android:contentDescription">@string/action_menu_overflow_description</item>
    </style>
    <style name="Widget.ActionButton.Overflow">
        <item name="android:src">@drawable/ic_menu_more</item>
        <item name="android:contentDescription">@string/action_menu_overflow_description</item>
    </style>
    <style name="Widget.Holo.ActionButton" parent="Widget.ActionButton">
        <item name="android:minWidth">@android:dimen/action_button_min_width</item>
        <item name="android:gravity">center</item>
        <item name="android:paddingLeft">12dip</item>
        <item name="android:paddingRight">12dip</item>
        <item name="android:scaleType">center</item>
        <item name="android:maxLines">2</item>
    </style>
    <style name="Widget.ActionButton">
        <item name="android:background">?android:attr/actionBarItemBackground</item>
        <item name="android:paddingLeft">12dip</item>
        <item name="android:paddingRight">12dip</item>
        <item name="android:minWidth">@android:dimen/action_button_min_width</item>
        <item name="android:minHeight">?android:attr/actionBarSize</item>
        <item name="android:gravity">center</item>
        <item name="android:maxLines">2</item>
    </style>
    <style name="Widget">
        <item name="android:textAppearance">?textAppearance</item>
    </style>
    
    *** ACTION BAR OVERFLOW MENU ITEM STYLE ***
    <style name="trekcrumbActionBarOverflowPopupMenuStyle" parent="@android:style/Widget.Holo.ListPopupWindow">
        <item name="android:popupBackground">@color/color_active_dark</item>
        <item name="android:textColor">@color/color_white</item>
    </style>
    <style name="Widget.Holo.ListPopupWindow" parent="Widget.ListPopupWindow">
        <item name="android:dropDownSelector">@android:drawable/list_selector_holo_dark</item>
        <item name="android:popupBackground">@android:drawable/menu_dropdown_panel_holo_dark</item>
        <item name="android:dropDownVerticalOffset">0dip</item>
        <item name="android:dropDownHorizontalOffset">0dip</item>
        <item name="android:dropDownWidth">wrap_content</item>
    </style>
    <style name="Widget.ListPopupWindow">
        <item name="android:dropDownSelector">@android:drawable/list_selector_background</item>
        <item name="android:popupBackground">@android:drawable/spinner_dropdown_background</item>
        <item name="android:dropDownVerticalOffset">-10dip</item>
        <item name="android:dropDownHorizontalOffset">0dip</item>
        <item name="android:dropDownWidth">wrap_content</item>        
    </style>
    <style name="Widget.Holo" parent="Widget">
    </style>
    <style name="Widget">
        <item name="android:textAppearance">?textAppearance</item>
    </style>
    
    *** ACTION BAR OVERFLOW MENU ITEM VIEW STYLE ***
    <style name="trekcrumbActionBarOverflowDropDownListViewStyle" parent="@android:style/Widget.Holo.ListView">
        <item name="android:divider">@null</item>
        <item name="android:listSelector">@drawable/drawable_statelist_clickable_actionbar_overflow_item</item>
    </style>
    <style name="Widget.Holo.ListView" parent="Widget.ListView">
        <item name="android:divider">?android:attr/listDivider</item>
        <item name="android:listSelector">?android:attr/listChoiceBackgroundIndicator</item>
    </style>
    <style name="Widget.ListView" parent="Widget.AbsListView">
        <item name="android:listSelector">@android:drawable/list_selector_background</item>
        <item name="android:cacheColorHint">?android:attr/colorBackgroundCacheHint</item>
        <item name="android:divider">@android:drawable/divider_horizontal_dark_opaque</item>
    </style>
    <style name="Widget.AbsListView">
        <item name="android:scrollbars">vertical</item>
        <item name="android:fadingEdge">vertical</item>
    </style>
    <style name="Widget.Holo" parent="Widget">
    </style>
    <style name="Widget">
        <item name="android:textAppearance">?textAppearance</item>
    </style>
    
    *** ACTIONBAR NAVIGATION TAB STYLE ***
    <style name="trekcrumbActionBarTabStyle" parent="android:style/Widget.ActionBar.TabView">
        <item name="android:background">@drawable/drawable_statelist_clickable_actionbar_tab_style</item>
        <item name="android:paddingLeft">0dp</item>
        <item name="android:paddingRight">0dp</item>
        <item name="android:layout_marginLeft">0dp</item>
        <item name="android:layout_marginRight">0dp</item>
        <item name="android:layout_width">fill_parent</item>
    </style>
    //TODO: Parent lineage 
    
    *** ACTIONBAR NAVIGATION TAB TEXT STYLE ***
    <style name="trekcrumbcActionBarTabTextStyle" parent="android:style/Widget.ActionBar.TabText">
        <item name="android:textAllCaps">false</item>
        <item name="android:textSize">12sp</item>
        <item name="android:textStyle">bold</item>
        <item name="android:textColor">@drawable/drawable_statelist_clickable_actionbar_tabtext_style</item>
    </style>
    //TODO: Parent lineage 

    *** ACTIONBAR NAVIGATION TAB EACH BAR STYLE ***
    <style name="trekcrumbActionBarTabBarStyle" parent="android:style/Widget.ActionBar.TabBar">
        <item name="android:showDividers">none</item>
        <item name="android:divider">@null</item>
        <item name="android:dividerPadding">0dp</item>
    </style>
    //TODO: Parent lineage 
    
    *** EDITTEXT STYLE ***
    <style name="trekcrumbEditTextStyle" parent="@android:style/Widget.EditText">
        <item name="android:textColor">@color/color_black</item>
    </style>
    <style name="Widget.EditText">
        <item name="android:focusable">true</item>
        <item name="android:focusableInTouchMode">true</item>
        <item name="android:clickable">true</item>
        <item name="android:background">?android:attr/editTextBackground</item>
        <item name="android:textAppearance">?android:attr/textAppearanceMediumInverse</item>
        <item name="android:textColor">?android:attr/editTextColor</item>
        <item name="android:gravity">center_vertical</item>
    </style>
    
-->
