package com.trekcrumb.webapp.utility;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.bean.UserSession;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webapp.controller.ErrorController;
import com.trekcrumb.webserviceclient.FavoriteWSClient;
import com.trekcrumb.webserviceclient.TripWSClient;
import com.trekcrumb.webserviceclient.UserAuthTokenWSClient;
import com.trekcrumb.webserviceclient.UserWSClient;

public class WebAppUtil {
    private final static Logger LOGGER = Logger.getLogger("com.trekcrumb.webapp.utility.WebAppUtil");
    
    public static HttpSession sessionInit(final HttpServletRequest servletRequest) {
        HttpSession session = servletRequest.getSession();
        
        //Validates the session if it is an existing one but already exceeds its age, or idle too long:
        /*
        if(!session.isNew()) {  
            //Validate only if it is not a new session:
            Date ageMaxDays = new Date(System.currentTimeMillis() - 24*60*60*1000);
            Date idleMaxHours = new Date(System.currentTimeMillis() - 60*60*1000);
            Date created = new Date(session.getCreationTime());
            Date accessed = new Date(session.getLastAccessedTime());
            if (created.before(ageMaxDays) || accessed.before(idleMaxHours)) {
                //Get a new session:
                session.invalidate();
                session = servletRequest.getSession(true);
            }
        }
        */
        
        session.setMaxInactiveInterval(WebAppConstants.SESSION_MAX_INACTIVE_INTERVAL);
        return session;
    }
    
    public static void sessionDestroy(HttpServletRequest servletRequest) {
        /*
         * We call request.getSession(false), meaning if there is no session available,
         * Do NOT implicitly automatically create a session (which was a default behavior).
         */
        HttpSession sessionCurrent = servletRequest.getSession(false);
        if(sessionCurrent != null) {
            sessionCurrent.removeAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
            sessionCurrent.invalidate();
        }
    }
    
    /**
     * Evaluates if incoming request contains SessionID from URL-Rewriting paradigm or from Cookie.
     * @return FALSE if incoming request contains URL-rewriting paradigm that contains the source for
     *         SessionID, or if it does not contain Cookie. Returns TRUE otherwise.
     */
    public static boolean isSessionFromCookie(final HttpServletRequest servletRequest) {
        if(servletRequest.isRequestedSessionIdFromURL()) {
            LOGGER.error("isSessionFromCookie() - Illegal request with URL Rewriting session ID [" + servletRequest.getRequestURL() + "]!");
            return false;
        }
        if(servletRequest.getHeader("Cookie") == null) {
            return false;
        }
        
        //else:
        return true;
    }

    public static boolean isUserSessionActive(final HttpServletRequest servletRequest) {
        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        if(userSession == null 
                || userSession.getUser() == null
                || !userSession.isUserAuthenticated()) {
            return false;
        }
        
        //else:
        return true;
    }
    
    public static boolean isUserLoginProfile(final HttpServletRequest servletRequest, String userID) {
        if(ValidationUtil.isEmpty(userID)) {
            return false;
        }
        if(!isUserSessionActive(servletRequest)) {
            return false;
        }
        
        User userLogin = ((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser();
        return userLogin.getUserId().equalsIgnoreCase(userID);
    }
    
    public static ServiceResponse verifyUserHasAccessToProfile(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody User userToUpdate,
            ServiceTypeEnum serviceType) {
        if(userToUpdate == null) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        if(!WebAppUtil.isUserSessionActive(servletRequest)) {
            boolean userAuthTokenSuccess = WebAppUtil.userAuthTokenAuthenticate(servletRequest);
            if(!userAuthTokenSuccess) {
                return ErrorController.handleError_userNotAuthenticated_AJAX(
                        servletRequest, servletResponse, serviceType);
            }
        }
        if(!WebAppUtil.isUserLoginProfile(servletRequest, userToUpdate.getUserId())) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        
        //Valid
        return null;
    }
    
    @Deprecated
    public static ModelAndView verifyUserHasAccessToTrip(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            String tripSelectedID) {
        if(!isSessionFromCookie(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated(servletRequest, servletResponse);
        }
        if(!isUserSessionActive(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated(servletRequest, servletResponse);
        }

        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        User userLogin = userSession.getUser();
        
        Trip tripSelectedInSession = userSession.getTripSelected();
        if(tripSelectedInSession == null) {
            return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
        }
        if(!tripSelectedInSession.getId().equalsIgnoreCase(tripSelectedID)) {
            return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
        }
        
        if(!userLogin.getUserId().equalsIgnoreCase(tripSelectedInSession.getUserID())) {
            return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
        }
        
        //No problem:
        return null;
    }
    
    public static ServiceResponse verifyUserHasAccessToTrip(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            String tripSelectedID,
            ServiceTypeEnum serviceType) {
        if(!isSessionFromCookie(servletRequest)) {
           return ErrorController.handleError_userNotAuthenticated_AJAX(servletRequest, servletResponse, serviceType);
        }
        if(!isUserSessionActive(servletRequest)) {
            boolean userAuthTokenSuccess = userAuthTokenAuthenticate(servletRequest);
            if(!userAuthTokenSuccess) {
                return ErrorController.handleError_userNotAuthenticated_AJAX(
                        servletRequest, servletResponse, serviceType);
            } else {
                /*
                 * If success, it has created brand new UserSession object and wiped out any stored
                 * data in session. Here, try to re-retrieve the trip in question.
                 */
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripSelectedID);
                ServiceResponse svcResponse = TripWSClient.retrieve(tripToRetrieve,  ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, 0, 1);
                svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
                if(svcResponse.isSuccess()) {  
                    UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
                    userSession.setTripSelected(svcResponse.getListOfTrips().get(0));
                } else {
                    return svcResponse;
                }
            }
        }
        
        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        User userLogin = userSession.getUser();
        
        Trip tripSelectedInSession = userSession.getTripSelected();
        if(tripSelectedInSession == null) {
            return ErrorController.handleError_illegalAccess_AJAX(servletRequest, servletResponse, serviceType);
        }
        if(!tripSelectedInSession.getId().equalsIgnoreCase(tripSelectedID)) {
            return ErrorController.handleError_illegalAccess_AJAX(servletRequest, servletResponse, serviceType);
        }
        
        if(!userLogin.getUserId().equalsIgnoreCase(tripSelectedInSession.getUserID())) {
            return ErrorController.handleError_illegalAccess_AJAX(servletRequest, servletResponse, serviceType);
        }
        
        //No problem:
        return null;
    }
    
    public static boolean isTripSelectedUserFavorite(
            final HttpServletRequest servletRequest,
            Trip trip) {
        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        if(userSession == null) {
            return false;
        } 
        if(userSession.getUser() == null
                || !userSession.isUserAuthenticated()) {
            userSession.setTripSelectedUserFavorite(false);
            return false;
        }
        

        //Else:
        Favorite favoriteToRetrieve = new Favorite();
        favoriteToRetrieve.setUserID(userSession.getUser().getUserId());
        favoriteToRetrieve.setTripID(trip.getId());
        ServiceResponse svcResponse = FavoriteWSClient.retrieve(favoriteToRetrieve, null, -1, -1);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()
                || svcResponse.getListOfFavorites() == null
                || svcResponse.getListOfFavorites().get(0) == null) {
            userSession.setTripSelectedUserFavorite(false);
            return false;
        }
        
        userSession.setTripSelectedUserFavorite(true);
        return true;
    }
    
    /**
     * Updates logged-in User attribute data WITH re-reading the database.
     */
    public static void updateUserLoginInSession(final HttpServletRequest servletRequest) {
        if(!isUserSessionActive(servletRequest)) {
            return;
        }
        
        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        User userLogin = userSession.getUser();

        User userToRetrieve = new User();
        userToRetrieve.setUsername(userLogin.getUsername());
        ServiceResponse svcResponse = UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()
                && svcResponse.getListOfUsers() != null
                && svcResponse.getListOfUsers().get(0) != null) {
            User userLoginRefreshed = svcResponse.getListOfUsers().get(0);
            
            //Restore secured data:
            userLoginRefreshed.setUserId(userLogin.getUserId());
            userLoginRefreshed.setEmail(userLogin.getEmail());
            
            userSession.setUser(userLoginRefreshed);
            servletRequest.getSession().setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        }
    }    
    
    /**
     * Updates logged-in User attribute data on the fly, WITHOUT re-reading the database.
     * @param numOfTripsDelta - Delta (plus or minus) of number of trips that will be add or deduct
     *                          from user's current total number of trips.
     * @param numOfFavoritesDelta - Delta (plus or minus) of number of favorites that will be add or deduct
     *                          from user's current total number of favorites.
     * @param numOfCommentsDelta - Delta (plus or minus) of number of Comments that will be add or deduct
     *                          from user's current total number of Comments.
     */
    public static void updateUserLoginInSession(
            final HttpServletRequest servletRequest,
            int numOfTripsDelta,
            int numOfFavoritesDelta,
            int numOfCommentsDelta) {
        if(!isUserSessionActive(servletRequest)) {
            return;
        }
        
        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        User userLogin = userSession.getUser();
        boolean isUserUpdated = false;

        //Trips:
        if(numOfTripsDelta != 0) {
            userLogin.setNumOfTotalTrips(userLogin.getNumOfTotalTrips() + numOfTripsDelta);
            isUserUpdated = true;
        }
            
        //Favorites:
        if(numOfFavoritesDelta > 0) {
            userLogin.setNumOfFavorites(userLogin.getNumOfFavorites() + numOfFavoritesDelta);
            if(userSession.getTripSelected() != null) {
                userSession.setTripSelectedUserFavorite(true);
            }
        } else if(numOfFavoritesDelta < 0) {
            userLogin.setNumOfFavorites(userLogin.getNumOfFavorites() + numOfFavoritesDelta);
            userSession.setTripSelectedUserFavorite(false);
        }
        
        //Comments:
        if(numOfCommentsDelta != 0) {
            userLogin.setNumOfComments(userLogin.getNumOfComments() + numOfCommentsDelta);
        }
        
        if(isUserUpdated) {
            userSession.setUser(userLogin);
            servletRequest.getSession().setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        }
    }

    /**
     * Evaluates if the incoming HttpServletRequest contains predefined embedded info/error
     * messages.
     * 1. From RedirectAttributes' FlashAttribute: The caller (other controllers, etc.) must 
     *    embed the info/error message String in this FlashAttribute in order for this method to read them
     *    with keys: CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO and/or CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR
     * 
     * 2. From HttpServletRequest: The caller must embed the info/error message as QUERY string with
     *    parameter names: CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO and/or CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR
     *    
     * If any info/error messages are found, the method will add them as a request attribute to the
     * ModelAndView result, and they become HTTPServletRequest.setAttribute() where attributes 
     * only persist as long as the request is being handled (once).
     * 
     * @param resultPage - The target ModelAndView landing page where it now may have info/error 
     *                     message from previous controller and the landing page (JSP) would 
     *                     process and display them.
     * @param infoMessage - Any info message to be displayed at the final view page.
     *                     
     */
    public static ModelAndView includeInforOrErrorMessages(
            final HttpServletRequest request,
            ModelAndView resultPage,
            String infoMessageInput) {
        String infoMsgFromServer = null;
        String errorMsgFromServer = null;
        
        //Look from input param
        if(!ValidationUtil.isEmpty(infoMessageInput)) {
            infoMsgFromServer = infoMessageInput;
            resultPage.addObject(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsgFromServer);
        }
        
        //Look from FlashAttribute
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
        if (flashMap != null) {
            if(ValidationUtil.isEmpty(infoMsgFromServer)) {
                infoMsgFromServer = (String)flashMap.get(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
                if(!ValidationUtil.isEmpty(infoMsgFromServer)) {
                    resultPage.addObject(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsgFromServer);
                }
            }
            
            errorMsgFromServer = (String)flashMap.get(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
            if(!ValidationUtil.isEmpty(errorMsgFromServer)) {
                resultPage.addObject(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, errorMsgFromServer);
            }
        }
        
        //Look from URL query:
        //TODO (1/2016: Unsure about this work, need more testing. This would require query params in URL
        /*
        if(ValidationUtil.isEmpty(infoMsgFromServer)) {
            //infoMsgFromServer = request.getParameter(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            infoMsgFromServer = (String)request.getAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
            if(!ValidationUtil.isEmpty(infoMsgFromServer)) {
                resultPage.addObject(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsgFromServer);
            }
        }
        if(ValidationUtil.isEmpty(errorMsgFromServer)) {
            errorMsgFromServer = request.getParameter(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
            if(!ValidationUtil.isEmpty(errorMsgFromServer)) {
                resultPage.addObject(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, errorMsgFromServer);
            }
        }
        */
        
        /*
        LOGGER.debug("\n\n\n WebAppUtil.includeInforOrErrorMessages() -  errorMsgFromServer [" 
                    + errorMsgFromServer 
                    + "], infoMsgFromServer [" + infoMsgFromServer + "]\n\n\n");
        */
        
        return resultPage;
    }
    
    /**
     * Neutralizes an input String (name, notes, etc.) from server to the UI (WebApp)
     * to avoid any invalid characters for HTML/JS such as double-quote, single-quote,
     * new line-feed, etc.  in input element, metadata or function.
     * 
     * @param isRemoveSpecialChars - If TRUE, it replaces any special char such as single quote ' and
     *                          double quote " with empty String.
     * @param isRemoveNewLine - if TRUE, it replaces new line char with empty String, and isNeutralizeNewLine
     *                          param is ignored.
     * @param isNeutralizeNewLine - if TRUE, it does NOT remove the new line but replaces it with
     *                          HTML tag <br/>.
     * @param maxLength - If provided and bigger than zero, it truncates the string up to the specified
     *                          maximum length. If value is less or equal to 0, ignored.                         
     * @return A new String with neutralized content.
     */
    public static String neutralizeStringValue(
            String stringOrig,
            boolean isRemoveSpecialChars,
            boolean isRemoveNewLine,
            boolean isNeutralizeNewLine,
            int maxLength) {
        if(ValidationUtil.isEmpty(stringOrig)) {
            return stringOrig;
        }
        
        String stringNeutralized = stringOrig;
        if(isRemoveSpecialChars) {
            stringNeutralized = stringNeutralized.replaceAll("(\")", "");
            stringNeutralized = stringNeutralized.replaceAll("(')", "");
        } else {
            stringNeutralized = stringOrig.replaceAll("(\")", "&quot;");
            stringNeutralized = stringNeutralized.replaceAll("(')", "&#39;");
        }
        
        if(isRemoveNewLine) {
            stringNeutralized = stringNeutralized.replaceAll("(\r\n|\n)", " ");
        } else if(isNeutralizeNewLine) {
            stringNeutralized = stringNeutralized.replaceAll("(\r\n|\n)", "<br/>");
        }
        
        if(maxLength > 0 && stringNeutralized.length() > maxLength) {
            stringNeutralized = stringNeutralized.substring(0, maxLength - 1) + " ... [more]";
        }
       
        return stringNeutralized;
    }    
    
    public static void populatePageIdentity(
            HttpServletRequest servletRequest,
            ModelAndView resultPage,
            String pageType,
            String pageURL,
            String pageTitle,
            String pageSummary,
            String pageImageURL) {
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_TYPE, pageType);
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_URL, pageURL);
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_TITLE, pageTitle);
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_SUMMARY, pageSummary);
        
        if(pageImageURL == null) {
            //Set it to App LOGO as default:
            StringBuilder pageImageURLBld = new StringBuilder();
            pageImageURLBld.append(servletRequest.getScheme() + "://");
            pageImageURLBld.append(servletRequest.getServerName());
            if(servletRequest.getServerPort() > 0) {
                pageImageURLBld.append(":" + servletRequest.getServerPort());
            }
            pageImageURLBld.append(servletRequest.getContextPath() + "/");
            pageImageURLBld.append(WebAppConstants.APP_LOGO);
            pageImageURL = pageImageURLBld.toString();
            resultPage.addObject(WebAppConstants.PAGE_IDENTITY_IMAGEURL, pageImageURL);
        }
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_IMAGEURL, pageImageURL);
    }
    
    public static void populatePageIdentityForHome(
            HttpServletRequest servletRequest,
            ModelAndView resultPage) {
        String pageURL = WebAppConstants.APP_DOMAIN_URL;
        String pageTitle = WebAppConstants.APP_TITLE;
        String pageSummary = MessageResourceUtil.getMessageValue("info_home_greeting_welcome", null, null);
        WebAppUtil.populatePageIdentity(
                servletRequest, resultPage, 
                WebAppConstants.PAGE_IDENTITY_TYPE_HOME, pageURL, pageTitle, pageSummary, null);
    }
    
    public static void populatePageIdentityForTripDetail(
            HttpServletRequest servletRequest,
            ModelAndView resultPage, 
            Trip trip) {
        String pageURL = servletRequest.getRequestURL().toString();
        String pageTitle = WebAppUtil.neutralizeStringValue(trip.getName(), true, true, false, -1);
        
        //Summary:
        StringBuilder pageSummaryBld = new StringBuilder();
        String tripCreatedDate = DateUtil.formatDate(
                trip.getCreated(), 
                DateFormatEnum.GMT_STANDARD_LONG, 
                DateFormatEnum.LOCALE_UI_WITHOUT_TIME);
        pageSummaryBld.append(tripCreatedDate);
        
        if(!ValidationUtil.isEmpty(trip.getLocation())) {
            //Capture ONLY the first String of location/address:
            String tripLocation = trip.getLocation();
            int charIndex = tripLocation.indexOf(",");
            if(charIndex > 0) {
                tripLocation = tripLocation.substring(0, charIndex);
            }
            pageSummaryBld.append(", " + tripLocation);
        }
        
        if(!ValidationUtil.isEmpty(trip.getNote())) {
            pageSummaryBld.append(" - " + trip.getNote());
        } else {
            //Default:
            pageSummaryBld.append(" - " );
            pageSummaryBld.append("Trip journal by " );
            pageSummaryBld.append(trip.getUserFullname());
            pageSummaryBld.append(" at Trekcrumb.com");
        }
        String pageSummary = WebAppUtil.neutralizeStringValue(pageSummaryBld.toString(), true, true, false, 100);
        
        //Image URL;
        String pageImageURL = null;
        if(trip.getNumOfPictures() > 0) {
            Picture picCover = trip.getListOfPictures().get(0);
            pageImageURL = CommonConstants.FS_SERVLET_URL_GETIMAGE_PICTURE + picCover.getImageName();
        }
        
        WebAppUtil.populatePageIdentity(
                servletRequest, resultPage, 
                WebAppConstants.PAGE_IDENTITY_TYPE_TRIP_DETAIL, pageURL, pageTitle, pageSummary, pageImageURL);
        
        //More:
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_AUTHOR, trip.getUsername());
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_CREATED_DATE, trip.getCreated());
        if(!ValidationUtil.isEmpty(trip.getLocation())) {
            resultPage.addObject(WebAppConstants.PAGE_IDENTITY_LOCATION, trip.getLocation());
        }
    }
    
    public static void populatePageIdentityForUserDetail(
            HttpServletRequest servletRequest,
            ModelAndView resultPage, 
            User user) {
        String pageURL = servletRequest.getRequestURL().toString();
        String pageTitle = WebAppUtil.neutralizeStringValue(user.getFullname(), true, true, false, -1) + " at " + WebAppConstants.APP_TITLE;

        //Summary:
        StringBuilder pageSummaryBld = new StringBuilder();
        pageSummaryBld.append("Trek posts: " + user.getNumOfTotalTrips());
        pageSummaryBld.append(". ");
        pageSummaryBld.append(WebAppUtil.neutralizeStringValue(user.getSummary(), true, true, false, -1));
        String pageSummary = pageSummaryBld.toString();
        
        //Image URL;
        String pageImageURL = null;
        if(!ValidationUtil.isEmpty(user.getProfileImageName())) {
            pageImageURL = CommonConstants.FS_SERVLET_URL_GETIMAGE_PROFILE + user.getProfileImageName();
        }
        
        WebAppUtil.populatePageIdentity(
                servletRequest, resultPage, 
                WebAppConstants.PAGE_IDENTITY_TYPE_USER_PROFILE, pageURL, pageTitle, pageSummary, pageImageURL);
        
        //More:
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_CREATED_DATE, user.getCreated());
        resultPage.addObject(WebAppConstants.PAGE_IDENTITY_LOCATION, user.getLocation());
    }
    
    /**
     * Authenticates UserAuthToken for auto login.
     * Business Rules:
     * 1. If UserLogin exists in current Session, skip it
     * 2. If HTTP Request does not have any Cookie, skip it. Note: App config setting (web.xml) is to  
     *    include Cookie only if the protocol is HTTPS secure. Therefore, for regular HTTP, we
     *    should not expect any Cookie.
     * 3. If protocol is not HTTPS, skip it. Note: Despite our app config, some server config may 
     *    override secure cookie setting and include cookie in non-HTTPS scheme.
     *    
     * @return TRUE if auto login success.
     */
    public static boolean userAuthTokenAuthenticate(HttpServletRequest servletRequest) {
        if(WebAppUtil.isUserSessionActive(servletRequest)) {
            return false;
        }
        if(servletRequest.getHeader("Cookie") == null) {
            return false;
        }
        if(servletRequest.getScheme() == null 
                || !servletRequest.getScheme().trim().equalsIgnoreCase("https")) {
            return false;
        }

        String userIDCrypted = null;
        String authTokenCrypted = null;
        Cookie[] arrayOfcookies = servletRequest.getCookies();
        for (int i = 0; i < arrayOfcookies.length; i++) {
            if(arrayOfcookies[i].getName().equalsIgnoreCase(WebAppConstants.COOKIE_NAME_USER_ID)) {
                userIDCrypted = arrayOfcookies[i].getValue();
            } else if(arrayOfcookies[i].getName().equalsIgnoreCase(WebAppConstants.COOKIE_NAME_AUTH_TOKEN)) {
                authTokenCrypted = arrayOfcookies[i].getValue();
            }
            
            if(userIDCrypted != null && authTokenCrypted != null) {
                //We get all we need from cookies:
                break;
            }
        }
        
        if(userIDCrypted == null || authTokenCrypted == null) {
            //Cookies for auth token are not complete
            return false;
        }
        
        UserAuthToken userAuthTokenInput = null;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            userAuthTokenInput = new UserAuthToken();
            userAuthTokenInput.setUserId(cryptoInstance.decrypt(userIDCrypted));
            userAuthTokenInput.setAuthToken(cryptoInstance.decrypt(authTokenCrypted));
        } catch(Exception e) {
            LOGGER.error("userAuthTokenAuthenticate() - Unexpected error decrypting UserAuthToken data! Error:  " + e.getMessage(), e);
            return false;
        }
        
        //1. Authenticate
        //User userLogin = UserAuthTokenManager.authenticate(userAuthTokenInput);
        ServiceResponse svcResponse = UserAuthTokenWSClient.authenticate(userAuthTokenInput);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            LOGGER.error("userAuthTokenAuthenticate() - FAILED!\n" + svcResponse);
            return false;
        }

        //2. Security101: Destroy any current session first. This wipes out any stored data in session
        WebAppUtil.sessionDestroy(servletRequest);

        //3. Compose new UserSession
        UserSession userSession = new UserSession();
        userSession.setUser(svcResponse.getUser());
        userSession.setUserAuthenticated(true);

        //4. Then store user in server session:
        HttpSession session = WebAppUtil.sessionInit(servletRequest);
        session.setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        return true;
    }
    
    
}