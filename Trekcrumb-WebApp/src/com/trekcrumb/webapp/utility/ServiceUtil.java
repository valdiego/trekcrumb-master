package com.trekcrumb.webapp.utility;

import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

public class ServiceUtil {

    public static ServiceResponse evaluateServiceResponse(ServiceResponse response) {
        if(response == null) {
            return CommonUtil.composeServiceResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    MessageResourceUtil.getMessageValue("error_system_commonError", null, null));
            
        } else if(response.isSuccess()) {
            return response;
        }
        
        //Else: There is error:
        String errMsg = null;
        if(response.getServiceError() != null) {
            //Translate back-end service error to user-friendly/generic error when possible:
            ServiceErrorEnum errorEnum = response.getServiceError().getErrorEnum();
            if(errorEnum == ServiceErrorEnum.SYSTEM_NETWORK_NOT_AVAILABLE_ERROR) {
                errMsg = MessageResourceUtil.getMessageValue("error_system_device_networkNotAvailable", null, null);
            } else if(errorEnum == ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR) {
                errMsg = MessageResourceUtil.getMessageValue("error_system_server_NotAvailable", null, null);
                
            } else if(errorEnum == ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR) {
                if(response.getServiceType() == ServiceTypeEnum.USER_LOGIN) {
                    errMsg = MessageResourceUtil.getMessageValue("error_user_LoginFailed", null, null);
                } else {
                    errMsg = MessageResourceUtil.getMessageValue("error_user_profile_empty", null, null);
                }
                
            } else if(errorEnum == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_FORMAT_NOT_SUPPORTED) {
                errMsg = MessageResourceUtil.getMessageValue("error_picture_create_imageWriteError", null, null);
            } else if(errorEnum == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND) {
                errMsg = MessageResourceUtil.getMessageValue("error_picture_image_notFound", null, null);
                
            } else if(errorEnum == ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR) {
                //Specific error for read/update an item:
                if(response.getServiceType() == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID
                        || response.getServiceType() == ServiceTypeEnum.TRIP_UPDATE) {
                    errMsg = MessageResourceUtil.getMessageValue("error_trip_notFound", null, null);
                } else if(response.getServiceType() == ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID
                        || response.getServiceType() == ServiceTypeEnum.PLACE_UPDATE) {
                    errMsg = MessageResourceUtil.getMessageValue("error_place_notFound", null, null);
                } else if(response.getServiceType() == ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID
                        || response.getServiceType() == ServiceTypeEnum.PICTURE_UPDATE) {
                    errMsg = MessageResourceUtil.getMessageValue("error_picture_notFound", null, null);
                }
                
            } else if(ValidationUtil.isEmpty(response.getServiceError().getErrorText())) {
                //Avoid empty error text:
                errMsg = MessageResourceUtil.getMessageValue("error_system_commonError", null, null);
            }
            
            if(errMsg != null) {
                response.getServiceError().setErrorText(errMsg);
            }
        
        } else {
            //Error is unknown and completely empty:
            response = CommonUtil.composeServiceResponseError(
                    response.getServiceType(),
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    MessageResourceUtil.getMessageValue("error_system_commonError", null, null));
            
        }
        return response;
    }
}
