package com.trekcrumb.webapp.utility;

import com.trekcrumb.common.utility.CommonProperties;

public class WebAppConstants {
    //App basic identity
    public static final String APP_NAME = MessageResourceUtil.getMessageValue("app_name", null, null);
    public static final String APP_TITLE = MessageResourceUtil.getMessageValue("app_title", null, null);
    public static final String APP_SUMMARY = MessageResourceUtil.getMessageValue("app_summary", null, null);
    public static final String APP_LOGO = "images/icon_applogo_1.png";
    public static final String APP_ICON_BROWSER = "images/favicon.ico";
    public static final String APP_ICON_SHORTCUT = "images/favicon.ico";
    public static final String APP_DOMAIN_URL = CommonProperties.getProperty("webapp.url.domain");
    
    //Session
    public static final int SESSION_MAX_INACTIVE_INTERVAL = 21600; //In seconds. 
                                                                   //By default, web server apply the
                                                                   //max interval 30 minutes or so.
                                                                   //Here, we set it to 6 hours.
    
    //Query params
    //Note: Some of these values must match those in trekcrumb-constants.js
    public static final String QUERYPARAM_VALUE_TRUE = "true";
    public static final String QUERYPARAM_NAME_IS_REMEMBER_ME = "rememberme";
    public static final String QUERYPARAM_NAME_DEVICE_IDENTITY = "devid";
    public static final String QUERYPARAM_NAME_IS_AFTER_LOGIN = "isAfterLogin";
    
    //Cookies
    //Note: Some of these values must match those in trekcrumb-constants.js
    public static final String COOKIE_NAME_USER_ID = "UID";
    public static final String COOKIE_NAME_AUTH_TOKEN = "UAUTH";
    public static final String COOKIE_NAME_AUTH_TOKEN_ID = "UAUTHID";
    
    public static final String PAGE_IDENTITY_TYPE = "PAGE_IDENTITY_TYPE";
    public static final String PAGE_IDENTITY_TYPE_HOME = "PAGE_IDENTITY_TYPE_HOME";
    public static final String PAGE_IDENTITY_TYPE_TRIP_DETAIL = "PAGE_IDENTITY_TYPE_TRIP_DETAIL";
    public static final String PAGE_IDENTITY_TYPE_USER_PROFILE = "PAGE_IDENTITY_TYPE_USER_PROFILE";
    public static final String PAGE_IDENTITY_URL = "PAGE_URL";
    public static final String PAGE_IDENTITY_TITLE = "PAGE_TITLE";
    public static final String PAGE_IDENTITY_SUMMARY = "PAGE_SUMMARY";
    public static final String PAGE_IDENTITY_IMAGEURL = "PAGE_IMAGE_URL";
    public static final String PAGE_IDENTITY_LOCATION = "PAGE_IDENTITY_LOCATION";
    public static final String PAGE_IDENTITY_CREATED_DATE = "PAGE_IDENTITY_CREATED_DATE";
    public static final String PAGE_IDENTITY_AUTHOR = "PAGE_IDENTITY_AUTHOR";

    //Controller submit operations:
    public static final String SUBMIT_OP_USER_CREATE = "SUBMIT_OP_USER_CREATE";
    public static final String SUBMIT_OP_USER_UPDATE = "SUBMIT_OP_USER_UPDATE";
    public static final String SUBMIT_OP_USER_LOGIN = "SUBMIT_OP_USER_LOGIN";
    public static final String SUBMIT_OP_USER_REACTIVATE = "SUBMIT_OP_USER_REACTIVATE";
    public static final String SUBMIT_OP_TRIP_CREATE = "SUBMIT_OP_TRIP_CREATE";
    public static final String SUBMIT_OP_TRIP_UPDATE = "SUBMIT_OP_TRIP_UPDATE";
    public static final String SUBMIT_OP_TRIP_DELETE = "SUBMIT_OP_TRIP_DELETE";
    public static final String SUBMIT_OP_PLACE_CREATE = "SUBMIT_OP_PLACE_CREATE";
    public static final String SUBMIT_OP_PLACE_UPDATE = "SUBMIT_OP_PLACE_UPDATE";
    public static final String SUBMIT_OP_PLACE_DELETE = "SUBMIT_OP_PLACE_DELETE";
    public static final String SUBMIT_OP_PICTURE_CREATE = "SUBMIT_OP_PICTURE_CREATE";
    public static final String SUBMIT_OP_PICTURE_UPDATE = "SUBMIT_OP_PICTURE_UPDATE";
    public static final String SUBMIT_OP_PICTURE_DELETE = "SUBMIT_OP_PICTURE_DELETE";
    
    //Page, menu or breadcrumbs
    public static final String PAGE_HOME = "page.home";
    
    
    
    
}
