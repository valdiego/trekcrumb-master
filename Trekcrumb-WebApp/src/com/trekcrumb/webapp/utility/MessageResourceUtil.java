package com.trekcrumb.webapp.utility;

import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import com.trekcrumb.webapp.otherimpl.ApplicationContextAwareImpl;

/**
 * Utility to retrieve message text values from configured MessageResource bundle in the application
 * context.
 * 
 * @author Val Triadi
 * @since 03/2015
 */
public class MessageResourceUtil {
    private final static Logger LOGGER = Logger.getLogger("com.trekcrumb.webapp.utility.MessageResourceUtil");
    
    private static MessageSource messageSource;
    
    static {
        init();
    }
    
    private static void init() {
        if(messageSource == null) {
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("Initializing MessageResource instance for the current app context ... ");
            }
            ApplicationContext appContext = ApplicationContextAwareImpl.getApplicationContext();
            messageSource = (MessageSource)appContext.getBean("messageSource");
        }
    }
    
    /**
     * Get message text value from MessageResource bundle properties.
     * @param messageKey - The key of message properties to look for.
     * @param messageArgs - An array of message arguments to be inserted into the message text value,
     *                      if found. These arguments are typically dynamic values from user data
     *                      or business logic. Pass null if no argument is needed.
     * @param locale - The internationalization locale for the returned message text. Example: 
     *                 Locale.CHINESE or Locale.FRENCH, etc. If null, it defaults to Locale.US
     *                 
     * @return The message text value in the language of chosen Locale parameter, if found. If nothing
     *         found from the MessageResource bundle, 
     */
    public static String getMessageValue(
            String messageKey, 
            Object[] messageArgs,
            Locale locale) {
        init();
        
        String messageValue = null;
        if(messageArgs == null) {
            messageArgs = new Object[0];
        }
        if(locale == null) {
            locale = Locale.US;
        }
        try {
            messageValue = messageSource.getMessage(messageKey, messageArgs, locale);
        } catch(NoSuchMessageException nsme) {
            LOGGER.error("ERROR: " + nsme.getMessage());
            messageValue = "Sorry, we are experiencing unexpected system error. (MSGRSC)";
        }
        return messageValue;
    }    
    
    
}
