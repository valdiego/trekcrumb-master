package com.trekcrumb.webapp.otherimpl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Concrete implementation of the ApplicationContextAware to get application current context
 * and to access configured Spring beans. Such requirements may include access to file resources,
 * MessageResource bean and/or to publish an application event. 
 * 
 * @author Val Triadi
 * @since 03/2015
 */
public class ApplicationContextAwareImpl implements ApplicationContextAware {

    private static ApplicationContext appContext = null;

    public static ApplicationContext getApplicationContext() {
        return appContext;
    }
    
    @SuppressWarnings("static-access")
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
         this.appContext = applicationContext;
    }
    
}
