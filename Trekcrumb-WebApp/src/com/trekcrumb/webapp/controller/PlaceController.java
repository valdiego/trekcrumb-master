package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.PlaceWSClient;

@Controller
@RequestMapping("/place")
public class PlaceController {

    /**
     * Usage: GET /place
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }

    /**
     * Usage: GET /place/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Place Create - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody placeToCreate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /place/create/submit
     */
    @RequestMapping(value = "/create/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse create_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Place placeToCreate) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(placeToCreate == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PLACE_CREATE,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, placeToCreate.getTripID(), ServiceTypeEnum.PLACE_CREATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Else:
        svcResponse = PlaceWSClient.create(placeToCreate);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
    
    /**
     * Place Update - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody placeToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /place/update/submit/AJAX
     */
    @RequestMapping(value = "/update/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse update_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Place placeToUpdate) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(placeToUpdate == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PLACE_UPDATE,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, placeToUpdate.getTripID(), ServiceTypeEnum.PLACE_UPDATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Call back-end service:
        svcResponse = PlaceWSClient.update(placeToUpdate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        } 
        return svcResponse;
    }        
    
    /**
     * Place Delete - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody placeToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /place/delete/submit/AJAX
     */
    @RequestMapping(value = "/delete/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse delete_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Place placeToDelete) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(placeToDelete == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PLACE_DELETE_BY_PLACEID,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, placeToDelete.getTripID(), ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Call back-end service:
        svcResponse = PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        } 
        return svcResponse;
    }        
    
    
}