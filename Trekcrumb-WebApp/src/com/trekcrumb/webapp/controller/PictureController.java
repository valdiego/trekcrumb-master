package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.PictureWSClient;

@Controller
@RequestMapping("/picture")
public class PictureController {
    
    /**
     * Usage: GET /picture
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Usage: GET /picture/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Picture Create - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody pictureToCreate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /picture/create/submit/AJAX
     */
    @RequestMapping(value = "/create/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse create_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Picture pictureToCreate) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(pictureToCreate == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_CREATE,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, pictureToCreate.getTripID(), ServiceTypeEnum.PICTURE_CREATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Validate input image:
        if(pictureToCreate.getImageBase64Data() == null
                || pictureToCreate.getImageBase64Data().trim() == "") {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_CREATE,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    MessageResourceUtil.getMessageValue("error_picture_invalidParam_picEmpty", null, null));
        }
        
        //Else:
        byte[] imageBytes = Base64.decodeBase64(pictureToCreate.getImageBase64Data().getBytes());
        pictureToCreate.setImageBytes(imageBytes);
        
        //Call back-end service:
        //1. WITHOUT WEBSERVICE
        /*
        try {
            Picture picCreated = PictureManager.create(pictureToCreate);
            response = CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.PICTURE_CREATE);
            List<Picture> listOfPics = new ArrayList<Picture>();
            listOfPics.add(picCreated);
            response.setListOfPictures(listOfPics);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            response = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_CREATE, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            LOGGER.error("Picture Create Submit: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            response = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_CREATE, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        */
        
        //2. OR, WITH WEBSERVICE
        svcResponse = PictureWSClient.create(pictureToCreate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_picture_create_success", null, null));
        }
        return svcResponse;
    }    
    
    /**
     * Picture Update - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody pictureToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /picture/update/submit/AJAX
     */
    @RequestMapping(value = "/update/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse update_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Picture pictureToUpdate) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(pictureToUpdate == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_UPDATE,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, pictureToUpdate.getTripID(), ServiceTypeEnum.PICTURE_UPDATE);
        if(svcResponse != null) {
            return svcResponse;
        }

        //Else:
        //1. WITHOUT WEBSERVICE
        /*
        try {
            Picture picUpdated = PictureManager.update(pictureToUpdate);
            List<Picture> listOfPics = new ArrayList<Picture>();
            listOfPics.add(picUpdated);
            
            response = CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.PICTURE_UPDATE);
            response.setListOfPictures(listOfPics);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            response = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_UPDATE, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            LOGGER.error("Picture Update Submit: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            response = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_UPDATE, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        */
        //2. OR, WITH WEBSERVICE
        svcResponse = PictureWSClient.update(pictureToUpdate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        }
        return svcResponse;
    }        
    
    /**
     * Picture Delete - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody pictureToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /picture/update/submit/AJAX
     */
    @RequestMapping(value = "/delete/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse delete_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Picture pictureToDelete) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(pictureToDelete == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, pictureToDelete.getTripID(), ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
        if(svcResponse != null) {
            return svcResponse;
        }

        //Else:
        //1. WITHOUT WEBSERVICE
        /*
        try {
            PictureManager.delete(pictureForm, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            response = CommonUtil.composeServiceResponseSuccess(ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            response = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            LOGGER.error("Picture Delete Submit: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            response = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        */
        //2. OR, WITH WEBSERVICE
        svcResponse = PictureWSClient.delete(pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        }
        return svcResponse;
    }            
    
    /**
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody pictureToRead - Enables to access HTTP request body as the input parameter,
     *                              and convert the value into java object with the declared 
     *                              Java type (using HttpMessageConverters). 
     *                              The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     */
    @RequestMapping(value = "/retrieve_byUsername/{offset}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse retrieve_byUsername(
            @RequestBody Picture pictureToRead,
            @PathVariable int offset) {
        ServiceResponse svcResponse = PictureWSClient.retrieve(
                pictureToRead, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, 
                OrderByEnum.ORDER_BY_CREATED_DATE, offset, CommonConstants.RECORDS_NUMBER_MAX);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
        
    /**
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody pictureToRead - Enables to access HTTP request body as the input parameter,
     *                              and convert the value into java object with the declared 
     *                              Java type (using HttpMessageConverters). 
     *                              The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse search() {
        ServiceResponse svcResponse = PictureWSClient.retrieve(
                new Picture(), ServiceTypeEnum.PICTURE_SEARCH, null, 0, -1);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
    
    /**
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody pictureToRead - Enables to access HTTP request body as the input parameter,
     *                              and convert the value into java object with the declared 
     *                              Java type (using HttpMessageConverters). 
     *                              The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     */
    @RequestMapping(value = "/pictureImageDownload", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse pictureImageDownload(
           @RequestBody Picture pictureToRead) {
        if(pictureToRead == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_IMAGE_DOWNLOAD,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Picture to read is required!");
        }
        
        ServiceResponse response = PictureWSClient.imageDownload(pictureToRead);;
        response = ServiceUtil.evaluateServiceResponse(response);
        if(!response.isSuccess()) {
            return response;
        }
        if(response.getListOfPictures() == null
                || response.getListOfPictures().get(0) == null
                || response.getListOfPictures().get(0).getImageBytes() == null) {
            //Success but image is empty:
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.PICTURE_IMAGE_DOWNLOAD,
                    ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND,
                    MessageResourceUtil.getMessageValue("error_picture_image_notFound", null, null));
        }
        
        //ELSE: Good to go!
        //TODO:
        //byte[] imgBytes = svcResponse.getListOfPictures().get(0).getImageBytes();
        //Android: Bitmap imgBitmap = ImageUtil.decodePictureImageIntoBitmap(imgBytes, null, ...)
        //OR, translate it as JPEG? See com.trekcrumb.common.utility.FileUtil.writeImageToFileDirectory()
        return response;
    }

    
    
}