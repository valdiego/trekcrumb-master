package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.webapp.utility.WebAppUtil;

@Controller
public class HomeController {

    @RequestMapping("/home")
    public ModelAndView home(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        /*
         * Auto-login user if conditions are met.
         * Note: 
         * 1. If user request has the cookie, it means his browser has record of previously
         *    logged in user who has not logged out. We need to honor the auto login by validating 
         *    this UserAuthToken. 
         * 2. If successful, the page and all of its fragmewnts must ackonwledge the UserLogin in 
         *    Session. 
         */
        WebAppUtil.userAuthTokenAuthenticate(servletRequest);
        
        //Populate view and necessary data/bean:
        ModelAndView resultPage = new ModelAndView();
        resultPage.setViewName("page_home.jsp");
        WebAppUtil.populatePageIdentityForHome(servletRequest, resultPage);
        
        //Forms:
        resultPage.addObject("userLoginFormBean", new User());
        
        //Greeting section:
        resultPage = WebAppUtil.includeInforOrErrorMessages(servletRequest, resultPage, null);

        return resultPage;
    }
    
}