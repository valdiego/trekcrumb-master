package com.trekcrumb.webapp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.bean.UserSession;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonProperties;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppConstants;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.UserAuthTokenWSClient;
import com.trekcrumb.webserviceclient.UserWSClient;

@Controller
@RequestMapping("/user")
public class UserController {
    private final static Logger LOGGER = Logger.getLogger("com.trekcrumb.webapp.controller.UserController");
    
    /**
     * Usage: GET /user
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Usage: GET /user/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * User Signup - Initiates landing (form) page that serves for user starting point for
     * authentication such as User Login, User Create, User Forget and User Auto-Authentication.
     * 
     * Usage: GET /user/signup
     */
    @RequestMapping(value="/signup", method = RequestMethod.GET)
    public ModelAndView signup(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        /*
         * Verify:
         * 1. If user has been login for this session, OR
         * 2. If request comes with UserAuthToken cookie
         *    Note: If user request has the cookie, it means his browser has record of previously
         *    logged in user who has not logged out. We need to honor the auto login by validating 
         *    this UserAuthToken. Otherwise, we would have failed the purpose of the auto login scheme.
         */
        if(WebAppUtil.isUserSessionActive(servletRequest)
                || WebAppUtil.userAuthTokenAuthenticate(servletRequest)) {
            String infoMessage = MessageResourceUtil.getMessageValue(
                    "info_user_login_success", 
                    new String[] {((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser().getFullname()}, 
                    null);
            
            return retrieve_Me(servletRequest, servletResponse, infoMessage);
        }
        
        //Else, proceed with initializing the page:
        ModelAndView resultPage = new ModelAndView();
        resultPage.setViewName("page_user_signup.jsp");

        /* 
         * 1. Check if any pre-populated form beans
         *    This method also serves as a redirect landing page from other controllers such as
         *    when its submit form failed. In that case, check if the request has FlashAttribute with
         *    pre-populated form bean model.
         */
        User userToCreate = null;
        User userToLogin = null;
        User userWhoForget = null;
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(servletRequest);
        if (flashMap != null) {
            userToCreate = (User) flashMap.get("userCreateFormBean");
            userToLogin = (User) flashMap.get("userLoginFormBean");
            userWhoForget = (User) flashMap.get("userForgetFormBean");
        }
        if(userToCreate == null) {
            userToCreate = new User();
        }
        if(userToLogin == null) {
            userToLogin = new User();
        }
        if(userWhoForget == null) {
            userWhoForget = new User();
        }
        resultPage.addObject("userCreateFormBean", userToCreate);
        resultPage.addObject("userLoginFormBean", userToLogin);
        resultPage.addObject("userForgetFormBean", userWhoForget);
        
        /*
         * 2. Check if any pre-populated messages (info or error).
         */
        resultPage = WebAppUtil.includeInforOrErrorMessages(servletRequest, resultPage, null);
        
        return resultPage;
    }        
    
    /**
     * User Create - Submits form for back-end service processing.
     * Usage: POST /user/create/submit/form
     */
    @RequestMapping(value="/create/submit/form", method = RequestMethod.POST)
    public String create_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userCreateFormBean") User userToCreate,
            RedirectAttributes redirectAttributes) {
        //Validate legality:
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return "redirect:/trekcrumb/error/illegalAccess";
        }
        
        //Insert defaults:        
        userToCreate.setLocation(MessageResourceUtil.getMessageValue("info_user_locationEmpty", null, null));
        userToCreate.setSummary(MessageResourceUtil.getMessageValue("info_user_summaryEmpty", null, null));

        //Call back-end service:
        ServiceResponse svcResponse = UserWSClient.userCreate(userToCreate, null);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            //Redirect to the same form page: 
            redirectAttributes.addFlashAttribute("userCreateFormBean", userToCreate);
            redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
            return "redirect:/trekcrumb/user/signup?section=userCreate";
       } 
        
        //Success:
        //1. Destroy any current session first:
        WebAppUtil.sessionDestroy(servletRequest);
        
        //2. Then store user in session:
        HttpSession session = WebAppUtil.sessionInit(servletRequest);

        UserSession userSession = new UserSession();
        userSession.setUser(svcResponse.getUser());
        userSession.setUserAuthenticated(true);
        session.setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        
        //Message from server:
        String infoMsg = MessageResourceUtil.getMessageValue("info_user_create_success_NeedLogin", null, null); 
        redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsg);

        return "redirect:/trekcrumb/user/retrieve/me";
    }
    
    
    /**
     * User Login - Submits form for back-end service processing.
     * Usage: POST /user/login/submit/form
     */
    @RequestMapping(value="/login/submit/form", method = RequestMethod.POST)
    public String login_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userLoginFormBean") User userToLogin,
            RedirectAttributes redirectAttributes) {
        //Validate legality:
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return "redirect:/trekcrumb/error/illegalAccess";
        }

        /*
         * Verify:
         * 1. If user has been login for this session, OR
         * 2. If request comes with UserAuthToken cookie
         *    Note: If user request has the cookie, it means his browser has record of previously
         *    logged in user who has not logged out. We need to honor the auto login by validating 
         *    this UserAuthToken. Otherwise, we would have re-created new UserAutoToken and its cookie.
         */
        if(WebAppUtil.isUserSessionActive(servletRequest)
                || WebAppUtil.userAuthTokenAuthenticate(servletRequest)) {
            String infoMessage = MessageResourceUtil.getMessageValue(
                    "info_user_login_success", 
                    new String[] {((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser().getFullname()}, 
                    null);
            redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMessage);
            
            return "redirect:/trekcrumb/user/retrieve/me";
        }
        
        //Else, proceed with login:
        UserAuthToken userAuthTokenToCreate = null;
        String isRememberMe = servletRequest.getParameter(WebAppConstants.QUERYPARAM_NAME_IS_REMEMBER_ME);
        if(!ValidationUtil.isEmpty(isRememberMe) 
                && isRememberMe.trim().equalsIgnoreCase(WebAppConstants.QUERYPARAM_VALUE_TRUE)) {
            String deviceIdentity = servletRequest.getParameter(WebAppConstants.QUERYPARAM_NAME_DEVICE_IDENTITY);
            if(ValidationUtil.isEmpty(deviceIdentity)) {
                deviceIdentity = "WEBAPP_DEFAULT_DEVICE_IDENTITY";
            }
            
            userAuthTokenToCreate = new UserAuthToken();
            userAuthTokenToCreate.setDeviceIdentity(deviceIdentity);
        }

        return doUserLogin(servletRequest, userToLogin, userAuthTokenToCreate, redirectAttributes);
    }
    
    /**
     * User Refresh - Submits form for back-end service processing.
     * Usage: POST /user/refresh/submit/form
     */
    @RequestMapping(value="/refresh/submit/form", method = RequestMethod.POST)
    public String refresh_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userLoginRefreshForm") User userToLoginRefresh,
            RedirectAttributes redirectAttributes) {
        //Validate legality:
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return "redirect:/trekcrumb/error/illegalAccess";
        }
        if(!WebAppUtil.isUserLoginProfile(servletRequest, userToLoginRefresh.getUserId())) {
            return "redirect:/trekcrumb/error/illegalAccess";
        }
        
        return doUserLogin(servletRequest, userToLoginRefresh, null, redirectAttributes);
    }    
    
    /**
     * User Logout - Submits form for back-end service processing.
     * Usage: /user/logout/submit/form
     */
    @RequestMapping(value = "/logout/submit/form")
    public String logout_submit_form(
            HttpServletRequest servletRequest,
            RedirectAttributes redirectAttributes) {
        doUserLogout(servletRequest, redirectAttributes);
        
        /*
         * At this point:
         * 1. previous session has been destroyed and we would create new session
         * 2. Exist HTTPS protocol and return to guest user HTTP protocol
         */
        String logoutSuccessResultPage = CommonProperties.getProperty("webapp.url.user.logoutAfter");
        return "redirect:" + logoutSuccessResultPage;
    }
    
    /**
     * User Logout - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /user/logout/submit/AJAX
     */
    @RequestMapping(value="/logout/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse logout_submit_AJAX(
            HttpServletRequest servletRequest,
            RedirectAttributes redirectAttributes) {
        doUserLogout(servletRequest, redirectAttributes);
        
        /*
         * At this point:
         * 1. previous session has been destroyed and we would create new session
         * 2. Return success response and let the caller take care what's next
         */
        ServiceResponse svcResponse = CommonUtil.composeServiceResponseSuccess(null);
        svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        return svcResponse;
    }    

    /**
     * User Update - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody userToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /user/update/submit/AJAX/{updateSection}
     */
    @RequestMapping(value="/update/submit/AJAX/{updateSection}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse update_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody User userToUpdate,
            @PathVariable String updateSection) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = WebAppUtil.verifyUserHasAccessToProfile(
                servletRequest, servletResponse, userToUpdate, ServiceTypeEnum.USER_UPDATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Call back-end service:
        if("profile".equalsIgnoreCase(updateSection)) {
            svcResponse = UserWSClient.userUpdate(userToUpdate);
            
        } else if("profileImage".equalsIgnoreCase(updateSection)) {
            if(userToUpdate.getProfileImageBase64Data() == null
                    || userToUpdate.getProfileImageBase64Data().trim() == "") {
                svcResponse = CommonUtil.composeServiceResponseError(
                        null,
                        ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                        MessageResourceUtil.getMessageValue("error_picture_invalidParam_picEmpty", null, null));
                
            } else {
                byte[] imageBytes = Base64.decodeBase64(userToUpdate.getProfileImageBase64Data().getBytes());
                userToUpdate.setProfileImageBytes(imageBytes);
                userToUpdate.setProfileImageBase64Data(null);
                svcResponse = UserWSClient.userUpdate(userToUpdate);
            }
            
        } else if("accountEmail".equalsIgnoreCase(updateSection)) {
            svcResponse = UserWSClient.userUpdate(userToUpdate);
            
        } else {
            svcResponse = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_UPDATE,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        
        //Evaluates result:
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            return svcResponse;
        }
        
        //Success:
        UserSession userSession = new UserSession();
        userSession.setUser(svcResponse.getUser());
        userSession.setUserAuthenticated(true);
        servletRequest.getSession().setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        
        String infoMsg = MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null);
        svcResponse.setSuccessMessage(infoMsg);

        return svcResponse;
    }
    
    /**
     * User Update Password - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody userToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /user/updatePwd/submit/AJAX
     */
    @RequestMapping(value="/updatePwd/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse updatePassword_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody User userToUpdate) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = WebAppUtil.verifyUserHasAccessToProfile(
                servletRequest, servletResponse, userToUpdate, ServiceTypeEnum.USER_UPDATE);
        if(svcResponse != null) {
            return svcResponse;
        }

        //Call back-end service:
        //1. Verify old password
        User userToLogin = new User();
        userToLogin.setUsername(userToUpdate.getUsername());
        userToLogin.setPassword(userToUpdate.getPassword());
        svcResponse = UserWSClient.userLogin(userToLogin, null);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_UPDATE,
                    ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR,
                    "Current password is invalid!");
        }
        
        //2. Update new password
        userToUpdate.setPassword(userToUpdate.getPasswordNew());
        svcResponse = UserWSClient.userUpdate(userToUpdate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            return svcResponse;
        }
        
        //Success:
        UserSession userSession = new UserSession();
        userSession.setUser(svcResponse.getUser());
        userSession.setUserAuthenticated(true);
        servletRequest.getSession().setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        
        svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        return svcResponse;
    }    

    /**
     * User Deactivate - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody userToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /user/deactivate/submit/AJAX
     */
    @RequestMapping(value="/deactivate/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse deactivate_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody User userToUpdate) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = WebAppUtil.verifyUserHasAccessToProfile(
                servletRequest, servletResponse, userToUpdate, ServiceTypeEnum.USER_DEACTIVATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Call back-end service:
        svcResponse = UserWSClient.userDeActivate(userToUpdate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            return svcResponse;
        }
        
        //Success:
        WebAppUtil.sessionDestroy(servletRequest);
        
        //TODO: How to put success message?
        //String infoMsg = MessageResourceUtil.getMessageValue("info_user_deactivate_success", null, null); 
        return svcResponse;
    }
    
    /**
     * User Reactivate - Initiates landing (form) page.
     * Usage: GET /user/reactivate
     */
    @RequestMapping(value="/reactivate", method = RequestMethod.GET)
    public ModelAndView reactivate(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            RedirectAttributes redirectAttributes) {
        /* 
         * This method expects a non-empty userReactivateForm model (form) bean since valid 
         * initial access occurs ONLY from a failed login_submit(). 
         * However, it also serves as a redirect landing page when submit form failed. 
         * In both cases, check if the request has FlashAttribute model (form) bean.
         * Note in the case submit form failure, the request typically would have FlashAttribue 
         * ServiceResponse as well that contains the error detail.
         */
        User userToReactivate = null;
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(servletRequest);
        if (flashMap != null) {
            userToReactivate = (User)flashMap.get("userReactivateFormBean");
        }
        if(userToReactivate == null) {
            //Verify if user has been reactivated but s/he clicks BACK button:
            if(WebAppUtil.isUserSessionActive(servletRequest)) {
                return retrieve_Me(servletRequest, servletResponse, null);
            } else {
                //Redirect to login page:
                return signup(servletRequest, servletResponse);
            }
        }
        
        //Return target page:
        ModelAndView resultView = new ModelAndView();
        resultView.setViewName("page_user_reactivate.jsp");
        resultView.addObject("userReactivateFormBean", userToReactivate);
        return resultView;
    }
    
    /**
     * User Reactivate - Submits form for back-end service processing.
     * Usage: POST /user/reactivate/submit/form
     */
    @RequestMapping(value="/reactivate/submit/form", method = RequestMethod.POST)
    public String reactivate_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userReactivateForm") User userForm,
            RedirectAttributes redirectAttributes) {
        //Validate legality:
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return "redirect:/trekcrumb/error/illegalAccess";
        }
        if(userForm == null || userForm.getUserId() == null) {
            return "redirect:/trekcrumb/error/userNotAuthenticated";
        }

        //Call back-end service:
        User userToUpdate = new User();
        userToUpdate.setUserId(userForm.getUserId());
        ServiceResponse svcResponse = UserWSClient.userReActivate(userToUpdate);
            
        //Evaluates result:
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            //Redirect to the same form page: 
            redirectAttributes.addFlashAttribute("userReactivateForm", userForm);
            redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
            return "redirect:/trekcrumb/user/reactivate";
        } 
        
        //Success:
        HttpSession session = WebAppUtil.sessionInit(servletRequest);

        UserSession userSession = new UserSession();
        userSession.setUser(svcResponse.getUser());
        userSession.setUserAuthenticated(true);
        session.setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        
        //Message from server:
        String[] infoMsgArgs = {svcResponse.getUser().getFullname()};
        String infoMsg = MessageResourceUtil.getMessageValue("info_user_reactivate_success", infoMsgArgs, null); 
        redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsg);
        
        return "redirect:/trekcrumb/user/retrieve/me";
    }
    
    /**
     * User Password Forget - Submits form for back-end service processing.
     * Usage: POST /user/forget/submit/form
     */
    @RequestMapping(value="/forget/submit/form", method = RequestMethod.POST)
    public String forget_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userForgetFormBean") User userWhoForget,
            RedirectAttributes redirectAttributes) {
        //Call back-end service:
        ServiceResponse svcResponse = UserWSClient.userPasswordForget(userWhoForget);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            //Redirect to the same form page: 
            redirectAttributes.addFlashAttribute("userForgetFormBean", userWhoForget);
            redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
            return "redirect:/trekcrumb/user/signup?section=userForget";
        } 
        
        //Success:
        //Message from server:
        String infoMsg = MessageResourceUtil.getMessageValue("info_user_forget_successConfirm", null, null); 
        redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsg);
        return "redirect:/trekcrumb/user/signup?section=userForget";
    }    
    
    /**
     * User Reset - Initiates landing (form) page.
     * 
     * Usage: GET /user/reset
     */
    @RequestMapping(value="/reset", method = RequestMethod.GET)
    public ModelAndView reset(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        //Verify if user has been login for this session:
        if(WebAppUtil.isUserSessionActive(servletRequest)) {
            return retrieve_Me(servletRequest, servletResponse, null);
        }
        
        /* 
         * Check if any pre-populated form beans
         * This method also serves as a redirect landing page from other controllers such as
         * when its submit form failed. In that case, check if the request has FlashAttribute with
         * pre-populated form bean model.
         */
        User userWhoReset = null;
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(servletRequest);
        if (flashMap != null) {
            userWhoReset = (User) flashMap.get("userResetFormBean");
        }
        if(userWhoReset == null) {
            userWhoReset = new User();
        }
        
        ModelAndView resultPage = new ModelAndView();
        resultPage.addObject("userResetFormBean", userWhoReset);
        resultPage.setViewName("page_user_reset.jsp");
        return resultPage;
    }         
    
    /**
     * User Password Reset - Submits form for back-end service processing.
     * Usage: POST /user/reset/submit/form
     */
    @RequestMapping(value="/reset/submit/form", method = RequestMethod.POST)
    public String reset_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userResetFormBean") User userWhoReset,
            RedirectAttributes redirectAttributes) {
        //Validate legality:
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return "redirect:/trekcrumb/error/illegalAccess";
        }

        //Call back-end service:
        ServiceResponse svcResponse = UserWSClient.userPasswordReset(userWhoReset);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            String userDeactivatedRedirectPage = handleUserDeactivated(servletRequest, redirectAttributes, svcResponse);
            if(userDeactivatedRedirectPage != null) {
                return userDeactivatedRedirectPage;
            
            } else {
                /*
                 * Failure due to any regular error. Make sure to destroy any possibe current
                 * user session. Then redirect to the same form page.
                 */
                redirectAttributes.addFlashAttribute("userResetFormBean", userWhoReset);
                redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
                return "redirect:/trekcrumb/user/reset";
            }
        }
        
        //Success:
        return handleUserLoginSuccess(servletRequest, svcResponse, redirectAttributes);
    }            
    
    /**
     * User Retrieve: Retrieves current UserLogin details given UserSession and User entities 
     * are in request's session.
     * Usage: 
     * 1. GET /user/retrieve/me
     * 2. Other use is as a redirect target after actions such as: 'join/submit', 'login/submit' 
     *    and 'reactivate/submit' to ensure end-client browser URL changes to this target URL. 
     *    This to avoid user click browser refresh and accidentally resend the "submit" request.
     * 3. In the cases of redirect target after 'login/submit', the value of UserAuthToken may 
     *    exist (or not) depending user choose to 'remember me' or not. Either way, we return 
     *    this object to the view.
     *    
     * @param infoMessage - Any info message to be displayed at the final view page.
     */
    @RequestMapping(value="/retrieve/me", method = RequestMethod.GET)
    public ModelAndView retrieve_Me(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            String infoMessage) {
        //Validate legality:
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated(servletRequest, servletResponse);
        }
        if(!WebAppUtil.isUserSessionActive(servletRequest)) {
            //Auto-login with UserAuthToken, if available:
            boolean userAuthTokenSuccess = WebAppUtil.userAuthTokenAuthenticate(servletRequest);
            if(!userAuthTokenSuccess) {
                return ErrorController.handleError_userNotAuthenticated(servletRequest, servletResponse);
            }
        }

        //Compose return view:
        ModelAndView resultView = new ModelAndView();
        resultView.setViewName("page_user_view.jsp");
        
        //Get User from session:
        UserSession userSession = ((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION));
        User userLogin = userSession.getUser();

        /*
         * Care for UserAuthToken:
         * 1. If this is 'after login', we must keep UserAuthToken object so that the view (UI) can
         *    save it for cookies and use them for auto-login. This event should only occur ONCE 
         *    immediately after login!
         * 2. Else, ensure UserAuthToken object is nullified. This is to ensure the view (UI) does not keep 
         *    re-creating UserAuthToken cookies everytime we go to user profile page
         */
        if(userLogin.getUserAuthToken() != null) {
            String isAfterLogin = servletRequest.getParameter(WebAppConstants.QUERYPARAM_NAME_IS_AFTER_LOGIN);
            if(ValidationUtil.isEmpty(isAfterLogin) 
                    || !isAfterLogin.trim().equalsIgnoreCase(WebAppConstants.QUERYPARAM_VALUE_TRUE)) {
                userLogin.setUserAuthToken(null);
            }
        }
        
        //Compose ServiceResponse:
        ServiceResponse svcResponse = new ServiceResponse();
        svcResponse.setSuccess(true);
        List<User> listOfUsers = new ArrayList<User>();
        listOfUsers.add(userLogin);
        svcResponse.setListOfUsers(listOfUsers);
        resultView.addObject(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
        
        /*
         * Enrich the view:
         * 1. Unlike retrieve_byUsername, We do NOT want to populate page identity for "ME" 
         *    because this should not be shareable nor searchable by public media.
         */
        resultView = WebAppUtil.includeInforOrErrorMessages(servletRequest, resultView, infoMessage);
        
        //Eligible forms inside the view:
        resultView.addObject("userLoginRefreshForm", userLogin);
        
        return resultView;
    }    

    /**
     * User Retrieve: Retrieves User details given username as URL input parameter.
     * Usage: GET /user/retrieve/user1234
     */
    @RequestMapping(value="/retrieve/{username}", method = RequestMethod.GET)
    public ModelAndView retrieve_ByUsername(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @PathVariable String username) {
        ServiceResponse svcResponse = null;
        if(ValidationUtil.isEmpty(username)) {
            return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
        }
        
        //Verifies if the target username is the same as the UserLogin
        if(WebAppUtil.isUserSessionActive(servletRequest)) {
            if(username.trim().equalsIgnoreCase("me")) {
                return retrieve_Me(servletRequest, servletResponse, null);
            }
            
            User userLogin = 
                    ((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser();
            if(username.trim().equalsIgnoreCase(userLogin.getUsername())) {
                return retrieve_Me(servletRequest, servletResponse, null);
            }
            
        } else if(WebAppUtil.userAuthTokenAuthenticate(servletRequest)) {
            User userLogin = 
                    ((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser();
            if(username.trim().equalsIgnoreCase(userLogin.getUsername())) {
                return retrieve_Me(servletRequest, servletResponse, null);
            }
        }
        
        //Else:
        ModelAndView resultView = new ModelAndView();
        resultView.setViewName("page_user_view.jsp");

        User userToRetrieve = new User();
        userToRetrieve.setUsername(username);
        svcResponse = UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        resultView.addObject(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
        
        if(svcResponse.isSuccess()
                && svcResponse.getListOfUsers() != null
                && svcResponse.getListOfUsers().get(0) != null) {
            User userRetrieved = svcResponse.getListOfUsers().get(0);
            WebAppUtil.populatePageIdentityForUserDetail(servletRequest, resultView, userRetrieved);
        }
        
        return resultView;
    }
    
    /**
     * User Retrieve: Retrieves User details given username as URL input parameter.
     * Usage: GET /user/user1234
     */
    @RequestMapping(value="/{username}", method = RequestMethod.GET)
    public ModelAndView retrieve_ByUsername_shortcut(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @PathVariable String username) {
        return retrieve_ByUsername(servletRequest, servletResponse, username);
    }
    
    /**
     * User Profile Image Download.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form)
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody userToRead - Enables to access HTTP request body as the input parameter,
     *                           and convert the value into java object with the declared 
     *                           Java type (using HttpMessageConverters). 
     *                           The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                           value type is converted from its object type into a String
     *                           (using HttpMessageConverters) as either XML or JSON string.
     */
    @RequestMapping(value = "/userProfileImageDownload", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse userProfileImageDownload(
           @RequestBody User userToRead) {
        if(userToRead == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_PROFILE_IMAGE_DOWNLOAD,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "User to read is required!");
        }
        return UserWSClient.userProfileImageDownload(userToRead);
    }
    
    private String doUserLogin(
            HttpServletRequest servletRequest,
            User userToLogin,
            UserAuthToken userAuthTokenToCreate,
            RedirectAttributes redirectAttributes) {
        //Call back-end service:
        ServiceResponse svcResponse = UserWSClient.userLogin(userToLogin, userAuthTokenToCreate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            String userDeactivatedRedirectPage = handleUserDeactivated(servletRequest, redirectAttributes, svcResponse);
            if(userDeactivatedRedirectPage != null) {
                return userDeactivatedRedirectPage;
            
            } else {
                /*
                 * Failure due to any regular error. Make sure to destroy any possibe current
                 * user session. Then redirect to the same form page.
                 */
                WebAppUtil.sessionDestroy(servletRequest);

                redirectAttributes.addFlashAttribute("userLoginFormBean", userToLogin);
                redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
                return "redirect:/trekcrumb/user/signup?section=userLogin";
            }
        } 
        
        //Success:
        return handleUserLoginSuccess(servletRequest, svcResponse, redirectAttributes);
    }
    
    private void doUserLogout(
            HttpServletRequest servletRequest,
            RedirectAttributes redirectAttributes) {
        /* 
         * 1. Check if there is any message from redirect that we need to preserve
         *    (e.g., after deactivate_submit() successful, etc.)
         */
        String infoMsgFromServer = null;
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(servletRequest);
        if (flashMap != null) {
            infoMsgFromServer = (String)flashMap.get(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
        }
        
        //2. Retrieve UserAuthToken cookie to delete, if any:
        handleUserAuthTokenDuringLogout(servletRequest);

        //3.
        WebAppUtil.sessionDestroy(servletRequest);

        //4. Put back any message from redirect:
        if(redirectAttributes != null
                && !ValidationUtil.isEmpty(infoMsgFromServer)) {
            redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsgFromServer);
        }
    }
    
    /*
     * Handles next steps when user is successfully log in.
     */
    private String handleUserLoginSuccess(
            HttpServletRequest servletRequest,
            ServiceResponse svcResponse,
            RedirectAttributes redirectAttributes) {
        //1. Security101: Destroy any current session first:
        WebAppUtil.sessionDestroy(servletRequest);
        
        //2. Get UserLogin result
        User userLogin = svcResponse.getUser();
        
        /*
         * 3. Secure UserAuthToken data, if any, before we store in Session and View
         * -> UserID and AuthToken: We use it for authentication during auto-login
         * -> UserID and UserAuthToken ID: We use it for delete during logout
         */
        UserAuthToken userAuthToken = userLogin.getUserAuthToken();
        if(userAuthToken != null) {
            try {
                Cryptographer cryptoInstance = Cryptographer.getInstance();
                userAuthToken.setId(cryptoInstance.encrypt(userAuthToken.getId()));
                userAuthToken.setUserId(cryptoInstance.encrypt(userAuthToken.getUserId()));
                userAuthToken.setAuthToken(cryptoInstance.encrypt(userAuthToken.getAuthToken()));
            } catch(Exception e) {
                LOGGER.error("handleUserLoginSuccess() - Unexpected error while ecrypting UserAuthToken for userID [" 
                        + userAuthToken.getUserId() + "]. Setting UserAuthToken to NULL for security! Error: " 
                        + e.getMessage(), e);
                userLogin.setUserAuthToken(null);
            }
        }
        
        //4. Then store UserSession in server session:
        UserSession userSession = new UserSession();
        userSession.setUser(userLogin);
        userSession.setUserAuthenticated(true);
        HttpSession session = WebAppUtil.sessionInit(servletRequest);
        session.setAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION, userSession);
        
        //5. Greeting message:
        String infoMessage = MessageResourceUtil.getMessageValue(
                "info_user_login_success", 
                new String[] {((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser().getFullname()}, 
                null);
        redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMessage);
        
        return "redirect:/trekcrumb/user/retrieve/me?" 
            + WebAppConstants.QUERYPARAM_NAME_IS_AFTER_LOGIN + "="
            + WebAppConstants.QUERYPARAM_VALUE_TRUE;
    }
    
    /*
     * Handles next steps when user attempts to login but has deactivated account.
     */
    private String handleUserDeactivated(
            HttpServletRequest servletRequest,
            RedirectAttributes redirectAttributes,
            ServiceResponse svcResponse) {
        String redirectPage = null;
        if(svcResponse != null
                && svcResponse.getServiceError() != null
                && svcResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR
                && svcResponse.getUser() != null
                && !ValidationUtil.isEmpty(svcResponse.getUser().getUserId())) {
            //Failure due to deactivated user:
            redirectAttributes.addFlashAttribute("userReactivateFormBean", svcResponse.getUser());
            redirectPage ="redirect:/trekcrumb/user/reactivate";
        }
        
        return redirectPage;
    }
    
    private void handleUserAuthTokenDuringLogout(HttpServletRequest servletRequest) {
        System.out.println("\n\n\n **** handleUserAuthTokenDuringLogout() - STARTING ...");
        
        if(servletRequest.getHeader("Cookie") != null) {
            String userIDCrypted = null;
            String authTokenIDCrypted = null;
            Cookie[] arrayOfcookies = servletRequest.getCookies();
            for (int i = 0; i < arrayOfcookies.length; i++) {
                if(arrayOfcookies[i].getName().equalsIgnoreCase(WebAppConstants.COOKIE_NAME_USER_ID)) {
                    userIDCrypted = arrayOfcookies[i].getValue();
                } else if(arrayOfcookies[i].getName().equalsIgnoreCase(WebAppConstants.COOKIE_NAME_AUTH_TOKEN_ID)) {
                    authTokenIDCrypted = arrayOfcookies[i].getValue();
                }

                if(userIDCrypted != null && authTokenIDCrypted != null) {
                    //We get all we need from cookies:
                    break;
                }            
            }
            
            System.out.println("\n\n\n **** handleUserAuthTokenDuringLogout() - userIDCrypted [" + userIDCrypted + "] and authTokenIDCrypted [" + authTokenIDCrypted + "]");

            if(userIDCrypted != null && authTokenIDCrypted != null) {
                UserAuthToken userAuthTokenToDelete = null;
                try {
                    Cryptographer cryptoInstance = Cryptographer.getInstance();
                    userAuthTokenToDelete = new UserAuthToken();
                    userAuthTokenToDelete.setUserId(cryptoInstance.decrypt(userIDCrypted));
                    userAuthTokenToDelete.setId(cryptoInstance.decrypt(authTokenIDCrypted));
                                        
                    System.out.println("\n\n\n **** handleUserAuthTokenDuringLogout() - userID [" + userAuthTokenToDelete.getUserId() 
                            + "] and authTokenID [" + userAuthTokenToDelete.getId() + "]");

                    //Submit to back-end. Ignore any error!
                    System.out.println("\n\n\n **** handleUserAuthTokenDuringLogout() - Calling service ... ");
                    UserAuthTokenWSClient.delete(userAuthTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
                    System.out.println("\n\n\n **** handleUserAuthTokenDuringLogout() - COMPLETED \n\n\n");
                } catch(Exception e) {
                    LOGGER.error("logout_submit_form() - Unexpected error while trying to delete UserAuthToken! Error:  " + e.getMessage(), e);
                }
            }
        }
    }
    
}