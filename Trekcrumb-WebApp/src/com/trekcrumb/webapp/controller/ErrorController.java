package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;

@Controller
@RequestMapping("/error")
public class ErrorController {
    
    /**
     * Methods used as a redirect to handle scenarios where user is not authenticated.
     * 
     * The metods accepts a redirect call with URI '/error/userNotAuthenticated' for other
     * controllers that must return a String, AND a direct static method call for other
     * controllers that must return ModelAndView.

     * @return A view to display.
     */
    @RequestMapping(value="/userNotAuthenticated")
    public static ModelAndView handleError_userNotAuthenticated(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        servletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        
        String errorMessage = MessageResourceUtil.getMessageValue("error_user_LoginExpiredOrNone", null, null);
        ServiceResponse svcResponse = CommonUtil.composeServiceResponseError(
                null,
                ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                errorMessage);
        
        ModelAndView resultPage = new ModelAndView();
        resultPage.setViewName("page_user_signup.jsp");
        resultPage.addObject("userCreateFormBean", new User());
        resultPage.addObject("userLoginFormBean", new User());
        resultPage.addObject("userForgetFormBean", new User());
        resultPage.addObject(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
        return resultPage;
    }
    
    /**
     * Method used as a direct HTTP response to handle scenarios where user is not authenticated.
     * 
     * The metods accepts a redirect call with URI '/error/userNotAuthenticated/AJAX' for other
     * controllers that must return a String, OR a direct static method call for other
     * controllers that must return @ResponseBody.

     * @return ServiceResponse as response body.
     */
    @RequestMapping(value="/userNotAuthenticated/AJAX")
    public static @ResponseBody ServiceResponse handleError_userNotAuthenticated_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            ServiceTypeEnum serviceType) {
        servletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        String errorMessage = MessageResourceUtil.getMessageValue("error_user_LoginExpiredOrNone", null, null);
        return CommonUtil.composeServiceResponseError(
                serviceType,
                ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                errorMessage);
    }
    
    /**
     * Methods used as a redirect to handle scenarios where user access to certain operations
     * is illegal.
     * 
     * The metods accepts a redirect call with URI '/error/illegalAccess' for other
     * controllers that must return a String, OR a direct static method call for other
     * controllers that must return ModelAndView.

     * @return A view to display.
     */
    @RequestMapping(value="/illegalAccess")
    public static ModelAndView handleError_illegalAccess(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        servletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        String errorMessage = MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null);
        
        ModelAndView result = new ModelAndView();
        result.setViewName("page_errorDefault.jsp");
        result.addObject(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR, errorMessage);
        return result;
    }

    /**
     * Method used as a direct HTTP response to handle scenarios where user access to certain operations
     * is illegal.
     * 
     * The metods accepts a redirect call with URI '/error/illegalAccess/AJAX' for other
     * controllers that must return a String, AND a direct static method call for other
     * controllers that must return @ResponseBody.

     * @return ServiceResponse as response body.
     */
    @RequestMapping(value="/illegalAccess/AJAX")
    public static @ResponseBody ServiceResponse handleError_illegalAccess_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            ServiceTypeEnum serviceType) {
        servletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        String errorMessage = MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null);
        return CommonUtil.composeServiceResponseError(
                serviceType,
                ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                errorMessage);
    }

}
