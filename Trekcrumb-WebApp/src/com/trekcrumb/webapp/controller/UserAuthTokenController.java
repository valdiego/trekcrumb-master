package com.trekcrumb.webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.bean.UserSession;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.UserAuthTokenWSClient;

@Controller
@RequestMapping("/userauthtoken")
public class UserAuthTokenController {
    
    /**
     * Usage: GET /userauthtoken
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Usage: GET /userauthtoken/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * UserAuthToken Delete - Submits form for back-end service processing. 
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody userAuthTokenToDelete - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /userAuthToken/delete/submit/AJAX/
     */
    @RequestMapping(value="/delete/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse delete_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody UserAuthToken userAuthTokenToDelete) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        if(userAuthTokenToDelete == null 
                || userAuthTokenToDelete.getUserId() == null || userAuthTokenToDelete.getId() == null ) {
            return CommonUtil.composeServiceResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_autoLogin_invalidParam_authTokenEmpty", null, null));
        }
        if(!WebAppUtil.isUserLoginProfile(servletRequest, userAuthTokenToDelete.getUserId())) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
        }
        
        //Call back-end service:
        svcResponse = UserAuthTokenWSClient.delete(userAuthTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
        
        //Evaluates result:
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(!svcResponse.isSuccess()) {
            return svcResponse;
        }
        
        //Success:
        List<UserAuthToken> listOfUserAuthTokensRemain = svcResponse.getListOfUserAuthTokens();
        UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
        User userLogin = userSession.getUser();
        userLogin.setListOfAuthTokens(listOfUserAuthTokensRemain);
        
        String infoMsg = MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null);
        svcResponse.setSuccessMessage(infoMsg);
        return svcResponse;
    }    
    

    
}