package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.UserSession;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.TripWSClient;

@Controller
@RequestMapping("/trip")
public class TripController {

    /*
     * TODO:
     * Supported ONLY IF Spring version 4.1 is used, and set in the xml config: 
     * mvc:path-matching trailing-slash="true". 
     * This is to replace both defaultOpNoSlash() and defaultOpWithSlash().
     *  
     * Usage: GET /trip or /trip/
     * 
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOp(HttpServletRequest request) {
        return search(request);
    }
    */

    /**
     * Usage: GET /trip
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return search(servletRequest, servletResponse);
    }
    
    /**
     * Usage: GET /trip/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) { 
        return search(servletRequest, servletResponse);
    }
    
    /**
     * Trip Search - Initiates landing page.
     * Usage: GET /trip/search
     */
    @RequestMapping(value="/search")
    public ModelAndView search(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) { 
        ModelAndView resultView = new ModelAndView();
        resultView.setViewName("page_trip_search.jsp");
        return resultView;
    }
    
    /**
     * Trip Search - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody tripSearchCriteria - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /trip/search/submit/AJAX
     */
    @RequestMapping(value = "/search/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse search_submit_AJAX(
            @RequestBody TripSearchCriteria tripSearchCriteria) {
        if(tripSearchCriteria == null) {
            //Default:
            tripSearchCriteria = new TripSearchCriteria();
            tripSearchCriteria.setOffset(0);
            tripSearchCriteria.setNumOfRows(CommonConstants.RECORDS_NUMBER_MAX);
        }
        ServiceResponse svcResponse = TripWSClient.search(tripSearchCriteria);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
    
    /**
     * Trip Create - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody tripToCreate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /trip/create/submit/AJAX
     */
    @RequestMapping(value = "/create/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse create_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Trip tripToCreate) {
        //Validate legality:
        if(tripToCreate == null) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, ServiceTypeEnum.TRIP_CREATE);
        }
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated_AJAX(
                    servletRequest, servletResponse, ServiceTypeEnum.TRIP_CREATE);
        }
        if(!WebAppUtil.isUserSessionActive(servletRequest)) {
            boolean userAuthTokenSuccess = WebAppUtil.userAuthTokenAuthenticate(servletRequest);
            if(!userAuthTokenSuccess) {
                return ErrorController.handleError_userNotAuthenticated_AJAX(
                        servletRequest, servletResponse, ServiceTypeEnum.TRIP_CREATE);
            }
        }

        //Call back-end service:
        ServiceResponse svcResponse = TripWSClient.create(tripToCreate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        
        //Update UserSession:
        if(svcResponse != null && svcResponse.isSuccess()) {
            WebAppUtil.updateUserLoginInSession(servletRequest, 1, 0, 0);
        }
        
        return svcResponse;
    }
    
    /**
     * Trip Update - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody tripToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /trip/update/submit/AJAX
     */
    @RequestMapping(value = "/update/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse update_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Trip tripToUpdate) {
        ServiceResponse svcResponse = null;

        //Validate access legality:
        if(tripToUpdate == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_UPDATE,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, tripToUpdate.getId(), ServiceTypeEnum.TRIP_UPDATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Call back-end service:
        svcResponse = TripWSClient.update(tripToUpdate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_common_saveSuccess", null, null));
        }
        return svcResponse;
    }    
    
    /**
     * Trip Delete - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody tripToUpdate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /trip/update/submit/AJAX
     */
    @RequestMapping(value = "/delete/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse delete_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Trip tripToDelete) {
        ServiceResponse svcResponse = null;
        
        //Validate access legality:
        if(tripToDelete == null) {
            return CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_DELETE_BY_TRIPID,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR,
                    MessageResourceUtil.getMessageValue("error_user_illegalAccess", null, null));
        }
        svcResponse = WebAppUtil.verifyUserHasAccessToTrip(
                servletRequest, servletResponse, tripToDelete.getId(), ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Call back-end service:
        svcResponse = TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
            userSession.setTripSelected(null);
            
            WebAppUtil.updateUserLoginInSession(servletRequest, -1, 0, 0);
            svcResponse.setSuccessMessage(MessageResourceUtil.getMessageValue("info_trip_delete_success", null, null));
        } 
        return svcResponse;
    }        
    
    /**
     * Retrieves Trip details given username and TripID as URL input parameters.
     * Usage: GET /trip/retrieve/user1234/tripIDxyz
     */
    @RequestMapping(value="/retrieve/{username}/{tripID}", method = RequestMethod.GET)
    public ModelAndView retrieve_ByTripID(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @PathVariable String username,
            @PathVariable String tripID) {
        ServiceResponse svcResponse = null;

        ModelAndView resultView = new ModelAndView();
        resultView.setViewName("page_trip_view.jsp");
        
        //Validate:
        if(ValidationUtil.isEmpty(username) || ValidationUtil.isEmpty(tripID)) {
            svcResponse = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID,
                    ServiceErrorEnum.TRIP_SERVICE_TRIP_NOT_FOUND_ERROR,
                    MessageResourceUtil.getMessageValue("error_trip_notFound", null, null));
            resultView.addObject(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
            return resultView;
        }
        
        /*
         * Auto-login user if conditions are met.
         * Note: 
         * 1. If user request has the cookie, it means his browser has record of previously
         *    logged in user who has not logged out. We need to honor the auto login by validating 
         *    this UserAuthToken. 
         * 2. If successful, the page and all of its fragmewnts must ackonwledge the UserLogin in 
         *    Session. 
         */
        WebAppUtil.userAuthTokenAuthenticate(servletRequest);
        
        //Prep params:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(tripID);
        tripToRetrieve.setUsername(username);
        if(WebAppUtil.isUserSessionActive(servletRequest)) {
            //Provide userLogin in case the trip is private:
            tripToRetrieve.setUserID(
                    ((UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION))
                    .getUser().getUserId());
        }
        
        //Call back-end service:
        svcResponse = TripWSClient.retrieve(tripToRetrieve,  ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, -1, -1);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        resultView.addObject(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);

        if(svcResponse.isSuccess()) {
            if(svcResponse.getListOfTrips() != null
                    && svcResponse.getListOfTrips().get(0) != null) {
                Trip tripRetrieved = svcResponse.getListOfTrips().get(0);
                
                //1. Populate page identity:
                WebAppUtil.populatePageIdentityForTripDetail(servletRequest, resultView, tripRetrieved);
                
                //2. If userLogin and legal, put selected Trip into User session for update/add/favorite ops:
                if(WebAppUtil.isSessionFromCookie(servletRequest) 
                        && WebAppUtil.isUserSessionActive(servletRequest)) {
                    UserSession userSession = (UserSession) servletRequest.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
                    userSession.setTripSelected(tripRetrieved);
                    
                    WebAppUtil.isTripSelectedUserFavorite(servletRequest, tripRetrieved);
                }
            }
            
            //Check if there is any info message (Used only once for landing page to display)
            resultView = WebAppUtil.includeInforOrErrorMessages(servletRequest, resultView, null);
        }
        
        return resultView;
    }
    
    /**
     * Retrieves Trip details given ONLY a TripID as URL input parameter.
     * Usage: GET /trip/retrieve/tripIDxyz
     */
    @RequestMapping(value="/retrieve/{tripID}", method = RequestMethod.GET)
    public ModelAndView retrieve_ByTripID_shortcut(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @PathVariable String tripID) {
        /*
         * Set username to default "SHORTCUT" and call the valid controller method. 
         * HINT: 
         * 1. Trip Retrieve by TripID only requires TripID. The username parameter is only
         *    for URL presentation and not required in the back-end services.
         * 2. We provide this shortcut controller to accomodate viewing Trip Detail
         *    when a username is unknown (e.g., from User List of Comments, etc.)
         */
        return retrieve_ByTripID(servletRequest, servletResponse, "SHORTCUT", tripID);

    }
    
    /**
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody tripToRetrieve - Enables to access HTTP request body as the input parameter,
     *                              and convert the value into java object with the declared 
     *                              Java type (using HttpMessageConverters). 
     *                              The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     */
    @RequestMapping(value = "/retrieve_byUsername/{offset}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse retrieve_byUsername(
            @RequestBody Trip tripToRetrieve,
            @PathVariable int offset) {
        ServiceResponse svcResponse = TripWSClient.retrieve(
                tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, offset, CommonConstants.RECORDS_NUMBER_MAX);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
    
}