package com.trekcrumb.webapp.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webapp.utility.MessageResourceUtil;

@Controller
@RequestMapping("/support")
public class SupportController {
    /**
     * Usage: GET /support
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return faq(servletRequest, servletResponse);
    }
    
    /**
     * Usage: GET /support/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) { 
        return faq(servletRequest, servletResponse);
    }
    
    /**
     * Support Terms - Initiates landing page
     * 
     * Usage: GET /support/terms
     */
    @RequestMapping(value="/terms", method = RequestMethod.GET)
    public ModelAndView terms(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        ModelAndView resultPage = new ModelAndView();
        resultPage.setViewName("page_support_terms.jsp");
        return resultPage;
    }        
    
    /**
     * Support Privacy - Initiates landing page
     * 
     * Usage: GET /support/privacy
     */
    @RequestMapping(value="/privacy", method = RequestMethod.GET)
    public ModelAndView privacy(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        ModelAndView resultPage = new ModelAndView();
        resultPage.setViewName("page_support_privacy.jsp");
        return resultPage;
    }        
    
    /**
     * Support FAQ - Initiates landing page
     * 
     * Usage: GET /support/faq
     */
    @RequestMapping(value="/faq", method = RequestMethod.GET)
    public ModelAndView faq(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        ModelAndView resultPage = new ModelAndView();
        resultPage.setViewName("page_support_faq.jsp");
        return resultPage;
    }        

    /**
     * Support Contact Us - Initiates landing (form) page
     * 
     * Usage: GET /support/contactus
     */
    @RequestMapping(value="/contactus", method = RequestMethod.GET)
    public ModelAndView contactUs(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        /* 
         * This method also serves as a redirect landing page when submit form failed. 
         * If it is, check if the request has FlashAttribute model (form) bean.
         * Note that the request typically would have FlashAttribue ServiceResponse as well.
         */
        User userWhoContactUs = null;
        Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(servletRequest);
        if (flashMap != null) {
            userWhoContactUs = (User) flashMap.get("userContactUsFormBean");
        }
        if(userWhoContactUs == null) {
            userWhoContactUs = new User();
        }

        ModelAndView resultPage = new ModelAndView();
        resultPage.addObject("userContactUsFormBean", userWhoContactUs);
        resultPage.setViewName("page_support_contactUs.jsp");
        return resultPage;
    }        
    
    @RequestMapping(value="/contactus/submit/form", method = RequestMethod.POST)
    public String contactUs_submit_form(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @ModelAttribute("userContactUsFormBean") User userWhoContactUs,
            RedirectAttributes redirectAttributes) {

        //Call back-end service:
        try {
            UserManager.userContactUs(userWhoContactUs);
        } catch(Exception e) {
            //Redirect to the same form page: 
            ServiceResponse svcResponse = CommonUtil.composeServiceResponseError(
                    ServiceTypeEnum.USER_CONTACT_US, 
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
            redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE, svcResponse);
            redirectAttributes.addFlashAttribute("userContactUsFormBean", userWhoContactUs);
            return "redirect:/trekcrumb/support/contactus";
        }

        //Success:
        String infoMsg = MessageResourceUtil.getMessageValue("info_support_contactUs_successConfirm", null, null); 
        redirectAttributes.addFlashAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO, infoMsg);
        return "redirect:/trekcrumb/support/contactus";
    }
    

}