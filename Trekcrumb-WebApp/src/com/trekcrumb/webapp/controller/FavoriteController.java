package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.FavoriteWSClient;

@Controller
@RequestMapping("/favorite")
public class FavoriteController {

    /**
     * Usage: GET /favorite
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }

    /**
     * Usage: GET /favorite/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Favorite Create - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody favoriteToCreate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /favorite/create/submit/AJAX
     */
    @RequestMapping(value = "/create/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse create_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Favorite favoriteToCreate) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = verifyUserAccessToFavoriteOps(
                servletRequest, servletResponse, favoriteToCreate, ServiceTypeEnum.FAVORITE_CREATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Else:
        svcResponse = FavoriteWSClient.create(favoriteToCreate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            WebAppUtil.updateUserLoginInSession(servletRequest, 0, 1, 0);
        }
        
        return svcResponse;
    }
    
    /**
     * Favorite Delete - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody favoriteToDelete - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /favorite/delete/submit/AJAX
     */
    @RequestMapping(value = "/delete/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse delete_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Favorite favoriteToDelete) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = verifyUserAccessToFavoriteOps(
                servletRequest, servletResponse, favoriteToDelete, ServiceTypeEnum.FAVORITE_DELETE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Else:
        svcResponse = FavoriteWSClient.delete(favoriteToDelete);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            WebAppUtil.updateUserLoginInSession(servletRequest, 0, -1, 0);
        }
        
        return svcResponse;
    }
    
    /**
     * Retrieves Users who favorite a Trip given its tripID.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody trip - Enables to access HTTP request body as the input parameter,
     *                     and convert the value into java object with the declared 
     *                     Java type (using HttpMessageConverters). 
     *                     The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                     value type is converted from its object type into a String
     *                     (using HttpMessageConverters) as either XML or JSON string.
     *             
     * Usage: POST /favorite/retrieveByTrip/0
     */
    @RequestMapping(value="/retrieveByTrip/AJAX/{offset}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse retrieve_byTrip(
            @RequestBody Trip trip,
            @PathVariable int offset) {
        ServiceResponse svcResponse = FavoriteWSClient.retrieveByTrip(trip, null, offset, CommonConstants.RECORDS_NUMBER_MAX);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
    
    /**
     * Retrieves Trips whom a user has favorited given the username.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody user - Enables to access HTTP request body as the input parameter,
     *                     and convert the value into java object with the declared 
     *                     Java type (using HttpMessageConverters). 
     *                     The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                     value type is converted from its object type into a String
     *                     (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /favorite/retrieveByUser/0
     */
    @RequestMapping(value="/retrieveByUser/AJAX/{offset}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse retrieve_byUser(
            @RequestBody User user,
            @PathVariable int offset) {
        ServiceResponse svcResponse = FavoriteWSClient.retrieveByUser(user, null, offset, CommonConstants.RECORDS_NUMBER_MAX);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }    
    
    private ServiceResponse verifyUserAccessToFavoriteOps(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Favorite favorite,
            ServiceTypeEnum serviceType) {
        if(favorite == null) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        if(!WebAppUtil.isUserSessionActive(servletRequest)) {
            boolean userAuthTokenSuccess = WebAppUtil.userAuthTokenAuthenticate(servletRequest);
            if(!userAuthTokenSuccess) {
                return ErrorController.handleError_userNotAuthenticated_AJAX(
                        servletRequest, servletResponse, serviceType);
            }
        }
        if(!WebAppUtil.isUserLoginProfile(servletRequest, favorite.getUserID())) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        
        //All good:
        return null;
    }
    
}