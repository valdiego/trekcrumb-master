package com.trekcrumb.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.webapp.utility.ServiceUtil;
import com.trekcrumb.webapp.utility.WebAppUtil;
import com.trekcrumb.webserviceclient.CommentWSClient;

@Controller
@RequestMapping("/comment")
public class CommentController {

    /**
     * Usage: GET /comment
     */
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView defaultOpNoSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }

    /**
     * Usage: GET /comment/
     */
    @RequestMapping(value="/", method = RequestMethod.GET) 
    public ModelAndView defaultOpWithSlash(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        return ErrorController.handleError_illegalAccess(servletRequest, servletResponse);
    }
    
    /**
     * Comment Create - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody commentToCreate - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /comment/create/submit/AJAX
     */
    @RequestMapping(value = "/create/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse create_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Comment commentToCreate) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = verifyUserAccessToCommentOps(
                servletRequest, servletResponse, commentToCreate, ServiceTypeEnum.COMMENT_CREATE);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Else:
        svcResponse = CommentWSClient.create(commentToCreate);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            WebAppUtil.updateUserLoginInSession(servletRequest, 0, 0, 1);
        }
        
        return svcResponse;
    }
    
    /**
     * Comment Retrieve by Trip.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody comment - Enables to access HTTP request body as the input parameter,
     *                     and convert the value into java object with the declared 
     *                     Java type (using HttpMessageConverters). 
     *                     The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                     value type is converted from its object type into a String
     *                     (using HttpMessageConverters) as either XML or JSON string.
     *             
     * Usage: POST /comment/retrieveByTrip/0
     */
    @RequestMapping(value="/retrieveByTrip/AJAX/{offset}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse retrieveByTrip(
            @RequestBody Comment comment,
            @PathVariable int offset) {
        ServiceResponse svcResponse = 
                CommentWSClient.retrieve(comment, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, offset, CommonConstants.RECORDS_NUMBER_MAX);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }
    
    /**
     * Comment Retrieve by User.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody comment - Enables to access HTTP request body as the input parameter,
     *                     and convert the value into java object with the declared 
     *                     Java type (using HttpMessageConverters). 
     *                     The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                     value type is converted from its object type into a String
     *                     (using HttpMessageConverters) as either XML or JSON string.
     *             
     * Usage: POST /comment/retrieveByTrip/0
     */
    @RequestMapping(value="/retrieveByUser/AJAX/{offset}", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse retrieveByUser(
            @RequestBody Comment comment,
            @PathVariable int offset) {
        ServiceResponse svcResponse = CommentWSClient.retrieve(
                comment, 
                ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, 
                OrderByEnum.ORDER_BY_CREATED_DATE, 
                offset, 
                CommonConstants.RECORDS_NUMBER_MAX);
        return ServiceUtil.evaluateServiceResponse(svcResponse);
    }    
    
    /**
     * Comment Delete - Submits form for back-end service processing.
     * 1. This method accepts input and returns output in JSON formats, so that it can
     *    accepts direct call via AJAX without the typical submit form.
     * 2. It accepts only POST method, such that no one can copy-paste the URL on browser to 'GET'
     *    the data.
     * @RequestBody favoriteToDelete - Enables to access HTTP request body as the input parameter,
     *                             and convert the value into java object with the declared 
     *                             Java type (using HttpMessageConverters). 
     *                             The input string can be either XML or JSON format.
     * @return @ResponseBody ServiceResponse - Enables to write the response in HTTP body. The return
     *                              value type is converted from its object type into a String
     *                              (using HttpMessageConverters) as either XML or JSON string.
     *                              
     * Usage: POST /favorite/delete/submit/AJAX
     */
    @RequestMapping(value = "/delete/submit/AJAX", method = RequestMethod.POST)
    public @ResponseBody ServiceResponse delete_submit_AJAX(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,
            @RequestBody Comment commentToDelete) {
        ServiceResponse svcResponse = null;

        //Validate legality:
        svcResponse = verifyUserAccessToCommentOps(
                servletRequest, servletResponse, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
        if(svcResponse != null) {
            return svcResponse;
        }
        
        //Else:
        svcResponse = CommentWSClient.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
        svcResponse = ServiceUtil.evaluateServiceResponse(svcResponse);
        if(svcResponse.isSuccess()) {
            WebAppUtil.updateUserLoginInSession(servletRequest, 0, 0, -1);
        }
        
        return svcResponse;
    }
    
    private ServiceResponse verifyUserAccessToCommentOps(
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse,            
            @RequestBody Comment comment,
            ServiceTypeEnum serviceType) {
        if(comment == null) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        if(!WebAppUtil.isSessionFromCookie(servletRequest)) {
            return ErrorController.handleError_userNotAuthenticated_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        if(!WebAppUtil.isUserSessionActive(servletRequest)) {
            boolean userAuthTokenSuccess = WebAppUtil.userAuthTokenAuthenticate(servletRequest);
            if(!userAuthTokenSuccess) {
                return ErrorController.handleError_userNotAuthenticated_AJAX(
                        servletRequest, servletResponse, serviceType);
            }
        }
        if(!WebAppUtil.isUserLoginProfile(servletRequest, comment.getUserID())) {
            return ErrorController.handleError_illegalAccess_AJAX(
                    servletRequest, servletResponse, serviceType);
        }
        
        //All good:
        return null;
    }
    
}