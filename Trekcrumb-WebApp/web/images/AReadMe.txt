####################################################################################################
# WebApp Images:
# NOTE:
# 1. DO NOT add/edit/delete ANY image files directly into/from this folder.
# 2. Image Files (PNG/JPG):  When any, they are generated from BASE library with ant build.xml. 
#    Therefore, you should add/edit/delete these files from that base library source: Base/images
# 3. Icon Fonts: For any menu icons, we no longer user image files. Instead, we use Web app 
#    IconFont technology. Also, while there were few 3rd-party libraries out there, we chose
#    FONTAWESOME.
# 4. References: 
#    - http://fontawesome.io/icons/
####################################################################################################
