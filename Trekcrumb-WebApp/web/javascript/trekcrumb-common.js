"use strict";

function common_window_getWidth() {
    var width = 0;
    if(window.innerWidth) {
        width = window.innerWidth;
    } else if(self.innerWidth) {
        width = self.innerWidth;
    } else if(document.documentElementWidth) {
        width = document.documentElementWidth;
    } else if(document.documentElement && document.documentElement.clientWidth) {
        width = document.documentElement.clientWidth;
    } else if(document.body && document.body.clientWidth) {
        width = document.body.clientWidth;
    }
    return width;
}

function common_window_getHeight() {
    var height = 0;
    if(window.innerHeight) {
        height = window.innerHeight;
    } else if(self.innerHeight) {
        height = self.innerHeight;
    } else if(document.documentElementHeight) {
        height = document.documentElementHeight;
    } else if(document.documentElement && document.documentElement.clientHeight) {
        height = document.documentElement.clientHeight;
    } else if(document.body && document.body.clientHeight) {
        height = document.body.clientHeight;
    }
    return height;
}

function common_window_evaluateOrientation() {
    if(common_window_getWidth() < common_window_getHeight()) {
        mDevice_orientation = DEVICE_ORENTATION_POTRAIT;
    } else {
        mDevice_orientation = DEVICE_ORENTATION_LANDSCAPE;
    }
}

function common_window_identity() {
    //var browserIdentity = navigator.platform;
    //browserIdentity += "_" + navigator.appCodeName;
    //browserIdentity += "_" + navigator.appName;
    //browserIdentity += "_" + navigator.appVersion;
    
    var browserIdentity = "Browser";
    browserIdentity += " " + platform.name.toUpperCase();
    browserIdentity += " v." + platform.version;
    browserIdentity += " on " + platform.os;
    if(platform.product != null) {
        browserIdentity += " " + platform.product;
    }
    
    return browserIdentity;
}

/*
 * Checks if the device browser is mobile version (return true) 
 * or desktop version (return false)
 * REF:
 * 1. http://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
 * 2. http://detectmobilebrowsers.com
 */
function common_window_isMobileBrowser() {
    var check = false;
    
    //Regex function to evaluate browser's UserAgent value:
    (function(a) { if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true })(navigator.userAgent||navigator.vendor||window.opera);
  
    return check;
}

function common_AJAX_isSupported() {
    if(window.XMLHttpRequest) {
        //Modern browsers (IE7+, Firefox, Chrome, Opera, Safari) supports XMLHTTPRequest:
        mDevice_isSupportAJAX = true;
        return true;
    }
    
    //Else:
    console.error("ERROR: " + ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED);
    mDevice_isSupportAJAX = false;
    return false;
}

/*
 * Creates a new XMLHttpRequest object that supports a cross-domain HTTP request-response 
 * interaction with AJAX webservice calls. 
 * Note:
 * 1. Cross-domain interaction AJAX calls violate Javascript's fundamental "Same Origin Policy". 
 * 2. In order to enable cross-domain AJAX call, the target server supports it by having 
 *    it configured to configure Access-Control-Allow-Origin header
 * 3. Also, the client browser must supports Cross-Origin Resource Sharing (CORS), and 
 *    set its content-type to be text/plain
 */
function common_AJAX_getCORSRequest(httpMethod, targetURL) {
    if(!mDevice_isSupportAJAX) {
        console.error("ERROR: " + ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED);
        return null;
    }
    
    var xmlhttpRequest = new XMLHttpRequest();
    if ("withCredentials" in xmlhttpRequest) {
        //Chrome/Firefox/Opera/Safari supports for CORS:
        xmlhttpRequest.open(httpMethod, targetURL, true);
        xmlhttpRequest.setRequestHeader("Content-type","text/plain");
        return xmlhttpRequest;

    } else if (typeof XDomainRequest != "undefined") {
        //IE supports for CORS.
        xmlhttpRequest = new XDomainRequest();
        xmlhttpRequest.open(httpMethod, targetURL);
        xmlhttpRequest.setRequestHeader("Content-type","text/plain");
        return xmlhttpRequest;

    } else {
        console.error("ERROR: Client browser does NOT support CORS!");
        return null;
    }    
}

function common_AJAX_opsError_show(pageName, errorMsgOrig, errorMsgDisplay) {
    console.error("ERROR: Encounter failure during AJAX call! Original message [" + errorMsgOrig + "]. Display message [" + errorMsgDisplay + "]");
    common_progressBarWidget_hide(pageName);
    
    var errorMsgArray = new Array();
    errorMsgArray.push(errorMsgDisplay);
    common_errorOrInfoMessage_show(errorMsgArray, null);
}

/*
 * Checks if the client browser used supports Locale Storage (both localStorage and sessionStorage),
 * AND its setting provide permission to access it.
 */
function common_localeStorage_isSupported() {
    try {
        if(typeof window.localStorage != 'undefined' && window.sessionStorage != 'undefined') {
            mDevice_isSupportLocaleStorage = true;
            return true;
        }
    } catch(error) {
        console.warn("WARNING: " + ERROR_MSG_CLIENT_LOCALSTORAGE_NOT_SUPPORTED + " Error: " + error.message);
        mDevice_isSupportLocaleStorage = false;
        return false;
    }        

    //Else:
    console.warn("WARNING: " + ERROR_MSG_CLIENT_LOCALSTORAGE_NOT_SUPPORTED + " Error: " + error.message);
    mDevice_isSupportLocaleStorage = false;
    return false;
}

function common_localeStorage_setItem(storageType, key, value) {
    if(mDevice_isSupportLocaleStorage) {
        if(storageType == CACHE_TYPE_LOCALESTORAGE_LOCAL) {
            try {
                localStorage.setItem(key, value);
            } catch(error) {
                console.error("common_localeStorage_setItem() - ERROR: Caching [" + key + "] into " + CACHE_TYPE_LOCALESTORAGE_LOCAL 
                        + " with error: " + error.message 
                        + " --> . Attempt to clear storage and retry.");
                common_localeStorage_clear(storageType, null);
                localStorage.setItem(key, value);
            }
            
        } else if(storageType == CACHE_TYPE_LOCALESTORAGE_SESSION) {
            try {
                sessionStorage.setItem(key, value);
            } catch(error) {
                console.error("common_localeStorage_setItem() - ERROR: Caching [" + key + "] into " + CACHE_TYPE_LOCALESTORAGE_SESSION 
                        + " with error: " + error.message 
                        + " --> . Attempt to clear storage and retry.");
                common_localeStorage_clear(storageType, null);
                sessionStorage.setItem(key, value);
            }
            
        } else {
            console.error("common_localeStorage_setItem() - ERROR: Invalid Locale Storage type [" + storageType + "]");
        }
    }
}

function common_localeStorage_clear(storageType, keyword) {
    if(mDevice_isSupportLocaleStorage) {
        if(storageType == CACHE_TYPE_LOCALESTORAGE_LOCAL) {
            if(keyword == undefined || keyword == null) {
                //Clear ALL:
                localStorage.clear();
            } else {
                /* 
                 * Clear only those whose keys matches keyword.
                 * We iterate over the storage object in reverse order, because when you remove the item, 
                 * the indexes get changed. This to avoid the loop to miss some items.
                 */
                var i = localStorage.length;
                var pattern = null;
                var key = null;
                while(i--) {
                    key = localStorage.key(i);
                    pattern = new RegExp(keyword);
                    if(pattern.test(key)) {
                        localStorage.removeItem(key);
                    }  
                }
            }
            
        } else if(storageType == CACHE_TYPE_LOCALESTORAGE_SESSION) {
            if(keyword == undefined || keyword == null) {
                //Clear ALL:
                sessionStorage.clear();
            } else {
                /* 
                 * Clear only those whose keys matches keyword.
                 * We iterate over the storage object in reverse order, because when you remove the item, 
                 * the indexes get changed. This to avoid the loop to miss some items.
                 */
                var i = sessionStorage.length;
                var pattern = null;
                var key = null;
                while(i--) {
                    key = sessionStorage.key(i);
                    pattern = new RegExp(keyword);
                    if(pattern.test(key)) {
                        sessionStorage.removeItem(key);
                    }  
                }
            }
            
        } else {
            console.error("common_localeStorage_clear() - ERROR: Invalid Locale Storage type [" + storageType + "]");
        }
    }
}

/*
 * Generic function to store current page's pagination parameters into session caching storage.
 * -> JS default is to ALWAYS store and retrieve values as Strings
 */
function common_localeStorage_cacheCurrentListState(
        pageName,
        cacheKeyOffsetCurrent,
        cacheKeyPageCurrent,
        cacheKeyTotalRecords) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
        
    if(mDevice_isSupportLocaleStorage) {
        var pageListLayout = document.getElementById(pageName + 'paginationListLayoutID');
        if(pageListLayout != undefined && pageListLayout != null) {
            var offset = pageListLayout.getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT);
            var page = pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
            var numRecordsTotal = pageListLayout.getAttribute(VARATTR_PAGE_LIST_NUMBER_RECORDS_TOTAL);
            try {
                sessionStorage.setItem(cacheKeyOffsetCurrent, offset);
                sessionStorage.setItem(cacheKeyPageCurrent, page);
                sessionStorage.setItem(cacheKeyTotalRecords, numRecordsTotal);
            } catch(error) {
                console.error("common_localeStorage_cacheCurrentListState() - ERROR: Caching data failed with error: " + error.message 
                        + " --> . Attempt to clear storage and retry.");
                common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, null);
                sessionStorage.setItem(cacheKeyOffsetCurrent, offset);
                sessionStorage.setItem(cacheKeyPageCurrent, page);
                sessionStorage.setItem(cacheKeyTotalRecords, numRecordsTotal);
            }
        }
    }
}

function common_fileAPIReader_isSupported() {
    if(window.File && window.FileReader && window.Blob) {
        return true;
    } else {
        console.error("ERROR: " + ERROR_MSG_CLIENT_FILEAPI_READER_NOT_SUPPORTED);
        return false;
    }
}

function common_fileAPIReader_inputImageFile_selectAndShow(inputElement) {
    var isFileSelected = true;
    var fileSelected = null;
    if(!inputElement.files || !inputElement.files[0] ) {
        isFileSelected = false;
        
    } else if(!common_fileAPIReader_isSupported()) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_FILEAPI_READER_NOT_SUPPORTED], null);
        isFileSelected = false;
        
    } else { 
        fileSelected = inputElement.files[0];
        if(!fileSelected.type.match('image.*')) {
            common_errorOrInfoMessage_show([ERROR_MSG_PICTURE_CREATE_IMAGE_INVALID_FILE], null);
            isFileSelected = false;
        }
    }
    
    if(!isFileSelected) {
        document.getElementById("imagePreviewSectionID").style.display = "none";
        document.getElementById("imagePreviewID").setAttribute('src', "");
        return;
    }
    
    //Else:
    common_errorOrInfoMessage_hide();
    document.getElementById("imagePreviewSectionID").style.display = "block";
    var imageFilePreview = document.getElementById("imagePreviewID");

    var reader = new FileReader();
    reader.onload = function(e) {
        imageFilePreview.setAttribute('src', e.target.result);
    };
    reader.onerror = function(e) {
        console.error("common_fileAPI_inputImageFile_selectAndShow() - ERROR: " + e);
        common_errorOrInfoMessage_show([ERROR_MSG_PICTURE_CREATE_IMAGE_PREVIEW_FAIL], null);
        document.getElementById("imagePreviewSectionID").style.display = "none";
        document.getElementById("imagePreviewID").setAttribute('src', "");
    };
    
    /*
     * This op is async and will invoke its onload/onerror when completed.
     * The result is file's data as Base64 encoded String.
     */
    reader.readAsDataURL(fileSelected);
}

function common_fileAPIReader_inputImageFile_cancelAndHide() {
    document.getElementById("imageFileInputID").value = "";
    document.getElementById("imagePreviewSectionID").style.display = "none";
    document.getElementById("imagePreviewID").setAttribute('src', "");
}

/*
 * NOTE (7/2017): 
 * We used to use 'navigator.cookieEnabled == true/false/undefined' during cookie
 * validation, but this navigator function cannot guarantee to be supported
 * across browsers and their setting. 
 * Example: In MS Explorer 11 browser, when its security setting is set to 'high', the function 
 * always return 'true'. 
 * 
 * Therefore, we used most commonly supported method 'document.cookie'.
 */
function common_cookie_isSupported() {
    document.cookie="testcookie";
    if(document.cookie != null
            && document.cookie.indexOf("testcookie") != -1) {
        mDevice_isSupportCookie = true;
        
        //Delete testcookie:
        document.cookie = "testcookie; expires=Thu, 01-Jan-1970 00:00:01 GMT";
        
    } else {
        console.error("ERROR: " + ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED);
        mDevice_isSupportCookie = false;
    }
    return mDevice_isSupportCookie;
}

function common_cookie_create(name, value, expireInMilliSeconds) {
    var expires = "";
    if (expireInMilliSeconds != null) {
        var date = new Date();
        date.setTime(date.getTime() + expireInMilliSeconds);
        expires = date.toGMTString();
    }
    var cookieString = name + "=" + value + "; expires=" + expires + "; path=" + APP_ROOT_URI + "/";
    document.cookie = cookieString;
}

function common_cookie_read(name) {
    //TODO - not implemented
    /*
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
    */
}

function common_cookie_delete(name) {
    common_cookie_create(name, "", -2);
}

function common_errorOrInfoMessage_show(errorMessageArray, infoMessage) {
    var errorAndInfoLayoutElement = document.getElementById('errorAndInfoLayoutID');
    if(errorAndInfoLayoutElement != undefined) {
        //Error messages:
        var errorLayoutElement = document.getElementById('errorLayoutID');
        if(errorMessageArray && errorMessageArray.length > 0) {
            var errorMessages = null;
            if(errorMessageArray.length == 1) {
                errorMessages = errorMessageArray[0];
                
            } else {
                //Display as a List:
                errorMessages = "<ul>";
                var len = errorMessageArray.length;
                for(var i = 0; i < len; ++i) {
                    errorMessages += "<li>" + errorMessageArray[i] + "</li>";
                }
                errorMessages += "</ul>";
            }
            
            document.getElementById('errorContentID').innerHTML = errorMessages;
            errorLayoutElement.style.display = "block";
        } else {
            document.getElementById('errorContentID').innerHTML = "";
            errorLayoutElement.style.display = "none";
        }
        
        //Info message:
        var isDisplayInfoMessage = false;
        var infoLayoutElement = document.getElementById('infoLayoutID');
        if(infoMessage != undefined && infoMessage != null 
                && infoMessage.trim() != "" && infoMessage.trim().toLowerCase() != "null") {
            document.getElementById('infoContentID').innerHTML = infoMessage;
            infoLayoutElement.style.display = "block";
            isDisplayInfoMessage = true;
        } else {
            document.getElementById('infoContentID').innerHTML = "";
            infoLayoutElement.style.display = "none";
        }
        
        //Display them:
        if(errorLayoutElement.style.display == "block"
            || infoLayoutElement.style.display == "block") {
            errorAndInfoLayoutElement.style.display = "block";
            
            //In case they were hidden due to prior infoMessage:
            errorAndInfoLayoutElement.style.visibility = "visible";
            errorAndInfoLayoutElement.style.marginTop = "0px";
        }
        
        //Set auto-close:
        if(isDisplayInfoMessage) {
            setTimeout(function() {
                infoLayoutElement.style.display = "none";
                if(!errorMessageArray || errorMessageArray.length == 0) {
                    //errorAndInfoLayoutElement.style.display = "none";
                    errorAndInfoLayoutElement.style.marginTop = "-1000px";
                    errorAndInfoLayoutElement.style.visibility = "hidden";
                }
            }, (INFOMSG_TIMEOUT_VALUE));
            
        }
    }
}

function common_errorOrInfoMessage_hide() {
    var errorAndInfoLayoutElement = document.getElementById('errorAndInfoLayoutID');
    if(errorAndInfoLayoutElement != undefined) {
        document.getElementById('errorContentID').innerHTML = "";
        document.getElementById('errorLayoutID').style.display = "none";

        document.getElementById('infoContentID').innerHTML = "";
        document.getElementById('infoLayoutID').style.display = "none";
        
        errorAndInfoLayoutElement.style.display = "none";
    }
}

function common_imageErrorWidget_show(pageName, errorMsg) {
    var imageErrorWidget = document.getElementById(pageName + "ImageErrorLayoutID");
    if(imageErrorWidget != undefined && imageErrorWidget != null) {
        imageErrorWidget.style.display = "block";
        document.getElementById(pageName + "ImageErrorTextID").innerHTML = errorMsg;
    }
}

function common_imageErrorWidget_hide(pageName) {
    var imageErrorWidget = document.getElementById(pageName + "ImageErrorLayoutID");
    if(imageErrorWidget != undefined && imageErrorWidget != null) {
        imageErrorWidget.style.display = "none";
        document.getElementById(pageName + "ImageErrorTextID").innerHTML = "";
    }
}

function common_progressBarWidget_show(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    
    var progressBarElement = document.getElementById(pageName + "ProgressBarLayoutID");
    if(progressBarElement != undefined) {
        progressBarElement.style.display = "block";
    }
}

function common_progressBarWidget_hide(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }

    var progressBarElement = document.getElementById(pageName + "ProgressBarLayoutID");
    if(progressBarElement != undefined) {
        progressBarElement.style.display = "none";
    }
}

function common_disabledBackground_show() {
    var disableBackgroundWidget = document.getElementById('disableBackgroundLayoutID');
    if(disableBackgroundWidget != undefined && disableBackgroundWidget != null) {
        disableBackgroundWidget.style.display = "block";
    }
}

function common_disabledBackground_hide() {
    var disableBackgroundWidget = document.getElementById('disableBackgroundLayoutID');
    if(disableBackgroundWidget != undefined && disableBackgroundWidget != null) {
        disableBackgroundWidget.style.display = "none";
    }
}

function common_form_afterInit(pageName, formMainLayoutElement) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    common_errorOrInfoMessage_hide();
    common_progressBarWidget_hide(pageName);
    menu_close_show(pageName);
    document.getElementById(pageName + "contentID").scrollTop = 0;
    
    formMainLayoutElement.style.display = "block";
}

function common_form_afterSubmit(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }

    common_progressBarWidget_show(pageName);
    menu_close_hide(pageName);
    document.getElementById(pageName + "formID").style.display = "none";
}

function common_form_AJAX_handleError(pageName, errorMsgOrig, errorMsgDisplay) {
    common_AJAX_opsError_show(pageName, errorMsgOrig, errorMsgDisplay);
    menu_close_show(pageName);

    //Display the submitted form, if any:
    var formLayout = document.getElementById(pageName + "formID");
    if(formLayout != undefined && formLayout != null) {
        formLayout.style.display = "block";
    }
}

function common_form_AJAX_evalResponse(pageName, xmlhttpRequest) {
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(!isSuccess) {
        common_progressBarWidget_hide(pageName);
        document.getElementById(pageName + "formID").style.display = "block";
    }
    
    return isSuccess;
}

/*
 * Note: The input parameter comes as java's JSON-formatted String. However, javascript
 *       receives it already as a JSON-parsed javascript object (not String). Therefore, we do not need 
 *       to do "JSON.parse(inputString)" to transform it into an javascript object. 
 *       Instead, just use it right away.
 */
function common_serviceResponse_evaluate(serviceResponseJSON) {
    if(serviceResponseJSON == undefined 
            || serviceResponseJSON == null
            || serviceResponseJSON == ""
            || serviceResponseJSON == "{}") {
        console.error("common_serviceResponse_evaluate() - ERROR: Incoming serviceResponseJSON to evaluate is EMPTY!");
        common_errorOrInfoMessage_show([ERROR_MSG_SERVER_DEFAULT], null);
        return false;
    }
    
    if(!serviceResponseJSON.success) {
        if(serviceResponseJSON.serviceError != null
                && serviceResponseJSON.serviceError.errorText != null) {
            common_errorOrInfoMessage_show([serviceResponseJSON.serviceError.errorText], null);
        } else {
            console.error("common_serviceResponse_evaluate() - ERROR: serviceResponseJSON is failed but NO message from server!");
            common_errorOrInfoMessage_show([ERROR_MSG_SERVER_DEFAULT], null);
        }
        return false;
    }
    
    return true;
}

/*
 * Evaluates AJAX server response. This method assumes that xmlhttpRequest.readyState == 4 (ready),
 * therefore there is no check of such status here. The caller should have checked the readyState
 * before calling this method.
 */
function common_serviceResponse_evaluateAJAX(xmlhttpRequest) {
    if(xmlhttpRequest.status != 200) {
        if(xmlhttpRequest.status == 401) {
            //Unauthorized error:
           window.location.href = APP_ROOT_URI + "/trekcrumb/error/userNotAuthenticated"

        } else if(xmlhttpRequest.status == 403) {
            //IllegalAccess error:
            window.location.href = APP_ROOT_URI + "/trekcrumb/error/illegalAccess"
        
        } else {
            //Other errors:
            var errorMessageArray = [xmlhttpRequest.statusText];
            common_errorOrInfoMessage_show(errorMessageArray, null);
        }
        return false;
    }
    
    if(xmlhttpRequest.responseText == undefined 
            || xmlhttpRequest.responseText == null
            || xmlhttpRequest.responseText.trim() == "") {
        console.error("common_serviceResponse_evaluateAJAX() - ERROR: Incoming AJAX xmlhttpRequest had EMPTY response!");
        common_errorOrInfoMessage_show([ERROR_MSG_SERVER_DEFAULT], null);
        return false;
    }
    
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    return common_serviceResponse_evaluate(serverResponseObj);
}

function common_date_translateToLocaleTime(dateElement, dateValueInGMT, isIncludeTime, isIncludeTimestamp) {
    if(dateElement == undefined || dateElement == null) {
        console.error("common_date_translateToLocaleTime() - Required parameter [dateElement] is NOT defined!");
        return null;
    }
    if(dateValueInGMT == undefined || dateValueInGMT == null || dateValueInGMT.trim() == "") {
        console.error("common_date_translateToLocaleTime() - Required parameter [dateValueInGMT] is EMPTY!");
        dateElement.innerHTML = "&nbsp;";
        return;
    }
    
    var dateFormatter = null;
    if(isIncludeTime) {
        dateFormatter = DATE_CREATEDDATE_UI_FORMAT;
    } else {
        dateFormatter = DATE_CREATEDDATE_UI_FORMAT_WO_HOUR;
    }
    
    if(isIncludeTimestamp) {
        dateElement.innerHTML = moment(dateValueInGMT).format(dateFormatter) + " " + DATE_TIMESTAMP_LOCALE;
    } else {
        dateElement.innerHTML = moment(dateValueInGMT).format(dateFormatter);
    }
}

function common_url_getServerHost() {
    return location.protocol + '//' + location.host;
}

function common_url_getURL(isWithParams) {
    var url = window.location.href;
    if(!isWithParams) {
        url = url.split('?')[0];
    }
    return url;
}

function common_url_getQueryParamValue(queryParamName) {
    //var queryParamValues = new RegExp('[\\?&]' + queryParamName+ '=([^&#]*)').exec(window.location.href);
    var url = common_url_getURL(true);
    var queryParamValues = new RegExp('[\\?&]' + queryParamName+ '=([^&#]*)').exec(url);
    if (!queryParamValues) { 
        return null;
    }
    return queryParamValues [1] || null;
}

function common_deleteCheckbox_clicked(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    
    var editableSection = document.getElementById(pageName + "editableSectionID");
    var deleteCheckboxElement = document.getElementById(pageName + "deleteCheckboxID");
    if(deleteCheckboxElement.checked) {
        document.getElementById(pageName + "Warning").style.display = "block";
        if(editableSection != undefined || editableSection != null) {
            editableSection.style.display = "none";;
        }
        
    } else {
        document.getElementById(pageName + "Warning").style.display = "none";
        if(editableSection != undefined || editableSection != null) {
            editableSection.style.display = "block";;
        }
    }
}

function common_sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

function common_cancel() {
    //Force to go to 'Home' page:
    window.location.href = APP_ROOT_URI + "/trekcrumb/home";
}

function common_back() {
    window.history.go(-1);
    return false;
}

function common_refresh() {
    window.location.href = common_url_getURL(false);
}

/*
 * Prevents the hosting page from being revisited by forwarding to 'forward/next' page.
 * Note:
 * 1. One purpose is to prevent browser's BACK button to revisit a form page after it is submitted.
 *    If the page is freshly visited, there is no 'forward/next' page therefore this function
 *    has no impact.
 * 2. The usage: Hosting page may call this function at the very beginning before it fully loads,
 *    or at the end after page completely loads.
 */
function common_disableBackButton() {
    window.history.forward();
    return false;
}

function common_support_terms(isOpenNewTab) {
    if(isOpenNewTab) {
        window.open(APP_ROOT_URI + "/trekcrumb/support/terms", "terms", "menubar=no,status=no,toolbar=no");
    } else {
        window.location.href = APP_ROOT_URI + "/trekcrumb/support/terms";
    }
}

function common_support_privacy(isOpenNewTab) {
    if(isOpenNewTab) {
        window.open(APP_ROOT_URI + "/trekcrumb/support/privacy", "privacy", "menubar=no,status=no,toolbar=no");
    } else {
        window.location.href = APP_ROOT_URI + "/trekcrumb/support/privacy";
    }
}

//END trekcrumb-common.js