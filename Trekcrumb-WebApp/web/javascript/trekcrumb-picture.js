"use strict";

/*
 * Generic function to send AJAX request to get Picture image.
 * Note: 
 * 1. pageName - The fragment page where this call comes from, as defined in trekcrumnbs-constants.js.
 *    This function can accomodate various fragment pages when it is time to populate them with
 *    the pictureJSON.
 * 2. pictureJSON - The input parameter "pictureJSON" must come as java's JSON-formatted String. 
 *    In JS, it would be received "as is" a JSON-parsed JS object (not String). 
 *    Therefore, no need to do "JSON.parse(inputString)" to transform the input into JS object.
 */
function picture_generic_image_RetrieveServiceRequest(pageName, pictureJSON) {
    if(pictureJSON == undefined || pictureJSON == null) {
        return;
    }
    
    //Caching: Check if image already on locale storage:
    var cachedImageName = CACHE_KEY_PICTURE_IMAGENAME_PREFIX + pictureJSON.imageName;
    if(mDevice_isSupportLocaleStorage) {
        var imageBytes = localStorage.getItem(cachedImageName);
        if(imageBytes != undefined && imageBytes != null) {
            pictureJSON.imageBytes = imageBytes;
            picture_generic_displayContent(pageName, pictureJSON, true);
            return;
        }
    } 
    
    //Else: Call server
    if(mDevice_isSupportAJAX) {
        var xmlhttpRequest = new XMLHttpRequest();
        
        //Prepare response handling:
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                picture_generic_image_RetrieveServiceResponse(pageName, xmlhttpRequest, pictureJSON);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(pageName, 
                                      xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                                      ERROR_MSG_SERVER_DEFAULT);
        };
        
        //Compose request: None. The input param should have been in JSON-formatted javascript object
        
        //Prepare send (as JSON request):
        var urlTarget = APP_ROOT_URI + "/trekcrumb/picture/pictureImageDownload";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send(JSON.stringify(pictureJSON));
        } catch(error) {
            common_AJAX_opsError_show(pageName, error.message, ERROR_MSG_SERVER_DEFAULT);
        }
    }
}

function picture_generic_image_RetrieveServiceResponse(pageName, xmlhttpRequest, pictureToRead) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    if(xmlhttpRequest.status != 200) {
        picture_generic_displayContent(pageName, pictureToRead, false, xmlhttpRequest.statusText);
        return;
    }
    var serverResponseJSON = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseJSON.success) {
        picture_generic_displayContent(pageName, pictureToRead, false, serverResponseJSON.serviceError.errorText);
        return;
    }
    
    //Success:
    var pictureFound = serverResponseJSON.listOfPictures[0];
    
    //Caching: Save image bytes into local storage for next usage:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_LOCAL, 
            CACHE_KEY_PICTURE_IMAGENAME_PREFIX + pictureFound.imageName, 
            pictureFound.imageBytes);
    
    picture_generic_displayContent(pageName, pictureFound, true, null);
}

function picture_generic_displayContent(pageName, picture, isSuccess, errorMsg) {
    switch(pageName) {
        case FRAGMENT_PICTURE_DETAIL:
            picture_generic_detailDisplayContent(pageName, picture, isSuccess, errorMsg, "pictureDetailLayoutID");
            break;
        case FRAGMENT_PICTURE_SLIDESHOW:
            picture_generic_detailDisplayContent(pageName, picture, isSuccess, errorMsg, "pictureSlideshowLayoutID");
            break;
        case FRAGMENT_PICTURE_GALLERY:
            picture_gallery_DisplayContent(picture, isSuccess, errorMsg);
            break;
        case FRAGMENT_PICTURE_UPDATE:
            picture_update_DisplayContent(picture, isSuccess, errorMsg);
            break;
            
        default:
            console.error("picture_generic_DisplayContent() - ERROR!!! Picture 'pageName' fragment is unknown: " + pageName);
    }
}

/*
 * Generic function to retrieve the CURRENT picture (its image and details). All the current
 * variables (index, etc.) are from Pagination Picture page's variables.
 * Param:
 * - listOfPictures: The source List of Pictures can be passed on as input param argument (in JSON format). 
 *   If this is null, the list should come from a hidden input value (String) that should 
 *   have been populated during window/document load.
 * - This generic function caters PictureDetail and PictureSlideshow fragment pages.
 *   In order to use it, one must specify which fragment page they intend to populate by
 *   providing pageName and main layout element ID in the arguments.
 */
function picture_generic_detailRetrieve(pageName, listOfPictures, mainLayoutElementID) {
    if(pageName == undefined || pageName == null || pageName.trim() == "") {
        pageName = "";
    }

    //1. Validate we are correct page:
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        console.error("picture_generic_detailRetrieve() - ERROR: Required element [paginationPictureLayoutID] not found! ");
        return;
    }
    var mainLayoutElement = document.getElementById(mainLayoutElementID);
    if(mainLayoutElement == undefined || mainLayoutElement == null) {
        console.error("picture_generic_detailRetrieve() - ERROR: Main layout element [" + mainLayoutElementID + "] not found!");
        return;
    }
    var tripID = pagePicLayout.getAttribute(VARATTR_TRIP_ID);
    var picImageElement = document.getElementById(pageName + "pictureImageID" + tripID);
    if(picImageElement == undefined || picImageElement == null) {
        console.error("picture_generic_detailRetrieve() - ERROR: Picture Image element not found!");
        return;
    
    }
    var listOfPicturesJSON = null;
    if(listOfPictures != null) {
        listOfPicturesJSON = listOfPictures;
    } else {
        var listOfPicturesString = document.getElementById('pagePic_listOfPicturesID').value;
        if(listOfPicturesString == undefined 
                || listOfPicturesString == null 
                || listOfPicturesString == "") {
            return;
        }
        listOfPicturesJSON = JSON.parse(listOfPicturesString);
    }
    if(listOfPicturesJSON == null) {
        console.error("picture_generic_detailRetrieve() - ERROR: List of pictures data not found (EMPTY)!");
        return;
    }

    //2. Re-initialize the image layout:
    common_progressBarWidget_show(pageName);
    common_imageErrorWidget_hide(pageName);
    picImageElement.style.display = "none"; 
    picImageElement.setAttribute("src", "");
    
    //3. Prevent invalid ArrayOutOfBound error:
    var picCurrentIndex = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT));
    if(picCurrentIndex < 0) {
        picCurrentIndex = 0;
        pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, picCurrentIndex);
    } else {
        var numOfTotalPictures = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
        if(picCurrentIndex > (numOfTotalPictures - 1)) {
            picCurrentIndex = (numOfTotalPictures - 1); 
            pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, picCurrentIndex );
        }
    }
    
    //4. Get the current picture:
    var picture = listOfPicturesJSON[picCurrentIndex];
    
    //5. Populate available details:
    var picNoteElement = document.getElementById(pageName + "pictureNoteID");
    if(picture.note != null && picture.note != "") {
        picNoteElement.innerHTML = picture.note;
    } else {
        picNoteElement.innerHTML = "";
    }
    
    //6. Async: Get the image:
    picture_generic_image_RetrieveServiceRequest(pageName, picture);
}

/* 
 * Generic function to display its CURRENT Picture content (image).
 * Since image retrieve processes are async, the image element ID must be unique
 * for each Trip View page to ensure we are dealing with right image for right Trip.
 * If they are matched, then we can proceed to populate the content.
 *
 * This generic function caters PictureDetail and PictureSlideshow fragment pages.
 * In order to use it, one must specify which fragment page they intend to populate by
 * providing pageName and main layout element ID in the arguments.
 */
function picture_generic_detailDisplayContent(pageName, picture, isSuccess, errorMsg, mainLayoutElementID) {
    var mainLayoutElement = document.getElementById(mainLayoutElementID);
    if(mainLayoutElement == undefined || mainLayoutElement == null) {
        return;
    }
    if(pageName == undefined || pageName == null || pageName.trim() == "") {
        pageName = "";
    }
    
    var picImageElement = document.getElementById(pageName + "pictureImageID" + picture.tripID);
    if(picImageElement != undefined && picImageElement != null) {
        common_progressBarWidget_hide(pageName);
        
        if(isSuccess) {
            picImageElement.style.display = "inline-block"; 
            picImageElement.style.height = "auto"; 
            picImageElement.style.width = "auto"; 
            picImageElement.setAttribute("src", IMAGE_BYTES_TO_IMAGE_BASE64 + picture.imageBytes);
            responsive_adjustPictureImageElement(picImageElement.id);
            
        } else {
            picImageElement.style.display = "none"; 
            picImageElement.setAttribute("src", "");
            common_imageErrorWidget_show(pageName, errorMsg);
        }
    }
}

/*
 * Picture List - display its rows.
 * Note:
 * 1. Reuse the same element content with dynamic data loading from AJAX calls.
 * 2. Image: Display each picture images directly with its image filename, without (another) AJAX calls.
 */
function picture_list_displayRows(pageName, listOfPictures, numOfRowsToDisplay, isLinkOpenNewTab) {
    if(pageName == undefined || pageName == null || pageName.trim() == "") {
        pageName = "";
    }
    
    var pictureListLayout = document.getElementById(pageName + "pictureListLayoutID");
    var pictureListContent = document.getElementById(pageName + "pictureListContentID");
    var pictureRowTemplate = document.getElementById("pictureRowTemplateID");
    if(pictureListLayout == undefined || pictureListLayout == null
            || pictureRowTemplate == undefined || pictureRowTemplate == null
            || pictureListContent == undefined || pictureListContent == null) {
        console.error("picture_list_displayRows() - ERROR: Missing required layouts in current view!");
        return;
    }

    pictureListLayout.style.display = "block";
    pictureListContent.scrollTop;

    if(listOfPictures == undefined || listOfPictures == null || listOfPictures.length == 0) {
        pictureListContent.innerHTML = ERROR_NO_DATA ;
        return;
    }
    
    //Else
    pictureListContent.innerHTML = "";

    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    var listSize = listOfPictures.length;
    for(var index = 0; index < listSize; index++) {
        var picture = listOfPictures[index];
        var picId = picture.id;

        //Clone row template and populate its unique data:
        var rowCurrent = pictureRowTemplate.cloneNode(true);
        rowCurrent.id = picId;
        
        //User details:
        rowCurrent.querySelector("#picRowUserDetailID").id = "picRowUserDetailID" + picId;
        rowCurrent.querySelector("#userThumbnailFullnameID").innerHTML = picture.userFullname;
        rowCurrent.querySelector("#userThumbnailUsernameID").innerHTML = picture.username;
        if(validate_isStringEmpty(picture.userImageName)) {
            rowCurrent.querySelector("#userThumbnailImageDefaultID").style.display = "block";
            rowCurrent.querySelector("#userThumbnailImageID").style.display = "none";
        } else {
            rowCurrent.querySelector("#userThumbnailImageDefaultID").style.display = "none";
            var userImageElement = rowCurrent.querySelector("#userThumbnailImageID");
            userImageElement.setAttribute("src", FILE_SERVER_URL_GETIMAGE_PROFILE + picture.userImageName);
            userImageElement.style.display = "block";
        }
        
        //Picture details:
        rowCurrent.querySelector("#picRowPictureDetailID").id = "picRowPictureDetailID" + picId;
        common_date_translateToLocaleTime(rowCurrent.querySelector("#picRowCreatedID"), picture.created, false, false);
        rowCurrent.querySelector("#picRowTripNameID").innerHTML = picture.tripName;
        if(picture.note != null && picture.note.trim() != "") {
            rowCurrent.querySelector("#picRowNoteID").innerHTML = picture.note;
        }
        
        //Picture image:
        rowCurrent.querySelector("#picRowImageID").style.backgroundImage = "url('" + FILE_SERVER_URL_GETIMAGE_PICTURE + picture.imageName + "')";

        //URL link:
        var targetURL = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + picture.username + "/" + picture.tripID
                        + "?" + QUERYPARAM_NAME_PICTURE_ID + "=" + picId;
        if(isLinkOpenNewTab) {
            rowCurrent.setAttribute("onclick", "menu_openNewTab('" + targetURL + "')");
        } else {
            rowCurrent.href = targetURL;
        }

        //Finally, display it:
        rowCurrent.style.display = 'inline-block';
        pictureListContent.appendChild(rowCurrent);

        //Check if we limit the display:
        if(numOfRowsToDisplay > 0 && index >= (numOfRowsToDisplay - 1)) {
            break;
        }
    }
}

function picture_list_rowHoverOn(rowElement, isDisplayUser) {
    menu_hover_ElementActive(rowElement);
    
    var pictureDetail = document.getElementById("picRowPictureDetailID" + rowElement.parentElement.id);
    if(pictureDetail != undefined) {
        pictureDetail.style.display = "block";
    }
    
    //Only show/hide if isDisplayUser false:
    if(!isDisplayUser) {
        var userDetail = document.getElementById("picRowUserDetailID" + rowElement.parentElement.id);
        if(userDetail != undefined) {
            userDetail.style.display = "block";
        }
    }
}

function picture_list_rowHoverOff(rowElement, isDisplayUser) {
    menu_hover_ElementInactive(rowElement);
    
    var pictureDetail = document.getElementById("picRowPictureDetailID" + rowElement.parentElement.id);
    if(pictureDetail != undefined) {
        pictureDetail.style.display = "none";
    }

    //Only show/hide if isDisplayUser false:
    if(!isDisplayUser) {
        var userDetail = document.getElementById("picRowUserDetailID" + rowElement.parentElement.id);
        if(userDetail != undefined) {
            userDetail.style.display = "none";
        }
    }
}

/*
 * PictureList - Homepage starting point. 
 */
function picture_list_home_current() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get the last result List if available
        var listOfPictures = sessionStorage.getItem(CACHE_KEY_PICTURE_LIST_RESULT_PAGE + "0");
        if(listOfPictures != undefined && listOfPictures != null) {
            common_progressBarWidget_hide(FRAGMENT_PICTURE_LIST);

            if(mHome_numOfListItemsToDisplay == 0) {
                mHome_numOfListItemsToDisplay = HOME_GALLERYITEM_NUM_TO_DISPLAY_DEFAULT;
            }
            picture_list_displayRows(FRAGMENT_PICTURE_LIST, JSON.parse(listOfPictures), mHome_numOfListItemsToDisplay, true);
            return;
        }
    }
    
    //No caching available. let's start from clean states:
    picture_list_home_serviceRequest();
}

function picture_list_home_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                picture_list_home_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_PICTURE_LIST, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        try {
            var urlTarget = APP_ROOT_URI + "/trekcrumb/picture/search";
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send();
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_PICTURE_LIST, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function picture_list_home_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_PICTURE_LIST);

    if(xmlhttpRequest.status != 200) {
        var errorMessageArray = [xmlhttpRequest.statusText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        var errorMessageArray = [serverResponseObj.serviceError.errorText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    
    //Success
    var listOfPictures = serverResponseObj.listOfPictures;
    if(mHome_numOfListItemsToDisplay == 0) {
        mHome_numOfListItemsToDisplay = HOME_GALLERYITEM_NUM_TO_DISPLAY_DEFAULT;
    }
    picture_list_displayRows(FRAGMENT_PICTURE_LIST, listOfPictures, mHome_numOfListItemsToDisplay, true);
    
    //Caching: Save list results:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_PICTURE_LIST_RESULT_PAGE + "0", 
            JSON.stringify(listOfPictures));    
}

/*
 * PictureList - UserPictures init stage.
 * -> Initializes its parameters to clean (original) state. 
 *    One particular usage is when client click a link to view one user profile to the next
 *    user profile. Therefore any previous stored data/cache must be cleared.
 */
function picture_list_userPictures_init() {
    //Clear ALL user-related cached data:
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserPictures");
}

/*
 * PictureList - UserPictures starting point. 
 * -> It would try to recover any last current state params and result list 
 *    when available (cached in session storage). 
 * -> This is to support feature when user navigates away from current UserPictures page, 
 *    but then click browser "BACK" button such that we avoid to re-init the list service call.
 */
function picture_list_userPictures_current(username, isUserLoginProfile) {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get current state params if available:
        var offsetCurrent = sessionStorage.getItem(CACHE_KEY_PICTURE_USERPICTURES_OFFSET_CURRENT);
        var pageCurrent = sessionStorage.getItem(CACHE_KEY_PICTURE_USERPICTURES_PAGE_CURRENT);
        var numOfRecordsTotal = sessionStorage.getItem(CACHE_KEY_PICTURE_USERPICTURES_TOTAL_RECORDS);
        var indexSelected = sessionStorage.getItem(CACHE_KEY_PICTURE_USERPICTURES_INDEX_SELECTED);
        if(offsetCurrent != undefined && offsetCurrent != null 
                && pageCurrent != undefined && pageCurrent != null
                && numOfRecordsTotal != undefined && numOfRecordsTotal != null) {
            //Set these params onto the page:
            pageList_update(FRAGMENT_PICTURE_LIST_USER_PICTURES, 
                            parseInt(offsetCurrent), parseInt(pageCurrent), parseInt(numOfRecordsTotal));
            
            //Caching: Get the last result List if available:
            var listOfPictures = sessionStorage.getItem(CACHE_KEY_PICTURE_USERPICTURES_RESULT_PAGE + username + pageCurrent);
            if(listOfPictures != undefined && listOfPictures != null) {
                common_progressBarWidget_hide(FRAGMENT_PICTURE_LIST_USER_PICTURES);
                picture_list_displayRows(FRAGMENT_PICTURE_LIST_USER_PICTURES, JSON.parse(listOfPictures), -1, false);
            } else {
                picture_list_userPictures_serviceRequest(username, isUserLoginProfile);
            }
            //Exit here:
            return;
        }
    }
    
    //Else: Caching Not available. let's start from clean states:
    pageList_init(FRAGMENT_PICTURE_LIST_USER_PICTURES);
    picture_list_userPictures_serviceRequest(username, isUserLoginProfile);
    
}

function picture_list_userPictures_next() {
    if(pageList_next(FRAGMENT_PICTURE_LIST_USER_PICTURES)) {
        document.getElementById(FRAGMENT_PICTURE_LIST_USER_PICTURES + "pictureListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_PICTURE_LIST_USER_PICTURES);
    
        picture_list_userPictures_retrieveList();
    }
}

function picture_list_userPictures_previous() {
    if(pageList_previous(FRAGMENT_PICTURE_LIST_USER_PICTURES)) {
        document.getElementById(FRAGMENT_PICTURE_LIST_USER_PICTURES + "pictureListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_PICTURE_LIST_USER_PICTURES);

        picture_list_userPictures_retrieveList();
    }
}

function picture_list_userPictures_retrieveList() {
    var userPicturesLayout = document.getElementById('userPicturesLayoutID');
    var username = userPicturesLayout.getAttribute(VARATTR_USER_USERNAME);
    var isUserLoginProfile = userPicturesLayout.getAttribute(VARATTR_USER_IS_USERLOGIN_PROFILE);

    if(mDevice_isSupportLocaleStorage) {
        //Caching: Update current state params into cache:
        common_localeStorage_cacheCurrentListState(
                FRAGMENT_PICTURE_LIST_USER_PICTURES,
                CACHE_KEY_PICTURE_USERPICTURES_OFFSET_CURRENT,
                CACHE_KEY_PICTURE_USERPICTURES_PAGE_CURRENT,
                CACHE_KEY_PICTURE_USERPICTURES_TOTAL_RECORDS);
                
        //Caching: Get the last result List if available:
        var pageListLayout = document.getElementById(FRAGMENT_PICTURE_LIST_USER_PICTURES + 'paginationListLayoutID');
        var pageCurrent = pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
        var listOfPictures = sessionStorage.getItem(CACHE_KEY_PICTURE_USERPICTURES_RESULT_PAGE + username + pageCurrent);
        if(listOfPictures != undefined && listOfPictures != null) {
            common_progressBarWidget_hide(FRAGMENT_PICTURE_LIST_USER_PICTURES);
            picture_list_displayRows(FRAGMENT_PICTURE_LIST_USER_PICTURES, JSON.parse(listOfPictures), -1, false);
        } else {
            picture_list_userPictures_serviceRequest(username, isUserLoginProfile);
        }
    
    } else {
        //No caching available:
        picture_list_userPictures_serviceRequest(username, isUserLoginProfile);
    }
}

function picture_list_userPictures_serviceRequest(usernameValue, isUserLoginProfile) {
    if(mDevice_isSupportAJAX) {
        var xmlhttpRequest = new XMLHttpRequest();
        
        //Get pagination params:
        var pageListLayout = document.getElementById(FRAGMENT_PICTURE_LIST_USER_PICTURES + 'paginationListLayoutID');
        var offsetCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT));
        var pageCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT));
        
        //Prepare response handling:
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                picture_list_userPictures_serviceResponse(usernameValue, offsetCurrent, pageCurrent, xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_PICTURE_LIST_USER_PICTURES, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        //Compose request:
        var pictureRetrieve = {
            username: usernameValue
        };
        if(isUserLoginProfile != "true") {
            pictureRetrieve.privacy = "PUBLIC";
        } else {
            pictureRetrieve.privacy = "PRIVATE";
        }
        
        //Prepare send (as JSON request):
        try {
            var urlTarget = APP_ROOT_URI + "/trekcrumb/picture/retrieve_byUsername/"  + offsetCurrent;
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send(JSON.stringify(pictureRetrieve));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_PICTURE_LIST_USER_PICTURES, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function picture_list_userPictures_serviceResponse(username, offset, pageCurrent, xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_PICTURE_LIST_USER_PICTURES);

    if(xmlhttpRequest.status != 200) {
        var errorMessageArray = [xmlhttpRequest.statusText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        var errorMessageArray = [serverResponseObj.serviceError.errorText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    
    //Success
    //Note: Do not call 'hide error or info message' function because we may want to display
    //      info message from server from other fragment processing.
    var totalNumOfRecords = serverResponseObj.numOfRecords;
    pageList_update(FRAGMENT_PICTURE_LIST_USER_PICTURES, offset, pageCurrent, totalNumOfRecords);
    
    var listOfPictures = serverResponseObj.listOfPictures;
    picture_list_displayRows(FRAGMENT_PICTURE_LIST_USER_PICTURES, listOfPictures, -1, false);
    
    //Caching: Save list results:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_PICTURE_USERPICTURES_RESULT_PAGE + username + pageCurrent, 
            JSON.stringify(listOfPictures));    
}

function picture_detail_Next() {
    if(pagePic_next(FRAGMENT_PICTURE_DETAIL)) {
        common_errorOrInfoMessage_hide();
        picture_generic_detailRetrieve(FRAGMENT_PICTURE_DETAIL, null, "pictureDetailLayoutID");
    }
}

function picture_detail_Previous() {
    if(pagePic_previous(FRAGMENT_PICTURE_DETAIL)) {
        common_errorOrInfoMessage_hide();
        picture_generic_detailRetrieve(FRAGMENT_PICTURE_DETAIL, null, "pictureDetailLayoutID");
    }
}

function picture_slideshow_Next() {
    if(pagePic_next(FRAGMENT_PICTURE_SLIDESHOW)) {
        common_errorOrInfoMessage_hide();
        picture_generic_detailRetrieve(FRAGMENT_PICTURE_SLIDESHOW, null, "pictureSlideshowLayoutID");
        
        //Update gallery selected item, if any:
        var pictureGalleryLayout = document.getElementById("pictureGalleryLayoutID");
        if(pictureGalleryLayout != undefined && pictureGalleryLayout != null) {
            picture_gallery_ItemSelectedHighlight();
        }
    }
}

function picture_slideshow_Previous() {
    if(pagePic_previous(FRAGMENT_PICTURE_SLIDESHOW)) {
        common_errorOrInfoMessage_hide();
        picture_generic_detailRetrieve(FRAGMENT_PICTURE_SLIDESHOW, null, "pictureSlideshowLayoutID");

        //Update gallery selected item, if any:
        var pictureGalleryLayout = document.getElementById("pictureGalleryLayoutID");
        if(pictureGalleryLayout != undefined && pictureGalleryLayout != null) {
            picture_gallery_ItemSelectedHighlight();
        }
    }
}

/*
 * Picture Gallery retrieves the list of pictures (their images).
 * Param:
 * - listOfPictures: The source List of Pictures can be passed on as input param argument (in JSON format). 
 *   If this is null, the list should come from a hidden input value (String) that should 
 *   have been populated during window/document load.
 */
function picture_gallery_Retrieve(listOfPictures) {
    var listOfPicturesJSON = null;
    if(listOfPictures != null) {
        listOfPicturesJSON = listOfPictures;
    } else {
        var listOfPicturesString = document.getElementById('pagePic_listOfPicturesID').value;
        if(listOfPicturesString == undefined 
                || listOfPicturesString == null 
                || listOfPicturesString == "") {
            return;
        }
        listOfPicturesJSON = JSON.parse(listOfPicturesString);
    }
    if(listOfPicturesJSON.length <= 1) {
        return;
    }
    
    //Start populating the gallery:
    var galleryContentElementTemplate = document.getElementById("picGalleryContentTemplateID");
    if(galleryContentElementTemplate == undefined || galleryContentElementTemplate == null) {
        return;
    }

    var pictureGalleryLayout = document.getElementById("pictureGalleryLayoutID");
    
    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    var picSize = listOfPicturesJSON.length;
    for(var indexPic = 0; indexPic < picSize; indexPic++) {
        var picture = listOfPicturesJSON[indexPic];

        //Clone template and setup its unique data:
        var galleryContentElementCurrent = galleryContentElementTemplate.cloneNode(true);
        galleryContentElementCurrent.id = indexPic;
        galleryContentElementCurrent.className += " layout-picture-gallery-content";
        galleryContentElementCurrent.querySelector("#picGalleryImageTemplateID").id = picture.imageName;
        galleryContentElementCurrent.querySelector("#picGalleryImageTemplateDefaultID").id = picture.imageName + "Default";

        //Finally, display it:
        galleryContentElementCurrent.style.display = 'inline-block';
        pictureGalleryLayout.appendChild(galleryContentElementCurrent);
        picture_generic_image_RetrieveServiceRequest(FRAGMENT_PICTURE_GALLERY, picture);
    }
}

/* 
 * Picture Gallery displays its current row content (image only).
 * Since in one gallery layout may contain two or more rows and this call is
 * async, the function will look for the correct row based on Image Name.
 */
function picture_gallery_DisplayContent(picture, isSuccess, errorMsg) {
    var rowImageElement = document.getElementById(picture.imageName);
    if(rowImageElement == undefined || rowImageElement == null) {
        return;
    }

    if(isSuccess) {
        rowImageElement.src = IMAGE_BYTES_TO_IMAGE_BASE64 + picture.imageBytes;
        rowImageElement.style.display = "inline-block";
        
        //Hide default:
        var rowImageDefaultElement = document.getElementById(picture.imageName + "Default");
        if(rowImageDefaultElement != undefined && rowImageDefaultElement != null) {
            rowImageDefaultElement.style.display = "none";
        }
    }
}

function picture_gallery_ItemSelected(itemElement) {
    var picIndexSelected = itemElement.id;
    
    //Update pagination variable:
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout != undefined && pagePicLayout != null) {
        pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, picIndexSelected);
    }

    //Highlight background:
    picture_gallery_ItemSelectedHighlight();
    
    //Update picture slideshow, if any:
    var pictureSlideshowLayoutElement = document.getElementById("pictureSlideshowLayoutID");
    if(pictureSlideshowLayoutElement != undefined && pictureSlideshowLayoutElement != null) {
        picture_generic_detailRetrieve(FRAGMENT_PICTURE_SLIDESHOW, null, "pictureSlideshowLayoutID");
        pagePic_updateNavigationMenu(FRAGMENT_PICTURE_SLIDESHOW, null, null);
    }
}

function picture_gallery_ItemSelectedHighlight() {
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        console.log("picture_gallery_ItemSelectedHighlight() - ERROR: Required element [paginationPictureLayoutID] not found! ");
        return;
    }
    var picCurrentIndex = pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT);
    
    var picGalleryContents = document.getElementsByClassName("layout-picture-gallery-content");
    var numOfContents = picGalleryContents.length;
    for(var i = 0; i < numOfContents; i++) {
        if(picGalleryContents[i].id == picCurrentIndex) {
            picGalleryContents[i].style.backgroundColor = "#C1E75B";
        } else {
            picGalleryContents[i].style.backgroundColor = "";
        }
    }
}

function picture_gallery_Previous() {
    var numberContentsToShowTotal = parseInt(document.getElementById("picGallery_NumberContentsToShowTotalID").value);
    var contentShownFirstRowIndex = parseInt(document.getElementById("picGallery_ContentShownFirstRowIndexID").value);
    
    var previousContentShownFirstRowIndex = contentShownFirstRowIndex - numberContentsToShowTotal;
    if(previousContentShownFirstRowIndex < 0) {
        //No more previous:
        return;
    }
    //Else:
    document.getElementById("picGallery_ContentShownFirstRowIndexID").value = previousContentShownFirstRowIndex;
    picture_gallery_DisplayOrHideContentRows();
}

function picture_gallery_Next() {
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        console.log("picture_gallery_Next() - ERROR: Required element [paginationPictureLayoutID] not found! ");
        return;
    }
    var numOfTotalPictures = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));

    var numberContentsToShowTotal = parseInt(document.getElementById("picGallery_NumberContentsToShowTotalID").value); 
    var contentShownFirstRowIndex = parseInt(document.getElementById("picGallery_ContentShownFirstRowIndexID").value);
    var nextContentShownFirstRowIndex = contentShownFirstRowIndex + numberContentsToShowTotal;
    if(nextContentShownFirstRowIndex > numOfTotalPictures) {
        //No more next:
        return;
    }
    //Else:
    document.getElementById("picGallery_ContentShownFirstRowIndexID").value = nextContentShownFirstRowIndex;
    picture_gallery_DisplayOrHideContentRows();
}

/* 
 * Picture Gallery to display or hide its row contents. 
 */
function picture_gallery_DisplayOrHideContentRows() {
    //1. Get page's current variables and contents:
    var picGalleryContents = document.getElementsByClassName("layout-picture-gallery-content");
    var numOfContents = picGalleryContents.length;
    var contentShownFirstRowIndex = parseInt(document.getElementById("picGallery_ContentShownFirstRowIndexID").value);
    var numberContentsToShowTotal = parseInt(document.getElementById("picGallery_NumberContentsToShowTotalID").value); 
    var contentLayoutSizeTotalAvailable = parseInt(document.getElementById("picGallery_layoutTotalSizeAvailableID").value);

    //2. Decide which content to show or to hide:
    var numberContentsToShowCurrent = 0;
    for(var i = 0; i < numOfContents; i++) {
        //Show only contents whose index is at/after the 'contentShownFirstRowIndex', but not more than 'numberContentsToShowTotal':
        if(i >= contentShownFirstRowIndex) {
            if(numberContentsToShowCurrent == numberContentsToShowTotal) {
                //Has reached max. number to show, hide the rest of contents after the 'contentShownFirstRowIndex' to the RIGHT:
                picGalleryContents[i].style.position = "absolute";
                picGalleryContents[i].style.left = "" + (2 * contentLayoutSizeTotalAvailable) + "px";
                picGalleryContents[i].style.visibility = "hidden";
                continue;
            } else {
                //Show content:
                picGalleryContents[i].style.position = "";
                picGalleryContents[i].style.left = "";
                picGalleryContents[i].style.visibility = "visible";
                numberContentsToShowCurrent++;
            }
            
        } else {
            //Hide contents before the 'contentShownFirstRowIndex' to the LEFT:
            picGalleryContents[i].style.position = "absolute";
            picGalleryContents[i].style.left = "-500px";
            picGalleryContents[i].style.visibility = "hidden";
        }
    }

    //3. Update previous/next menu icon:
    if((contentShownFirstRowIndex - numberContentsToShowTotal) < 0) {
        document.getElementById(FRAGMENT_PICTURE_GALLERY + "MenuGoPreviousID").style.display = "none";
    } else {
        document.getElementById(FRAGMENT_PICTURE_GALLERY + "MenuGoPreviousID").style.display = "block";
    }
    if((contentShownFirstRowIndex + numberContentsToShowTotal) >= numOfContents) {
        document.getElementById(FRAGMENT_PICTURE_GALLERY + "MenuGoNextID").style.display = "none";
    } else {
        document.getElementById(FRAGMENT_PICTURE_GALLERY + "MenuGoNextID").style.display = "block";
    }
}

function picture_create_Init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    var picCreateLayout = document.getElementById('pictureCreateLayoutID');
    if(picCreateLayout == undefined || picCreateLayout == null) {
        //Invalid view:
        console.error("picture_create_Init() - ERROR: Could not find pictureCreateLayoutID layout!");
        return;
    }
    
    //Display:
    common_form_afterInit(FRAGMENT_PICTURE_CREATE, picCreateLayout);
}

function picture_create_cancel() {
    var picCreateLayout = document.getElementById('pictureCreateLayoutID');
    if(picCreateLayout == undefined || picCreateLayout == null) {
        //Invalid:
        return;
    }
    
    //Clean any leftover:
    common_fileAPIReader_inputImageFile_cancelAndHide();
    document.getElementById('imageBase64DataID').value = "";
    document.getElementById('pictureCreateNoteInputID').value = "";
    picCreateLayout.style.display = "none";
}

function picture_create_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    
    //Validate:
    var validateInputImageFileError = validate_inputImageFile();
    if(validateInputImageFileError != null) {
        common_errorOrInfoMessage_show([validateInputImageFileError], null);
        return;
    }

    //Call server:
    common_form_afterSubmit(FRAGMENT_PICTURE_CREATE);    
    picture_create_ServiceRequest();
}

function picture_create_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var pictureToCreate = {};
        pictureToCreate.userID = document.getElementById('userID').value;
        pictureToCreate.tripID = document.getElementById('tripID').value;
        pictureToCreate.note = document.getElementById('pictureCreateNoteInputID').value;
        pictureToCreate.imageBase64Data = document.getElementById('imageBase64DataID').value;
        
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                picture_create_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_PICTURE_CREATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/picture/create/submit/AJAX/";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(pictureToCreate));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_PICTURE_CREATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function picture_create_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var picCreateLayout = document.getElementById("pictureCreateLayoutID");
    if(picCreateLayout == undefined || picCreateLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_PICTURE_CREATE, xmlhttpRequest);
    if(isSuccess) {
        //Find the new Picture index:
        var numOfTotalPicPrevious = -1;
        var paginationPictureLayout = document.getElementById('paginationPictureLayoutID');
        if(paginationPictureLayout != undefined && paginationPictureLayout != null) {
            numOfTotalPicPrevious = parseInt(paginationPictureLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
        }

        //Redirect to result page:
        var username = document.getElementById('usernameID').value;
        var tripID = document.getElementById('tripID').value;
        window.location.href = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID
                + "?" + QUERYPARAM_NAME_PICTURE_INDEX + "=" + (numOfTotalPicPrevious + 1);
    }
}

function picture_update_Init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    var picUpdateLayout = document.getElementById('pictureUpdateLayoutID');
    if(picUpdateLayout == undefined || picUpdateLayout == null) {
        //Invalid view:
        console.error("picture_update_Init() - ERROR: Could not find pictureUpdateLayoutID layout!");
        return;
    }
    var paginationPicLayout = document.getElementById('paginationPictureLayoutID');
    if(paginationPicLayout == undefined || paginationPicLayout == null) {
        console.error("picture_update_Init() - ERROR: Cannot find paginationPictureLayoutID layout!");
        return;
    }
    var listOfPicsString = document.getElementById('pagePic_listOfPicturesID').value;
    if(listOfPicsString == undefined 
            || listOfPicsString == null 
            || listOfPicsString == "") {
        console.log("picture_update_Init() - ERROR: Cannot find the data list of Pictures at current page!");
        return;
    }
    
    var picCurrentIndex = parseInt(paginationPicLayout.getAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT));
    var numOfTotalPictures = parseInt(paginationPicLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
    if(picCurrentIndex < 0 
        || picCurrentIndex > (numOfTotalPictures - 1)) {
        //Prevent invalid ArrayOutOfBound error:
        console.log("picture_update_Init() - ERROR: Current selected Picture index is out of bound: " + picCurrentIndex);
        return;
    }

    //Populate form values:
    var listOfPicsJSON = JSON.parse(listOfPicsString);
    var picture = listOfPicsJSON[picCurrentIndex];
    document.getElementById('pictureID').value = picture.id;
    document.getElementById('picUpdateIndexID').innerHTML = (picCurrentIndex +1);
    document.getElementById('pictureUpdateNoteInputID').value = picture.note;
    
    var imageElement = document.getElementById('pictureUpdateImageID');
    imageElement.id = "pictureUpdateImageID" + picture.id;
    
    //Display:
    common_form_afterInit(FRAGMENT_PICTURE_UPDATE, picUpdateLayout);
    
    /*
     * Retrieve image
     * Note: We could populate Picture update image SRC directly given image filename without AJAX.
     * But here we take advantage of the picture detail/slideshow caching mechanism since the 
     * image must have been displayed before picture update is invoked, hence more efficient.
     * See: picture_update_DisplayContent()
     */
    picture_generic_image_RetrieveServiceRequest(FRAGMENT_PICTURE_UPDATE, picture);    
}

/* 
 * Picture Update displays its image for review only.
 * 1. Its image element has unique ID associated with the picture's ID.
 * 2. We could populate Picture update image SRC directly given image filename without AJAX.
 *    But here we take advantage of the picture detail/slideshow caching mechanism since the 
 *    image must have been displayed before picture update is invoked, hence more efficient.
 */
function picture_update_DisplayContent(picture, isSuccess, errorMsg) {
    var imageElement = document.getElementById("pictureUpdateImageID" + picture.id);
    if(imageElement == undefined || imageElement == null) {
        return;
    }

    if(!isSuccess) {
        common_imageErrorWidget_show(FRAGMENT_PICTURE_SLIDESHOW, errorMsg);
        imageElement.setAttribute('src', "");
        return;
    }
    
    //Else:
    imageElement.setAttribute('src', IMAGE_BYTES_TO_IMAGE_BASE64 + picture.imageBytes);
}

function picture_update_cancel() {
    var picUpdateLayout = document.getElementById('pictureUpdateLayoutID');
    if(picUpdateLayout == undefined || picUpdateLayout == null) {
        //Illegal access:
        return;
    }
    
    //Clean any leftover:
    common_errorOrInfoMessage_hide();
    document.getElementById(FRAGMENT_PICTURE_UPDATE + "deleteCheckboxID").checked = false;
    common_deleteCheckbox_clicked(FRAGMENT_PICTURE_UPDATE);
    
    var pictureID = document.getElementById('pictureID').value;
    var imageElement = document.getElementById("pictureUpdateImageID" + pictureID);
    if(imageElement != undefined && imageElement != null) {
        imageElement.setAttribute('src', "");
        imageElement.id = "pictureUpdateImageID";
    }
    
    picUpdateLayout.style.display = "none";
}

function picture_update_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    common_form_afterSubmit(FRAGMENT_PICTURE_UPDATE);
    picture_update_ServiceRequest();
}

function picture_update_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var isDeleted = false;
        var picToUpdate = {};
        picToUpdate.id = document.getElementById('pictureID').value;
        picToUpdate.userID = document.getElementById('userID').value;
        picToUpdate.tripID = document.getElementById('tripID').value;
        if(document.getElementById(FRAGMENT_PICTURE_UPDATE + "deleteCheckboxID").checked) {
            isDeleted = true;
        } else {
            picToUpdate.note = document.getElementById('pictureUpdateNoteInputID').value;
        }

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                picture_update_ServiceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_PICTURE_UPDATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = null;
        if(isDeleted) {
            urlTarget = APP_ROOT_URI + "/trekcrumb/picture/delete/submit/AJAX";
        } else {
            urlTarget = APP_ROOT_URI + "/trekcrumb/picture/update/submit/AJAX/";
        }
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(picToUpdate));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_PICTURE_UPDATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function picture_update_ServiceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var picUpdateLayout = document.getElementById("pictureUpdateLayoutID");
    if(picUpdateLayout == undefined || picUpdateLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_PICTURE_UPDATE, xmlhttpRequest);
    if(isSuccess) {
        var username = document.getElementById('usernameID').value;
        var tripID = document.getElementById('tripID').value;
        var successTarget = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID;
        if(document.getElementById(FRAGMENT_PICTURE_UPDATE + "deleteCheckboxID").checked == true) {
            //Clean all possible Picture-related caches:
            common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserPictures");
            
        } else {
            var pictureIndex = parseInt(document.getElementById('picUpdateIndexID').innerHTML) -1;
            successTarget = successTarget + "?" + QUERYPARAM_NAME_PICTURE_INDEX + "=" + pictureIndex;
        }
        window.location.href = successTarget;
    }
}


//END trekcrumb-picture.js