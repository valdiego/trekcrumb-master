"use strict";

function geoLoc_currentLocation_Init(callerPageName) {
    if(callerPageName == undefined || callerPageName == null) {
        console.error("geoLoc_currentLocation_Init() - ERROR: Caller pageName must be provided!");
        return;
    }
    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        console.error("geoLoc_currentLocation_Init() - ERROR: Cannot find geoLocCurrentLocationLayoutID layout!");
        return;
    }
    
    common_errorOrInfoMessage_hide();
    common_progressBarWidget_show(FRAGMENT_GEOLOC_CURRENTLOCATION);

    var geoLocMessage = document.getElementById("geoLocCurrentLocationMessageID");
    geoLocMessage.innerHTML = "";
    geoLocMessage.style.display = "none";
    
    var geoLocRetryButton = document.getElementById("geoLocCurrentLocationRetryBtnID");
    geoLocRetryButton.style.display = "none";
    
    geoLocCurrentLocationLayout.setAttribute(VARATTR_GEOLOC_LOCATION_CURRENT, null);
    geoLocCurrentLocationLayout.setAttribute(VARATTR_GEOLOC_LOCATION_ADDRESSSTRING, null);
    geoLocCurrentLocationLayout.setAttribute(VARATTR_GEOLOC_CALLERFRAGMENT, callerPageName);
    geoLocCurrentLocationLayout.style.display = "block";
    
    geoLoc_currentLocation();
}

/*
 * Gets current location by either GeoLocation API (browsers supporting HTML5) or IP address
 * (older browser)
 */
function geoLoc_currentLocation() {
    if (navigator.geolocation) {
        geoLoc_currentLocation_byGeoLocAPI();
    } else {
        //Older browsers: 
        geoLoc_currentLocation_byIPAddress();
    }
}

function geoLoc_currentLocation_byGeoLocAPI() {
    var positionOptions = { 
        enableHighAccuracy: GEOLOC_ENABLE_HIGH_ACCURACY,
        timeout: GEOLOC_TIMEOUT_VALUE,
        maximumAge: 0 
    };
        
    navigator.geolocation.getCurrentPosition(
        geoLoc_currentLocation_serviceResponse_byGeoLocAPI,
        geoLoc_currentLocation_errorCallback,
        positionOptions);
}

function geoLoc_currentLocation_byIPAddress() {
    yqlgeo.get('visitor', geoLoc_currentLocation_serviceResponse_byIPAddress);      
}

/*
 * The format of returned Position object is as the following:
 * { 
 *   "coords": {
 *     "latitude": a, "longitude": b, "altitude": c, 
 *     "accuracy": d, "altitudeAccuracy": e,
 *     "heading": f, "speed": g
 *   },
 *   "timestamp": h
 * }
 */
function geoLoc_currentLocation_serviceResponse_byGeoLocAPI(position) {
    var geoPosition = {  
        coords: {  
            latitude: position.coords.latitude,  
            longitude: position.coords.longitude,
            altitude: position.coords.altitude
        }
    };
    geoLoc_currentLocation_successCallback(geoPosition);  
}

/*
 * The format of returned Response object is as the following:
 * ???
 */
function geoLoc_currentLocation_serviceResponse_byIPAddress(response) {
    if(response.error)  {  
        var error = { code : 0 };
        geoLoc_currentLocation_errorCallback(error);  
        return;
    }
    
    //Compose GeoPosition object:
    var geoPosition = {  
        coords: {  
            latitude: response.place.centroid.latitude,  
            longitude: response.place.centroid.longitude  
        },  
        address: {  
            city: response.place.locality2.content,  
            region: response.place.admin1.content,  
            country: response.place.country.content  
        }  
    };
    geoLoc_currentLocation_successCallback(geoPosition);  
}

function geoLoc_currentLocation_successCallback(geoPosition) {
    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        //Async call, possible user has browsed away:
        console.error("geoLoc_currentLocation_successCallback() - ERROR: Cannot find geoLocCurrentLocationLayoutID layout!");
        return;
    }

    var geoPositionString = JSON.stringify(geoPosition);
    geoLocCurrentLocationLayout.setAttribute(VARATTR_GEOLOC_LOCATION_CURRENT, geoPositionString);

    common_progressBarWidget_hide(FRAGMENT_GEOLOC_CURRENTLOCATION);    

    //The caller page/fragment will decide what to do next:
    var callerFragment = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_CALLERFRAGMENT);
    if(callerFragment == FRAGMENT_TRIP_CREATE) {
        trip_create_currentLocationFound();
    } else if(callerFragment == FRAGMENT_PLACE_CREATE) {
        place_create_validateAndSubmit();
    } else if(callerFragment == FRAGMENT_PLACE_DETAIL) {
        place_mapCurrentLocation_Found();
    } else {
        console.log("geoLoc_successCallback() - ERROR: Unknown caller fragment value: " + callerFragment);
    }
}

function geoLoc_currentLocation_errorCallback(error) {
    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        //Async call, possible user has browsed away:
        return;
    }

    var errorMsg = null;
    switch(error.code) {
        case error.PERMISSION_DENIED:
            errorMsg = ERROR_GEOLOC_USER_DENIES_PERMISSION;
            break;
        case error.POSITION_UNAVAILABLE:
            errorMsg = ERROR_GEOLOC_LOC_INFO_NOT_AVAILABLE;
            break;
        case error.TIMEOUT:
            errorMsg = ERROR_GEOLOC_TIMEOUT;
            break;
        default:
            errorMsg = ERROR_GEOLOC_UNKNOWN;
            break;
    }
    common_errorOrInfoMessage_show([errorMsg], null);
    
    common_progressBarWidget_hide(FRAGMENT_GEOLOC_CURRENTLOCATION);    

    var geoLocMessage = document.getElementById("geoLocCurrentLocationMessageID");
    geoLocMessage.innerHTML = errorMsg;
    geoLocMessage.style.display = "block";
    
    var geoLocRetryButton = document.getElementById("geoLocCurrentLocationRetryBtnID");
    geoLocRetryButton.style.display = "inline-block";
}

function geoLoc_currentLocation_cancel() {
    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        //Invalid view:
        return;
    }

    common_errorOrInfoMessage_hide();
    geoLocCurrentLocationLayout.style.display = "none";
}

function geoLoc_currentLocation_retry() {
    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        //Invalid view:
        return;
    }

    common_errorOrInfoMessage_hide();
    common_progressBarWidget_show(FRAGMENT_GEOLOC_CURRENTLOCATION);

    var geoLocMessage = document.getElementById("geoLocCurrentLocationMessageID");
    geoLocMessage.innerHTML = "";
    geoLocMessage.style.display = "none";
    
    var geoLocRetryButton = document.getElementById("geoLocCurrentLocationRetryBtnID");
    geoLocRetryButton.style.display = "none";
    
    geoLoc_currentLocation();
}

/*
 * GeoLoc FindAddress: Gets location's address (locality names) given its latitude and longitude.
 * Note:
 * 1. This function operates with multitude efforts, that is we are attempting 
 *    to get the address by calling a number of 3rd-party webservices until
 *    we get what we want. Current 3rd-party we used were defined in trekcrumb-constants.js.
 *    We do this to anticipate if one 3rd-party server is down or reject our quota, then
 *    we call the back-up 3rd-party server.
 * 2. Parameters:
 *    - findAddressServerName: Valid value is "PRIMARY", "SECONDARY".
 *    - callerPageName: TripCreate page, PlaceCreate page, etc.
 */
function geoLoc_findAddress(lat, lon, findAddressServer, callerPageName) {
    switch(findAddressServer) {
        case "PRIMARY":
            var isRequestSuccess = geoLoc_findAddress_googleServer_serviceRequest(lat, lon, callerPageName);
            if(!isRequestSuccess) {
                geoLoc_findAddress(lat, lan, "SECONDARY");
            }
            break;
            
        case "SECONDARY":
            //TODO: Not implemented yet
            geoLoc_findAddress_twitterServer_serviceRequest(lat, lon, callerPageName);
            geoLoc_findAddress_completed(callerPageName);
            break;
            
        default:
    }
}

function geoLoc_findAddress_completed(callerPageName) {
    if(callerPageName == FRAGMENT_TRIP_CREATE) {
        trip_create_serviceRequest();
    } else if(callerPageName == FRAGMENT_PLACE_CREATE) {
        place_create_serviceRequest();
    }
}

/*
 * GeoLoc FindAddress with "PRIMARY" server.
 * Note:
 * If any failure occurred during request, the function returns 'false' and the caller must handle it.
 * Otherwise, it returns "true" while also invoke the callback response who will take care of any response 
 * after send() and evaluate the result.
 */
function geoLoc_findAddress_googleServer_serviceRequest(lat, lon, callerPageName) {
    if(!mDevice_isSupportAJAX) {
        console.error("geoLoc_findAddress_googleServer_serviceRequest() - ERROR: Client browser does NOT support AJAX! Cannot proceed.");
        return false;
    }
    
    //Compose request:
    var targetURL = GEOLOC_SERVER_FINDADDRESS_GOOGLEMAP + "&latlng=" + lat + "," + lon;
    var xmlhttpRequest = common_AJAX_getCORSRequest("GET", targetURL);
    if(xmlhttpRequest == null) {
        console.error("geoLoc_findAddress_googleServer_serviceRequest() - ERROR: Cannot make AJAX cross-domain calls!");
        return false;
    }

    //Else:
    xmlhttpRequest.onreadystatechange = function() {
        if (xmlhttpRequest.readyState == 4) {
            //Request finished and response is ready:
            geoLoc_findAddress_googleServer_serviceResponse(xmlhttpRequest, lat, lon, callerPageName);
        }
    };
    xmlhttpRequest.onerror = function() {
        console.error("geoLoc_findAddress_googleServer_serviceResponse() - ERROR: " + xmlhttpRequest.statusText);
        return false;
    };

    //Send to server:
    try {
        xmlhttpRequest.send();
    } catch(error) {
        console.error("geoLoc_findAddress_googleServer_serviceResponse() - ERROR: " + error.message);
        return false;
    }
    
    return true;
}

/*
 * GeoLoc FindAddress with "PRIMARY" server response.
 * Note:
 * 1. When response is a failure, the function would call the next FindAddress 3rd-party
 *    webservice (see geoLoc_findAddress() note for details).
 * 2. When response is a success, the function would call the next callerPageName's operation
 *    whatever that is.
 * 3. Structure:
 *
 */
function geoLoc_findAddress_googleServer_serviceResponse(xmlhttpRequest, lat, lon, callerPageName) {
    //Check for failure:
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    if(xmlhttpRequest.status != 200) {
        console.error("geoLoc_findAddress_googleServer_serviceResponse() - ERROR: " + xmlhttpRequest.statusText);
        geoLoc_findAddress(lat, lan, "SECONDARY", callerPageName);
        return;
    }
    
    var responseString = xmlhttpRequest.responseText;
    if(responseString == null 
            || responseString.trim() == ""
            || responseString.trim() == "{}") {
        console.error("geoLoc_findAddress_googleServer_serviceResponse() - ERROR: Response body is EMPTY!");
        geoLoc_findAddress(lat, lan, "SECONDARY", callerPageName);
        return;
    }

    var responseObj = JSON.parse(responseString);
    if(responseObj.status != "OK") {
        console.error("geoLoc_findAddress_googleServer_serviceResponse() - ERROR: " 
            + responseObj.status + " " + responseObj.error_message);
        geoLoc_findAddress(lat, lan, "SECONDARY", callerPageName);
        return;
    }
        
    //Else: Success
    var localityName = null;
    var cityName = null;
    var countyName = null;
    var provinceName = null;
    var countryName = null;
    var firstResult = responseObj.results[0];
    var firstResultAddressComponentArray = firstResult.address_components;
    for(var i in firstResultAddressComponentArray) {
        var typeArray = firstResultAddressComponentArray[i].types;
        switch(typeArray[0]) {
            case "country":
                countryName = firstResultAddressComponentArray[i].long_name;
                break;
                
            case "administrative_area_level_1":
                provinceName = firstResultAddressComponentArray[i].long_name;
                break;
                
            //case "administrative_area_level_2":
            //    countyName = firstResultAddressComponentArray[i].long_name;
            //    break;
                
            case "administrative_area_level_3":
                cityName = firstResultAddressComponentArray[i].long_name;
                break;
                
            case "locality":
                localityName = firstResultAddressComponentArray[i].long_name;
                break;
        }
    }
    
    var locationAddressString = geoLoc_locationAddress_composeString(
            lat, lon, 
            localityName, cityName, countyName, provinceName, countryName);
        
    //Save it to page variable:
    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        console.error("geoLoc_findAddress_googleServer_serviceResponse() - ERROR: Cannot find geoLocCurrentLocationLayoutID layout to store successful address: " + locationAddressString);
        return;
    }

    geoLocCurrentLocationLayout.setAttribute(VARATTR_GEOLOC_LOCATION_ADDRESSSTRING, locationAddressString);
    geoLoc_findAddress_completed(callerPageName);
}

/*
 * GeoLoc FindAddress with "SECONDARY" server.
 */
function geoLoc_findAddress_twitterServer_serviceRequest(lat, lon, callerPageName) {
    //TODO - This has not been implemented yet
    console.warning("geoLoc_findAddress_twitterServer_serviceRequest() - This method is not implemented yet!");
    return false;
}

function geoLoc_locationAddress_composeString(
        latitude, longitude,
        localityName, cityName, countyName, provinceName, countryName) {
    var locationAddressString = "";
    if(localityName != null) {
        locationAddressString = locationAddressString.concat(localityName);
    }
    if(cityName != null) {
        if(locationAddressString != "") {
            locationAddressString = locationAddressString.concat(", ");
        }
        locationAddressString = locationAddressString.concat(cityName);
    }
    if(countyName != null) {
        if(locationAddressString != "") {
            locationAddressString = locationAddressString.concat(", ");
        }
        locationAddressString = locationAddressString.concat(countyName);
    }
    if(provinceName != null) {
        if(locationAddressString != "") {
            locationAddressString = locationAddressString.concat(", ");
        }
        locationAddressString = locationAddressString.concat(provinceName);
    }
    if(countryName != null) {
        if(locationAddressString != "") {
            locationAddressString = locationAddressString.concat(", ");
        }
        locationAddressString = locationAddressString.concat(countryName);
    }
    
    if(locationAddressString == "") {
        //Default it:
        locationAddressString = geoLoc_locationAddress_composeDefault(latitude, longitude);
    }
    
    return locationAddressString;
}

function geoLoc_locationAddress_composeDefault(latitude, longitude) {
    return "(Latitude, Longitude) = " + latitude + ", " + longitude;
}

function geoLoc_locationAddress_isHasAddressValue(locationAddressString) {
    if(locationAddressString == null || locationAddressString.trim() == "") {
        return false;
    }
    if(locationAddressString.indexOf("(Latitude, Longitude)") != -1) {
        return false;
    }
    
    return true;
}

function geoLoc_locationAddress_getFirstAddressValue(locationAddressString) {
    var charIndex = locationAddressString.indexOf(",");
    if(charIndex > 0) {
        return locationAddressString.slice(0, charIndex);
    } else {
        return locationAddressString;
    }
}


//END trekcrumb-geoLoc.js