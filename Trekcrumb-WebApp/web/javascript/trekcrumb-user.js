"use strict";

/*
 * Generic function to remove any possible session caching
 * from previous user login.
 */
function user_generic_cleanSessionCache() {
    common_cookie_delete(COOKIE_NAME_DEFAULT_SESSION);
    userauthtoken_cookie_delete();
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, null);
}

function user_profileImage_Retrieve(userImageName, imageElementName) {
    if(validate_isStringEmpty(userImageName)) {
        return;
    }
    
    //Caching: Check if image already on locale storage:
    var cachedImageName = CACHE_KEY_PROFILE_IMAGENAME_PREFIX + userImageName;
    if(mDevice_isSupportLocaleStorage) {
        var imageBytes = localStorage.getItem(cachedImageName);
        if(imageBytes != undefined && imageBytes != null) {
            user_profileImage_DisplayContent(imageElementName, imageBytes);
            return;
        }
    } 
    
    //Else: Call server
    user_profileImage_RetrieveServiceRequest(userImageName, imageElementName);
}

function user_profileImage_RetrieveServiceRequest(userImageName, imageElementName) {
    if(mDevice_isSupportAJAX) {
        var xmlhttpRequest = new XMLHttpRequest();
        
        //Prepare response handling:
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                user_profileImage_RetrieveServiceResponse(userImageName, imageElementName, xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            console.log("user_profileImage_RetrieveServiceRequest() - ERROR: ");
        };
        
        //Compose request: 
        var user = {
            profileImageName: userImageName
        };
        
        //Prepare send (as JSON request):
        try {
            var urlTarget = APP_ROOT_URI + "/trekcrumb/user/userProfileImageDownload";
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send(JSON.stringify(user));
        } catch(error) {
            console.log("user_profileImage_RetrieveServiceRequest() - ERROR: ");
        }
    }
}

function user_profileImage_RetrieveServiceResponse(userImageName, imageElementName, xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    if(xmlhttpRequest.status != 200) {
        console.log("ERROR: user_profileImage_RetrieveServiceResponse() - Received error from server: " + xmlhttpRequest.statusText);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        console.log("ERROR: user_profileImage_RetrieveServiceResponse() - Received error from server: " + serverResponseObj.serviceError.errorText);
        return;
    }
    if(serverResponseObj.listOfUsers == null
            || serverResponseObj.listOfUsers[0] == null
            || serverResponseObj.listOfUsers[0].profileImageBytes == null) {
        console.log("ERROR: user_profileImage_RetrieveServiceResponse() - Image file NOT FOUND on server!");
        return;
    }

    //Success:
    var imageBytes = serverResponseObj.listOfUsers[0].profileImageBytes;
    
    //Cache into local storage:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_LOCAL, 
            CACHE_KEY_PROFILE_IMAGENAME_PREFIX + userImageName, 
            imageBytes);
    user_profileImage_DisplayContent(imageElementName, imageBytes);    
}

/* 
 * UserProfile Image: Since the imagelement name is very unique per user profile (one page can have rows of user profile images), 
 * we must be sure we are populating the correct element.
 */
function user_profileImage_DisplayContent(imageElementName, imageBytes) {
    if(imageElementName == undefined || imageElementName == null
            || imageBytes == undefined || imageBytes == null) {
        return;
    }
    
    var userImageElements = document.getElementsByName(imageElementName);
    if(userImageElements  != undefined && userImageElements != null) {
        //Set IMG source:
        var size = userImageElements.length;
        for(var i = 0; i < size; i++) {
            userImageElements[i].src = IMAGE_BYTES_TO_IMAGE_BASE64 + imageBytes;
            userImageElements[i].style.display = "inline-block";
        }        

        //Hide default (if any):
        var userImageDefaultElements = document.getElementsByName(imageElementName + "Default");
        if(userImageDefaultElements != undefined && userImageDefaultElements != null) {
            var size = userImageDefaultElements.length;
            for(var i = 0; i < size; i++) {
                userImageDefaultElements[i].style.display = "none";
            }
        }
    }
}

/*
 * UserView starting point.
 */
function user_view_Retrieve(isUserLoginProfile, username) {
    //Clear any cached data:
    trip_userTrips_Init();
    picture_list_userPictures_init();
    favorite_retrieveByUser_init();
    comment_user_init();
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY);

    //Redirect to server:
    if(isUserLoginProfile) {
        window.location.href = APP_ROOT_URI + "/trekcrumb/user/retrieve/me";
    } else {
        window.location.href = APP_ROOT_URI + "/trekcrumb/user/retrieve/" + username;
    }
}

/*
 * UserView to display result from back-end service.
 * Note: 
 * 1. The input parameter comes as java's JSON-formatted String. However, javascript
 *    receives it already as a JSON-parsed javascript object (not String). Therefore, we do not need 
 *    to do "JSON.parse(inputString)" to transform it into an javascript object. Instead, just use it right away.
 */
function user_view_DisplayContent(serviceResponseJSON) {
    var isSuccess = common_serviceResponse_evaluate(serviceResponseJSON);
    if(!isSuccess) {
        return;
    }
    if(serviceResponseJSON.listOfUsers == null) {
        //Success, but User is empty:
        common_errorOrInfoMessage_show([ERROR_MSG_SERVER_USER_NOT_FOUND ], null);
        return;
    }
    
    //Else: While all is good, each "embedded" fragment page will take care of themselves:
    user_view_DisplaySelectedTab(null);
}

/*
 * UserView navigation to display its main fragments: UserDetail, UserTrips, UserFavorites, UserComments or UserFriends.
 * 1. UserDetail: It is the default view for first landing. However for medium/large landscape screen, the default is to 
 *    display both UserDetail and UserTrips (therefore, UserTrips is loaded at first landing). 
 *    This screen size rules must be consistent between JS and CSS style. 
 *    Any further user tab-hopping: On small screen, UserDetail will be hidden. For medium/large landscape screen,
 *    it will always shown as the default.
 * 2. Others (UserTrips, UserPictures, etc.): When user tab-hopping, display it as a single view or half-screen 
 *    along with UserDetail.
 * 3. WARNING: 
 *    -> In order to support history which tab was current when user navigates away and click "BACK" button,
 *       we need to store the current tab (i.e., "fragmentName") as cache in SessionStorage.
 *    -> When displaying view from one to another, we may set one layout's display = "NONE" instead of visibility = "hidden".
 *       This will cause the layout in question to have offsetWidth/Height to ZERO, thus may cause error in 
 *       responsiveness calculation. To avoid this issue, we ensure to always use its parent layout's dimension
 *       OR to not use display none but to set marginLeft/Right and visibility to switch one view to the next.
 */
function user_view_DisplaySelectedTab(fragmentPage) {
    var userViewLayout = document.getElementById("userViewLayoutID");
    var parentLayoutWidth = userViewLayout.offsetWidth;
    var userDetailLayout = document.getElementById('userDetailLayoutID');
    var userTripsLayout = document.getElementById('userTripsLayoutID');
    var userTripsTitleLayout = document.getElementById('userTripsTitleID');
    var userPicturesLayout = document.getElementById('userPicturesLayoutID');
    var userPicturesTitleLayout = document.getElementById('userPicturesTitleID');
    var userFavoritesLayout = document.getElementById('userFavoritesLayoutID');
    var userFavoritesTitleLayout = document.getElementById('userFavoritesTitleID');
    var userCommentsLayout = document.getElementById('userCommentsLayoutID');
    var userCommentsTitleLayout = document.getElementById('userCommentsTitleLayoutID');
    var menuMainBackIcon = document.getElementById('menuBackIconID');
    
    //Check the current fragmentPage:
    if((fragmentPage == null || fragmentPage.trim() == "")
            && mDevice_isSupportLocaleStorage) {
        fragmentPage = sessionStorage.getItem(CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY);
    }
    if(fragmentPage == null || fragmentPage.trim() == "") {
        fragmentPage = userViewLayout.getAttribute(VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY);
    }
    if(fragmentPage == null || fragmentPage.trim() == "") {
        fragmentPage = FRAGMENT_USER_DETAIL;
    }
    
    //UserDetail:
    if(fragmentPage == FRAGMENT_USER_DETAIL) {
        //SHOW
        userViewLayout.setAttribute(VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_USER_DETAIL);
        common_localeStorage_setItem(
                CACHE_TYPE_LOCALESTORAGE_SESSION, 
                CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY, 
                FRAGMENT_USER_DETAIL); 
        userDetailLayout.style.display = "inline-block";
        userDetailLayout.scrollTop = 0;
        
        menuMainBackIcon.style.display = "none";
        menuMainBackIcon.setAttribute("onclick", "");
    
    } else {
        //HIDE: Show or hide depending screen size/orientation:
        if(parentLayoutWidth >= DEVICE_SCREEN_WIDTH_MAX_MEDIUM) {
            userDetailLayout.style.display = "inline-block";
        } else {
            userDetailLayout.style.display = "none";
        }
        
        menuMainBackIcon.style.display = "inline-block";
        menuMainBackIcon.setAttribute("onclick", "user_view_DisplaySelectedTab(FRAGMENT_USER_DETAIL)");
    }
    
    //Other fragments
    switch(fragmentPage) {
        case FRAGMENT_USER_DETAIL:
            //HIDE
            //1. UserTrips: Show half-page or hide
            userTripsTitleLayout.style.display = "none";
            if(parentLayoutWidth >= DEVICE_SCREEN_WIDTH_MAX_MEDIUM) {
                userTripsLayout.style.display = "inline-block";
            } else {
                userTripsLayout.style.display = "none";
            }
            
            //2. Others:
            userPicturesTitleLayout.style.display = "none";
            userPicturesLayout.style.display= "none";
            userFavoritesTitleLayout.style.display = "none";
            userFavoritesLayout.style.display = "none";
            userCommentsLayout.style.display = "none";
            userCommentsTitleLayout.style.display = "none";
            
            //Trigger responsiveness:
            responsive_adjustUserViewLayout();
            break;
            
        case FRAGMENT_TRIP_LIST_USER_TRIPS:
            userViewLayout.setAttribute(VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_TRIP_LIST_USER_TRIPS);
            common_localeStorage_setItem(
                    CACHE_TYPE_LOCALESTORAGE_SESSION, 
                    CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY, 
                    FRAGMENT_TRIP_LIST_USER_TRIPS); 

            //HIDE
            userPicturesTitleLayout.style.display = "none";
            userPicturesLayout.style.display= "none";
            userFavoritesTitleLayout.style.display = "none";
            userFavoritesLayout.style.display = "none";
            userCommentsLayout.style.display = "none";
            userCommentsTitleLayout.style.display = "none";
            
            //SHOW
            userTripsTitleLayout.style.display = "inline-block";
            userTripsLayout.style.display = "inline-block";
            userTripsLayout.scrollTop = 0;

            //Trigger responsiveness:
            responsive_adjustUserViewLayout();
            break;

        case FRAGMENT_PICTURE_LIST_USER_PICTURES:
            userViewLayout.setAttribute(VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_PICTURE_LIST_USER_PICTURES);
            common_localeStorage_setItem(
                    CACHE_TYPE_LOCALESTORAGE_SESSION, 
                    CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY, 
                    FRAGMENT_PICTURE_LIST_USER_PICTURES); 
            
            //HIDE
            userTripsTitleLayout.style.display = "none";
            userTripsLayout.style.display = "none";
            userFavoritesTitleLayout.style.display = "none";
            userFavoritesLayout.style.display = "none";
            userCommentsLayout.style.display = "none";
            userCommentsTitleLayout.style.display = "none";

            //SHOW
            userPicturesTitleLayout.style.display = "inline-block";
            userPicturesLayout.style.display = "inline-block";
            userPicturesLayout.scrollTop = 0;
            
            //Load data:
            picture_list_userPictures_current(
                    userPicturesLayout.getAttribute(VARATTR_USER_USERNAME), 
                    userPicturesLayout.getAttribute(VARATTR_USER_IS_USERLOGIN_PROFILE));

            //Trigger responsiveness:
            responsive_adjustUserViewLayout();
            break;

        case FRAGMENT_TRIP_LIST_USER_FAVORITES:
            userViewLayout.setAttribute(VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_TRIP_LIST_USER_FAVORITES);
            common_localeStorage_setItem(
                    CACHE_TYPE_LOCALESTORAGE_SESSION, 
                    CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY, 
                    FRAGMENT_TRIP_LIST_USER_FAVORITES); 
            
            //HIDE
            userTripsTitleLayout.style.display = "none";
            userTripsLayout.style.display = "none";
            userPicturesTitleLayout.style.display = "none";
            userPicturesLayout.style.display = "none";
            userCommentsLayout.style.display = "none";
            userCommentsTitleLayout.style.display = "none";

            //SHOW
            userFavoritesTitleLayout.style.display = "inline-block";
            userFavoritesLayout.style.display = "inline-block";
            userFavoritesLayout.scrollTop = 0;
            
            //Load data:
            favorite_retrieveByUser_current();

            //Trigger responsiveness:
            responsive_adjustUserViewLayout();
            break;

        case FRAGMENT_COMMENT_LIST_USER_COMMENTS:
            userViewLayout.setAttribute(VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_COMMENT_LIST_USER_COMMENTS);
            common_localeStorage_setItem(
                    CACHE_TYPE_LOCALESTORAGE_SESSION, 
                    CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY, 
                    FRAGMENT_COMMENT_LIST_USER_COMMENTS); 
            
            //HIDE
            userTripsTitleLayout.style.display = "none";
            userTripsLayout.style.display = "none";
            userPicturesTitleLayout.style.display = "none";
            userPicturesLayout.style.display = "none";
            userFavoritesTitleLayout.style.display = "none";
            userFavoritesLayout.style.display = "none";

            //SHOW
            userCommentsTitleLayout.style.display = "inline-block";
            userCommentsLayout.style.display = "inline-block";
            userCommentsLayout.scrollTop = 0;
            
            //Load data:
            comment_user_current();

            //Trigger responsiveness:
            responsive_adjustUserViewLayout();
            break;
            
        default:
            //None.
    }
}

/*
 * User Signup: Initializes the page.
 * NOTE:
 * 1. We clean any leftover caching data (in storage) BUT leave cookies in place to support 
 *    UserAutoToken operation if the same user is returning and trying to relogin. Hence,
 *    do not call user_generic_cleanSessionCache().
 */
function user_signup_init() {
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, null);
    
    //Redirect to server:
    window.location.href = WEBAPP_URL_USER_SIGNUP;
}

/*
 * User SignUp: Display content and menu given the selected tab menu.
 * NOTE:
 * 1. This method accepts the value of section selected as defined in trekcrumb-constant JS
 *    and it will display the content of that section. 
 * 2. In addition, this method accepts NULL section selected, which it will assume to look from 
 *    the URI. This is the scenario when back-end server returns result to the same view with
 *    error and need to display the previously selected section that it passes in the URI.
 * 3. If section selected is not provided in input nor in URI, it will default to FRAGMENT_USER_LOGIN.
 */
function user_signup_displaySelectedTab(sectionSelected) {
    common_errorOrInfoMessage_hide();
    
    if(sectionSelected == null) {
        //Look from URI:
        sectionSelected = common_url_getQueryParamValue(QUERYPARAM_NAME_SECTION_SELECTED);
        if(sectionSelected == null || sectionSelected.trim() == "") {
            sectionSelected = FRAGMENT_USER_LOGIN;
        }
    }
    
    var tabSelected = null;
    switch(sectionSelected) {
        case FRAGMENT_USER_CREATE:
            //Show:
            document.getElementById("userCreateLayoutID").style.display = "block";
            tabSelected = document.getElementById("menuUserCreateID");
            
            //Hide:
            document.getElementById("userLoginLayoutID").style.display = "none";
            document.getElementById("userForgetLayoutID").style.display = "none";
            break;
            
        case FRAGMENT_USER_FORGET:
            //Show:
            document.getElementById("userForgetLayoutID").style.display = "block";
            tabSelected = document.getElementById("menuUserForgetID");

            //Hide:
            document.getElementById("userLoginLayoutID").style.display = "none";
            document.getElementById("userCreateLayoutID").style.display = "none";
            break;

        default:
            //Show: FRAGMENT_USER_LOGIN:
            document.getElementById("userLoginLayoutID").style.display = "block";
            tabSelected = document.getElementById("menuUserLoginID");
            
            //Hide:
            document.getElementById("userCreateLayoutID").style.display = "none";
            document.getElementById("userForgetLayoutID").style.display = "none";
    }
    
    //Adjust tab display:
    if(tabSelected != null) {
        var tabs = document.getElementsByClassName("tab-menu-bar-item");
        var numOfTabs = tabs.length;
        for(var i = 0; i < numOfTabs; i++) {
            menu_selectedReset(tabs[i]);
        }
        menu_selected(tabSelected);
    }
}

function user_create_accept_agreement(isAccept) {
    if(isAccept) {
        document.getElementById("userCreateFunctionalButtonsID").style.display = "block";
    } else {
        document.getElementById("userCreateFunctionalButtonsID").style.display = "none";
    }
}

function user_create_validateAndSubmit() {
    var errorMessages = [];
    
    var validateUsernameResultError = validate_username("userCreateUsernameInputID");
    if(validateUsernameResultError != null) {
        errorMessages.push(validateUsernameResultError);
    }
    var validateEmailResultError = validate_email("userCreateEmailInputID", true, "userCreateEmailInputReenterID");
    if(validateEmailResultError != null) {
        errorMessages.push(validateEmailResultError);
    }
    var validatePasswordResultError = validate_password("userCreatePwdInputID", true, "userCreatePwdInputReenterID");
    if(validatePasswordResultError != null) {
        errorMessages.push(validatePasswordResultError);
    }
    var validateFullnameResultError = validate_fullname("userCreateFullnameInputID");
    if(validateFullnameResultError != null) {
        errorMessages.push(validateFullnameResultError);
    }
    
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
    } else {
        common_errorOrInfoMessage_hide();
        common_disabledBackground_show();
        document.forms["userCreateFormID"].submit();
    }
}

/*
 * Validate User Login conditions and form. 
 * The login menu may come from "Home" page or side menu. Prevent hitting server if we can 
 * invalidate it here.
 */
function user_login_validateAndSubmit() {
    var errorMessages = [];
    
    if(!mDevice_isSupportCookie) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED], null); 
        return;
    }
    
    var usernameInputElement = document.getElementById('userLoginUsernameInputID');
    if(usernameInputElement == undefined || usernameInputElement == null
            || usernameInputElement.value == null || usernameInputElement.value.trim() == "") {
        usernameInputElement.className += " body-error";
        errorMessages.push(ERROR_MSG_PARAM_INVALID_USERNAME_EMPTY);
    } else {
        usernameInputElement.className = usernameInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    }
    
    var pwdInputElement = document.getElementById('userLoginPwdInputID');
    if(pwdInputElement == undefined || pwdInputElement == null
            || pwdInputElement.value == null || pwdInputElement.value.trim() == "") {
        pwdInputElement.className += " body-error";
        errorMessages.push(ERROR_MSG_PARAM_INVALID_PASSWORD_EMPTY);
    } else {
        pwdInputElement.className = pwdInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    }
    
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
        return;
    }
    
    //Else:
    common_errorOrInfoMessage_hide();
    common_disabledBackground_show();
    
    if(document.getElementById("stayLoginCheckboxID").checked) {
        var deviceIdentity = common_window_identity();
        document.getElementById('userLoginFormID').setAttribute(
                "action", 
                WEBAPP_URL_USER_LOGIN_SUBMIT + "?" + QUERYPARAM_NAME_REMEMBER_ME + "=true&" 
                + QUERYPARAM_NAME_DEVICE_IDENTITY + "=" + deviceIdentity);
    } else {
        document.getElementById('userLoginFormID').setAttribute("action", WEBAPP_URL_USER_LOGIN_SUBMIT);
    }
    document.forms["userLoginFormID"].submit();
}

function user_forget_validateAndSubmit() {
    var errorMessages = [];
    var validateEmailResultError = validate_email("userForgetEmailInputID", false, null);
    if(validateEmailResultError != null) {
        errorMessages.push(validateEmailResultError);
    }
    
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
        return;
    }
    
    //Else:
    common_errorOrInfoMessage_hide();
    common_disabledBackground_show();
    document.forms["userForgetFormID"].submit();
}

function user_reset_validateAndSubmit() {
    var errorMessages = [];
    
    var validateEmailResultError = validate_email("userResetEmailInputID", false, null);
    if(validateEmailResultError != null) {
        errorMessages.push(validateEmailResultError);
    }
    var validatePasswordResultError = validate_password("userResetPwdNewInputID", true, "userResetPwdNewInputReenterID");
    if(validatePasswordResultError != null) {
        errorMessages.push(validatePasswordResultError);
    }
    var securityTokenInputElement = document.getElementById('userResetSecurityTokenInputID');
    if(securityTokenInputElement == undefined || securityTokenInputElement == null
            || securityTokenInputElement.value == null || securityTokenInputElement.value.trim() == "") {
        securityTokenInputElement.className += " body-error";
        errorMessages.push(ERROR_MSG_PARAM_INVALID_SECURITY_TOKEN);
    } else {
        securityTokenInputElement.className = securityTokenInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    }
    
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
    } else {
        common_errorOrInfoMessage_hide();
        common_disabledBackground_show();
        document.forms["userResetFormID"].submit();
    }
}

function user_login_refresh_init() {
    var userLoginRefreshLayout = document.getElementById('userLoginRefreshLayoutID');
    if(userLoginRefreshLayout == undefined || userLoginRefreshLayout == null) {
        //Invalid:
        return;
    }
    
    common_form_afterInit(FRAGMENT_USER_LOGIN_REFRESH, userLoginRefreshLayout);
}

function user_login_refresh_cancel() {
    var userLoginRefreshLayout = document.getElementById('userLoginRefreshLayoutID');
    if(userLoginRefreshLayout == undefined || userLoginRefreshLayout == null) {
        //Invalid:
        return;
    }
    
    //We are at the right page:
    common_errorOrInfoMessage_hide();
    userLoginRefreshLayout.style.display = "none";
}

function user_login_refresh_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    var errorMessages = [];
    
    var pwdInputElement = document.getElementById('userLoginRefreshPwdInputID');
    if(pwdInputElement == undefined || pwdInputElement == null
            || pwdInputElement.value == null || pwdInputElement.value.trim() == "") {
        pwdInputElement.className += " body-error";
        errorMessages.push(ERROR_MSG_PARAM_INVALID_PASSWORD_EMPTY);
    } else {
        pwdInputElement.className = pwdInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    }
    
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
        return;
    } 
    
    //Else:
    common_form_afterSubmit(FRAGMENT_USER_LOGIN_REFRESH);
    document.forms["userLoginRefreshFormID"].submit();
}

/*
 * User Logout - We accomodate whether to submit via form (if AJAX is not supported) or via AJAX (ideal).
 */
function user_logout() {
    if(!mDevice_isSupportAJAX) {
        user_logout_form();
    } else {
        user_logout_ServiceRequest();
    }
}

function user_logout_form() {
    /*
     * WARNING: Cleaning up any UserLogin caching is necessary. BUT, if we do this here - we delete
     * UserAuthToken cookies as well, and as the result - the back end components (controller) will 
     * not remove any UserAuthToken DB as part of User Logout operation.
     */
    user_generic_cleanSessionCache(); 

    //Redirect to server to invalidate session.
    window.location.href = APP_ROOT_URI + "/trekcrumb/user/logout/submit/form";
}

function user_logout_ServiceRequest() {
    /*
     * NOTE: We do not clean UserLogin caching during logout call, but AFTER logout is successful.
     * Hence, we include UserAuthToken cookies in the request call, thus allowing back end components 
     * (controller) to remove any UserAuthToken DB as part of User Logout operation.
     */
    if(mDevice_isSupportAJAX) {
        common_disabledBackground_show();
        
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                user_logout_ServiceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(
                    null, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
            common_disabledBackground_hide();
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/user/logout/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(null);
        } catch(error) {
            common_AJAX_opsError_show(
                    null, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
            common_disabledBackground_hide();
        }
    }
}

function user_logout_ServiceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(isSuccess) {
        //Clean any UserLogin caching/cookies
        user_generic_cleanSessionCache(); 
        
        //Redirect to home:
        window.location.href = WEBAPP_URL_USER_LOGOUT_AFTER;
    }
}

function user_update_init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    var userUpdateLayout = document.getElementById("userUpdateLayoutID");
    if(userUpdateLayout == undefined || userUpdateLayout == null) {
        //Illegal access:
        return;
    }
    
    //Populate editable input fields with original values:
    document.getElementById("userUpdateFullnameInputID").value = document.getElementById("userUpdateFullnameOrigID").value;
    document.getElementById("userUpdateLocationInputID").value = document.getElementById("userUpdateLocationOrigID").value;
    document.getElementById("userUpdateSummaryInputID").value = document.getElementById("userUpdateSummaryOrigID").value;
    
    common_form_afterInit(FRAGMENT_USER_UPDATE, userUpdateLayout);
    var userUpdateProfileTab = document.getElementById("userUpdateProfileMenuID");
    user_update_displaySelectedTab(userUpdateProfileTab, FRAGMENT_USER_UPDATE_PROFILE);
}

function user_update_cancel() {
    var userUpdateLayout = document.getElementById("userUpdateLayoutID");
    if(userUpdateLayout == undefined || userUpdateLayout == null) {
        //Illegal access:
        return;
    }
    
    //Clean any leftover:
    common_errorOrInfoMessage_hide();
    userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, "");
    common_fileAPIReader_inputImageFile_cancelAndHide();
    userUpdateLayout.style.display = "none";
}

function user_update_displaySelectedTab(tabSelectedElement, sectionSelected) {
    common_errorOrInfoMessage_hide();
    
    if(tabSelectedElement != null) {
        var tabs = document.getElementsByClassName("tab-menu-bar-item");
        var numOfTabs = tabs.length;
        for(var i = 0; i < numOfTabs; i++) {
            menu_selectedReset(tabs[i]);
        }
        menu_selected(tabSelectedElement);
    }
    
    var userUpdateLayout = document.getElementById("userUpdateLayoutID");
    switch(sectionSelected) {
        case FRAGMENT_USER_UPDATE_PROFILE:
            userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, FRAGMENT_USER_UPDATE_PROFILE);
            
            //Show:
            document.getElementById("userUpdateProfileFormContentID").style.display = "block";
            
            //Hide:
            document.getElementById("userUpdateProfileImageFormContentID").style.display = "none";
            document.getElementById("userUpdateAccountFormContentID").style.display = "none";
            break;
            
        case FRAGMENT_USER_UPDATE_PROFILEIMAGE:
            userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, FRAGMENT_USER_UPDATE_PROFILEIMAGE);
            
            //Show:
            document.getElementById("userUpdateProfileImageFormContentID").style.display = "block";

            //Hide:
            document.getElementById("userUpdateProfileFormContentID").style.display = "none";
            document.getElementById("userUpdateAccountFormContentID").style.display = "none";
            
            //Reset:
            common_fileAPIReader_inputImageFile_cancelAndHide();
            break;
            
        case FRAGMENT_USER_UPDATE_ACCOUNT:
            userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, "");

            //Show:
            document.getElementById("userUpdateAccountFormContentID").style.display = "block";

            //Hide:
            document.getElementById("userUpdateProfileFormContentID").style.display = "none";
            document.getElementById("userUpdateProfileImageFormContentID").style.display = "none";
            
            //Reset:
            user_update_account_init(true, false, false, false);
            break;

        case FRAGMENT_USER_UPDATE_ACCOUNT_EMAIL:
            userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, FRAGMENT_USER_UPDATE_ACCOUNT_EMAIL);
            
            //Show:
            document.getElementById("userUpdateAccountEmailFormContentID").style.display = "block";

            //Hide
            document.getElementById("userUpdateAccountPwdFormContentID").style.display = "none";

            //Reset:
            user_update_account_init(false, true, false, true);
            break;
            
        case FRAGMENT_USER_UPDATE_ACCOUNT_PWD:
            userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, FRAGMENT_USER_UPDATE_ACCOUNT_PWD);
            
            //Show:
            document.getElementById("userUpdateAccountPwdFormContentID").style.display = "block";
            
            //Hide
            document.getElementById("userUpdateAccountEmailFormContentID").style.display = "none";

            //Reset:
            user_update_account_init(false, false, true, true);
            break;
            
        case FRAGMENT_USER_UPDATE_DEACTIVATE:
            //Show:
            var deactivateAccountElement = document.getElementById("userDeactivateCheckboxID");
            if(deactivateAccountElement.checked) {
                userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, FRAGMENT_USER_UPDATE_DEACTIVATE);
                document.getElementById("userDeactivateCheckboxIDWarning").style.display = "block";
            } else {
                userUpdateLayout.setAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED, "");
                document.getElementById("userDeactivateCheckboxIDWarning").style.display = "none";
            }
        
            //Hide
            document.getElementById("userUpdateEmailID").checked = false;
            document.getElementById("userUpdateAccountEmailFormContentID").style.display = "none";
            document.getElementById("userUpdatePwdID").checked = false;
            document.getElementById("userUpdateAccountPwdFormContentID").style.display = "none";

            //Reset:
            user_update_account_init(false, true, true, false);
            break;
        default:
            return;            
    }
}

function user_update_account_init(isAll, isEmailInit, isPasswordInit, isDeactivateInit) {
    if(isAll) {
        document.getElementById("userUpdateEmailID").checked = false;
        document.getElementById("userUpdateAccountEmailFormContentID").style.display = "none";
        document.getElementById("userUpdatePwdID").checked = false;
        document.getElementById("userUpdateAccountPwdFormContentID").style.display = "none";
        document.getElementById("userDeactivateCheckboxID").checked = false;
        document.getElementById("userDeactivateCheckboxIDWarning").style.display = "none";
    
    } else {
        if(isEmailInit == true) {
            var emailInput = document.getElementById("userUpdateEmailInputID");
            emailInput.value = "";
            emailInput.className = emailInput.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );

            var emailReenterInput = document.getElementById("userUpdateEmailInputReenterID");
            emailReenterInput.value = "";
            emailReenterInput.className = emailReenterInput.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
            document.getElementById("userUpdateEmailInputReenterIDError").style.display = "none";
        }

        if(isPasswordInit) {
            document.getElementById("userUpdatePwdCurrentInputID").value = "";

            var passwordNewInput = document.getElementById("userUpdatePwdNewInputID");
            passwordNewInput.value = "";
            passwordNewInput.className = passwordNewInput.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );

            var passwordNewReenterInput = document.getElementById("userUpdatePwdNewInputReenterID");
            passwordNewReenterInput.value = "";
            passwordNewReenterInput.className = passwordNewReenterInput.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
            document.getElementById("userUpdatePwdNewInputReenterIDError").style.display = "none";
        }

        if(isDeactivateInit) {
            document.getElementById("userDeactivateCheckboxID").checked = false;
            document.getElementById("userDeactivateCheckboxIDWarning").style.display = "none";
        }
    }
}

function user_update_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    var errorMessages = [];

    //Validate:
    var sectionBeingUpdated = document.getElementById('userUpdateLayoutID').getAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED);
    switch(sectionBeingUpdated) {
        case FRAGMENT_USER_UPDATE_PROFILE:
            var validateFullnameResultError = validate_fullname("userUpdateFullnameInputID");
            if(validateFullnameResultError != null) {
                errorMessages.push(validateFullnameResultError);
            }
            var validateLocationResultError = validate_location("userUpdateLocationInputID");
            if(validateLocationResultError != null) {
                errorMessages.push(validateLocationResultError );
            }
            break;
            
        case FRAGMENT_USER_UPDATE_PROFILEIMAGE:
            var validateInputImageFileError = validate_inputImageFile();
            if(validateInputImageFileError != null) {
                errorMessages.push(validateInputImageFileError);
            }
            break;
            
        case FRAGMENT_USER_UPDATE_ACCOUNT_EMAIL:
            var validateEmailResultError = validate_email("userUpdateEmailInputID", true, "userUpdateEmailInputReenterID");
            if(validateEmailResultError != null) {
                errorMessages.push(validateEmailResultError);
            }
            break;
            
        case FRAGMENT_USER_UPDATE_ACCOUNT_PWD:
            var validatePasswordResultError = validate_password("userUpdatePwdNewInputID", true, "userUpdatePwdNewInputReenterID"); 
            if(validatePasswordResultError != null) {
                errorMessages.push(validatePasswordResultError);
            }
            break;
            
        case FRAGMENT_USER_UPDATE_DEACTIVATE:
            //Nothing to validate
            user_generic_cleanSessionCache(); 
            break;

        default:
            console.log("user_update_validateAndSubmit() - ERROR!!! Section being updated is unknown [" + sectionBeingUpdated + "]");
            errorMessages.push("Invalid data/section to update!");
    }

    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
        return;
    }
    
    //Call server:
    common_form_afterSubmit(FRAGMENT_USER_UPDATE);
    user_update_ServiceRequest();
}

function user_update_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var userId = document.getElementById('userUpdateUserID').value;
        var userToUpdate = {
            userId: userId
        };

        var sectionBeingUpdated = document.getElementById('userUpdateLayoutID').getAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED);
        switch(sectionBeingUpdated) {
            case FRAGMENT_USER_UPDATE_PROFILE:
                userToUpdate.fullname = document.getElementById('userUpdateFullnameInputID').value;
                userToUpdate.location  = document.getElementById('userUpdateLocationInputID').value;
                userToUpdate.summary = document.getElementById('userUpdateSummaryInputID').value;
                break;

            case FRAGMENT_USER_UPDATE_PROFILEIMAGE:
                userToUpdate.profileImageBase64Data = document.getElementById('imageBase64DataID').value;
                break;

            case FRAGMENT_USER_UPDATE_ACCOUNT_EMAIL:
                userToUpdate.email = document.getElementById('userUpdateEmailInputID').value;
                break;

            case FRAGMENT_USER_UPDATE_ACCOUNT_PWD:
                userToUpdate.username = document.getElementById('userUpdateUsernameID').value;
                userToUpdate.password = document.getElementById('userUpdatePwdCurrentInputID').value;
                userToUpdate.passwordNew = document.getElementById('userUpdatePwdNewInputID').value;
                break;
        }

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                user_update_ServiceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_USER_UPDATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = null;
        if(sectionBeingUpdated == FRAGMENT_USER_UPDATE_DEACTIVATE) {
            urlTarget = APP_ROOT_URI + "/trekcrumb/user/deactivate/submit/AJAX";
            
        } else if(sectionBeingUpdated == FRAGMENT_USER_UPDATE_ACCOUNT_PWD) {
            urlTarget = APP_ROOT_URI + "/trekcrumb/user/updatePwd/submit/AJAX";
            
        } else {
            urlTarget = APP_ROOT_URI + "/trekcrumb/user/update/submit/AJAX/" + sectionBeingUpdated;
        }
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            
            var userToUpdateJSONString = JSON.stringify(userToUpdate);
            
            //xmlhttpRequest.send(JSON.stringify(userToUpdate));
            xmlhttpRequest.send(userToUpdateJSONString);
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_USER_UPDATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function user_update_ServiceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var userUpdateLayout = document.getElementById("userUpdateLayoutID");
    if(userUpdateLayout == undefined || userUpdateLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has moved away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_USER_UPDATE, xmlhttpRequest);
    if(isSuccess) {
        var sectionBeingUpdated = userUpdateLayout.getAttribute(VARATTR_USER_UPDATE_SECTION_ON_EDITED);
        if(sectionBeingUpdated == FRAGMENT_USER_UPDATE_DEACTIVATE) {
            //Redirect to home:
            window.location.href = APP_ROOT_URI + "/trekcrumb/home";
        } else {
            //Redirect to server to retrieve newly updated user profile:
            window.location.href = APP_ROOT_URI + "/trekcrumb/user/retrieve/me";
        }
    }
}

function user_reactivate_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    common_disabledBackground_show();
    
    var formActionTarget = APP_ROOT_URI + "/trekcrumb/user/reactivate/submit/form";
    document.getElementById('userReactivateFormID').setAttribute("action", formActionTarget);
    document.forms["userReactivateFormID"].submit();
}

function user_contactUs_validateAndSubmit() {
    var errorMessages = [];
    var validateEmailResultError = validate_email("userContactUsEmailInputID", false, null);
    if(validateEmailResultError != null) {
        errorMessages.push(validateEmailResultError);
    }

    var contactUsSummaryElement = document.getElementById("userContactUsSummaryInputID");
    var contactUsSummaryValue = contactUsSummaryElement.value;
    if(contactUsSummaryValue == null || contactUsSummaryValue.trim() == "") {
        errorMessages.push(ERROR_MSG_PARAM_INVALID_CONTACT_US_SUMMARY);
        contactUsSummaryElement.className += " body-error";
    } else {
        contactUsSummaryElement.className = contactUsSummaryElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    }
    
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
        return;
    }
    
    //Else:
    common_errorOrInfoMessage_hide();
    common_disabledBackground_show();
    //document.getElementById("supportContactUsLayoutID").style.overflowY = "hidden";
    document.forms["userContactUsFormID"].submit();
}

//END trekcrumb-user.js