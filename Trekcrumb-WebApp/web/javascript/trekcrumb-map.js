"use strict";
/*
 * This JS functions as map factory. Its main function is to redirect map operation calls to
 * the concrete map API implementations (map-google, map-openstreet, etc.) where the actual map logics 
 * and work occur. The configuration for which map API to use is specified in 
 * trekcrumb-constants JS.
 */
 
function map_detail_DisplayContent() {
    //TODO - factory logic based on config which map API to use
    //IF(MapGoogle): 
    /* 
     * TODO (10/2015): Is this really an issue or necessary?
     * Execute it ONLY IF after the document finishes loading to ensure
     * the elements and their attributes have been created.
     */
    //google.maps.event.addDomListener(window, 'load', mapGoogle_detail_DisplayContent);
    mapGoogle_detail_DisplayContent();
}

function map_trigger_resizeEvent() {
    //TODO - factory logic based on config which map API to use
    //IF(MapGoogle):
    mapGoogle_trigger_resizeEvent();
}

function map_trigger_markerClickedEvent(index) {
    //TODO - factory logic based on config which map API to use
    //IF(MapGoogle):
    mapGoogle_trigger_markerClickedEvent(index);
}

function map_currentLocation_displayContent(lat, lon) {
    //TODO - factory logic based on config which map API to use
    //IF(MapGoogle):
    mapGoogle_currentLocation_init(lat, lon);
}

/*
 * Draws static map given a (latitude, longitude) coordinate.
 */
function map_static_displayContent(lat, lon) {
    //TODO - factory logic based on config which map API to use
    //IF(MapGoogle):
    mapGoogle_static_displayContent(lat, lon);
}

//END trekcrumb-map.js