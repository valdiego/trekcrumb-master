/*
 * App global variables functions as user session variables whose values keep changing (or null) 
 * based on 'CURRENT' user navigating the pages and links. These are intentionally 
 * global whose values are changed on only specific points, but used widely on various pages and JS operations.
 * NOTE:
 * 1. Alternatively, we could have kept these variables as HTML hidden input elements. This approach
 *    would require JS operation to look and read for them, then check if they are not undefined nor null,
 *    then parse it into JS datatype (since by default they became String).
 * 2. Alternatively, we could keep some of these variables in browser SessionStorage
 * 3. Consequently, we will need to initialize some of these variables on certain navigation points
 *    to ensure they are refresh and not values from previous cache/history.   
 */

//DEVICE
var mDevice_orientation = null;
var mDevice_isSupportAJAX;
var mDevice_isSupportLocaleStorage;
var mDevice_isSupportCookie;
var mSession_Id = null;

//HOME PAGE
var mHome_numOfListItemsToDisplay = 0;

//END trekcrumb-variables.js