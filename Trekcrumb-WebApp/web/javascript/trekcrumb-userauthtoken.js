"use strict";

function userauthtoken_cookie_create(userAuthTokenIdCrypted, userIdCrypted, userAuthTokenCrypted) {
    //Delete any previous cookies
    userauthtoken_cookie_delete();
    
    //Create new ones:
    common_cookie_create(COOKIE_NAME_USER_ID, userIdCrypted, COOKIE_EXPIRATION_TIME_MS);
    common_cookie_create(COOKIE_NAME_USER_AUTH_TOKEN, userAuthTokenCrypted, COOKIE_EXPIRATION_TIME_MS);
    common_cookie_create(COOKIE_NAME_USER_AUTH_TOKEN_ID, userAuthTokenIdCrypted, COOKIE_EXPIRATION_TIME_MS);
}

function userauthtoken_cookie_delete() {
    common_cookie_delete(COOKIE_NAME_USER_ID);
    common_cookie_delete(COOKIE_NAME_USER_AUTH_TOKEN);
    common_cookie_delete(COOKIE_NAME_USER_AUTH_TOKEN_ID);
}

/*
 * UserAuthToken List initialization
 * 1. Unlike our typical page/fragment, we do not populate UserAuthToken fragment with its data 
 *    on the server (via JSP) before returning to the client, neither do we populate it on client
 *    during loading (via JS). Instead, we would populate the rows only when user click a menu
 *    to display it.
 * 2. To support this design, we save the UserAuthToken list data in a hidden input field to be used
 *    later dynamically.
 *    WARNING: The list is a JSON object, and we MUST store it in HTML DOM input element as a String
 *    (Otherwise, the data would be corrupted due to chars such as double quote, single quote, etc.)
 */
function userauthtoken_list_init(listOfUserAuthTokensJSON) {
    var userAuthTokenLayout = document.getElementById('userAuthTokenLayoutID');
    if(userAuthTokenLayout == undefined || userAuthTokenLayout == null) {
        return;
    }

    if(listOfUserAuthTokensJSON != null) {
        document.getElementById('userAuthTokenListInputID').value = JSON.stringify(listOfUserAuthTokensJSON);
    }
}


function userauthtoken_list_showOrHide() {
    menu_detailLayout_showOrHide('UserAuthTokenContentID');
    
    var detailLayout = document.getElementById('UserAuthTokenContentID');
    if(detailLayout.style.display == "block") {
        //Find if it has been populated:
        var authTokenRows = document.getElementsByName("userAuthTokenRowName");
        if(authTokenRows != null && authTokenRows.length > 0) {
            //Nothing more to be done
            return;
        
        } else {
            //Populate and display them:
            userauthtoken_list_displayRows(null);
        }
    }
}

/*
 * UserAuthToken List populate and display
 * 1. Dynamically repopulate the fragment with rows from the list. 
 * 2. To prevent old rows and new rows were appended to old/existing rows, we must first
 *    clean the fragment from any existing rows, then adding new rows from clean layout.
 * 3. The data source is either from input param (as JSON object) or from hidden input
 *    field (String values that needs to parse into JSON object).
 */
function userauthtoken_list_displayRows(listOfUserAuthTokensJSON) {
    var userAuthTokenContent = document.getElementById("UserAuthTokenContentID");
    var userAuthTokenRowTemplate = document.getElementById("userAuthTokenRowTemplateID");
    if(userAuthTokenContent == undefined || userAuthTokenContent == null
            || userAuthTokenRowTemplate == undefined || userAuthTokenRowTemplate == null) {
        console.error("userauthtoken_list_displayRows() - ERROR: Missing required layouts in current view!");
        return;
    }
    
    userAuthTokenContent.scrollTop;
    
    if(listOfUserAuthTokensJSON == null) {
        //The source data is available as hidden input element:
        var listOfUserAuthTokenString = document.getElementById('userAuthTokenListInputID').value;
        if(listOfUserAuthTokenString == undefined || listOfUserAuthTokenString == null || listOfUserAuthTokenString.trim().length == 0) {
            userAuthTokenContent.innerHTML = INFO_MSG_USERAUTHTOKEN_LIST_EMPTY ;
            return;
        } else {
            listOfUserAuthTokensJSON = JSON.parse(listOfUserAuthTokenString);
        }
    }
    
    /*
     * Clean out any existing rows.
     * 1. UserAuthTokenContentID layout is the parent of these rows
     * 2. WARNING: We are working on an array object, and removing entity out of this array. Upon entity removal,
     *    browser engine automatically updates the array's content and size since it is the same referenced object.
     *    Therefore, we always retrieve entity "index 0" out of the array until there is no more left.
     */
    var authTokenRows = document.getElementsByName("userAuthTokenRowName");
    if(authTokenRows != null && authTokenRows.length > 0) {
        var numOfRows = authTokenRows.length;
        for(var i = 0; i < numOfRows; i++) {
            var row = authTokenRows[0];
            if(row != undefined) {
                userAuthTokenContent.removeChild(row);
            }
        }
    }
    
    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    var listSize = listOfUserAuthTokensJSON.length;
    for(var index = 0; index < listSize; index++) {
        var userAuthToken = listOfUserAuthTokensJSON[index];
        var rowCurrent = userAuthTokenRowTemplate.cloneNode(true);
        
        //Style
        if(index % 2 == 0) {
            rowCurrent.className += " " + STYLE_ELEMENT_ROW_EVEN;
        } else {
            rowCurrent.className += " " + STYLE_ELEMENT_ROW_ODD;
        }
        
        //Data
        rowCurrent.id = "";
        rowCurrent.setAttribute("name", "userAuthTokenRowName");
        rowCurrent.querySelector("#deviceIdentityID").innerHTML = userAuthToken.deviceIdentity;
        common_date_translateToLocaleTime(rowCurrent.querySelector("#createdDateID"), userAuthToken.created, true, false);
        
        //Menu Delete:
        rowCurrent.querySelector("#menuDeleteID").setAttribute("onclick", "userauthtoken_delete('" + userAuthToken.id + "')");
        
        //Finally, display it:
        rowCurrent.style.display = 'block';
        userAuthTokenContent.appendChild(rowCurrent);
    }
}

function userauthtoken_delete(authTokenID) {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    
    common_errorOrInfoMessage_hide();
    common_disabledBackground_show();
    userauthtoken_delete_serviceRequest(authTokenID);
}

function userauthtoken_delete_serviceRequest(authTokenID) {
    var userId = document.getElementById('userID').value;
    var authTokenToDelete = {
        userId: userId,
        id: authTokenID
    };

    //Prepare and send as JSON request:
    var xmlhttpRequest = new XMLHttpRequest();
    xmlhttpRequest.onreadystatechange = function() {
        if (xmlhttpRequest.readyState == 4) {
            //Request finished and response is ready:
            userauthtoken_delete_serviceResponse(xmlhttpRequest);
        }
    };
    xmlhttpRequest.onerror = function() {
        common_AJAX_opsError_show(
                null, 
                xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        common_disabledBackground_hide();
    };

    var urlTarget = APP_ROOT_URI + "/trekcrumb/userauthtoken/delete/submit/AJAX";
    try {
        xmlhttpRequest.open("POST", urlTarget, true);
        xmlhttpRequest.setRequestHeader("Content-type","application/json");
        xmlhttpRequest.send(JSON.stringify(authTokenToDelete));
    } catch(error) {
        common_AJAX_opsError_show(null, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        common_disabledBackground_hide();
    }
}

function userauthtoken_delete_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var userAuthTokenLayout = document.getElementById("userAuthTokenLayoutID");
    if(userAuthTokenLayout == undefined || userAuthTokenLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    common_disabledBackground_hide();
    if(isSuccess) {
        //Refresh UserAuthToken's list
        var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
        var listOfUserAuthTokensJSON = serverResponseObj.listOfUserAuthTokens;
        document.getElementById('userAuthTokenListInputID').value = JSON.stringify(listOfUserAuthTokensJSON);
        userauthtoken_list_displayRows(listOfUserAuthTokensJSON);
    }
}


//END trekcrumb-userauthtoken.js