"use strict";

function place_detail_DisplayContent() {
    //Ensure we are on the right page with required elements in place:
    var paginationPlaceLayout = document.getElementById('paginationPlaceLayoutID');
    if(paginationPlaceLayout == undefined || paginationPlaceLayout == null) {
        return;
    }
    var placeDetailLayout = document.getElementById('placeDetailLayoutID');
    if(placeDetailLayout == undefined || placeDetailLayout == null) {
        return;
    }

    var numOfTotalPlaces = parseInt(paginationPlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    if(numOfTotalPlaces > 0) {
        var listOfPlacesString = document.getElementById('pagePlace_listOfPlacesID').value;
        if(listOfPlacesString == undefined 
                || listOfPlacesString == null 
                || listOfPlacesString == "") {
            console.log("place_detail_DisplayContent() - ERROR: Number of places is [" + numOfTotalPlaces 
                + "] but found EMPTY pagePlace_listOfPlacesID!");
            return;
        }

        //Get the current place:
        var listOfPlacesJSON = JSON.parse(listOfPlacesString);
        var placeCurrentIndex = parseInt(paginationPlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));

        //Prevent invalid ArrayOutOfBound error:
        if(placeCurrentIndex < 0) {
            placeCurrentIndex = 0;
            paginationPlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, placeCurrentIndex);
        } else if(placeCurrentIndex > (numOfTotalPlaces - 1)) {
            placeCurrentIndex = (numOfTotalPlaces - 1); 
            paginationPlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, placeCurrentIndex);
        }
        var place = listOfPlacesJSON[placeCurrentIndex];
        
        //Display contents:
        document.getElementById('placeLocationID').innerHTML = place.location;
        document.getElementById('placeNoteID').innerHTML = place.note;
        common_date_translateToLocaleTime(document.getElementById('placeCreatedDateID'), place.created, true, false);
    
    } else {
        //Empty places:
        var mapMessage = document.getElementById('mapMessageID');
        if(mapMessage != undefined && mapMessage != null) {
            mapMessage.innerHTML = ERROR_MSG_PLACE_EMPTY;
        }
        
        //Populate note, too, for case where userLogin has access to trip, and trip is active:
        var placeNoteElement = document.getElementById('placeNoteID');
        if(placeNoteElement != undefined && placeNoteElement != null) {
            placeNoteElement.innerHTML = ERROR_MSG_PLACE_EMPTY;
        }
    }
}

/*
 * Display/Load place detail > map canvas
 * WARNING: 
 * 1. We shall only initialize map if it is not yet so (e.g., due to its layout was hidden/display was "none"). 
 *    Otherwise, we will just readjust its dimension by firing a window/element 'resize' event for use by map API to react with
 *    necessary adjustments.
 * 2. When using GoogleMap API, if there was an async error from google server, it may wipe out
 *    the mapCanvasID DIV entirely resulting a NULL element!!!
 * 3. Do NOT put place_detail_DisplayMapCanvas() as part of (inside) place_detail_DisplayContent()
 *    since the latter function gets called from various business logic that has nothing to do with
 *    displaying/reloading the map canvas.
 */
function place_detail_DisplayMapCanvas() {
    //Ensure we are on the right page with required elements in place:
    var paginationPlaceLayout = document.getElementById('paginationPlaceLayoutID');
    if(paginationPlaceLayout == undefined || paginationPlaceLayout == null) {
        return;
    }

    var numOfTotalPlaces = parseInt(paginationPlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    if(numOfTotalPlaces > 0) {
        var mapCanvas = document.getElementById('mapCanvasID');
        if(mapCanvas != undefined && mapCanvas != null) {
            if(mapCanvas.getAttribute(VARATTR_MAP_IS_MAP_INITIALIZED) == "false") {
                map_detail_DisplayContent();
                //google.maps.event.trigger(window, 'load');
            } else {
                map_trigger_resizeEvent();
            }
        }
    }
}

function place_detail_Next() {
    if(pagePlace_next()) {
        place_detail_DisplayContent();
        
        var paginationPlaceLayout = document.getElementById('paginationPlaceLayoutID');
        var indexCurrent = parseInt(paginationPlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
        map_trigger_markerClickedEvent(indexCurrent);
    }
}

function place_detail_Previous() {
    if(pagePlace_previous()) {
        place_detail_DisplayContent();
        
        var paginationPlaceLayout = document.getElementById('paginationPlaceLayoutID');
        var indexCurrent = parseInt(paginationPlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
        map_trigger_markerClickedEvent(indexCurrent);
    }
}

function place_create_Init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    
    geoLoc_currentLocation_Init(FRAGMENT_PLACE_CREATE);
}

function place_create_cancel() {
    var placeCreateLayout = document.getElementById('placeCreateLayoutID');
    if(placeCreateLayout == undefined || placeCreateLayout == null) {
        //Invalid:
        return;
    }
    
    placeCreateLayout.style.display = "none";
}

function place_create_validateAndSubmit() {
    var placeCreateLayout = document.getElementById('placeCreateLayoutID');
    if(placeCreateLayout == undefined || placeCreateLayout == null) {
        //Invalid view:
        console.error("place_create_validateAndSubmit() - ERROR: Could not find placeCreateLayoutID layout!");
        return;
    }

    var geoLocCurrentLocationLayout = document.getElementById('geoLocCurrentLocationLayoutID');
    var geoLocationCurrentString = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_CURRENT);
    if(geoLocationCurrentString == null 
            || geoLocationCurrentString.trim() == "null"
            || geoLocationCurrentString.trim() == "") {
        common_errorOrInfoMessage_show([ERROR_GEOLOC_CURRENTLOCATION_EMPTY], null);
        geoLocCurrentLocationLayout.style.display = "none";
        return;
    }
    
    //Else:
    geoLocCurrentLocationLayout.style.display = "none";
    placeCreateLayout.style.display = "block";
    document.getElementById(FRAGMENT_PLACE_CREATE + 'contentID').scrollTop = 0;
    
    /*
     * Note: 
     * We do not immediately call place_create_serviceRequest() here. Instead, we call 
     * findAddress() function to try to get the location address given its (lat,lan) 
     * before we save them.
     */
    var geoLocationCurrentObj = JSON.parse(geoLocationCurrentString);
    geoLoc_findAddress(geoLocationCurrentObj.coords.latitude, 
                       geoLocationCurrentObj.coords.longitude,
                       "PRIMARY",
                       FRAGMENT_PLACE_CREATE);
}

function place_create_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
        var geoLocationCurrentString = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_CURRENT);
        var geoLocLocationCurrentObj = JSON.parse(geoLocationCurrentString);

        var locationAddress = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_ADDRESSSTRING);
        if(locationAddress == null 
                || locationAddress.trim() == "null" 
                || locationAddress.trim() == "") {
            locationAddress = geoLoc_locationAddress_composeDefault(geoLocLocationCurrentObj.coords.latitude, 
                                                                    geoLocLocationCurrentObj.coords.longitude);
        }
        
        var userID = document.getElementById('userID').value;
        var tripID = document.getElementById('tripID').value;
        var placeToCreate = {
            userID: userID,
            tripID: tripID,
            location: locationAddress,
            latitude: geoLocLocationCurrentObj.coords.latitude,
            longitude: geoLocLocationCurrentObj.coords.longitude
        };

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                place_create_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_PLACE_CREATE, 
                                      xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                                      ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
            place_create_cancel();
        };
        var urlTarget = APP_ROOT_URI + "/trekcrumb/place/create/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(placeToCreate));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_PLACE_CREATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
            place_create_cancel();
        }
    }
}

function place_create_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return false;
    }
    
    place_create_cancel();
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(isSuccess) {
        //Find the new Place index:
        var numOfTotalPlacesPrevious = -1;
        var paginationPlaceLayout = document.getElementById('paginationPlaceLayoutID');
        if(paginationPlaceLayout != undefined && paginationPlaceLayout != null) {
            numOfTotalPlacesPrevious = parseInt(paginationPlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
        }
        
        //Redirect to result page:
        var username = document.getElementById('usernameID').value;
        var tripID = document.getElementById('tripID').value;;
        window.location.href = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID
                + "?" + QUERYPARAM_NAME_PLACE_INDEX + "=" + (numOfTotalPlacesPrevious + 1);
    }
}

function place_update_Init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    var placeUpdateLayout = document.getElementById('placeUpdateLayoutID');
    if(placeUpdateLayout == undefined || placeUpdateLayout == null) {
        //Invalid view:
        console.error("place_update_Init() - ERROR: Could not find placeUpdateLayoutID layout!");
        return;
    }
    var placePageLayout = document.getElementById('paginationPlaceLayoutID');
    if(placePageLayout == undefined || placePageLayout == null) {
        console.error("place_update_Init() - ERROR: Cannot find paginationPlaceLayoutID layout!");
        return;
    }
    var listOfPlacesString = document.getElementById('pagePlace_listOfPlacesID').value;
    if(listOfPlacesString == undefined 
            || listOfPlacesString == null 
            || listOfPlacesString == "") {
        console.log("place_update_Init() - ERROR: Cannot find the data list of Places at current page!");
        return;
    }
    
    var placeCurrentIndex = parseInt(placePageLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
    var numOfTotalPlaces = parseInt(placePageLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    if(placeCurrentIndex < 0 
        || placeCurrentIndex > (numOfTotalPlaces - 1)) {
        //Prevent invalid ArrayOutOfBound error:
        console.log("place_update_Init() - ERROR: Current selected Place index is out of bound: " + placeCurrentIndex);
        return;
    }

    //Populate form values:
    var listOfPlacesJSON = JSON.parse(listOfPlacesString);
    var place = listOfPlacesJSON[placeCurrentIndex];
    document.getElementById('placeID').value = place.id;
    document.getElementById('placeUpdateIndexID').innerHTML = (placeCurrentIndex +1);
    document.getElementById('placeUpdateLocationID').innerHTML = place.location;
    document.getElementById('placeUpdateNoteInputID').value = place.note;
    
    //Display:
    common_form_afterInit(FRAGMENT_PLACE_UPDATE, placeUpdateLayout);
}

function place_update_cancel() {
    var placeUpdateLayout = document.getElementById('placeUpdateLayoutID');
    if(placeUpdateLayout == undefined || placeUpdateLayout == null) {
        //Illegal access:
        return;
    }
    
    //Clean any leftover:
    common_errorOrInfoMessage_hide();
    document.getElementById(FRAGMENT_PLACE_UPDATE + "deleteCheckboxID").checked = false;
    common_deleteCheckbox_clicked(FRAGMENT_PLACE_UPDATE);
    placeUpdateLayout.style.display = "none";
}

function place_update_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    common_form_afterSubmit(FRAGMENT_PLACE_UPDATE);
    place_update_ServiceRequest();
}

function place_update_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var isDeleted = false;
        var placeToUpdate = {};
        placeToUpdate.id = document.getElementById('placeID').value;
        placeToUpdate.userID = document.getElementById('userID').value;
        placeToUpdate.tripID = document.getElementById('tripID').value;
        if(document.getElementById(FRAGMENT_PLACE_UPDATE + "deleteCheckboxID").checked) {
            isDeleted = true;
        } else {
            placeToUpdate.note = document.getElementById('placeUpdateNoteInputID').value;
        }

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                place_update_ServiceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_PLACE_UPDATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = null;
        if(isDeleted) {
            urlTarget = APP_ROOT_URI + "/trekcrumb/place/delete/submit/AJAX";
        } else {
            urlTarget = APP_ROOT_URI + "/trekcrumb/place/update/submit/AJAX/";
        }
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(placeToUpdate));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_PLACE_UPDATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function place_update_ServiceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var placeUpdateLayout = document.getElementById("placeUpdateLayoutID");
    if(placeUpdateLayout == undefined || placeUpdateLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_PLACE_UPDATE, xmlhttpRequest);
    if(isSuccess) {
        var username = document.getElementById('usernameID').value;
        var tripID = document.getElementById('tripID').value;
        var successTarget = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID;
        if(document.getElementById(FRAGMENT_PLACE_UPDATE + "deleteCheckboxID").checked == false) {
            var placeIndex = parseInt(document.getElementById('placeUpdateIndexID').innerHTML) -1;
            successTarget = successTarget + "?" + QUERYPARAM_NAME_PLACE_INDEX + "=" + placeIndex;
        }
        window.location.href = successTarget;
    }
}

function place_mapCurrentLocation_Init() {
    geoLoc_currentLocation_Init(FRAGMENT_PLACE_DETAIL);
}

function place_mapCurrentLocation_Found() {
    var geoLocCurrentLocationLayout = document.getElementById('geoLocCurrentLocationLayoutID');
    if(geoLocCurrentLocationLayout == undefined || geoLocCurrentLocationLayout == null) {
        //Invalid:
        return;
    }
    var geoLocationCurrentString = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_CURRENT);
    if(geoLocationCurrentString == null 
            || geoLocationCurrentString.trim() == "null"
            || geoLocationCurrentString.trim() == "") {
        common_errorOrInfoMessage_show([ERROR_GEOLOC_CURRENTLOCATION_EMPTY], null);
        geoLocCurrentLocationLayout.style.display = "none";
        return;
    }
    
    //Else:
    geoLocCurrentLocationLayout.style.display = "none";    
    var geoLocLocationCurrentObj = JSON.parse(geoLocationCurrentString);
    map_currentLocation_displayContent(
            geoLocLocationCurrentObj.coords.latitude, 
            geoLocLocationCurrentObj.coords.longitude);
}

function place_notes_clicked(placeIndex) {
    //Adjust PaginationPlace variable:
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    pagePlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, placeIndex);
    pagePlace_updateNavigationMenu(placeIndex, null);

    //Update PlaceDetail content:
    place_detail_DisplayContent();
    map_trigger_markerClickedEvent(placeIndex);
    
    //Show PlaceDetail (if hidden in potrait/small/medium screen):
    var parentLayoutWidth = document.getElementById("tripViewLayoutID").offsetWidth;
    if(mDevice_orientation == DEVICE_ORENTATION_POTRAIT
            || parentLayoutWidth <= DEVICE_SCREEN_WIDTH_MAX_MEDIUM) {
        trip_view_DisplaySelectedTab(FRAGMENT_PLACE_DETAIL);     
    }
}

/*
 * Home PlaceList Gallery to display the given list of Trips' locations into the place list fragment.
 * -> BY default, we expect to have and display the latest 20 Trips data created
 * -> For the value to display, it is Trip's location BUT we only display 
 *    the first name (before comma, if any).
 * -> If the location does not have any geo-name, but its (lat,lan), then do not display!
 */
function place_home_placeListAsTagDisplayContent(listOfTrips) {
    var placeListContent = document.getElementById("homePlaceGalleryListContentID");
    var placeRowTemplate = document.getElementById("placeRowAsTagTemplateID");
    if(placeListContent == undefined || placeListContent == null
            || placeRowTemplate == undefined || placeRowTemplate == null) {
        console.error("place_home_placeListAsTagDisplayContent() - ERROR: Required elements NOT found in current view!");
        return;
    }

    if(listOfTrips == null) {
        if(mDevice_isSupportLocaleStorage) {
            //Caching: Get the last result List if available
            var listOfTripsString = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + "0");
            listOfTrips = JSON.parse(listOfTripsString);
        }    
    }
    
    common_progressBarWidget_hide(FRAGMENT_PLACE_LIST_HOME);
    if(listOfTrips == undefined || listOfTrips == null) {
        placeListContent.innerHTML = ERROR_NO_DATA;
        return;
    }

    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    placeListContent.innerHTML = "";
    var listSize = listOfTrips.length;
    for(var index = 0; index < listSize ; index++) {
        var trip = listOfTrips[index];
        
        //Check if Trip's location is valid to display:
        var location = trip.location;
        if(!geoLoc_locationAddress_isHasAddressValue(location)) {
            //Skip it:
            continue;
        }

        //Clone row template and populate target data:
        var rowCurrent = placeRowTemplate.cloneNode(true);
        rowCurrent.id = trip.id;
        
        //URL link:
        var targetURL = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + trip.username + "/" + trip.id;
        rowCurrent.setAttribute("onclick", "menu_openNewTab('" + targetURL + "')");
        
        //Location: Display only the first name (before comma, if any)
        rowCurrent.querySelector("#locationID").innerHTML = geoLoc_locationAddress_getFirstAddressValue(location);
        
        //Put fancy styling for fun randomly:
        if(index == 1 || index == 7 || index == 12 || index == 17) {
            rowCurrent.querySelector("#placeRowAsTagContentID").className += " font-size-large font-bold";
        } else if(index == 2 || index == 6 || index == 8 || index == 13 || index == 18) {
            rowCurrent.querySelector("#placeRowAsTagContentID").className += " font-size-small font-bold";
        } else if(index == 4 || index == 10 || index == 14) {
            rowCurrent.querySelector("#placeRowAsTagContentID").className += " font-italic";
        } else if(index == 9) {
            rowCurrent.querySelector("#placeRowAsTagContentID").className += " font-size-xlarge";
        }
        
        //Finally, display it:
        rowCurrent.style.display = 'inline-block';
        placeListContent.appendChild(rowCurrent);

    }    
}


//END trekcrumb-place.js