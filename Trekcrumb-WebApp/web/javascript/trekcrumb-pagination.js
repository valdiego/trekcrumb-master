"use strict";

function pageList_init(pageName) {
    pageList_update(pageName, 0, 1, -1);
}

function pageList_update(pageName, offsetCurrent, pageCurrent, numRecordsTotal) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    var pageListLayout = document.getElementById(pageName + 'paginationListLayoutID');
    if(pageListLayout == undefined || pageListLayout == null) {
        return;
    }
    
    //Update pageList variables:
    var numRowsMax = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_NUMBER_ROWS_MAXIMUM));
    pageListLayout.setAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT, offsetCurrent);
    pageListLayout.setAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT, pageCurrent);
    
    if(numRecordsTotal >= 0) {
        pageListLayout.setAttribute(VARATTR_PAGE_LIST_NUMBER_RECORDS_TOTAL, numRecordsTotal);
    } else {
        numRecordsTotal = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_NUMBER_RECORDS_TOTAL));
    }

    //Page number:
    document.getElementById(pageName + 'pageNumberID').innerHTML = "Page " + pageCurrent;
    
    //Previous menu:
    if(pageCurrent == 0 || pageCurrent == 1) {
        document.getElementById(pageName + 'MenuGoPreviousID').style.display = 'none';
    } else {
        document.getElementById(pageName + 'MenuGoPreviousID').style.display = 'inline';
    }
    
    //Next menu:
    if((offsetCurrent + numRowsMax) >= numRecordsTotal) {
        document.getElementById(pageName + 'MenuGoNextID').style.display = 'none';
    } else {
        document.getElementById(pageName + 'MenuGoNextID').style.display = 'inline';
    }
}

function pageList_next(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    var pageListLayout = document.getElementById(pageName + 'paginationListLayoutID');
    if(pageListLayout == undefined || pageListLayout == null) {
        return;
    }

    //Get current states:
    var numRowsMax = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_NUMBER_ROWS_MAXIMUM));
    var offsetCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT));
    var pageCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT));
    var numRecordsTotal = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_NUMBER_RECORDS_TOTAL));
    if((offsetCurrent + numRowsMax) >= numRecordsTotal) {
        return false;
    }

    //Update:
    var offsetNext = offsetCurrent + numRowsMax;
    var pageNext = pageCurrent + 1;
    pageList_update(pageName, offsetNext, pageNext, -1);
    return true;
}

function pageList_previous(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    var pageListLayout = document.getElementById(pageName + 'paginationListLayoutID');
    if(pageListLayout == undefined || pageListLayout == null) {
        return;
    }

    //Get current states:
    var numRowsMax = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_NUMBER_ROWS_MAXIMUM));
    var offsetCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT));
    var pageCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT));
    if(pageCurrent  == 1
        || offsetCurrent == 0
        || offsetCurrent < numRowsMax) {
        return false;
    }
    
    //Update:
    var offsetPrevious = offsetCurrent - numRowsMax;
    var pagePrevious = pageCurrent - 1;
    pageList_update(pageName, offsetPrevious, pagePrevious, -1);
    return true;
}

/*
 * Initiates Pagination Picture local variables such as picture index to 0, number of
 * total pictures on the selected Trip, etc. This mandatory step in order for 
 * picture navigation to work.
 */
function pagePic_init(tripID, listOfPicturesJSON) {
    //Set pagination page's variables:
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        return;
    }

    var numOfTotalPictures = 0;
    if(listOfPicturesJSON != null) {
        document.getElementById('pagePic_listOfPicturesID').value = JSON.stringify(listOfPicturesJSON);
        numOfTotalPictures = listOfPicturesJSON.length;
    } else {
        document.getElementById('pagePic_listOfPicturesID').value = null;
    }

    pagePicLayout.setAttribute(VARATTR_TRIP_ID, tripID);
    pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL, numOfTotalPictures);
    pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, 0);
}

/*
 * Updates navigation menus (previous, next, number of pages) based on current pagination variable values.
 * This function should handle variant picture fragments such as Picture Detail or Picture Slideshow that
 * has different IDs for the menus to be looked for and updated.
 * Param:
 * - pictureFragmentPage: The name of the picture fragment as defined in trekcrumb_constants.js file.
 */
function pagePic_updateNavigationMenu(pictureFragmentPage, pictureCurrentIndex, numOfTotalPictures) {
    if(pictureFragmentPage == undefined || pictureFragmentPage == null) {
        pictureFragmentPage = "";
    }
    if(pictureCurrentIndex == undefined || pictureCurrentIndex == null 
            || numOfTotalPictures == undefined || numOfTotalPictures == null) {
        var pagePicLayout = document.getElementById('paginationPictureLayoutID');
        numOfTotalPictures = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
        pictureCurrentIndex = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT));
    }
     
    //Previous menu:
    var menuGoPrevious = document.getElementById(pictureFragmentPage + "MenuGoPreviousID");
    if(menuGoPrevious != undefined && menuGoPrevious != null) {
        if(pictureCurrentIndex <= 0) {
            menuGoPrevious.style.display = 'none';
        } else {
            menuGoPrevious.style.display = 'inline-block';
        }
    }
    
    //Next menu:
    var menuGoNext = document.getElementById(pictureFragmentPage + "MenuGoNextID");
    if(menuGoNext != undefined && menuGoNext != null) {
        if((pictureCurrentIndex + 1) >= numOfTotalPictures) {
            menuGoNext.style.display = 'none';
        } else {
            menuGoNext.style.display = 'inline-block';
        }
    }
    
    //Page count:
    var pageCountElement = document.getElementById(pictureFragmentPage + "PageCountID");
    if(pageCountElement != undefined && pageCountElement != null) {
        pageCountElement.innerHTML = (pictureCurrentIndex + 1) + " of " + numOfTotalPictures;
    }
}

/*
 * Pagination Picture navigates to next picture item. If next item is available, it returns 'TRUE' and 
 * updates the pagination variables and navigation menus. Otherwise, it returns 'FALSE'.
 * Param:
 * - pictureFragmentPage: The name of the picture fragment as defined in trekcrumb_constants.js file.
 */
function pagePic_next(pictureFragmentPage) {
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        return;
    }

    var numOfTotalPics = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
    var picCurrentIndex = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT));
    if(picCurrentIndex >= numOfTotalPics) {
        return false;
    }
    var picNextIndex = picCurrentIndex + 1;
    pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, picNextIndex);
    pagePic_updateNavigationMenu(pictureFragmentPage, picNextIndex, numOfTotalPics);
    return true;
}

/*
 * Pagination Picture navigates to previous picture item. If previous item is available, it returns 'TRUE' and 
 * updates the pagination variables and navigation menus. Otherwise, it returns 'FALSE'.
 * Param:
 * - pictureFragmentPage: The name of the picture fragment as defined in trekcrumb_constants.js file.
 */
function pagePic_previous(pictureFragmentPage) {
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        return;
    }

    var numOfTotalPics = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
    var picCurrentIndex = parseInt(pagePicLayout.getAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT));
    if(picCurrentIndex  == 0) {
        return false;
    }
    var picPreviousIndex = picCurrentIndex - 1;
    pagePicLayout.setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, picPreviousIndex);
    pagePic_updateNavigationMenu(pictureFragmentPage, picPreviousIndex, numOfTotalPics);
    return true;
}

/*
 * Initiates Pagination Place local variables such as place index to 0, number of
 * total places on the selected Trip, etc. This mandatory step in order for 
 * place navigation on map to work.
 */
function pagePlace_init(listOfPlacesJSON) {
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    if(pagePlaceLayout == undefined || pagePlaceLayout == null) {
        return;
    }
    
    //Set pagination variables:
    var numOfTotalPlaces;
    if(listOfPlacesJSON != null) {
        document.getElementById('pagePlace_listOfPlacesID').value = JSON.stringify(listOfPlacesJSON);
        numOfTotalPlaces = listOfPlacesJSON.length;
    } else {
        document.getElementById('pagePlace_listOfPlacesID').value = null;
        numOfTotalPlaces = 0;
    }
    
    pagePlaceLayout.setAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL, numOfTotalPlaces);
    pagePlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, 0);
    pagePlace_updateNavigationMenu(0, numOfTotalPlaces);
}

/*
 * Pagination Place navigates to next place item. If next item is available, it returns 'TRUE' and 
 * updates the pagination variables and navigation menus. Otherwise, it returns 'FALSE'.
 */
function pagePlace_next() {
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    var numOfTotalPlaces = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    var placeCurrentIndex = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
    if((placeCurrentIndex+1) >= numOfTotalPlaces) {
        return false;
    }
    
    var placeNextIndex = placeCurrentIndex + 1;
    pagePlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, placeNextIndex);
    pagePlace_updateNavigationMenu(placeNextIndex, numOfTotalPlaces);
    return true;
}

/*
 * Pagination Place navigates to previous place item. If previous item is available, it returns 'TRUE' and 
 * updates the pagination variables and navigation menus. Otherwise, it returns 'FALSE'.
 */
function pagePlace_previous() {
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    var numOfTotalPlaces = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    var placeCurrentIndex = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
    if(placeCurrentIndex == 0) {
        return false;
    }

    var placePreviousIndex = placeCurrentIndex - 1;
    pagePlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, placePreviousIndex);
    pagePlace_updateNavigationMenu(placePreviousIndex, numOfTotalPlaces);
    return true;
}

/*
 * Updates navigation menus (previous, next, number of pages) based on current pagination variable values.
 */
function pagePlace_updateNavigationMenu(currentIndex, numOfTotalItems) {
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    if(currentIndex == undefined || currentIndex == null) {
        currentIndex = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
    }
    if(numOfTotalItems == undefined || numOfTotalItems == null) {
        numOfTotalItems = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    }
     
    //Previous menu:
    var menuGoPrevious = document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuGoPreviousID");
    if(menuGoPrevious != undefined && menuGoPrevious != null) {
        if(currentIndex == 0) {
            menuGoPrevious.style.display = 'none';
        } else {
            menuGoPrevious.style.display = 'inline-block';
        }
    }
    
    //Next menu:
    var menuGoNext = document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuGoNextID");
    if(menuGoNext != undefined && menuGoNext != null) {
        if((currentIndex + 1) >= numOfTotalItems) {
            menuGoNext.style.display = 'none';
        } else {
            menuGoNext.style.display = 'inline-block';
        }
    }
    
    //Page count:
    if(numOfTotalItems > 0) {
        var pageCountElement = document.getElementById("PlaceDetailPageCountID");
        if(pageCountElement != undefined && pageCountElement != null) {
            pageCountElement.innerHTML = (currentIndex + 1) + " of " + numOfTotalItems;
        }
    }
}


//END trekcrumb-pagination.js