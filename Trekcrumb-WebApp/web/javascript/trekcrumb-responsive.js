"use strict";

/*
 * Base layout(s) Responsiveness
 * 1. Any layout that most pages share in common, such as re-evaluating
 *    window (display) new orientation whenever there is change on window size. Typically, we need
 *    to invoke this first before making adjustment to other layout/elements.
 */
function responsive_adjustBaseLayouts() {
    common_window_evaluateOrientation();
}

/*
 * TripSearch Responsiveness
 * 1. TripSearchMenuPageContent (search menu): See responsive_adjustMenuPageLayout()
 */
function responsive_adjustTripSearchLayout() {
    var tripSearchLayout = document.getElementById("searchLayoutID");
    if(tripSearchLayout != "undefined" && tripSearchLayout != null) {
        responsive_adjustBaseLayouts();
        
        var parentWidth = tripSearchLayout.offsetWidth;
        var parentHeight = tripSearchLayout.offsetHeight;
        
        //MenuPage:
        responsive_adjustMenuPageLayout("TripSearch", parentWidth, parentHeight, true);
    }
}

function responsive_adjustTripViewLayout() {
    var tripViewLayoutElement = document.getElementById("tripViewLayoutID");
    if(tripViewLayoutElement == "undefined" || tripViewLayoutElement == null) {
        return;
    }
    
    responsive_adjustBaseLayouts();
    responsive_adjustPictureGalleryLayout();
    responsive_adjustPictureSlideshowLayout();
    responsive_adjustPictureDetailLayout();
    responsive_adjustPlaceDetailLayout();
}

/*
 * PictureGallery Responsiveness
 * 1. Its CSS handles some of its dynamic styling, when possible.
 * 2. This element is more complicated since we re-adjust not only dimensions, but also
 *    the number of gallery items, their size, and how many to show/hide based on its parent container width.
 * 3. Navigation Menu: 
 *    -> With its style having borders (body-inside-border), it adds complexity in 
 *       calculating its height/line-height to make sure menu-icons (size 48px) are centered while
 *       taking into account the borders (1px each side)
 */
function responsive_adjustPictureGalleryLayout() {
    var picGalleryLayout = document.getElementById("pictureGalleryLayoutID");
    if(picGalleryLayout == undefined 
        || picGalleryLayout == null
        || picGalleryLayout.style.display == "none"
        || picGalleryLayout.parentElement.style.display == "none") {
        return;
    }
    
    //1. Get contents:
    var picGalleryContents = document.getElementsByClassName("layout-picture-gallery-content");
    if(picGalleryContents == null) {
        return;
    }
    
    //2. Calculate its variables (total items, number of items to show, etc.):
    var layoutTotalSizeAvailable = picGalleryLayout.offsetWidth - 100; //100 is width of two "previous/next" menu icons plus borders
    var picThumbnailSize = picGalleryContents[0].offsetWidth;
    var numberContentsToShowTotal = (layoutTotalSizeAvailable / picThumbnailSize >> 0);
    document.getElementById("picGallery_picThumbnailSizeID").value = picThumbnailSize;
    document.getElementById("picGallery_layoutTotalSizeAvailableID").value = layoutTotalSizeAvailable;
    document.getElementById("picGallery_NumberContentsToShowTotalID").value = numberContentsToShowTotal; 
    
    //3. Adjust Navigation Menus:
    var menuPreviousLayout = document.getElementById(FRAGMENT_PICTURE_GALLERY + "MenuGoPreviousID");
    menuPreviousLayout.style.height = "" + picThumbnailSize + "px";
    menuPreviousLayout.style.lineHeight = "" + picThumbnailSize + "px";
    var menuNextLayout = document.getElementById(FRAGMENT_PICTURE_GALLERY + "MenuGoNextID");
    menuNextLayout.style.height = "" + picThumbnailSize + "px";
    menuNextLayout.style.lineHeight = "" + picThumbnailSize + "px";
    
    //4. Since everything changed, reset the contents to show to starting index 0:
    document.getElementById("picGallery_ContentShownFirstRowIndexID").value = 0;
    
    //5. Decide which content to show or to hide:
    picture_gallery_DisplayOrHideContentRows();
}

/*
 * PictureSlideshow Responsiveness
 * 1. Its CSS handles some of its dynamic styling, when possible.
 *    (See #pictureSlideshowLayoutID and its related elements' stylings)
 * 2. pictureSlideshowImgContentContainerID
 *    -> In order to position the image vertically centered, we must set its image content's line-height
 *       the same as its height where the height is dynamicaly calculated based on width.
 * 3. Image element 
 *    -> It must be adjusted as well based on image orientation AFTER the img is downloaded. 
 *       See responsive_adjustPictureImageElement().
 *    
 * 4. WARNING: If parent layout's current style display is 'none', its offsetwidth/height will return ZERO. 
 *    This may cause error in responsiveness calculations.
 */
function responsive_adjustPictureSlideshowLayout() {
    var pictureSlideshowLayout = document.getElementById("pictureSlideshowLayoutID");
    if(pictureSlideshowLayout == undefined 
        || pictureSlideshowLayout  == null
        || pictureSlideshowLayout.style.display == "none"
        || pictureSlideshowLayout.parentElement.style.display == "none") {
        return;
    }
    
    var pictureSlideshowImgContentContainer = document.getElementById("pictureSlideshowImgContentContainerID");
    var height = pictureSlideshowImgContentContainer.offsetHeight;
    pictureSlideshowImgContentContainer.style.lineHeight = "" + height + "px";
    
    //Image content:
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        console.log("responsive_adjustPictureSlideshowLayout() - WARNING: Required element [paginationPictureLayoutID] not found. Perhaps NO picture to display.");
        return;
    }
    var tripID = pagePicLayout.getAttribute(VARATTR_TRIP_ID);
    responsive_adjustPictureImageElement(FRAGMENT_PICTURE_SLIDESHOW + "pictureImageID" + tripID);
}

/*
 * PictureDetail Responsiveness
 * 1. Its CSS handles some of its dynamic styling, when possible.
 *    (See #pictureDetailLayoutID and its related elements' stylings)
 * 2. pictureDetailLayout (main container)
 *    -> This layout may be hidden by default, therefore we should extract its parent's offset height/width
 *       for calculation. (If an element display is 'NONE', its dimension would be ZERO).
 *    -> Since pictureDetailLayout lays on <BODY> layout to cover entire screen, its parent height
 *       would be ZERO px. To solve this, we assume its own width/height as the dimensions.
 * 3. pictureDetailImageContent (image container):
 *    -> Line-Height: Adjust its value so that the image would fit at enter of its container.
 * 4. PictureDetailMenuPageContent: See responsive_adjustMenuPageLayout()
 * 5. Image element: 
 *    -> It must be adjusted as well based on image orientation AFTER the img is downloaded. 
 *       See responsive_adjustPictureImageElement().
 */
function responsive_adjustPictureDetailLayout() {
    var pictureDetailLayout = document.getElementById("pictureDetailLayoutID");
    if(pictureDetailLayout == undefined 
        || pictureDetailLayout == null
        || pictureDetailLayout.style.display == "none"
        || pictureDetailLayout.parentElement.style.display == "none") {
        return;
    }

    //Take its own dimension as 'parent' dimension:
    var parentLayoutWidth = pictureDetailLayout.offsetWidth;
    var parentLayoutHeight = pictureDetailLayout.offsetHeight;

    //Image container:
    var pictureDetailImageContent = document.getElementById("pictureDetailImgContentID");
    pictureDetailImageContent.style.lineHeight = "" + parentLayoutHeight + "px";

    //MenuPage:
    responsive_adjustMenuPageLayout(FRAGMENT_PICTURE_DETAIL, parentLayoutWidth, parentLayoutHeight, false);

    //Image content:
    var pagePicLayout = document.getElementById('paginationPictureLayoutID');
    if(pagePicLayout == undefined || pagePicLayout == null) {
        console.log("responsive_adjustPictureDetailLayout() - WARNING: Required element [paginationPictureLayoutID] not found. Perhaps NO picture to display.");
        return;
    }
    var tripID = pagePicLayout.getAttribute(VARATTR_TRIP_ID);
    responsive_adjustPictureImageElement(FRAGMENT_PICTURE_DETAIL + "pictureImageID" + tripID);
}

/* 
 * Image Element Responsiveness
 * 1. Adjust image width/height whether one should fill the container area (set value to "100%") 
 *    or not ("auto") based on the image orientation (potrait or landscape). 
 *    -> If image is square, we must put consideration of the container orientation while 
 *       adjusting image dimension.
 * 2. WARNING: 
 *    -> In order to get image's offset dimension, its display value must be visible (not 'NONE').
 *    -> 8/2016: Known issue with this algorithm:
 *       --> If image is potrait BUT wider, it would be out of space (cut-off) if device orientation is potrait (too narrow) 
 *       --> If image is landscape BUT higher, it would be out of space (cut-off) if device orientation is landscape (too narrow)
 *       
 *       Only known solution is to make the image's container layout to be SQUARE (like that of PictureSlideshow) 
 */
function responsive_adjustPictureImageElement(imageElementID) {
    var imageElement = document.getElementById(imageElementID);
    if(imageElement == undefined || imageElement == null) {
        console.warn("responsive_adjustPictureImageElement() - WARNING: Image Element NOT FOUND: " + imageElementID);
        return; 
    }
    if(imageElement.getAttribute("src") == null || imageElement.getAttribute("src") == "") {
        return;
    }
    
    //Get image original dimensions:
    var imgWidthOrig = imageElement.offsetWidth;
    var imgHeightOrig = imageElement.offsetHeight;
    if(imgWidthOrig < imgHeightOrig) {
        //Potrait image
        imageElement.style.height = "100%"; 
        imageElement.style.width = "auto"; 
        
    } else if(imgWidthOrig > imgHeightOrig) {
        //Landscape image
        imageElement.style.height = "auto"; 
        imageElement.style.width = "100%"; 

    } else {
        //Square image - Get image's parent container dimension:
        var parentLayoutWidth = imageElement.parentElement.offsetWidth;
        var parentLayoutHeight = imageElement.parentElement.offsetHeight;
        var isParentLayoutPotrait = (parentLayoutWidth <= parentLayoutHeight ) ? true:false;
        if(isParentLayoutPotrait) {
            imageElement.style.height = "auto"; 
            imageElement.style.width = "100%"; 
        } else {
            imageElement.style.height = "100%"; 
            imageElement.style.width = "auto"; 
        }
    }
}
 
/*
 * PlaceDetail Responsiveness
 * 1. Fragment layouts: 
 *    -> Width/Height: Fragment widths adjustment is made via CSS styling.
 *    -> If device orientation is POTRAIT: By default, the placeDetailLayout will fill entire parent container's width
 *       but will not be displayed. If it is LANDSCAPE but small/medium screen: The same requirement as potrait. 
 *       Otherwise, this layout by default will have limited (smaller) dimension but is displayed ONLY IF TripDetail
 *       page is displayed. However, if fragment on display is FRAGMENT_PLACE_DETAIL, regardless
 *       device orientation, it should be displayed in FULL filling entire parent container.
 * 3. MapCanvas layout: 
 *    -> The area inside placeDetailLayout where the map API will be painted over. 
 *    -> It must have its width/height dimensions specified in order to display the map API. Otherwise it would be hidden 
 *       (height is 0). Its dimension is calculated based on its container's dimensions.
 *    -> We, unfortunately, must use parent layout (tripViewLayoutID) offsetWidth/Height, not its container (placeDetailLayoutID)'s.
 *       Using placeDetailLayoutID, the offsetWidth result will be inconsistent from using menu-icon switch back and forth
 *       because we change its width dynamically (from 50% to 100%, etc.) and somehow the JS picked up the last value, not the current value.
 *    -> Includes its container layout (placeDetailLayoutID) paddings in width/height calculations
 *    -> Strangely, if map API (such as Google) throws exception (such as apiKey expired, etc.), it could wipe out the entire
 *       layout DIV, thus mapCanvasID could be NULL.
 */
function responsive_adjustPlaceDetailLayout() {
    var placeDetailLayout = document.getElementById("placeDetailLayoutID");
    if(placeDetailLayout == undefined || placeDetailLayout == null) {
        return;
    }

    var parentElement = document.getElementById("tripViewLayoutID");
    var parentLayoutWidth = parentElement.offsetWidth;
    var parentLayoutHeight = parentElement.offsetHeight;
    var parentFragmentOnDisplayCurrent = parentElement.getAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY);
    if(mDevice_orientation == DEVICE_ORENTATION_POTRAIT
            || parentLayoutWidth <= DEVICE_SCREEN_WIDTH_MAX_MEDIUM) {
        //Potrait or small/medium screen:
        if(parentFragmentOnDisplayCurrent != FRAGMENT_PLACE_DETAIL) {
            placeDetailLayout.style.display = "none";
            return;
        }
    } else {
        //Landscape and large screen: 
        if(parentFragmentOnDisplayCurrent != FRAGMENT_PLACE_DETAIL
                && parentFragmentOnDisplayCurrent != FRAGMENT_TRIP_DETAIL) {
            placeDetailLayout.style.display = "none";
            return;
        }
    }

    //Else: PlaceDetail is on display, let's adjust the layout:
    var mapCanvas = document.getElementById("mapCanvasID");
    var menuClose = document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuCloseID");
    var mapWidth;
    var mapHeight;
    var placeDetailPadding;
    var placeDetailPaddingStr;
    if(mDevice_orientation == DEVICE_ORENTATION_POTRAIT
            || parentLayoutWidth <= DEVICE_SCREEN_WIDTH_MAX_MEDIUM) {
        //Potrait or small/medium screen:
        if(parentFragmentOnDisplayCurrent == FRAGMENT_PLACE_DETAIL) {
            placeDetailLayout.style.display = "inline-block";

            placeDetailPadding = 5;
            placeDetailPaddingStr = "" + placeDetailPadding + "px";
            placeDetailLayout.style.padding = placeDetailPaddingStr;
            placeDetailLayout.style.width = "100%";
            menuClose.style.display = "block";
            mapWidth = parentLayoutWidth - (placeDetailPadding * 2);
            mapHeight = parentLayoutHeight - (placeDetailPadding * 2);
            if(mapCanvas != undefined && mapCanvas != null) {
                mapCanvas.style.width = "" + mapWidth + "px";
                mapCanvas.style.height = "" + mapHeight + "px";
            }
            
        } else {
            console.error("responsive_adjustPlaceDetailLayout() - ERROR: PlaceDetail layout should have not been displayed/adjusted for current 'fragment on display': " + parentFragmentOnDisplayCurrent);
            return;
        }
        
    } else {
        //Landscape and large screen: 
        placeDetailLayout.style.display = "inline-block";
        
        if(parentFragmentOnDisplayCurrent == FRAGMENT_TRIP_DETAIL) {
            //Part of screen: 
            placeDetailPadding = 20;
            placeDetailPaddingStr = "" + placeDetailPadding + "px";
            placeDetailLayout.style.padding = placeDetailPaddingStr;
            placeDetailLayout.style.width = "50%";
            menuClose.style.display = "none";
            mapWidth = (parentLayoutWidth/2) - (placeDetailPadding * 2);
            mapHeight = parentLayoutHeight - (placeDetailPadding * 2);
            if(mapCanvas != undefined && mapCanvas != null) {
                mapCanvas.style.width = "" + mapWidth + "px";
                mapCanvas.style.height = "" + mapHeight + "px";
            }
        
        } else if(parentFragmentOnDisplayCurrent == FRAGMENT_PLACE_DETAIL) {
            //Full screen:
            placeDetailPadding = 5;
            placeDetailPaddingStr = "" + placeDetailPadding + "px";
            placeDetailLayout.style.padding = placeDetailPaddingStr;
            placeDetailLayout.style.width = "100%";
            menuClose.style.display = "block";
            mapWidth = parentLayoutWidth - (placeDetailPadding * 2);
            mapHeight = parentLayoutHeight - (placeDetailPadding * 2);
            if(mapCanvas != undefined && mapCanvas != null) {
                mapCanvas.style.width = "" + mapWidth + "px";
                mapCanvas.style.height = "" + mapHeight + "px";
            }
        } else {
            console.error("responsive_adjustPlaceDetailLayout() - ERROR: PlaceDetail layut should have not been displayed nor adjusted for current 'fragment on display': " + parentFragmentOnDisplayCurrent);
            return;
        }     
    }
    
    //MenuPage:
    responsive_adjustMenuPageLayout(FRAGMENT_PLACE_DETAIL, mapWidth, mapHeight, false);
    var PlaceDetailMenuPageMainLayout = document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuPageMainLayoutID");
    if(PlaceDetailMenuPageMainLayout != undefined && PlaceDetailMenuPageMainLayout != null) {
        PlaceDetailMenuPageMainLayout.style.bottom = placeDetailPaddingStr;
    }

    //Menu Navigations:
    document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuGoPreviousID").style.left = placeDetailPaddingStr;
    document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuGoNextID").style.right = placeDetailPaddingStr;
    
    //DEBUG ONLY:
    /*
    console.log("responsive_adjustPlaceDetailLayout() - Completed."
        + " Parent (tripViewLayoutID) offset [width,height] = [" + parentLayoutWidth + "," + parentLayoutHeight + "]"
        + " PlaceDetailLayout offset [width,height] = [" + placeDetailLayout.offsetWidth + "," + placeDetailLayout.offsetHeight + "]"
        + " MapCanvas offset [width,height] = [" + mapCanvas.offsetWidth + "," + mapCanvas.offsetHeight  + "]"
        + " MapCanvas CSS [width,height] = [" + mapCanvas.style.width + "," + mapCanvas.style.height + "]");
    */
}

/*
 * UserView Responsiveness
 * 1. Fragment layouts: 
 *    -> Width/Height: Fragment widths adjustment is made via CSS styling.
 * 2. What to display:
 *    -> Depending on current VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY value, it will
 *       recall user_view_DisplaySelectedTab() to make sure the current fragment on
 *       display is maintained during window resize or orientation changes.
 */
function responsive_adjustUserViewLayout() {
    var userViewLayout = document.getElementById("userViewLayoutID");
    if(userViewLayout != "undefined" && userViewLayout != null) {
        responsive_adjustBaseLayouts();
        responsive_adjustUserFragmentsLayout(FRAGMENT_TRIP_LIST_USER_TRIPS, "userTripsLayoutID");
        responsive_adjustUserFragmentsLayout(FRAGMENT_TRIP_LIST_USER_FAVORITES, "userFavoritesLayoutID");
        responsive_adjustUserFragmentsLayout(FRAGMENT_PICTURE_LIST_USER_PICTURES, "userPicturesLayoutID");
        responsive_adjustUserFragmentsLayout(FRAGMENT_COMMENT_LIST_USER_COMMENTS, "userCommentsLayoutID");
    }
}

/*
 * UserView Fragments (UserTrips, UserPictures, etc.) Responsiveness
 * 1. Navigation menus:
 *    -> Readjust position where the next/previous menus would stay on center of 
 *       fragment's layout height.
 *    -> Take into account the icon size is 48px, so in order to be perfectly center
 *       we need to deduct the position by 48/2 = 24px.
 *    -> This is more challenging since the layout may occupy the screen 100% on small
 *       screen, or only 50% on the right side on large/landscape screen. 
 *       Therefore, for 'Previous' menu, we need to also set its left position.
 *       Note that we like a "5px padding" from the left side.
 * 2. WARNING:
 *    -> Be aware some of fragment pages may be display = "NONE", causing 
 *       getting their offsetHeight/Width to be zero. Therefore, always attempt to get
 *       their parent's dimension in any calculation (parent/main layout is never hidden).
 */
function responsive_adjustUserFragmentsLayout(pageName, fragmentLayoutID) {
    var fragmentLayout = document.getElementById(fragmentLayoutID);
    if(fragmentLayout == undefined || fragmentLayout == null) {
        console.error("responsive_adjustUserFragmentsLayout() - Fragment layout NOT FOUND: " + fragmentLayoutID);
        return;
    }
    if(fragmentLayout.style.display == "none") {
        return;
    }
    
    var parentWidth = fragmentLayout.parentElement.offsetWidth;

    //Menu Navigations:
    var menuPreviousLayout = document.getElementById(pageName + "MenuGoPreviousID");
    if(parentWidth >= DEVICE_SCREEN_WIDTH_MAX_MEDIUM) {
        menuPreviousLayout.style.left = "" + (parentWidth - (parentWidth/2) + 5) + "px";
    } else {
        menuPreviousLayout.style.left = "5px";
    }        
}

/*
 * Responsiveness of menuPageContent layout:
 * -> Width: Adjust where its maximum width for medium/large screen is 600px. 
 *           Otherwise, it would fill the entire screen width minus nice-to-look-at paddings 
 *           (20px each left/right) and the menu icon size (50px) so they stay inline.
 * -> Height: Adjust its maximum height is 1/2 of the parent's height such that we avoid it to
 *            overflow outside view for a narrow height screen (landscape, etc.) because its contents. 
 *            The layout must be configured to be scrollable.
 */
function responsive_adjustMenuPageLayout(pageName, parentWidth, parentHeight, isFullHeight) {
    var menuPageContentElementID;
    if(pageName != undefined && pageName != null) {
        menuPageContentElementID = pageName + "MenuPageContentID";
    } else {
        menuPageContentElementID = "MenuPageContentID";
    }
    
    var menuPageContentElement= document.getElementById(menuPageContentElementID);
    if(menuPageContentElement == undefined || menuPageContentElement == null) {
        return;
    }
    
    if(mDevice_orientation == DEVICE_ORENTATION_POTRAIT) {
        //Potrait: Fill entire parent's width
        var width = parentWidth - 40;
        menuPageContentElement.style.width = "" + (width - 50) + "px";
    } else {
        //Landscape:
        if(parentWidth <= DEVICE_SCREEN_WIDTH_MAX_SMALL) {
            //Small screen: Fill entire parent's width
            var width = parentWidth - 40;
            menuPageContentElement.style.width = "" + (width - 50) + "px";
        } else {
            //Large screen: Limit the size
            menuPageContentElement.style.width = "600px";
        }
    }
    
    if(isFullHeight) {
        menuPageContentElement.style.maxHeight = "" + parentHeight + "px";
    } else {
        menuPageContentElement.style.maxHeight = "" + (parentHeight/2) + "px";
    }
}

//END trekcrumb-responsive.js