"use strict";

function comment_generic_displayRows(pageName, listOfComments, isAppend) {
    var commentListContent = document.getElementById("commentListContentID");
    var commentRowTemplate = document.getElementById("commentRowTemplateID");
    if(commentListContent == undefined || commentListContent == null
            || commentRowTemplate == undefined || commentRowTemplate == null) {
        console.error("comment_generic_displayRows() - ERROR: Missing required layouts in current view!");
        return;
    }
    
    if(isAppend) {
        //Do not delete any existing rows, but append the new ones after them.
        if(listOfComments == undefined || listOfComments == null || listOfComments.length == 0) {
            var newContentElement = document.createElement('span');
            newContentElement.innerHTML = ERROR_NO_DATA;
            commentListContent.appendChild(newContentElement);
            return;
        }
        
    } else {
        //Clean out any existing rows and replace with new ones:
        commentListContent.scrollTop;
        if(listOfComments == undefined || listOfComments == null || listOfComments.length == 0) {
            commentListContent.innerHTML = ERROR_NO_DATA ;
            return;
        } else {
            commentListContent.innerHTML = "";
        }
    }
    
    //Determine user login:
    var isUserLogin = validate_isUserLogin();
    var userLoginUsername = null;
    if(isUserLogin) {
        var usernameInputElement = document.getElementById('usernameID');
        if(usernameInputElement != undefined && usernameInputElement != null && usernameInputElement.value != null) {
            userLoginUsername = usernameInputElement.value.toLowerCase();
        }
    }
    
    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    var listSize = listOfComments.length;
    for(var index = 0; index < listSize; index++) {
        var comment = listOfComments[index];
        var rowCurrent = commentRowTemplate.cloneNode(true);
        rowCurrent.id = comment.id;
        if(pageName == FRAGMENT_COMMENT_LIST_USER_COMMENTS) {
            //Entire row is clickable:
            rowCurrent.querySelector("#commentRowContentID").href = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + comment.username + "/" + comment.tripID;
            
        } else if(pageName == FRAGMENT_COMMENT_LIST_TRIP_COMMENTS) {
            //Only user section is clickable:
            rowCurrent.querySelector("#commentUserSectionID").href = "javascript:user_view_Retrieve(false, '" + comment.username + "');";
        }
        
        //User Details:
        rowCurrent.querySelector("#userThumbnailFullnameID").innerHTML = comment.userFullname;
        rowCurrent.querySelector("#userThumbnailUsernameID").innerHTML = comment.username.toLowerCase();
        if(validate_isStringEmpty(comment.userImageName)) {
            rowCurrent.querySelector("#userThumbnailImageDefaultID").style.display = "block";
            rowCurrent.querySelector("#userThumbnailImageID").style.display = "none";
        } else {
            rowCurrent.querySelector("#userThumbnailImageDefaultID").style.display = "none";
            var userImageElement = rowCurrent.querySelector("#userThumbnailImageID");
            userImageElement.setAttribute("src", FILE_SERVER_URL_GETIMAGE_PROFILE + comment.userImageName);
            userImageElement.style.display = "block";
        }
        
        //Comment Details:
        if(pageName == FRAGMENT_COMMENT_LIST_USER_COMMENTS) {
            rowCurrent.querySelector("#tripNameID").innerHTML = comment.tripName;
        }        
        common_date_translateToLocaleTime(rowCurrent.querySelector("#createdDateID"), comment.created, true, false);
        rowCurrent.querySelector("#commentNoteID").innerHTML = comment.note;
        
        //Delete Menu:
        if(isUserLogin) {
            if(comment.username.toLowerCase() == userLoginUsername) {
                rowCurrent.querySelector("#menuCommentDeleteID").setAttribute("onclick", "comment_delete_init(" + pageName + ", '" + comment.id + "')");
                rowCurrent.querySelector("#menuCommentDeleteID").id = "menuCommentDeleteID" + comment.id;
                rowCurrent.querySelector("#menuCommentDeleteIDHover").id = "menuCommentDeleteID" + comment.id + "Hover";
            } else {
                rowCurrent.querySelector("#menuCommentDeleteLayoutID").style.display = "none";
            }
        } else {
            rowCurrent.querySelector("#menuCommentDeleteLayoutID").style.display = "none";
        }

        //Finally, display it:
        rowCurrent.style.display = 'block';
        commentListContent.appendChild(rowCurrent);
    }
}

function comment_trip_view() {
    //1. Force to open and populate:
    document.getElementById('commentListContentID').style.display = "none";
    comment_trip_init();
    
    //2. Scroll there:
    var targetTopPosition = document.getElementById('tripCommentLayoutID').offsetTop;
    jQuery("#tripDetailLayoutID").stop(true, true).animate(
            {scrollTop: targetTopPosition}, 
            1500, 
            "easeInOutExpo");
}

function comment_trip_init() {
    menu_detailLayout_showOrHide('commentListContentID');
    
    var commentListContent = document.getElementById('commentListContentID');
    if(commentListContent.style.display == "block") {
        //1. Check if Comment is ZERO:
        var tripCommentLayout = document.getElementById('tripCommentLayoutID');
        var numOfCommentsTotal = parseInt(tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL));
        if(numOfCommentsTotal == 0) {
            commentListContent.innerHTML = INFO_MSG_COMMENT_EMPTY;
            return;
        }

        //2. Else - Check if comment initial contents may have already dowloaded:
        if(!validate_isStringEmpty(commentListContent.innerHTML)) {
            console.log("comment_trip_init() - Initial contents already dowloaded!");
            common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS);
            comment_trip_paginationUpdate(false);
            return;
        }

        //3. Else - Download initial content:
        //comment_trip_serviceRequest();
    
    } else {
        //Hide any content
        common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS);
        document.getElementById('commentByTripMenuMoreNextID').style.display = "none";
    }
}

function comment_trip_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        common_progressBarWidget_show(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS);
        document.getElementById('commentByTripMenuMoreNextID').style.display = "none";
    
        //Compose request:
        var tripID = document.getElementById('tripID').value;
        var tripCommentLayout = document.getElementById('tripCommentLayoutID');
        var offsetCurrent = tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_OFFSET_CURRENT);
        var commentToRetrieve = {
            tripID: tripID
        };

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                comment_trip_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            comment_trip_errorHandling(xmlhttpRequest.statusText + " - " + xmlhttpRequest.statusText);
        };
        var urlTarget = APP_ROOT_URI + "/trekcrumb/comment/retrieveByTrip/AJAX/" + offsetCurrent;
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(commentToRetrieve));
        } catch(error) {
            comment_trip_errorHandling(error.message);
        }
    }
}

function comment_trip_errorHandling(errorMessage) {
    common_AJAX_opsError_show(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS, "Loading Comment Error: " + errorMessage, errorMessage);
    comment_trip_paginationUpdate(false);
}

function comment_trip_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return false;
    }
    
    //It is async:
    var commentListContent = document.getElementById('commentListContentID');
    if(commentListContent == undefined || commentListContent == null) {
        console.error("comment_trip_serviceResponse() - ERROR: Cannot find commentListContentID on current view!");
        return;
    }
    
    common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS);

    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(!isSuccess) {
        comment_trip_paginationUpdate(false);
        return;    
    }
    
    var listOfComments = JSON.parse(xmlhttpRequest.responseText).listOfComments;
    if(listOfComments == undefined || listOfComments == null || listOfComments.length == 0) {
        comment_trip_paginationUpdate(false);
    } else {
        comment_trip_paginationUpdate(true);
    }
    comment_generic_displayRows(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS, listOfComments, true);
}

function comment_trip_paginationUpdate(isUpdate) {
    var tripCommentLayout = document.getElementById('tripCommentLayoutID');
    var numRecordsTotal = parseInt(tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL));
    var numOfRowsMax = parseInt(tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_NUMBER_ROWS_MAXIMUM));
    var offsetCurrent = parseInt(tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_OFFSET_CURRENT));
    
    if(isUpdate) {
        offsetCurrent = offsetCurrent + numOfRowsMax;
        tripCommentLayout.setAttribute(VARATTR_COMMENT_TRIP_OFFSET_CURRENT, offsetCurrent);
    }
    
    /*
     * Show menu 'More' or not. 
     * If we are to show the menu, we unfortunately need to validate if the 'tripCommentLayoutID'
     * is on display or hidden. Do not show the menu if the comments are hidden.
     */
    if(offsetCurrent < numRecordsTotal) {
        //There's more:
        var commentListContent = document.getElementById('commentListContentID');
        if(commentListContent.style.display == "block") {
            document.getElementById('commentByTripMenuMoreNextID').style.display = "block";
        } else {
            document.getElementById('commentByTripMenuMoreNextID').style.display = "none";
        }
    } else {
        //No more comments available:
        document.getElementById('commentByTripMenuMoreNextID').style.display = "none";
    }
}

function comment_trip_next() {
    var tripCommentLayout = document.getElementById('tripCommentLayoutID');
    var numRecordsTotal = parseInt(tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL));
    var offsetCurrent = parseInt(tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_OFFSET_CURRENT));
    if(offsetCurrent < numRecordsTotal) {
        comment_trip_serviceRequest();
    } else {
        console.error("comment_trip_next() - ERROR: There is NO MORE trek comments to retrieve!");
        document.getElementById('commentByTripMenuMoreNextID').style.display = "none";
    }
}

function comment_user_init() {
    //Clear ALL cached data:
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserComments");
}

/*
 * User Comments starting point. 
 * -> It would try to recover any last current state params and result list 
 *    when available (cached in session storage). 
 * -> This is to support feature when user navigates away from current UserComments page, 
 *    but then click browser "BACK" button such that we avoid to re-init the list service call.
 */
function comment_user_current() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get current state params if available:
        var offsetCurrent = sessionStorage.getItem(CACHE_KEY_COMMENT_USER_OFFSET_CURRENT);
        var pageCurrent = sessionStorage.getItem(CACHE_KEY_COMMENT_USER_PAGE_CURRENT);
        var numOfRecordsTotal = sessionStorage.getItem(CACHE_KEY_COMMENT_USER_TOTAL_RECORDS);
        if(offsetCurrent != undefined && offsetCurrent != null 
                && pageCurrent != undefined && pageCurrent != null
                && numOfRecordsTotal != undefined && numOfRecordsTotal != null) {
            //Set these params onto the page:
            pageList_update(FRAGMENT_COMMENT_LIST_USER_COMMENTS, 
                            parseInt(offsetCurrent), parseInt(pageCurrent), parseInt(numOfRecordsTotal));
            
            //Caching: Get the last result List if available:
            var username = document.getElementById('userCommentsLayoutID').getAttribute(VARATTR_USER_USERNAME);
            var listOfComments = sessionStorage.getItem(CACHE_KEY_COMMENT_USER_RESULT_PAGE + username + pageCurrent);
            if(listOfComments != undefined && listOfComments != null) {
                common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_USER_COMMENTS);
                comment_generic_displayRows(FRAGMENT_COMMENT_LIST_USER_COMMENTS, JSON.parse(listOfComments), false);
            } else {
                comment_user_serviceRequest();
            }
            return;
        }
    }
    
    //No caching available. let's start from clean states:
    pageList_init(FRAGMENT_COMMENT_LIST_USER_COMMENTS);
    comment_user_serviceRequest();
}

function comment_user_next() {
    if(pageList_next(FRAGMENT_COMMENT_LIST_USER_COMMENTS)) {
        document.getElementById("commentListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_COMMENT_LIST_USER_COMMENTS);
    
        comment_user_retrieveList();
    }
}

function comment_user_previous() {
    if(pageList_previous(FRAGMENT_COMMENT_LIST_USER_COMMENTS)) {
        document.getElementById("commentListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_COMMENT_LIST_USER_COMMENTS);

        comment_user_retrieveList();
    }
}

function comment_user_retrieveList() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Update current pagination params into cache:
        common_localeStorage_cacheCurrentListState(
                FRAGMENT_COMMENT_LIST_USER_COMMENTS,
                CACHE_KEY_COMMENT_USER_OFFSET_CURRENT,
                CACHE_KEY_COMMENT_USER_PAGE_CURRENT,
                CACHE_KEY_COMMENT_USER_TOTAL_RECORDS);
                
        //Caching: Get the last result List if available:
        var username = document.getElementById('userCommentsLayoutID').getAttribute(VARATTR_USER_USERNAME);
        var pageCurrent = document.getElementById(FRAGMENT_COMMENT_LIST_USER_COMMENTS + 'paginationListLayoutID').getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
        var listOfComments = sessionStorage.getItem(CACHE_KEY_COMMENT_USER_RESULT_PAGE + username + pageCurrent);
        if(listOfComments != undefined && listOfComments != null) {
            common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_USER_COMMENTS);
            comment_generic_displayRows(FRAGMENT_COMMENT_LIST_USER_COMMENTS, JSON.parse(listOfComments), false);
        } else {
            comment_user_serviceRequest();
        }
    } else {
        comment_user_serviceRequest();
    }
}

function comment_user_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        var xmlhttpRequest = new XMLHttpRequest();
        
        //Prepare response handling:
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                comment_user_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_COMMENT_LIST_USER_COMMENTS, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        //Get params:
        var username = document.getElementById('userCommentsLayoutID').getAttribute(VARATTR_USER_USERNAME);
        var offsetCurrent = document.getElementById(FRAGMENT_COMMENT_LIST_USER_COMMENTS + 'paginationListLayoutID').getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT);

        //Compose request:
        var commentToRetrieve = {
            username: username
        };
        
        //Prepare send (as JSON request):
        var urlTarget = APP_ROOT_URI + "/trekcrumb/comment/retrieveByUser/AJAX/" + offsetCurrent;
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(commentToRetrieve));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_COMMENT_LIST_USER_COMMENTS, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function comment_user_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_USER_COMMENTS);

    if(xmlhttpRequest.status != 200) {
        var errorMessageArray = [xmlhttpRequest.statusText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        var errorMessageArray = [serverResponseObj.serviceError.errorText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    
    //Success
    var listOfComments = serverResponseObj.listOfComments;
    comment_generic_displayRows(FRAGMENT_COMMENT_LIST_USER_COMMENTS, listOfComments, false);
    
    //Caching: Save list results
    var username = document.getElementById('userCommentsLayoutID').getAttribute(VARATTR_USER_USERNAME);
    var pageCurrent = document.getElementById(FRAGMENT_COMMENT_LIST_USER_COMMENTS + 'paginationListLayoutID').getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_COMMENT_USER_RESULT_PAGE + username + pageCurrent, 
            JSON.stringify(listOfComments)); 
}

function comment_create_init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    
    if(!validate_isUserLogin()) {
        //Redirect to server:
        console.warn("comment_create_init() - WARNING: User is NOT login. Forwarding to login page!");
        window.location.href = WEBAPP_URL_USER_SIGNUP;
    }
    
    var commentCreateLayout = document.getElementById('commentCreateLayoutID');
    if(commentCreateLayout == undefined || commentCreateLayout == null) {
        //Invalid view:
        console.error("comment_create_Init() - ERROR: Could not find commentCreateLayoutID layout!");
        return;
    }
    
    //Display:
    document.getElementById(FRAGMENT_COMMENT_CREATE + "formID").style.display = "block";
    document.getElementById('commentCreateNoteInputID').value = "";
    common_form_afterInit(FRAGMENT_COMMENT_CREATE, commentCreateLayout);
}

function comment_create_cancel() {
    var commentCreateLayout = document.getElementById('commentCreateLayoutID');
    if(commentCreateLayout == undefined || commentCreateLayout == null) {
        //Invalid:
        return;
    }
    
    commentCreateLayout.style.display = "none";
}

function comment_create_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    
    //Validate:
    var commentNote = document.getElementById('commentCreateNoteInputID').value;
    if(validate_isStringEmpty(commentNote)) {
        common_errorOrInfoMessage_show([ERROR_MSG_PARAM_INVALID_COMMENT_NOTE], null);
        return;
    }

    //Call server:
    common_form_afterSubmit(FRAGMENT_COMMENT_CREATE);
    comment_create_serviceRequest();
}

function comment_create_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var commentToCreate = {};
        commentToCreate.userID = document.getElementById('userID').value;
        commentToCreate.tripID = document.getElementById('tripID').value;
        commentToCreate.note = document.getElementById('commentCreateNoteInputID').value;
        
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                comment_create_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_COMMENT_CREATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/comment/create/submit/AJAX/";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(commentToCreate));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_COMMENT_CREATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function comment_create_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var commentCreateLayout = document.getElementById("commentCreateLayoutID");
    if(commentCreateLayout == undefined || commentCreateLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_COMMENT_CREATE, xmlhttpRequest);
    if(isSuccess) {
        var tripCommentLayout = document.getElementById('tripCommentLayoutID');
        var numOfCommentsTotal = tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL);
        var numCommentsNew = parseInt(numOfCommentsTotal) + 1;
        tripCommentLayout.setAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL, numCommentsNew);
        document.getElementById('commentNumberID').innerHTML = numCommentsNew;
        document.getElementById('tripCommentHeaderNumRecordsTotalID').innerHTML = numCommentsNew;
        
        tripCommentLayout.setAttribute(VARATTR_COMMENT_TRIP_OFFSET_CURRENT, "0");

        //Adjust comment icon color and symbol:
        var commentIcon = document.getElementById('commentIconID');
        commentIcon.className = commentIcon.className.replace( /(?:^|)font-color(\S+)/g , 'font-color-menu' );

        var commentIconSymbol = document.getElementById('commentIconSymbolID');
        commentIconSymbol.className = commentIconSymbol.className.replace('fa-comment-o' , 'fa-comment' );

        document.getElementById('commentListContentID').innerHTML = "";
        comment_trip_view();
        
        common_errorOrInfoMessage_show(null, INFO_MSG_COMMENT_CREATE_SUCCESS);
    }
    
    commentCreateLayout.style.display = "none";
}

function comment_delete_init(pageName, commentID) {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    
    var commentDeleteLayout = document.getElementById('commentDeleteLayoutID');
    if(commentDeleteLayout == undefined || commentDeleteLayout == null) {
        //Invalid view:
        console.error("comment_delete_Init() - ERROR: Could not find commentDeleteLayoutID layout!");
        return;
    }
    
    var commentRowToDelete = document.getElementById(commentID);
    if(commentRowToDelete == undefined || commentRowToDelete == null) {
        //Invalid:
        common_errorOrInfoMessage_show(["Selected comment to delete could not be found in this view!"], null); 
        return;
    }

    //Populate values:
    document.getElementById('commentDeletePageNameID').value = pageName;
    document.getElementById('commentDeleteCommentIDInputID').value = commentRowToDelete.id;
    document.getElementById('commentDeleteCreatedID').innerHTML= commentRowToDelete.querySelector("#createdDateID").innerHTML;
    document.getElementById('commentDeleteNoteInputID').value = commentRowToDelete.querySelector("#commentNoteID").innerHTML;
    
    //Display:
    document.getElementById(FRAGMENT_COMMENT_DELETE + "formID").style.display = "block";
    common_form_afterInit(FRAGMENT_COMMENT_DELETE, commentDeleteLayout);
    
    commentDeleteLayout.style.display = "block";
}

function comment_delete_cancel() {
    var commentDeleteLayout = document.getElementById('commentDeleteLayoutID');
    if(commentDeleteLayout == undefined || commentDeleteLayout == null) {
        //Invalid:
        return;
    }
    
    document.getElementById('commentDeletePageNameID').value = "";
    document.getElementById('commentDeleteCommentIDInputID').value = "";
    document.getElementById('commentDeleteCreatedID').innerHTML= "";
    document.getElementById('commentDeleteNoteInputID').value = "";
    commentDeleteLayout.style.display = "none";
}

function comment_delete_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    
    //Call server:
    common_form_afterSubmit(FRAGMENT_COMMENT_DELETE);
    comment_delete_serviceRequest();
}

function comment_delete_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        var pageName = document.getElementById('commentDeletePageNameID').value;
        if(pageName == null || pageName == "") {
            var errMsg = "Illegal: Unknown source page name!";
            common_form_AJAX_handleError(FRAGMENT_COMMENT_DELETE, errMsg, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + errMsg);
            return;
        }

        //Compose request:
        var commentToDelete = {};
        commentToDelete.userID = document.getElementById('userID').value;
        commentToDelete.id = document.getElementById('commentDeleteCommentIDInputID').value;
        
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                if(pageName == FRAGMENT_COMMENT_LIST_TRIP_COMMENTS) {
                    comment_delete_serviceResponse_byTrip(xmlhttpRequest);
                } else if(pageName == FRAGMENT_COMMENT_LIST_USER_COMMENTS) {
                    comment_delete_serviceResponse_byUser(xmlhttpRequest);
                }
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_COMMENT_DELETE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/comment/delete/submit/AJAX/";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(commentToDelete));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_COMMENT_DELETE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function comment_delete_serviceResponse_byTrip(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var commentDeleteLayout = document.getElementById("commentDeleteLayoutID");
    if(commentDeleteLayout == undefined || commentDeleteLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_COMMENT_DELETE, xmlhttpRequest);
    if(isSuccess) {
        var tripCommentLayout = document.getElementById('tripCommentLayoutID');
        var numOfCommentsTotal = tripCommentLayout.getAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL);
        var numCommentsNew = 0;
        if(numOfCommentsTotal > 0) {
            numCommentsNew = parseInt(numOfCommentsTotal) - 1;
            tripCommentLayout.setAttribute(VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL, numCommentsNew);
            document.getElementById('commentNumberID').innerHTML = numCommentsNew;
            document.getElementById('tripCommentHeaderNumRecordsTotalID').innerHTML = numCommentsNew;
        }
        
        tripCommentLayout.setAttribute(VARATTR_COMMENT_TRIP_OFFSET_CURRENT, "0");

        //Adjust icon color and symbol:
        var commentIcon = document.getElementById('commentIconID');
        var commentIconSymbol = document.getElementById('commentIconSymbolID');
        var newColor = "";
        var newSymbol = "";
        if(numCommentsNew > 0) {
            newColor = "font-color-menu";
            newSymbol = "fa-comment";
        } else {
            newColor = "font-color-passive";
            newSymbol = "fa-comment-o";
        }
        commentIcon.className = commentIcon.className.replace( /(?:^|)font-color(\S+)/g , newColor);
        commentIconSymbol.className = commentIconSymbol.className.replace( /fa-comment/g , newSymbol);

        document.getElementById('commentListContentID').innerHTML = "";
        comment_trip_view();
        
        common_errorOrInfoMessage_show(null, INFO_MSG_COMMENT_DELETE_SUCCESS);
        commentDeleteLayout.style.display = "none";
    }
}

function comment_delete_serviceResponse_byUser(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var commentDeleteLayout = document.getElementById("commentDeleteLayoutID");
    if(commentDeleteLayout == undefined || commentDeleteLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has browsed away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_COMMENT_DELETE, xmlhttpRequest);
    if(isSuccess) {
        //After Delete: Clean all possible related caches:
        comment_user_init();
        commentDeleteLayout.style.display = "none";
            
        //Redirect to profile:
        window.location.href = APP_ROOT_URI + "/trekcrumb/user/retrieve/me";
    }
}



//END trekcrumb-comment.js