"use strict";
/*
 * JS library to deal with Google Map API.
 * NOTE:
 * 1. On GoogleMap Javascript API library: We do not include the JS script URL in the include_header.jsp 
 *    file as other libraries, but instead we define and init call here so that it only applicable 
 *    when we need to display map. Furthermore, we make call to Google API JS async-ly to anticipate 
 *    if there was any failure during the loading since it is a 3rd party server call.
 * 2. WARNING: GoogleMap API may still throw an async error even after script is loaded and map is
 *    configured/displayed successfully. We could only know this because the API would spit console.error()
 *    message and THERE IS NOTHING we can do or catch.
 */

var mMapGoogle_numPlacesTotal;
var mMapGoogle_map;
var mMapGoogle_listOfLatLang;
var mMapGoogle_listOfMarkers;
var mMapGoogle_infoWindow;
var mMapGoogle_pathPolyline;
var mMapGoogle_marker_currentLoc;
var mMapGoogle_currentLoc_latitude;
var mMapGoogle_currentLoc_longitude;
var mMapGoogle_currentLoc_infoWindow;

/*
 * Starts Google MapAPI.
 * This is a-sync operation where we try to load the API script. Upon success or error,
 * the function will invoke the corresponding call-back functions to act accordingly. 
 * NOTE: NEVER invoke any other operation before this a-sync call completes. Hence, ensure
 * any subsequent operation is on the call-back functions when appropriate.
 */
function mapGoogle_start(mapFragment) {
    console.debug("mapGoogle_start() - Starting ...");
    jQuery.getScript(MAP_SERVER_DYNAMIC_GOOGLEMAP)
    .done(function(script, textStatus) {
        console.debug("mapGoogle_start() - API script loaded successfully: " + textStatus);
        
        if(mapFragment == FRAGMENT_MAP_DYNAMIC_DETAILCONTENT) {
            mapGoogle_detail_displayContent_googleMapAPISuccess();
        } else if(mapFragment == FRAGMENT_MAP_DYNAMIC_CURRENTLOC) {
            mapGoogle_currentLocation_googleMapAPISuccess();
        } else {
            console.error("mapGoogle_start() - ERROR: Unexpected map fragment to initialize: " + mapFragment);
        }
    })
    
    .fail(function(jqxhr, settings, exception) {
        console.error("mapGoogle_start() - API script loaded FAILED with error: " + jqxhr.responseText );
        var mapMessage = document.getElementById('mapMessageID');
        if(mapMessage != undefined && mapMessage != null) {
            mapMessage.innerHTML = ERROR_MSG_PLACE_GOOGLEMAP_API_FAILURE ;
        }
    });        
}

function mapGoogle_initMap() {
    if(mMapGoogle_map != null) {
        console.debug("mapGoogle_initMap() - mMapGoogle_map is not NULL. Let's reuse it!");
        return;    
    } 
    
    //Else:
    console.debug("mapGoogle_initMap() - mMapGoogle_map is NULL! Initialize ...");
    var mapCanvas = document.getElementById('mapCanvasID');
    var latLangCenterDefault = new google.maps.LatLng(0, 0);
    var mapOptionsDefault = {
            center: latLangCenterDefault,
            zoom: MAP_ZOOM_LEVEL_DEFAULT_FAR,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: { 
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_BOTTOM    
            },
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            scaleControl: true
    };

    mMapGoogle_map = new google.maps.Map(mapCanvas, mapOptionsDefault);
    console.debug("mapGoogle_initMap() - mMapGoogle_map successfully initialized.");
}

function mapGoogle_initComponents() {
    mMapGoogle_listOfLatLang = [];
    mMapGoogle_numPlacesTotal = 0;
    mMapGoogle_marker_currentLoc = null;

    //Markers:
    if(mMapGoogle_listOfMarkers == null) {
        mMapGoogle_listOfMarkers = [];
    } else {
        //Refresh:
        for (var i = 0; i < mMapGoogle_listOfMarkers.length; i++) {
            mMapGoogle_listOfMarkers[i].setMap(null);
        }
        mMapGoogle_listOfMarkers = [];
    }
    
    //Infowindow:
    if(mMapGoogle_infoWindow == null) {
        mMapGoogle_infoWindow = new google.maps.InfoWindow();
    }
    
    //Pathline:
    if(mMapGoogle_pathPolyline == null) {
        mMapGoogle_pathPolyline = new google.maps.Polyline({
            strokeColor: MAP_PATHLINE_COLOR,
            strokeOpacity: 0,
            icons: [{
                icon: MAP_PATHLINE_SYMBOL_GOOGLEMAP,
                offset: '0',
                repeat: '20px'
            }]
        });       
    } else {
        //Refresh:
        mMapGoogle_pathPolyline.setMap(null);
        mMapGoogle_pathPolyline.setPath(null);
        mMapGoogle_pathPolyline.setVisible(false);
    }    
}

function mapGoogle_detail_DisplayContent() {
    console.debug("mapGoogle_detail_DisplayContent() - Starting ...");
    
    //1. Find required variables on current page:
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    if(pagePlaceLayout == undefined || pagePlaceLayout == null) {
        console.warn("mapGoogle_detail_DisplayContent() - WARNING: Pagination Place Layout is missing!");
        return;
    }
    var listOfPlacesString = jQuery("#pagePlace_listOfPlacesID").val();
    if(listOfPlacesString == undefined 
            || listOfPlacesString == null 
            || listOfPlacesString == "") {
        console.warn("mapGoogle_detail_DisplayContent() - WARNING: No list of places to display.");
        return;
    }
    var mapCanvas = document.getElementById('mapCanvasID');
    if(mapCanvas == undefined || mapCanvas == null
            || mapCanvas.offsetWidth == 0
            || mapCanvas.offsetHeight == 0) {
        //Cannot generate proper GoogleMap without valid canvas to paint on:
        console.warn("mapGoogle_detail_DisplayContent() - WARNING: MapCanvas is either null or not displayed! Skipping the operation.");
        return;
    }
    
    //2. Initializes all required variables/components:
    mapGoogle_start(FRAGMENT_MAP_DYNAMIC_DETAILCONTENT);
}

function mapGoogle_detail_displayContent_googleMapAPISuccess() {
    mapGoogle_initMap();
    mapGoogle_initComponents();
    mapGoogle_detail_populateAndAdjust();
}

function mapGoogle_detail_populateAndAdjust() {
    console.debug("mapGoogle_detail_populateAndAdjust() - Starting ...");
    
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    
    //1. Processes the list of Places:
    mMapGoogle_numPlacesTotal = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    var placeIndexSelected = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
    var listOfPlacesString = jQuery("#pagePlace_listOfPlacesID").val();
    var listOfPlacesJSON = JSON.parse(listOfPlacesString);
    var placeSelected = listOfPlacesJSON[placeIndexSelected];

    //2. Re-center Google Map
    var latLangPlaceSelected = new google.maps.LatLng(placeSelected.latitude, placeSelected.longitude);
    mMapGoogle_map.setCenter(latLangPlaceSelected);

    //3. Paint markers:
    for(var i = 0; i < mMapGoogle_numPlacesTotal; i++) {
        var place = listOfPlacesJSON[i];

        var placeLatLang = new google.maps.LatLng(place.latitude, place.longitude);
        mMapGoogle_listOfLatLang.push(placeLatLang);
        
        var placeMarker = new google.maps.Marker({
            position: placeLatLang,
            map: mMapGoogle_map,
            title: "" + i,
            icon: MAP_MARKER_ICON_PLACE
        });
        mapGoogle_marker_addClickListener(placeMarker);
        mMapGoogle_listOfMarkers.push(placeMarker);
    }
    
    //4. Paint pathlines:
    if(mMapGoogle_numPlacesTotal > 1) {
        mMapGoogle_pathPolyline.setMap(mMapGoogle_map);
        mMapGoogle_pathPolyline.setPath(mMapGoogle_listOfLatLang);
        mMapGoogle_pathPolyline.setVisible(true);
    }
    
    //5. Final adjustments:
    mapGoogle_detail_adjustBoundsAndZoom();
    mapGoogle_trigger_markerClickedEvent(placeIndexSelected);
    
    //6. Mark as completed:
    var mapCanvas = document.getElementById('mapCanvasID');
    mapCanvas.setAttribute(VARATTR_MAP_IS_MAP_INITIALIZED, "true");
    console.debug("mapGoogle_detail_populateAndAdjust() - Completed.");
}

/*
 * Adjusts map boundary to ensure all markers (bounds) are within the current view.
 */
function mapGoogle_detail_adjustBoundsAndZoom() {
    if(mMapGoogle_map == null || mMapGoogle_listOfLatLang == null) {
        //Map is not ready:
        return;
    }
    
    /*
     * WARNING: GoogleMap's fitBounds() is async operation that requires time to complete.
     * Therefore we cannot call the next operation immediately, and should use
     * a callback by registering a listener once all the work on the map is done.
     */
    var latLngBounds = new google.maps.LatLngBounds();
    for(var i = 0; i < mMapGoogle_listOfLatLang.length; i++) {
        latLngBounds.extend(mMapGoogle_listOfLatLang[i]);
    }
    mMapGoogle_map.fitBounds(latLngBounds);
    google.maps.event.addListenerOnce(mMapGoogle_map, 'idle', function() {
        mapGoogle_detail_adjustZoomCallback();
    });    
}

/*
 * Adjusts map zoom level to make sure all markers (bounds) are in the view, and yet
 * not too close. That is, if there was only one marker, or multiple markers but
 * short distances, the zoom became too closed-up. 
 */
function mapGoogle_detail_adjustZoomCallback() {
    var zoomLevelAdjusted = mMapGoogle_map.getZoom();
    if(zoomLevelAdjusted >= 20) {
        //Too close, zoom out to default:
        mMapGoogle_map.setZoom(MAP_ZOOM_LEVEL_DEFAULT_CLOSEUP);
        mMapGoogle_map.setCenter(mMapGoogle_listOfLatLang[0]);
    }
}

function mapGoogle_marker_addClickListener(marker) {
    google.maps.event.addListener(marker, 'click', function() {
        mapGoogle_marker_clicked(marker);
    });
}

function mapGoogle_marker_clicked(marker) {
    //mapGoogle_infoWindow_custom();
    
    /*
     * Custom: Display info window
     */
    var markerIndex = parseInt(marker.getTitle());
    mMapGoogle_infoWindow.setContent("Place " + (markerIndex + 1));
    mMapGoogle_infoWindow.open(mMapGoogle_map, marker);

    /*
     * Custom: Update PlaceDetail pagination and content data
     */
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    if(pagePlaceLayout != undefined && pagePlaceLayout != null) {
        var indexCurrent = marker.title;
        pagePlaceLayout.setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, indexCurrent);
        pagePlace_updateNavigationMenu(null, null);
        place_detail_DisplayContent();
    }
}

/* 
 * Custom: Hide InfoWindow 'close' (X) button
 * 1. This step is tricky. The close button inside InfoWindow is a
 *    div node without a classname, but it is right after InfoWindow content node
 *    whose CSS classname is 'gm-style-iw'. Therefore we find the close button node
 *    by the following syntax. If GoogleMap changes this layout arrangement, then
 *    this logic will not work.
 * 2. Once this is done, ALL InfoWindow at the given Google Map object will suffer
 *    lack of the 'close' button and cannot be closed. Therefore, use it only if necessary.
 */
function mapGoogle_infoWindow_custom() {
    var iwCloseButtonElement = null;
    var iwContentElement = jQuery(".gm-style-iw");
    if(iwContentElement != undefined && iwContentElement != null) {
        iwCloseButtonElement = iwContentElement.next();
    }
    if(iwCloseButtonElement != undefined && iwCloseButtonElement != null) {
        iwCloseButtonElement.css({display:"none"});
    }
}

/*
 * Triggers a 'resize' event to the map whenever the MapCanvas area changes it sizes,
 * after which the map API would automatically readjust its dimensions and tiles.
 * NOTE:
 * 1. This is to support MapCanvas changes its DIV dimension (from small to larger, etc.)
 *    via Javascript where the map API wouldn't know that the window size has changed. Hence,
 *    this special operation triggers the 'resize' event such that the map API event
 *    listener on 'resize' would catch it and readjust map dimension and tiles within the
 *    new MapCanvas dimension. Without this trigger, the map is left with its old dimension
 *    and would cause empty blank grey area on MapCanvas.
 * 2. This is NOT to support if user manually changes the browser size because it seems 
 *    map API catches this already, and readjust the map accordingly.
 * 3. Ref: https://code.google.com/p/gmaps-api-issues/issues/detail?id=1448
 */
function mapGoogle_trigger_resizeEvent() {
    if(mMapGoogle_map == null) {
        //Not ready:
        return;
    }
    
    /* 
     * WARNING: When MapCanvas dimension changes, map API works its way to fill the new space.
     * However, this takes time and we cannot immediately trigger the 'resize' event because
     * the resizing is not done yet and cause the map listener to fail to adjust to new
     * dimension. Hence, we put an extra sleep time to make sure all resizing is done
     * before we trigger the 'resize' event for map Listener to catch it.
     * For some reason, using listener on 'idle' event similar to mapGoogle_adjustBoundsAndZoom()
     * did not work here.
     */
    var centerCurrent = mMapGoogle_map.getCenter();
    var zoomCurrent = mMapGoogle_map.getZoom();
    
    setTimeout(function() {
        google.maps.event.trigger(mMapGoogle_map, 'resize');
    }, (2000));


    mMapGoogle_map.setCenter(centerCurrent);
    mMapGoogle_map.setZoom(zoomCurrent);
}

/*
 * Triggers a 'click' event for a marker given its index when an outside element (not part of Map API elements)
 * is clicked independently by user.
 */
function mapGoogle_trigger_markerClickedEvent(index) {
    if(mMapGoogle_listOfMarkers == null || mMapGoogle_listOfMarkers.length == 0) {
        return;
    }
    if(index < 0 || (index + 1) > mMapGoogle_listOfMarkers.length) {
        return;
    }
    var marker = mMapGoogle_listOfMarkers[index];
    
    //Trigger marker listener:
    google.maps.event.trigger( marker, 'click' );
    
    //Re-center the map:
    //mMapGoogle_map.setCenter(marker.position);
}

function mapGoogle_currentLocation_init(lat, lon) {
    var mapCanvas = document.getElementById('mapCanvasID');
    if(mapCanvas == undefined || mapCanvas == null
            || mapCanvas.offsetWidth == 0
            || mapCanvas.offsetHeight == 0) {
        //Cannot generate proper GoogleMap without valid canvas to paint on:
        console.warn("mapGoogle_currentLocation_init() - WARNING: MapCanvas is either null or not displayed! Skipping the operation.");
        return;
    }

    mMapGoogle_currentLoc_latitude = lat;
    mMapGoogle_currentLoc_longitude = lon;

    if(mMapGoogle_map == null) {
        /* 
         * This could be the case where trip has empty place and MapAPI has not been initialized. 
         * Hence, we must start and init MapAPI
         */
        document.getElementById('mapMessageID').innerHTML = INFO_PLACE_MAP_LOADING;
        mapGoogle_start(FRAGMENT_MAP_DYNAMIC_CURRENTLOC);
    
    } else {
        mapGoogle_currentLocation_displayContent();
    }
}

function mapGoogle_currentLocation_googleMapAPISuccess() {
    mapGoogle_initMap();
    mMapGoogle_map.setZoom(MAP_ZOOM_LEVEL_DEFAULT_CLOSEUP);
    mapGoogle_currentLocation_displayContent();
}

function mapGoogle_currentLocation_displayContent() {
    console.debug("mapGoogle_currentLocation_displayContent() - Processing ...");
    
    //Reset any previous current location marker:
    if(mMapGoogle_marker_currentLoc != null ) {
        //Clean any previous currentLoc marker and re-initialize:
        mMapGoogle_marker_currentLoc.setMap(null);
        mMapGoogle_marker_currentLoc = null;
    }
    var currentLocLatLang = new google.maps.LatLng(mMapGoogle_currentLoc_latitude, mMapGoogle_currentLoc_longitude);
    mMapGoogle_marker_currentLoc = new google.maps.Marker({
                        position: currentLocLatLang,
                        map: mMapGoogle_map,
                        icon: MAP_MARKER_ICON_CURRENTLOC});   
   
    mMapGoogle_map.setCenter(currentLocLatLang);

    /*
     * InfoWindow: Beware that Google API would come back with its default CSS stylng including
     * overflow bars. We force our own in the content below.
     */
    if(mMapGoogle_currentLoc_infoWindow == null) {
        var currentLocInfoWindowContent = 
            "<div class='font font-size-small' style='overflow:hidden;'>"
            + "<b>You</b><br>"
            + "Your current location<br/>"
            + "<div style='text-align:center;'>"
            + "<div class='button-map-currentlocation-add' onclick='place_create_validateAndSubmit()'>Place Add</div></div>"
            + "</div>";
        
        mMapGoogle_currentLoc_infoWindow = new google.maps.InfoWindow();
        mMapGoogle_currentLoc_infoWindow.setContent(currentLocInfoWindowContent);
    }
    
    google.maps.event.addListener(mMapGoogle_marker_currentLoc, 'click', function() {
        mMapGoogle_currentLoc_infoWindow.open(mMapGoogle_map, mMapGoogle_marker_currentLoc);
    });

    //Open currentLoc infowindow, and close any other infowindow
    if(mMapGoogle_infoWindow != undefined && mMapGoogle_infoWindow != null) {
        mMapGoogle_infoWindow.close();
    }
    mMapGoogle_currentLoc_infoWindow.open(mMapGoogle_map, mMapGoogle_marker_currentLoc);
    console.debug("mapGoogle_currentLocation_displayContent() - Completed.");
}

/*
 * Draws static map given a (latitude, longitude) coordinate with Google Map API.
 */
function mapGoogle_static_displayContent(lat, lon) {
    var mapCanvasLayout = document.getElementById("mapStaticCanvasID");
    if(mapCanvasLayout == undefined || mapCanvasLayout == null) {
        console.error("map_static_displayContent() - ERROR: Required mapStaticCanvasID element missing on current page!");
        return;
    }
    
    //Else:
    var mapURL = MAP_SERVER_STATIC_GOOGLEMAP 
                 + "&center=" + lat + "," + lon
                 + "&size=" + mapCanvasLayout.offsetWidth + "x" + MAP_STATIC_DIMENSION_HEIGHT
                 + "&markers=color:green%7C" + lat + "," + lon;
    var mapStaticImage = document.getElementById("mapStaticImageID");
    mapStaticImage.src = mapURL;
}

/*
 * Draws static map given a (latitude, longitude) coordinate with Google Map API.
 * NOTE: 
 * The idea was to make AJAX call to the target URL, and evaluate response success or fail. If fail,
 * then handle it to call a different map API. If success, then display the map.
 * 
 * DEPRECATED (UNUSED)
 */
function mapGoogle_static_displayContent_serviceRequest(lat, lan) {
    if(!mDevice_isSupportAJAX) {
        console.error("mapGoogle_static_displayContent_serviceRequest() - ERROR: Client browser does NOT support AJAX! Cannot proceed.");
        return false;
    }
    
    var mapCanvasLayout = document.getElementById("mapStaticCanvasID");
    if(mapCanvasLayout != undefined || mapCanvasLayout != null) {
        console.error("mapGoogle_static_displayContent_serviceRequest() - ERROR: Missing 'locationMapStaticLayoutID' element on current page! Cannot proceed.");
        return false;
    }
    
    //Compose request:
    var mapURL = MAP_SERVER_STATIC_GOOGLEMAP 
                 + "&center=" + lat + "," + lan
                 + "&size=" + mapCanvasLayout.offsetWidth + "x" + mapCanvasLayout.offsetHeight
                 + "&markers=color:green%7C" + lat + "," + lan;
    
    var xmlhttpRequest = common_AJAX_getCORSRequest("GET", mapURL);
    if(xmlhttpRequest == null) {
        console.error("mapGoogle_static_displayContent_serviceRequest() - ERROR: Cannot make AJAX cross-domain calls!");
        return false;
    }

    //Else:
    xmlhttpRequest.onreadystatechange = function() {
        if (xmlhttpRequest.readyState == 4) {
            //Request finished and response is ready:
            mapGoogle_staticMap_serviceResponse(xmlhttpRequest);
        }
    };
    xmlhttpRequest.onerror = function() {
        console.error("mapGoogle_static_displayContent_serviceRequest() - ERROR: " + xmlhttpRequest.statusText);
        return false;
    };

    //Send to server:
    try {
        xmlhttpRequest.send();
    } catch(error) {
        console.error("mapGoogle_static_displayContent_serviceRequest() - ERROR: " + error.message);
        return false;
    }
    return true;
}

/*
 * Draws static map given a (latitude, longitude) coordinate with Google Map API.
 * NOTE: 
 * The idea was to make AJAX call to the target URL, and evaluate response success or fail. If fail,
 * then handle it to call a different map API. If success, then display the map.
 * 
 * DEPRECATED (UNUSED)
 */
function mapGoogle_static_displayContent_serviceResponse(xmlhttpRequest) {
    //Check for failure:
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    if(xmlhttpRequest.status != 200) {
        console.error("mapGoogle_static_displayContent_serviceResponse() - ERROR: " 
            + xmlhttpRequest.status
            + " " + xmlhttpRequest.statusText
            + " " + xmlhttpRequest.responseText);
        return false;
    }
    
    var responseString = xmlhttpRequest.responseText;
}

//END trekcrumb-map-google.js