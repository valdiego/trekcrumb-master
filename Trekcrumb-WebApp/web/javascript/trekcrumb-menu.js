"use strict";

function menu_menuMore_Show() {
    var menuMoreList = document.getElementById('MenuMoreMenuPageContentID');
    menuMoreList.style.right = "0px";
    menuMoreList.visibility = "visible";
}

function menu_menuMore_Hide() {
    var menuMoreList = document.getElementById('MenuMoreMenuPageContentID');
    menuMoreList.style.right = "-500px";
    menuMoreList.visibility = "hidden";
}

/*
 * Determines whether to show or hide a Menu-Page section.
 * Param:
 * - pageName: Specific custom page/fragment name to differ between one fragment to another
 *             who could be on the same parent page and both have their own Menu-Page. If
 *             not provided (null), then it is assume the Menu-Page layout id is "MenuPageLayoutID".
 * - direction: Specify how Menu-Page to show up or hide. It could be either sliding in/out or fading in/out.
 *             Sliding from left to right, or from right to left, or from bottom to top, etc. 
 *             The expected values are defined in trekcrumb-constants.js
 * - marginPixelToShow: This param is used only if the direction is to slide in/out. 
 *             How many pixels the Menu-Page must slide from left/right/bottom/top margin. The value
 *             could be "0px" or "5px", etc. depending each layout padding, border, etc. 
 *             To hide, the default is set to "marginPixelToHide".
 * - displayStyle: The CSS value of how to display the Menu-Page either as "block", "inline'block" or
 *              "inline", depending each layout design. This param is used only if the direction is to fade in/out.
 * WARNING:
 * -> When show vs. hide, do NOT count on MenuPageContent's visibility since we implemented a sleep-timeout before
 *    changing its value. Doing so will cause race-condition delay when user clicking show/hide too fast. Instead,
 *    use MenuPageContent's margin/position setting.
 */
function menu_menuPage_ShowOrHide(pageName, direction, marginPixelToShow, displayStyle) {
    var marginPixelToHide = "-500px";

    if(pageName == undefined || pageName == null) {
        pageName = "";
    }

    var menuPageContent = document.getElementById(pageName + "MenuPageContentID");
    if(menuPageContent != undefined && menuPageContent != null) {
        var isShow = false;
        switch(direction) {
            case MENU_SLIDE_LEFT_TO_RIGHT:
                /* 
                 * To show: Set MenuPageContent segment to 'inline-block'. See reason below.
                 * To hide: On some browser (firefox) MenuPageShow, MenuPageHide and MenuPageContent would
                 *          sit on top of each other if we do not set MenuPageContent display to none.
                 *          To resolve this, we must set it to none (not just hidden it).
                 */
                if(menuPageContent.style.marginLeft == marginPixelToHide) {
                    //Show
                    isShow = true;
                    menuPageContent.style.display = "inline-block";
                    menuPageContent.style.visibility = "visible";
                    menuPageContent.style.marginLeft = marginPixelToShow;
                } else {
                    //Hide
                    menuPageContent.style.marginLeft = marginPixelToHide;
                    setTimeout(function() {
                        menuPageContent.style.visibility = "hidden";
                        menuPageContent.style.display = "none";
                    }, (1000));
                }
                break;
        
            case MENU_SLIDE_RIGHT_TO_LEFT:
                if(menuPageContent.style.right == marginPixelToHide) {
                    //Show
                    isShow = true;
                    menuPageContent.style.right = marginPixelToShow;
                    menuPageContent.style.visibility = "visible";
                } else {
                    //Hide
                    menuPageContent.style.right = marginPixelToHide;
                    setTimeout(function() {
                        menuPageContent.style.visibility = "hidden";
                    }, (1000));
                }
                break;

            case MENU_SLIDE_BOTTOM_TO_TOP:
                if(menuPageContent.style.marginBottom == marginPixelToHide) {
                    //Show
                    isShow = true;
                    menuPageContent.style.visibility = "visible";
                    menuPageContent.style.marginBottom = marginPixelToShow;
                    menuPageContent.scrollTop = 0;
                } else {
                    //Hide
                    menuPageContent.style.marginBottom = marginPixelToHide;
                    setTimeout(function() {
                        menuPageContent.style.visibility = "hidden";
                    }, (1000));
                }
                break;
        
            case MENU_FADE_IN_OUT:
                /* 
                 * To show: We cannot really simulate fade in effect since setting display to something 
                 *          always occurs immediately ignoring the CSS transition style.
                 * To hide: We simulate by make it disappear slowly with its CSS transition style, 
                 *          then have a timeout before setting display to 'none'.
                 */
                if(menuPageContent.style.display == "none") {
                    isShow = true;
                    menuPageContent.style.opacity = "1";
                    menuPageContent.style.display = displayStyle;
                } else {
                    menuPageContent.style.opacity = "0";
                    setTimeout(function() {
                        menuPageContent.style.display = "none";
                    }, (1000));
                }
                break;

            default:
                console.log("menu_menuPage_ShowOrHide() - ERROR: Unknown menu direction: " + direction);
                return;
        }
        
        var menuShowElement = document.getElementById(pageName + "MenuPageShowID");
        var menuHideElement = document.getElementById(pageName + "MenuPageHideID");
        if(menuShowElement != undefined && menuShowElement != null
                && menuHideElement != undefined && menuHideElement != null) {
            if(isShow) {
                menuShowElement.style.display = "none";
                menuHideElement.style.display = "inline-block";
            } else {
                menuShowElement.style.display = "inline-block";
                menuHideElement.style.display = "none";
            }
        }
    }
}

/*
 * Menu Page - Custom case for PlaceDetail.
 */
function menu_menuPage_PlaceDetailShowOrHide() {
    var pagePlaceLayout = document.getElementById('paginationPlaceLayoutID');
    var numOfTotalPlaces = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL));
    var placeCurrentIndex = parseInt(pagePlaceLayout.getAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT));
    pagePlace_updateNavigationMenu(placeCurrentIndex, numOfTotalPlaces);
    
    menu_menuPage_ShowOrHide(FRAGMENT_PLACE_DETAIL, MENU_SLIDE_BOTTOM_TO_TOP, '0px', null);
    
    /*
     * Display or not the MenuClose: 
     * -> This logic only applicable if the current fragment on display is FRAGMENT_PLACE_DETAIL
     * -> Do not count on MenuPageContent visibility since there is a sleep-timeout logic
     */
    var parentElement = document.getElementById("tripViewLayoutID");
    var parentFragmentOnDisplayCurrent = parentElement.getAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY);
    if(parentFragmentOnDisplayCurrent == FRAGMENT_PLACE_DETAIL) {
        var menuPageContent = document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuPageContentID");
        if(menuPageContent.style.marginBottom == "0px") {
            //Show:
            document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuCloseID").style.display = "block";
        } else {
            //Hide:
            document.getElementById(FRAGMENT_PLACE_DETAIL + "MenuCloseID").style.display = "none";
        }
    }
}

/*
 * Generic menu to serve any layout to show/hide its detail contents. The layout in question must 
 * have the following requirements:
 * 1. Parameter "detailLayoutID": The ID string for the content to show/hide
 * 2. The layout must have two menus to show/hide with their IDs: "menuShow" + detailLayoutID 
 *    and "menuHide" + detailLayoutID
 * 3. Optionally, the layout may have a header with ID: "header" + detailLayoutID, and the function
 *    would manipulate its style.
 */
function menu_detailLayout_showOrHide(detailLayoutID) {
    var detailLayout = document.getElementById(detailLayoutID);
    if(detailLayout == undefined || detailLayout == null) {
        console.log("menu_detailLayout_showOrHide() - ERROR: Could not find element with ID [" + detailLayoutID + "]");
    }
    var menuShow = document.getElementById("menuShow" + detailLayoutID);
    var menuHide = document.getElementById("menuHide" + detailLayoutID);
    if(menuShow == undefined || menuShow == null
            || menuHide == undefined || menuHide == null) {
        console.log("menu_detailLayout_showOrHide() - ERROR: Could not find menus to hide/show for element with ID [" + detailLayoutID + "]");
    }
    var detailHeader = document.getElementById("header" + detailLayoutID);
    
    if(detailLayout.style.display == "none") {
        //show:
        detailLayout.style.display = "block";
        menuShow.style.display = "none";
        menuHide.style.display = "inline-block";
        if(detailHeader != undefined && detailHeader != null) {
            detailHeader.style.borderBottom = "2px solid black";
        }
        
    } else {
        //hide:
        detailLayout.style.display = "none";
        menuShow.style.display = "inline-block";
        menuHide.style.display = "none";
        if(detailHeader != undefined && detailHeader != null) {
            detailHeader.style.borderBottom = "";
        }
    }
}

function menu_Clicked(element) {
    /* 
       Add a new CSS class only if it has not been previously added.
       Note:
       1. (?:^|\s) = Match the start of the string, or any single whitespace character
       2. body-clickable-clicked = The name of target CSS class
       3. (?!\S) = Negative lookahead to verify the above is the whole classname, to
                   ensure there is no non-space character following
                   (i.e. must be end of string or a space)
       4. WARNING: This won't work if the element has its "style: background-color" set since it
                   overrides any CSS attributes.
    */
    if( !element.className.match(/(?:^|\s)body-clickable-clicked(?!\S)/) ){
        element.className += " " + STYLE_LAYOUT_CLICKABLE_CLICKED_CSS_CLASS;
    }
}

function menu_ClickedReset(element) {
    /* 
       Reset by removing the CSS class.
       Note:
       1. (?:^|\s) = Match the start of the string, or any single whitespace character
       2. body-clickable-clicked = The name of target CSS class
       3. (?!\S) = Negative lookahead to verify the above is the whole classname, to
                   ensure there is no non-space character following
                   (i.e. must be end of string or a space)
    */
    element.className = element.className.replace( /(?:^|\s)body-clickable-clicked(?!\S)/g , '' );
}

function menu_selected(element) {
    /* 
       Add a new CSS class only if it has not been previously added.
       Note:
       1. (?:^|\s) = Match the start of the string, or any single whitespace character
       2. body-clickable-selected = The name of target CSS class
       3. (?!\S) = Negative lookahead to verify the above is the whole classname, to
                   ensure there is no non-space character following
                   (i.e. must be end of string or a space)
       4. WARNING: This won't work if the element has its "style: background-color" set since it
                   overrides any CSS attributes.
    */
    if( !element.className.match(/(?:^|\s)body-clickable-selected(?!\S)/) ){
        element.className += " body-clickable-selected";
    }
}

function menu_selectedReset(element) {
    /* 
       Reset by removing the CSS class.
       Note:
       1. (?:^|\s) = Match the start of the string, or any single whitespace character
       2. body-clickable-selected = The name of target CSS class
       3. (?!\S) = Negative lookahead to verify the above is the whole classname, to
                   ensure there is no non-space character following
                   (i.e. must be end of string or a space)
    */
    element.className = element.className.replace( /(?:^|\s)body-clickable-selected(?!\S)/g , '' );
}


function menu_hover_LabelShow(element) {
    var hoverTextElement = document.getElementById(element.id + 'Hover');
    if(hoverTextElement != undefined) {
        hoverTextElement.style.display = "inline";
    }
}

function menu_hover_LabelHide(element) {
    var hoverTextElement = document.getElementById(element.id + 'Hover');
    if(hoverTextElement != undefined) {
        hoverTextElement.style.display = "none";
    }
}

function menu_hover_ElementActive(element) {
    var currentClassValues = element.className;

    if(currentClassValues == null || currentClassValues == "") {
        element.className = STYLE_ELEMENT_ACTIVE_CSS_CLASS;
    } else if(currentClassValues.indexOf(STYLE_ELEMENT_ACTIVE_CSS_CLASS) == -1) {
        element.className += " " + STYLE_ELEMENT_ACTIVE_CSS_CLASS;
    }
}

function menu_hover_ElementInactive(element) {
    var currentClassValues = element.className;
    
    if(currentClassValues == null || currentClassValues == "") {
        return;
    } else if (currentClassValues == STYLE_ELEMENT_ACTIVE_CSS_CLASS) {
        element.className = "";
        return;
    }
 
    var currentClassValuesArray = currentClassValues.split(" ");
    var modifiedClassValuesArray = [];
    var size = currentClassValuesArray.length;
    for (var i = 0 ; i < size; i++) {
        if(currentClassValuesArray[i] != STYLE_ELEMENT_ACTIVE_CSS_CLASS) {
            modifiedClassValuesArray.push(currentClassValuesArray[i]);
        }
    }
    element.className = modifiedClassValuesArray.join(" ");
}

function menu_disable(element) {
    if( !element.className.match(/(?:^|\s)menu-disabled(?!\S)/) ){
        element.className += " menu-disabled";
    }
    element.style.cursor = "not-allowed";
    element.setAttribute("onclick", "");
}

function menu_enable(element, onclickFunction) {
    element.className = element.className.replace( /(?:^|\s)menu-disabled(?!\S)/g , '' );
    element.style.cursor = "pointer";
    element.setAttribute("onclick", onclickFunction);
}

function menu_close_show(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }

    var closeMenu = document.getElementById(pageName + "menuCloseID");
    if(closeMenu != undefined && closeMenu != null) {
        closeMenu.style.display = "inline";
    }
}

function menu_close_hide(pageName) {
    if(pageName == undefined || pageName == null) {
        pageName = "";
    }
    var closeMenu = document.getElementById(pageName + "menuCloseID");
    if(closeMenu != undefined && closeMenu != null) {
        closeMenu.style.display = "none";
    }
}

function menu_openNewTab(targetURL) {
    var win = window.open(targetURL, '_blank');
    //win.focus();
}

function menu_dropdown_showOrHide(dropdownID) {
    var dropdownElement = document.getElementById(dropdownID);
    if(dropdownElement != undefined) {
        if(dropdownElement.style.display == "none") {
            dropdownElement.style.display = "block";
        } else {
            dropdownElement.style.display = "none";
        }
    }
}

//END trekcrumb-menu.js