"use strict";

function validate_isStringEmpty(stringValue) {
    //1. Check if NULL
    if(stringValue == undefined || stringValue == null) {
        return true;
    }
    
    //2. Check if empty String - WARNING: Not all browser supports trim()
    if(stringValue.trim()) {
        stringValue = stringValue.trim();
    } else {
        stringValue = stringValue.replace(/^\s+/,'');
    }
    if(stringValue.length == 0) {
        return true;
    }
    
    //3. Check if value is 'null'
    if(stringValue.toLowerCase() == "null") {
        return null;
    }
    
    return false;
}

function validate_InputVsReenterInput(inputID, reenterElement, isCaseSensitive) {
    var inputElement = document.getElementById(inputID);
    if(inputElement == undefined || inputElement == null) {
        return false;
    }
    if(reenterElement == undefined || reenterElement == null) {
        common_errorOrInfoMessage_show([ERROR_MSG_PARAM_INVALID_REENTER_VALUE ], null);
        return false;
    }
    
    var inputElementValue = inputElement.value;
    var reenterElementValue = reenterElement.value;
    if(!isCaseSensitive) {
        inputElementValue = inputElementValue.toLowerCase();
        reenterElementValue = reenterElementValue.toLowerCase();
    }

    if(inputElementValue != reenterElementValue) {
        reenterElement.className += " body-error";
        document.getElementById(reenterElement.id + "Error").style.display = "block";
        return false;
    } else {
        //Success:
        reenterElement.className = reenterElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
        common_errorOrInfoMessage_hide();
        document.getElementById(reenterElement.id + "Error").style.display = "none";
        return true;
    }
}

function validate_username(usernameInputID) {
    var usernameInputElement = document.getElementById(usernameInputID);
    if(usernameInputElement == undefined || usernameInputElement == null) {
        return ERROR_MSG_PARAM_INVALID_USERNAME;
    }
    
    var usernameInputValue = usernameInputElement.value;
    if(validate_isStringEmpty(usernameInputValue)) {
        usernameInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_USERNAME;
    }
    
    if(usernameInputValue.trim()) {
        usernameInputValue = usernameInputValue.trim();
    } else {
        usernameInputValue = usernameInputValue.replace(/^\s+/,'');
    }
    if(usernameInputValue.length < 6 || usernameInputValue.length > 25) {
        usernameInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_USERNAME;
        
    } else if(REGEX_INVALID_CHARS.test(usernameInputValue)) {
        usernameInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_USERNAME;  
    }
    
    //Success:
    usernameInputElement.value = usernameInputValue;
    usernameInputElement.className = usernameInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    return null;
}

function validate_email(emailInputID, isValidateReenterInput, emailInputReenterID) {
    var emailInputElement = document.getElementById(emailInputID);
    if(emailInputElement == undefined || emailInputElement == null) {
        return ERROR_MSG_PARAM_INVALID_EMAIL;
    }
    
    var emailInputValue = emailInputElement.value;
    if(validate_isStringEmpty(emailInputValue)) {
        emailInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_EMAIL;
    }
    
    if(emailInputValue.trim()) {
        emailInputValue = emailInputValue.trim();
    } else {
        emailInputValue = emailInputValue.replace(/^\s+/,'');
    }
    if(!REGEX_VALID_EMAIL_RFC0822.test(emailInputValue)) {
        emailInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_EMAIL;
    }
    /*
    if(emailInputValue.indexOf('@') <= 0) {
        emailInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_EMAIL;
    }
    if(emailInputValue.indexOf('.') < 0
            || emailInputValue.indexOf('.') < emailInputValue.indexOf('@')) {
        emailInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_EMAIL;
    }
    */
    
    if(isValidateReenterInput) {
        var emailInputReenterElement = document.getElementById(emailInputReenterID);
        if(!validate_InputVsReenterInput(emailInputID, emailInputReenterElement, false)) {
            emailInputElement.className += " body-error";
            return ERROR_MSG_PARAM_INVALID_REENTER_VALUE ;  
        }
    }
    
    //Success:
    emailInputElement.className = emailInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    return null;
}

function validate_password(pwdInputID, isValidateReenterInput, pwdInputReenterID) {
    var pwdInputElement = document.getElementById(pwdInputID);
    if(pwdInputElement == undefined || pwdInputElement == null) {
        return ERROR_MSG_PARAM_INVALID_PASSWORD;
    }
    
    var pwdInputValue = pwdInputElement.value;
    if(validate_isStringEmpty(pwdInputValue)) {
        pwdInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_PASSWORD;
    }

    if(pwdInputValue.trim()) {
        pwdInputValue = pwdInputValue.trim();
    } else {
        pwdInputValue = pwdInputValue.replace(/^\s+/,'');
    }
    if(pwdInputValue.length < 6 || pwdInputValue.length > 25) {
        pwdInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_PASSWORD;
    }
    if(isValidateReenterInput) {
        var pwdInputReenterElement = document.getElementById(pwdInputReenterID);
        if(!validate_InputVsReenterInput(pwdInputID, pwdInputReenterElement, true)) {
            pwdInputElement.className += " body-error";
            return ERROR_MSG_PARAM_INVALID_REENTER_VALUE ;  
        }
    }
    
    //Success:
    pwdInputElement.className = pwdInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    return null;
}

function validate_fullname(fullnameInputID) {
    var fullnameInputElement = document.getElementById(fullnameInputID);
    if(fullnameInputElement == undefined || fullnameInputElement == null) {
        return ERROR_MSG_PARAM_INVALID_FULLNAME;
    }
    
    var fullnameInputValue = fullnameInputElement.value;
    if(validate_isStringEmpty(fullnameInputValue)) {
        fullnameInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_FULLNAME;
    }

    //Success:
    fullnameInputElement.className = fullnameInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    return null;
}

function validate_location(locationInputID) {
    var locationInputElement = document.getElementById(locationInputID);
    if(locationInputElement == undefined || locationInputElement == null) {
        return ERROR_MSG_PARAM_INVALID_LOCATION;
    }
    
    var locationInputValue = locationInputElement.value;
    if(validate_isStringEmpty(locationInputValue)) {
        locationInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_LOCATION;
    }

    //Success:
    locationInputElement.className = locationInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    return null;
}

function validate_tripName(tripNameInputID) {
    var tripNameInputElement = document.getElementById(tripNameInputID);
    if(tripNameInputElement == undefined || tripNameInputElement == null) {
        return ERROR_MSG_PARAM_INVALID_TRIPNAME;
    }
    
    var tripNameInputValue = tripNameInputElement.value;
    if(validate_isStringEmpty(tripNameInputValue)) {
        tripNameInputElement.className += " body-error";
        return ERROR_MSG_PARAM_INVALID_TRIPNAME;
    }
    
    //Success:
    tripNameInputElement.className = tripNameInputElement.className.replace( /(?:^|\s)body-error(?!\S)/g , '' );
    return null;
}

function validate_inputImageFile() {
    var imagePreviewElement = document.getElementById("imagePreviewID");
    if(imagePreviewElement == undefined || imagePreviewElement == null
            || imagePreviewElement.getAttribute('src') == undefined || imagePreviewElement.getAttribute('src') == null 
            || imagePreviewElement.getAttribute('src') == "") {
        return ERROR_MSG_PICTURE_CREATE_IMAGE_EMPTY;
    }

    /* 
     * Get the file image data
     * Note:
     * 1. JS FileReader.readAsDataURL() transforms file content into Base64 Encoded-String data
     * 2. JS Base64 Encoded-String file data come with metadata such as 'data:image/png;base64,xyz' 
     *    or 'data:image/jpg;base64,xyz', etc. The 'xyz' is the encoded content of the image body.
     */
    var fileDataBase64String = imagePreviewElement.getAttribute('src');

    /*
     * TODO (8/2016): Image format not supported
     * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
     * with error: "Invalid argument to native writeImage". Try to validate up front on client
     * before we send to the back-end server
     */
     var indexUnsupportedFormat = fileDataBase64String.indexOf("image/gif");
     if(indexUnsupportedFormat >= 0) {
        return ERROR_MSG_PICTURE_CREATE_IMAGE_WRITE_ERROR;
     }

    /* 
     * Get only the encoded content ('xyz') from file image data and clean up JS decode header
     * Note:
     * 1. From the JS Base64 Encoded-String file data, what we need to send is the encoded content 'xyz';
     *    therefore we remove the metadata before we send.
     * 2. Trim the JS Base64 Encoded-String file data with regular expression is to find 'base64,' 
     *    and all the contents after the match. With modifier 'm', it returns as 2 results as an array. 
     *    We select the 2nd result (index = 1).
     */
    fileDataBase64String = fileDataBase64String.match(/base64,(.*)/mi)[1];

    var imageBase64Data = document.getElementById("imageBase64DataID");
    if(imageBase64Data == undefined || imageBase64Data == null) {
        return "Missing required element to submit: imageBase64DataID!";
    }
    
    //Success:
    imageBase64Data.value = fileDataBase64String;
    return null;
}

function validate_isUserLogin() {
    var userLoginID = document.getElementById('userID');
    if(userLoginID != undefined 
            && userLoginID != null
            && !validate_isStringEmpty(userLoginID.value)) {
        return true;
    }
    
    return false;

}

//END trekcrumb-validation.js