"use strict";

function favorite_retrieveByTrip_view() {
    //1. Force to open and populate:
    document.getElementById('favoriteByTripContentID').style.display = "none";
    favorite_retrieveByTrip_init();
    
    //2. Scroll there:
    var targetTopPosition = document.getElementById('favoriteByTripLayoutID').offsetTop;
    jQuery("#tripDetailLayoutID").stop(true, true).animate(
            {scrollTop: targetTopPosition}, 
            1500, 
            "easeInOutExpo");
}

function favorite_retrieveByTrip_init() {
    menu_detailLayout_showOrHide('favoriteByTripContentID');
    
    var favoriteByTripContent = document.getElementById('favoriteByTripContentID');
    if(favoriteByTripContent.style.display == "block") {
        //1. Check if Trip numOfFavorite is ZERO:
        var favoriteByTripLayout = document.getElementById('favoriteByTripLayoutID');
        var numOfFavoritesTotal = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL));
        if(numOfFavoritesTotal == 0) {
            document.getElementById('favoriteByTripGreetingID').style.display = "none";
            favoriteByTripContent.innerHTML = INFO_MSG_FAVORITE_EMPTY;
            return;
        }

        //2. Else - Check if favorite initial contents may have already dowloaded:
        if(!validate_isStringEmpty(favoriteByTripContent.innerHTML)) {
            console.log("favorite_retrieveByTrip_init() - Initial contents already dowloaded!");
            document.getElementById('favoriteByTripGreetingID').style.display = "block";
            common_progressBarWidget_hide(FRAGMENT_TRIP_FAVORITE);
            favorite_retrieveByTrip_paginationUpdate(false);
            return;
        }

        //3. Else - Download initial content:
        document.getElementById('favoriteByTripGreetingID').style.display = "block";
        favorite_retrieveByTrip_serviceRequest();
    
    } else {
        //Hide any content
        common_progressBarWidget_hide(FRAGMENT_TRIP_FAVORITE);
        document.getElementById('favoriteByTripGreetingID').style.display = "none";
        document.getElementById('favoriteByTripMenuMoreNextID').style.display = "none";
    }
}

function favorite_retrieveByTrip_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        common_progressBarWidget_show(FRAGMENT_TRIP_FAVORITE);
        document.getElementById('favoriteByTripMenuMoreNextID').style.display = "none";
    
        //Compose request:
        var tripID = document.getElementById('tripID').value;
        var favoriteByTripLayout = document.getElementById('favoriteByTripLayoutID');
        var offsetCurrent = favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT);
        var tripWithFavorites = {
            id: tripID
        };

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                favorite_retrieveByTrip_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            favorite_retrieveByTrip_errorHandling(xmlhttpRequest.statusText + " - " + xmlhttpRequest.statusText);
        };
        var urlTarget = APP_ROOT_URI + "/trekcrumb/favorite/retrieveByTrip/AJAX/" + offsetCurrent;
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(tripWithFavorites));
        } catch(error) {
            favorite_retrieveByTrip_errorHandling(error.message);
        }
    }
}

function favorite_retrieveByTrip_errorHandling(errorMessage) {
    common_AJAX_opsError_show(FRAGMENT_TRIP_FAVORITE, "Favorite Retrieve by Trip Error: " + errorMessage, errorMessage);
    favorite_retrieveByTrip_paginationUpdate(false);
}

function favorite_retrieveByTrip_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return false;
    }
    
    //It is async:
    var favoriteByTripContent = document.getElementById('favoriteByTripContentID');
    if(favoriteByTripContent == undefined || favoriteByTripContent == null) {
        console.error("favorite_retrieveByTrip_serviceResponse() - ERROR: Cannot find favoriteByTripContentID on current view!");
        return;
    }
    
    common_progressBarWidget_hide(FRAGMENT_TRIP_FAVORITE);

    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(!isSuccess) {
        favorite_retrieveByTrip_paginationUpdate(false);
        return;    
    }
    
    var listOfUsers = JSON.parse(xmlhttpRequest.responseText).listOfUsers;
    if(listOfUsers == undefined || listOfUsers == null || listOfUsers.length == 0) {
        favoriteByTripContent.innerHTML = ERROR_NO_DATA;
        favorite_retrieveByTrip_paginationUpdate(false);
        return;
    }
        
    favorite_retrieveByTrip_displayRows(listOfUsers);
    favorite_retrieveByTrip_paginationUpdate(true);
}

function favorite_retrieveByTrip_displayRows(listOfUsers) {
    var favoriteByTripContent = document.getElementById("favoriteByTripContentID");
    var favoriteByTripRowTemplate = document.getElementById("favoriteByTripRowTemplateID");
    if(favoriteByTripContent == undefined || favoriteByTripContent == null
            || favoriteByTripRowTemplate == undefined || favoriteByTripRowTemplate == null) {
        console.error("favorite_retrieveByTrip_displayRows() - ERROR: Missing required layouts in current view!");
        return;
    }
    
    //Else: Append the new rows to any existing content
    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    var listSize = listOfUsers.length;
    for(var index = 0; index < listSize; index++) {
        var user = listOfUsers[index];
        var username = user.username;

        //Clone row template and populate its unique data:
        var rowCurrent = favoriteByTripRowTemplate.cloneNode(true);
        rowCurrent.id = username;
        rowCurrent.href = "javascript:user_view_Retrieve(false, '" + username + "');";
        rowCurrent.querySelector("#hoverID").id = username + "Hover";

        //User details:
        rowCurrent.querySelector("#userFullnameID").innerHTML = user.fullname;
        rowCurrent.querySelector("#usernameID").innerHTML = user.username;
        if(validate_isStringEmpty(user.profileImageName)) {
            rowCurrent.querySelector("#userImageDefaultID").style.display = "block";
            rowCurrent.querySelector("#userImageID").style.display = "none";
        } else {
            rowCurrent.querySelector("#userImageDefaultID").style.display = "none";
            var userImageElement = rowCurrent.querySelector("#userImageID");
            userImageElement.setAttribute("src", FILE_SERVER_URL_GETIMAGE_PROFILE  + user.profileImageName);
            userImageElement.style.display = "block";
        }

        //Finally, display it:
        rowCurrent.style.display = 'inline-block';
        favoriteByTripContent.appendChild(rowCurrent);
    }
}

function favorite_retrieveByTrip_paginationUpdate(isUpdate) {
    var favoriteByTripLayout = document.getElementById('favoriteByTripLayoutID');
    var numRecordsTotal = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL));
    var numOfRowsMax = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_ROWS_MAXIMUM));
    var offsetCurrent = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT));
    
    if(isUpdate) {
        offsetCurrent = offsetCurrent + numOfRowsMax;
        favoriteByTripLayout.setAttribute(VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT, offsetCurrent);
    }
    
    /*
     * Show menu 'More' or not. 
     * If we are to show the menu, we unfortunately need to validate if the 'favoriteByTripContentID'
     * is on display or hidden. Do not show the menu if the favorites are hidden.
     */
    if(offsetCurrent < numRecordsTotal) {
        //There's more:
        var favoriteByTripContent = document.getElementById('favoriteByTripContentID');
        if(favoriteByTripContent.style.display == "block") {
            document.getElementById('favoriteByTripMenuMoreNextID').style.display = "block";
        } else {
            document.getElementById('favoriteByTripMenuMoreNextID').style.display = "none";
        }
    } else {
        //No more favorites available:
        document.getElementById('favoriteByTripMenuMoreNextID').style.display = "none";
    }
}

function favorite_retrieveByTrip_next() {
    var favoriteByTripLayout = document.getElementById('favoriteByTripLayoutID');
    var numRecordsTotal = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL));
    var offsetCurrent = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT));
    if(offsetCurrent < numRecordsTotal) {
        favorite_retrieveByTrip_serviceRequest();
    } else {
        console.error("favorite_retrieveByTrip_next() - ERROR: There is NO MORE trek favorites to retrieve!");
        document.getElementById('favoriteByTripMenuMoreNextID').style.display = "none";
    }
}

function favorite_retrieveByUser_init() {
    //Clear any cached data:
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserFavorites");
}

/*
 * User Favorites starting point. 
 * -> It would try to recover any last current state params and result list 
 *    when available (cached in session storage). 
 * -> This is to support feature when user navigates away from current UserFavorites page, 
 *    but then click browser "BACK" button such that we avoid to re-init the list service call.
 */
function favorite_retrieveByUser_current() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get current state params if available:
        var offsetCurrent = sessionStorage.getItem(CACHE_KEY_TRIP_USERFAVORITES_OFFSET_CURRENT);
        var pageCurrent = sessionStorage.getItem(CACHE_KEY_TRIP_USERFAVORITES_PAGE_CURRENT);
        var numOfRecordsTotal = sessionStorage.getItem(CACHE_KEY_TRIP_USERFAVORITES_TOTAL_RECORDS);
        if(offsetCurrent != undefined && offsetCurrent != null 
                && pageCurrent != undefined && pageCurrent != null
                && numOfRecordsTotal != undefined && numOfRecordsTotal != null) {
            //Set these params onto the page:
            pageList_update(FRAGMENT_TRIP_LIST_USER_FAVORITES, 
                            parseInt(offsetCurrent), parseInt(pageCurrent), parseInt(numOfRecordsTotal));
            
            //Caching: Get the last result List if available:
            var username = document.getElementById('userFavoritesLayoutID').getAttribute(VARATTR_USER_USERNAME);
            var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_USERFAVORITES_RESULT_PAGE + username + pageCurrent);
            if(listOfTrips != undefined && listOfTrips != null) {
                common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_USER_FAVORITES);
                trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST_USER_FAVORITES, JSON.parse(listOfTrips), -1, false);
            } else {
                favorite_retrieveByUser_serviceRequest();
            }
            return;
        }
    }
    
    //No caching available. let's start from clean states:
    pageList_init(FRAGMENT_TRIP_LIST_USER_FAVORITES);
    favorite_retrieveByUser_serviceRequest();
}

function favorite_retrieveByUser_next() {
    if(pageList_next(FRAGMENT_TRIP_LIST_USER_FAVORITES)) {
        document.getElementById(FRAGMENT_TRIP_LIST_USER_FAVORITES + "tripListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_TRIP_LIST_USER_FAVORITES);
    
        favorite_retrieveByUser_retrieveList();
    }
}

function favorite_retrieveByUser_previous() {
    if(pageList_previous(FRAGMENT_TRIP_LIST_USER_FAVORITES)) {
        document.getElementById(FRAGMENT_TRIP_LIST_USER_FAVORITES + "tripListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_TRIP_LIST_USER_FAVORITES);

        favorite_retrieveByUser_retrieveList();
    }
}

function favorite_retrieveByUser_retrieveList() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Update current pagination params into cache:
        common_localeStorage_cacheCurrentListState(
                FRAGMENT_TRIP_LIST_USER_FAVORITES,
                CACHE_KEY_TRIP_USERFAVORITES_OFFSET_CURRENT,
                CACHE_KEY_TRIP_USERFAVORITES_PAGE_CURRENT,
                CACHE_KEY_TRIP_USERFAVORITES_TOTAL_RECORDS);
                
        //Caching: Get the last result List if available:
        var username = document.getElementById('userFavoritesLayoutID').getAttribute(VARATTR_USER_USERNAME);
        var pageCurrent = document.getElementById(FRAGMENT_TRIP_LIST_USER_FAVORITES + 'paginationListLayoutID').getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
        var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_USERFAVORITES_RESULT_PAGE + username + pageCurrent);
        if(listOfTrips != undefined && listOfTrips != null) {
            common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_USER_FAVORITES);
            trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST_USER_FAVORITES, JSON.parse(listOfTrips), -1, false);
        } else {
            favorite_retrieveByUser_serviceRequest();
        }
    } else {
        favorite_retrieveByUser_serviceRequest();
    }
}

function favorite_retrieveByUser_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        var xmlhttpRequest = new XMLHttpRequest();
        
        //Prepare response handling:
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                favorite_retrieveByUser_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST_USER_FAVORITES, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        //Get params:
        var username = document.getElementById('userFavoritesLayoutID').getAttribute(VARATTR_USER_USERNAME);
        var offsetCurrent = document.getElementById(FRAGMENT_TRIP_LIST_USER_FAVORITES + 'paginationListLayoutID').getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT);

        //Compose request:
        var userWithFavorites = {
            username: username
        };
        
        //Prepare send (as JSON request):
        var urlTarget = APP_ROOT_URI + "/trekcrumb/favorite/retrieveByUser/AJAX/" + offsetCurrent;
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(userWithFavorites));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST_USER_FAVORITES, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function favorite_retrieveByUser_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_USER_FAVORITES);

    if(xmlhttpRequest.status != 200) {
        var errorMessageArray = [xmlhttpRequest.statusText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        var errorMessageArray = [serverResponseObj.serviceError.errorText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    
    //Success
    var listOfTrips = serverResponseObj.listOfTrips;
    trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST_USER_FAVORITES, listOfTrips, -1, false);
    
    //Caching: Save list results
    var username = document.getElementById('userFavoritesLayoutID').getAttribute(VARATTR_USER_USERNAME);
    var pageCurrent = document.getElementById(FRAGMENT_TRIP_LIST_USER_FAVORITES + 'paginationListLayoutID').getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_TRIP_USERFAVORITES_RESULT_PAGE + username + pageCurrent, 
            JSON.stringify(listOfTrips)); 
}

function favorite_create() {
    //Check validity:
    var userIDInputElement = document.getElementById('userID');
    if(userIDInputElement == undefined || userIDInputElement == null) {
        //Redirect to login page:
        window.location.href = WEBAPP_URL_USER_SIGNUP;
        return;
    }

    //Else:
    document.getElementById('menuFavoriteAddID').style.visibility = "hidden";
    favorite_create_serviceRequest();
}

function favorite_create_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var userID = document.getElementById('userID').value;
        var tripID = document.getElementById('tripID').value;
        var favoriteToCreate = {
            userID: userID,
            tripID: tripID
        };

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                favorite_create_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            favorite_createOrDelete_errorHandling(
                    xmlhttpRequest.statusText + " - " + xmlhttpRequest.statusText, 
                    true);
        };
        var urlTarget = APP_ROOT_URI + "/trekcrumb/favorite/create/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(favoriteToCreate));
        } catch(error) {
            favorite_createOrDelete_errorHandling(error.message, true);
        }
    }
}

function favorite_create_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return false;
    }
    
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(isSuccess) {
        document.getElementById('menuFavoriteAddID').style.display = "none";
        
        var menuFavoriteDelete = document.getElementById('menuFavoriteDeleteID');
        menuFavoriteDelete.style.display = "inline-block";
        menuFavoriteDelete.style.visibility = "visible";
        
        var favoriteByTripLayout = document.getElementById('favoriteByTripLayoutID');
        var numOfFavoritesTotal = favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL);
        var numFavoritesNew = parseInt(numOfFavoritesTotal) + 1;
        favoriteByTripLayout.setAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL, numFavoritesNew);
        document.getElementById('favoriteNumberID').innerHTML = numFavoritesNew;
        document.getElementById('favoriteByTripHeaderNumRecordsTotalID').innerHTML = numFavoritesNew;
        
        favoriteByTripLayout.setAttribute(VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT, "0");

        //Adjust favorite icon color and symbol:
        var favoriteIcon = document.getElementById('favoriteIconID');
        favoriteIcon.className = favoriteIcon.className.replace( /(?:^|)font-color(\S+)/g , 'font-color-highlight' );
        
        var favoriteIconSymbol = document.getElementById('favoriteIconSymbolID');
        favoriteIconSymbol.className = favoriteIconSymbol.className.replace('fa-heart-o' , 'fa-heart' );
        
        document.getElementById('favoriteByTripContentID').innerHTML = "";
        favorite_retrieveByTrip_view();
        
        common_errorOrInfoMessage_show(null, INFO_MSG_FAVORITE_CREATE_SUCCESS);
    }
}

function favorite_delete() {
    document.getElementById('menuFavoriteDeleteID').style.visibility = "hidden";
    favorite_delete_serviceRequest();
}

function favorite_delete_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var userID = document.getElementById('userID').value;
        var tripID = document.getElementById('tripID').value;
        var favoriteToDelete = {
            userID: userID,
            tripID: tripID
        };

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                favorite_delete_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            favorite_createOrDelete_errorHandling(
                    xmlhttpRequest.statusText + " - " + xmlhttpRequest.statusText, 
                    false);
        };
        var urlTarget = APP_ROOT_URI + "/trekcrumb/favorite/delete/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(favoriteToDelete));
        } catch(error) {
            favorite_createOrDelete_errorHandling(error.message, false);
        }
    }
}

function favorite_delete_serviceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return false;
    }
    
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(isSuccess) {
        document.getElementById('menuFavoriteDeleteID').style.display = "none";

        var menuFavoriteAdd = document.getElementById('menuFavoriteAddID');
        menuFavoriteAdd.style.display = "inline-block";
        menuFavoriteAdd.style.visibility = "visible";
        
        var favoriteByTripLayout = document.getElementById('favoriteByTripLayoutID');
        var numOfFavoritesTotal = parseInt(favoriteByTripLayout.getAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL));
        var numFavoritesNew = 0;
        if(numOfFavoritesTotal > 0) {
            //Prevents invalid minus value
            numFavoritesNew = numOfFavoritesTotal - 1;
            favoriteByTripLayout.setAttribute(VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL, numFavoritesNew);
            document.getElementById('favoriteNumberID').innerHTML = numFavoritesNew;
            document.getElementById('favoriteByTripHeaderNumRecordsTotalID').innerHTML = numFavoritesNew;
        }
        favoriteByTripLayout.setAttribute(VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT, "0");

        //Adjust icon color and symbol:
        var favoriteIcon = document.getElementById('favoriteIconID');
        var favoriteIconSymbol = document.getElementById('favoriteIconSymbolID');
        var newColor = "";
        var newSymbol = "";
        if(numFavoritesNew > 0) {
            newColor = "font-color-menu";
            newSymbol = "fa-heart";
        } else {
            newColor = "font-color-passive";
            newSymbol = "fa-heart-o";
        }
        favoriteIcon.className = favoriteIcon.className.replace( /(?:^|)font-color(\S+)/g , newColor);
        favoriteIconSymbol.className = favoriteIconSymbol.className.replace( /fa-heart/g , newSymbol);
        
        document.getElementById('favoriteByTripContentID').innerHTML = "";
        favorite_retrieveByTrip_view();
        
        common_errorOrInfoMessage_show(null, INFO_MSG_FAVORITE_DELETE_SUCCESS);
    }
}

function favorite_createOrDelete_errorHandling(errorMessage, isCreate) {
    common_AJAX_opsError_show(null, "Favorite Create/Delete Error: " + errorMessage, errorMessage);
    
    if(isCreate) {
        document.getElementById('menuFavoriteAddID').style.visibility = "visible";
        document.getElementById('menuFavoriteAddID').style.display = "inline-block";
        
    } else {
        //Delete:
        document.getElementById('menuFavoriteDeleteID').style.visibility = "visible";
        document.getElementById('menuFavoriteDeleteID').style.display = "inline-block";
    }
}


//END trekcrumb-favorite.js