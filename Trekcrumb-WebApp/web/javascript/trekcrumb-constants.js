//Misc
var REGEX_INVALID_CHARS = new RegExp("[^a-zA-Z0-9]");
var REGEX_VALID_EMAIL_RFC0822 = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
var DATE_CREATEDDATE_UI_FORMAT = "D MMM YYYY [at] h:mm A";
var DATE_CREATEDDATE_UI_FORMAT_WO_HOUR = "D MMM YYYY";
var DATE_TIMESTAMP_LOCALE = new Date().toString().match(/\(([A-Za-z].*)\)/)[0];  //Date().toString() produces: 'Mon Sep 28 2015 13:40:35 GMT-0700 (Pacific Daylight Time)'
var INFOMSG_TIMEOUT_VALUE = 3000;  //milliseconds.

//Query (URL) parameters
var QUERYPARAM_NAME_PLACE_INDEX = "place";
var QUERYPARAM_NAME_PICTURE_INDEX = "picture";
var QUERYPARAM_NAME_PICTURE_ID = "pictureid";
var QUERYPARAM_NAME_SECTION_SELECTED = "section";
var QUERYPARAM_NAME_REMEMBER_ME = "rememberme";
var QUERYPARAM_NAME_DEVICE_IDENTITY = "devid";

//Device
/* 
 * The following max. width dimensions follow Bootstrap theme framework's.
 * Extra-small/small = phone, medium = tablet, large = laptop/desktop/external monitor. 
 */
var DEVICE_ORENTATION_POTRAIT = "DEVICE_ORENTATION_POTRAIT";
var DEVICE_ORENTATION_LANDSCAPE = "DEVICE_ORENTATION_LANDSCAPE";
var DEVICE_SCREEN_WIDTH_MAX_EXTRA_SMALL = 400;
var DEVICE_SCREEN_WIDTH_MAX_SMALL = 768;
var DEVICE_SCREEN_WIDTH_MAX_MEDIUM = 992;
var DEVICE_SCREEN_WIDTH_MIN_LARGE = 1200;
var DEVICE_SCREEN_HEIGHT_MAX_SMALL = 320;
var DEVICE_SCREEN_HEIGHT_MAX_MEDIUM = 400;
var DEVICE_SCREEN_HEIGHT_MAX_LARGE = 768;

//Fragments
var FRAGMENT_TRIP_LIST = "FRAGMENT_TRIP_LIST";
var FRAGMENT_TRIP_LIST_HOME = "FRAGMENT_TRIP_LIST_HOME";
var FRAGMENT_TRIP_LIST_USER_TRIPS = "FRAGMENT_TRIP_LIST_USER_TRIPS";
var FRAGMENT_TRIP_LIST_USER_FAVORITES = "FRAGMENT_TRIP_LIST_USER_FAVORITES";
var FRAGMENT_TRIP_DETAIL = "FRAGMENT_TRIP_DETAIL";
var FRAGMENT_TRIP_CREATE = "FRAGMENT_TRIP_CREATE";
var FRAGMENT_TRIP_UPDATE = "FRAGMENT_TRIP_UPDATE";
var FRAGMENT_TRIP_FAVORITE = "FRAGMENT_TRIP_FAVORITE";
var FRAGMENT_PICTURE_LIST = "FRAGMENT_PICTURE_LIST";
var FRAGMENT_PICTURE_LIST_USER_PICTURES = "FRAGMENT_PICTURE_LIST_USER_PICTURES";
var FRAGMENT_PICTURE_DETAIL = "FRAGMENT_PICTURE_DETAIL";
var FRAGMENT_PICTURE_SLIDESHOW = "FRAGMENT_PICTURE_SLIDESHOW";
var FRAGMENT_PICTURE_COVER = "FRAGMENT_PICTURE_COVER";
var FRAGMENT_PICTURE_GALLERY = "FRAGMENT_PICTURE_GALLERY";
var FRAGMENT_PICTURE_CREATE = "FRAGMENT_PICTURE_CREATE";
var FRAGMENT_PICTURE_UPDATE = "FRAGMENT_PICTURE_UPDATE";
var FRAGMENT_PLACE_LIST_HOME = "FRAGMENT_PLACE_LIST_HOME";
var FRAGMENT_PLACE_DETAIL = "FRAGMENT_PLACE_DETAIL";
var FRAGMENT_PLACE_CREATE = "FRAGMENT_PLACE_CREATE";
var FRAGMENT_PLACE_UPDATE = "FRAGMENT_PLACE_UPDATE";
var FRAGMENT_GEOLOC_CURRENTLOCATION = "FRAGMENT_GEOLOC_CURRENTLOCATION";
var FRAGMENT_USER_LOGIN = "userLogin";
var FRAGMENT_USER_CREATE = "userCreate";
var FRAGMENT_USER_FORGET = "userForget";
var FRAGMENT_USER_RESET = "userReset";
var FRAGMENT_USER_DETAIL = "FRAGMENT_USER_DETAIL";
var FRAGMENT_USER_LOGIN_REFRESH = "FRAGMENT_USER_LOGIN_REFRESH";
var FRAGMENT_USER_UPDATE = "FRAGMENT_USER_UPDATE";
var FRAGMENT_USER_UPDATE_PROFILE = "profile";
var FRAGMENT_USER_UPDATE_PROFILEIMAGE = "profileImage";
var FRAGMENT_USER_UPDATE_ACCOUNT = "account";
var FRAGMENT_USER_UPDATE_ACCOUNT_EMAIL = "accountEmail";
var FRAGMENT_USER_UPDATE_ACCOUNT_PWD = "accountPwd";
var FRAGMENT_USER_UPDATE_DEACTIVATE = "deactivate";
var FRAGMENT_USER_CONTACTUS = "contactus";
var FRAGMENT_COMMENT_LIST_TRIP_COMMENTS = "FRAGMENT_COMMENT_LIST_TRIP_COMMENTS";
var FRAGMENT_COMMENT_LIST_USER_COMMENTS = "FRAGMENT_COMMENT_LIST_USER_COMMENTS";
var FRAGMENT_COMMENT_CREATE = "FRAGMENT_COMMENT_CREATE";
var FRAGMENT_COMMENT_DELETE = "FRAGMENT_COMMENT_DELETE";
var FRAGMENT_MAP_DYNAMIC_DETAILCONTENT = "FRAGMENT_MAP_DYNAMIC_DETAILCONTENT";
var FRAGMENT_MAP_DYNAMIC_CURRENTLOC = "FRAGMENT_MAP_DYNAMIC_CURRENTLOC";

//Images
var IMAGE_BYTES_TO_IMAGE_BASE64 = "data:image/jpeg;base64,";
//var IMAGE_BYTES_TO_IMAGE_BASE64 = "data:image/png;base64,";

//Cookies
var COOKIE_NAME_DEFAULT_SESSION = "JSESSIONID";
var COOKIE_NAME_USER_ID = "UID";
var COOKIE_NAME_USER_AUTH_TOKEN = "UAUTH";
var COOKIE_NAME_USER_AUTH_TOKEN_ID = "UAUTHID";
var COOKIE_EXPIRATION_TIME_MS = 1209600000; //In milliseconds; equals to: 14 days * (24 * 60 * 60 * 1000)

//Caching
var CACHE_TYPE_LOCALESTORAGE_LOCAL = "CACHE_TYPE_LOCALESTORAGE_LOCAL";
var CACHE_TYPE_LOCALESTORAGE_SESSION = "CACHE_TYPE_LOCALESTORAGE_SESSION";
var CACHE_KEY_USER_VIEW_FRAGMENT_ON_DISPLAY = "UserView_FragmentOnDisplay";
var CACHE_KEY_TRIP_SELECTED = "TripSelected_";
var CACHE_KEY_TRIP_SEARCH_RESULT_PAGE = "TripSearch_ResultPage";
var CACHE_KEY_TRIP_SEARCH_OFFSET_CURRENT = "TripSearch_OffsetCurrent";
var CACHE_KEY_TRIP_SEARCH_PAGE_CURRENT = "TripSearch_PageCurrent";
var CACHE_KEY_TRIP_SEARCH_TOTAL_RECORDS = "TripSearch_TotalRecords";
var CACHE_KEY_TRIP_SEARCH_INDEX_SELECTED = "TripSearch_IndexSelected";
var CACHE_KEY_TRIP_USERTRIPS_RESULT_PAGE = "UserTrips_ResultPage_";
var CACHE_KEY_TRIP_USERTRIPS_OFFSET_CURRENT = "UserTrips_OffsetCurrent";
var CACHE_KEY_TRIP_USERTRIPS_PAGE_CURRENT = "UserTrips_PageCurrent";
var CACHE_KEY_TRIP_USERTRIPS_TOTAL_RECORDS = "UserTrips_TotalRecords";
var CACHE_KEY_TRIP_USERTRIPS_INDEX_SELECTED = "UserTrips_IndexSelected";
var CACHE_KEY_TRIP_USERFAVORITES_RESULT_PAGE = "UserFavorites_ResultPage_";
var CACHE_KEY_TRIP_USERFAVORITES_OFFSET_CURRENT = "UserFavorites_OffsetCurrent";
var CACHE_KEY_TRIP_USERFAVORITES_PAGE_CURRENT = "UserFavorites_PageCurrent";
var CACHE_KEY_TRIP_USERFAVORITES_TOTAL_RECORDS = "UserFavorites_TotalRecords";
var CACHE_KEY_TRIP_USERFAVORITES_INDEX_SELECTED = "UserFavorites_IndexSelected";
var CACHE_KEY_PICTURE_LIST_RESULT_PAGE = "PictureList_ResultPage";
var CACHE_KEY_PICTURE_LIST_OFFSET_CURRENT = "PictureList_OffsetCurrent";
var CACHE_KEY_PICTURE_LIST_PAGE_CURRENT = "PictureList_PageCurrent";
var CACHE_KEY_PICTURE_LIST_TOTAL_RECORDS = "PictureList_TotalRecords";
var CACHE_KEY_PICTURE_LIST_INDEX_SELECTED = "PictureList_IndexSelected";
var CACHE_KEY_PICTURE_IMAGENAME_PREFIX = "TrekcrumbPic";
var CACHE_KEY_PROFILE_IMAGENAME_PREFIX = "TrekcrumbProfile";
var CACHE_KEY_COMMENT_USER_RESULT_PAGE = "UserComments_ResultPage_";
var CACHE_KEY_COMMENT_USER_OFFSET_CURRENT = "UserComments_OffsetCurrent";
var CACHE_KEY_COMMENT_USER_PAGE_CURRENT = "UserComments_PageCurrent";
var CACHE_KEY_COMMENT_USER_TOTAL_RECORDS = "UserComments_TotalRecords";
var CACHE_KEY_COMMENT_USER_INDEX_SELECTED = "UserComments_IndexSelected";


/* TODO: DELETE BELOW AND REPLACE WITH  CACHE_KEY_PICTURE_LIST_XXX */
var CACHE_KEY_PICTURE_USERPICTURES_RESULT_PAGE = "UserPictures_ResultPage_";
var CACHE_KEY_PICTURE_USERPICTURES_OFFSET_CURRENT = "UserPictures_OffsetCurrent";
var CACHE_KEY_PICTURE_USERPICTURES_PAGE_CURRENT = "UserPictures_PageCurrent";
var CACHE_KEY_PICTURE_USERPICTURES_TOTAL_RECORDS = "UserPictures_TotalRecords";
var CACHE_KEY_PICTURE_USERPICTURES_INDEX_SELECTED = "UserPictures_IndexSelected";
/* END TODO: DELETE BELOW AND REPLACE WITH  CACHE_KEY_PICTURE_LIST_XXX */

//Page Variables (via DOM3 node custom attributes)
var VARATTR_PAGE_LIST_NUMBER_ROWS_MAXIMUM    = "var_pageList_numOfRowsMax";
var VARATTR_PAGE_LIST_OFFSET_CURRENT         = "var_pageList_offsetCurrent";
var VARATTR_PAGE_LIST_PAGENUM_CURRENT        = "var_pageList_pageNumberCurrent";
var VARATTR_PAGE_LIST_NUMBER_RECORDS_TOTAL   = "var_pageList_numRecordsTotal";
var VARATTR_PAGE_PLACE_NUMBER_PLACES_TOTAL   = "var_pagePlace_numOfTotalPlaces";
var VARATTR_PAGE_PLACE_INDEX_CURRENT         = "var_pagePlace_placeIndexCurrent";
var VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL = "var_pagePic_numOfTotalPictures";
var VARATTR_PAGE_PICTURE_INDEX_CURRENT       = "var_pagePic_pictureCurrentIndex";
var VARATTR_TRIP_ID                          = "var_tripID";
var VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY    = "var_trip_view_fragmentOnDisplay";
var VARATTR_PLACE_MAP_DISPLAY_TYPE           = "var_place_map_displayType";
var VARATTR_USER_USERNAME                    = "var_user_username";
var VARATTR_USER_IS_USERLOGIN_PROFILE        = "var_user_isUserLoginProfile";
var VARATTR_USER_VIEW_FRAGMENT_ON_DISPLAY    = "var_user_view_fragmentOnDisplay";
var VARATTR_USER_UPDATE_SECTION_ON_EDITED    = "var_user_update_sectionOnEdited";
var VARATTR_MAP_DISPLAY_TYPE                 = "var_map_displayType";
var VARATTR_MAP_IS_MAP_INITIALIZED           = "var_map_isMapInitialized";
var VARATTR_GEOLOC_IS_SEARCHLOCATIONCURRENT  = "var_geoLoc_isSearchForLocation";
var VARATTR_GEOLOC_LOCATION_CURRENT          = "var_geoloc_locationCurrent";
var VARATTR_GEOLOC_LOCATION_ADDRESSSTRING    = "var_geoloc_locationAddressString";
var VARATTR_GEOLOC_CALLERFRAGMENT            = "var_geoLoc_callerFragment";
var VARATTR_FAVORITE_BY_TRIP_NUMBER_RECORDS_TOTAL = "var_favoriteByTrip_numRecordsTotal";
var VARATTR_FAVORITE_BY_TRIP_NUMBER_ROWS_MAXIMUM  = "var_favoriteByTrip_numOfRowsMax";
var VARATTR_FAVORITE_BY_TRIP_OFFSET_CURRENT       = "var_favoriteByTrip_offsetCurrent";
var VARATTR_COMMENT_TRIP_NUMBER_RECORDS_TOTAL     = "var_commentForTrip_numRecordsTotal";
var VARATTR_COMMENT_TRIP_NUMBER_ROWS_MAXIMUM      = "var_commentForTrip_numOfRowsMax";
var VARATTR_COMMENT_TRIP_OFFSET_CURRENT           = "var_commentForTrip_offsetCurrent";

//HOME PAGE
var HOME_GALLERYITEM_NUM_TO_DISPLAY_DEFAULT = 5;
var HOME_GALLERYITEM_WIDTH_INCLUDING_MARGIN_MAX = 370;
var HOME_GALLERYITEM_NUM_OF_ROWS_DISPLAY_MAX = 2;
var HOME_PAGESCROLL_TIMEOUT = 500; //milliseconds

//MAP
var MAP_DISPLAY_TYPE_STREET = "1";
var MAP_DISPLAY_TYPE_SATELITE = "2";
var MAP_DISPLAY_TYPE_HYBRID = "3";
var MAP_ZOOM_LEVEL_DEFAULT_FAR = 5;
var MAP_ZOOM_LEVEL_DEFAULT_CLOSEUP = 15;
var MAP_MARKER_ICON_PLACE = APP_ROOT_URI + "/images/icon_place_marker.png";
var MAP_MARKER_ICON_CURRENTLOC = APP_ROOT_URI + "/images/icon_place_marker_currentloc.png";
var MAP_PATHLINE_COLOR = "#072E0B";
var MAP_PATHLINE_STROKE_OPACITY = 1.0;
var MAP_PATHLINE_STROKE_WEIGHT = 2;
var MAP_PATHLINE_SYMBOL_GOOGLEMAP = { path: 'M 0,-1 0,1', strokeOpacity: 1, scale: 4 };
var MAP_STATIC_DIMENSION_HEIGHT = 300;

/* 
 * Google Map API library. 
 * Includes:
 * 1) v = Specify current release version. If none, it would default to  'experimental' version.
 * 2) key = Google Map application key for authorization, max quotes and billing
 *    Note: 
 *    ->> Google kept changing their requirements to include "key" token.
 *    ->> 1/2018: Before using Google Map APIs (dynamic and static), we now must "ENABLE" them from
 *        google cloud platform dashboard console!
 * 3) libraries = List of additional libraries to use (e.g., places is Google GeoLocation 
 *    details and search for point of interests v.3) 
 */
var MAP_SERVER_DYNAMIC_GOOGLEMAP = "https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=" + WEBAPP_GOOGLEMAP_KEY;
var MAP_SERVER_STATIC_GOOGLEMAP = "https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&key=" + WEBAPP_GOOGLEMAP_KEY + "&zoom=" + MAP_ZOOM_LEVEL_DEFAULT_CLOSEUP;

//GEO-LOCATION
var GEOLOC_ENABLE_HIGH_ACCURACY = true;  //boolean. If true, it is more accurate but takes longer.
var GEOLOC_TIMEOUT_VALUE = 30000;  //milliseconds. If empty, it is infinite.
var GEOLOC_MAX_AGE_VALUE = 0;      //milliseconds. If 0, will never use cache data. Otherwise, it is the age of cache data to use
var GEOLOC_SERVER_FINDADDRESS_GOOGLEMAP = "https://maps.googleapis.com/maps/api/geocode/json?sensor=false";;
var GEOLOC_SERVER_FINDADDRESS_TWITTERGEO = "";

//Styling
var STYLE_ELEMENT_ACTIVE_CSS_CLASS = "hoverElementActive";
var STYLE_LAYOUT_CLICKABLE_CLICKED_CSS_CLASS = "body-clickable-clicked";
var STYLE_ELEMENT_ROW_EVEN = "body-row-even";
var STYLE_ELEMENT_ROW_ODD = "body-row-odd";
var MENU_SLIDE_LEFT_TO_RIGHT = "MENU_SLIDE_LEFT_TO_RIGHT ";
var MENU_SLIDE_RIGHT_TO_LEFT = "MENU_SLIDE_RIGHT_TO_LEFT ";
var MENU_SLIDE_BOTTOM_TO_TOP = "MENU_SLIDE_BOTTOM_TO_TOP ";
var MENU_FADE_IN_OUT = "MENU_FADE_IN_OUT";


//END trekcrumb-constants.js