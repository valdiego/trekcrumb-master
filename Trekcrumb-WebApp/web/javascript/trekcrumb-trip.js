"use strict";

/*
 * Generic function to display the given list of Trips into the trip list fragment/page.
 * -> numOfRowsToDisplay = An integer value > 0 if we want to limit the number of rows to display
 *                         regardless of how many of them returned by the server. Enter "-1" if to display ALL. (no limit)
 * -> isLinkOpenNewTab = TRUE if we want to set a URL link to view its detail to open as a new browser
 *                       window tab. FALSE (default) if it will just open on its current window.
 */
function trip_generic_tripListDisplayContent(pageName, listOfTrips, numOfRowsToDisplay, isLinkOpenNewTab) {
    if(pageName == undefined || pageName == null || pageName.trim() == "") {
        pageName = "";
    }
    
    var tripListLayout = document.getElementById(pageName + "tripListLayoutID");
    var tripListContent = document.getElementById(pageName + "tripListContentID");
    var tripRowTemplate = document.getElementById("tripRowTemplateID");
    if(tripListLayout == undefined || tripListLayout == null
            || tripRowTemplate == undefined || tripRowTemplate == null
            || tripListContent == undefined || tripListContent == null) {
        console.error("trip_generic_tripListDisplayContent() - ERROR: tripListLayoutID, tripRowTemplateID or tripListContentID NOT found in current view!");
        return;
    }

    tripListLayout.style.display = "block";
    
    if(listOfTrips == undefined || listOfTrips == null || listOfTrips.length == 0) {
        tripListContent.innerHTML = ERROR_NO_DATA ;
        
    } else {
        tripListContent.innerHTML = "";
        tripListContent.scrollTop;

        //!!! WARNING: Older browsers do not support "querySelector()" !!!
        var listSize = listOfTrips.length;
        var trip = null;
        var tripNote = null;
        for(var index = 0; index < listSize ; index++) {
            trip = listOfTrips[index];
            
            //Clone row template and populate its unique data:
            var rowCurrent = tripRowTemplate.cloneNode(true);
            rowCurrent.id = trip.id;
            
            //URL link:
            var targetURL = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + trip.username + "/" + trip.id;
            if(isLinkOpenNewTab) {
                rowCurrent.setAttribute("onclick", "menu_openNewTab('" + targetURL + "')");
            } else {
                rowCurrent.href = targetURL;
            }
            
            //User Details:
            rowCurrent.querySelector("#userThumbnailFullnameID").innerHTML = trip.userFullname;
            rowCurrent.querySelector("#userThumbnailUsernameID").innerHTML = trip.username;
            if(validate_isStringEmpty(trip.userImageName)) {
                rowCurrent.querySelector("#userThumbnailImageDefaultID").style.display = "block";
                rowCurrent.querySelector("#userThumbnailImageID").style.display = "none";
            } else {
                rowCurrent.querySelector("#userThumbnailImageDefaultID").style.display = "none";
                var userImageElement = rowCurrent.querySelector("#userThumbnailImageID");
                userImageElement.setAttribute("src", FILE_SERVER_URL_GETIMAGE_PROFILE  + trip.userImageName);
                userImageElement.style.display = "block";
            }
            
            //Trip Details:
            if(trip.status != "ACTIVE") {
                rowCurrent.querySelector("#statusID").style.display = 'none';
            }
            if(trip.privacy == "PUBLIC") {
                rowCurrent.querySelector("#privacyPrivateID").style.display = "none";
            }

            rowCurrent.querySelector("#tripNameID").innerHTML = trip.name;
            rowCurrent.querySelector("#locationID").innerHTML = trip.location;
            common_date_translateToLocaleTime(rowCurrent.querySelector("#createdDateID"), trip.created, true, false);
            
            //Trip Note:
            tripNote = trip.note;
            if(tripNote != null && tripNote.trim() != "") {
                if(tripNote.length > 100) {
                    tripNote = tripNote.substring(0,100) + " ... [more]";
                }
                rowCurrent.querySelector("#tripNoteLayoutID").style.display = "block";
                rowCurrent.querySelector("#tripNoteID").innerHTML = tripNote;
            }
            
            //Trip Picture Cover:
            if(trip.listOfPictures != null && trip.listOfPictures.length > 0) {
                var pictureCover = trip.listOfPictures[0];
                rowCurrent.querySelector("#tripPictureCoverLayoutID").style.display = "block";
                rowCurrent.querySelector("#tripPictureCoverImageID").setAttribute("src", FILE_SERVER_URL_GETIMAGE_PICTURE + pictureCover.imageName);
            } else {
                rowCurrent.querySelector("#tripPictureCoverLayoutID").style.display = "none";
            }
            
            //Trip Summary:
            if(trip.listOfPlaces != null && trip.listOfPlaces.length > 0) {
                rowCurrent.querySelector("#placeIconID").className += " font-color-menu";
                rowCurrent.querySelector("#placeNumberID").innerHTML = trip.listOfPlaces.length ;
                rowCurrent.querySelector("#placeNumberID").className += " font-bold";
            } else {
                rowCurrent.querySelector("#placeIconID").className += " font-color-passive";
            }

            if(trip.listOfPictures != null && trip.listOfPictures.length > 0) {
                rowCurrent.querySelector("#picIconID").className += " font-color-menu";
                rowCurrent.querySelector("#picNumberID").innerHTML = trip.listOfPictures.length ;
                rowCurrent.querySelector("#picNumberID").className += " font-bold";
            } else {
                rowCurrent.querySelector("#picIconID").className += " font-color-passive";
            }
            
            if(trip.numOfFavorites > 0) {
                rowCurrent.querySelector("#favoriteIconPassiveID").style.display = "none";
                rowCurrent.querySelector("#favoriteNumberID").innerHTML = trip.numOfFavorites;
                rowCurrent.querySelector("#favoriteNumberID").className += " font-bold";
            } else {
                rowCurrent.querySelector("#favoriteIconActiveID").style.display = "none";
            }

            if(trip.numOfComments > 0) {
                rowCurrent.querySelector("#commentIconPassiveID").style.display = "none";
                rowCurrent.querySelector("#commentNumberID").innerHTML = trip.numOfComments;
                rowCurrent.querySelector("#commentNumberID").className += " font-bold";
            } else {
                rowCurrent.querySelector("#commentIconActiveID").style.display = "none";
            }

            //Finally, display it:
            if(pageName == FRAGMENT_TRIP_LIST_USER_TRIPS 
                    || pageName == FRAGMENT_TRIP_LIST_USER_FAVORITES) {
                rowCurrent.style.display = 'block';
            } else {
                rowCurrent.style.display = 'inline-block';
            }
            tripListContent.appendChild(rowCurrent);
            
            //Check if we limit the display:
            if(numOfRowsToDisplay > 0 && index >= (numOfRowsToDisplay - 1)) {
                break;
            }
        }
    }
}

function trip_generic_privacy_displayWarning(pageName) {
    if(pageName == undefined || pageName == null ) {
        pageName = "";
    }

    if(document.getElementById(pageName + "tripPrivacyPublicInputID").checked == true) {
        document.getElementById(pageName + "privacyPublicTextWarningID").style.display = 'table-cell';
        document.getElementById(pageName + "privacyPrivateTextWarningID").style.display = 'none';
        
    } else if(document.getElementById(pageName + "tripPrivacyPrivateInputID").checked == true) {
        document.getElementById(pageName + "privacyPublicTextWarningID").style.display = 'none';
        document.getElementById(pageName + "privacyPrivateTextWarningID").style.display = 'table-cell';
    
    } else {
        document.getElementById(pageName + "privacyPublicTextWarningID").style.display = 'none';
        document.getElementById(pageName + "privacyPrivateTextWarningID").style.display = 'none';
    }
}

function trip_generic_status_displayWarning(pageName) {
    if(pageName == undefined || pageName == null ) {
        pageName = "";
    }

    if(document.getElementById(pageName + "tripStatusCompletedInputID").checked == true) {
        document.getElementById(pageName + "statusTextIDWarning").style.display = 'inline-block';
    } else {
        document.getElementById(pageName + "statusTextIDWarning").style.display = 'none';
    }
}

/*
 * TripSearch init stage.
 * -> Initializes its parameters to clean (original) state. 
 * -> One particular usage is when client opens tripSearch page to conduct 
 *    clean-state search without any remains from previous search or cached data.
 */
function trip_search_Init() {
    //Clear any cached data:
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "TripSearch");

    //Redirect to server:
    window.location.href = APP_ROOT_URI + "/trekcrumb/trip/search/";
}

/*
 * TripSearch starting point. 
 * -> It would try to recover any last current state params and result list 
 *    when available (cached in session storage). 
 * -> This is to support feature when user navigates away from TripSearch page, 
 *    but then click browser "BACK" button, or copy-paste the search URL, or using browser 'refresh' button.
 */
function trip_search_Current() {
    common_errorOrInfoMessage_hide();    
    
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get current state params if available:
        var offsetCurrent = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_OFFSET_CURRENT);
        var pageCurrent = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_PAGE_CURRENT);
        var numOfRecordsTotal = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_TOTAL_RECORDS);
        if(offsetCurrent != undefined && offsetCurrent != null
                && pageCurrent != undefined && pageCurrent != null
                && numOfRecordsTotal != undefined && numOfRecordsTotal != null) {
            //Set these params onto the page:
            pageList_update(FRAGMENT_TRIP_LIST, parseInt(offsetCurrent), parseInt(pageCurrent), parseInt(numOfRecordsTotal));
            
            //Caching: Get the last result List if available:
            var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + pageCurrent);
            if(listOfTrips != undefined && listOfTrips != null) {
                common_progressBarWidget_hide(FRAGMENT_TRIP_LIST);
                trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST, JSON.parse(listOfTrips), -1, true);
            } else {
                trip_search_ServiceRequest();
            }
            return;
        }
    }
    
    //No caching available. let's start from clean states:
    pageList_init(FRAGMENT_TRIP_LIST);
    trip_search_ServiceRequest();
}

function trip_search_Next() {
    if(pageList_next(FRAGMENT_TRIP_LIST)) {
        document.getElementById(FRAGMENT_TRIP_LIST + "tripListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_TRIP_LIST);
        
        trip_search_RetrieveTripList();
    }
}

function trip_search_Previous() {
    if(pageList_previous(FRAGMENT_TRIP_LIST)) {
        document.getElementById(FRAGMENT_TRIP_LIST + "tripListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_TRIP_LIST);
        
        trip_search_RetrieveTripList();
    }
}

function trip_search_RetrieveTripList() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Update current state params into cache:
        common_localeStorage_cacheCurrentListState(
                FRAGMENT_TRIP_LIST,
                CACHE_KEY_TRIP_SEARCH_OFFSET_CURRENT,
                CACHE_KEY_TRIP_SEARCH_PAGE_CURRENT,
                CACHE_KEY_TRIP_SEARCH_TOTAL_RECORDS);

        //Caching: Get the last result List if available:
        var pageListLayout = document.getElementById(FRAGMENT_TRIP_LIST + 'paginationListLayoutID');
        var pageCurrent = pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
        var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + pageCurrent);
        if(listOfTrips != undefined && listOfTrips != null) {
            common_progressBarWidget_hide(FRAGMENT_TRIP_LIST);
            trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST, JSON.parse(listOfTrips), -1, true);
        } else {
            trip_search_ServiceRequest();
        }
    } else {
        trip_search_ServiceRequest();
    }
}

/*
 * TripSearch Form init
 * -> Before showing the form, push the main page back to scroll top position.
 *    This is because the form has "position = absolute", not "fixed" and
 *    it would cover only portion of area of its height, and the remaining
 *    area were still visible/accessable. Scrolling to top will trick the issue.
 * -> Then disabling the main page vertical scroll.
 */
function trip_search_FormInit() {
    var tripSearchMainLayout = document.getElementById("searchLayoutID");
    if(tripSearchMainLayout == undefined || tripSearchMainLayout == null) {
        //Illegal access:
        return;
    }

    tripSearchMainLayout.scrollTop = 0;
    tripSearchMainLayout.style.overflowY = "hidden";
    
    document.getElementById("tripSearchFormLayoutID").style.display = "block";
}

function trip_search_FormCancel() {
    document.getElementById("tripSearchFormLayoutID").style.display = "none";
    document.getElementById("searchLayoutID").style.overflowY = "auto";
}

function trip_search_FormClear() {
    document.getElementById('tripKeywordInputID').value = "";
    document.getElementById('userKeywordInputID').value = "";
    document.getElementById('statusInputALLID').checked = true;
}

function trip_search_FormSubmit() {
    //Hide:
    common_errorOrInfoMessage_hide();
    document.getElementById(FRAGMENT_TRIP_LIST + 'tripListContentID').innerHTML = "";
    document.getElementById(FRAGMENT_TRIP_LIST + "tripListLayoutID").style.display = "none";
    
    var searchCriteriaCrumbs = document.getElementById('searchCriteriaCrumbsID');
    searchCriteriaCrumbs.innerHTML = "";
    searchCriteriaCrumbs.style.display = "none";
    
    trip_search_FormCancel();

    //Reset:
    pageList_init(FRAGMENT_TRIP_LIST);
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "TripSearch");
    //document.getElementById('searchCriteriaCrumbsInputID').value = null;
    
    //Show:
    common_progressBarWidget_show(FRAGMENT_TRIP_LIST);
    
    //Call server:
    trip_search_ServiceRequest();
}

function trip_search_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Get pagination params:
        var pageListLayout = document.getElementById(FRAGMENT_TRIP_LIST + 'paginationListLayoutID');
        var offsetCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT));
        var pageCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT));
        
        //Compose request:
        var tripKeywordValue = document.getElementById('tripKeywordInputID').value;
        if(tripKeywordValue != null) {
            tripKeywordValue = tripKeywordValue.trim();
        }
        var userKeywordValue = document.getElementById('userKeywordInputID').value;
        if(userKeywordValue != null) {
            userKeywordValue = userKeywordValue.trim();
        }
        var statusValue = null;
        var statusOptions = document.getElementsByName("tripStatus");
        var len = statusOptions.length;
        for(var i = 0; i < len; ++i) {
            if(statusOptions[i].checked == true) {
                if(statusOptions[i].id == "statusInputALLID") {
                    //Ignore
                    break;
                }
                statusValue = statusOptions[i].value;
                break;
            }
        }
        
        var tripSearchCriteria = {
            offset: offsetCurrent,
            orderBy: "ORDER_BY_CREATED_DATE",
            tripKeyword: tripKeywordValue,
            userKeyword: userKeywordValue ,
            status: statusValue 
        };
        
        //Compose search criteria breadcrumbs:
        var searchCriteriaCrumbs = null;
        if(tripKeywordValue != null && tripKeywordValue != "") {
            searchCriteriaCrumbs = "Search results for Trek \'" + tripKeywordValue + "\'";
        }
        if(userKeywordValue != null && userKeywordValue != "") {
            if(searchCriteriaCrumbs == null) {
                searchCriteriaCrumbs = "Search results for User \'" + userKeywordValue + "\'";
            } else {
                searchCriteriaCrumbs = searchCriteriaCrumbs + " >> User \'" + userKeywordValue + "\'";
            }
        }
        if(statusValue != null) {
            if(searchCriteriaCrumbs == null) {
                searchCriteriaCrumbs = "Search results for Status \'" + statusValue + "\'";
            } else {
                searchCriteriaCrumbs = searchCriteriaCrumbs + " >> Status \'" + statusValue + "\'";
            }
        }
        if(searchCriteriaCrumbs != null) {
            //document.getElementById('searchCriteriaCrumbsInputID').value = searchCriteriaCrumbs;
            //document.getElementById('searchCriteriaCrumbsID').innerHTML = searchCriteriaCrumbs;
            var searchCriteriaCrumbsElement = document.getElementById('searchCriteriaCrumbsID');
            searchCriteriaCrumbsElement.innerHTML = searchCriteriaCrumbs;
            searchCriteriaCrumbsElement.style.display = "block";
        }
        
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                trip_search_ServiceResponse(offsetCurrent, pageCurrent, xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/trip/search/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send(JSON.stringify(tripSearchCriteria));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function trip_search_ServiceResponse(offset, page, xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_TRIP_LIST);
    
    if(xmlhttpRequest.status != 200) {
        var errorMessageArray = [xmlhttpRequest.statusText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        var errorMessageArray = [serverResponseObj.serviceError.errorText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    
    //Success:    
    common_errorOrInfoMessage_hide();
    var totalNumOfRecords = serverResponseObj.numOfRecords;
    pageList_update(FRAGMENT_TRIP_LIST, offset, page, totalNumOfRecords);
    
    var listOfTrips = serverResponseObj.listOfTrips;
    trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST, listOfTrips, -1, true);
    
    //Caching: Save list results:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + page, 
            JSON.stringify(listOfTrips)); 
}

/*
 * Home TripSearch starting point. 
 * -> Similar to TripSearch page landing operation, this is for Home Trip Gallery where it
 *    displays the most recent Trips with default search criteria. Unlike TripSearch, however,
 *    there is no pagination, no next/previous menus, no breadcrumbs, etc.
 * -> See trip_search_Current() for other comments.
 */
function trip_home_search_Current() {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get the last result List if available
        var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + "0");
        if(listOfTrips != undefined && listOfTrips != null) {
            common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_HOME);

            if(mHome_numOfListItemsToDisplay == 0) {
                mHome_numOfListItemsToDisplay = HOME_GALLERYITEM_NUM_TO_DISPLAY_DEFAULT;
            }
            trip_generic_tripListDisplayContent(
                    FRAGMENT_TRIP_LIST_HOME, JSON.parse(listOfTrips), mHome_numOfListItemsToDisplay, true);
            return;
        }
    }
    
    //No caching available. let's start from clean states:
    trip_home_search_ServiceRequest();
}

function trip_home_search_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var tripSearchCriteria = {
            offset: 0,
            orderBy: "ORDER_BY_CREATED_DATE"
        };
        
        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                trip_home_search_ServiceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST_HOME, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/trip/search/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send(JSON.stringify(tripSearchCriteria));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST_HOME, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function trip_home_search_ServiceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_HOME);
    var isSuccess = common_serviceResponse_evaluateAJAX(xmlhttpRequest);
    if(!isSuccess) {
        //Insert error message onto Home's Trip gallery section:
        var tripListContent = document.getElementById(FRAGMENT_TRIP_LIST_HOME + "tripListContentID");
        if(tripListContent != undefined && tripListContent != null) {
            tripListContent.innerHTML = ERROR_MSG_SERVER_DEFAULT;
        }
        
        return;
    }
    
    //Success:
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    var listOfTrips = serverResponseObj.listOfTrips;
    if(mHome_numOfListItemsToDisplay == 0) {
        mHome_numOfListItemsToDisplay = HOME_GALLERYITEM_NUM_TO_DISPLAY_DEFAULT;
    }
    trip_generic_tripListDisplayContent(
            FRAGMENT_TRIP_LIST_HOME, listOfTrips, mHome_numOfListItemsToDisplay, true);
    
    //Caching: Save list results:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + "0", 
            JSON.stringify(listOfTrips)); 
    
    place_home_placeListAsTagDisplayContent(listOfTrips);
}

/*
DELETEME
function trip_viewServiceRequest(element) {
    //!!! WARNING: Older browsers do not support "querySelector()" !!!
    console.log("trip_viewServiceRequest() - Incoming element: " + element.innerHTML);
    var username = element.querySelector("#usernameID").innerHTML;
    var tripID = element.id;
    var url = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID;
    window.location.href = url;
    //Force to open new window or tab:
    //var win = window.open(url, '_blank');
    //win.focus();
}
*/

/*
 * TripView loading: Captures its URL query parameters, if any, that comes as part of loading up
 * the Trip page. This could contain place/picture indexes to open or focus on once the loading
 * completes. 
 * -> This capture step must be done before loading any children fragments (PlaceDetail, PictureDetail, etc.)
 *    because they are the one(s) who will use it.
 * -> This step should only be called if server response is successful and the Trip is found (not null).   
 */
function trip_view_captureURLQueryParams(serviceResponseJSON) {
    if(serviceResponseJSON == undefined 
            || serviceResponseJSON == null
            || !serviceResponseJSON.success
            || serviceResponseJSON.listOfTrips == null) {
        return;
    }
    
    //Look for specified Place:
    var placeIndex = common_url_getQueryParamValue(QUERYPARAM_NAME_PLACE_INDEX);
    if(placeIndex != null && !isNaN(placeIndex)) {
        document.getElementById('paginationPlaceLayoutID').setAttribute(VARATTR_PAGE_PLACE_INDEX_CURRENT, placeIndex);
    }
    
    //Look for specified Picture:
    var pictureIndex = common_url_getQueryParamValue(QUERYPARAM_NAME_PICTURE_INDEX);
    if(pictureIndex != null && !isNaN(pictureIndex)) {
        document.getElementById('paginationPictureLayoutID').setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, pictureIndex);
    } else {
        var pictureid = common_url_getQueryParamValue(QUERYPARAM_NAME_PICTURE_ID);
        if(pictureid != null) {
            //Only if query is provided, we would go with all these troubles:
            var listOfPics = serviceResponseJSON.listOfTrips[0].listOfPictures;
            if(listOfPics != null && listOfPics.length > 0) {
                var numOfPics = listOfPics.length;
                for(var index = 0; index < numOfPics ; index++) {
                    var picture = listOfPics[index];
                    if(pictureid.toUpperCase() == (picture.id).toUpperCase()) {
                        //Found it!
                        document.getElementById('paginationPictureLayoutID').setAttribute(VARATTR_PAGE_PICTURE_INDEX_CURRENT, index);
                        break;
                    }
                }
            }
        }
    }
}

/*
 * TripView to display result from back-end service.
 * -> The input parameter comes as java's JSON-formatted String. However, javascript
 *    receives it NOT as String, but already as a JSON-parsed javascript object. Therefore, we do not need 
 *    to do "JSON.parse(inputString)" to transform it into an javascript object. Instead, just use it right away.
 */
function trip_view_DisplayContent(serviceResponseJSON) {
    if(serviceResponseJSON == undefined || serviceResponseJSON == null) {
        common_errorOrInfoMessage_show([ERROR_MSG_SERVER_DEFAULT], null);
        
    } else if(!serviceResponseJSON.success) {
        if(serviceResponseJSON.serviceError != null
                && serviceResponseJSON.serviceError.errorText != null) {
            common_errorOrInfoMessage_show([serviceResponseJSON.serviceError.errorText], null);
            return;
        } else {
            common_errorOrInfoMessage_show([ERROR_MSG_SERVER_DEFAULT], null);
            return;
        }
    } else if(serviceResponseJSON.listOfTrips == null || serviceResponseJSON.listOfTrips.length == 0) {
        //Success, but Trip is empty:
        common_errorOrInfoMessage_show([ERROR_MSG_SERVER_TRIP_NOT_FOUND], null);
        return;
    }
    
    /*
     * Else: While serviceResponse is all good and successful, further business logic may occur
     * in each "embedded" fragment pages to take care of themselves
     */
    if(common_url_getQueryParamValue(QUERYPARAM_NAME_PLACE_INDEX) != null) {
        trip_view_DisplaySelectedTab(FRAGMENT_PLACE_DETAIL);
        return;
    } 
    if(common_url_getQueryParamValue(QUERYPARAM_NAME_PICTURE_INDEX) != null
            || common_url_getQueryParamValue(QUERYPARAM_NAME_PICTURE_ID) != null) {
        trip_view_DisplaySelectedTab(FRAGMENT_PICTURE_DETAIL);
        return;
    }
    
    //Default:
    trip_view_DisplaySelectedTab(null);
}

/*
 * TripView navigation to display its main fragments: TripDetail, PictureDetail or PlaceDetail.
 * 1. TripDetail: It is the default view. However, TripView can display both TripDetail and PlaceDetail
 *    for large landscape screen as defined in their CSS styling and responsive functions. 
 * 2. WARNING: 
 *    -> When displaying view from one to another, we may set the current layout to have 
 *       its style.display to 'none'. This will cause the layout in question to have offsetWidth/Height
 *       to ZERO, thus can cause error in responsiveness calculation when user changes screen size/orientation.
 *       To avoid this issue, we can set marginLeft/Right and visibility to switch one view to the next.
 */
function trip_view_DisplaySelectedTab(fragmentPage) {
    var tripViewLayout = document.getElementById("tripViewLayoutID");
    var tripDetailLayout = document.getElementById('tripDetailLayoutID');
    var pictureDetailLayout = document.getElementById('pictureDetailLayoutID');
    var pictureDetailTitleLayout = document.getElementById('pictureDetailTitleID');
    var placeDetailLayout = document.getElementById('placeDetailLayoutID');
    var placeDetailTitleLayout = document.getElementById('placeDetailTitleID');
    var menuMainBackIcon = document.getElementById('menuBackIconID');
    
    //Check the current fragmentPage:
    if(fragmentPage == null || fragmentPage.trim() == "") {
        fragmentPage = tripViewLayout.getAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY);
    }
    if(fragmentPage == null || fragmentPage.trim() == "") {
        fragmentPage = FRAGMENT_TRIP_DETAIL;
    }
    
    switch(fragmentPage) {
        case FRAGMENT_TRIP_DETAIL:
            var previousFragmentOnDisplay = tripViewLayout.getAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY);
            tripViewLayout.setAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_TRIP_DETAIL);
            
            //HIDE
            //1. PictureDetail
            pictureDetailTitleLayout.style.display = "none";
            if(pictureDetailLayout != undefined && pictureDetailLayout != null) {
                pictureDetailLayout.style.display = "none";        
            }
            
            //2. PlaceDetail: Hide or show half-page will be determined by triggering responsiveness
            placeDetailTitleLayout.style.display = "none";
            
            //SHOW:
            //1. TripDetail
            tripDetailLayout.style.display = "inline-block";
            
            //2. Trigger responsiveness:
            menuMainBackIcon.style.display = "none";
            menuMainBackIcon.setAttribute("onclick", "");
            responsive_adjustTripViewLayout();
            
            //3. Load data:
            /*
             * If previous tab is PictureDetail, then we may need to re-load some picture
             * contents to make sure we sync-up with user latest picture navigations and 
             * PictureSlideshow/PictureGallery inside TripDetail.
             */
            if(previousFragmentOnDisplay == FRAGMENT_PICTURE_DETAIL) {
                var numOfPictures = parseInt(document.getElementById('paginationPictureLayoutID').getAttribute(VARATTR_PAGE_PICTURE_NUMBER_PICTURES_TOTAL));
                if(numOfPictures > 0) {
                    pagePic_updateNavigationMenu(FRAGMENT_PICTURE_SLIDESHOW, null, null);
                    picture_generic_detailRetrieve(FRAGMENT_PICTURE_SLIDESHOW, null, "pictureSlideshowLayoutID");
                    picture_gallery_ItemSelectedHighlight();
                }
            }
            
            /*
             * If PlaceDetail is on display, we need to populate its view.
             */
            placeDetailLayout = document.getElementById('placeDetailLayoutID');
            if(placeDetailLayout.style.display != "none") {
                place_detail_DisplayContent();
                place_detail_DisplayMapCanvas();
                menu_menuPage_PlaceDetailShowOrHide();
            }
            break;

        case FRAGMENT_PICTURE_DETAIL:
            if(pictureDetailLayout == undefined || pictureDetailLayout == null) {
                //Not available
                return;        
            }
            
            tripViewLayout.setAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_PICTURE_DETAIL);

            //HIDE:
            //1. TripDetail
            tripDetailLayout.style.display = "none";
            
            //2. PlaceDetail
            placeDetailTitleLayout.style.display = "none";
            if(placeDetailLayout != undefined && placeDetailLayout != null) {
                placeDetailLayout.style.display = "none";
            }
            
            //SHOW:
            //1. PictureDetail
            pictureDetailTitleLayout.style.display = "inline-block";
            pictureDetailLayout.style.display = "block";
            
            //2. Load data:
            /*
             * Re-load some contents to make sure they sync-up with user latest 
             * navigations on pictures to use by PictureDetail.
             */
            pagePic_updateNavigationMenu(FRAGMENT_PICTURE_DETAIL, null, null);
            picture_generic_detailRetrieve(FRAGMENT_PICTURE_DETAIL, null, "pictureDetailLayoutID");

            //3. Trigger responsiveness:
            menuMainBackIcon.style.display = "inline-block";
            menuMainBackIcon.setAttribute("onclick", "trip_view_DisplaySelectedTab(FRAGMENT_TRIP_DETAIL)");
            responsive_adjustTripViewLayout();
            break;
            
        case FRAGMENT_PLACE_DETAIL:
            if(placeDetailLayout == undefined || placeDetailLayout == null) {
                //Not available
                return;        
            }

            tripViewLayout.setAttribute(VARATTR_TRIP_VIEW_FRAGMENT_ON_DISPLAY, FRAGMENT_PLACE_DETAIL);

            //HIDE
            //1. TripDetail
            tripDetailLayout.style.display = "none";

            //2. PictureDetail
            pictureDetailTitleLayout.style.display = "none";
            if(pictureDetailLayout != undefined && pictureDetailLayout != null) {
                pictureDetailLayout.style.display = "none";        
            }
            
            //SHOW
            //1. PlaceDetail
            placeDetailTitleLayout.style.display = "inline-block";
            placeDetailLayout.style.display = "inline-block";

            //2. Trigger responsiveness:
            menuMainBackIcon.style.display = "inline-block";
            menuMainBackIcon.setAttribute("onclick", "trip_view_DisplaySelectedTab(FRAGMENT_TRIP_DETAIL)");
            responsive_adjustTripViewLayout();
            
            //3. Load data:
            place_detail_DisplayContent();
            place_detail_DisplayMapCanvas();
            menu_menuPage_PlaceDetailShowOrHide();
            break;
            
        default:
            //None
    }
}

/*
 * UserTrips init stage.
 * -> Initializes its parameters to clean (original) state. 
 *    One particular usage is when client click a link to view one user profile to the next
 *    user profile. Therefore any previous stored data/cache must be cleared.
 */
function trip_userTrips_Init() {
    //Clear any cached data:
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserTrips");
}

/*
 * UserTrips starting point. 
 * -> It would try to recover any last current state params and result list 
 *    when available (cached in session storage). 
 * -> This is to support feature when user navigates away from current UserTrips page, 
 *    but then click browser "BACK" button such that we avoid to re-init the list service call.
 */
function trip_userTrips_Current(username, isUserLoginProfile) {
    if(mDevice_isSupportLocaleStorage) {
        //Caching: Get current state params if available:
        var offsetCurrent = sessionStorage.getItem(CACHE_KEY_TRIP_USERTRIPS_OFFSET_CURRENT);
        var pageCurrent = sessionStorage.getItem(CACHE_KEY_TRIP_USERTRIPS_PAGE_CURRENT);
        var numOfRecordsTotal = sessionStorage.getItem(CACHE_KEY_TRIP_USERTRIPS_TOTAL_RECORDS);
        var indexSelected = sessionStorage.getItem(CACHE_KEY_TRIP_USERTRIPS_INDEX_SELECTED);
        if(offsetCurrent != undefined && offsetCurrent != null 
                && pageCurrent != undefined && pageCurrent != null
                && numOfRecordsTotal != undefined && numOfRecordsTotal != null) {
            //Set these params onto the page:
            pageList_update(FRAGMENT_TRIP_LIST_USER_TRIPS, 
                            parseInt(offsetCurrent), parseInt(pageCurrent), parseInt(numOfRecordsTotal));
            
            //Caching: Get the last result List if available:
            var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_USERTRIPS_RESULT_PAGE + username + pageCurrent);
            if(listOfTrips != undefined && listOfTrips != null) {
                common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_USER_TRIPS);
                trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST_USER_TRIPS, JSON.parse(listOfTrips), -1, false);
            } else {
                trip_userTrips_ServiceRequest(username, isUserLoginProfile);
            }
            return;
        }
    }
    
    //No caching available. let's start from clean states:
    pageList_init(FRAGMENT_TRIP_LIST_USER_TRIPS);
    trip_userTrips_ServiceRequest(username, isUserLoginProfile);
}

function trip_userTrips_Next() {
    if(pageList_next(FRAGMENT_TRIP_LIST_USER_TRIPS)) {
        document.getElementById(FRAGMENT_TRIP_LIST_USER_TRIPS + "tripListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_TRIP_LIST_USER_TRIPS);
    
        trip_userTrips_RetrieveTripList();
    }
}

function trip_userTrips_Previous() {
    if(pageList_previous(FRAGMENT_TRIP_LIST_USER_TRIPS)) {
        document.getElementById(FRAGMENT_TRIP_LIST_USER_TRIPS + "tripListContentID").innerHTML = "";
        common_progressBarWidget_show(FRAGMENT_TRIP_LIST_USER_TRIPS);

        trip_userTrips_RetrieveTripList();
    }
}

function trip_userTrips_RetrieveTripList() {
    var userTripsLayout = document.getElementById('userTripsLayoutID');
    var username = userTripsLayout.getAttribute(VARATTR_USER_USERNAME);
    var isUserLoginProfile = userTripsLayout.getAttribute(VARATTR_USER_IS_USERLOGIN_PROFILE);

    if(mDevice_isSupportLocaleStorage) {
        //Caching: Update current state params into cache:
        common_localeStorage_cacheCurrentListState(
                FRAGMENT_TRIP_LIST_USER_TRIPS,
                CACHE_KEY_TRIP_USERTRIPS_OFFSET_CURRENT,
                CACHE_KEY_TRIP_USERTRIPS_PAGE_CURRENT,
                CACHE_KEY_TRIP_USERTRIPS_TOTAL_RECORDS);
                
        //Caching: Get the last result List if available:
        var pageListLayout = document.getElementById(FRAGMENT_TRIP_LIST_USER_TRIPS + 'paginationListLayoutID');
        var pageCurrent = pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT);
        var listOfTrips = sessionStorage.getItem(CACHE_KEY_TRIP_USERTRIPS_RESULT_PAGE + username + pageCurrent);
        if(listOfTrips != undefined && listOfTrips != null) {
            common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_USER_TRIPS);
            trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST_USER_TRIPS, JSON.parse(listOfTrips), -1, false);
        } else {
            trip_userTrips_ServiceRequest(username, isUserLoginProfile);
        }
    } else {
        trip_userTrips_ServiceRequest(username, isUserLoginProfile);
    }
}

function trip_userTrips_ServiceRequest(usernameValue, isUserLoginProfile) {
    if(mDevice_isSupportAJAX) {
        var xmlhttpRequest = new XMLHttpRequest();
        
        //Get pagination params:
        var pageListLayout = document.getElementById(FRAGMENT_TRIP_LIST_USER_TRIPS + 'paginationListLayoutID');
        var offsetCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_OFFSET_CURRENT));
        var pageCurrent = parseInt(pageListLayout.getAttribute(VARATTR_PAGE_LIST_PAGENUM_CURRENT));
        
        //Prepare response handling:
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                trip_userTrips_ServiceResponse(usernameValue, offsetCurrent, pageCurrent, xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST_USER_TRIPS, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        //Compose request:
        var tripToRetrieve = {
            username: usernameValue
        };
        if(isUserLoginProfile != "true") {
            tripToRetrieve.privacy = "PUBLIC";
        }
        
        //Prepare send (as JSON request):
        try {
            var urlTarget = APP_ROOT_URI + "/trekcrumb/trip/retrieve_byUsername/" + offsetCurrent;
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            //xmlhttpRequest.setRequestHeader("cache","false");
            xmlhttpRequest.send(JSON.stringify(tripToRetrieve));
        } catch(error) {
            common_AJAX_opsError_show(FRAGMENT_TRIP_LIST_USER_TRIPS, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function trip_userTrips_ServiceResponse(username, offset, page, xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    
    //Else:
    common_progressBarWidget_hide(FRAGMENT_TRIP_LIST_USER_TRIPS);

    if(xmlhttpRequest.status != 200) {
        var errorMessageArray = [xmlhttpRequest.statusText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
    if(!serverResponseObj.success) {
        var errorMessageArray = [serverResponseObj.serviceError.errorText];
        common_errorOrInfoMessage_show(errorMessageArray, null);
        return;
    }
    
    //Success
    //Note: Do not call 'hide error or info message' function because we may want to display
    //      info message from server from other fragment processing.
    var totalNumOfRecords = serverResponseObj.numOfRecords;
    pageList_update(FRAGMENT_TRIP_LIST_USER_TRIPS, offset, page, totalNumOfRecords);
    
    var listOfTrips = serverResponseObj.listOfTrips;
    trip_generic_tripListDisplayContent(FRAGMENT_TRIP_LIST_USER_TRIPS, listOfTrips, -1, false);
    
    //Caching: Save list results:
    common_localeStorage_setItem(
            CACHE_TYPE_LOCALESTORAGE_SESSION, 
            CACHE_KEY_TRIP_USERTRIPS_RESULT_PAGE + username + page, 
            JSON.stringify(listOfTrips)); 
}

/*
 * TripCreate init
 * -> Before showing the form, push the main page back to scroll top position.
 *    This is because the form has "position = absolute", not "fixed" and
 *    it would cover only portion of area of its height, and the remaining
 *    area of long page were still visible/accessable. Scrolling to top will trick the issue.
 * -> Then disabling the main page vertical scroll.
 */
function trip_create_Init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    document.body.style.overflowY = "hidden";
    document.documentElement.style.overflowY = "hidden";
    
    geoLoc_currentLocation_Init(FRAGMENT_TRIP_CREATE);
}

function trip_create_currentLocationFound() {
    var tripCreateLayout = document.getElementById('tripCreateLayoutID');
    if(tripCreateLayout == undefined || tripCreateLayout == null) {
        //Invalid view:
        console.error("trip_create_currentLocationFound() - ERROR: Could not find tripCreateLayoutID layout!");
        return;
    }

    var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
    geoLocCurrentLocationLayout.style.display = "none";

    common_form_afterInit(FRAGMENT_TRIP_CREATE, tripCreateLayout);
    
    var geoLocationCurrentString = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_CURRENT);
    if(geoLocationCurrentString != null 
            && geoLocationCurrentString.trim() != "null"
            && geoLocationCurrentString.trim() != "") {
        document.getElementById(FRAGMENT_TRIP_CREATE  + "formID").style.display = "block";
        document.getElementById(FRAGMENT_TRIP_CREATE + 'tripPrivacyPublicInputID').checked = true;
        trip_generic_privacy_displayWarning(FRAGMENT_TRIP_CREATE);
        
        /*
         * Draw static map: The parent and its layouts MUST NOT have display = "none" at this point.
         */
        var geoLocationCurrentObj = JSON.parse(geoLocationCurrentString);
        map_static_displayContent(geoLocationCurrentObj.coords.latitude, 
                                  geoLocationCurrentObj.coords.longitude);
    }
}

function trip_create_cancel() {
    var tripCreateLayout = document.getElementById("tripCreateLayoutID");
    if(tripCreateLayout == undefined || tripCreateLayout == null) {
        //Illegal access:
        return;
    }
    
    //Clean any leftover:
    common_errorOrInfoMessage_hide();
    document.getElementById("tripCreateTripNameInputID").value = null;
    document.getElementById("tripCreateTripNoteInputID").value = null;
    
    var mapStaticCanvas = document.getElementById("mapStaticImageID");
    if(mapStaticCanvas != null && mapStaticCanvas != undefined) {
        mapStaticCanvas.setAttribute("src", "");
    }
    
    //Enable back main window scroll
    document.body.style.overflowY = "initial";
    document.documentElement.style.overflowY = "initial";
    
    tripCreateLayout.style.display = "none";
}

function trip_create_refresh() {
    //Preserve any inserted values:
    var tripNamePending = document.getElementById("tripCreateTripNameInputID").value;
    var tripNotePending = document.getElementById("tripCreateTripNoteInputID").value;
    
    trip_create_cancel();
    trip_create_Init();
    
    //Put back previously entered values:
    document.getElementById("tripCreateTripNameInputID").value = tripNamePending;
    document.getElementById("tripCreateTripNoteInputID").value = tripNotePending;
}

function trip_create_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    
    var geoLocationCurrentString = document.getElementById("geoLocCurrentLocationLayoutID").getAttribute(VARATTR_GEOLOC_LOCATION_CURRENT);
    if(geoLocationCurrentString == null 
            || geoLocationCurrentString.trim() == "null"
            || geoLocationCurrentString.trim() == "") {
        common_errorOrInfoMessage_show([ERROR_GEOLOC_CURRENTLOCATION_EMPTY], null);
        return;
    }
    var validateTripNameResultError = validate_tripName("tripCreateTripNameInputID");
    if(validateTripNameResultError != null) {
        common_errorOrInfoMessage_show([validateTripNameResultError], null);
        return;
    }

    //Else:
    common_form_afterSubmit(FRAGMENT_TRIP_CREATE);
   
    /*
     * Note: 
     * We do not immediately call trip_create_serviceRequest() here. Instead, we call 
     * findAddress() function to try to get the location address given its (lat,lan) 
     * before we save them. Notice that we invoke findAddress op here, that is ONLY when the 
     * user decides to save, so not to deal with user cancel/navigate away which wastes resources. 
     * Only when findAddress is all done (success or failure), we proceed with trip create save.
     */
    var geoLocationCurrentObj = JSON.parse(geoLocationCurrentString);
    geoLoc_findAddress(geoLocationCurrentObj.coords.latitude, 
                       geoLocationCurrentObj.coords.longitude,
                       "PRIMARY",
                       FRAGMENT_TRIP_CREATE);
}

function trip_create_serviceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var geoLocCurrentLocationLayout = document.getElementById("geoLocCurrentLocationLayoutID");
        var geoLocationCurrentString = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_CURRENT);
        var geoLocLocationCurrentObj = JSON.parse(geoLocationCurrentString);

        var locationAddress = geoLocCurrentLocationLayout.getAttribute(VARATTR_GEOLOC_LOCATION_ADDRESSSTRING);
        if(locationAddress == null 
                || locationAddress.trim() == "null" 
                || locationAddress.trim() == "") {
            locationAddress = geoLoc_locationAddress_composeDefault(geoLocLocationCurrentObj.coords.latitude, 
                                                                    geoLocLocationCurrentObj.coords.longitude);
        }

        var userID = document.getElementById('tripCreateUserID').value;
        var tripName = document.getElementById('tripCreateTripNameInputID').value.trim();
        var tripNote = document.getElementById('tripCreateTripNoteInputID').value;
        if(tripNote != null) {
            tripNote = tripNote.trim();
        }
        var tripStatus = document.getElementById(FRAGMENT_TRIP_CREATE + 'tripStatusActiveInputID').value;

        var tripPrivacy = null;
        if(document.getElementById(FRAGMENT_TRIP_CREATE + 'tripPrivacyPublicInputID').checked == true) {
            tripPrivacy = document.getElementById(FRAGMENT_TRIP_CREATE + 'tripPrivacyPublicInputID').value;
        } else if(document.getElementById(FRAGMENT_TRIP_CREATE + 'tripPrivacyPrivateInputID').checked == true) {
            tripPrivacy = document.getElementById(FRAGMENT_TRIP_CREATE + 'tripPrivacyPrivateInputID').value;;
        }
        
        var tripToCreate = {
            userID: userID,
            name: tripName,
            location: locationAddress,
            note: tripNote,
            status: tripStatus,
            privacy: tripPrivacy,
            listOfPlaces: [
                {
                    userID: userID,
                    location: locationAddress,
                    latitude: geoLocLocationCurrentObj.coords.latitude,
                    longitude: geoLocLocationCurrentObj.coords.longitude
                }
            ]
        };

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                trip_create_serviceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_TRIP_CREATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = APP_ROOT_URI + "/trekcrumb/trip/create/submit/AJAX";
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(tripToCreate));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_TRIP_CREATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function trip_create_serviceResponse(xmlhttpRequest) {
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_TRIP_CREATE, xmlhttpRequest);
    if(isSuccess) {
        var trip = null;
        var serverResponseObj = JSON.parse(xmlhttpRequest.responseText);
        if(serverResponseObj.listOfTrips != null) {
            trip = serverResponseObj.listOfTrips[0];
        }
        if(trip == null) {
            common_errorOrInfoMessage_show([ERROR_MSG_TRIP_CREATE_SUCCESS_NO_TRIP_FOUND], null);
            return;
        }

        //ELSE: Redirect to result page
        var username = document.getElementById('tripCreateUsernameID').value;
        var tripID = trip.id;
        window.location.href = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID;
    }
}

function trip_update_init() {
    if(!mDevice_isSupportAJAX) {
        common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_AJAX_NOT_SUPPORTED], null); 
        return;
    }
    var tripUpdateLayout = document.getElementById("tripUpdateLayoutID");
    if(tripUpdateLayout == undefined || tripUpdateLayout == null) {
        //Illegal access:
        console.error("trip_update_init() - ERROR: Cannot find tripUpdateLayoutID!");
        return;
    }
    
    //Populate editable input fields with original values:
    document.getElementById("tripUpdateTripNameInputID").value = document.getElementById("tripUpdateTripNameOrigID").value;
    document.getElementById("tripUpdateNoteInputID").value = document.getElementById("tripUpdateNoteOrigID").value;
    document.getElementById("tripUpdateTripLocationInputID").innerHTML = document.getElementById("tripUpdateTripLocationOrigID").value;
    
    //Status: Do this ONLY-IF status is not "COMPLETED"
    var tripStatusActiveInput = document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripStatusActiveInputID');
    if(tripStatusActiveInput != undefined || tripStatusActiveInput != null) {
        tripStatusActiveInput.checked = true;;
    }

    //Privacy:
    var tripUpdateTripPrivacyOrig = document.getElementById('tripUpdateTripPrivacyOrigID').value;
    if(tripUpdateTripPrivacyOrig == "PUBLIC") {
        document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripPrivacyPublicInputID').checked = true;
    } else {
        document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripPrivacyPrivateInputID').checked = true;
    }
    trip_generic_privacy_displayWarning(FRAGMENT_TRIP_UPDATE);
    
    common_form_afterInit(FRAGMENT_TRIP_UPDATE, tripUpdateLayout);
}

function trip_update_location_dropdown_select(elementSelected) {
    var tripLocationElement = document.getElementById("tripUpdateTripLocationInputID");
    if(tripLocationElement != undefined) {
        tripLocationElement.innerHTML = elementSelected.innerHTML.trim();
        menu_dropdown_showOrHide("tripUpdateLocationDropdownID");
    }
}

function trip_update_cancel() {
    var tripUpdateLayout = document.getElementById("tripUpdateLayoutID");
    if(tripUpdateLayout == undefined || tripUpdateLayout == null) {
        //Illegal access:
        return;
    }
    
    //Clean any leftover:
    common_errorOrInfoMessage_hide();
    document.getElementById(FRAGMENT_TRIP_UPDATE + "deleteCheckboxID").checked = false;
    common_deleteCheckbox_clicked(FRAGMENT_TRIP_UPDATE);
    
    var tripUpdateLocationDropdown = document.getElementById("tripUpdateLocationDropdownID");
    if(tripUpdateLocationDropdown != undefined && tripUpdateLocationDropdown != null) {
        tripUpdateLocationDropdown.style.display = "none";
    }
    
    tripUpdateLayout.style.display = "none";
}

function trip_update_validateAndSubmit() {
    common_errorOrInfoMessage_hide();
    var errorMessages = [];

    //Validate:
    var validateTripNameResultError = validate_tripName("tripUpdateTripNameInputID");
    if(validateTripNameResultError != null) {
        errorMessages.push(validateTripNameResultError);
    }
    if(errorMessages.length > 0) {
        common_errorOrInfoMessage_show(errorMessages, null);
        return;
    }
    
    //Call server:
    common_form_afterSubmit(FRAGMENT_TRIP_UPDATE);
    trip_update_ServiceRequest();
}

function trip_update_ServiceRequest() {
    if(mDevice_isSupportAJAX) {
        //Compose request:
        var isDeleted = false;
        var tripToUpdate = {};
        tripToUpdate.id = document.getElementById('tripID').value;
        tripToUpdate.userID = document.getElementById('userID').value;
        if(document.getElementById(FRAGMENT_TRIP_UPDATE + "deleteCheckboxID").checked) {
            isDeleted = true;

        } else {
            tripToUpdate.name = document.getElementById('tripUpdateTripNameInputID').value;
            tripToUpdate.note = document.getElementById('tripUpdateNoteInputID').value;
            tripToUpdate.location = document.getElementById('tripUpdateTripLocationInputID').innerHTML;
            
            //Status: Do this ONLY-IF status is not "COMPLETED"
            var tripStatusActiveInput = document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripStatusActiveInputID');
            if(tripStatusActiveInput != undefined || tripStatusActiveInput != null) {
                if(tripStatusActiveInput.checked == true) {
                    tripToUpdate.status = tripStatusActiveInput.value;
                } else {
                    tripToUpdate.status = document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripStatusCompletedInputID').value;;
                }
            }
        
            //Privacy:
            if(document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripPrivacyPublicInputID').checked == true) {
                tripToUpdate.privacy = document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripPrivacyPublicInputID').value;
            } else if(document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripPrivacyPrivateInputID').checked == true) {
                tripToUpdate.privacy = document.getElementById(FRAGMENT_TRIP_UPDATE + 'tripPrivacyPrivateInputID').value;;
            }
        }

        //Prepare and send as JSON request:
        var xmlhttpRequest = new XMLHttpRequest();
        xmlhttpRequest.onreadystatechange = function() {
            if (xmlhttpRequest.readyState == 4) {
                //Request finished and response is ready:
                trip_update_ServiceResponse(xmlhttpRequest);
            }
        };
        xmlhttpRequest.onerror = function() {
            common_form_AJAX_handleError(
                    FRAGMENT_TRIP_UPDATE, 
                    xmlhttpRequest.status + ":" + xmlhttpRequest.statusText, 
                    ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + xmlhttpRequest.statusText);
        };
        
        var urlTarget = null;
        if(isDeleted) {
            urlTarget = APP_ROOT_URI + "/trekcrumb/trip/delete/submit/AJAX";
        } else {
            urlTarget = APP_ROOT_URI + "/trekcrumb/trip/update/submit/AJAX/";
        }
        try {
            xmlhttpRequest.open("POST", urlTarget, true);
            xmlhttpRequest.setRequestHeader("Content-type","application/json");
            xmlhttpRequest.send(JSON.stringify(tripToUpdate));
        } catch(error) {
            common_form_AJAX_handleError(FRAGMENT_TRIP_UPDATE, error.message, ERROR_MSG_SERVER_DEFAULT_WITH_MESSAGE + error.message);
        }
    }
}

function trip_update_ServiceResponse(xmlhttpRequest) {
    if(xmlhttpRequest.readyState != 4) {
        //Not a server response, do nothing:
        return;
    }
    var tripUpdateLayout = document.getElementById("tripUpdateLayoutID");
    if(tripUpdateLayout == undefined || tripUpdateLayout == null) {
        //Invalid page - Since AJAX is async, it is possible user has moved away:
        return;
    }
    
    var isSuccess = common_form_AJAX_evalResponse(FRAGMENT_TRIP_UPDATE, xmlhttpRequest);
    if(isSuccess) {
        if(document.getElementById(FRAGMENT_TRIP_UPDATE + "deleteCheckboxID").checked) {
            //After Delete: Clean all possible Trip-related caches:
            common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserTrips");
            common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserPictures");
            common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserFavorites");
            common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, "UserComments");
            
            //Redirect to profile:
            window.location.href = APP_ROOT_URI + "/trekcrumb/user/retrieve/me";
        } else {
            //After Update: Redirect to server to retrieve newly updated Trip:
            var username = document.getElementById('usernameID').value;
            var tripID = document.getElementById('tripID').value;
            window.location.href = APP_ROOT_URI + "/trekcrumb/trip/retrieve/" + username + "/" + tripID;
        }
    }
}

/*
 * NOTE (7/2017):
 * Upon displaying the layout, we may force "tripShareInput" element select()/focus() to highlight
 * the URL value, similar to that of function trip_share_clicked(). BUT on MS Explorer 11 browser,
 * it threw JS error on select() function: "Incorrect function" (so stupid!). 
 * Thus, we removed them from here.
 */
function trip_share_init() {
    var tripShareLayout = document.getElementById("tripShareLayoutID");
    if(tripShareLayout == undefined || tripShareLayout == null) {
        console.error("trip_share_init() - ERROR: tripShareLayoutID not found!");
    }
    
    var tripShareInput = document.getElementById("tripShareInputID");
    if(tripShareInput.value == null || tripShareInput.value.trim() == "") {
        //Do this only-if its value has not been set:
        var url = common_url_getURL(false);
        tripShareInput.value = url;
    }

    tripShareLayout.style.display = "block";
}

function trip_share_close() {
    var tripShareLayout = document.getElementById("tripShareLayoutID");
    if(tripShareLayout != undefined && tripShareLayout != null) {
        tripShareLayout.style.display = "none";
    }
}

function trip_share_clicked(tripShareInputElement) {
    if(!common_window_isMobileBrowser()) {
        /*
         * For desktop browsers (non-mobile), select all URL text for
         * convenient copy-paste
         */
        tripShareInputElement.select();
        tripShareInputElement.focus();
    }
    
    /*
     * Else: For mobile browser, no need to auto select all. Let user
     * use his mobile browser built-in select all/copy/share menu icons.
     */
}


//END trekcrumb-trip.js