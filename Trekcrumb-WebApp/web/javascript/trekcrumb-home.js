"use strict";

/*
 * Atypical to other JS scripts, Home page has its own JS library: trekcrumb-home.js
 * and furthermore, it has its own collection of global variables. Most of these
 * global variables will be used throughout the live of Home page where their values are static. 
 * Without having them as global variables, they would be need to re-calculate to support
 * the dynamic support for page-scrolling and clicking within the Home page.
 *
 * We accepted this global variable setting considering the trekcrumb-home.js exists only
 * to serve page_home.jsp
 */
var mHome_PagePaddingTop = 0;
var mHome_SectionTopGreeting = 0;
var mHome_SectionTopTripList = 0;
var mHome_SectionTopPlaceList = 0;
var mHome_SectionTopPictureList = 0;
var mHome_SectionTopLogin = 0;
var mScrollingTimer = -1;

function home_init() {
    //Clean any home-related cache:
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, CACHE_KEY_TRIP_SEARCH_RESULT_PAGE + "0");
    common_localeStorage_clear(CACHE_TYPE_LOCALESTORAGE_SESSION, CACHE_KEY_PICTURE_LIST_RESULT_PAGE + "0");
    
    //Redirect to server:
    window.location.href = APP_ROOT_URI + "/trekcrumb/home";
}

/*
 * Home current (or last previous) state
 * -> To support the page-scroll top mechanic each fragment must have "height = 100%", which is
 *    the height of the screen.
 * -> mHome_PagePaddingTop
 *    --> The "padding-top" in home's body layout that we must include in calculating for page-scroll top 
 *        mechanic, due to the Container-Menu and Container-Title that can appear/disappear based on screen dimensions.
 *    --> If height is narrow, webapp would ONLY display Menu-Main bar (height = 58px). Else, webapp displays 
 *        both Menu-Main bar and Title-Main bar (height = 106px)
 */
function home_current() {
    //padding-top
    if(common_window_getHeight() <= DEVICE_SCREEN_HEIGHT_MAX_MEDIUM) {
        mHome_PagePaddingTop = 58;
    } else {
        mHome_PagePaddingTop = 106;
    }
    
    //Highlight selected menuPageScroll:
    home_scrollPage_stop();
}

/*
 * Home operation when user clicks one of the MenuPage navigations.
 * -> There is a smooth-scroll animation that requires JQuery Easing library.
 * -> For this to work, the document style.y-overflow should let it be default ("initial"),
 *    and cannot be set to "auto"
 */
function home_menuPageScrollClicked(event, menuPageElement) {
    if(event != null) {
        event.preventDefault();
    }

    //Regular JS element is NOT jQuery element, so:
    var menuPageElementJQuery = jQuery("#" + menuPageElement.id);
    var sectionTopPosition = jQuery(menuPageElementJQuery.attr("href")).offset().top - mHome_PagePaddingTop;
    /*
    console.log("home_menuPageScrollClicked() - Scrolling page to [" +  menuPageElement.id
                + "] at sectionTopPosition: " +  sectionTopPosition);
    */
    jQuery("html,body").stop(true, true).animate(
            {scrollTop: sectionTopPosition}, 
            1500, 
            "easeInOutExpo");
}

/*
 * Home operation when MenuPage navigations is selected.
 */
function home_menuPageScrollSelected(menuPageScrollID) {
    var menuPageScrollItems = document.getElementsByName("menuPageScrollContent");
    var numOfContents = menuPageScrollItems.length;
    for(var i = 0; i < numOfContents; i++) {
        var menuPageScroll = menuPageScrollItems[i];
        menu_ClickedReset(menuPageScroll);
    }
    
    var menuPageScrollContentClicked = document.getElementById(menuPageScrollID + "content");
    menu_Clicked(menuPageScrollContentClicked );
}

/*
 * Scrolls page to its utmost position Top (0 pixel)
 * -> To make this work and to support cross-browsers (IE, Firefox, Chrome), we must give 
 *    little time out to give the browser a chance to do the default scroll, and also 
 *    set BOTH <body> and <html> nodes scrollTop to "0".
 * -> Do not execute home_menuPageScrollSelected() to highlight the menuPageScroll, 
 *    instead, the registered scroll listener will be triggered automatically
 *    and execute the home_pageSectionScrolled()
 * -> NOTE 11/2015:
 *    Previously home_current() invoke this method to scroll page to top after Home page first load or refresh
 *    However, this approach introduces defects/issues:
 *    --> User click to view one of Trips from the Home Trip Gallery, then hit browser "Back"
 *       button but to find him at the top of Home page, instead of the last position he was in
 *       before browsing away.
 *    --> At some mobile browsers (Chrome, etc.), it created bug when user was in Home and scrolled
 *       up or down, the browser randomly forced him back to top of page.
 *    --> As the result of these issues, we no longer call this function after page load/refresh.
 */
function home_scrollPage_toTop() {
    setTimeout(function() {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }, 15);
}

/*
 * Scrolls page to its utmost position Bottom (Document height)
 * -> Do not execute home_menuPageScrollSelected() to highlight the menuPageScroll, 
 *    instead, the registered scroll listener will be triggered automatically
 *    and execute the home_pageSectionScrolled()
 */
function home_scrollPage_toBottom() {
    window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
}

/*
 * Home operation when user scrolls the page.
 * -> By standard, we must set EventListener to "scroll", and as long as user keeps
 *    scrolling, its target callback handler (function) will be invoked continously.
 * -> To prevent the core logic being executed continously while user still scrolls,
 *    we move it into a callback function ("home_scrollPage_stop"), and then 
 *    set window timeout to wait before we execute the core logic.
 * -> For this to work, the document style.y-overflow should let it be default ("initial"),
 *    and cannot be set to "auto"
 */
function home_scrollPage_trigger() {
    if (mScrollingTimer != -1) {
        clearTimeout(mScrollingTimer);
    }

    mScrollingTimer = window.setTimeout("home_scrollPage_stop()", HOME_PAGESCROLL_TIMEOUT);
}

/*
 * Home operation when user (finish) scroll the page.
 */
function home_scrollPage_stop()  {
    var scrollTopCurrent = $(document).scrollTop();
    /* 
    console.log("home_scrollPage_stop() - Page is SCROLLED! Current scrollTop position: " +  scrollTopCurrent);
    */
    
    /*
     * Special case when scroll reach bottom of page: The Footer section does not have enough height
     * dimension for the scrollTop to be set. Therefore, we would calculate if the scroll has reached
     * the bottom of page, then skip the rest of the logic.
     */
    if(scrollTopCurrent + common_window_getHeight() == $(document).height()) {
        home_menuPageScrollSelected("menuPageScrollFooterID");
        return;
    }
    
    home_calculateSectionTopPosition();
    if(scrollTopCurrent < mHome_SectionTopTripList ) {
        home_menuPageScrollSelected("menuPageScrollGreetingID");
    } else if(scrollTopCurrent >= mHome_SectionTopTripList && scrollTopCurrent < mHome_SectionTopPlaceList) {
        home_menuPageScrollSelected("menuPageScrollTripListID");
    } else if(scrollTopCurrent >= mHome_SectionTopPlaceList && scrollTopCurrent < mHome_SectionTopPictureList) {
        home_menuPageScrollSelected("menuPageScrollPlaceListID");
    } else if(scrollTopCurrent >= mHome_SectionTopPictureList && scrollTopCurrent < mHome_SectionTopLogin) {
        home_menuPageScrollSelected("menuPageScrollPictureListID");
    } else if(scrollTopCurrent >= mHome_SectionTopLogin) {
        home_menuPageScrollSelected("menuPageScrollLoginID");
    }
}

/*
 * Calculates the values of each section/fragment position.top (in pixels)
 * -> NOTE 1/2016:
 *    Previously home_current() invoke this method ONCE during Home page load/refresh to find all position.top values 
 *    and store them as global variables we use later to support page-scroll top mechanic and other dynamic behavior.
 *    However, this approach introduces defects/issues:
 *    --> Due to dynamic heights of TripList and PictureList whose height values cannot be set ahead of time and whose
 *        contents are populated async, they change ALL position.top values of ALL sections beneath them. 
 *    --> To resolve the defect, we now must invoke it each time we need them (example: home_scrollPage_stop())
 *        and recalculate these position.top values
 */
function home_calculateSectionTopPosition() {
    mHome_SectionTopGreeting = $('#homeGreetingSectionID').position().top;
    mHome_SectionTopTripList = $('#homeTripListSectionID').position().top - mHome_PagePaddingTop;
    mHome_SectionTopPlaceList = $('#homePlaceListSectionID').position().top - mHome_PagePaddingTop;
    mHome_SectionTopPictureList = $('#homePictureListSectionID').position().top - mHome_PagePaddingTop;
    mHome_SectionTopLogin = $('#homeLoginSectionID').position().top - mHome_PagePaddingTop;
    /*
    console.log("home_calculateSectionTopPosition() - SectionTopGreeting: " +  mHome_SectionTopGreeting
        + ", SectionTopTripList: " +  mHome_SectionTopTripList
        + ", SectionTopPlaceList: " +  mHome_SectionTopPlaceList
        + ", SectionTopLogin: " +  mHome_SectionTopLogin); 
    */
}

function home_populateFragments() {
    //Calculate how many gallery items to display:
    var deviceScreenWidth = common_window_getWidth();
    var screenWidthMinimumToDisplayListItemsAligned = (HOME_GALLERYITEM_WIDTH_INCLUDING_MARGIN_MAX * 2) + 20;
    if(deviceScreenWidth <= screenWidthMinimumToDisplayListItemsAligned) {
        mHome_numOfListItemsToDisplay = HOME_GALLERYITEM_NUM_TO_DISPLAY_DEFAULT;
    } else {
        mHome_numOfListItemsToDisplay = (deviceScreenWidth / HOME_GALLERYITEM_WIDTH_INCLUDING_MARGIN_MAX >> 0) * HOME_GALLERYITEM_NUM_OF_ROWS_DISPLAY_MAX;
    }
   
    trip_home_search_Current();
    place_home_placeListAsTagDisplayContent(null);
    picture_list_home_current();
}


/****** UNUSED *****
 * Home operation when user clicks one of the MenuPage navigations. 
 * -> An ALTERNATIVE to jQuery function above.
 * -> In this example, it scrolls back up to top.
 * -> To use it: <button onclick="home_menuPageScrollToTop()">Back to Top</button>
 * -> Ref: http://stackoverflow.com/questions/4210798/how-to-scroll-to-top-of-page-with-javascript-jquery
 */
var STEP_TIME = 20;
var SPEED = 1000;
function home_menuPageScrollToTop() {
    var htmlElement = document.documentElement;
    var bodyElement = document.body;

    var topOffset = bodyElement.scrollTop || htmlElement.scrollTop;
    var stepAmount = topOffset;

    SPEED && (stepAmount = (topOffset * STEP_TIME)/SPEED);

    home_menuPageScrollToTop_animate(topOffset, stepAmount);
}

function home_menuPageScrollToTop_animate(initPos, stepAmount) {
    var newPos = initPos - stepAmount > 0 ? initPos - stepAmount : 0;
    document.body.scrollTop = document.documentElement.scrollTop = newPos;

    newPos && setTimeout(function() {
        home_menuPageScrollToTop_animate(newPos, stepAmount);
    }, STEP_TIME);
}
/******* END OF UNUSED *****/

//END trekcrumb-home.js