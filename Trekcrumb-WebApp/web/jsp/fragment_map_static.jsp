<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%-- STATIC MAP TO DISPLAY LOCATION GIVEN (LAT,LAN) --%>
<div id="mapStaticCanvasID" class="body-mapStatic" 
     style="text-align:center; display:block">
    <img id="mapStaticImageID" />
</div>

<%-- END fragment_map_static.jsp --%>