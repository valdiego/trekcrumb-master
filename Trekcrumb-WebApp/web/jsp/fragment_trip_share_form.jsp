<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<div id="tripShareLayoutID" class="body-disabled-background" 
     style="display:none;">
    <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
            with a shade, making the previous main content un-operable (all clickables 
            become non functional).
    --%>
    <div class="body-inside-border body-form-embedded-container font font-size-normal" >
        <div class="body-form-embedded-content" >
            <%-- SUB-TITLE AND GREETING --%>
            <jstl-fmt:message key="label_trip_share" var="labelText" bundle="${msgprop}"/>  
            <jsp:include page="widget_menuClose.jsp" >
                <jsp:param name="id" value="FRAGMENT_TRIP_SHARE" />
                <jsp:param name="labelText" value="${labelText}" />
                <jsp:param name="closeMethodName" value="trip_share_close()" />
            </jsp:include>
            <div class="border-bottom-black" style="margin-bottom:20px; "></div>
            <span><jstl-fmt:message key="info_trip_share_greeting" bundle="${msgprop}"/> </span>

            <%-- FORM --%>
            <div>
                <div style="margin-top:20px;">
                    <textarea id="tripShareInputID"
                              readOnly="true"
                              cols="50" rows="4" 
                              onclick="trip_share_clicked(this)"></textarea>
                </div>                 

                <%-- FUNCTIONAL BUTTONS --%>
                <jstl-fmt:message key="label_common_close" var="submitLabel" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                    <jsp:param name="submitLabel" value="${submitLabel}" />
                    <jsp:param name="submitMethodName" value="trip_share_close()" />
                    <jsp:param name="isCancelHide" value="true" />
                </jsp:include>
            </div><%-- END formID --%>

        </div><%-- END body-form-embedded-content --%>
    </div><%-- END body-form-embedded-container --%>
</div><%-- END tripShareLayoutID --%>


<%-- END fragment_trip_share_form.jsp --%>