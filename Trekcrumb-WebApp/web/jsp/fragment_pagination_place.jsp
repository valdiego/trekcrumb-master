<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="java.util.List" %>
<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.Place" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPagePlace = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);

    String listOfPlacesJSON = null;
    if(trip_forPagePlace != null && trip_forPagePlace.getNumOfPlaces() > 0) {
        List<Place> listOfPlaces = trip_forPagePlace.getListOfPlaces();
        listOfPlacesJSON = (new ObjectMapper()).writeValueAsString(listOfPlaces);
    }
%>

<div id="paginationPlaceLayoutID"
     var_pagePlace_numOfTotalPlaces = "0"
     var_pagePlace_placeIndexCurrent = "0">
    <input id="pagePlace_listOfPlacesID" type="hidden" />

    <script>
        pagePlace_init(<%=listOfPlacesJSON%>);
    </script>
</div>


<%-- END fragment_pagination_place.jsp --%>