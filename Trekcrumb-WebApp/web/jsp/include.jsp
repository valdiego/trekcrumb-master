<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    boolean isUserSessionActive = WebAppUtil.isUserSessionActive(request);   
%>

<%-- DEFAULT --%>
<%@ include file="include_header.jsp" %>
<%@ include file="include_menu.jsp" %>
<%@ include file="include_errorAndInfo.jsp" %>

<%-- EMBEDDED FRAGMENTS:
     - Parts of global fragments (accessable from any page) but must meet conditions
--%>
<% if(isUserSessionActive) {%>
       <%@ include file="fragment_geoLoc_currentLocation_form.jsp" %>
       <%@ include file="fragment_trip_create_form.jsp" %>
<% } %>

<%-- END include.jsp --%>