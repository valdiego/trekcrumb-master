<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%
    int httpResponseStatus = response.getStatus();
    if(httpResponseStatus == -1) {
        httpResponseStatus = 500;
    }
    
    String serverErrorMessage = (String)request.getAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
    if(ValidationUtil.isEmpty(serverErrorMessage)) {
        serverErrorMessage = "Woops! Unexpected system error when saving or loading data.";
    }
%>

<html>
    <body>
        <div>
            <p/>
            <h1 style="color:red;">Status: <%=httpResponseStatus%></h1>
            <p/>
            <h3><%=serverErrorMessage%></h3>
        </div>
    </body>
</html>