<%@ include file="include.jsp" %>

<%--HOME PAGE
     1. It has feature of "One-Page Section Scroll" navigation (i.e., smooth scroll) between page's sections. 
        -> In order for the feature to work, we must declare <HTML>, <BODY> and the element(s) 
           height = 100%. This enables individual section to fill the entire screen height.
        -> Do NOT declare width = 100% on <HTML> nor <BODY> since it can mess up the left/right margin and
           children nodes' widths consistency.
     2. Main body "homeLayoutID": 
        -> See: "container-body-home" CSS class.
     3. Greeting Section:
        -> Set padding-top = "xx" value since it is the first section such that it would sit right
           beneath the Menu-Main and Title-Main whose positions are "fixed".
     4. Trip Gallery Section:
        -> Set height = "auto" (not 100%) to enable the layout to expand/shrink as it contains
           a list of trip rows.
           
     5. Metadata Search Engine: 
        > To support search engine (Google, etc.)
        > In addition to markup format composed in include_headerSearchEngine.jsp, we support 
          MICRODATA markup format as well that must be included in particular elements inside the page. 
          These metadata formats are based on schema.org standards and noted in "itemxyz" property/attribute. 
        > When some of required microdata tags have no place in actual content, we unfortunately must
          put them contained inside hidden "microdataTagID" section.
        > REF: http://schema.org/docs/gs.html
 --%>
<%@ page import="com.trekcrumb.webapp.utility.WebAppConstants" %>
<%
    //Page identity:
    String pageIdentityType_forHome = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TYPE);
    String pageURL_forHome = null;
    String pageTitle_forHome = null;
    String pageSummary_forHome = null;
    String pageImageURL_forHome = null;
    if(pageIdentityType_forHome != null) {
        pageTitle_forHome = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TITLE);
        if(pageTitle_forHome == null) {
            pageTitle_forHome = WebAppConstants.APP_TITLE;
        }
        
        pageSummary_forHome = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_SUMMARY);
        if(pageSummary_forHome == null) {
            pageSummary_forHome = WebAppConstants.APP_SUMMARY;
        }
        
        pageURL_forHome = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_URL);
        pageImageURL_forHome = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_IMAGEURL);
    }
    
%>
<body style="height:100%">
    <%-- PAGE TITLE --%>
    <div class="container-title">
        <span id="pageTitleID" class="font font-style-title"><jstl-fmt:message key="label_common_home" bundle="${msgprop}"/></span>
    </div>

    <%-- MAIN BODY --%>
    <div id="homeLayoutID" class="container-body-home font" 
         itemscope itemtype="http://schema.org/Organization" itemid="<%=pageURL_forHome %>">
    
        <%-- MICRODATA TAGS HIDDEN SECTION --%>
        <div id="microdataTagID" style="display:none;">
            <meta itemprop="url" content="<%=pageURL_forHome %>" />
            <meta itemprop="name" content="Trekcrumb.com" />
            <meta itemprop="legalName" content="Trekcrumb" />
            <meta itemprop="description" content="<%=pageSummary_forHome %>" />
            <meta itemprop="logo" content="<%=pageImageURL_forHome %>" />
        </div>
    
        <section id="homeGreetingSectionID" >
            <%@ include file="fragment_home_greeting.jsp" %>
        </section>

        <section id="homeTripListSectionID" >
            <%@ include file="fragment_home_trip_gallery.jsp" %>
        </section>

        <section id="homePlaceListSectionID" >
            <%@ include file="fragment_home_place_gallery.jsp" %>
        </section>

        <section id="homePictureListSectionID" >
            <%@ include file="fragment_home_picture_gallery.jsp" %>
        </section>

        <section id="homeLoginSectionID">
            <%@ include file="fragment_home_login.jsp" %>
        </section>

        <section id="homeFooterSectionID" >
            <jsp:include page="widget_footer.jsp" />
        </section>
        
        <%-- MENU PAGE --%>
        <div id="homeMenuPageID" class="body-white-transparent">
            <a id="menuPageScrollGreetingID" 
               class="hoverhints-menuIcon"
               href="#homeGreetingSectionID"
               onmouseover="menu_hover_LabelShow(this)"
               onmouseout="menu_hover_LabelHide(this)"               
               onclick="home_menuPageScrollClicked(event, this)">
                <jsp:include page="widget_menuPageScrollButton.jsp" >
                    <jsp:param name="id" value="menuPageScrollGreetingID" />
                    <jsp:param name="title" value="Greeting" />
                </jsp:include>

                <div id="menuPageScrollGreetingIDHover" 
                      class="hoverhints-menuIcon-text-container-leftCenter" 
                      style="display: none;">
                    <div class="hoverhints-menuIcon-text-body-leftCenter"
                         style="min-height:20px;" >Greeting</div>
                </div>
            </a>
            
            <a id="menuPageScrollTripListID"  
               class="hoverhints-menuIcon"
               href="#homeTripListSectionID"
               onmouseover="menu_hover_LabelShow(this)"
               onmouseout="menu_hover_LabelHide(this)"               
               onclick="home_menuPageScrollClicked(event, this)">
                <jsp:include page="widget_menuPageScrollButton.jsp" >
                    <jsp:param name="id" value="menuPageScrollTripListID" />
                    <jsp:param name="title" value="Trek Gallery" />
                </jsp:include>

                <div id="menuPageScrollTripListIDHover" 
                      class="hoverhints-menuIcon-text-container-leftCenter" 
                      style="display: none;">
                    <div class="hoverhints-menuIcon-text-body-leftCenter"
                         style="min-height:20px;" >Trek Gallery</div>
                </div>
            </a>

            <a id="menuPageScrollPlaceListID"  
               class="hoverhints-menuIcon"
               href="#homePlaceListSectionID"
               onmouseover="menu_hover_LabelShow(this)"
               onmouseout="menu_hover_LabelHide(this)"               
               onclick="home_menuPageScrollClicked(event, this)">
                <jsp:include page="widget_menuPageScrollButton.jsp" >
                    <jsp:param name="id" value="menuPageScrollPlaceListID" />
                    <jsp:param name="title" value="Place Gallery" />
                </jsp:include>

                <div id="menuPageScrollPlaceListIDHover" 
                      class="hoverhints-menuIcon-text-container-leftCenter" 
                      style="display: none;">
                    <div class="hoverhints-menuIcon-text-body-leftCenter"
                         style="min-height:20px;" >Place Gallery</div>
                </div>
            </a>

            <a id="menuPageScrollPictureListID"  
               class="hoverhints-menuIcon"
               href="#homePictureListSectionID"
               onmouseover="menu_hover_LabelShow(this)"
               onmouseout="menu_hover_LabelHide(this)"               
               onclick="home_menuPageScrollClicked(event, this)">
                <jsp:include page="widget_menuPageScrollButton.jsp" >
                    <jsp:param name="id" value="menuPageScrollPictureListID" />
                    <jsp:param name="title" value="Picture Gallery" />
                </jsp:include>

                <div id="menuPageScrollPictureListIDHover" 
                      class="hoverhints-menuIcon-text-container-leftCenter" 
                      style="display: none;">
                    <div class="hoverhints-menuIcon-text-body-leftCenter"
                         style="min-height:20px;" >Picture Gallery</div>
                </div>
            </a>

            <a id="menuPageScrollLoginID"  
               class="hoverhints-menuIcon"
               href="#homeLoginSectionID"
               onmouseover="menu_hover_LabelShow(this)"
               onmouseout="menu_hover_LabelHide(this)"               
               onclick="home_menuPageScrollClicked(event, this)">
                <jsp:include page="widget_menuPageScrollButton.jsp" >
                    <jsp:param name="id" value="menuPageScrollLoginID" />
                    <jsp:param name="title" value="Login" />
                </jsp:include>

                <div id="menuPageScrollLoginIDHover" 
                      class="hoverhints-menuIcon-text-container-leftCenter" 
                      style="display: none;">
                    <div class="hoverhints-menuIcon-text-body-leftCenter"
                         style="min-height:20px;" >Login</div>
                </div>
            </a>

            <a id="menuPageScrollFooterID"  
               class="hoverhints-menuIcon"
               href="#homeFooterSectionID"
               onmouseover="menu_hover_LabelShow(this)"
               onmouseout="menu_hover_LabelHide(this)"               
               onclick="home_scrollPage_toBottom()">
                <jsp:include page="widget_menuPageScrollButton.jsp" >
                    <jsp:param name="id" value="menuPageScrollFooterID" />
                    <jsp:param name="title" value="Support" />
                </jsp:include>

                <div id="menuPageScrollFooterIDHover" 
                      class="hoverhints-menuIcon-text-container-leftCenter" 
                      style="display: none;">
                    <div class="hoverhints-menuIcon-text-body-leftCenter"
                         style="min-height:20px;" >Support</div>
                </div>
            </a>
        </div>    

        <%@ include file="widget_disableBackground.jsp" %>
    </div><%-- END homeLayoutID --%>
    
    <script>
        //Setup page initiations:
        window.onload = function() {
            home_current();
        };

        //On scroll-page:
        document.addEventListener("scroll", home_scrollPage_trigger, false);
        
        /* 
         * On resize:
         * -> Do NOT put on <body onresize=""> tag anymore because it could be executed
         *    before the page completes resizing, causing getting invalid element's 
         *    values/states (offsetWidth, offsetHeight, etc) which were previous values.
         * -> Even in here, we MUST put a 'waiting period' to make sure client browser has 
         *    completely configure all DOM elements during resize for the same reason. 
         */
        window.addEventListener("resize", function() {
            setTimeout(function() {
                home_current();
            }, (1000));
        });
    
        home_populateFragments();
    </script>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_home.jsp --%>