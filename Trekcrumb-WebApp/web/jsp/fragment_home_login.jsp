<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.UserSession" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    boolean isUserSessionActive_forHomeLogin = WebAppUtil.isUserSessionActive(request);
    String userLoginFullname_forHomeLogin = null; 
    if(isUserSessionActive_forHomeLogin) {
        userLoginFullname_forHomeLogin = ((UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser().getFullname();
    }
%>

<div id="homeLoginFragmentLayoutID">
     
    <%-- Greeting Message: See its layout CSS for dynamic positioning. --%>
    <div id="homeLoginFragmentMessageID" class="body-white-transparent font font-style-greeting" >
        
<%      if(isUserSessionActive_forHomeLogin) { %>
            <jstl-fmt:message key="info_home_greeting_login_userInSession" bundle="${msgprop}">
                <jstl-fmt:param value="<%=userLoginFullname_forHomeLogin%>"/>
            </jstl-fmt:message>
            

<%      } else { %>
            <jstl-fmt:message key="info_home_greeting_login" bundle="${msgprop}"/>
            <div id="homeLoginSignUpID" class="body-clickable font-bold"
                 onclick="user_signup_init()" >
                <jstl-fmt:message key="label_common_signup" bundle="${msgprop}"/>
            </div>

            <div id="homeLoginFormID">
                <%@ include file="fragment_user_login_form.jsp" %>
            </div>
<%      } %>              
    </div>
</div>

<%-- END fragment_home_login.jsp --%>