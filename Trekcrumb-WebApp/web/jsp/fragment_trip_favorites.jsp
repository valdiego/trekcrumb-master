<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%-- NOTE:
     1. This fragment repesents the list of USERS who are favoriting the Trip in question. 
     
--%>
<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.enums.TripPrivacyEnum" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forFavorites = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
%>
<div id="favoriteByTripLayoutID" class="font" 
     style="margin:0px 10px 0px 10px;"
     var_favoriteByTrip_numRecordsTotal = "<%=trip_forFavorites.getNumOfFavorites()%>"
     var_favoriteByTrip_numOfRowsMax = "<%=CommonConstants.RECORDS_NUMBER_MAX%>"
     var_favoriteByTrip_offsetCurrent = "0" >

    <%-- HEADER AND SHOW/HIDE MENUS --%>
    <div id="headerfavoriteByTripContentID" class="body-clickable"
         onclick="favorite_retrieveByTrip_init()">
        <span id="menuShowfavoriteByTripContentID" class="font-size-large fa fa-plus-square-o" 
              style="vertical-align:middle; margin-right:10px;"></span>
        <span id="menuHidefavoriteByTripContentID" class="font-size-large fa fa-minus-square-o" 
              style="vertical-align:middle; margin-right:10px; display:none;"></span>
        <span class="font-bold"><jstl-fmt:message key="label_favorite" bundle="${msgprop}"/></span>
        <span id="favoriteByTripHeaderNumRecordsTotalID" class="font-bold" style="margin-left:10px;"><%=trip_forFavorites.getNumOfFavorites()%></span>
    </div>

<%  if(trip_forFavorites.getPrivacy() == TripPrivacyEnum.PRIVATE) { %>    
        <span id="favoriteByTripGreetingID" style="display:none;"><jstl-fmt:message key="label_favorite_list_users_private" bundle="${msgprop}"/></span>
<%  } else { %>
        <span id="favoriteByTripGreetingID" style="display:none;"><jstl-fmt:message key="label_favorite_list_users" bundle="${msgprop}"/></span>
<%  } %>
    
    <%-- CONTENTS:
         -> This layout will be wiped out and populated by rows --%>
    <div id="favoriteByTripContentID" style="display:none;"></div>
    
    <%-- PROGRESS WIDGET --%>
    <jsp:include page="widget_progressBar.jsp" >
        <jsp:param name="id" value="FRAGMENT_TRIP_FAVORITE" />
        <jsp:param name="color" value="black" />
        <jsp:param name="size" value="small" />
        <jsp:param name="marginTop" value="5" />
        <jsp:param name="labelText" value="Loading More Favorites ..." />
    </jsp:include>
    
    <%-- MENU MORE --%>
    <jsp:include page="widget_menuMoreNext.jsp" >
        <jsp:param name="widgetPrefixID" value="favoriteByTrip" />
        <jsp:param name="menuNextMethodName" value="favorite_retrieveByTrip_next()" />
        <jsp:param name="isHideByDefault" value="true" />
    </jsp:include>
    
    <%-- FAVORITE USER LIST ROW TEMPLATE WIDGET --%> 
    <jsp:include page="widget_row_favorite_usersList.jsp" />
    
</div>
<script>
    common_progressBarWidget_hide(FRAGMENT_TRIP_FAVORITE);
    
    /*
     * If returned Trip comes with handy List of Favorites, let's display them and we would save
     * one user request to retrieve them from back-end service.
     * Note: This should apply for offset = 0.
     */
<%  if(trip_forFavorites.getNumOfFavorites() > 0 && trip_forFavorites.getListOfFavoriteUsers() != null) { 
        ObjectMapper jacksonObjMapper = new ObjectMapper();
        String listOfFavoriteUsersJSON = jacksonObjMapper.writeValueAsString(trip_forFavorites.getListOfFavoriteUsers()); %>
        
        favorite_retrieveByTrip_displayRows(<%=listOfFavoriteUsersJSON%>);
        favorite_retrieveByTrip_paginationUpdate(true);
<%  } %>
    
</script>
        
<%-- END fragment_trip_favorites.jsp --%>