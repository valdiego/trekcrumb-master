<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.CommonUtil" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%
    String userImageURL = null;
    String userImageName = request.getParameter("userImageName");
    if(!ValidationUtil.isEmpty(userImageName)) {
        userImageURL = CommonConstants.FS_SERVLET_URL_GETIMAGE_PROFILE + userImageName;
    }
    
    String userFullnameValue = request.getParameter("userFullnameValue");
    if(userFullnameValue == null) {
        userFullnameValue = "";
    }

    String usernameValue = request.getParameter("usernameValue");
    if(usernameValue == null) {
        usernameValue = "";
    }
    
    String userThumbnailCustomCSS = request.getParameter("userThumbnailCustomCSS");
    if(userThumbnailCustomCSS == null) {
        userThumbnailCustomCSS = "";
    }
%>
<div id="userProfileThumbnailID" class="font font-size-small <%=userThumbnailCustomCSS %>" >
    <div style="display:inline-block;">
<%      if(userImageURL != null) { %>        
            <img id="userThumbnailImageID" class="image image-profile-thumbnail" src="<%=userImageURL%>" />
<%      } else { %>
            <span id="userThumbnailImageDefaultID" class="image image-profile-thumbnail fa fa-user"></span>
            <img id="userThumbnailImageID" class="image image-profile-thumbnail" style="display:none;" />
<%      } %>     
    </div>
    
    <div style="padding-left:10px; vertical-align:top; display:inline-block;" >
        <span id="userThumbnailFullnameID" class="font-bold"><%=userFullnameValue %></span><br/>
        <span id="userThumbnailUsernameID" class="font-italic"><%=usernameValue %></span>
    </div>
</div>


<%-- END widget_userThumbnail.jsp --%>