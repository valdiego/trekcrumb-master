<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String submitLabel = request.getParameter("submitLabel");

    String submitButtonIDPrefix = request.getParameter("submitButtonIDPrefix");
    if(submitButtonIDPrefix == null || submitButtonIDPrefix.trim() == "") {
        submitButtonIDPrefix = "";
    }

    String submitMethodName = request.getParameter("submitMethodName");
    if(submitMethodName == null) {
        submitMethodName = "";
    }
    
    boolean isCancelHide = Boolean.parseBoolean(request.getParameter("isCancelHide"));
    
    String cancelButtonIDPrefix = request.getParameter("cancelButtonIDPrefix");
    if(cancelButtonIDPrefix == null || cancelButtonIDPrefix.trim() == "") {
        cancelButtonIDPrefix = "";
    }

    String cancelMethodName = request.getParameter("cancelMethodName");
    if(cancelMethodName == null || cancelMethodName.trim() == "") {
        cancelMethodName = "common_cancel()";
    }
%>

<%-- NOTE:
     1. Unless you intend it, DO NOT put the buttons inside <FORM> tag because
        the form will directly submit itself when the submit button is clicked
        bypassing the submit's onclick() function call. 
     2. Its "position = absolute" and always attempt to be placed at the bottom
        of its parent layout. To work, the parent layout must declare "position = relative".
--%>
<div id="functionalButtonsID" 
     class="body-white"
<%   if(isCancelHide) { %>
         style="text-align:right;"
<%   } %>
     >

    <%-- CANCEL BUTTON --%>
<%  if(!isCancelHide) { %>
        <input id="<%=cancelButtonIDPrefix%>CancelBtnID"
               type="submit" 
               class="button-passive"
               style="width:45%"
               name="submit"
               value="Cancel"
               onclick="<%=cancelMethodName%>">
<%  } %>

    <%-- SUBMIT BUTTON --%>
    <input id="<%=submitButtonIDPrefix%>SubmitBtnID"
           type="submit" 
           class="button-active"
           style="width:45%"
           name="submit"
           value="<%=submitLabel%>"
           onclick="<%=submitMethodName%>">
</div>  

<%-- END widget_menuCancelAndSubmit.jsp --%>