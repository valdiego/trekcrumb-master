<%@ include file="include.jsp" %>

<body>
    <script>
        //TODO: This is causing lots of trouble when user click "BACK" button
        //common_disableBackButton();
    </script>

    <%-- PAGE TITLE --%>
    <div class="container-title">
        <div class="font font-style-title">
            <span><jstl-fmt:message key="label_common_signup" bundle="${msgprop}"/></span>
        </div>
    </div>

    <%-- MAIN BODY --%>
    <div class="container-body"
         style="bottom:50px;">
        <div id="userSignupCookieEnabledID" class="tab-container">
            <%-- MENU TAB --%>
            <div class="tab-menu-bar font font-size-normal">
                <div id="menuUserLoginID" class="tab-menu-bar-item"
                     onclick="user_signup_displaySelectedTab(FRAGMENT_USER_LOGIN)" >
                    <jstl-fmt:message key="label_common_login" bundle="${msgprop}"/>
                </div>
                <div id="menuUserCreateID" class="tab-menu-bar-item"
                     onclick="user_signup_displaySelectedTab(FRAGMENT_USER_CREATE)" >
                    <jstl-fmt:message key="label_common_join" bundle="${msgprop}"/>
                </div>
                <div id="menuUserForgetID" class="tab-menu-bar-item"
                     onclick="user_signup_displaySelectedTab(FRAGMENT_USER_FORGET)" >
                    <jstl-fmt:message key="label_common_forget" bundle="${msgprop}"/>
                </div>
            </div>
            <div class="tab-body">
                <%-- EMBEDDED FORMS --%>
                <%@ include file="fragment_user_login_form.jsp" %>
                <%@ include file="fragment_user_create_form.jsp" %>
                <%@ include file="fragment_user_forget_form.jsp" %>
            </div>
        </div>
        
        <div id="userSignupCookieDisabledID" class="body-inside-border font font-size-normal"
             style="display:none;">
        </div>
    </div>

    <%-- FOOTER --%>
    <jsp:include page="widget_footer.jsp" >
        <jsp:param name="isPositionFixed" value="true" />
    </jsp:include>

    <%-- DISABLING BACKGROUND --%>
    <%@ include file="widget_disableBackground.jsp" %>
    
    <script>
        if(mDevice_isSupportCookie) {
            user_signup_displaySelectedTab(null);

        } else {
            common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED], null); 
            document.getElementById("userSignupCookieEnabledID").style.display = "none";
            var userSignupCookieDisabled = document.getElementById("userSignupCookieDisabledID");
            userSignupCookieDisabled.style.display = "block";
            userSignupCookieDisabled.innerHTML = ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED;
        }
    </script>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_user_signup.jsp --%>