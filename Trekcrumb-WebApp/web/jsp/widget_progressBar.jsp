<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<head>
    <link type="text/css" rel="stylesheet"  href="${pageContext.request.contextPath}/css/widget-progressSpinner.css"/>
</head>

<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    /*
     * Main layout's custom ID that will function as a prefix, if provided.
     */
    String idPrefix = request.getParameter("id");
    if(idPrefix == null) {
        idPrefix = "";
    }

    /*
     * Widget color follows the classes on the CSS: 'black' or 'white'.
     */
    String widgetColor = request.getParameter("color");
    if(widgetColor != null && widgetColor.trim() != "" && widgetColor.trim().equalsIgnoreCase("white")) {
        widgetColor = "white";
    } else {
        //Default:
        widgetColor = "black";
    }
    
    /*
     * Widget (diameter) size follows the classes on the CSS: 'small' (30px) or 'large' (60px).
     * The container height is adjusted based on this size plus its top/bottom border sizes per
     * the specifics on the CSS configuration.
     */
    int containerHeight = 0;
    String widgetSize = request.getParameter("size");
    if(widgetSize != null && widgetSize.trim() != "" && widgetSize.trim().equalsIgnoreCase("large")) {
        widgetSize = "large";
        containerHeight = 80;
    } else {
        //Default:
        widgetSize = "small";
        containerHeight = 50;
    }
    
    /*
     * Widget Position either centered on the screen, or center on current layout section (but not
     * necessarily on screen) or not centered (left).
     * If it is center-screen, limit its width and set marginLeft for half of the width so it will be
     * centered on screen.
     */
    String widgetWidth = "auto";
    String widgetMarginLeft = "0px";
    String widgetPosition = request.getParameter("position");
    if(widgetPosition != null && widgetPosition.trim() != "") {
        if(widgetPosition.trim().equalsIgnoreCase("center-screen")) {
            widgetPosition = "body-position-center-screen";
            widgetWidth = "200px";
            widgetMarginLeft = "-100px";
            
        } else if(widgetPosition.trim().equalsIgnoreCase("center-layout")) {
            widgetPosition = "body-position-center";
        } else {
            widgetPosition = "";
        }
    } else {
        widgetPosition = "";
    }
    
    /*
     * Widget margin-top value, if required. If you set widgetPosition = 'center-screen', it is
     * not recommended to set margin-top.
     */
    int marginTop = 0;
    String marginTopStr = request.getParameter("marginTop");
    if(marginTopStr != null && marginTopStr.trim() != "") {
        try {
            marginTop = Integer.parseInt(marginTopStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }

    String labelText = request.getParameter("labelText");
    if(ValidationUtil.isEmpty(labelText)) {
        labelText = MessageResourceUtil.getMessageValue("info_common_pleaseWait", null, null);
    }
%>

<div id="<%=idPrefix%>ProgressBarLayoutID" 
     class="<%=widgetPosition%> container-spinner-with-circle-line"
     style="text-align:center; height:auto; line-height:normal; width:<%=widgetWidth%>; margin-left:<%=widgetMarginLeft%>; 
            padding-top:<%=marginTop%>px;">
     
    <div class="spinner-with-circle-line <%=widgetColor%> <%=widgetSize%>" 
         style="display:inline-block;"></div>
    
<%  if(labelText != null && labelText.trim() != "") { %>
        <div style="margin-top:10px;">
<%          if(widgetColor.equalsIgnoreCase("white")) { %>        
                <span id="progressBarLabelID" class="font font-normal font-color-white"><%=labelText%></span>
<%          } else { %>        
                <span id="progressBarLabelID" class="font font-normal"><%=labelText%></span>
<%          } %>                
        </div>
<%  } %>
</div> 


<%-- END widget_progressBar.jsp --%>