<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String idPrefix = request.getParameter("id");
    if(idPrefix == null) {
        idPrefix = "";
    }

    String title = request.getParameter("title");
    if(title == null) {
        title = "";
    }
%>

<%-- MenuPageScroll widget:
     1. Represents a button (bullet) to scroll within page
     2. Structure:
        -> The outer <div class="menu-scroll-navigation" serves as a parent layout
        -> In order to display the bullet itself (smaller than the parent layout)
           centered vertically and horizontally, we must display the inner divs as 
           table and table-cell. 
--%>
<div title="<%=title%>" class="menu-scroll"
     onmousedown="menu_Clicked(this)"
     onmouseup="menu_ClickedReset(this)">
    <div class="menu-scroll-content-outer" style="display: table;">
        <div style="display: table-cell; vertical-align: middle;">
            <div id="<%=idPrefix%>content" 
                 name="menuPageScrollContent" 
                 class="menu-scroll-content-inner">&nbsp;</div>
        </div>
    </div>
</div>

<%-- END widget_menuPageScrollButton.jsp --%>