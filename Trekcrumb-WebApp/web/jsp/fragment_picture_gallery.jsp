<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPicGallery = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
%>
<% if(trip_forPicGallery != null && trip_forPicGallery.getNumOfPictures() > 1) { %>
    
    <%-- ROW TEMPLATE: 
         1. To be copied and populated into the view
         2. Its Layout style's position/width/height will be readjusted based on screen dimensions.
            See trekcrumb-layout.CSS and trekcrumb-responsive.JS 
     --%>
    <div id="picGalleryContentTemplateID"
         class="layout-picture-gallery-content-size"
         style="position:absolute; left:-500px; display:none;"
         onmousedown="menu_Clicked(this)"
         onmouseup="menu_ClickedReset(this)"
         onClick="picture_gallery_ItemSelected(this)" >
        <span id="picGalleryImageTemplateDefaultID" class="image-none-default fa fa-image"></span>
        <img id="picGalleryImageTemplateID"
             style="width:95%; height:95%; display:none;"  />
    </div>
         
    <%-- GALLERY LAYOUT --%>
    <div id="pictureGalleryLayoutID">
        
        <%-- PAGE VARIABLE --%>
        <input id="picGallery_picThumbnailSizeID" type="hidden" />
        <input id="picGallery_layoutTotalSizeAvailableID" type="hidden" />
        <input id="picGallery_NumberContentsToShowTotalID" type="hidden" />
        <input id="picGallery_ContentShownFirstRowIndexID" type="hidden" value="0" />
        
        <%-- PAGINATION: PREVIOUS/NEXT MENU:
             1. The outer <DIV> are required for JS to setup their heights and line-heights
                in order to place previous/next icons vertically centered. 
        --%>
        <div id="FRAGMENT_PICTURE_GALLERYMenuGoPreviousID" 
             class="body-clickable body-position-left"
             onmousedown="menu_Clicked(this)"
             onmouseup="menu_ClickedReset(this)"
             onclick="picture_gallery_Previous()"> 
            <span class="menu-icon font-color-menu fa fa-chevron-left" 
                  style="vertical-align:middle;"></span>
        </div>
        <div id="FRAGMENT_PICTURE_GALLERYMenuGoNextID" 
             class="body-clickable body-position-right"
             onmousedown="menu_Clicked(this)"
             onmouseup="menu_ClickedReset(this)"
             onclick="picture_gallery_Next()"> 
            <span class="menu-icon font-color-menu fa fa-chevron-right" 
                  style="vertical-align:middle;"></span>
        </div>
    </div>
    <script>
        picture_gallery_Retrieve(null);
    </script>
<% } %>

<%-- END fragment_picture_gallery.jsp --%>