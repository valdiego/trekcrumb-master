<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    Trip tripSelected_forPicCreate = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forPicCreate = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>
<% if(tripSelected_forPicCreate != null && isUserHasAccessToTrip_forPicCreate) { %>
    <div id="pictureCreateLayoutID" class="body-disabled-background" 
         style="display:none;">
        <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                with a shade, making the previous main content un-operable (all clickables 
                become non functional).
             2. It has a form. 
             3. It may require some "hidden" input variables that are shared among other embedded forms,
                and expect these variables to be set in its parent JSP. 
        --%>

        <div id="FRAGMENT_PICTURE_CREATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
            <div class="body-form-embedded-content" >
                <%-- SUB-TITLE AND GREETING --%>
                <jstl-fmt:message key="label_picture_create" var="labelText" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuClose.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_CREATE" />
                    <jsp:param name="labelText" value="${labelText}" />
                    <jsp:param name="closeMethodName" value="picture_create_cancel()" />
                </jsp:include>
                <div class="border-bottom-black" style="margin-bottom:20px; "></div>
                <span><jstl-fmt:message key="info_picture_create_greeting" bundle="${msgprop}"/> </span>

                <%-- PROGRESS WIDGET --%>
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_CREATE" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="marginTop" value="20" />
                </jsp:include>

                <%-- FORM --%>
                <div id="FRAGMENT_PICTURE_CREATEformID">
                    <%-- READ-ONLY DETAILS --%>
                    <div style="margin-top:10px;">
                        <table>
                            <tr style="vertical-align:top;">
                                <td width="25%" class="font-bold">
                                    <jstl-fmt:message key="label_trip_name" bundle="${msgprop}"/>
                                </td>
                                <td><%=tripSelected_forPicCreate.getName()%></td>
                            </tr>
                        </table>
                    </div>

                    <%-- PICTURE IMAGE FILE (SELECT AND PREVIEW) --%>
                    <jstl-fmt:message key="label_picture_select" var="labelText" bundle="${msgprop}"/>  
                    <jsp:include page="widget_imageFileSelect.jsp" >
                        <jsp:param name="marginTop" value="10" />
                        <jsp:param name="labelText" value="${labelText}" />
                    </jsp:include>
                    <input type="hidden" id="imageBase64DataID" path="imageBase64Data"  />
                    
                    <%-- INPUT WARNING MESSAGE --%>       
                    <jstl-fmt:message key="info_picture_create_greeting_format" var="warningMessage" bundle="${msgprop}"/>
                    <jsp:include page="widget_inputFieldWarning.jsp" >
                        <jsp:param name="warningMessage" value="${warningMessage}" />
                        <jsp:param name="isDisplay" value="true" />
                    </jsp:include>
                    
                    <%-- PICTURE NOTE --%>
                    <div style="margin-top:20px; margin-bottom:20px;">
                        <span class="font-bold"><jstl-fmt:message key="label_common_notes" bundle="${msgprop}"/></span><br/>
                        <textarea id="pictureCreateNoteInputID" 
                                  maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE %>" 
                                  cols="50" rows="4" ></textarea>
                     </div>

                    <%-- FUNCTIONAL BUTTONS --%>
                    <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                        <jsp:param name="submitLabel" value="${submitLabel}" />
                        <jsp:param name="submitMethodName" value="picture_create_validateAndSubmit()" />
                        <jsp:param name="cancelMethodName" value="picture_create_cancel()" />
                    </jsp:include>
                </div><%-- END formID --%>

            </div><%-- END body-form-embedded-content --%>
        </div><%-- END body-form-embedded-container --%>
    </div><%-- END pictureCreateLayoutID --%>
<% } %>      


<%-- END fragment_picture_create_form.jsp --%>