<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%-- Layout to display user's list of comments.
     1. On page (document) load: By default this fragment is hidden and empty. Upon some specific
        events (user click menu, etc.), then it triggers call JS functions to
        retrieve the data and images. 
        See: user_view_DisplaySelectedTab()
     2. Any pagination and MenuPage sections should be AFTER main layout (image, etc.) such that on narrow
        view, they will be on-top of the main layout.
 --%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    User user_forUserComments = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
    boolean isUserLoginProfile_forUserComments = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<%  if(user_forUserComments != null) { %>  
        <div id="userCommentsLayoutID" 
             class="layout body-halfOrFullWidth body-halfOrFullWidth-secondary body-scroll menu-sliding "
             style="height:100%; display:none;"
             var_user_username="<%=user_forUserComments.getUsername()%>"
             var_user_isUserLoginProfile="<%=isUserLoginProfile_forUserComments%>" >
             
            <%-- SUB-TITLE --%>
            <div class="body-position-center font font-size-normal font-bold"
                 style="margin-top:40px;">
                <%=user_forUserComments.getFullname()%>&nbsp;<jstl-fmt:message key="label_comment" bundle="${msgprop}"/> 
            </div>

            <%-- PROGRESS WIDGET --%>
            <jsp:include page="widget_progressBar.jsp" >
                <jsp:param name="id" value="FRAGMENT_COMMENT_LIST_USER_COMMENTS" />
                <jsp:param name="color" value="black" />
                <jsp:param name="size" value="small" />
                <jsp:param name="position" value="center-layout" />
                <jsp:param name="marginTop" value="60" />
            </jsp:include>

            <%-- COMMENT LIST SECTION --%>
            <div id="commentListContentID" class="layout-commentlist">
                <%-- NOTE:
                     This is the content where it could be either empty message, or the list of rows
                     copy-pasted from the widget and populated. --%>
            </div>
            
            <%-- COMMENT LIST ROW TEMPLATE WIDGET --%> 
            <jsp:include page="widget_row_comment_byUser.jsp" />
            
            <%-- PAGINATION WIDGET --%>
            <jsp:include page="widget_pagination_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_COMMENT_LIST_USER_COMMENTS" />
                <jsp:param name="menuGoPreviousMethodName" value="comment_user_previous()" />
                <jsp:param name="menuGoNextMethodName" value="comment_user_next()" />
                <jsp:param name="numberOfRecordsTotal" value="<%=user_forUserComments.getNumOfComments() %>" />
            </jsp:include>
        </div>
<%  } %>    
        
<%-- END fragment_user_comments.jsp --%>