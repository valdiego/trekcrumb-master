<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>

<div id="userLoginLayoutID" class="body-form-embedded-container font font-size-normal">
    <div class="body-form-embedded-content" >
        <%-- SUB-TITLE AND GREETING --%>
        <div>
            <span class="font-size-large font-bold"><jstl-fmt:message key="label_user_login" bundle="${msgprop}"/></span>
            <br/>
            <span><jstl-fmt:message key="info_user_login_greeting" bundle="${msgprop}"/></span>
        </div>
        <div class="border-bottom-black" style="margin-bottom:20px; "></div>

        <%-- FORM --%>
        <spring-form:form id="userLoginFormID"
                          method="post" 
                          commandName="userLoginFormBean"
                          action="" >
            <table class="body-form-input-field"
                   style="width:100%;">
                <%-- USERNAME --%>
                <tr>
                    <td width=34%>
                        <span class="font-bold"><jstl-fmt:message key="label_user_username" bundle="${msgprop}"/></span>
                    </td>
                    <td width=66%>
                        <spring-form:input path="username"
                                           id="userLoginUsernameInputID"
                                           type="text"
                                           maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_USERNAME)%>"
                                           tabindex="1" />
                    </td>
                </tr>
                <%-- PASSWORD --%>
                <tr>
                    <td width=34%>
                        <span class="font-bold"><jstl-fmt:message key="label_user_password" bundle="${msgprop}"/></span>
                    </td>
                    <td width=66%>
                        <jstl-fmt:message key="info_rule_caseSensitive" var="pwdPlaceholder" bundle="${msgprop}"/>  
                        <spring-form:input path="password"
                                           id="userLoginPwdInputID"
                                           type="password"
                                           maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD) %>"
                                           tabindex="2"
                                           placeholder="${pwdPlaceholder}" />
                    </td>
                </tr>
            </table>
        </spring-form:form>

        <%-- STAY LOGIN --%>
        <div style="margin-top:10px; margin-bottom:5px; margin-left:10px">
            <input id="stayLoginCheckboxID" 
                   type="checkbox" 
                   name="stayLoginCheckbox" 
                   checked />
            <label for="stayLoginCheckboxID" class="font-bold"><jstl-fmt:message key="label_user_my_rememberMe" bundle="${msgprop}"/></label>
            
        </div>

        <%-- FUNCTIONAL BUTTONS --%>
        <jstl-fmt:message key="label_common_login" var="submitLabel" bundle="${msgprop}"/>
        <jsp:include page="widget_menuCancelAndSubmit.jsp" >
            <jsp:param name="submitButtonIDPrefix" value="FRAGMENT_USER_LOGIN" />
            <jsp:param name="submitLabel" value="${submitLabel}" />
            <jsp:param name="submitMethodName" value="user_login_validateAndSubmit()" />
        </jsp:include>

    </div>
</div><%--END of userLoginLayoutID --%>

<%-- END fragment_user_login_form.jsp --%>