<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%-- TRIP DETAIL FRAGMENT
     1. Metadata Search Engine: 
        > To support search engine (Google, etc.)
        > In addition to markup format composed in include_headerSearchEngine.jsp, we support 
          MICRODATA markup format as well that must be included in particular elements inside the page. 
          These metadata formats are based on schema.org standards and noted in "itemxyz" attributes. 
        > When some of required microdata tags have no place in actual content, we unfortunately must
          put them contained inside hidden "microdataTagID" section.
        > REF: http://schema.org/docs/gs.html
--%>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.UserSession" %>
<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<%@ page import="com.trekcrumb.common.enums.TripPrivacyEnum" %>
<%@ page import="com.trekcrumb.common.enums.TripPublishEnum" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppConstants" %>
<%
    Trip trip_forTripDetail = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forTripDetail = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
    boolean isUserFavoritedSelectedTrip = false;
    String tripNote = "";            
    if(trip_forTripDetail != null) {
        tripNote = WebAppUtil.neutralizeStringValue(trip_forTripDetail.getNote(), false, false, true, -1);
        
        if(WebAppUtil.isUserSessionActive(request)) {
            UserSession userSession = (UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
            isUserFavoritedSelectedTrip = userSession.isTripSelectedUserFavorite();
        }
    }
    
    //Page identity:
    String pageIdentityType_forTripDetail = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TYPE);
    String pageURL_forTripDetail = null;
    String pageImageURL_forTripDetail = null;
    if(pageIdentityType_forTripDetail != null) {
        pageURL_forTripDetail = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_URL);
        pageImageURL_forTripDetail = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_IMAGEURL);
    }
    
%>

<%  if(trip_forTripDetail != null) { %>
        <div id="tripDetailLayoutID" class="body-halfOrFullWidth body-scroll menu-sliding" 
             itemscope itemtype="http://schema.org/BlogPosting" >
             
            <%-- MICRODATA TAGS HIDDEN SECTION --%>
            <div id="microdataTagID" style="display:none;">
                <meta itemprop="mainEntityOfPage" itemscope itemtype="http://schema.org/WebPage" itemid="<%=pageURL_forTripDetail %>" />
                <meta itemprop="name" content="<%=trip_forTripDetail.getName()%>" />
                <meta itemprop="author" content="<%=trip_forTripDetail.getUsername()%> at Trekcrumb.com" />            
                <meta itemprop="dateModified" content="<%=trip_forTripDetail.getCreated()%>" />
                <div itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name" >Trekcrumb.com</span>            
                    <div itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                        <img itemprop="url" src="<%=WebAppConstants.APP_DOMAIN_URL %>/<%=WebAppConstants.APP_LOGO %>" />
                    </div>
                </div>
                <div itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
                    <img itemprop="url" src="<%=pageImageURL_forTripDetail %>" />
                    <meta itemprop="height" content="600" />
                    <meta itemprop="width" content="600" />
                </div>
            </div>
             
            <%-- USER SECTION --%>
            <div id="tripUserSectionID" class="hoverhints-menuIcon body-clickable"
                 onmouseover="menu_hover_LabelShow(this)"
                 onmouseout="menu_hover_LabelHide(this)"
                 onmousedown="menu_Clicked(this)"
                 onmouseup="menu_ClickedReset(this)"
<%               if(isUserHasAccessToTrip_forTripDetail) { %>                         
                     onclick="user_view_Retrieve(true, null)" >
<%               } else { %>                         
                     onclick="user_view_Retrieve(false, '<%=trip_forTripDetail.getUsername()%>')" >
<%               } %>                          

                <jsp:include page="widget_userThumbnail.jsp" >
                    <jsp:param name="userImageName" value="<%=trip_forTripDetail.getUserImageName() %>" />
                    <jsp:param name="userFullnameValue" value="<%=trip_forTripDetail.getUserFullname()%>" />
                    <jsp:param name="usernameValue" value="<%=trip_forTripDetail.getUsername()%>" />
                </jsp:include>

                <%-- HOVER TEXT --%>
                <div id="tripUserSectionIDHover" 
                      class="hoverhints-menuIcon-text-container-leftBottom" 
                      style="display: none;">
                    <span class="hoverhints-menuIcon-text-body-leftBottom"><jstl-fmt:message key="label_user_profile" bundle="${msgprop}"/></span>
                </div>
            </div>

            <%-- TRIP NAME --%>
            <div class="font font-style-tripName"
                 style="margin-top:20px;" >
                <span itemprop="headline"><%=trip_forTripDetail.getName()%></span>
            </div>

            <%-- TRIP ATTRIBUTES --%>
            <div class="font font-size-normal">
                <%-- LOCATION --%>
                <div id="locationID"
                     itemprop="contentLocation" itemscope itemtype="http://schema.org/Place">
                    <span itemprop="description"><%=trip_forTripDetail.getLocation()%></span>
                </div>

                <%-- CREATED DATE: 
                     Do not fill the data on the server side, but process it on client's browser side with JS --%>
                <time id="tripCreatedDateID" 
                      style="margin-right:10px; display:inline-block; "
                      itemprop="datePublished" content="<%=trip_forTripDetail.getCreated()%>"></time>

                <%-- STATUSES --%>
<%              if(trip_forTripDetail.getStatus() == TripStatusEnum.ACTIVE) { %>
                    <div id="statusID" class="layout-trip-status-active font-size-small hoverhints-menuIcon"
                         onmouseover="menu_hover_LabelShow(this)"
                         onmouseout="menu_hover_LabelHide(this)" >
                        <span><jstl-fmt:message key="label_status_active" bundle="${msgprop}"/></span>
                        <div id="statusIDHover" 
                              class="hoverhints-menuIcon-text-container-rightBottom" 
                              style="top:40px; display: none;">
                            <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="info_trip_status_active" bundle="${msgprop}"/></span>
                        </div>
                    </div>
<%              } %>
<%              if(trip_forTripDetail.getPrivacy() == TripPrivacyEnum.PRIVATE) { %>
                    <div id="privacyID" class="layout-trip-privacy-private font-size-small hoverhints-menuIcon"
                         onmouseover="menu_hover_LabelShow(this)"
                         onmouseout="menu_hover_LabelHide(this)" >
                        <span class="fa fa-lock"></span>
                        <span><jstl-fmt:message key="label_privacy_private" /></span>
                        <div id="privacyIDHover" 
                              class="hoverhints-menuIcon-text-container-rightBottom" 
                              style="top:40px; display: none;">
                            <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="info_trip_privacy_private" bundle="${msgprop}"/></span>
                        </div>
                    </div>
<%              } %>
            </div>

            <%-- TRIP SUMMARY ICONS ROW --%>
            <div class="layout-trip-summary-icons">
                <%-- PLACES --%>
                <div class="hoverhints-menuIcon"
                     style="display:inline-block;">
                    <div id="placeIconID" 
<%                      if(trip_forTripDetail.getNumOfPlaces() > 0) { %>
                            class="menu-icon-summary body-clickable font font-color-menu"
                            onClick="trip_view_DisplaySelectedTab(FRAGMENT_PLACE_DETAIL)"
<%                      } else if(isUserHasAccessToTrip_forTripDetail
                                       && trip_forTripDetail.getStatus() == TripStatusEnum.ACTIVE) { %>
                            class="menu-icon-summary body-clickable font font-color-passive"
                            onClick="trip_view_DisplaySelectedTab(FRAGMENT_PLACE_DETAIL)"
<%                      } else { %>
                            class="menu-icon-summary font font-color-passive"
                            style="cursor:not-allowed;"
                            onClick=""
<%                      } %>
                        onmouseover="menu_hover_LabelShow(this)"
                        onmouseout="menu_hover_LabelHide(this)"
                        onmousedown="menu_Clicked(this)"
                        onmouseup="menu_ClickedReset(this)">
                        <span class="fa fa-map-marker"></span>
                        <div id="placeNumberID" 
                             class="font-size-normal 
<%                                 if(trip_forTripDetail.getNumOfPlaces() > 0) { %>
                                       font-bold
<%                                 } %>
                             "><%=trip_forTripDetail.getNumOfPlaces()%></div>
                    </div>
                    <div id="placeIconIDHover" 
                          class="hoverhints-menuIcon-text-container-rightBottom" 
                          style="display: none;">
                        <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_place" bundle="${msgprop}"/></span>
                    </div>
                </div>

                <%-- PICTURES --%>
                <div class="hoverhints-menuIcon"
                     style="display:inline-block;">
                    <div id="picIconID"
<%                      if(trip_forTripDetail.getNumOfPictures() > 0) { %>                            
                            class="menu-icon-summary body-clickable font font-color-menu"
                            onClick="trip_view_DisplaySelectedTab(FRAGMENT_PICTURE_DETAIL)"
<%                      } else { %>
                            class="menu-icon-summary font font-color-passive"
                            style="cursor:not-allowed;"
                            onClick=""                                    
<%                      } %>
                        onmouseover="menu_hover_LabelShow(this)"
                        onmouseout="menu_hover_LabelHide(this)"
                        onmousedown="menu_Clicked(this)"
                        onmouseup="menu_ClickedReset(this)">
                        <span class="fa fa-camera"></span>
                        <div id="picNumberID"
                             class="font-size-normal 
<%                                 if(trip_forTripDetail.getNumOfPictures() > 0) { %>
                                       font-bold
<%                                 } %>
                             "><%=trip_forTripDetail.getNumOfPictures()%></div>
                    </div>
                    <div id="picIconIDHover" 
                          class="hoverhints-menuIcon-text-container-rightBottom" 
                          style="display: none;">
                        <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_picture" bundle="${msgprop}"/></span>
                    </div>
                </div>

                <%-- FAVORITES --%>
                <div class="hoverhints-menuIcon"
                     style="display:inline-block;" >
                    <div id="favoriteIconID" 
                         class="menu-icon-summary body-clickable font
<%                      if(isUserFavoritedSelectedTrip) { %>
                            font-color-highlight"
<%                      } else if(trip_forTripDetail.getNumOfFavorites() > 0) { %>
                            font-color-menu"
<%                      } else { %>
                            font-color-passive"
<%                      } %>
                        onmouseover="menu_hover_LabelShow(this)"
                        onmouseout="menu_hover_LabelHide(this)"
                        onmousedown="menu_Clicked(this)"
                        onmouseup="menu_ClickedReset(this)"
                        onClick="favorite_retrieveByTrip_view()" >
<%                      if(trip_forTripDetail.getNumOfFavorites() > 0) { %>
                            <span id="favoriteIconSymbolID" class="fa fa-heart"></span>
<%                      } else { %>
                            <span id="favoriteIconSymbolID" class="fa fa-heart-o"></span>
<%                      } %>
                        <div id="favoriteNumberID" 
                             class="font-size-normal 
<%                                 if(trip_forTripDetail.getNumOfFavorites() > 0) { %>
                                       font-bold
<%                                 } %>
                             "><%=trip_forTripDetail.getNumOfFavorites()%></div>
                    </div>
                    <div id="favoriteIconIDHover" 
                          class="hoverhints-menuIcon-text-container-rightBottom" 
                          style="display: none;">
                        <span class="hoverhints-menuIcon-text-body-rightBottom">
<%                          if(isUserFavoritedSelectedTrip) { %>
                                <jstl-fmt:message key="label_favorite_user_to_trip" bundle="${msgprop}"/>
<%                          } else { %>
                                <jstl-fmt:message key="label_favorite" bundle="${msgprop}"/>
<%                          } %>
                        </span>
                    </div>
                </div>                
                
                <%-- COMMENTS --%>
                <div class="hoverhints-menuIcon"
                     style="display:inline-block;" >
                    <div id="commentIconID" 
                         class="menu-icon-summary body-clickable font
<%                      if(trip_forTripDetail.getNumOfComments() > 0) { %>
                            font-color-menu"
<%                      } else { %>
                            font-color-passive"
<%                      } %>
                        onmouseover="menu_hover_LabelShow(this)"
                        onmouseout="menu_hover_LabelHide(this)"
                        onmousedown="menu_Clicked(this)"
                        onmouseup="menu_ClickedReset(this)"
                        onClick="comment_trip_view()" >
<%                      if(trip_forTripDetail.getNumOfComments() > 0) { %>
                            <span id="commentIconSymbolID" class="fa fa-comment"></span>
<%                      } else { %>
                            <span id="commentIconSymbolID" class="fa fa-comment-o"></span>
<%                      } %>
                        <div id="commentNumberID" 
                             class="font-size-normal 
<%                                 if(trip_forTripDetail.getNumOfComments() > 0) { %>
                                       font-bold
<%                                 } %>
                             "><%=trip_forTripDetail.getNumOfComments()%></div>
                    </div>
                    <div id="commentIconIDHover" 
                          class="hoverhints-menuIcon-text-container-rightBottom" 
                          style="display: none;">
                        <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_comment" bundle="${msgprop}"/></span>
                    </div>
                </div>                  
            </div><%-- END of layout-trip-summary-icons --%>

            <div class="border-bottom-passive" style="margin-bottom:20px;"></div>

            <%-- PICTURE SLIDESHOW --%>
<%          if(trip_forTripDetail.getNumOfPictures() > 0) { %>
                <%@ include file="fragment_picture_slideshow.jsp" %>
                <div style="margin-bottom:10px;"></div>
<%          } %>

            <%-- PICTURE GALLERY --%>
<%          if(trip_forTripDetail.getNumOfPictures() > 1) { %>
                <%@ include file="fragment_picture_gallery.jsp" %>
                <div style="margin-bottom:10px;"></div>
<%          } %>

            <%-- TRIP NOTE --%>
            <div id="tripNoteID" class="font font-size-normal font-style-note" 
                 style="padding: 0px 10px 0px 10px;" >
                <span itemprop="description"><%=tripNote%></span>
            </div>

            <%-- PLACE NOTES --%>
<%          if(trip_forTripDetail.getListOfPlaces() != null) { %>
                <div style="margin-top:20px;"></div>
                <%@ include file="fragment_place_notes.jsp" %>
<%          } %>

            <%-- TRIP FAVORITES --%>
            <div style="margin-top:20px;"></div>
            <%@ include file="fragment_trip_favorites.jsp" %>
            
            <%-- COMMENTS --%>
            <div style="margin-top:20px;"></div>
            <%@ include file="fragment_trip_comments.jsp" %>
            
            <%-- MENU PAGE SECTION --%>
            <div class="menu-icon" style="margin-bottom:10px;">
                <%-- This extra white space as high as the Menu-Page icon is to provide space between body's last content
                     (gallery or note, etc.) and the Menu-Page icon so they would not overshadow each other at the end of the body. --%>
                <span>&nbsp;</span>
            </div>
            <div id="TripDetailMenuPageMainLayoutID" 
                 style="position:fixed; left:5px; bottom:5px;" >

                <%-- MENU SHOW/HIDE --%>
                <jsp:include page="widget_menuPageShowAndHide.jsp" >
                    <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_DETAIL" />
                    <jsp:param name="menuPageShowMethodName" value="menu_menuPage_ShowOrHide(FRAGMENT_TRIP_DETAIL, MENU_SLIDE_LEFT_TO_RIGHT, '0px', null)" />
                    <jsp:param name="menuPageHideMethodName" value="menu_menuPage_ShowOrHide(FRAGMENT_TRIP_DETAIL, MENU_SLIDE_LEFT_TO_RIGHT, '0px', null)" />
                </jsp:include>

                <%-- MENU CONTENTS --%>
                <div id="FRAGMENT_TRIP_DETAILMenuPageContentID" class="menu-page menu-sliding"
                     style="vertical-align:bottom; display:none; margin-left:-500px; visibility:hidden;">
                     
                    <%-- MENU FOR USERLOGIN TRIPS ONLY --%>
<%                  if(isUserHasAccessToTrip_forTripDetail) { %>
                        <div>
                            <%-- TRIP EDIT --%>
                            <div id="menuTripEditID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="trip_update_init()" >
                                <span class="fa fa-pencil" ></span>
                                <div id="menuTripEditIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_trip_edit" bundle="${msgprop}"/></span>
                                </div>
                            </div>

                            <%-- PICTURE ADD --%>
                            <div id="menuPictureAddID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="picture_create_Init()" >
                                <span class="fa fa-camera" ></span>
                                <span class="menu-icon-supplemental fa fa-plus"></span>
                                <div id="menuPictureAddIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_picture_create" bundle="${msgprop}"/></span>
                                </div>
                            </div>

                            <%-- PLACE ADD --%>
<%                          if(trip_forTripDetail.getStatus() == TripStatusEnum.ACTIVE) { %>
                                <div id="menuPlaceAddID" class="hoverhints-menuIcon menu-icon font-color-white"
                                     style="display:inline-block;" 
                                     onmouseover="menu_hover_LabelShow(this)"
                                     onmouseout="menu_hover_LabelHide(this)"
                                     onmousedown="menu_Clicked(this)"
                                     onmouseup="menu_ClickedReset(this)"
                                     onclick="place_create_Init()" >
                                    <span class="fa fa-map-marker" ></span>
                                    <span class="menu-icon-supplemental fa fa-plus"></span>
                                    <div id="menuPlaceAddIDHover" 
                                          class="hoverhints-menuIcon-text-container-leftTop" 
                                          style="display: none;">
                                        <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_place_create" bundle="${msgprop}"/></span>
                                    </div>
                                </div>
<%                          } %>
                        </div>
<%                  } //END-IF isUserHasAccessToTrip_forTripDetail %>                     
                     
                    <%-- MENU FOR PUBLIC/GENERIC --%>
                    <div>
                        <%-- MENU ONLY FOR PUBLIC (NOT PRIVATE) TRIP --%>
<%                      if(trip_forTripDetail.getPrivacy() == TripPrivacyEnum.PUBLIC) { %>                    
                            <%-- SHARE --%>
                            <div id="menuTripShareAddID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="trip_share_init()" >
                                <span class="fa fa-share-alt" ></span>
                                <div id="menuTripShareAddIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop">Share</span>
                                </div>
                            </div>
<%                      }  //END-IF Trip public  %>
                    
                        <%-- MENU ONLY FOR PUBLIC TRIP, OR PRIVATE BUT USERLOGIN HAS ACCESS --%>
<%                      if(trip_forTripDetail.getPrivacy() == TripPrivacyEnum.PUBLIC
                                || isUserHasAccessToTrip_forTripDetail) { %>                    
                            <%-- FAVORITE ADD/REMOVE --%>
                            <div id="menuFavoriteAddID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="favorite_create()"
<%                              if(isUserFavoritedSelectedTrip) { %>
                                    style="display:none;" >
<%                              } else { %>
                                    style="display:inline-block;" >
<%                              } %>
                                <span class="fa fa-heart" ></span>
                                <span class="menu-icon-supplemental fa fa-plus"></span>
                                <div id="menuFavoriteAddIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_favorite_create" bundle="${msgprop}"/></span>
                                </div>
                            </div>
                            <div id="menuFavoriteDeleteID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="favorite_delete()" 
<%                              if(isUserFavoritedSelectedTrip) { %>
                                    style="display:inline-block;" >
<%                              } else { %>
                                    style="display:none;" >
<%                              } %>
                                <span class="fa fa-heart" ></span>
                                <span class="menu-icon-supplemental fa fa-minus"></span>
                                <div id="menuFavoriteDeleteIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_favorite_delete" bundle="${msgprop}"/></span>
                                </div>
                            </div>
    
                            <%-- COMMENT ADD --%>
                            <div id="menuTripCommentAddID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="comment_create_init()" >
                                <span class="fa fa-comment" ></span>
                                <%-- span class="fa fa-plus" style="display:inline-block; font-size:16px; margin-left:-20px; padding: 3px 0px 0px 3px; background-color: #F4F5F0;"></span --%>
                                <span class="menu-icon-supplemental fa fa-plus"></span>
                                <div id="menuTripCommentAddIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_comment_create" bundle="${msgprop}"/></span>
                                </div>
                            </div>
<%                      }  //END-IF Trip public OR belongs to userLogin  %>

                    </div><%-- END of Menu for Public/Generic --%>
                </div><%-- END of TripDetailMenuPageContentID --%>
            </div><%-- END of TripDetailMenuPageMainLayoutID --%>
        </div> <%-- END of tripDetailLayoutID --%>
        
        <script>
            common_date_translateToLocaleTime(document.getElementById('tripCreatedDateID'), "<%=trip_forTripDetail.getCreated()%>", true, true);
        </script>
<%  } //END-IF Trip not null %>

<%-- END fragment_trip_detail.jsp --%>