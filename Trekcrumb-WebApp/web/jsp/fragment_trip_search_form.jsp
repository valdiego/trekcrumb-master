<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<div id="tripSearchFormLayoutID" class="body-disabled-background"
     style="display:none;">
    <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
            with a shade, making the previous main content un-operable (all clickables 
            become non functional).
         2. It has a form. 
    --%>
    <div class="body-inside-border body-form-embedded-container font font-size-normal">
        <div class="body-form-embedded-content" >
            <%-- SUB-TITLE AND GREETING --%>
            <jstl-fmt:message key="label_trip_search" var="labelText" bundle="${msgprop}"/>  
            <jsp:include page="widget_menuClose.jsp" >
                <jsp:param name="id" value="FRAGMENT_TRIP_LIST" />
                <jsp:param name="labelText" value="${labelText}" />
                <jsp:param name="closeMethodName" value="trip_search_FormCancel()" />
            </jsp:include>
            <div class="border-bottom-black" style="margin-bottom:20px; "></div>

            <%-- FORM --%>
            <span class="font-bold"><jstl-fmt:message key="label_trip_search_by_nameOrLocation" bundle="${msgprop}"/></span><br/>
            <input id="tripKeywordInputID" type="text" name="tripKeyword" maxlength="100" tabindex="1" ">
            <br/>
            <span class="font-bold"><jstl-fmt:message key="label_trip_search_by_usernameOrFullname" bundle="${msgprop}"/></span><br/>
            <input id="userKeywordInputID" type="text" name="userKeyword" maxlength="100" tabindex="2" >

            <%-- STATUS OPTIONS --%>
            <div style="margin-top:10px; ">
                <div style="width:30%; vertical-align:top; display:inline-block;">
                    <span class="font-bold"><jstl-fmt:message key="label_status" bundle="${msgprop}"/></span>
                </div>
                <div style="vertical-align:top; display:inline-block;">
                    <input id="statusInputALLID" type="radio" name="tripStatus" checked="true">
                    <label for="statusInputALLID" ><jstl-fmt:message key="label_common_all" bundle="${msgprop}"/></label>
                    <br/>
                    <input id="statusInputActiveID" type="radio" name="tripStatus" value="<%=TripStatusEnum.ACTIVE.getValue()%>">
                    <label for="statusInputActiveID" ><jstl-fmt:message key="label_status_active" bundle="${msgprop}"/></label>
                    <br/>
                    <input id="statusInputCompletedID" type="radio" name="tripStatus" value="<%=TripStatusEnum.COMPLETED.getValue()%>">
                    <label for="statusInputCompletedID" ><jstl-fmt:message key="label_status_completed" bundle="${msgprop}"/></label>
                </div>
            </div>

            <%-- CLEAR BUTTON
                 1. searchFormClearContainerID: Display is inline-block to prevent its total width to be as wide as
                    the entire parent layout (100%). It matters since it is clickable, just enough width for its contents.
                 2. searchFormClearID: Display is inline-block such that the text will be horizontally inline. Vertical-align is
                    middle such that it sits vertically centered with the text.
            --%>
            <div id="searchFormClearContainerID" class="body-clickable"
                 style="margin-top:10px; padding:5px 5px 5px 0px; display:inline-block;"
                 onmousedown="menu_Clicked(this)" 
                 onmouseup="menu_ClickedReset(this)" 
                 onclick="trip_search_FormClear()">
                <div id="searchFormClearID" class="menu-icon font-color-menu"
                     style="display:inline-block; vertical-align:middle;">
                    <span class="fa fa-times-circle"></span>
                </div>
                <span class="font-bold" style="text-decoration:underline;"><jstl-fmt:message key="label_common_clear" bundle="${msgprop}"/></span>
            </div>
            
            <%-- FUNCTIONAL BUTTONS --%>
            <jstl-fmt:message key="label_common_search" var="submitLabel" bundle="${msgprop}"/>  
            <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                <jsp:param name="submitLabel" value="${submitLabel}" />
                <jsp:param name="submitMethodName" value="trip_search_FormSubmit()" />
                <jsp:param name="cancelMethodName" value="trip_search_FormCancel()" />
            </jsp:include>
            
        </div><%-- END body-form-embedded-content --%>
    </div><%-- END body-form-embedded-container --%>
</div><%-- END tripSearchFormLayoutID --%>

<%-- END fragment_trip_search_form.jsp --%>