<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%-- USER DETAIL FRAGMENT
     1. Metadata Search Engine: 
        > To support search engine (Google, etc.)
        > In addition to markup format composed in include_headerSearchEngine.jsp, we support 
          MICRODATA markup format as well that must be included in particular elements inside the page. 
          These metadata formats are based on schema.org standards and noted in "itemxyz" property/attribute. 
        > When some of required microdata tags have no place in actual content, we unfortunately must
          put them contained inside hidden "microdataTagID" section.
        > REF: http://schema.org/docs/gs.html
--%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.enums.DateFormatEnum" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.CommonUtil" %>
<%@ page import="com.trekcrumb.common.utility.DateUtil" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppConstants" %>
<%
    User user_forUserDetail = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
    boolean isUserLoginProfile_forUserDetail = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
    
    String userImageURL = null;
    if(!ValidationUtil.isEmpty(user_forUserDetail.getProfileImageName())) {
        userImageURL = CommonConstants.FS_SERVLET_URL_GETIMAGE_PROFILE + user_forUserDetail.getProfileImageName();
    }
    
    //Page identity:
    String pageIdentityType_forUserDetail = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TYPE);
    String pageURL_forUserDetail = null;
    String pageImageURL_forUserDetail = null;
    if(pageIdentityType_forUserDetail != null) {
        pageURL_forUserDetail = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_URL);
        pageImageURL_forUserDetail = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_IMAGEURL);
    }
    
%>

<%  if(user_forUserDetail != null) { %>  
        <div id="userDetailLayoutID" class="body-halfOrFullWidth body-scroll body-position-center menu-sliding"
             itemscope itemtype="http://schema.org/Person" >

            <%-- MICRODATA TAGS HIDDEN SECTION --%>
            <div id="microdataTagID" style="display:none;">
                <meta itemprop="mainEntityOfPage" itemscope itemtype="http://schema.org/WebPage" itemid="<%=pageURL_forUserDetail %>" />
                <meta itemprop="url" content="<%=pageURL_forUserDetail %>" />
            </div>

            <div id="userDetailContentID" class="body-inside-border"> 
                <%-- USER IMAGE --%>
                <div class="body-position-center image-profile shadow-bottom"
                     style="margin-bottom:20px;"
                     itemprop="image" itemscope itemtype="http://schema.org/ImageObject" >
<%                  if(userImageURL != null) { %>
                        <img id="profileImageID<%=user_forUserDetail.getUsername()%>"
                             class="image"
                             style="width:100%; height:auto;" 
                             itemprop="url"
                             src="<%=userImageURL%>" />

<%                  } else { %>
                        <span id="profileImageID<%=user_forUserDetail.getUsername()%>Default" class="image fa fa-user"></span>
                        <meta itemprop="url" src="<%=pageImageURL_forUserDetail %>" />
<%                  } %>
                    <meta itemprop="height" content="200" />
                    <meta itemprop="width" content="200" />
                </div>

                <%-- USER PROFILE --%>
                <div class="body-inside-border font font-size-normal"
                     style="margin-left:20px; margin-right:20px;">
                    <span class="font-size-large font-bold"
                          itemprop="name" ><%=user_forUserDetail.getFullname()%></span><br/>
                    <span class="font-bold"><%=user_forUserDetail.getUsername()%></span><br/>
                    <span><jstl-fmt:message key="label_common_joined" bundle="${msgprop}"/>&nbsp;
                          <%=DateUtil.formatDate(user_forUserDetail.getCreated(), 
                                                 DateFormatEnum.GMT_STANDARD_LONG, 
                                                 DateFormatEnum.LOCALE_UI_WITHOUT_TIME)%></span>
                    <div itemprop="homeLocation" itemscope itemtype="http://schema.org/Place">
                        <span itemprop="description"><%=user_forUserDetail.getLocation()%></span>
                    </div>

                    <%-- USER SUMMARY ICONS ROW --%>
                    <div id="summaryLayoutID"
                         style="margin-top:20px;">
                        <div class="border-bottom-passive" style="opacity:0.25;"></div>
                        <!-- TRIPS -->
                        <div class="hoverhints-menuIcon" style="display:inline-block;">
                            <div id="userTripIconID" 
                                class="body-clickable"
                                style="display:inline-block; padding:5px;"
                                onmouseover="menu_hover_LabelShow(this)"
                                onmouseout="menu_hover_LabelHide(this)"
                                onmousedown="menu_Clicked(this)"
                                onmouseup="menu_ClickedReset(this)"
                                onClick="user_view_DisplaySelectedTab(FRAGMENT_TRIP_LIST_USER_TRIPS)" >
                                <div class="menu-icon">
                                    <img class="image-menu-icon"
                                         src="<%=request.getContextPath()%>/images/icon_applogo_menu_active_dark.png" />
                                </div>
                                <div class="font-size-large font-bold"><%=user_forUserDetail.getNumOfTotalTrips()%></div>
                            </div>
                            <!-- Due to containing icon and text, override the CSS 'top' param
                                 so hover to position more appropriately. -->
                            <div id="userTripIconIDHover" 
                                  class="hoverhints-menuIcon-text-container-leftBottom" 
                                  style="top:95px; display: none;">
                                <span class="hoverhints-menuIcon-text-body-leftBottom"><jstl-fmt:message key="label_trip_list" bundle="${msgprop}"/></span>
                            </div>
                        </div>
                        
                        <!-- PICTURES -->
                        <div class="hoverhints-menuIcon" style="display:inline-block;">
                            <div id="userPicturesIconID" 
                                class="body-clickable hoverhints-menuIcon"
                                style="display:inline-block; padding:5px;"
                                onmouseover="menu_hover_LabelShow(this)"
                                onmouseout="menu_hover_LabelHide(this)"
                                onmousedown="menu_Clicked(this)"
                                onmouseup="menu_ClickedReset(this)"
                                onClick="user_view_DisplaySelectedTab(FRAGMENT_PICTURE_LIST_USER_PICTURES)" >
                                <span class="menu-icon font-color-menu fa fa-camera"></span>
                                <div class="font-size-large font-bold"><%=user_forUserDetail.getNumOfTotalPictures()%></div>
                            </div>
                            <!-- Due to containing icon and text, override the CSS 'top' param
                                 so hover to position more appropriately. -->
                            <div id="userPicturesIconIDHover" 
                                  class="hoverhints-menuIcon-text-container-leftBottom" 
                                  style="top:95px; display: none;">
                                <span class="hoverhints-menuIcon-text-body-leftBottom"><jstl-fmt:message key="label_picture" bundle="${msgprop}"/></span>
                            </div>
                        </div>
                        
                        <!-- FAVORITES -->
                        <div class="hoverhints-menuIcon" style="display:inline-block;">
                            <div id="userFaveIconID" 
                                class="body-clickable hoverhints-menuIcon"
                                style="display:inline-block; padding:5px;"
                                onmouseover="menu_hover_LabelShow(this)"
                                onmouseout="menu_hover_LabelHide(this)"
                                onmousedown="menu_Clicked(this)"
                                onmouseup="menu_ClickedReset(this)"
                                onClick="user_view_DisplaySelectedTab(FRAGMENT_TRIP_LIST_USER_FAVORITES)" >
                                <span class="menu-icon font-color-menu fa fa-heart"></span>
                                <div class="font-size-large font-bold"><%=user_forUserDetail.getNumOfFavorites()%></div>
                            </div>
                            <!-- Due to containing icon and text, override the CSS 'top' param
                                 so hover to position more appropriately. -->
                            <div id="userFaveIconIDHover" 
                                  class="hoverhints-menuIcon-text-container-leftBottom" 
                                  style="top:95px; display: none;">
                                <span class="hoverhints-menuIcon-text-body-leftBottom"><jstl-fmt:message key="label_favorite" bundle="${msgprop}"/></span>
                            </div>
                        </div>
                        
                        <!-- COMMENTS -->
                        <div class="hoverhints-menuIcon" style="display:inline-block;">
                            <div id="userCommentIconID" 
                                class="body-clickable hoverhints-menuIcon"
                                style="display:inline-block; padding:5px;"
                                onmouseover="menu_hover_LabelShow(this)"
                                onmouseout="menu_hover_LabelHide(this)"
                                onmousedown="menu_Clicked(this)"
                                onmouseup="menu_ClickedReset(this)"
                                onClick="user_view_DisplaySelectedTab(FRAGMENT_COMMENT_LIST_USER_COMMENTS)" >
                                <span class="menu-icon font-color-menu fa fa-comment"></span>
                                <div class="font-size-large font-bold"><%=user_forUserDetail.getNumOfComments()%></div>
                            </div>
                            <!-- Due to containing icon and text, override the CSS 'top' param
                                 so hover to position more appropriately. -->
                            <div id="userCommentIconIDHover" 
                                  class="hoverhints-menuIcon-text-container-leftBottom" 
                                  style="top:95px; display: none;">
                                <span class="hoverhints-menuIcon-text-body-leftBottom"><jstl-fmt:message key="label_comment" bundle="${msgprop}"/></span>
                            </div>
                        </div>
                    </div>        
                </div>

                <%-- USER NOTE --%>
                <div class="font font-size-large"
                     style="padding:20px 20px 0px 20px; text-align:left;">
                    <span class="font-style-note"
                          itemprop="description"><%=user_forUserDetail.getSummary()%></span>
                </div>
                
                <%-- USER LOGIN PRIVATE DATA/SETTING --%>
<%              if(isUserLoginProfile_forUserDetail) { %>
                    <%@ include file="fragment_user_account.jsp" %>
                    <%@ include file="fragment_userAuthToken_list.jsp" %>
                    <%-- TODO: USER SETTING/PREFERENCES --%>
<%              } %>

                <%-- MENU PAGE SECTION --%>
<%              if(isUserLoginProfile_forUserDetail) { %>
                    <div class="menu-icon" style="margin-bottom:10px;">
                        <%-- This extra white space as high as the Menu-Page icon is to provide space between body's last content
                             (gallery or note, etc.) and the Menu-Page icon so they would not overshadow each other at the end of the body. --%>
                        <span>&nbsp;</span>
                    </div>

                    <div id="UserDetailMenuPageMainLayoutID" 
                         style="position:fixed; left:5px; bottom:5px; text-align:left;" >
                         
                        <%-- MENU SHOW/HIDE --%>
                        <jsp:include page="widget_menuPageShowAndHide.jsp" >
                            <jsp:param name="widgetPrefixID" value="FRAGMENT_USER_DETAIL" />
                            <jsp:param name="menuPageShowMethodName" value="menu_menuPage_ShowOrHide(FRAGMENT_USER_DETAIL, MENU_SLIDE_LEFT_TO_RIGHT, '0px', null)" />
                            <jsp:param name="menuPageHideMethodName" value="menu_menuPage_ShowOrHide(FRAGMENT_USER_DETAIL, MENU_SLIDE_LEFT_TO_RIGHT, '0px', null)" />
                        </jsp:include>
                        
                        <%-- MENU CONTENTS --%>
                        <div id="FRAGMENT_USER_DETAILMenuPageContentID" class="menu-page menu-sliding"
                             style="vertical-align:bottom; display:none; margin-left:-500px; visibility:hidden;">

                            <%-- USER LOGIN REFRESH --%> 
                            <div id="menuUserRefreshID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="user_login_refresh_init()" >
                                <span class="fa fa-sign-in" ></span>
                                <div id="menuUserRefreshIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_user_my_profile_refresh" bundle="${msgprop}"/></span>
                                </div>
                            </div>
                            <%-- USER EDIT --%> 
                            <div id="menuUserEditID" class="hoverhints-menuIcon menu-icon font-color-white"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="user_update_init()" >
                                <span class="fa fa-pencil" ></span>
                                <div id="menuUserEditIDHover" 
                                      class="hoverhints-menuIcon-text-container-leftTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_user_edit" bundle="${msgprop}"/></span>
                                </div>
                            </div>

                        </div><%-- END of UserDetailMenuPageContentID--%>
                    </div><%-- END of UserDetailMenuPageMainLayoutID--%>
<%              } %>
            </div>
        </div><%-- END of userDetailLayoutID --%>
<%  } %>    

<%-- END fragment_user_detail.jsp --%>