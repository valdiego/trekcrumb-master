<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="java.util.List" %>
<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.Picture" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPagePicture = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);

    String tripID = null;
    String listOfPicturesJSON = null;
    if(trip_forPagePicture != null && trip_forPagePicture.getNumOfPictures() > 0) {
        tripID = trip_forPagePicture.getId();
        List<Picture> listOfPictures = trip_forPagePicture.getListOfPictures();
        listOfPicturesJSON = (new ObjectMapper()).writeValueAsString(listOfPictures);
    }
%>

<div id="paginationPictureLayoutID"
     var_tripID = ""
     var_pagePic_numOfTotalPictures = "0"
     var_pagePic_pictureCurrentIndex = "0" >

    <input id="pagePic_listOfPicturesID" type="hidden" />
    
    <script>
        //Populate pagination local variables:
        pagePic_init("<%=tripID%>", <%=listOfPicturesJSON%>);
    </script>
</div>


<%-- END fragment_pagination_picture.jsp --%>