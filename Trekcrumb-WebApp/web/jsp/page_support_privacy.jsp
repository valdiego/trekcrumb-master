<%@ include file="include.jsp" %>

<%-- Note:
     1. The body text content is no longer static content that is directly written inside this
        JSP. Instead, we generate the content from Base's resources due to its multiple
        apps/devices support (addressing newline chars, etc.) and localization language.
     2. Our JSP is located in private webcontent: /WEB-INF/jsp directory while the generated
        txt file is stored in /WEB-INF directly. Hence, moves up one subdir level.
--%>
<body>
    <%-- PAGE TITLE --%>
    <div class="container-title">
        <span id="pageTitleID" class="font font-style-title"><jstl-fmt:message key="label_support_privacy_long" bundle="${msgprop}"/></span>
    </div>

    <%-- MAIN BODY --%>
    <div class="container-body body-inside font font-size-normal font-style-support" 
         style="background-color:white; bottom:50px;">
        <jstl-core:import url="../messages-support-privacy.txt" />
    </div>
    
    <%-- FOOTER --%>
    <jsp:include page="widget_footer.jsp" >
        <jsp:param name="isPositionFixed" value="true" />
    </jsp:include>
</body>

<%-- END page_support_privacy.jsp --%>