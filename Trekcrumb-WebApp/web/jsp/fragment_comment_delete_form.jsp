<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<div id="commentDeleteLayoutID" class="body-disabled-background" 
     style="display:none;">
    <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
            with a shade, making the previous main content un-operable (all clickables 
            become non functional).
         2. It has a form. 
         3. It may require some "hidden" input variables that are shared among other embedded forms,
            and expect these variables to be set in its parent JSP. 
    --%>
    <div id="FRAGMENT_COMMENT_DELETEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
        <div class="body-form-embedded-content" >
            <%-- SUB-TITLE AND GREETING --%>
            <jstl-fmt:message key="label_comment_delete" var="labelText" bundle="${msgprop}"/>  
            <jsp:include page="widget_menuClose.jsp" >
                <jsp:param name="id" value="FRAGMENT_COMMENT_DELETE" />
                <jsp:param name="labelText" value="${labelText}" />
                <jsp:param name="closeMethodName" value="comment_delete_cancel()" />
            </jsp:include>
            <div class="border-bottom-black" style="margin-bottom:20px; "></div>
                <span><jstl-fmt:message key="info_comment_delete_greeting" bundle="${msgprop}"/></span>

            <%-- PROGRESS WIDGET --%>
            <jsp:include page="widget_progressBar.jsp" >
                <jsp:param name="id" value="FRAGMENT_COMMENT_DELETE" />
                <jsp:param name="color" value="black" />
                <jsp:param name="size" value="small" />
                <jsp:param name="marginTop" value="20" />
            </jsp:include>

            <%-- FORM --%>
            <div id="FRAGMENT_COMMENT_DELETEformID">
                <%-- READ-ONLY DETAILS --%>
                <input id="commentDeletePageNameID" type="hidden" />
                <input id="commentDeleteCommentIDInputID" type="hidden" />
                <div style="margin-top:10px;">
                    <table>
                        <tr style="vertical-align:top;">
                            <td width="25%" class="font-bold"><jstl-fmt:message key="label_common_added" bundle="${msgprop}"/></td>
                            <td id="commentDeleteCreatedID"></td>
                        </tr>
                    </table>
                </div>

                <%-- COMMENT NOTE --%>
                <div style="margin-top:10px; margin-bottom:20px;">
                    <textarea id="commentDeleteNoteInputID" 
                              cols="50" rows="4" 
                              readonly></textarea>
                </div>

                <%-- FUNCTIONAL BUTTONS --%>
                <jstl-fmt:message key="label_common_submit" var="submitLabel" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                    <jsp:param name="submitLabel" value="${submitLabel}" />
                    <jsp:param name="submitMethodName" value="comment_delete_validateAndSubmit()" />
                    <jsp:param name="cancelMethodName" value="comment_delete_cancel()" />
                </jsp:include>
            </div><%-- END formID --%>

        </div><%-- END body-form-embedded-content --%>
    </div><%-- END body-form-embedded-container --%>
</div>

<%-- END fragment_comment_delete_form.jsp --%>