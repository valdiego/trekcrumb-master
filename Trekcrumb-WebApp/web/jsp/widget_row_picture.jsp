<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    String labelTripName = MessageResourceUtil.getMessageValue("label_trip_name", null, null);
    String labelCreatedDate = MessageResourceUtil.getMessageValue("label_common_added", null, null);
    //String labelImageLoading = MessageResourceUtil.getMessageValue("info_picture_imageLoading", null, null);

    boolean isDisplayUser = Boolean.parseBoolean(request.getParameter("isDisplayUser"));
%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> "a" tag is the outer element that wraps the main content as a link. 
        Set is "vertical-align = top" so that multiple rows can align
        horizontally parallel when they are display side by side.
     -> "pictureRow" element is the main content of the row. Set "position = relative", 
        so that any element within can be position absolute anywhere inside its body.
     -> Some of IDs will later be modified to be unique for each rows: imageID, infoOrErrorMsgID, etc.
     -> The actual image will be displayed as the "background" of the "picRowImageID" element.
--%>
<a id="pictureRowTemplateID"
   style="vertical-align:top; display:none;">
    <div name="pictureRow" class="body-inside layout-picturelist-row-gallery" 
         onmouseover="picture_list_rowHoverOn(this, <%=isDisplayUser%>)"
         onmouseout="picture_list_rowHoverOff(this, <%=isDisplayUser%>)">

        <%-- USER PROFILE --%>
        <div id="picRowUserDetailID"
<%           if(isDisplayUser) { %>
                 style="display:block;">
<%           } else { %>
                 style="display:none;">
<%           } %>
            <jsp:include page="widget_userThumbnail.jsp">
                <jsp:param name="userThumbnailCustomCSS" value="layout-picturelist-row-gallery-userdetail body-black-transparent font-color-white" />
            </jsp:include>
        </div>

        <%-- IMAGE --%>
        <div id="picRowImageID" class="layout-picturelist-row-gallery-image"
             style="display:inline-block;">
        </div>

        <%-- PICTURE DETAIL --%>
        <div id="picRowPictureDetailID" class="layout-picturelist-row-gallery-note body-black-transparent font font-size-small font-color-white" 
             style="display:none;">
            <div class="font-bold">
                <%=labelTripName %>:&nbsp;
                <span id="picRowTripNameID"></span>
            </div>
            <%=labelCreatedDate %>:&nbsp;<span id="picRowCreatedID"></span><br/>
            <div id="picRowNoteID" class="font-size-normal" style="margin-top:10px"></div>
        </div>
    </div>
</a>


<%-- END of widget_row_picture.jsp --%>