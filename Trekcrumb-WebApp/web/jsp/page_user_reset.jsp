<%@ include file="include.jsp" %>

<body>
    <script>
        common_disableBackButton();
    </script>

    <%-- PAGE TITLE --%>
    <div class="container-title">
        <div class="font font-style-title">
            <span><jstl-fmt:message key="label_user_my_resetAccount" bundle="${msgprop}"/></span>
        </div>
    </div>

    <%-- MAIN BODY --%>
    <div id="userResetLayoutID" class="container-body font font-size-normal">
        <div id="userResetCookieEnabledID" class="body-inside-border body-form ">
            <%-- SUB-TITLE AND GREETING --%>
            <div>
                <span class="font-size-large font-bold"><jstl-fmt:message key="label_user_my_resetAccount" bundle="${msgprop}"/></span>
                <br/>
                <span><jstl-fmt:message key="info_user_reset_greeting" bundle="${msgprop}"/></span>
            </div>
            <div class="border-bottom-black" style="margin-bottom:20px; "></div>
        
            <%-- FORM --%>
            <spring-form:form id="userResetFormID"
                              method="post" 
                              commandName="userResetFormBean"
                              action="" >
                <table class="body-form-input-field"
                       style="width:100%;">
                    <%-- EMAIL --%>
                    <tr>
                        <td width=34%>
                            <span class="font-bold"><jstl-fmt:message key="label_user_email" bundle="${msgprop}"/></span>
                        </td>
                        <td width=66%>
                            <spring-form:input 
                                path="email"
                                id="userResetEmailInputID"
                                type="text"
                                maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL)%>" />
                        </td>
                    </tr>
                    <%-- SECURITY TOKEN --%>
                    <tr>
                        <td width=34%>
                            <span class="font-bold"><jstl-fmt:message key="label_user_security_token" bundle="${msgprop}"/></span>
                        </td>
                        <td width=66%>
                            <spring-form:input 
                                path="securityToken"
                                id="userResetSecurityTokenInputID"
                                type="text"
                                maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SECURITY_TOKEN)%>" />
                        </td>
                    </tr>
                </table>
 
                <%-- PASSWORD --%>
                <table class="body-form-input-field"
                       style="width:100%; padding-top:20px; ">
                    <tr>
                        <td width=100%>
                            <span class="font-bold">Enter New Password</span>
                            <jstl-fmt:message key="info_rule_charsBetween6And25" var="pwdPlaceholder1" bundle="${msgprop}"/>  
                            <jstl-fmt:message key="info_rule_caseSensitive" var="pwdPlaceholder2" bundle="${msgprop}"/>  
                            <spring-form:input 
                                path="password"
                                id="userResetPwdNewInputID"
                                type="password"
                                maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD)%>"
                                placeholder="${pwdPlaceholder1}, ${pwdPlaceholder2}" />
                            <input id="userResetPwdNewInputReenterID" 
                                   type="password" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD %>"
                                   placeholder="<jstl-fmt:message key="label_user_password_reenter" bundle="${msgprop}"/>"
                                   onkeyup="validate_InputVsReenterInput('userResetPwdNewInputID', this, true)" >
                            
                            <%-- INPUT ERROR MESSAGE --%>       
                            <jstl-fmt:message key="error_user_invalidparam_confirmValueNotMatch" var="errorMessage" bundle="${msgprop}"/>
                            <jsp:include page="widget_inputFieldError.jsp" >
                                <jsp:param name="widgetPrefixID" value="userResetPwdNewInputReenterID" />
                                <jsp:param name="errorMessage" value="${errorMessage}" />
                            </jsp:include>
                        </td>
                    </tr>
                </table>
            </spring-form:form>
            
            <%-- FUNCTIONAL BUTTONS --%>
            <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>
            <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                <jsp:param name="submitLabel" value="${submitLabel}" />
                <jsp:param name="submitMethodName" value="user_reset_validateAndSubmit()" />
                <jsp:param name="submitButtonIDPrefix" value="USER_RESET" />
            </jsp:include>
        </div>
            
        <div id="userResetCookieDisabledID" class="body-inside-border"
             style="display:none;">
        </div>
    </div><%--END of userResetLayoutID --%>

    <%@ include file="widget_disableBackground.jsp" %>
    <script>
        if(mDevice_isSupportCookie) {
            document.getElementById('userResetFormID').setAttribute("action", WEBAPP_URL_USER_RESET_SUBMIT);

        } else {
            common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED], null); 
            document.getElementById("userResetCookieEnabledID").style.display = "none";
            var bodyCookieDisabled = document.getElementById("userResetCookieDisabledID");
            bodyCookieDisabled.style.display = "block";
            bodyCookieDisabled.innerHTML = ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED;
        }
    </script>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_user_reset.jsp --%>