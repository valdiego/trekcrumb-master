<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    String customCSS = request.getParameter("customCSS");
    if(customCSS == null) {
        customCSS = "layout-triplist-row";
    }

    String customStyle = request.getParameter("customStyle");
    if(customStyle == null) {
        customStyle = "";
    }
    
    String labelStatusActive = MessageResourceUtil.getMessageValue("label_status_active", null, null);
    String labelPrivacyPrivate = MessageResourceUtil.getMessageValue("label_privacy_private", null, null);
    
%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> "a" tag is the outer element that wraps the main content as a link. 
        Set is "vertical-align = top" so that multiple rows can align
        horizontally parallel when they are display side by side.
     -> "tripRow" element is the main content of the row. Set "position = relative", 
        so that any element within can be position absolute anywhere inside its body.
--%>
<a id="tripRowTemplateID"
   style="vertical-align:top; display:none;">
    <div name="tripRow" class="body-inside-shadow <%=customCSS%>" 
         style="<%=customStyle%>"
         onmouseover="menu_hover_ElementActive(this)"
         onmouseout="menu_hover_ElementInactive(this)">

        <%-- USER SECTION --%>
        <jsp:include page="widget_userThumbnail.jsp" />

        <%-- TRIP NAME --%>
        <div class="font"
             style="margin-top:10px; padding-left:20px; padding-right:20px;">
            <span id="tripNameID" class="font-size-large font-bold" style="display:inline-block;"></span><br/>
        </div>
        
        <%-- TRIP ATTRIBUTES --%>
        <div class="font font-size-small"
             style="padding-left:20px; padding-right:20px;">
            <span id="locationID" class="font-size-small" ></span><br/>
            <span id="createdDateID" style="margin-right:5px;"></span>
            <div id="statusID" class="layout-trip-status-active"><%=labelStatusActive %></div>
            <div id="privacyPrivateID" class="layout-trip-privacy-private">
                <span class="fa fa-lock"></span>
                <span><%=labelPrivacyPrivate %></span>
            </div>
        </div>
        
        <%-- TRIP NOTE AND PICTURE COVER 
             1. We have options whether to display Trip note and picture cover side by side
                (display the elements as "table" and "table-cell") or regular one after another
                (display the elements as "block"). The input "customCSS" determines which style to do.
             2. To display side by side, we must do "table" and "table-cell" so that we can 
                display image and note side by side without dropping out of line when content is too long. 
        --%>
        <div style="margin-top:20px; padding-left:20px; padding-right:20px;">
            <table style="width:100%;">
                <tr>
                    <td id="tripPictureCoverLayoutID" class="<%=customCSS%>-picture-cover" style="display:none;">
                        <img id="tripPictureCoverImageID" class="layout-triplist-picture-cover-image" />
                    </td>
                    <td id="tripNoteLayoutID" class="font font-size-normal"
                        style="display:none; vertical-align:top;">
                        <span id="tripNoteID"></span>
                    </td>
                </tr>
           </table>        
        </div>
        
        <div class="border-bottom-passive" style="margin-top:20px;"></div>
        
        <%-- TRIP SUMMARY ICONS ROW --%>
        <div class="layout-trip-summary-icons">
            <%-- PLACES --%>
            <div style="display:inline-block;">
                <div id="placeIconID" class="menu-icon-summary">
                    <span class="fa fa-map-marker"></span>
                    <div id="placeNumberID" class="font font-size-small">0</div>
                </div>
            </div>

            <%-- PICTURES --%>
            <div style="display:inline-block;">
                <div id="picIconID" class="menu-icon-summary">
                    <span class="fa fa-camera"></span>
                    <div id="picNumberID" class="font font-size-small">0</div>
                </div>
            </div>
            
            <%-- FAVORITES --%>
            <div style="display:inline-block;">
                <div class="menu-icon-summary">
                    <span id="favoriteIconActiveID" class="font-color-menu fa fa-heart"></span>
                    <span id="favoriteIconPassiveID" class="font-color-passive fa fa-heart-o"></span>
                    <div id="favoriteNumberID" class="font font-size-small">0</div>
                </div>
            </div>

            <%-- COMMENTS --%>
            <div style="display:inline-block;">
                <div class="menu-icon-summary">
                    <span id="commentIconActiveID" class="font-color-menu fa fa-comment"></span>
                    <span id="commentIconPassiveID" class="font-color-passive fa fa-comment-o"></span>
                    <div id="commentNumberID" class="font font-size-small">0</div>
                </div>
            </div>
        </div>       
    </div><%-- END tripRow--%>
</a>


<%-- END of widget_row_trip.jsp --%>