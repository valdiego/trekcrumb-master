<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> "a" tag is the outer element that wraps the main content as a link. 
     -> "placeRow" element is the main content of the row. 
--%>
<a id="placeRowAsTagTemplateID"
   style="vertical-align:top; display:none;">
    <div id="placeRowAsTagContentID"  class="layout-row-place-list-asTag font font-size-normal"
         onmouseover="menu_hover_ElementActive(this)"
         onmouseout="menu_hover_ElementInactive(this)">
        <span id="locationID"></span>
    </div>
</a>


<%-- END of widget_row_place_asTag.jsp --%>