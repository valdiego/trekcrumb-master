<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }
    
    String warningMessage = request.getParameter("warningMessage");
    if(warningMessage == null) {
        warningMessage = "Proceed with caution!";
    }
    
    boolean isDisplay = Boolean.parseBoolean(request.getParameter("isDisplay"));
%>
<%-- NOTE:
     1. "widgetPrefixID" is required to uniquely identified this widget usage within a page. In one page/view,
         there could be more than one usages. The value could be page/fragment name, etc.
     2. The "widgetPrefixID" is then used to one or all elements and are used by javascript to dynamically
        modify: show/hide, change text/value, etc.
     4. The div with display 'table' is to display image and long text to be vertically centered to each other 
--%>
<div id="<%=widgetPrefixID%>Warning" class="body-warning"
<%   if(isDisplay) { %>
         style="display:block;">
<%   } else { %>
         style="display:none;">
<%   } %>
     
    <div style="display:table;">
        <div style="display:table-cell; padding-right:5px; vertical-align:middle;" >
            <span class="menu-icon font-color-warning fa fa-exclamation-triangle"></span>
        </div>
        <span id="<%=widgetPrefixID%>WarningMessage" style="display:table-cell; vertical-align:middle;"><%=warningMessage%></span>
    </div>
</div>

<%-- END widget_inputFieldWarning.jsp --%>