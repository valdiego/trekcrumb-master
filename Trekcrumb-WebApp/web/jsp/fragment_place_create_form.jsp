<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<div id="placeCreateLayoutID" class="body-disabled-background" 
     style="display:none;">
    <%-- 1. When displayed, placeCreateLayout will cover the entire original main content 
            with a shade, making the previous main content disabled (all clickables 
            become non functional).
         2. It does NOT have form. Its data are dynamically provided by its parent page
            and geoLocation operations. 
         3. It may require some "hidden" input variables that are shared among other embedded forms,
            and expect these variables to be set in its parent JSP. 
         4. This form behavior is automatic driven by its JS functions.   
    --%>
    <div id="FRAGMENT_PLACE_CREATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" 
         style="height:auto;">
        <div class="body-form-embedded-content" >
            <%-- SUB-TITLE AND GREETING --%>
            <span class="font-size-large font-bold"><jstl-fmt:message key="label_place_create" bundle="${msgprop}"/> </span>
            <div class="border-bottom-active-dark" style="margin-bottom:20px; "></div>

            <%-- PROGRESS WIDGET --%>
            <jstl-fmt:message key="info_place_create_greeting" var="progressBarText" bundle="${msgprop}" />  
            <jsp:include page="widget_progressBar.jsp" >
                <jsp:param name="id" value="FRAGMENT_PLACE_CREATE" />
                <jsp:param name="color" value="black" />
                <jsp:param name="size" value="small" />
                <jsp:param name="marginTop" value="20" />
                <jsp:param name="labelText" value="${progressBarText}" />
            </jsp:include>

        </div><%-- END body-form-embedded-content --%>
    </div><%-- END body-form-embedded-container --%>
</div>


<%-- END fragment_place_create_form.jsp --%>