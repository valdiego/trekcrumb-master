<!DOCTYPE html>
<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<%-- 3RD PARTY UI TAGS --%>
<%@ taglib prefix="jstl-core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="jstl-fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring-core" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form"%>

<%-- STATEFUL CONNECTION
     1) This is default J2EE setting
     2) Allow J2EE server to establish stateful connection with the client browser,
        maintain a default cookie with name 'JSESSIONID' where user session
        entities would be stored during the session. --%>
<%@ page session="true"%>

<%@ page import="com.trekcrumb.webapp.utility.WebAppConstants" %>
<%
    String homeURI = request.getContextPath();

    //Page summaries:
    String pageTitle = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TITLE);
    if(pageTitle == null) {
        pageTitle = WebAppConstants.APP_TITLE;
    }
    
    String pageSummary = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_SUMMARY);
    if(pageSummary == null) {
        pageSummary = WebAppConstants.APP_SUMMARY;
    }
%>

<%-- HTML TAG
     1) Use manifest by uncommenting this tag to enable HTML5 application cache. 
     2) "height = 100%" to ensure any page content that wants to occupy the entire
        screen height can do so. The child nodes must also declare the same height = 100% 
        whenever they need to. This is also to support "One-Page Sections Scroll" 
        responsiveness.
     3) Do NOT declare "width = 100%" since it can mess up the left/right margin and
        children nodes' widths consistency.
--%>
<%-- html manifest="<%=homeURI%>/config/trekcrumb.appcache" --%>
<html style="height:100%;"
      xmlns:og="http://ogp.me/ns#">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <%-- METADATA VIEWPORT
             Metadata to support browser behavior on mobile device (small touchable screen).
             Used by Bootstrap library, Google Map API, etc.
             1) initial-scale : Default initial scale on load
             2) user-scalable=no : Disable user from changing the size of content by pinching
         --%>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">        

        <%-- METADATA PAGE SUMMARY: 
             1. This common HEAD summary include page title, description and shortcut icon.
             2. They are dynamically populated based on what page that is currently on display. 
             3. For favicon, add "?r=xxx" in definition as random version number to beat caching
                on client browser .htaccess file.
        --%>
        <title><%=pageTitle%></title>
        <meta name="description" content="<%=pageSummary%>">
        <link rel="shortcut icon" href="<%=homeURI%>/images/favicon.ico?r=1" type="image/x-icon" />
        <link rel="icon" href="<%=homeURI%>/images/favicon.ico?r=1" type="image/x-icon" />
        
        <%-- METADATA SEARCH ENGINE --%>
        <%@ include file="include_headerSearchEngine.jsp" %>

        <%-- METADATA SOCIAL MEDIA --%>
        <%@ include file="include_headerSocialMedia.jsp" %>
        
        <%-- LIBRARIES: THIRD-PARTY (JS/CSS) --%>
       <%-- ** JQUERY --%>
        <script src="<%=request.getContextPath()%>/javascript/jquery-1.11.3.min.js"></script>
        <script src="<%=request.getContextPath()%>/javascript/jquery.easing.js"></script>

        <%-- ** YAHOO YQL-GEO LIBRARY
                Library for Geo Location by IP address --%>
        <script src="<%=homeURI%>/javascript/yqlgeo.js"></script>
        
        <%-- ** MOMENT FOR PARSE/DISPLAY DATES --%>
        <script src="<%=homeURI%>/javascript/moment.js"></script>

       <%-- ** FONTAWESOME FOR MENU-ICONS --%>
       <link rel="stylesheet" href="<%=homeURI%>/css/font-awesome-4.5.0/css/font-awesome.min.css">

        <%-- ** PLATFORM FOR PARSE/DISPLAY BROWSER IDENTITY --%>
        <script src="<%=homeURI%>/javascript/bestieJS_platform_1.3.1.js"></script>

        <%-- LIBRARIES: TREKCRUMB JS/CSS --%>
        <link type="text/css" rel="stylesheet"  href="<%=homeURI%>/css/trekcrumb-webapp.css"/>
        <link type="text/css" rel="stylesheet"  href="<%=homeURI%>/css/trekcrumb-layout.css"/>
        <script src="<%=homeURI%>/javascript/trekcrumb-properties.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-constants.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-constant-messages.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-variables.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-common.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-validation.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-responsive.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-menu.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-pagination.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-trip.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-picture.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-place.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-user.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-userauthtoken.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-geoLoc.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-map.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-map-google.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-home.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-favorite.js"></script>
        <script src="<%=homeURI%>/javascript/trekcrumb-comment.js"></script>
    
        <%-- CONFIGURE JSTL:FMT MESSAGE PROPERTIES 
             1) So that it works directly on JSP without depending on Spring Framework configuration
                (inside its trekcrumbwebapp-servlet.xml) --%>
        <jstl-fmt:setBundle basename="messages" var="msgprop"/>
    </head>
    
    <script>
        //Evaluates required global variables ONLY IF they have not been done before:
        if(mDevice_orientation == undefined || mDevice_orientation == null) {
            common_window_evaluateOrientation();
        }
        if(mDevice_isSupportAJAX == undefined || mDevice_isSupportAJAX == null) {
            common_AJAX_isSupported();
        }
        if(mDevice_isSupportLocaleStorage == undefined || mDevice_isSupportLocaleStorage == null) {
            common_localeStorage_isSupported();
        }
        if(mDevice_isSupportCookie == undefined || mDevice_isSupportCookie == null) {
            common_cookie_isSupported();
        }
        
    </script>    
</html>

<%-- END include_header.jsp --%>