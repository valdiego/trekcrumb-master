<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>
     
<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    boolean isPositionFixed = Boolean.parseBoolean(request.getParameter("isPositionFixed"));

    String labelSupportTerms = MessageResourceUtil.getMessageValue("label_support_terms", null, null);
    String labelSupportPrivacy = MessageResourceUtil.getMessageValue("label_support_privacy", null, null);
    String labelSupportFAQ = MessageResourceUtil.getMessageValue("label_support_faq", null, null);
    String labelSupportContactUs = MessageResourceUtil.getMessageValue("label_support_contactUs", null, null);
%>

<div class="container-footer font font-color-white"
<%   if(isPositionFixed) { %>
         style="position:fixed; bottom:0px; left:0px; right:0px;"
<%   } %>
>
    <div class="menu-footer-list-item font-bold" 
         onclick="common_support_terms(false)"><%=labelSupportTerms %></div>
    
    <div class="menu-footer-list-item font-bold" 
         onclick="common_support_privacy(false)"><%=labelSupportPrivacy %></div>
    
    <div class="menu-footer-list-item font-bold" 
         onclick="location.href='${pageContext.request.contextPath}/trekcrumb/support/faq'"><%=labelSupportFAQ %></div>

    <div class="menu-footer-list-item font-bold" 
         onclick="location.href='${pageContext.request.contextPath}/trekcrumb/support/contactus'"><%=labelSupportContactUs %></div>
</div>

<%-- END widget_footer.jsp --%>