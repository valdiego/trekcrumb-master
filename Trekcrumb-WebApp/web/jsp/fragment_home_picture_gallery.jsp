<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<div id="homePictureGalleryFragmentLayoutID"
     style="position:relative;">

    <%-- SUB-TITLE --%>
    <div class="font font-bold font-style-greeting"
         style="padding-top: 20px; opacity:0.5;">
        <jstl-fmt:message key="info_home_greeting_pictureGallery" bundle="${msgprop}"/>  
    </div>

    <%-- PROGRESS WIDGET --%>
    <jsp:include page="widget_progressBar.jsp" >
        <jsp:param name="id" value="FRAGMENT_PICTURE_LIST" />
        <jsp:param name="color" value="black" />
        <jsp:param name="size" value="small" />
        <jsp:param name="position" value="center-layout" />
        <jsp:param name="marginTop" value="60" />
    </jsp:include>

    <%-- PICTURE LIST SECTION --%>
    <jsp:include page="fragment_picture_list.jsp" >
        <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_LIST" />
        <jsp:param name="isDisplayUser" value="true" />
    </jsp:include>
    
    <%-- MENU MORE --%>
    <jsp:include page="widget_menuMoreNext.jsp" >
        <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_LIST" />
        <jsp:param name="menuNextMethodName" value="trip_search_Init()" />
    </jsp:include>
    
</div>

<%-- END fragment_home_picture_gallery.jsp --%>