<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    /*
     * Custom unique ID, if provided, will function as prefix to all elements in this widget.
     * This input is typically the name of page ('pageName') that embeds this widget. 
     */
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }
    
    String menuGoPreviousMethodName = request.getParameter("menuGoPreviousMethodName");
    if(menuGoPreviousMethodName == null) {
        menuGoPreviousMethodName = "";
    }
    
    String menuGoNextMethodName = request.getParameter("menuGoNextMethodName");
    if(menuGoNextMethodName == null) {
        menuGoNextMethodName = "";
    }
    
    String numberOfRecordsTotalString = request.getParameter("numberOfRecordsTotal");
    int numberOfRecordsTotal = -1;
    if(numberOfRecordsTotalString != null) {
        //Force to throw exception if value invalid:
        numberOfRecordsTotal = Integer.parseInt(numberOfRecordsTotalString);
    }
    
%>

<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<div id="<%=widgetPrefixID%>paginationListLayoutID" 
     style="position:absolute; top:0px; left:0px; right:0px; text-align:center;"
     var_pageList_numOfRowsMax = "<%=CommonConstants.RECORDS_NUMBER_MAX%>"
<%   if(numberOfRecordsTotal >= 0) { %>
         var_pageList_numRecordsTotal = "<%=numberOfRecordsTotal%>"
<%   } else { %>
         var_pageList_numRecordsTotal = "0"
<%   } %>
     var_pageList_offsetCurrent = ""
     var_pageList_pageNumberCurrent = "" >

    <%-- PAGE NUMBER --%>
    <div class="body-black-transparent" 
         style="padding:5px; display:inline-block;"> 
        <span id="<%=widgetPrefixID%>pageNumberID" class="font font-size-normal font-color-white"></span>
    </div>              
    
    <%-- PREVIOUS/NEXT MENU --%>
    <jsp:include page="widget_menuPreviousAndNext.jsp" >
        <jsp:param name="widgetPrefixID" value="<%=widgetPrefixID%>" />
        <jsp:param name="menuPreviousMethodName" value="<%=menuGoPreviousMethodName%>" />
        <jsp:param name="menuNextMethodName" value="<%=menuGoNextMethodName%>" />
        <jsp:param name="isPositionFixed" value="true" />
    </jsp:include>
    
</div>

<%-- END widget_pagination_list.jsp --%>