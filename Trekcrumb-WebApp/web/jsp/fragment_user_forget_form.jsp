<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%
    /*
     * Check if there is any info message from server to be displayed.
     */
    String userForgetSubmitConfirmMsg = (String)request.getAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
%>

<div id="userForgetLayoutID" class="body-form-embedded-container font font-size-normal">

    <%-- SUCCESS MESSAGE CONFIRMATION
         NOTE: 
         1. Upon form submission successful, the back-end server would prompt a confirmation, and 
            we must display it. We don't use the regular "info" in include_errorAndInfo.jsp because its
            automatic disappearing act.
         2. The div must be displayed = 'table' is to display image and long text
            to be vertically centered to each other 
    --%>
    <div id="userForgetSubmitSucccessMessageID" class="body-warning font-bold"
         style="text-align:left; display:none;">
        <div style="display:table-cell; padding-right:5px; vertical-align:middle;" >
            <span class="menu-icon font-color-warning fa fa-exclamation-triangle"></span>
        </div>
        <span id="userForgetSubmitSucccessMessageContentID" 
              style="display:table-cell; vertical-align:middle; padding-left:10px;"></span>
    </div>
        
    <div class="body-form-embedded-content" >
        <%-- SUB-TITLE AND GREETING --%>
        <div>
            <span class="font-size-large font-bold"><jstl-fmt:message key="label_user_my_forgetAccount" bundle="${msgprop}"/></span>
            <br/>
            <span><jstl-fmt:message key="info_user_forget_greeting" bundle="${msgprop}"/></span>
        </div>
        <div class="border-bottom-black" style="margin-bottom:20px; "></div>

        <%-- FORM --%>
        <spring-form:form id="userForgetFormID"
                          method="post" 
                          commandName="userForgetFormBean"
                          action="" >
            <div class="body-form-input-field">
                <span class="font-bold"><jstl-fmt:message key="label_user_email" bundle="${msgprop}"/></span>
                <br/>
                <spring-form:input path="email"
                                   id="userForgetEmailInputID"
                                   type="email"
                                   maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL)%>" />
            </div>
        </spring-form:form>

        <%-- FUNCTIONAL BUTTONS --%>
        <jstl-fmt:message key="label_common_submit" var="submitLabel" bundle="${msgprop}"/>  
        <jsp:include page="widget_menuCancelAndSubmit.jsp" >
            <jsp:param name="submitButtonIDPrefix" value="FRAGMENT_USER_FORGET" />
            <jsp:param name="submitLabel" value="${submitLabel}" />
            <jsp:param name="submitMethodName" value="user_forget_validateAndSubmit()" />
        </jsp:include>
        
    </div>
</div><%--END of userForgetLayoutID --%>
<script>
    if(mDevice_isSupportCookie) {
        document.getElementById('userForgetFormID').setAttribute("action", WEBAPP_URL_USER_FORGET_SUBMIT);
        
<%      if(!ValidationUtil.isEmpty(userForgetSubmitConfirmMsg)) { %>
            document.getElementById("userForgetSubmitSucccessMessageContentID").innerHTML = "<%=userForgetSubmitConfirmMsg%>";
            document.getElementById("userForgetSubmitSucccessMessageID").style.display = "table";
<%      } %>
        
    }
</script>

<%-- END fragment_user_forget_form.jsp --%>