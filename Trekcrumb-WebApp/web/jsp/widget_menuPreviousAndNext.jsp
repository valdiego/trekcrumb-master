<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }

    String menuPreviousMethodName = request.getParameter("menuPreviousMethodName");
    if(menuPreviousMethodName == null) {
        menuPreviousMethodName = "";
    }
    
    String menuNextMethodName = request.getParameter("menuNextMethodName");
    if(menuNextMethodName == null) {
        menuNextMethodName = "";
    }
    
    boolean isPositionFixed = Boolean.parseBoolean(request.getParameter("isPositionFixed"));
%>

<div id="<%=widgetPrefixID%>MenuGoPreviousID" 
     class="menu-icon body-black-transparent font-color-white
<%          if(isPositionFixed) { %>
                menu-previous-fixed
<%          } else { %>
                menu-previous-absolute
<%          } %>"
     style="opacity:0.75; display:none;"
     onmousedown="menu_Clicked(this)"
     onmouseup="menu_ClickedReset(this)"
     onclick="<%=menuPreviousMethodName%>"> 
    <span class="fa fa-chevron-left" ></span>
</div>

<div id="<%=widgetPrefixID%>MenuGoNextID" 
     class="menu-icon body-black-transparent font-color-white
<%          if(isPositionFixed) { %>
                menu-next-fixed
<%          } else { %>
                menu-next-absolute
<%          } %>"
     style="opacity:0.75; display:none;"
     onmousedown="menu_Clicked(this)"
     onmouseup="menu_ClickedReset(this)"
     onclick="<%=menuNextMethodName%>"> 
    <span class="fa fa-chevron-right" ></span>
</div>


<%-- END widget_menuPreviousAndNext.jsp --%>