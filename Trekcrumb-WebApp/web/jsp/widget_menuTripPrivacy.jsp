<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<%@ page import="com.trekcrumb.common.enums.TripPrivacyEnum" %>
<%
    String idPrefix = request.getParameter("id");
    if(idPrefix == null) {
        idPrefix = "";
    }

    String labelTitle = request.getParameter("labelTitle");
    if(labelTitle == null) {
        labelTitle = "";
    }
    
    String labelPublic = request.getParameter("labelPublic");
    if(labelPublic == null) {
        labelPublic = "";
    }

    String labelPrivate = request.getParameter("labelPrivate");
    if(labelPrivate == null) {
        labelPrivate = "";
    }
    
    String privacyPublicMessage = request.getParameter("privacyPublicMessage");
    String privacyPrivateMessage = request.getParameter("privacyPrivateMessage");
    
    String privacyValueCurrent = request.getParameter("privacyValueCurrent");
    if(privacyValueCurrent == null) {
        privacyValueCurrent = "";
    }
%>

<div id="tripPrivacyOptionLayoutID" 
     style="margin-top:20px; ">
    <div style="width:30%; vertical-align:top; display:inline-block;">
        <span class="font-bold"><%=labelTitle%></span>
    </div>
    
    <%-- SELECTION SECTION --%>
    <div style="vertical-align:top; display:inline-block;">
        <input id="<%=idPrefix%>tripPrivacyPublicInputID"
               type="radio"
               name="privacy"
               value="<%=TripPrivacyEnum.PUBLIC.getValue()%>"
<%             if(privacyValueCurrent == TripPrivacyEnum.PUBLIC.getValue()) { %>                                
                   checked="checked"
<%             } %>                                
               onchange="trip_generic_privacy_displayWarning('<%=idPrefix%>')" />
        <label for="<%=idPrefix%>tripPrivacyPublicInputID"><%=labelPublic%></label>
        <br/>
        <input id="<%=idPrefix%>tripPrivacyPrivateInputID"
               type="radio"
               name="privacy"
               value="<%=TripPrivacyEnum.PRIVATE.getValue()%>"
<%             if(privacyValueCurrent == TripPrivacyEnum.PRIVATE.getValue()) { %>                                
                   checked="checked"
<%             } %>                                
               onchange="trip_generic_privacy_displayWarning('<%=idPrefix%>')" />
        <label for="<%=idPrefix%>tripPrivacyPrivateInputID"><%=labelPrivate%></label>
    </div>
    
    <%-- WARNING MESSAGE SECTION --%>
    <div class="body-warning"
         style="margin-top:10px; display:inline-block;">
        <div style="display:table;">
            <div style="display:table-cell; padding-right:5px; vertical-align:middle;" >
                <span class="menu-icon font-color-warning fa fa-exclamation-triangle"></span>
            </div>
            <span id="<%=idPrefix%>privacyPublicTextWarningID" 
<%          if(privacyValueCurrent == TripPrivacyEnum.PUBLIC.getValue()) { %>                                
                style="vertical-align:middle; display:table-cell;" >
<%          } else { %>                                
                style="vertical-align:middle; display:none;" >
<%          } %>                                        
                <%=privacyPublicMessage%>
            </span>
            
            <span id="<%=idPrefix%>privacyPrivateTextWarningID" 
<%          if(privacyValueCurrent == TripPrivacyEnum.PRIVATE.getValue()) { %>                                
                style="vertical-align:middle; display:table-cell;" >
<%          } else { %>                                
                style="vertical-align:middle; display:none;" >
<%          } %>                                        
                <%=privacyPrivateMessage%>
            </span>
        </div>
    </div>
</div>
            

<%-- END widget_menuTripPrivacy.jsp --%>