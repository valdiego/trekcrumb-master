<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<%-- METADATA SOCIAL MEDIA SUMMARY: TITLE, DESCRIPTION, ICON/LOGO
     1) The Social Media enabled metadata is based on Open Graph protocol that enables a 
        web page to become a rich object in a social graph. Common current usage was to embed 
        a link of a website onto a social media (Facebook, Twitter, Google+, etc), then the 
        host social media was able to display the web page into graph objects such as: a 
        summary snippet, a card with media (picture/video/etc.)
     2) To enable a web page as open graph objects, we need to add these required metadata in the <HEAD>
        -> Meta property="og:xyz" is used by Facebook
        -> Meta name="twitter:xyz" is used by Twitter. If required meta not provided, it would 
           look for the equivalient "og:xyz". 
        -> We can combine these into one meta for equivalent metadata.
     3) For image: 
        -> Minimum size = 120px by 120px, and < 1MB in file size. 
        -> Best practice is to define image dimension: og:image:width x og:image:height 
           to avoid it to cache the URL without the image (image was downloaded async). 
           REF: https://developers.facebook.com/docs/sharing/best-practices
--%>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppConstants" %>
<%
    String pageIdentityType_forSocialMedia = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TYPE);
    String pageURL_forSocialMedia = null;
    String pageTitle_forSocialMedia = null;
    String pageSummary_forSocialMedia = null;
    String pageImageURL_forSocialMedia = null;
    if(pageIdentityType_forSocialMedia != null) {
        pageURL_forSocialMedia = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_URL);
        pageTitle_forSocialMedia = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TITLE);
        if(pageTitle_forSocialMedia == null) {
            pageTitle_forSocialMedia = WebAppConstants.APP_TITLE;
        }
        
        pageSummary_forSocialMedia = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_SUMMARY);
        if(pageSummary_forSocialMedia == null) {
            pageSummary_forSocialMedia = WebAppConstants.APP_SUMMARY;
        }

        pageImageURL_forSocialMedia = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_IMAGEURL);
    }
    
%>
<%  if(pageIdentityType_forSocialMedia != null) { %>
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="trekcrumb.com" />
        <meta property="og:title"  name="twitter:title" content="<%=pageTitle_forSocialMedia%>" />
        <meta property="og:description" name="twitter:description"  content="<%=pageSummary_forSocialMedia%>" />
        <meta property="og:image" name="twitter:image"  content="<%=pageImageURL_forSocialMedia%>" /> 
        <meta property="og:image:width" content="200" />
        <meta property="og:image:height" content="200" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@trekcrumb" />
        <meta name="twitter:creator" content="@trekcrumb" />
        <meta name="twitter:domain" content="trekcrumb.com">
<%  } %>

<%-- END include_headerSocialMedia.jsp --%>