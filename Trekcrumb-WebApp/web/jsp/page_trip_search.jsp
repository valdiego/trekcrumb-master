<%@ include file="include.jsp" %>

<body>
    <%-- PAGE TITLE --%>
    <div class="container-title">
        <span id="pageTitleID" class="font font-style-title"><jstl-fmt:message key="label_trip_list_search" bundle="${msgprop}"/></span>
    </div>

    <%-- MAIN BODY --%>
    <div id="searchLayoutID" class="container-body" 
         style="text-align:center;">
        <%-- BREADCRUMBS --%>
        <div id="searchCriteriaCrumbsID" class="body-inside-border font font-size-normal"
             style="margin-top:50px; display:none;"></div>
        
        <%-- PROGRESS WIDGET --%>
        <jsp:include page="widget_progressBar.jsp" >
            <jsp:param name="id" value="FRAGMENT_TRIP_LIST" />
            <jsp:param name="color" value="black" />
            <jsp:param name="size" value="large" />
            <jsp:param name="position" value="center-screen" />
        </jsp:include>

        <%-- TRIP LIST SECTION --%>
        <jsp:include page="fragment_trip_list.jsp" >
            <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_LIST" />
            <jsp:param name="tripRowStyleCSS" value="layout-triplist-row-gallery" />
        </jsp:include>
        
        <%-- PAGINATION WIDGET --%>
        <jsp:include page="widget_pagination_list.jsp" >
            <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_LIST" />
            <jsp:param name="menuGoPreviousMethodName" value="trip_search_Previous()" />
            <jsp:param name="menuGoNextMethodName" value="trip_search_Next()" />
        </jsp:include>

        <%-- MENU PAGE SECTION --%>
        <div id="TripSearchMenuPageMainLayoutID" 
             style="position:fixed; left:5px; bottom:5px;" >
            <div id="TripSearchMenuPageShowID" class="menu-page-transparent menu-icon hoverhints-menuIcon"
                 style="display:inline-block;" 
                 onmouseover="menu_hover_LabelShow(this)"
                 onmouseout="menu_hover_LabelHide(this)"
                 onmousedown="menu_Clicked(this)"
                 onmouseup="menu_ClickedReset(this)"
                 onclick="trip_search_FormInit()" >
                <span class="fa fa-search font-color-menu" ></span>
                <div id="TripSearchMenuPageShowIDHover" 
                      class="hoverhints-menuIcon-text-container-leftTop" 
                      style="display: none;">
                    <span class="hoverhints-menuIcon-text-body-leftTop"><jstl-fmt:message key="label_trip_search" bundle="${msgprop}"/></span>
                </div>
            </div>

        </div>
    </div><%-- END OF searchLayoutID --%>
    <%@ include file="fragment_trip_search_form.jsp" %>
    
    <script>
        trip_search_Current();
        responsive_adjustTripSearchLayout();
        
        /* 
         * On resize:
         * -> Do NOT put on <body onresize=""> tag anymore because it could be executed
         *    before the page completes resizing, causing getting invalid element's 
         *    values/states (offsetWidth, offsetHeight, etc) which were previous values.
         * -> Even in here, we MUST put a 'waiting period' to make sure client browser has 
         *    completely configure all DOM elements during resize for the same reason. 
         * -> Operations: Determine which fragment to display and their responsiveness.
         */
        window.addEventListener("resize", function() {
            setTimeout(function() {
                responsive_adjustTripSearchLayout();
            }, (1000));
        });
        
    </script>
</body>

<%-- END page_trip_search.jsp --%>