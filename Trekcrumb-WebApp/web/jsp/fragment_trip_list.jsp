<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%
    /*
     * Custom unique ID, if provided, will function as prefix to all elements in this widget.
     * This input is typically the name of page ('pageName') that embeds this widget. 
     */
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }
    
    
    String tripRowStyleCSS = request.getParameter("tripRowStyleCSS");
    if(tripRowStyleCSS == null) {
        tripRowStyleCSS = "layout-triplist-row";
    }
%>

<div id="<%=widgetPrefixID%>tripListLayoutID" class="layout-triplist"
     style="display:none;">

    <%-- LIST CONTENT --%>
    <div id="<%=widgetPrefixID%>tripListContentID" class="font font-size-normal" 
         style="margin-left:auto; margin-right: auto;">
        <%-- NOTE:
             This is the content where it could be either empty message, or the list of rows
             copy-pasted from the widget and populated. --%>
    </div>
    
    <%-- ROW TEMPLATE WIDGET --%> 
    <jsp:include page="widget_row_trip.jsp" >
        <jsp:param name="customCSS" value="<%=tripRowStyleCSS%>" />
    </jsp:include>

</div>



<%-- END of fragment_trip_list.jsp --%>