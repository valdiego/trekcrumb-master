<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> "a" tag is the outer element that wraps the main content as a link. 
        Set is "vertical-align = top" so that multiple rows can align
        horizontally parallel when they are display side by side.
     -> "favoriteUserRow" element is the main content of the row. Set "position = relative", 
        so that any element within can be position absolute anywhere inside its body.
     -> Some of IDs will later be modified to be unique for each rows.
--%>
<a id="favoriteByTripRowTemplateID" class="hoverhints-menuIcon"
   style="vertical-align:top; display:none;"
   onmouseover="menu_hover_LabelShow(this)"
   onmouseout="menu_hover_LabelHide(this)"
   href="#">
    <div name="favoriteUserRow" 
         style="margin:5px; display:inline-block;">
        <span id="userImageDefaultID" class="image image-profile-thumbnail fa fa-user"></span>
        <img id="userImageID" class="image image-profile-thumbnail" style="display:none;" />
    </div>
    <div id="hoverID" 
          class="hoverhints-menuIcon-text-container-leftTop" 
          style="bottom:68px; display: none;">
        <div class="hoverhints-menuIcon-text-body-leftTop hoverhints-userDetail">
            <span id="userFullnameID" class="font-bold"></span><br/>
            <span id="usernameID"></span>
        </div>
    </div>
</a>

<%-- END widget_row_favorite_usersList.jsp --%>