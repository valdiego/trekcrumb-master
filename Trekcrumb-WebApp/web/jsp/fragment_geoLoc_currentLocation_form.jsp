<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
        with a shade, making the previous main content un-operable (all clickables 
        become non functional).
     2. It does NOT have form.
     3. Its submit operation, success and failure are driven by the caller page functions.
--%>
<div id="geoLocCurrentLocationLayoutID" class="body-disabled-background" 
     style="display:none;"
     var_geoLoc_isSearchForLocation = "true"
     var_geoLoc_callerFragment = "null" 
     var_geoloc_locationCurrent = "null" 
     var_geoloc_locationAddressString = "null" >
    <div class="body-inside-border body-form-embedded-container font font-size-normal" 
         style="height:auto;">
        <div class="body-form-embedded-content" >

            <%-- SUB-TITLE AND GREETING --%>
            <span class="font-size-large font-bold"><jstl-fmt:message key="label_place_locationCurrent" bundle="${msgprop}"/> </span>
            <div class="border-bottom-active-dark" style="margin-bottom:20px; "></div>

            <div class="body-position-center">
                <%-- PROGRESS WIDGET --%>
                <jstl-fmt:message key="info_place_pleaseWaitGetCurrentGPSLoc" var="widgetGreetingText" bundle="${msgprop}"/>  
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_GEOLOC_CURRENTLOCATION" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="position" value="center-layout" />
                    <jsp:param name="marginTop" value="10" />
                    <jsp:param name="labelText" value="${widgetGreetingText}" />
                </jsp:include>

                <%-- MESSAGE --%>
                <div class="font font-size-normal font-italic">
                    <span id="geoLocCurrentLocationMessageID" style="display:none;"></span>
                </div>

                <%-- FUNCTIONAL BUTTON --%>
                <div style="margin-top:20px;">
                    <input id="geoLocCurrentLocationCancelBtnID"
                           type="submit" 
                           class="button-passive"
                           style="width:40%; display: inline-block;"
                           name="submit"
                           value="Cancel"
                           onclick="geoLoc_currentLocation_cancel()">
                    <input id="geoLocCurrentLocationRetryBtnID"
                           type="submit" 
                           class="button-active"
                           style="width:40%; display:none;"
                           name="submit"
                           value="Retry"
                           onclick="geoLoc_currentLocation_retry()">
                </div>
            </div> 
        
        </div><%-- END body-form-embedded-content --%>
    </div><%-- END body-form-embedded-container --%>
</div>

<%-- END fragment_geoLoc_currentLocation_form.jsp --%>