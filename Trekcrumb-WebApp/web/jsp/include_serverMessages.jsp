<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<%-- NOTE: 
     1. This include must be the LAST in a page! This is because many pages/fragments would 
        enforce common_errorOrInfoMessage_hide() call in their init-mechanism which would
        erase any error/info messages. By putting this last, it would stay there. 
--%>
<%@ page import="com.trekcrumb.common.bean.ServiceResponse" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    String infoMessageFromServer = null;
    String errorMessageFromServer = null;
    
    //Find messages from server ServiceResponse, if any:
    ServiceResponse serviceResponse_forSvrMsg = 
            (ServiceResponse) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE);
    if(serviceResponse_forSvrMsg != null) {
        if(serviceResponse_forSvrMsg.isSuccess()) {
            infoMessageFromServer = serviceResponse_forSvrMsg.getSuccessMessage();
        } else {
            if(serviceResponse_forSvrMsg.getServiceError() != null) {
                errorMessageFromServer = serviceResponse_forSvrMsg.getServiceError().getErrorText();
            }
        }
    }
    
    //Find messages from embeded request attributes:
    if(ValidationUtil.isEmpty(infoMessageFromServer)) {
        infoMessageFromServer = (String) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_INFO);
    }
    if(ValidationUtil.isEmpty(errorMessageFromServer)) {
        errorMessageFromServer = (String) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_MESSAGE_ERROR);
    }
    
    //Neutralize them:
    infoMessageFromServer = WebAppUtil.neutralizeStringValue(infoMessageFromServer, false, false, false, -1);   
    errorMessageFromServer = WebAppUtil.neutralizeStringValue(errorMessageFromServer, false, false, false, -1);   
%>

<script>
<% if(!ValidationUtil.isEmpty(errorMessageFromServer)) { %>
       common_errorOrInfoMessage_show(["<%=errorMessageFromServer%>"], "<%=infoMessageFromServer%>");
<% } else { %>
       common_errorOrInfoMessage_show(null, "<%=infoMessageFromServer%>");
<% } %>
</script>


<%-- END include_serverMessages.jsp --%>