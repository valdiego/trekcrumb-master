<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>
<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }

    String menuPageShowMethodName = request.getParameter("menuPageShowMethodName");
    if(menuPageShowMethodName == null) {
        menuPageShowMethodName = "";
    }
    
    String menuPageHideMethodName = request.getParameter("menuPageHideMethodName");
    if(menuPageHideMethodName == null) {
        menuPageHideMethodName = "";
    }

    boolean isColorWhite = Boolean.parseBoolean(request.getParameter("isColorWhite"));
    
    String labelCommonMenuShow = MessageResourceUtil.getMessageValue("label_common_menu_show", null, null);
%>

<div id="<%=widgetPrefixID%>MenuPageShowID" 
<%   if(isColorWhite) { %>
         class="menu-page-black menu-icon hoverhints-menuIcon font-color-white"
         style="opacity:0.75; display:inline-block; "
<%   } else { %>
         class="menu-page-transparent menu-icon hoverhints-menuIcon font-color-menu"
         style="display:inline-block; "
<%   } %>
     onmouseover="menu_hover_LabelShow(this)"
     onmouseout="menu_hover_LabelHide(this)"
     onmousedown="menu_Clicked(this)"
     onmouseup="menu_ClickedReset(this)"
     onclick="<%=menuPageShowMethodName%>">
    <span class="fa fa-cog"></span>
    <div id="<%=widgetPrefixID%>MenuPageShowIDHover" 
          class="hoverhints-menuIcon-text-container-leftTop" 
          style="display: none;">
        <span class="hoverhints-menuIcon-text-body-leftTop"><%=labelCommonMenuShow %></span>
    </div>
</div>

<div id="<%=widgetPrefixID%>MenuPageHideID"
<%   if(isColorWhite) { %>
         class="menu-page-black menu-icon font-color-white"
         style="opacity:0.75; display:none; "
<%   } else { %>
         class="menu-page-transparent menu-icon font-color-menu"
         style="display:none; "
<%   } %>
     onmousedown="menu_Clicked(this)"
     onmouseup="menu_ClickedReset(this)"
     onclick="<%=menuPageHideMethodName%>">
    <span class="fa fa-minus"></span>
</div>


<%-- END widget_menuPageShowOrHide.jsp --%>