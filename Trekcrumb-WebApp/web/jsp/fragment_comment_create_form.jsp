<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip tripSelected_forCommentCreate = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
%>
<% if(tripSelected_forCommentCreate != null) { %>
    <div id="commentCreateLayoutID" class="body-disabled-background" 
         style="display:none;">
        <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                with a shade, making the previous main content un-operable (all clickables 
                become non functional).
             2. It has a form. 
             3. It may require some "hidden" input variables that are shared among other embedded forms,
                and expect these variables to be set in its parent JSP. 
        --%>

        <div id="FRAGMENT_COMMENT_CREATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
            <div class="body-form-embedded-content" >
                <%-- SUB-TITLE AND GREETING --%>
                <jstl-fmt:message key="label_comment_create" var="labelText" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuClose.jsp" >
                    <jsp:param name="id" value="FRAGMENT_COMMENT_CREATE" />
                    <jsp:param name="labelText" value="${labelText}" />
                    <jsp:param name="closeMethodName" value="comment_create_cancel()" />
                </jsp:include>
                <div class="border-bottom-black" style="margin-bottom:20px; "></div>

                <%-- PROGRESS WIDGET --%>
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_COMMENT_CREATE" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="marginTop" value="20" />
                </jsp:include>

                <%-- FORM --%>
                <div id="FRAGMENT_COMMENT_CREATEformID">
                    <%-- READ-ONLY DETAILS --%>
                    <div style="margin-top:10px;">
                        <table>
                            <tr style="vertical-align:top;">
                                <td width="25%" class="font-bold">
                                    <jstl-fmt:message key="label_trip_name" bundle="${msgprop}"/>
                                </td>
                                <td><%=tripSelected_forCommentCreate.getName()%></td>
                            </tr>
                        </table>
                    </div>

                    <%-- COMMENT NOTE --%>
                    <div style="margin-top:10px; margin-bottom:20px;">
                        <textarea id="commentCreateNoteInputID" 
                                  maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_COMMENT_NOTE %>" 
                                  cols="50" rows="4" ></textarea>
                     </div>

                    <%-- FUNCTIONAL BUTTONS --%>
                    <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                        <jsp:param name="submitLabel" value="${submitLabel}" />
                        <jsp:param name="submitMethodName" value="comment_create_validateAndSubmit()" />
                        <jsp:param name="cancelMethodName" value="comment_create_cancel()" />
                    </jsp:include>
                </div><%-- END formID --%>

            </div><%-- END body-form-embedded-content --%>
        </div><%-- END body-form-embedded-container --%>
    </div><%-- END commentCreateLayoutID --%>
<% } %>      


<%-- END fragment_comment_create_form.jsp --%>