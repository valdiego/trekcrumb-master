<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%-- Layout to display user's list of pictures.
     1. On page (document) load: By default this fragment is hidden and empty. Upon some specific
        events (user click menu, etc.), then it triggers call JS functions to
        retrieve the picture list and images. 
        See: user_view_DisplaySelectedTab()
     2. Any pagination and MenuPage sections should be AFTER main layout (image, etc.) such that on narrow
        view, they will be on-top of the main layout.
 --%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    User user_forUserPictures = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
    boolean isUserLoginProfile_forUserPictures = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<%  if(user_forUserPictures != null) { %>
        <div id="userPicturesLayoutID" 
             class="layout body-halfOrFullWidth body-halfOrFullWidth-secondary body-scroll menu-sliding"
             style="height:100%; display:none;"
             var_user_username="<%=user_forUserPictures .getUsername()%>" 
             var_user_isUserLoginProfile="<%=isUserLoginProfile_forUserPictures%>" >

            <%-- SUB-TITLE --%>
            <div class="font font-size-normal font-bold"
                style="text-align:center; padding-top:40px;">
                <%=user_forUserPictures.getFullname()%>&nbsp;<jstl-fmt:message key="label_picture" bundle="${msgprop}"/> 
            </div>

            <%-- PROGRESS WIDGET --%>
            <jsp:include page="widget_progressBar.jsp" >
                <jsp:param name="id" value="FRAGMENT_PICTURE_LIST_USER_PICTURES" />
                <jsp:param name="color" value="black" />
                <jsp:param name="size" value="small" />
                <jsp:param name="position" value="center-layout" />
                <jsp:param name="marginTop" value="60" />
            </jsp:include>

            <%-- LIST SECTION --%>
            <jsp:include page="fragment_picture_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_LIST_USER_PICTURES" />
            </jsp:include>
        
            <%-- PAGINATION WIDGET --%>
            <jsp:include page="widget_pagination_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_LIST_USER_PICTURES" />
                <jsp:param name="menuGoPreviousMethodName" value="picture_list_userPictures_previous()" />
                <jsp:param name="menuGoNextMethodName" value="picture_list_userPictures_next()" />
            </jsp:include>

        </div>
<%  } %>    

<%-- END fragment_user_pictures.jsp --%>