<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }
    
    String errorMessage = request.getParameter("errorMessage");
    if(errorMessage == null) {
        errorMessage = "Input value is invalid!";
    }
%>
<%-- NOTE:
     1. "widgetPrefixID" is required to uniquely identified this widget usage within a page. In one page/view,
         there could be more than one usages. THe value could be page/fragment name, etc.
     2. The "widgetPrefixID" + "Error" becomes the ID that is used in javascript to display/hide the widget.
     3. The div with display 'table' is to display image and long text to be vertically centered to each other 
--%>
<div id="<%=widgetPrefixID%>Error" class="body-error"
     style="margin:0px; display:none;">
    <div style="display:table;">
        <div style="display:table-cell; vertical-align:middle; vertical-align:middle;" >
            <%-- span class="font font-color-error font-size-xlarge font-bold">&#8855;</span --%>
            <span class="menu-icon font-color-error fa fa-minus-circle"></span>
        </div>
        <span style="display:table-cell; vertical-align:middle;"><%=errorMessage%></span>
    </div>
</div>                    


<%-- END widget_inputFieldError.jsp --%>