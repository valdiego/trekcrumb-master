<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String idPrefix = request.getParameter("id");
    if(idPrefix == null) {
        idPrefix = "";
    }

    String label = request.getParameter("label");
    if(label == null) {
        label = "";
    }
    
    String warningMessage = request.getParameter("warning");

    /*
     * Widget margin-top value, if provided.
     */
    int marginTop = 0;
    String marginTopStr = request.getParameter("marginTop");
    if(marginTopStr != null && marginTopStr.trim() != "") {
        try {
            marginTop = Integer.parseInt(marginTopStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }

    /*
     * Widget margin-bottom value, if provided.
     */
    int marginBottom = 0;
    String marginBottomStr = request.getParameter("marginBottom");
    if(marginBottomStr != null && marginBottomStr.trim() != "") {
        try {
            marginBottom = Integer.parseInt(marginBottomStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }
%>

<div style="margin-top:<%=marginTop%>px; margin-bottom:<%=marginBottom%>px;">
    <input id="<%=idPrefix%>deleteCheckboxID" 
           type="checkbox" 
           name="deleteCheckbox"
           onclick="common_deleteCheckbox_clicked('<%=idPrefix%>')" />
    <label for="<%=idPrefix%>deleteCheckboxID" class="font-bold"><%=label%></label>
    
<%  if(warningMessage != null) {%>    
        <%-- INPUT WARNING MESSAGE --%>       
        <jsp:include page="widget_inputFieldWarning.jsp" >
            <jsp:param name="widgetPrefixID" value="<%=idPrefix%>" />
            <jsp:param name="warningMessage" value="<%=warningMessage%>" />
        </jsp:include>
<%  } %>    
</div>               

<%-- END widget_menuDelete.jsp --%>