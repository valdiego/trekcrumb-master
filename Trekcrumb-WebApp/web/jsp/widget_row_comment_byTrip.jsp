<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> The difference between widget_row_comment byTrip vs. byUser:
        ->> byUser: The entire row body is clickable to link to Trip Detail in question.
            Its main layout is also an <a href> so user can right-click and open a new tab.
        ->> byTrip: Only the user section is clickable to User Detail in question. 
            User section is an <a href> so user can right-click and open a new tab.  
     -> Set is "vertical-align = top" so that multiple rows can align
        horizontally parallel when they are display side by side.
     -> Some of IDs will later be modified to be unique for each rows: noteID, etc.
--%>
<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    String labelCommonAdded = MessageResourceUtil.getMessageValue("label_common_added", null, null);
    String labelCommentDelete = MessageResourceUtil.getMessageValue("label_comment_delete", null, null);
%>

<div id="commentRowTemplateID"
     style="vertical-align:top; display:none;">
    <div name="commentRow" class="body-inside font"
         style="margin-top:5px; margin-bottom:0px; background-color:white;">
         
        <table width=100%>
            <tr>
                <td>
                    <%-- USER SECTION --%>
                    <a id="commentUserSectionID" class="body-clickable" 
                       onmousedown="menu_Clicked(this)" 
                       onmouseup="menu_ClickedReset(this)" 
                       href="#">
                        <jsp:include page="widget_userThumbnail.jsp" />
                    </a>        

                    <%-- COMMENT DETAIL --%>
                    <div class="font-size-small">
                        <%=labelCommonAdded %>&nbsp;
                        <span id="createdDateID" ></span>
                    </div>
                    <div id="commentNoteID" class="font-size-normal font-style-note"
                         style="margin-top:10px;"></div>
                </td>
                <td id="menuCommentDeleteLayoutID" width="53px"
                    style="text-align:right; vertical-align:top; padding:0px;">
                    <%-- Note: The menu-icon image is 48x48px, reserve the TD element with extra 5px. 
                         Then position the content at top-right corner of the TD element.
                    --%>
                    <div id="menuCommentDeleteID" 
                         class="hoverhints-menuIcon menu-icon font-color-menu"
                         style="display:inline-block;" 
                         onmouseover="menu_hover_LabelShow(this)"
                         onmouseout="menu_hover_LabelHide(this)"
                         onmousedown="menu_Clicked(this)"
                         onmouseup="menu_ClickedReset(this)"
                         onclick="" >
                        <span class="fa fa-trash" ></span>
                        <div id="menuCommentDeleteIDHover" 
                              class="hoverhints-menuIcon-text-container-rightBottom" 
                              style="display: none;">
                            <span class="hoverhints-menuIcon-text-body-rightBottom"><%=labelCommentDelete %></span>
                        </div>
                    </div>
                </td>
            </tr>
        </table>        
        <div class="border-bottom-passive"></div>        
    </div>
</div>


<%-- END of widget_row_comment_byTrip.jsp --%>