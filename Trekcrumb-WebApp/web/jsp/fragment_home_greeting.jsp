<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<div id="homeGreetingFragmentLayoutID"
     style="position:relative;">
     
    <%-- Greeting Message: See its layout CSS for dynamic positioning. --%>
    <div id="homeGreetingFragmentMessageID" class="body-white-transparent font font-style-greeting">
        <jstl-fmt:message key="info_home_greeting_welcome" bundle="${msgprop}"/>
    </div>
</div>


<%-- END fragment_home_greeting.jsp --%>