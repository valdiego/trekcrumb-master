<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPictureCover = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);

    String tripJSON_forPictureCover = null;
    if(trip_forPictureCover != null) {
        if(trip_forPictureCover.getNumOfPictures() > 0) {
            tripJSON_forPictureCover = (new ObjectMapper()).writeValueAsString(trip_forPictureCover);
        }
    }
%>

<% if(trip_forPictureCover != null && trip_forPictureCover.getNumOfPictures() > 0) { %>
    <div id="tripCoverImageLayoutID" 
         style="margin-top:10px; margin-bottom:10px; margin-left:auto; margin-right:auto; display:block;" >

        <%-- PictureCover's image must fit its container 100% so that its dimension will readjust as 
             the container width changes per device display. Its aspect ratio should stay original hence
             the height = 'auto'. --%>
        <img id="picCoverImageID<%=trip_forPictureCover.getId()%>" class="image" 
             style="width:100%; height:auto;" />
             
        <div id="picCoverNoteLayoutID" class="body-black" width="100%" 
             style="padding:10px; display:none;">
            <span id="picCoverNoteID" class="font font-size-normal font-color-white"></span>
        </div>
    </div>

    <script>
        picture_cover_ImageRetrieve(<%=tripJSON_forPictureCover%>);
    </script>
<% } %>

<%-- END fragment_picture_cover.jsp --%>