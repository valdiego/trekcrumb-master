<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<div id="errorAndInfoLayoutID" class="menu-sliding" 
     style="display:none;">
    <%-- ERROR --%>
    <div id="errorLayoutID" class="body-error">
        <div class="body-clickable" 
             style="display:table;"
             onclick="common_errorOrInfoMessage_hide()" >
            <%-- The div with display 'table' is to display image and long text
                 to be vertically centered to each other --%>
            <div style="display:table-cell; padding-right:5px; vertical-align:middle;" >
                <span class="menu-icon font-color-error fa fa-minus-circle"></span>
            </div>
            <span id="errorContentID" class="font font-size-normal" 
                  style="display:table-cell; vertical-align:middle;" ></span>
        </div>
    </div>
    
    <%-- INFO --%>
    <div id="infoLayoutID" class="body-info">
        <div class="body-clickable" 
             style="display:table;"
             onclick="common_errorOrInfoMessage_hide()" >
            <%-- The div with display 'table' is to display image and long text
                 to be vertically centered to each other --%>
            <div style="display:table-cell; padding-right:5px; vertical-align:middle;" >
                <span class="menu-icon font-color-info fa fa-info-circle"></span>
            </div>
            <span id="infoContentID" class="font font-size-normal" 
                  style="display:table-cell; vertical-align:middle;"></span>
        </div>
    </div>
</div>

<%-- END include-errorAndInfo.jsp --%>