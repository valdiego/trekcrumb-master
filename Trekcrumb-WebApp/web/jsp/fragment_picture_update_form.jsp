<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    Trip tripSelected_forPicUpdate = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forPicUpdate = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>
<% if(tripSelected_forPicUpdate != null && isUserHasAccessToTrip_forPicUpdate) { %>
    <div id="pictureUpdateLayoutID" class="body-disabled-background" 
         style="display:none;">
        <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                with a shade, making the previous main content un-operable (all clickables 
                become non functional).
             2. It has a form. By default out of server, the form is EMPTY. JS will populate them.
             3. It may require some "hidden" input variables that are shared among other embedded forms,
                and expect these variables to be set in its parent JSP. 
        --%>

        <div id="FRAGMENT_PICTURE_UPDATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
            <div class="body-form-embedded-content" >
                <%-- SUB-TITLE AND GREETING --%>
                <jstl-fmt:message key="label_picture_edit" var="labelText" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuClose.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_UPDATE" />
                    <jsp:param name="labelText" value="${labelText}" />
                    <jsp:param name="closeMethodName" value="picture_update_cancel()" />
                </jsp:include>
                <div class="border-bottom-black" style="margin-bottom:20px; "></div>

                <%-- PROGRESS WIDGET --%>
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_UPDATE" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="marginTop" value="20" />
                </jsp:include>

                <%-- FORM --%>
                <div id="FRAGMENT_PICTURE_UPDATEformID">
                    <%-- READ-ONLY DETAILS --%>
                    <div style="margin-top:10px;">
                        <table>
                            <tr style="vertical-align:top;">
                                <td width="25%" class="font-bold">
                                    <jstl-fmt:message key="label_trip_name" bundle="${msgprop}"/>
                                </td>
                                <td><%=tripSelected_forPicUpdate.getName()%></td>
                            </tr>
                            <tr style="vertical-align:top;">
                                <td width="25%" class="font-bold">
                                    Picture
                                </td>
                                <td>
                                    <span id="picUpdateIndexID"></span>
                                </td>
                            </tr>
                        </table>

                        <%-- IMAGE REVIEW --%>
                        <div style="text-align:center; margin-top:10px;">
                            <img id="pictureUpdateImageID" 
                                 style="width:33%" />
                        </div>
                        <input id="pictureID" type="hidden" />
                    </div>

                    <%-- EDITABLE SECTION
                         NOTE: Show/hide when delete checkbox is selected. --%>    
                    <div id="FRAGMENT_PICTURE_UPDATEeditableSectionID">
                        <%-- PICTURE NOTE --%>
                        <div style="margin-top:20px;">
                            <span class="font-bold"><jstl-fmt:message key="label_common_notes" bundle="${msgprop}"/></span><br/>
                            <textarea id="pictureUpdateNoteInputID" 
                                      maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE %>" 
                                      cols="50" rows="4" ></textarea>
                        </div>
                    </div><%-- END editableSectionID --%>

                    <%-- DELETE CHECKBOX --%>
                    <jstl-fmt:message key="label_picture_delete" var="label" bundle="${msgprop}"/>  
                    <jstl-fmt:message key="info_common_deleteWarning" var="warningMsg" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuDelete.jsp" >
                        <jsp:param name="id" value="FRAGMENT_PICTURE_UPDATE" />
                        <jsp:param name="label" value="${label}" />
                        <jsp:param name="warning" value="${warningMsg}" />
                        <jsp:param name="marginTop" value="20" />
                        <jsp:param name="marginBottom" value="20" />
                    </jsp:include>

                    <%-- FUNCTIONAL BUTTONS --%>
                    <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                        <jsp:param name="submitLabel" value="${submitLabel}" />
                        <jsp:param name="submitMethodName" value="picture_update_validateAndSubmit()" />
                        <jsp:param name="cancelMethodName" value="picture_update_cancel()" />
                    </jsp:include>
                </div><%-- END formID --%>

            </div><%-- END body-form-embedded-content --%>
        </div><%-- END body-form-embedded-container --%>
    </div><%-- END pictureUpdateLayoutID --%>
<% } %>      


<%-- END fragment_picture_update_form.jsp --%>