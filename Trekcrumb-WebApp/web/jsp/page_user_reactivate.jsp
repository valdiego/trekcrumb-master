<%@ include file="include.jsp" %>

<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.enums.DateFormatEnum" %>
<%@ page import="com.trekcrumb.common.utility.DateUtil" %>
<%
    User userDeactivated = (User) request.getAttribute("userReactivateFormBean");
    String userUpdatedDate = null;
    if(userDeactivated != null) {
       userUpdatedDate = DateUtil.formatDate(userDeactivated.getUpdated(), 
                                             DateFormatEnum.GMT_STANDARD_LONG, 
                                             DateFormatEnum.LOCALE_UI_WITH_TIME);
    }
    
%>

<body>
    <%-- PAGE TITLE --%>
    <div class="container-title">
        <div class="font font-style-title">
            <span><jstl-fmt:message key="label_user_reactivate" bundle="${msgprop}"/></span>
        </div>
    </div>

    <%-- MAIN BODY --%>
    <div class="container-body">
        <div class=" body-inside-border body-form font font-size-normal" 
             style="padding-left:10px; padding-right:10px;">
            <spring-form:form id="userReactivateFormID"
                              method="post" 
                              commandName="userReactivateFormBean"
                              action="toBeSetByJavascriptUponSubmit" >
                <%-- HIDDEN INPUTS:
                     Somehow it is required to have non-entries data (such as UserId/Username)
                     as an input as well, otherwise upon submit, their values would be NULL.
                     Therefore, we include them as a hidden input. --%>
                <spring-form:input type="hidden" path="userId" />
                
                <span>
                    <jstl-fmt:message key="info_user_reactivate_greeting_1" bundle="${msgprop}">
<%                     if(userDeactivated != null) { %>                    
                           <jstl-fmt:param value="<%=userDeactivated.getUsername()%>"/>
                           <jstl-fmt:param value="<%=userUpdatedDate%>"/>
<%                     } %>                    
                    </jstl-fmt:message>
                </span>
                <p></p>
                <span>
                    <jstl-fmt:message key="info_user_reactivate_greeting_2" bundle="${msgprop}"/>
                </span>
            </spring-form:form>
            
            <%-- FUNCTIONAL BUTTONS --%>
            <p/>
            <div class="body-position-center">
                <input id="userReactivateSubmitBtnID" 
                       type="submit" 
                       class="button-active"
                       style="width:100%"
                       name="submit"
                       tabindex="5"
                       value="<jstl-fmt:message key="label_user_my_reactivate"/>"
                       onclick="user_reactivate_validateAndSubmit()">
            </div>                
        </div>
        
        <%@ include file="widget_disableBackground.jsp" %>
    </div><%--END of userReactivateLayoutID --%>
    
    <script>
        if(!mDevice_isSupportCookie) {
            common_errorOrInfoMessage_show([ERROR_MSG_CLIENT_COOKIE_NOT_SUPPORTED], null); 
            menu_disable(document.getElementById('userReactivateSubmitBtnID'));
        }
    </script>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_user_reactivate.jsp --%>