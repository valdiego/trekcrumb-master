<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.bean.UserSession" %>
<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<%@ page import="com.trekcrumb.common.enums.TripPrivacyEnum" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    User userLogin_forTripCreate = null;
    if(WebAppUtil.isUserSessionActive(request)) {
        userLogin_forTripCreate = ((UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser();
    }
%>
<%  if(userLogin_forTripCreate != null) { %>
        <div id="tripCreateLayoutID" class="body-disabled-background"
             style="display:none;">
            <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                    with a shade, making the previous main content un-operable (all clickables 
                    become non functional).
                 2. It has a form. 
                 3. By default out of server, the form is EMPTY. JS functions will populate them.
            --%>
            <div id="FRAGMENT_TRIP_CREATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
                <div class="body-form-embedded-content" >
                    <%-- SUB-TITLE AND GREETING --%>
                    <jstl-fmt:message key="label_trip_create" var="labelText" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuClose.jsp" >
                        <jsp:param name="id" value="FRAGMENT_TRIP_CREATE" />
                        <jsp:param name="labelText" value="${labelText}" />
                        <jsp:param name="closeMethodName" value="trip_create_cancel()" />
                    </jsp:include>
                    <div class="border-bottom-black" style="margin-bottom:20px; "></div>

                    <%-- PROGRESS WIDGET --%>
                    <jsp:include page="widget_progressBar.jsp" >
                        <jsp:param name="id" value="FRAGMENT_TRIP_CREATE" />
                        <jsp:param name="color" value="black" />
                        <jsp:param name="size" value="small" />
                        <jsp:param name="marginTop" value="20" />
                    </jsp:include>

                    <%-- FORM --%>
                    <div id="FRAGMENT_TRIP_CREATEformID"
                         style="display:none;">
                        <input id="tripCreateUserID" type="hidden" value="<%=userLogin_forTripCreate.getUserId() %>" />
                        <input id="tripCreateUsernameID" type="hidden" value="<%=userLogin_forTripCreate.getUsername() %>" />

                        <%-- TRIP NAME --%>
                        <div>
                            <span class="font-bold"><jstl-fmt:message key="label_trip_name" bundle="${msgprop}"/></span><br/>
                            <textarea id="tripCreateTripNameInputID" 
                                      maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME %>" 
                                      cols="50" rows="1" ></textarea>
                        </div>

                        <%-- CURRENT LOCATION MAP:
                             1. It has two columns: One position at left (regular), the other at right. Our previous
                                approach was to use "position=absolute" with "right=0px" e.g., CSS .body-position-right class.
                                But it was problematic for responsive layout where position absolute tends to fall out of space.
                             2. The new approach is simply to use two <DIV> sitting one to another ("display=inline-block") 
                                with fixed "width=49%" for each of them. Then set "text-align=right" for the second column.
                             3. Since the refresh column is clickable, we need to avoid the entire 49% width area to be as such,
                                but just enough width with its contents is clickable. To accomplish this, we must setup
                                another <DIV> inside it with its "display=inline-block".
                        --%>
                        <div style="margin-top:20px;">
                            <div class="font-bold">
                                <%-- 1ST COLUMN ON LEFT --%>
                                <div style="width:49%; text-align:left; vertical-align:bottom; display:inline-block;">
                                    <span><jstl-fmt:message key="label_place_locationCurrent" bundle="${msgprop}"/></span>
                                </div>

                                <%-- 2ND COLUMN ON RIGHT AND CLICKABLE --%>
                                <div style="width:49%; text-align:right; display:inline-block;">
                                    <div class="body-clickable"
                                         style="display:inline-block;"
                                         onmousedown="menu_Clicked(this)" 
                                         onmouseup="menu_ClickedReset(this)" 
                                         onclick="trip_create_refresh()">
                                        <div class="menu-icon"
                                             style="display:inline-block; vertical-align:middle;">
                                            <span class="fa fa-refresh"></span>
                                        </div>
                                        <span class="font-bold" style="text-decoration:underline;"><jstl-fmt:message key="label_common_refresh" bundle="${msgprop}"/></span>
                                    </div>
                                </div>
                            </div>
                            
                            <%@ include file="fragment_map_static.jsp" %>
                        </div>

                        <%-- STATUS --%>
                        <div style="margin-top:10px; ">
                            <div style="width:30%; vertical-align:top; display:inline-block;">
                                <span class="font-bold"><jstl-fmt:message key="label_status" bundle="${msgprop}"/></span>
                            </div>
                            <div style="display:inline-block;">
                                <input id="FRAGMENT_TRIP_CREATEtripStatusActiveInputID" 
                                       type="radio" 
                                       name="status" 
                                       value="<%=TripStatusEnum.ACTIVE.getValue() %>" 
                                       checked="true" />
                                <label for="FRAGMENT_TRIP_CREATEtripStatusActiveInputID"><jstl-fmt:message key="label_status_active" bundle="${msgprop}"/></label>
                            </div>
                        </div>

                        <%-- PRIVACY WIDGET --%>
                        <jstl-fmt:message key="label_privacy_select" var="labelTitle" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="label_privacy_public" var="labelPublic" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="label_privacy_private" var="labelPrivate" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="info_trip_public_warning" var="privacyPublicMessage" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="info_trip_private_warning" var="privacyPrivateMessage" bundle="${msgprop}"/>  
                        <jsp:include page="widget_menuTripPrivacy.jsp" >
                            <jsp:param name="id" value="FRAGMENT_TRIP_CREATE" />
                            <jsp:param name="labelTitle" value="${labelTitle}" />
                            <jsp:param name="labelPublic" value="${labelPublic}" />
                            <jsp:param name="labelPrivate" value="${labelPrivate}" />
                            <jsp:param name="privacyPublicMessage" value="${privacyPublicMessage}" />
                            <jsp:param name="privacyPrivateMessage" value="${privacyPrivateMessage}" />
                            <jsp:param name="privacyValueCurrent" value="<%=TripPrivacyEnum.PUBLIC.getValue() %>" />
                        </jsp:include>

                        <%-- TRIP NOTE --%>
                        <div style="margin-top:20px; margin-bottom:20px;">
                            <span class="font-bold"><jstl-fmt:message key="label_common_notes" bundle="${msgprop}"/></span><br/>
                            <textarea id="tripCreateTripNoteInputID" 
                                      maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NOTE %>" 
                                      rows="4" cols="50"></textarea>                  
                        </div>

                        <%-- FUNCTIONAL BUTTONS --%>
                        <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>  
                        <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                            <jsp:param name="submitLabel" value="${submitLabel}" />
                            <jsp:param name="submitMethodName" value="trip_create_validateAndSubmit()" />
                            <jsp:param name="cancelMethodName" value="trip_create_cancel()" />
                        </jsp:include>
                    </div><%-- END formID --%>

                </div><%-- END body-form-embedded-content --%>
            </div><%-- END body-form-embedded-container --%>
        </div><%-- END tripCreateLayoutID --%>
<%  } %>      

<%-- END fragment_trip_create_form.jsp --%>