<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>

<div id="userContactUsLayoutID" class="body-form-embedded-container font font-size-normal">
    <div class="body-form-embedded-content" >
        <%-- SUB-TITLE AND GREETING --%>
        <div>
            <span class="font-size-large font-bold"><jstl-fmt:message key="label_support_contactUs" bundle="${msgprop}"/></span>
            <br/>
            <span><jstl-fmt:message key="info_support_contactUs_greeting" bundle="${msgprop}"/></span>
        </div>
        <div class="border-bottom-black" style="margin-bottom:20px; "></div>

        <%-- FORM --%>
        <spring-form:form id="userContactUsFormID"
                          method="post" 
                          commandName="userContactUsFormBean"
                          action="" >
            <table class="body-form-input-field"
                   style="width:100%;">
                <%-- EMAIL --%>
                <tr>
                    <td width=34%>
                        <span class="font-bold"><jstl-fmt:message key="label_user_email" bundle="${msgprop}"/></span>
                    </td>
                    <td width=66%>
                        <spring-form:input path="email"
                                           id="userContactUsEmailInputID"
                                           type="email"
                                           maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL)%>" />
                    </td>
                </tr>
                <%-- FULLNAME --%>
                <tr>
                    <td width=34%>
                        <span><jstl-fmt:message key="label_user_fullname" bundle="${msgprop}"/></span>
                    </td>
                    <td width=66%>
                        <spring-form:input path="fullname"
                                           id="userContactUsFullnameInputID"
                                           type="text"
                                           maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME) %>" />
                    </td>
                </tr>
            </table>
            
            <%-- SUMMARY --%>
            <div class="body-form-input-field"
                 style="margin-top:20px;">
                <span class="font-bold">Your Message</span>
                <br/>
                <spring-form:textarea 
                    path="summary"
                    id="userContactUsSummaryInputID"
                    cols="50" rows="4"
                    maxlength="1000" />
            </div>
        </spring-form:form>

        <%-- FUNCTIONAL BUTTONS --%>
        <jstl-fmt:message key="label_common_submit" var="submitLabel" bundle="${msgprop}"/>
        <jsp:include page="widget_menuCancelAndSubmit.jsp" >
            <jsp:param name="submitButtonIDPrefix" value="FRAGMENT_USER_CONTACT_US" />
            <jsp:param name="submitLabel" value="${submitLabel}" />
            <jsp:param name="submitMethodName" value="user_contactUs_validateAndSubmit()" />
            <jsp:param name="isCancelHide" value="true" />
        </jsp:include>

    </div>
</div><%--END of userContactUsLayoutID --%>
<script>
    document.getElementById('userContactUsFormID').setAttribute("action", WEBAPP_URL_USER_CONTACTUS_SUBMIT);
</script>


<%-- END fragment_user_contactUs_form.jsp --%>