<%@ include file="include.jsp" %>

<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import="com.trekcrumb.common.bean.ServiceResponse" %>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.bean.UserAuthToken" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%
    User user_forUserView = null;
    boolean isUserLoginProfile_forUserView = false;
    UserAuthToken userAuthToken = null;
    ServiceResponse serviceResponse = 
            (ServiceResponse) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE);
    String serviceResponseJSON = null;
    if(serviceResponse != null) {
        ObjectMapper jacksonObjMapper = new ObjectMapper();
        serviceResponseJSON = jacksonObjMapper.writeValueAsString(serviceResponse);
        
        if(serviceResponse.isSuccess() 
                && serviceResponse.getListOfUsers() != null
                && serviceResponse.getListOfUsers().get(0) != null) {
            user_forUserView = serviceResponse.getListOfUsers().get(0);
            request.setAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED, user_forUserView);
            
            //Check if this is UserLogin profile
            isUserLoginProfile_forUserView = WebAppUtil.isUserLoginProfile(request, user_forUserView.getUserId());
            request.setAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, isUserLoginProfile_forUserView);
            
            //Check for user auth token, if any:
            if(isUserLoginProfile_forUserView && user_forUserView.getUserAuthToken() != null) {
                userAuthToken = user_forUserView.getUserAuthToken();
            }
        }
    }
%>

<body>
    <%-- PAGE TITLE --%>
    <div class="container-title">
<%      if(user_forUserView != null) { %>
            <div id="userDetailTitleID" class="body-clickable font font-style-title"
                 style="display:inline-block; vertical-align:middle;"
                 onclick="user_view_DisplaySelectedTab(FRAGMENT_USER_DETAIL)">
                <span><%=user_forUserView.getFullname() %></span>
            </div>
            <div id="userTripsTitleID" class="font font-style-title-sub"
                 style="display:none; vertical-align:middle;"> 
                <span style="margin-left:5px; margin-right:5px;">&gt;&gt;</span>
                <span><jstl-fmt:message key="label_trip_list" bundle="${msgprop}"/></span>
            </div>
            <div id="userPicturesTitleID" class="font font-style-title-sub"
                 style="display:none; vertical-align:middle;"> 
                <span style="margin-left:5px; margin-right:5px;">&gt;&gt;</span>
                <span><jstl-fmt:message key="label_picture" bundle="${msgprop}"/></span>
            </div>
            <div id="userFavoritesTitleID" class="font font-style-title-sub"
                 style="display:none; vertical-align:middle;"> 
                <span style="margin-left:5px; margin-right:5px;">&gt;&gt;</span>
                <span><jstl-fmt:message key="label_favorite" bundle="${msgprop}"/></span>
            </div>
            <div id="userCommentsTitleLayoutID" class="font font-style-title-sub"
                 style="display:none; vertical-align:middle;"> 
                <span style="margin-left:5px; margin-right:5px;">&gt;&gt;</span>
                <span><jstl-fmt:message key="label_comment" bundle="${msgprop}"/></span>
            </div>

<%      } else { %>
            <div id="userDetailTitleID" class="font font-style-title"
                 style="display:table-cell; vertical-align:middle;">
                <span><jstl-fmt:message key="label_user_profile" bundle="${msgprop}"/></span>
            </div>
<%      } %>             
    </div>

    <%-- MAIN BODY --%>
    <div id="userViewLayoutID" class="container-body"
         style="overflow-y:hidden;"
         var_user_view_fragmentOnDisplay = "" >
<%      if(serviceResponse != null && serviceResponse.isSuccess()) { 
            if(user_forUserView == null) { %>
                <span class="font font-size-normal font-italic"
                      style="padding:10px;"><jstl-fmt:message key="error_user_profile_empty" bundle="${msgprop}"/></span>

<%          } else { %>            
                <%@ include file="fragment_user_detail.jsp" %>
                <%@ include file="fragment_user_trips.jsp" %>
                <%@ include file="fragment_user_pictures.jsp" %>
                <%@ include file="fragment_user_favorites.jsp" %>
                <%@ include file="fragment_user_comments.jsp" %>
<%          } 
        } %>
    </div>
    
    <%-- EMBEDDED FORMS
         1. Forms have "body-disabled-background" CSS style that must cover entire screen. 
            To work and overlay the Menu-Main and Title-Main, they MUST BE ON <BODY> level.
         2. The hidden input variables are REQUIRED for use by forms.
    --%>
<%  if(isUserLoginProfile_forUserView) { %>
        <input id="userID" type="hidden" value="<%=user_forUserView.getUserId()%>" />
        <input id="usernameID" type="hidden" value="<%=user_forUserView.getUsername()%>" />
        <%@ include file="fragment_user_loginRefresh_form.jsp" %>
        <%@ include file="fragment_user_update_form.jsp" %>
        <%@ include file="fragment_comment_delete_form.jsp" %>
<%  } %>
    
    <%-- DISABLED BACKGROUND
         1. Layout hase "body-disabled-background" CSS style that must cover entire screen. 
            To work and overlay the Menu-Main and Title-Main, they MUST BE ON <BODY> level.
         2. Used when updating servers without formal forms such as 'adding other user as a friend",
            "deleting user login's setting",  etc. 
    --%>
    <%@ include file="widget_disableBackground.jsp" %>
    
    <script>
<%      if(user_forUserView != null) { %>         
            //1. Upon page-loading completed:
            user_view_DisplayContent(<%=serviceResponseJSON%>);

            /* 
             * 2. On resize:
             *    -> Do NOT put on <body onresize=""> tag anymore because it could be executed
             *       before the page completes resizing, causing getting invalid element's 
             *       values/states (offsetWidth, offsetHeight, etc) which were previous values.
             *    -> Even in here, we MUST put a 'waiting period' to make sure client browser has 
             *       completely configure all DOM elements during resize for the same reason. 
             *    -> Operations: Determine which fragment to display and their responsiveness.
             */
            window.addEventListener("resize", function() {
                setTimeout(function() {
                    user_view_DisplaySelectedTab(null);
                }, (1000));
            });
            
            //3. Cookies and userAuthToken
<%          if(userAuthToken != null) { %>
                userauthtoken_cookie_create('<%=userAuthToken.getId()%>', '<%=userAuthToken.getUserId()%>', '<%=userAuthToken.getAuthToken()%>'); 
<%          } %>
            
<%      } else { %>
            <%-- USER OBJECT IS NULL AND POSSIBLY ERROR FROM SERVER --%>
            user_view_DisplayContent(<%=serviceResponseJSON%>);
<%      } %>            
    </script>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_user_view.jsp --%>