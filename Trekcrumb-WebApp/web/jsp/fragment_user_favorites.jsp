<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%-- Layout to display user's list of favorites.
     1. On page (document) load: By default this fragment is hidden and empty. Upon some specific
        events (user click menu, etc.), then it triggers call JS functions to
        retrieve the data and images. 
        See: user_view_DisplaySelectedTab()
     2. Any pagination and MenuPage sections should be AFTER main layout (image, etc.) such that on narrow
        view, they will be on-top of the main layout.
 --%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    User user_forUserFavorites = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
    boolean isUserLoginProfile_forUserFavorites = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<%  if(user_forUserFavorites != null) { %>  
        <div id="userFavoritesLayoutID" 
             class="layout body-halfOrFullWidth body-halfOrFullWidth-secondary body-scroll menu-sliding"
             style="height:100%;"
             var_user_username="<%=user_forUserFavorites.getUsername()%>"
             var_user_isUserLoginProfile="<%=isUserLoginProfile_forUserFavorites%>" >
             
            <%-- SUB-TITLE --%>
            <div class="body-position-center font font-size-normal font-bold"
                 style="margin-top:40px;">
                <%=user_forUserFavorites.getFullname()%>&nbsp;<jstl-fmt:message key="label_favorite" bundle="${msgprop}"/> 
            </div>

            <%-- PROGRESS WIDGET --%>
            <jsp:include page="widget_progressBar.jsp" >
                <jsp:param name="id" value="FRAGMENT_TRIP_LIST_USER_FAVORITES" />
                <jsp:param name="color" value="black" />
                <jsp:param name="size" value="small" />
                <jsp:param name="position" value="center-layout" />
                <jsp:param name="marginTop" value="60" />
            </jsp:include>

            <%-- TRIP LIST SECTION --%>
            <jsp:include page="fragment_trip_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_LIST_USER_FAVORITES" />
                <jsp:param name="tripRowStyleCSS" value="layout-triplist-row" />
            </jsp:include>
            
            <%-- PAGINATION WIDGET --%>
            <jsp:include page="widget_pagination_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_LIST_USER_FAVORITES" />
                <jsp:param name="menuGoPreviousMethodName" value="favorite_retrieveByUser_previous()" />
                <jsp:param name="menuGoNextMethodName" value="favorite_retrieveByUser_next()" />
                <jsp:param name="numberOfRecordsTotal" value="<%=user_forUserFavorites.getNumOfFavorites() %>" />
            </jsp:include>
        </div>
<%  } %>    

<%-- END fragment_user_favorites.jsp --%>