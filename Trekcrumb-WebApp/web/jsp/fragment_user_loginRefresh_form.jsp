<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.bean.UserSession" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    User userLogin_forUserRefresh = null;
    if(WebAppUtil.isUserSessionActive(request)) {
        userLogin_forUserRefresh = ((UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser();
    }
%>
<% if(userLogin_forUserRefresh != null) { %>
    <div id="userLoginRefreshLayoutID" class="body-disabled-background" 
         style="display:none;">
        <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                with a shade, making the previous main content un-operable (all clickables 
                become non functional). 
             2. It has a form.  
        --%>
        <div id="FRAGMENT_USER_LOGIN_REFRESHcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
            <div class="body-form-embedded-content" >
                <%-- SUB-TITLE AND GREETING --%>
                <jstl-fmt:message key="label_user_my_profile_refresh" var="labelText" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuClose.jsp" >
                    <jsp:param name="id" value="FRAGMENT_USER_LOGIN_REFRESH" />
                    <jsp:param name="labelText" value="${labelText}" />
                    <jsp:param name="closeMethodName" value="user_login_refresh_cancel()" />
                </jsp:include>
                <div class="border-bottom-black" style="margin-bottom:20px; "></div>
                <span><jstl-fmt:message key="info_user_profileRefresh_greeting" bundle="${msgprop}"/></span>

                <%-- PROGRESS WIDGET --%>
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_USER_LOGIN_REFRESH" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="marginTop" value="20" />
                </jsp:include>

                <%-- FORM --%>
                <div id="FRAGMENT_USER_LOGIN_REFRESHformID">
                    <spring-form:form id="userLoginRefreshFormID"
                                      method="post" 
                                      commandName="userLoginRefreshForm"
                                      action="${pageContext.request.contextPath}/trekcrumb/user/refresh/submit/form" >
                        <table style="width: 100%;">
                            <%-- USERID --%>
                            <spring-form:input path="userId"
                                               type="hidden"
                                               value="<%=userLogin_forUserRefresh.getUserId()%>" />
                            
                            <%-- USERNAME --%>
                            <tr>
                                <td width=34%>
                                    <span class="font-bold"><jstl-fmt:message key="label_user_username" bundle="${msgprop}"/></span>
                                </td>
                                <td width=66%>
                                    <span><%=userLogin_forUserRefresh.getUsername()%></span>
                                    <spring-form:input path="username"
                                                       id="usernameInputID"
                                                       type="hidden"
                                                       value="<%=userLogin_forUserRefresh.getUsername()%>"
                                                       readonly="true" />
                                </td>
                            </tr>
                            
                            <%-- PASSWORD --%>
                            <tr>
                                <td width=34%>
                                    <span class="font-bold"><jstl-fmt:message key="label_user_password" bundle="${msgprop}"/></span>
                                </td>
                                <td width=66%>
                                    <jstl-fmt:message key="info_rule_caseSensitive" var="pwdPlaceholder" bundle="${msgprop}"/>  
                                    <spring-form:input path="password"
                                                       id="userLoginRefreshPwdInputID"
                                                       type="password"
                                                       maxlength="25"
                                                       style="font-size:16px;"
                                                       placeholder="${pwdPlaceholder}" />
                                </td>
                            </tr>
                        </table>
                    </spring-form:form>

                    <%-- FUNCTIONAL BUTTONS --%>
                    <p/>
                    <jstl-fmt:message key="label_common_refresh" var="submitLabel" bundle="${msgprop}"/>
                    <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                        <jsp:param name="submitLabel" value="${submitLabel}" />
                        <jsp:param name="submitMethodName" value="user_login_refresh_validateAndSubmit()" />
                        <jsp:param name="cancelMethodName" value="user_login_refresh_cancel()" />
                    </jsp:include>
                </div><%-- END hideable formID --%>
            
            </div><%-- END body-form-embedded-content --%>
        </div><%-- END body-form-embedded-container --%>
    </div>
<% } %>

<%-- END fragment_user_loginRefresh_form.jsp --%>