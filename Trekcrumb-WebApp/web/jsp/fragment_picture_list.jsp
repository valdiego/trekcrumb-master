<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%
    /*
     * Custom unique ID, if provided, will function as prefix to all elements in this widget.
     * This input is typically the name of page ('pageName') that embeds this widget. 
     */
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }
    
    boolean isDisplayUser = Boolean.parseBoolean(request.getParameter("isDisplayUser"));
%>

<div id="<%=widgetPrefixID%>pictureListLayoutID" class="layout-picturelist body-position-center"
     style="display:none;">

    <%-- ROW TEMPLATE WIDGET --%> 
    <jsp:include page="widget_row_picture.jsp" >
        <jsp:param name="isDisplayUser" value="<%=isDisplayUser%>" />
    </jsp:include>

    <%-- LIST CONTENT --%>
    <div id="<%=widgetPrefixID%>pictureListContentID" style="margin-left:auto; margin-right: auto;">
        <%-- NOTE:
             This is the content where it could be either empty message, or the list of rows
             copy-pasted from the widget and populated. --%>
    </div>
</div>

<%-- END of fragment_picture_list.jsp --%>