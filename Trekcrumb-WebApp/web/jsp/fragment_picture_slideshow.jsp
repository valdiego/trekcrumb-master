<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPicSlideshow = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forPicSlideshow = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<% if(trip_forPicSlideshow != null && trip_forPicSlideshow.getNumOfPictures() > 0) { %>
    <div id="pictureSlideshowLayoutID" class="body-position-center" >

        <%-- IMAGE CONTENT --%>
        <div id="pictureSlideshowImgContentContainerID">
             <div id="pictureSlideshowImgContentHeightAsFunctionOfWidthID"></div>
             <div id="pictureSlideshowImgContentID">
                <%-- PROGRESS WIDGET --%>
                <jstl-fmt:message key="info_picture_imageLoading" var="progressBarText" bundle="${msgprop}"/>  
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_SLIDESHOW" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="position" value="center-layout" />
                    <jsp:param name="marginTop" value="60" />
                    <jsp:param name="labelText" value="${progressBarText}" />
                </jsp:include>

                <%-- IMAGE ERROR WIDGET --%>
                <jsp:include page="widget_imageError.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_SLIDESHOW" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="75" />
                    <jsp:param name="position" value="center-layout" />
                    <jsp:param name="marginTop" value="60" />
                </jsp:include>                  
                  
                <%-- IMAGE --%>
                <img id="FRAGMENT_PICTURE_SLIDESHOWpictureImageID<%=trip_forPicSlideshow.getId()%>" class="image"
                     style="margin:auto; cursor:zoom-in; display:none;" 
                     onclick="trip_view_DisplaySelectedTab(FRAGMENT_PICTURE_DETAIL)" />
                  
                <%-- PAGINATION: PREVIOUS/NEXT MENU --%>
                <jsp:include page="widget_menuPreviousAndNext.jsp" >
                    <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_SLIDESHOW" />
                    <jsp:param name="menuPreviousMethodName" value="picture_slideshow_Previous()" />
                    <jsp:param name="menuNextMethodName" value="picture_slideshow_Next()" />
                </jsp:include>
             </div>
        </div><%-- END of pictureSlideshowImgContentContainerID --%>
        
        <%-- DETAIL CONTENTS --%>
        <div id="pictureSlideshowDetailID" class="font font-size-normal">
            <table width=100%>
                <tr>
                    <td>
                        <span id="FRAGMENT_PICTURE_SLIDESHOWPageCountID" class="font-bold"></span><br/>
                        <span id="FRAGMENT_PICTURE_SLIDESHOWpictureNoteID" class="font-style-note"></span>
                    </td>
<%                  if(isUserHasAccessToTrip_forPicSlideshow) { %>
                        <%-- Note:
                             The menu-icon image is 48x48px, reserve the TD element with extra 5px. 
                             Then position the content at top-right corner of the TD element.
                        --%>
                        <td width="53px"
                            style="text-align:right; vertical-align:top; padding:0px;">
                            <div id="PictureSlideshowMenuPictureEditID" 
                                 class="hoverhints-menuIcon menu-icon font-color-menu"
                                 style="display:inline-block;" 
                                 onmouseover="menu_hover_LabelShow(this)"
                                 onmouseout="menu_hover_LabelHide(this)"
                                 onmousedown="menu_Clicked(this)"
                                 onmouseup="menu_ClickedReset(this)"
                                 onclick="picture_update_Init()" >
                                <span class="fa fa-pencil" ></span>
                                <div id="PictureSlideshowMenuPictureEditIDHover" 
                                      class="hoverhints-menuIcon-text-container-rightTop" 
                                      style="display: none;">
                                    <span class="hoverhints-menuIcon-text-body-rightTop"><jstl-fmt:message key="label_picture_edit" bundle="${msgprop}"/></span>
                                </div>
                            </div>
                        </td>
<%                  } %>
                </tr>
            </table>
        
        </div><%-- END of pictureSlideshowDetailID --%>
    </div><%-- END pictureSlideshowLayoutID--%>
    
    <script>
        picture_generic_detailRetrieve(FRAGMENT_PICTURE_SLIDESHOW, null, "pictureSlideshowLayoutID");
        pagePic_updateNavigationMenu(FRAGMENT_PICTURE_SLIDESHOW, null, null);
    </script>
    
<% } %><%-- END if trip NOT empty and pictures NOT empty --%>


<%-- END fragment_picture_slideshow.jsp --%>