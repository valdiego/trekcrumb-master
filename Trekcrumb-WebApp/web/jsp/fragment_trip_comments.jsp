<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.Comment" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forComments = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
%>
<div id="tripCommentLayoutID" class="font" 
     style="margin:0px 10px 0px 10px;"
     var_commentForTrip_numRecordsTotal = "<%=trip_forComments.getNumOfComments()%>"
     var_commentForTrip_numOfRowsMax = "<%=CommonConstants.RECORDS_NUMBER_MAX%>"
     var_commentForTrip_offsetCurrent = "0" >

    <%-- HEADER AND SHOW/HIDE MENUS --%>
    <div id="headercommentListContentID" class="body-clickable font-size-normal"
         onclick="comment_trip_init()">
        <span id="menuShowcommentListContentID" class="font-size-large fa fa-plus-square-o" 
              style="vertical-align:middle; margin-right:10px;"></span>
        <span id="menuHidecommentListContentID" class="font-size-large fa fa-minus-square-o" 
              style="vertical-align:middle; margin-right:10px; display:none;"></span>
        <span class="font-bold"><jstl-fmt:message key="label_comment" bundle="${msgprop}"/></span>
        <span id="tripCommentHeaderNumRecordsTotalID" class="font-bold" style="margin-left:10px;"><%=trip_forComments.getNumOfComments()%></span>
    </div>
    
    <%-- CONTENTS:
         -> This layout will be wiped out and populated by rows --%>
    <div id="commentListContentID" style="display:none;"></div>
    
    <%-- PROGRESS WIDGET --%>
    <jsp:include page="widget_progressBar.jsp" >
        <jsp:param name="id" value="FRAGMENT_COMMENT_LIST_TRIP_COMMENTS" />
        <jsp:param name="color" value="black" />
        <jsp:param name="size" value="small" />
        <jsp:param name="marginTop" value="5" />
        <jsp:param name="labelText" value="Loading More Comments ..." />
    </jsp:include>
    
    <%-- MENU MORE --%>
    <jsp:include page="widget_menuMoreNext.jsp" >
        <jsp:param name="widgetPrefixID" value="commentByTrip" />
        <jsp:param name="menuNextMethodName" value="comment_trip_next()" />
        <jsp:param name="isHideByDefault" value="true" />
    </jsp:include>
    
    <%-- COMMENT LIST ROW TEMPLATE WIDGET --%> 
    <jsp:include page="widget_row_comment_byTrip.jsp" />
    
</div>
<script>
    common_progressBarWidget_hide(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS);
    
    /*
     * If returned Trip comes with handy List of Comments, let's display them and we would save
     * one user request to retrieve them from back-end service.
     * Note: This should apply for offset = 0.
     */
<%  if(trip_forComments.getNumOfComments() > 0 && trip_forComments.getListOfComments() != null) { 
        ObjectMapper jacksonObjMapper = new ObjectMapper();
        String listOfCommentsJSON = jacksonObjMapper.writeValueAsString(trip_forComments.getListOfComments()); %>
        
        comment_generic_displayRows(FRAGMENT_COMMENT_LIST_TRIP_COMMENTS, <%=listOfCommentsJSON%>, false);
        comment_trip_paginationUpdate(true);
<%  } %>

</script>
        
<%-- END fragment_trip_comments.jsp --%>