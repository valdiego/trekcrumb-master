<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<%-- METADATA SEARCH ENGINE
    1) Define metadata "robot" to enable search engine to index/follow the page
    2) Google search
        -> For homepage, add Corporate info (logo, URL, etc.) using Google's schema.org vocabulary 
           and JSON-LD Markup OR Microdata format
        -> To support Microdata format, each JSP fragment must have embedded markup embedded on 
           particular elements inside the page (See: fragment_trip_detail.jsp)
        -> "google-site-verification" metadata: In order to enable Google search console, we must
           register our website/domain at Google Search Console. To verify we are the owner of
           the website, include this Google generated verification ID on the header of our website.
        -> The JSON-LD markup format includes search result rich-content and breadcrumbs.
           
    3) Facebook search
        -> Enable FB Crawler to crawl page by providing canonical URL with Open Graph URL syntax
           (preferred) OR link syntax 
        -> URL value must be absolute path, not relative
--%>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppConstants" %>
<%
    String pageIdentityType_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TYPE);
    String pageURL_forSearchEngine = null;
    String pageTitle_forSearchEngine = null;
    String pageSummary_forSearchEngine = null;
    String pageImageURL_forSearchEngine = null;
    String pageAuthor_forSearchEngine = null;
    String pageLocation_forSearchEngine = null;
    if(pageIdentityType_forSearchEngine != null) {
        pageURL_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_URL);
        pageTitle_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_TITLE);
        if(pageTitle_forSearchEngine == null) {
            pageTitle_forSearchEngine = WebAppConstants.APP_TITLE;
        }
        
        pageSummary_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_SUMMARY);
        if(pageSummary_forSearchEngine == null) {
            pageSummary_forSearchEngine = WebAppConstants.APP_SUMMARY;
        }

        pageImageURL_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_IMAGEURL);
        pageAuthor_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_AUTHOR);
        pageLocation_forSearchEngine = (String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_LOCATION);
    }
%>

<%  if(pageIdentityType_forSearchEngine != null) { %>
        <meta name="ROBOTS" content="index, follow" />
        <meta property="og:url" content="<%=pageURL_forSearchEngine %>" />
        <link rel="canonical" href="<%=pageURL_forSearchEngine %>" />
        
        <meta name="google-site-verification" content="GP-JhCaXpew4u7uOF5Ugze07bUcEcn1XH1lz4oRRNrs" />

<%-- HOMEPAGE --%>
<%      if(pageIdentityType_forSearchEngine.equalsIgnoreCase(WebAppConstants.PAGE_IDENTITY_TYPE_HOME)) { %>
            <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "Organization",
                    "url": "<%=pageURL_forSearchEngine %>",
                    "name":  "Trekcrumb.com",
                    "legalName": "Trekcrumb",
                    "logo": "<%=pageImageURL_forSearchEngine %>",
                    "description": "<%=pageSummary_forSearchEngine%>"
                }
            </script>    

<%-- USER PROFILE --%>
<%      } else if(pageIdentityType_forSearchEngine.equalsIgnoreCase(WebAppConstants.PAGE_IDENTITY_TYPE_USER_PROFILE)) { %>
            <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "Person",
                    "mainEntityOfPage":{
                        "@type": "WebPage",
                        "@id": "<%=pageURL_forSearchEngine %>"
                    },
                    "name": "<%=pageTitle_forSearchEngine %>",
                    "url": "<%=pageURL_forSearchEngine %>",
                    "description": "<%=pageSummary_forSearchEngine%>",
                    "image": {
                        "@type": "ImageObject",
                        "url": "<%=pageImageURL_forSearchEngine %>",
                        "height": 200,
                        "width": 200
                    },
                    "homeLocation": {
                        "@type": "Place",
                        "address": "<%=pageLocation_forSearchEngine %>",
                        "description": "<%=pageLocation_forSearchEngine %>"
                    }
                }
            </script>     
                           
<%-- TRIP DETAIL --%>
<%      } else if(pageIdentityType_forSearchEngine.equalsIgnoreCase(WebAppConstants.PAGE_IDENTITY_TYPE_TRIP_DETAIL)) { %>
            <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "WebPage",
                    "mainEntityOfPage":{
                        "@type": "WebPage",
                        "@id": "<%=pageURL_forSearchEngine %>"
                    },
                    "name": "<%=pageTitle_forSearchEngine%>",
                    "headline": "<%=pageTitle_forSearchEngine%>",
                    "datePublished": "<%=(String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_CREATED_DATE) %>",
                    "dateModified": "<%=(String) request.getAttribute(WebAppConstants.PAGE_IDENTITY_CREATED_DATE) %>",
                    "description": "<%=pageSummary_forSearchEngine%>",
                    "image": {
                        "@type": "ImageObject",
                        "url": "<%=pageImageURL_forSearchEngine %>",
                        "height": 600,
                        "width": 600
                    },
                    "author": {
                        "@type": "Person",
                        "name": "<%=pageAuthor_forSearchEngine %> at Trekcrumb.com"
                    },
                    "publisher": {
                        "@type": "Organization",
                        "name": "Trekcrumb.com",
                        "logo": {
                            "@type": "ImageObject",
                            "url": "<%=WebAppConstants.APP_DOMAIN_URL %>/<%=WebAppConstants.APP_LOGO %>",
                            "width": 200
                        }
                    }
                    
<%                  if(pageLocation_forSearchEngine != null) { %>
                        , "contentLocation": {
                            "@type": "Place",
                            "address": "<%=pageLocation_forSearchEngine %>",
                            "description": "<%=pageLocation_forSearchEngine %>"
                        }
<%                  } %>

                    , "breadcrumb": {
                        "@type": "BreadcrumbList",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "position": 1,
                                "item": {
                                    "@id": "<%=WebAppConstants.APP_DOMAIN_URL %>",
                                    "name": "Trekcrumb.com"
                                }
                            },
                            {
                                "@type": "ListItem",
                                "position": 2,
                                "item": {
                                    "@id": "<%=WebAppConstants.APP_DOMAIN_URL %>/user/<%=pageAuthor_forSearchEngine %>",
                                    "name": "<%=pageAuthor_forSearchEngine %>"
                                }
                            },
                            {
                                "@type": "ListItem",
                                "position": 3,
                                "item": {
                                    "@id": "<%=pageURL_forSearchEngine%>",
                                    "name": "Trek Detail"
                                }
                            }
                        ]
                    }
                }
            </script>
<%      } 
    } %>

<%-- END include_headerSearchEngine.jsp --%>