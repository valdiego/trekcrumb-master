<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="java.util.List" %>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.Place" %>
<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<%@ page import="com.trekcrumb.common.enums.TripPrivacyEnum" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    Trip tripSelected_forTripUpdate = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forTripUpdate = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);

    String tripName_forTripUpdate = "";
    String tripNote_forTripUpdate = "";            
    if(tripSelected_forTripUpdate != null && isUserHasAccessToTrip_forTripUpdate) {
        tripName_forTripUpdate = WebAppUtil.neutralizeStringValue(tripSelected_forTripUpdate.getName(), false, false, false, -1);
        tripNote_forTripUpdate = WebAppUtil.neutralizeStringValue(tripSelected_forTripUpdate.getNote(), false, false, false, -1);
    }
%>
<% if(tripSelected_forTripUpdate != null && isUserHasAccessToTrip_forTripUpdate) { %>
    <div id="tripUpdateLayoutID" class="body-disabled-background" 
         style="display:none;">
        <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                with a shade, making the previous main content un-operable (all clickables 
                become non functional).
             2. It has a form. By default out of server, the form is EMPTY. JS will populate them.
             3. It may require some "hidden" input variables that are shared among other embedded forms,
                and expect these variables to be set in its parent JSP. 
        --%>
        <div id="FRAGMENT_TRIP_UPDATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
            <div class="body-form-embedded-content" >
                <%-- SUB-TITLE AND GREETING --%>
                <jstl-fmt:message key="label_trip_edit" var="labelText" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuClose.jsp" >
                    <jsp:param name="id" value="FRAGMENT_TRIP_UPDATE" />
                    <jsp:param name="labelText" value="${labelText}" />
                    <jsp:param name="closeMethodName" value="trip_update_cancel()" />
                </jsp:include>
                <div class="border-bottom-black" style="margin-bottom:20px; "></div>

                <%-- PROGRESS WIDGET --%>
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_TRIP_UPDATE" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="marginTop" value="20" />
                </jsp:include>

                <%-- FORM --%>
                <div id="FRAGMENT_TRIP_UPDATEformID">
                    <%-- TRIP NAME --%>
                    <div>
                        <span class="font-bold"><jstl-fmt:message key="label_trip_name" bundle="${msgprop}"/></span><br/>
                        <input id="tripUpdateTripNameOrigID" type="hidden" value="<%=tripName_forTripUpdate%>" />
                        <textarea id="tripUpdateTripNameInputID" 
                                  maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME %>" 
                                  cols="50" rows="1" ></textarea>
                    </div>

                    <%-- EDITABLE SECTION
                         NOTE: Show/hide when delete checkbox is selected. --%>    
                    <div id="FRAGMENT_TRIP_UPDATEeditableSectionID">

                        <%-- STATUS --%>
                        <div style="margin-top:10px; ">
                            <div style="width:30%; vertical-align:top; display:inline-block;">
                                <span class="font-bold"><jstl-fmt:message key="label_status" bundle="${msgprop}"/></span>
                            </div>
                            <div style="vertical-align:top; display:inline-block;">
<%                            if(tripSelected_forTripUpdate.getStatus() == TripStatusEnum.ACTIVE) { %>
                                    <input id="FRAGMENT_TRIP_UPDATEtripStatusActiveInputID"
                                           type="radio"
                                           name="status"
                                           value="<%=TripStatusEnum.ACTIVE.getValue()%>"
                                           checked="true"
                                           onchange="trip_generic_status_displayWarning(FRAGMENT_TRIP_UPDATE)" />
                                    <label for="FRAGMENT_TRIP_UPDATEtripStatusActiveInputID"><jstl-fmt:message key="label_status_active" bundle="${msgprop}"/></label>
                                    <br/>
<%                            } %>                    
                                <input id="FRAGMENT_TRIP_UPDATEtripStatusCompletedInputID"
                                       type="radio"
                                       name="status"
                                       value="<%=TripStatusEnum.COMPLETED.getValue()%>"
<%                                   if(tripSelected_forTripUpdate.getStatus() != TripStatusEnum.ACTIVE) { %>
                                           checked="true"
<%                                   } %>                    
                                       onchange="trip_generic_status_displayWarning(FRAGMENT_TRIP_UPDATE)" />
                                <label for="FRAGMENT_TRIP_UPDATEtripStatusCompletedInputID"><jstl-fmt:message key="label_status_completed" bundle="${msgprop}"/></label>
                            </div>
                            
                            <%-- WARNING MESSAGE --%>       
                            <jstl-fmt:message key="info_trip_completed_warning" var="tripCompletedWarningMessage" bundle="${msgprop}"/>
                            <jsp:include page="widget_inputFieldWarning.jsp" >
                                <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_UPDATEstatusTextID" />
                                <jsp:param name="warningMessage" value="${tripCompletedWarningMessage}" />
                            </jsp:include>
                        </div>

                        <%-- PRIVACY WIDGET --%>
                        <input id="tripUpdateTripPrivacyOrigID" type="hidden" value="<%=tripSelected_forTripUpdate.getPrivacy().getValue()%>" />
                        <jstl-fmt:message key="label_privacy_select" var="labelTitle" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="label_privacy_public" var="labelPublic" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="label_privacy_private" var="labelPrivate" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="info_trip_public_warning" var="privacyPublicMessage" bundle="${msgprop}"/>  
                        <jstl-fmt:message key="info_trip_private_warning" var="privacyPrivateMessage" bundle="${msgprop}"/>  
                        <jsp:include page="widget_menuTripPrivacy.jsp" >
                            <jsp:param name="id" value="FRAGMENT_TRIP_UPDATE" />
                            <jsp:param name="labelTitle" value="${labelTitle}" />
                            <jsp:param name="labelPublic" value="${labelPublic}" />
                            <jsp:param name="labelPrivate" value="${labelPrivate}" />
                            <jsp:param name="privacyPublicMessage" value="${privacyPublicMessage}" />
                            <jsp:param name="privacyPrivateMessage" value="${privacyPrivateMessage}" />
                            <jsp:param name="privacyValueCurrent" value="<%=tripSelected_forTripUpdate.getPrivacy().getValue()%>" />
                        </jsp:include>
                        
                        <%-- TRIP LOCATION --%>
                        <div style="margin-top:20px; ">
                            <div style="width:30%; vertical-align:top; display:inline-block;">
                                <span class="font-bold"><jstl-fmt:message key="label_common_location" bundle="${msgprop}"/></span>
                            </div>
                            <div id="tripUpdateTripLocationInputID" style="vertical-align:top; display:inline-block;"><%=tripSelected_forTripUpdate.getLocation() %></div>
                            <input id="tripUpdateTripLocationOrigID" type="hidden" value="<%=tripSelected_forTripUpdate.getLocation()%>" />
                        </div>
<%                      if(tripSelected_forTripUpdate.getNumOfPlaces() > 0) { %>                        
                            <div style="position:relative; width:75%; display:inline-block;">
                                <button class="menu-page body-clickable"
                                        style="padding: 0px 10px 0px 10px; height: 48px;"
                                        onclick="menu_dropdown_showOrHide('tripUpdateLocationDropdownID')" >
                                    <span class=" font font-size-normal font-color-white">Change</span>
                                </button>
                                <div id="tripUpdateLocationDropdownID" class="menu-dropdown-container shadow-rightBottom"
                                     style="display: none; ">
<%                                  List<Place> listOfPlaces = tripSelected_forTripUpdate.getListOfPlaces();
                                    for(Place place : listOfPlaces) { %>
                                        <div id="<%=place.getId()%>"
                                             class="menu-dropdown-content"
                                             onClick="trip_update_location_dropdown_select(this)">
                                            <%=place.getLocation()%>
                                        </div>
<%                                  } %>                                    
                                </div>
                            </div>
                            
                            <%-- WARNING MESSAGE --%>
                            <jstl-fmt:message key="info_trip_location_change_warning" var="tripLocationChangeWarningMessage" bundle="${msgprop}"/>
                            <jsp:include page="widget_inputFieldWarning.jsp" >
                                <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_UPDATElocationTextID" />
                                <jsp:param name="warningMessage" value="${tripLocationChangeWarningMessage}" />
                                <jsp:param name="isDisplay" value="true" />
                            </jsp:include>
<%                      } %>

                        <%-- TRIP NOTE --%>
                        <div style="margin-top:20px;">
                            <span class="font-bold"><jstl-fmt:message key="label_common_notes" bundle="${msgprop}"/></span><br/>
                            <input id="tripUpdateNoteOrigID" type="hidden" value="<%=tripNote_forTripUpdate %>" />
                            <textarea id="tripUpdateNoteInputID" 
                                      maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NOTE %>" 
                                      cols="50" rows="4" ></textarea>
                        </div>                    
                    </div><%-- END editableSectionID --%>

                    <%-- DELETE CHECKBOX --%>
                    <jstl-fmt:message key="label_trip_delete" var="label" bundle="${msgprop}"/>  
                    <jstl-fmt:message key="info_trip_delete_greeting" var="warningMsg" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuDelete.jsp" >
                        <jsp:param name="id" value="FRAGMENT_TRIP_UPDATE" />
                        <jsp:param name="label" value="${label}" />
                        <jsp:param name="warning" value="${warningMsg}" />
                        <jsp:param name="marginTop" value="20" />
                        <jsp:param name="marginBottom" value="20" />
                    </jsp:include>

                    <%-- FUNCTIONAL BUTTONS --%>
                    <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                        <jsp:param name="submitLabel" value="${submitLabel}" />
                        <jsp:param name="submitMethodName" value="trip_update_validateAndSubmit()" />
                        <jsp:param name="cancelMethodName" value="trip_update_cancel()" />
                    </jsp:include>
                </div><%-- END formID --%>

            </div><%-- END body-form-embedded-content --%>
        </div><%-- END body-form-embedded-container --%>
    </div><%-- END tripUpdateLayoutID --%>
<% } %>      


<%-- END fragment_trip_update_form.jsp --%>