<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    User user_forUserTrips = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
    boolean isUserLoginProfile_forUserTrips = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<%  if(user_forUserTrips != null) { %>  
        <div id="userTripsLayoutID" 
             class="layout body-halfOrFullWidth body-halfOrFullWidth-secondary body-scroll menu-sliding"
             style="height:100%;"
             var_user_username="<%=user_forUserTrips.getUsername()%>"
             var_user_isUserLoginProfile="<%=isUserLoginProfile_forUserTrips%>">
             
            <%-- SUB-TITLE --%>
            <div class="body-position-center font font-size-normal font-bold"
                 style="margin-top:40px;">
                <%=user_forUserTrips.getFullname()%>&nbsp;<jstl-fmt:message key="label_trip_list" bundle="${msgprop}"/> 
            </div>

            <%-- PROGRESS WIDGET --%>
            <jsp:include page="widget_progressBar.jsp" >
                <jsp:param name="id" value="FRAGMENT_TRIP_LIST_USER_TRIPS" />
                <jsp:param name="color" value="black" />
                <jsp:param name="size" value="small" />
                <jsp:param name="position" value="center-layout" />
                <jsp:param name="marginTop" value="60" />
            </jsp:include>

            <%-- TRIP LIST SECTION --%>
            <jsp:include page="fragment_trip_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_LIST_USER_TRIPS" />
                <jsp:param name="tripRowStyleCSS" value="layout-triplist-row" />
            </jsp:include>
            
            <%-- PAGINATION WIDGET --%>
            <jsp:include page="widget_pagination_list.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_TRIP_LIST_USER_TRIPS" />
                <jsp:param name="menuGoPreviousMethodName" value="trip_userTrips_Previous()" />
                <jsp:param name="menuGoNextMethodName" value="trip_userTrips_Next()" />
            </jsp:include>
        </div>
        <script>
            trip_userTrips_Current('<%=user_forUserTrips.getUsername()%>', '<%=isUserLoginProfile_forUserTrips%>');
        </script>
<%  } %>    

<%-- END fragment_user_trips.jsp --%>