<%@ include file="include.jsp" %>

<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<%@ page import="com.trekcrumb.common.bean.ServiceResponse" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    Trip trip_forTripView = null;
    String serviceResponseJSON = null;
    User userLogin_forTripView = null;
    boolean isUserHasAccessToTrip_forTripView = false;
    
    ServiceResponse serviceResponse = 
            (ServiceResponse) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_SERVICE_RESPONSE);
    if(serviceResponse != null) {
        ObjectMapper jacksonObjMapper = new ObjectMapper();
        serviceResponseJSON = jacksonObjMapper.writeValueAsString(serviceResponse);
        
        if(serviceResponse.isSuccess() 
                && serviceResponse.getListOfTrips() != null) {
            //Get the Trip in question:
            trip_forTripView = serviceResponse.getListOfTrips().get(0);
        
            //Check user accessability
            if(WebAppUtil.isUserSessionActive(request)) {
                UserSession userSession = (UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION);
                userSession.setTripSelected(trip_forTripView);
                userLogin_forTripView = userSession.getUser();

                if(WebAppUtil.verifyUserHasAccessToTrip(request, response, trip_forTripView.getId(), null) == null) {
                    isUserHasAccessToTrip_forTripView = true;
                }
            }
            
            //Store into Session atributes for JSP fragments to use:
            request.setAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED, trip_forTripView);
            request.setAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE, isUserHasAccessToTrip_forTripView);
        }
    }
%>

<body>
    <%-- PAGE TITLE --%>
    <div class="container-title">
        <div id="tripDetailTitleID" class="body-clickable font font-style-title"
             style="display:inline-block; vertical-align:middle;"
             onClick="trip_view_DisplaySelectedTab(FRAGMENT_TRIP_DETAIL)"> 
            <span><jstl-fmt:message key="label_trip_detail" bundle="${msgprop}"/></span>
        </div>
        
<%      if(trip_forTripView != null) { %>        
            <div id="pictureDetailTitleID" class="font font-style-title-sub"
                 style="display:none; vertical-align:middle;"> 
                <span style="margin-left:5px; margin-right:5px;">&gt;&gt;</span>
                <span><jstl-fmt:message key="label_picture" bundle="${msgprop}"/></span>
            </div>
            <div id="placeDetailTitleID" class="font font-style-title-sub"
                 style="display:none; vertical-align:middle;"> 
                <span style="margin-left:5px; margin-right:5px;">&gt;&gt;</span>
                <span><jstl-fmt:message key="label_place" bundle="${msgprop}"/></span>
            </div>
<%      } %>
    </div>

    <%-- MAIN BODY --%>
    <div id="tripViewLayoutID" class="container-body body-white"
         style="overflow-y:hidden;"
         var_trip_view_fragmentOnDisplay = "" >
<%      if(serviceResponse != null && serviceResponse.isSuccess()) { 
            if(trip_forTripView == null) { %>
                <div class="font font-size-normal font-italic"
                     style="padding:10px;">
                    <span><jstl-fmt:message key="error_trip_notFound" bundle="${msgprop}"/></span>
                </div>

<%          } else { %>
                <input id="tripID" type="hidden" value="<%=trip_forTripView.getId()%>" />
<%              if(userLogin_forTripView != null) { %>
                    <input id="userID" type="hidden" value="<%=userLogin_forTripView.getUserId()%>" />
                    <input id="usernameID" type="hidden" value="<%=userLogin_forTripView.getUsername()%>" />
<%              } %>

                <%@ include file="fragment_pagination_picture.jsp" %>
                <%@ include file="fragment_pagination_place.jsp" %>

                <script>
                    trip_view_captureURLQueryParams(<%=serviceResponseJSON%>);
                </script>

                <%@ include file="fragment_trip_detail.jsp" %>
                <%@ include file="fragment_place_detail.jsp" %>
<%          } //END-IF trip not null
        } //END-IF serviceResponse not null %>
    </div>
    
    <%-- OTHER FRAGMENTS AND FORMS
         1. These fragments intentionally lay outside the main "tripViewLayoutID" <DIV>, because
            when activated, they must cover the entire screen including overshadowing Menu-Main and
            Title-Main. Thus, they must be on<BODY> level.
         2. These include forms with "body-disabled-background" CSS style that must cover entire screen.
         3. The hidden input variables are REQUIRED for use by forms.
    --%>
<%  if(trip_forTripView != null) { %>
        <%@ include file="fragment_trip_share_form.jsp" %>

<%      if(trip_forTripView.getNumOfPictures() > 0) { %>
            <%@ include file="fragment_picture_detail.jsp" %>
            <%@ include file="fragment_picture_update_form.jsp" %>
<%      } %>
    
<%      if(userLogin_forTripView != null) { %>
            <%@ include file="fragment_comment_create_form.jsp" %>
            <%@ include file="fragment_comment_delete_form.jsp" %>
<%      } %>

<%      if(isUserHasAccessToTrip_forTripView) { %>
            <%@ include file="fragment_trip_update_form.jsp" %>
            <%@ include file="fragment_picture_create_form.jsp" %>
                    
<%          if(trip_forTripView.getStatus() == TripStatusEnum.ACTIVE) { %>
                <%@ include file="fragment_place_create_form.jsp" %>
<%          } %>

<%          if(trip_forTripView.getNumOfPlaces() > 0) { %>
                <%@ include file="fragment_place_update_form.jsp" %>
<%          } %>

<%      } //END-IF isUserHasAccessToTrip 
    } //END-IF trip not null %>                
    
    <script>
<%      if(trip_forTripView != null) { %>            
             //1. Upon page-loading completed:
            trip_view_DisplayContent(<%=serviceResponseJSON%>);

            /* 
             * 2. On resize:
             *    -> Do NOT put on <body onresize=""> tag anymore because it could be executed
             *       before the page completes resizing, causing getting invalid element's 
             *       values/states (offsetWidth, offsetHeight, etc) which were previous values.
             *    -> Even in here, we MUST put a 'waiting period' to make sure client browser has 
             *       completely configure all DOM elements during resize for the same reason. 
             *    -> Operations: 
             *       --> Triggers responsiveness.
             */
            window.addEventListener("resize", function() {
                setTimeout(function() {
                    responsive_adjustTripViewLayout();
                }, (1000));
            });
            
<%      } else { %>    
            trip_view_DisplayContent(<%=serviceResponseJSON%>);
<%      } %>            
    </script>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_trip_view.jsp --%>