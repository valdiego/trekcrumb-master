<%@ include file="include.jsp" %>
<body>
    <%-- PAGE TITLE --%>
    <div class="container-title">
        <span id="pageTitleID" class="font font-style-title"><jstl-fmt:message key="label_support_contactUs" bundle="${msgprop}"/></span>
    </div>

    <%-- MAIN BODY --%>
    <div id="supportContactUsLayoutID" class="container-body" 
         style="bottom:50px;">
        <div class="body-inside-border">
            <%@ include file="fragment_user_contactUs_form.jsp" %>
        </div>
    </div>

    <%-- FOOTER --%>
    <jsp:include page="widget_footer.jsp" >
        <jsp:param name="isPositionFixed" value="true" />
    </jsp:include>
    
    <%-- DISABLING BACKGROUND --%>
    <%@ include file="widget_disableBackground.jsp" %>
    
    <%-- INFO/ERROR/RESPONSE MESSAGES FROM SERVER --%>
    <%@ include file="include_serverMessages.jsp" %>
</body>

<%-- END page_support_contactUs.jsp --%>