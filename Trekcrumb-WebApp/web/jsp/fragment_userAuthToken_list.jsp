<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%-- NOTE:
     1. Unlike our typical page/fragment, we do not populate UserAuthToken fragment with its data 
        on the server (via JSP) before returning to the client, neither do we populate it on client
        during loading (via JS). Instead, we would populate the rows only when user click a menu
        to display it.
     2. "UserAuthTokenContentID" is the main content layout where all the list rows would reside on.
     3. "userAuthTokenRowName" is the name of each list rows that is the key for javascript 
        to populate/remove/refresh the content.
--%>

<%@ page import="java.util.List" %>
<%@ page import="org.codehaus.jackson.map.ObjectMapper" %>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    User user_forUserAuthToken = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
    String userAuthTokensListJSON = null;
    if(user_forUserAuthToken != null 
            && user_forUserAuthToken.getListOfAuthTokens() != null
            && user_forUserAuthToken.getListOfAuthTokens().size() > 0) {
        userAuthTokensListJSON = (new ObjectMapper()).writeValueAsString(user_forUserAuthToken.getListOfAuthTokens());
    }
%>

<%  if(user_forUserAuthToken != null) { %>  
        <div id="userAuthTokenLayoutID" 
             class="font font-size-normal"
             style="margin-top:20px; padding:20px 20px 0px 20px; text-align:left;">
            
            <%-- Place holder for the latest UserAuthToken list value. JS operations will use
                 this value to display the rows, hence no need to pass the JSON data around
                 between operations. --%>
            <input id="userAuthTokenListInputID" type="hidden" />

            <%-- HEADER AND SHOW/HIDE MENUS --%>
            <div id="headerUserAuthTokenID" class="body-clickable"
                 onclick="userauthtoken_list_showOrHide()">
                <span id="menuShowUserAuthTokenContentID" class="font-size-large fa fa-plus-square-o" 
                      style="vertical-align:middle; margin-right:10px;"></span>
                <span id="menuHideUserAuthTokenContentID" class="font-size-large fa fa-minus-square-o" 
                      style="vertical-align:middle; margin-right:10px; display:none;"></span>
                <span class="font-bold"><jstl-fmt:message key="label_user_my_userAuthToken" bundle="${msgprop}"/></span>
            </div>

            <%-- CONTENTS --%>
            <div id="UserAuthTokenContentID"
                 style="display:none">
<%              if(userAuthTokensListJSON == null) { %>
                    <jstl-fmt:message key="info_userauthToken_listEmpty" bundle="${msgprop}"/>
   
<%              } else { %>
                    <jstl-fmt:message key="info_userauthToken_listNotEmpty" bundle="${msgprop}"/>
                    
                    <%-- TABLE HEADER --%>
                    <div>
                        <table style="width:100%; margin-top:10px; ">
                            <tr>
                                <td class="font-bold" style="width:60%; padding-right:5px;">Device Identity</td>
                                <td class="font-bold" style="padding-left:5px;"><jstl-fmt:message key="label_common_created" bundle="${msgprop}"/></td>
                                <td class="font-bold" style="width:53px; padding-left:5px;"><jstl-fmt:message key="label_common_delete" bundle="${msgprop}"/></td>
                            </tr>
                        </table>
                    </div>
                    
                    <%-- TABLE ROWS
                         1. By JS function: This is the list of rows where it would be copy-pasted 
                            from the widget and populated, or REMOVED when the list is empty. 
                         2. Each row is a direct child-node of UserAuthTokenContentID element. This is key for
                            adding rows and removing rows. --%>
                        
<%              } //END UserAuthToken list not empty %>                             
            </div>
            
            <%-- ROW TEMPLATE WIDGET --%> 
            <jsp:include page="widget_row_userAuthToken.jsp" />

<%          if(userAuthTokensListJSON != null) { %>
                <script>
                    userauthtoken_list_init(<%=userAuthTokensListJSON %>);
                </script>
<%          } %>
        </div>
        
<%  } //END user_forUserAuthToken not null %>

<%-- END fragment_userAuthToken_list.jsp --%>