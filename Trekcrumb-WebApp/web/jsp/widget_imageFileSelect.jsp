<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String labelText = request.getParameter("labelText");

    int imagePreviewWidthPercent = 75;
    String imagePreviewWidthPercentStr = request.getParameter("imagePreviewWidthPercent");
    if(imagePreviewWidthPercentStr != null && imagePreviewWidthPercentStr.trim() != "") {
        try {
            imagePreviewWidthPercent = Integer.parseInt(imagePreviewWidthPercentStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }

    int marginTop = 0;
    String marginTopStr = request.getParameter("marginTop");
    if(marginTopStr != null && marginTopStr.trim() != "") {
        try {
            marginTop = Integer.parseInt(marginTopStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }
    
    int marginBottom = 0;
    String marginBottomStr = request.getParameter("marginBottom");
    if(marginBottomStr != null && marginBottomStr.trim() != "") {
        try {
            marginBottom = Integer.parseInt(marginBottomStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }
%>

<div id="imageFileSelectID"
     style="padding-top:20px; padding-bottom:20px; margin-top:<%=marginTop%>px; margin-bottom:<%=marginBottom%>px; ">
    <%-- SELECT FILE INPUT MENU:
         1. The outer <DIV> is critical to wrap the entire contents within a menu-page box, esp.
            when display screen becomes too narrow and everything drops to next line.
         2. The "clickable" contents must be within the <LABEL> in order to have <INPUT> file to work.
         3. TODO (8/2016): Image format not supported
            Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
            with error: "Invalid argument to native writeImage". Try limit file selection to JPG
    --%>
    <div class="menu-page body-clickable font-color-white"
         style="padding:10px; display:inline-block;" >
        <label>
            <input id="imageFileInputID" 
                   type="file"
                   accept="image/jpg, .jpg, *.*"
                   style="display:none;" 
                   onchange="common_fileAPIReader_inputImageFile_selectAndShow(this)" />
            <span class="fa fa-camera" 
                  style="vertical-align:middle; margin-right:5px;"></span>
            <span class="font-bold"><%=labelText%></span>
        </label>
    </div>
    
    <%-- PREVIEW SECTION --%>
    <div id="imagePreviewSectionID" 
         style="text-align:center; margin-top:20px; overflow:hidden; display:none;">
        <img id="imagePreviewID" style="width:<%=imagePreviewWidthPercent%>%;" src="" />
    </div>
</div>

<%-- END widget_imageFileSelect.jsp --%>