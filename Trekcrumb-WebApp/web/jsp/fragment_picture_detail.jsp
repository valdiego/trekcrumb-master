<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPicDetail = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forPicDetail = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<%  if(trip_forPicDetail != null && trip_forPicDetail.getNumOfPictures() > 0) { %>
        <%-- Layout to contain PictureDetail
             1. On page (document) load: By default this fragment is hidden and empty. Upon some specific
                events (user click menu, etc.), then it triggers call JS functions to
                retrieve the Picture details and image. 
             2. Image section: The container and image dimensions are adjusted depending on device 
                display orientation and size. If excess still occurs, hide the overflow content.
                See the corresponding CSS and responsive functions.
             3. Pagination and MenuPage sections should be AFTER main layout (image, etc.) such that on narrow
                view, they will be on-top of the main layout.
         --%>
        <div id="pictureDetailLayoutID"
             style="display:none;">
             
            <div id="pictureDetailImgContentID" class="body-position-center" >
                <%-- PROGRESS WIDGET --%>
                <jstl-fmt:message key="info_picture_imageLoading" var="progressBarText" bundle="${msgprop}"/>  
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_DETAIL" />
                    <jsp:param name="color" value="white" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="position" value="center-screen" />
                    <jsp:param name="labelText" value="${progressBarText}" />
                </jsp:include>

                <%-- IMAGE ERROR WIDGET --%>
                <jsp:include page="widget_imageError.jsp" >
                    <jsp:param name="id" value="FRAGMENT_PICTURE_DETAIL" />
                    <jsp:param name="color" value="white" />
                    <jsp:param name="size" value="75" />
                    <jsp:param name="position" value="center-screen" />
                </jsp:include>

                <%-- IMAGE --%>
                <img id="FRAGMENT_PICTURE_DETAILpictureImageID<%=trip_forPicDetail.getId()%>" class="image"
                     style="margin-left:auto; margin-right:auto; margin-top:auto; margin-bottom:auto; display:none;"/>
            </div>

            <%-- MENU PAGE SECTION --%>
            <div id="FRAGMENT_PICTURE_DETAILMenuPageMainLayoutID" 
                 style="position:fixed; bottom:5px; left:5px" >

                <%-- MENU SHOW/HIDE --%>
                <jsp:include page="widget_menuPageShowAndHide.jsp" >
                    <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_DETAIL" />
                    <jsp:param name="menuPageShowMethodName" value="menu_menuPage_ShowOrHide(FRAGMENT_PICTURE_DETAIL, MENU_SLIDE_BOTTOM_TO_TOP, '5px', null)" />
                    <jsp:param name="menuPageHideMethodName" value="menu_menuPage_ShowOrHide(FRAGMENT_PICTURE_DETAIL, MENU_SLIDE_BOTTOM_TO_TOP, '5px', null)" />
                    <jsp:param name="isColorWhite" value="true" />
                </jsp:include>

                <%-- DETAIL CONTENTS:
                     1. By default, widget_menuPageShowAndHide.jsp will show menuPageShow, and hide menuPageHide.
                        Therefore, we must hide this content. And as the result, call menu_menuPage_ShowOrHide()
                        to display the content upon loading. --%>
                <div id="FRAGMENT_PICTURE_DETAILMenuPageContentID" 
                     class="menu-page-black menu-sliding body-scroll"
                     style="vertical-align:bottom; text-align:left; padding:5px; display:inline-block; width:200px; margin-bottom:-500px; visibility:hidden;">

                    <%-- MENUS --%>
<%                  if(isUserHasAccessToTrip_forPicDetail) { %>
                        <div style="position:relative;">
                            <div style="height:48px; line-height:48px; display:inline-block;">
                                <%-- In order to display body-position-right as inline-block at this particular
                                     line, first we need a parent container whose position is relative, then 
                                     somehow we need this space (empty or not) with the same dimension height.
                                     Without this, body-position-right would fall off out of line. --%>
                                &nbsp;
                            </div>
                            <div class="body-position-right" 
                                 style="display:inline;">
                                <%-- EDIT MENU --%> 
                                <div id="menuPictureEditID" class="hoverhints-menuIcon menu-icon font-color-white"
                                     style="opacity:0.75; display:inline-block;" 
                                     onmouseover="menu_hover_LabelShow(this)"
                                     onmouseout="menu_hover_LabelHide(this)"
                                     onmousedown="menu_Clicked(this)"
                                     onmouseup="menu_ClickedReset(this)"
                                     onclick="picture_update_Init()" >
                                    <span class="fa fa-pencil" ></span>
                                    <div id="menuPictureEditIDHover" 
                                          class="hoverhints-menuIcon-text-container-rightBottom" 
                                          style="display: none;">
                                        <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_picture_edit" bundle="${msgprop}"/></span>
                                    </div>
                                </div>
                            </div>
                        </div><%-- END Menus --%>
                        <div class="border-bottom-white" style="margin-bottom:10px;"></div>
<%                  } %>

                    <%-- USER PROFILE --%>
                    <jsp:include page="widget_userThumbnail.jsp" >
                        <jsp:param name="userImageName" value="<%=trip_forPicDetail.getUserImageName() %>" />
                        <jsp:param name="userFullnameValue" value="<%=trip_forPicDetail.getUserFullname()%>" />
                        <jsp:param name="usernameValue" value="<%=trip_forPicDetail.getUsername()%>" />
                        <jsp:param name="userThumbnailCustomCSS" value="font-color-white" />
                    </jsp:include>
                    
                    <%-- PICTURE ATTRIBUTES --%>
                    <div class="font font-size-small font-color-white"
                         style="margin-top:10px;">
                        Trek:&nbsp;&nbsp;<%=trip_forPicDetail.getName()%><br/>
                        Picture:&nbsp;&nbsp;<span id="FRAGMENT_PICTURE_DETAILPageCountID" class="font-bold"></span>
                    </div>
                    <div class="font font-size-normal font-color-white font-style-note"
                         style="margin-top:10px;" >
                        <span id="FRAGMENT_PICTURE_DETAILpictureNoteID" ></span>
                    </div>
                </div>
            </div><%-- END of menuPageMainLayoutID --%>
            
            <%-- PAGINATION: PREVIOUS/NEXT MENU
                 1. Placed last so that it will be clickable on top of other elements.
            --%>
            <jsp:include page="widget_menuPreviousAndNext.jsp" >
                <jsp:param name="widgetPrefixID" value="FRAGMENT_PICTURE_DETAIL" />
                <jsp:param name="menuPreviousMethodName" value="picture_detail_Previous()" />
                <jsp:param name="menuNextMethodName" value="picture_detail_Next()" />
            </jsp:include>

            <%-- MENU CLOSE --%>
            <div id="pictureDetailCloseID" 
                 class="body-position-right menu-icon body-black-transparent font-color-white" 
                 style="top:0px; opacity:0.75;"
                 onmousedown="menu_Clicked(this)"
                 onmouseup="menu_ClickedReset(this)"
                 onclick="trip_view_DisplaySelectedTab(FRAGMENT_TRIP_DETAIL)">
                <span class="fa fa-times"></span>
            </div>
        </div><%-- END of PictureDetailLayoutID --%>
        
        <script>
            menu_menuPage_ShowOrHide(FRAGMENT_PICTURE_DETAIL, MENU_SLIDE_BOTTOM_TO_TOP, '5px', null);
        </script>
        
<% } %><%-- END if trip NOT empty and pictures NOT empty --%>

<%-- END fragment_picture_detail.jsp --%>