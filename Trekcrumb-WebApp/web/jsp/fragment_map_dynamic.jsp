<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%-- DYNAMIC MAP
     -> To display mapAPI details with markers, zoom in/out tools, view type tools, etc.
     -> When Map is available, this layout will be reserved and used entirely
        for and by the Map API (such as GoogleMap, etc.). Any static content inside
        this layout would be REMOVED and replaced with map DIV, tiles and 
        logos whose z-index were set in millions.
        Therefore, unless necessary, avoid to have any element inside this layout.
     -> Do NOT declare any styling for "mapCanvasID" such as fonts, vertical/horizontal alignments,
        etc. because it will affect how mapAPI elements/texts styling too.
     -> In the case of 'mapMessageID' below, when we need to style the text, we must place the
        styling specific only to its layout. (It would be wiped out by MapAPI upon loading)
--%>
<div id="mapCanvasID" class="menu-sliding font font-size-normal" 
     var_map_displayType = "1"
     var_map_isMapInitialized = "false" >
     
    <%-- THIS LAYOUT WILL BE WHERE THE MAPAPI WILL OCCUPY --%>
         
    <div class="body-position-center font font-size-normal"
         style="margin-top:20px;">
        <span id="mapMessageID"><jstl-fmt:message key="info_place_map_loading" bundle="${msgprop}"/> </span>
    </div>
</div>

<%-- END fragment_map_dynamic.jsp --%>