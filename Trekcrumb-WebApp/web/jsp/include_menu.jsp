<%-- INCLUDE JSP IS MANDATORY TO BE INCLUDED ONTO HOST (PAGE) JSP AS PART OF OVERALL FUNCTION/DESIGN. 
     NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE --%>

<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.bean.UserSession" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    boolean isUserSessionActive_forMenu = WebAppUtil.isUserSessionActive(request);
    String userLoginImageName_forMenu = null; 
    if(isUserSessionActive_forMenu) {
        userLoginImageName_forMenu = ((UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser().getProfileImageName();
    }
%>
<%-- MENU MAIN --%>
<div id="menuMainID" class="container-menu">
    <%-- MENU MAIN - APP LOGO --%>
    <div id="menuBackIconID" class="menu-main-icon"
         style="display:none; "
         onclick="toBeSetByJavascript" >
        <span class="fa fa-chevron-circle-left" ></span>
    </div>
    <div id="appLogoID" class="menu-logo"></div>

    <%-- MENU MAIN ICONS --%>
    <div class="body-position-right" style="display:inline;">
    
        <%-- MENU MAIN - USER PROFILE (USERLOGIN ONLY) --%>
<%      if(isUserSessionActive_forMenu) { %>
            <div id="menuUserProfileIconID" class="menu-main-icon hoverhints-menuIcon"
                 style="line-height:normal; "
                 onmouseover="menu_hover_LabelShow(this)"
                 onmouseout="menu_hover_LabelHide(this)"
                 onmousedown="menu_Clicked(this)"
                 onmouseup="menu_ClickedReset(this)"
                 onclick="user_view_Retrieve(true, null)" >
                <div class="menu-main-userprofile-frame">
                    <span name="menuUserProfileImageNameDefault" class="fa fa-user font-color-menu"></span>
<%                  if(userLoginImageName_forMenu != null) { %>
                        <img name="menuUserProfileImageName" class="menu-main-userprofile-image"
                             style="display:none;" />
<%                  } %>   
                </div>
                <div id="menuUserProfileIconIDHover" 
                      class="hoverhints-menuIcon-text-container-rightBottom" 
                      style="display: none;">
                    <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_user_my_profile" bundle="${msgprop}"/></span>
                </div>
            </div>
<%      } %>        
    
        <%-- MENU MAIN - DEFAULT/PUBLIC --%>
        <div id="menuHomeIconID" class="menu-main-icon hoverhints-menuIcon"
             onmouseover="menu_hover_LabelShow(this)"
             onmouseout="menu_hover_LabelHide(this)"
             onmousedown="menu_Clicked(this)"
             onmouseup="menu_ClickedReset(this)"
             onclick="home_init()" >
            <span class="fa fa-home" ></span>
            <div id="menuHomeIconIDHover" 
                  class="hoverhints-menuIcon-text-container-rightBottom" 
                  style="display: none;">
                <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_common_home" bundle="${msgprop}"/></span>
            </div>
        </div>
        <div id="menuSearchIconID" class="menu-main-icon hoverhints-menuIcon"
             onmouseover="menu_hover_LabelShow(this)"
             onmouseout="menu_hover_LabelHide(this)"
             onmousedown="menu_Clicked(this)"
             onmouseup="menu_ClickedReset(this)"
             onclick="trip_search_Init()" >
            <span class="fa fa-search" ></span>
            <div id="menuSearchIconIDHover" 
                  class="hoverhints-menuIcon-text-container-rightBottom" 
                  style="display: none;">
                <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_trip_search" bundle="${msgprop}"/></span>
            </div>
        </div>
    </div><%-- END 'body-position-right' --%>
</div><%-- END menuMainID --%>

<%-- MENU MORE --%>
<div id="menuMoreID" class="menu-more " 
     onmousedown="menu_Clicked(this)"
     onmouseup="menu_ClickedReset(this)"
     onclick="menu_menuPage_ShowOrHide('MenuMore', MENU_SLIDE_RIGHT_TO_LEFT, '0px', null)">
    <div class="menu-icon font-color-white">
        <span class="fa fa-bars" ></span>
    </div>
</div>

<%-- MENU MORE LIST (CONTENTS) --%>
<div id="MenuMoreMenuPageContentID" class="menu-more-list menu-sliding font font-size-normal font-color-white" 
     style="right:-500px; visibility:hidden;" 
     onmouseover="menu_menuMore_Show()"
     onmouseout="menu_menuMore_Hide()" >
    <%-- MENU MORE - USER PROFILE (USERLOGIN ONLY) --%>
<%  if(isUserSessionActive_forMenu) { %>
        <div id="menuUserProfileTextID" class="menu-more-list-item-hideable"
             onmouseover="menu_hover_ElementActive(this)"
             onmouseout="menu_hover_ElementInactive(this)"
             onclick="user_view_Retrieve(true, null)" >
            <div class="menu-icon"
                 style="display:inline-block; ">
                <div class="menu-main-userprofile-frame" >
                    <span name="menuUserProfileImageNameDefault" class="fa fa-user font-color-menu"></span>
<%                  if(userLoginImageName_forMenu != null) { %>
                        <img name="menuUserProfileImageName" class="menu-main-userprofile-image"
                             style="display:none;" />
<%                  } %>        
                </div>
            </div>
            <span><jstl-fmt:message key="label_user_my_profile" bundle="${msgprop}"/></span>
        </div>
<%  } %>

    <%-- MENU MORE - DEFAULT/PUBLIC --%>
    <div id="menuHomeTextID" class="menu-more-list-item-hideable"
         onmouseover="menu_hover_ElementActive(this)"
         onmouseout="menu_hover_ElementInactive(this)"
         onclick="home_init()">
        <span class="menu-icon fa fa-home" ></span>
        <span><jstl-fmt:message key="label_common_home" bundle="${msgprop}"/></span>
    </div>
    <div id="menuSearchTextID" class="menu-more-list-item-hideable"
         onmouseover="menu_hover_ElementActive(this)"
         onmouseout="menu_hover_ElementInactive(this)"
         onclick="trip_search_Init()">
        <span class="menu-icon fa fa-search" ></span>
        <span><jstl-fmt:message key="label_trip_search" bundle="${msgprop}"/></span>
    </div>
    <div id="menuRefreshID" class="menu-more-list-item"
         onmouseover="menu_hover_ElementActive(this)"
         onmouseout="menu_hover_ElementInactive(this)"
         onclick="common_refresh()">
        <span class="menu-icon fa fa-refresh" ></span>
        <span><jstl-fmt:message key="label_common_refresh" bundle="${msgprop}"/></span>
    </div>

    <%-- MENU MORE - USERLOGIN ONLY --%>
<%  if(isUserSessionActive_forMenu) { %>
        <div id="menuTripCreateID" class="menu-more-list-item"
             onmouseover="menu_hover_ElementActive(this)"
             onmouseout="menu_hover_ElementInactive(this)"
             onclick="trip_create_Init()" >
            <span class="menu-icon fa fa-plus" ></span>
            <span><jstl-fmt:message key="label_trip_create" bundle="${msgprop}"/></span>
        </div>
<%  } %>

    <%-- MENU MORE - LOGIN/LOGOUT --%>
<%  if(isUserSessionActive_forMenu) { %>
        <div id="menuLogoutID" class="menu-more-list-item"
             onmouseover="menu_hover_ElementActive(this)"
             onmouseout="menu_hover_ElementInactive(this)"
             onclick="user_logout()">
            <span class="menu-icon fa fa-sign-out" ></span>
            <span><jstl-fmt:message key="label_common_logout" bundle="${msgprop}"/></span>
        </div>
<%  } else { %>
        <div id="menuSignUpID" class="menu-more-list-item"
             onmouseover="menu_hover_ElementActive(this)"
             onmouseout="menu_hover_ElementInactive(this)"
             onclick="user_signup_init()">
            <span class="menu-icon fa fa-sign-in" ></span>
            <span><jstl-fmt:message key="label_common_signup" bundle="${msgprop}"/></span>
        </div>
<%  } %>

    <%-- SUPPORT (LAST) --%>
    <div id="menuSupportTextID" class="menu-more-list-item"
         onmouseover="menu_hover_ElementActive(this)"
         onmouseout="menu_hover_ElementInactive(this)"
         onclick="location.href='${pageContext.request.contextPath}/trekcrumb/support'">
        <span class="menu-icon fa fa-question-circle" ></span>
        <span><jstl-fmt:message key="label_support" bundle="${msgprop}"/></span>
    </div>
</div>

<% if(userLoginImageName_forMenu != null) { %>
    <script>
         user_profileImage_Retrieve('<%=userLoginImageName_forMenu %>', 'menuUserProfileImageName');
    </script>
<% } %>

<%-- END include_menu.jsp --%>