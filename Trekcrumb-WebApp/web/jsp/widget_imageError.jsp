<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    /*
     * Main layout's custom ID that will function as a prefix, if provided.
     */
    String idPrefix = request.getParameter("id");
    if(idPrefix == null) {
        idPrefix = "";
    }

    /*
     * Widget text color: Expected value is either empty for default black, or 'white'.
     */
    String cssFontStyleColor = ""; 
    String widgetColor = request.getParameter("color");
    if(widgetColor != null && widgetColor.trim() != "" && widgetColor.trim().equalsIgnoreCase("white")) {
        cssFontStyleColor = "font-color-white";
    }
    
    /*
     * Widget image size (square): integer
     */
    int widgetSize = 48;
    String widgetSizeStr = request.getParameter("size");
    if(widgetSizeStr != null && widgetSizeStr.trim() != "") {
        try {
            widgetSize = Integer.parseInt(widgetSizeStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }
    
    /*
     * Widget Position either centered on the screen, or center on current layout section (but not
     * necessarily on screen) or not centered (left).
     * If it is center-screen, limit its width and set marginLeft for half of the width so it will be
     * centered on screen.
     */
    String widgetWidth = "auto";
    String widgetMarginLeft = "0px";
    String widgetPosition = request.getParameter("position");
    if(widgetPosition != null && widgetPosition.trim() != "") {
        if(widgetPosition.trim().equalsIgnoreCase("center-screen")) {
            widgetPosition = "body-position-center-screen";
            widgetWidth = "200px";
            widgetMarginLeft = "-100px";
        } else if(widgetPosition.trim().equalsIgnoreCase("center-layout")) {
            widgetPosition = "body-position-center";
        } else {
            widgetPosition = "";
        }
    }
    
    /*
     * Widget margin-top value, if required. If you set widgetPosition = 'center-screen', it is
     * not recommended to set margin-top.
     */
    int marginTop = 0;
    String marginTopStr = request.getParameter("marginTop");
    if(marginTopStr != null && marginTopStr.trim() != "") {
        try {
            marginTop = Integer.parseInt(marginTopStr);
        } catch(NumberFormatException nfe) {
            //Nothing
        }
    }
%>

<div id="<%=idPrefix%>ImageErrorLayoutID"
     class=<%=widgetPosition%>
     style="line-height:20px; width:<%=widgetWidth%>; margin-left:<%=widgetMarginLeft%>; margin-top:<%=marginTop%>px;">
     <span class="fa fa-image"
           style="color:#7F7F7F; margin-bottom:10px; opacity:0.75; font-size:<%=widgetSize%>px;"></span>
    <br/>
    <span id="<%=idPrefix%>ImageErrorTextID" class="font font-size-small font-italic <%=cssFontStyleColor%>"></span>
</div> 


<%-- END widget_imageError.jsp --%>