<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    String widgetPrefixID = request.getParameter("widgetPrefixID");
    if(widgetPrefixID == null) {
        widgetPrefixID = "";
    }

    String menuNextMethodName = request.getParameter("menuNextMethodName");
    if(menuNextMethodName == null) {
        menuNextMethodName = "";
    }
    
    boolean isHideByDefault = Boolean.parseBoolean(request.getParameter("isHideByDefault"));
    
    String menuLabel = MessageResourceUtil.getMessageValue("label_common_more", null, null);
%>

<div id="<%=widgetPrefixID%>MenuMoreNextID" 
     style="position:relative; height:48px; line-height:48px;
<%   if(isHideByDefault) { %>
        display:none;
<%   } %> ">
    <div style="display:inline-block;">
        <%-- In order to display body-position-right as inline-block at this particular
             line, first we need a parent container whose position is relative, then 
             somehow we need this space (empty or not) with the same dimension height.
             Without this, body-position-right would fall off out of line. --%>
        &nbsp;
    </div>
    <div class="body-position-right body-clickable" 
         style="padding:0px 10px 0px 10px; text-align:center; display:inline-block;"
         onmousedown="menu_Clicked(this)"
         onmouseup="menu_ClickedReset(this)"
         onclick="<%=menuNextMethodName%>">
         <span class="font-bold" style="margin-right:5px;"><%=menuLabel %></span>
         <span class="fa fa-chevron-right"></span>
    </div>
</div>


<%-- END widget_menuMoreNext.jsp --%>