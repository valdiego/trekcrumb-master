<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>

<div id="userCreateLayoutID" class="body-form-embedded-container font font-size-normal">
    <div class="body-form-embedded-content" >
        <%-- SUB-TITLE AND GREETING --%>
        <div>
            <span class="font-size-large font-bold"><jstl-fmt:message key="label_user_create" bundle="${msgprop}"/></span>
            <br/>
            <span><jstl-fmt:message key="info_user_create_greeting" bundle="${msgprop}"/></span>
        </div>
        <div class="border-bottom-black" style="margin-bottom:20px; "></div>

        <%-- FORM --%>
        <spring-form:form id="userCreateFormID"
                          method="post" 
                          commandName="userCreateFormBean"
                          action="" >
            <div class="body-form-input-field">
                <span class="font-bold"><jstl-fmt:message key="label_user_username" bundle="${msgprop}"/></span>
                <span class="font-style-footnote">&nbsp;*<jstl-fmt:message key="info_rule_charsBetween6And25" bundle="${msgprop}"/></span>
                <br/>
                <spring-form:input path="username"
                                   id="userCreateUsernameInputID"
                                   type="text"
                                   maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_USERNAME)%>"
                                   tabindex="1" />
            </div>

            <div class="body-form-input-field">
                <span class="font-bold"><jstl-fmt:message key="label_user_email" bundle="${msgprop}"/></span>
                <span class="font-style-footnote">&nbsp;*Example: yourname@yyy.zzz</span>
                <br/>
                <spring-form:input path="email"
                                   id="userCreateEmailInputID"
                                   type="email"
                                   maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL)%>"
                                   tabindex="2" />
                <div id="emailInputIDReenterSection" style="margin-bottom:20px;">
                    <span><jstl-fmt:message key="label_user_email_reenter" bundle="${msgprop}"/></span><br/>
                    <input id="userCreateEmailInputReenterID" 
                           type="text" 
                           maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL)%>"
                           onchange="validate_InputVsReenterInput('userCreateEmailInputID', this, false)" >
                           
                    <%-- INPUT ERROR MESSAGE --%>       
                    <jstl-fmt:message key="error_user_invalidparam_confirmValueNotMatch" var="errorMessage" bundle="${msgprop}"/>
                    <jsp:include page="widget_inputFieldError.jsp" >
                        <jsp:param name="widgetPrefixID" value="userCreateEmailInputReenterID" />
                        <jsp:param name="errorMessage" value="${errorMessage}" />
                    </jsp:include>
                </div>
            </div>

            <div class="body-form-input-field">
                <span class="font-bold"><jstl-fmt:message key="label_user_password" bundle="${msgprop}"/></span>
                <jstl-fmt:message key="info_rule_charsBetween6And25" var="pwdPlaceholder1" bundle="${msgprop}"/>  
                <jstl-fmt:message key="info_rule_caseSensitive" var="pwdPlaceholder2" bundle="${msgprop}"/>  
                <span class="font-style-footnote">&nbsp;*${pwdPlaceholder1}, ${pwdPlaceholder2}</span>
                <br/>
                <spring-form:input path="password"
                                   id="userCreatePwdInputID"
                                   type="password"
                                   maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD)%>"
                                   tabindex="3" />
                <div id="pwdInputIDReenterSection" style="margin-bottom:20px;">
                    <span><jstl-fmt:message key="label_user_password_reenter" bundle="${msgprop}"/></span><br/>
                    <input id="userCreatePwdInputReenterID" 
                           type="password" 
                           maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD)%>"
                           onkeyup="validate_InputVsReenterInput('userCreatePwdInputID', this, true)" >
                           
                    <%-- INPUT ERROR MESSAGE --%>       
                    <jstl-fmt:message key="error_user_invalidparam_confirmValueNotMatch" var="errorMessage" bundle="${msgprop}"/>
                    <jsp:include page="widget_inputFieldError.jsp" >
                        <jsp:param name="widgetPrefixID" value="userCreatePwdInputReenterID" />
                        <jsp:param name="errorMessage" value="${errorMessage}" />
                    </jsp:include>
                </div>
            </div>

            <div class="body-form-input-field">
                <span class="font-bold"><jstl-fmt:message key="label_user_fullname" bundle="${msgprop}"/></span><br/>
                <spring-form:input path="fullname"
                                   id="userCreateFullnameInputID"
                                   type="text"
                                   maxlength="<%=String.valueOf(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME) %>"
                                   tabindex="4" />
            </div>
        </spring-form:form>
        
        <%-- TERMS AND PRIVACY AGREEMENTS --%>
        <div class="body-form-input-field" style="margin:20px 0px 20px 0px;">
            <div>
                <jstl-core:set var="terms" value="<span style='font-weight:bold; text-decoration:underline; cursor:pointer;' onclick='common_support_terms(true)'>Terms</span>"/>
                <jstl-core:set var="privacy" value="<span style='font-weight:bold; text-decoration:underline; cursor:pointer;' onclick='common_support_privacy(true)'>Privacy</span>"/>
                <jstl-fmt:message key="info_user_create_read_terms_and_privacy" bundle="${msgprop}">
                    <jstl-fmt:param value="${terms}"/>
                    <jstl-fmt:param value="${privacy}"/>
                </jstl-fmt:message>
            </div>
            <div style="vertical-align:top; display:inline-block;">
                <input id="doNotAcceptAgreementID"
                       type="radio"
                       name="agreement"
                       checked="checked"
                       onchange="user_create_accept_agreement(false)" />
                <label for="doNotAcceptAgreementID">
                    <jstl-fmt:message key="info_user_create_donotaccept_terms_and_privacy" bundle="${msgprop}"/>
                </label>
                <br/>
                <input id="doAcceptAgreementID"
                       type="radio"
                       name="agreement"
                       onchange="user_create_accept_agreement(true)" />
                <label for="doAcceptAgreementID">
                    <jstl-fmt:message key="info_user_create_accept_terms_and_privacy" bundle="${msgprop}"/>
                </label>
            </div>        
        </div>
        
        <%-- FUNCTIONAL BUTTONS --%>
        <div id="userCreateFunctionalButtonsID"
             style="display:none;">
            <jstl-fmt:message key="label_common_submit" var="submitLabel" bundle="${msgprop}"/>  
            <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                <jsp:param name="submitButtonIDPrefix" value="FRAGMENT_USER_CREATE" />
                <jsp:param name="submitLabel" value="${submitLabel}" />
                <jsp:param name="submitMethodName" value="user_create_validateAndSubmit()" />
            </jsp:include>
        </div>
        
    </div>
</div>
<script>
    if(mDevice_isSupportCookie) {
        document.getElementById('userCreateFormID').setAttribute("action", WEBAPP_URL_USER_CREATE_SUBMIT);
    }
</script>


<%-- END fragment_user_create_form.jsp --%>