<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<div id="disableBackgroundLayoutID" class="body-disabled-background" 
     style="display:none;">
    <%-- disableBackgroundLayoutID is, when displayed, to cover the entire original main content 
         with a shade when user action prompts operation or another layout that should make 
         the previous main content un-operable (all clickables become non functional). --%>
    
    <div class="body-inside-border body-position-center-screen"
         style="width:250px; margin-left:-125px; text-align:center;">

        <%-- PROGRESS WIDGET --%>
        <jsp:include page="widget_progressBar.jsp" >
            <jsp:param name="color" value="black" />
            <jsp:param name="size" value="small" />
        </jsp:include>

    </div>
</div>
