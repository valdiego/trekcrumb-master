<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.enums.TripStatusEnum" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    Trip trip_forPlaceDetail = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);
    boolean isUserHasAccessToTrip_forPlaceDetail = (Boolean) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_IS_TRUE_OR_FALSE);
%>

<% if(trip_forPlaceDetail != null) { %>
    <%-- Layout to contain not only PlaceDetail, but also Map API (map canvas)
         1. The layout has CSS styles in a way it enable the map may occupy the entire screen, 
            part of the screen or hidden based on current user navigation and device orientation 
            and width dimension (responsiveness).
         2. Pagination and MenuPage sections shouild be AFTER main layout (map canvas, etc.) such that on narrow
            view, they will be on-top of the main layout.
         3. We do NOT populate nor load the details by default during page load. But it will be determined
            afterwards by JS functions (CSS styling, responsiveness and which Trip View fragment to view).
            This is to prevent reloading the content and map multiple times by accident.
         4. WARNING: 
            -> If we set placeDetailLayoutID display to 'none' during page load or tab-hopping, we need
               to becareful since it causes its mapCanvas to have zero offsetWidth/Height, and map API may return
               empty or invalid map behaviors. Therefore, BEFORE any call to map API, the placeDetailLayoutID
               must set its display to anything but "none".
     --%>
    <div id="placeDetailLayoutID" class="body-halfOrFullWidth body-halfOrFullWidth-secondary menu-sliding">

        <%-- MAP CANVAS AREA --%> 
        <%@ include file="fragment_map_dynamic.jsp" %>
        
        
        <%-- MENU PAGE SECTION --%>
<%      if(trip_forPlaceDetail.getNumOfPlaces() > 0
            || ( isUserHasAccessToTrip_forPlaceDetail
                    && (trip_forPlaceDetail.getStatus() == TripStatusEnum.ACTIVE) )) { %>
            <div id="FRAGMENT_PLACE_DETAILMenuPageMainLayoutID" 
                 style="position:absolute; bottom:0px;" >

                <%-- MENU SHOW/HIDE --%>
                <jsp:include page="widget_menuPageShowAndHide.jsp" >
                    <jsp:param name="widgetPrefixID" value="FRAGMENT_PLACE_DETAIL" />
                    <jsp:param name="menuPageShowMethodName" value="menu_menuPage_PlaceDetailShowOrHide()" />
                    <jsp:param name="menuPageHideMethodName" value="menu_menuPage_PlaceDetailShowOrHide()" />
                    <jsp:param name="isColorWhite" value="true" />
                </jsp:include>
                
                <%-- DETAIL CONTENTS:
                     1. By default, widget_menuPageShowAndHide.jsp will show menuPageShow, and hide menuPageHide.
                        Therefore, we must hide this content. And as the result, call menu_menuPage_PlaceDetailShowOrHide()
                        to display the content upon loading. --%>
                <div id="FRAGMENT_PLACE_DETAILMenuPageContentID" 
                     class="menu-page-black menu-sliding body-scroll"
                     style="vertical-align:bottom; text-align:left; padding:5px; display:inline-block; width:200px; margin-bottom:-500px; visibility:hidden;">

                    <%-- MENUS --%>
<%                  if(isUserHasAccessToTrip_forPlaceDetail) { %>
                        <div style="position:relative;">
                            <div style="height:48px; line-height:48px; display:inline-block;">
                                <%-- In order to display body-position-right as inline-block at this particular
                                     line, first we need a parent container whose position is relative, then 
                                     somehow we need this space (empty or not) with the same dimension height.
                                     Without this, body-position-right would fall off out of line. --%>
                                &nbsp;
                            </div>
                            <div class="body-position-right" 
                                 style="display:inline;">
                                <%-- CURRENT POSITION --%>
<%                              if(trip_forPlaceDetail.getStatus() == TripStatusEnum.ACTIVE) { %>
                                    <div id="menuCurrentLocationID" class="hoverhints-menuIcon menu-icon font-color-white"
                                         style="display:inline-block;" 
                                         onmouseover="menu_hover_LabelShow(this)"
                                         onmouseout="menu_hover_LabelHide(this)"
                                         onmousedown="menu_Clicked(this)"
                                         onmouseup="menu_ClickedReset(this)"
                                         onclick="place_mapCurrentLocation_Init()" >
                                        <span class="fa fa-location-arrow" style="opacity:0.75;"></span>
                                        <div id="menuCurrentLocationIDHover" 
                                              class="hoverhints-menuIcon-text-container-rightBottom" 
                                              style="display: none;">
                                            <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_place_locationCurrent" bundle="${msgprop}"/></span>
                                        </div>
                                    </div>
<%                              } %>
                                 
                                <%-- EDIT --%>
<%                              if(trip_forPlaceDetail.getNumOfPlaces() > 0) { %>
                                    <div id="menuPlaceEditID" class="hoverhints-menuIcon menu-icon font-color-white"
                                         style="display:inline-block;" 
                                         onmouseover="menu_hover_LabelShow(this)"
                                         onmouseout="menu_hover_LabelHide(this)"
                                         onmousedown="menu_Clicked(this)"
                                         onmouseup="menu_ClickedReset(this)"
                                         onclick="place_update_Init()" >
                                        <span class="fa fa-pencil" style="opacity:0.75;"></span>
                                        <div id="menuPlaceEditIDHover" 
                                              class="hoverhints-menuIcon-text-container-rightBottom" 
                                              style="display: none;">
                                            <span class="hoverhints-menuIcon-text-body-rightBottom"><jstl-fmt:message key="label_place_edit" bundle="${msgprop}"/></span>
                                        </div>
                                    </div>
<%                              } %>
                                
                            </div>
                        </div><%-- END Menus --%>
                        <%-- div class="border-bottom-active-dark"></div --%>                        
                        <div class="border-bottom-white"></div>
<%                  } %>

                    <%-- PLACE DETAILS
                         NOTE: Notice we are using <div> between text lines/sections instead of
                               using <br> or <p> to avoid useless white-space when the texts
                               were empty. --%>
<%                  if(trip_forPlaceDetail.getNumOfPlaces() > 0) { %>
                        <div class="font font-size-small font-color-white font-bold">
                            <span id="PlaceDetailPageCountID"></span>
                        </div>
                        <div class="font font-size-small font-color-white">
                            <span id="placeLocationID"></span>
                        </div>
                        <div class="font font-size-small font-color-white">
                            <span id="placeCreatedDateID"></span>
                        </div>
<%                  } %>

                    <div class="font font-size-normal font-color-white font-style-note" style="margin-top:10px;">
                        <span id="placeNoteID"></span>
                    </div>
            
                </div><%-- END of PlaceDetailMenuPageContentID section --%>
            </div><%-- END of PlaceDetailMenuPageMainLayoutID section --%>
<%      } //END IF places not null or trip is active %>
       
       
        <%-- PAGINATION: PREVIOUS/NEXT MENU
             NOTE: Placed last so that it will be clickable on top of other elements.
        --%>
        <jsp:include page="widget_menuPreviousAndNext.jsp" >
            <jsp:param name="widgetPrefixID" value="FRAGMENT_PLACE_DETAIL" />
            <jsp:param name="menuPreviousMethodName" value="place_detail_Previous()" />
            <jsp:param name="menuNextMethodName" value="place_detail_Next()" />
        </jsp:include>
        
        <%-- MENU CLOSE:
             NOTE: Only display when PlaceDetail in full page. See related responsive function. 
        --%>
        <div id="FRAGMENT_PLACE_DETAILMenuCloseID" class="body-position-right menu-icon body-black-transparent font-color-white" 
             style="top:5px; right:5px; opacity:0.75; display:none;"
             onmousedown="menu_Clicked(this)"
             onmouseup="menu_ClickedReset(this)"
             onclick="trip_view_DisplaySelectedTab(FRAGMENT_TRIP_DETAIL)">
            <span class="fa fa-times"></span>
        </div>
    </div><%-- END of placeDetailLayoutID --%>
<% } //END IF trip_forPlaceDetail not null %>


<%-- END fragment_place_detail.jsp --%>