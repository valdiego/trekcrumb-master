<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF , BUT SOME OF ITS ATTRIBUTES 
     (LABEL TEXT, CSS CLASSES, ETC) MUST BE CUSTOM EDITED BY EACH PAGE. --%>

<%
    String idPrefix = request.getParameter("id");
    if(idPrefix == null) {
        idPrefix = "";
    }

    String labelText = request.getParameter("labelText");
    if(labelText == null) {
        labelText = "&nbsp;";
    }
    
    String closeMethodName = request.getParameter("closeMethodName");
    if(closeMethodName == null) {
        closeMethodName = "";
    }
    
    String closeButtonCSS = request.getParameter("closeButtonCSS");
    if(closeButtonCSS == null) {
        closeButtonCSS = "";
    }
    
    String closeButtonStyle = request.getParameter("closeButtonStyle");
    if(closeButtonStyle == null) {
        closeButtonStyle = "";
    }
%>
<div style="position:relative;">
    <div style="height:48px; line-height:48px; display:inline-block;">
        <%-- In order to display body-position-right as inline-block at this particular
             line, first we need a parent container whose position is relative, then 
             somehow we need this space with the same dimension height and a content.
             Without this, body-position-right would fall off out of line. --%>
        <span class="font-size-large font-bold"><%=labelText%></span>
    </div>
    <div id="<%=idPrefix%>menuCloseID" class="body-position-right menu-icon" 
         style="display:inline; cursor:pointer;"
         onmousedown="menu_Clicked(this)"
         onmouseup="menu_ClickedReset(this)"
         onclick="<%=closeMethodName%>">
        <%-- span class="font font-size-xlarge <%=closeButtonCSS%>"
              style="<%=closeButtonStyle%>" >&#8855;</span --%>
        <%-- span class="font font-size-xlarge <%=closeButtonCSS%>"
              style="<%=closeButtonStyle%>" >&#9746;</span --%>
        <span class="fa fa-times"></span>
    </div>
</div>

<%-- END widget_menuClose.jsp --%>