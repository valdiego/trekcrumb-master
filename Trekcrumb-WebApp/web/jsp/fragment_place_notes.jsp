<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<%@ page import="java.util.List" %>
<%@ page import="com.trekcrumb.common.bean.Trip" %>
<%@ page import="com.trekcrumb.common.bean.Place" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%
    Trip trip_forPlaceNotes = (Trip) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_TRIP_SELECTED);

    List<Place> listOfPlaces = null;
    boolean isPlaceWithNoteExist = false;
    if(trip_forPlaceNotes != null) {
        listOfPlaces = trip_forPlaceNotes.getListOfPlaces();
    }
%>

<%  if(listOfPlaces != null && listOfPlaces.size() > 0 ) { %>
        <div class="font" 
             style="margin:0px 10px 0px 10px;">
            
            <%-- HEADER AND SHOW/HIDE MENUS --%>
            <div id="headerPlaceNoteListContentID" class="body-clickable"
                 style="border-bottom:2px solid black;"
                 onclick="menu_detailLayout_showOrHide('PlaceNoteListContentID')">
                <span id="menuShowPlaceNoteListContentID" class="font-size-large fa fa-plus-square-o" 
                      style="vertical-align:middle; margin-right:10px; display:none;"></span>
                <span id="menuHidePlaceNoteListContentID" class="font-size-large fa fa-minus-square-o" 
                      style="vertical-align:middle; margin-right:10px; "></span>
                <span class="font-bold"><jstl-fmt:message key="label_place" bundle="${msgprop}"/></span>
            </div>
            
            <%-- CONTENTS:
                 1. We must use <table> to separate columns between the place icon and its details (note) to
                    maintain their alignment. If we used <div>: When the detail contain long content and 
                    display width is narrow, the detail element would drop beneath the place icon.
                 2. For each <TD> set padding-bottom to give space between rows. We cannot set any space
                    (margin-bottom nor padding-bottom) on <TR> which is ignored.
            --%>
            <div id="PlaceNoteListContentID">
                <table style="width:100%;">
<%                  int index = 0;
                    for(Place place : listOfPlaces) {
                        if(!ValidationUtil.isEmpty(place.getNote())) { 
                            if(!isPlaceWithNoteExist) {
                                isPlaceWithNoteExist = true;
                            } %>
                            <tr class="body-clickable"
                                style="vertical-align:top;"
                                onmouseover="menu_Clicked(this)"
                                onmouseout="menu_ClickedReset(this)"
                                onClick="place_notes_clicked(<%=index%>)">
                                <td style="width:48px; text-align:center;">
                                    <span class="fa fa-map-marker" style="font-size:32px;"></span>
                                    <div class="font-size-normal font-bold" style="line-height:0.5;"><%=index+1%></div>
                                </td>
                                <td style="padding:0px 0px 20px 0px;" class="font">
                                    <div class="font-size-small">
                                        <%-- LOCATION --%>
                                        <span><%=place.getLocation()%></span>
                                        <br/>

                                        <%-- CREATED DATE: 
                                             Do not fill the data on the server side, but process it on client's browser side with JS --%>
                                        <span id="placeCreatedDate<%=index%>">
                                            <script type="text/javascript">
                                               common_date_translateToLocaleTime(document.getElementById("placeCreatedDate<%=index%>"), '<%=place.getCreated()%>', true, false);
                                            </script>
                                        </span>
                                    </div>

                                    <%-- NOTE --%>
                                    <div class="font-size-normal font-style-note"><%=place.getNote()%></div>
                                </td>
                            </tr>
<%                      }
                        index++;
                   } %>
               </table>            
            </div><%-- END of PlaceNoteListContentID --%>

        </div>
        <script>
<%          if(isPlaceWithNoteExist) { %>        
                document.getElementById("headerPlaceNoteListContentID").style.display = "block";
<%          } else { %>
                document.getElementById("headerPlaceNoteListContentID").style.display = "none";
<%          } %>
        </script>
        
<%  } //END-IF Places not null %>

<%-- END fragment_place_notes.jsp --%>