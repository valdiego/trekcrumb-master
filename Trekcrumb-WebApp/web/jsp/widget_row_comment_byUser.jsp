<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> The difference between widget_row_comment byTrip vs. byUser:
        ->> byUser: The entire row body is clickable to link to Trip Detail in question.
            Its row main layout is also an <a href> so user can right-click and open a new tab.
        ->> byTrip: Only the user section is clickable to User Detail in question. 
            User section is an <a href> so user can right-click and open a new tab.  
     -> Set is "vertical-align = top" so that multiple rows can align
        horizontally parallel when they are display side by side.
     -> Some of IDs will later be modified to be unique for each rows: noteID, etc.
--%>
<%@ page import="com.trekcrumb.webapp.utility.MessageResourceUtil" %>
<%
    String labelTripName = MessageResourceUtil.getMessageValue("label_trip_name", null, null);
    String labelCreatedDate = MessageResourceUtil.getMessageValue("label_common_added", null, null);
    String labelCommentDelete = MessageResourceUtil.getMessageValue("label_comment_delete", null, null);
%>

<div id="commentRowTemplateID" class="font"
     style="vertical-align:top; display:none;">
    <table width=100%>
        <tr>
            <td class="body-inside-border"
                onmouseover="menu_hover_ElementActive(this)"
                onmouseout="menu_hover_ElementInactive(this)">
                <a id="commentRowContentID" href="#">
                    <%-- USER SECTION --%>
                    <jsp:include page="widget_userThumbnail.jsp" />

                    <%-- COMMENT DETAIL --%>
                    <div class="font-size-small font-bold">
                        <%=labelTripName %>:&nbsp;
                        <span id="tripNameID" ></span>
                    </div>
                    <div class="font-size-small">
                        <%=labelCreatedDate %>&nbsp;
                        <span id="createdDateID" ></span>
                    </div>
                    <div id="commentNoteID" class="font-size-normal font-style-note"
                         style="margin-top:10px;"></div>
                </a>
            </td>
            <td id="menuCommentDeleteLayoutID" width="53px"
                style="text-align:right; vertical-align:top; padding:0px;">
                <%-- NOTE: 
                     The menu-icon image is 48x48px, reserve the <TD> element with extra 5px. 
                     Then position the content at top-right corner of the TD element.
                --%>
                <div id="menuCommentDeleteID" 
                     class="hoverhints-menuIcon menu-icon font-color-menu"
                     style="display:inline-block;" 
                     onmouseover="menu_hover_LabelShow(this)"
                     onmouseout="menu_hover_LabelHide(this)"
                     onmousedown="menu_Clicked(this)"
                     onmouseup="menu_ClickedReset(this)"
                     onclick="" >
                    <span class="fa fa-trash" ></span>
                    <div id="menuCommentDeleteIDHover" 
                          class="hoverhints-menuIcon-text-container-rightBottom" 
                          style="display: none;">
                        <span class="hoverhints-menuIcon-text-body-rightBottom"><%=labelCommentDelete %></span>
                    </div>
                </div>
            </td>
        </tr>
    </table>  
</div>


<%-- END of widget_row_comment_byUser.jsp --%>