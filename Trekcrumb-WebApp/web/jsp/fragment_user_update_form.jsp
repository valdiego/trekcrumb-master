<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.bean.UserSession" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%@ page import="com.trekcrumb.common.utility.CommonUtil" %>
<%@ page import="com.trekcrumb.common.utility.ValidationUtil" %>
<%@ page import="com.trekcrumb.webapp.utility.WebAppUtil" %>
<%
    User userLogin_forUserUpdate = null;
    String userImageURL_forUserUpdate = null;
    String userSummary_forUserUpdate = "";            
    if(WebAppUtil.isUserSessionActive(request)) {
        userLogin_forUserUpdate = ((UserSession) request.getSession().getAttribute(CommonConstants.KEY_SESSIONOBJ_USERSESSION)).getUser();
        
        if(!ValidationUtil.isEmpty(userLogin_forUserUpdate.getProfileImageName())) {
            userImageURL_forUserUpdate = CommonConstants.FS_SERVLET_URL_GETIMAGE_PROFILE + userLogin_forUserUpdate.getProfileImageName();
        }
        
        userSummary_forUserUpdate = WebAppUtil.neutralizeStringValue(userLogin_forUserUpdate.getSummary(), false, false, false, -1);
    }
%>
<% if(userLogin_forUserUpdate != null) { %>
    <div id="userUpdateLayoutID" class="body-disabled-background" 
         style="display:none;">
        <%-- 1. When displayed, this fragment will not replace but cover the entire original main page content 
                with a shade, making the previous main content un-operable (all clickables 
                become non functional). 
             2. It has a form. By default out of server, the form is EMPTY. JS will populate them. 
             3. It may require some "hidden" input variables that are shared among other embedded forms,
                and expect these variables to be set in its parent JSP. 
        --%>
        <div id="FRAGMENT_USER_UPDATEcontentID" class="body-inside-border body-form-embedded-container font font-size-normal" >
            <div class="body-form-embedded-content" >
                <%-- SUB-TITLE AND GREETING --%>
                <jstl-fmt:message key="label_user_edit" var="labelText" bundle="${msgprop}"/>  
                <jsp:include page="widget_menuClose.jsp" >
                    <jsp:param name="id" value="FRAGMENT_USER_UPDATE" />
                    <jsp:param name="labelText" value="${labelText}" />
                    <jsp:param name="closeMethodName" value="user_update_cancel()" />
                </jsp:include>
                
                <%-- PROGRESS WIDGET --%>
                <jsp:include page="widget_progressBar.jsp" >
                    <jsp:param name="id" value="FRAGMENT_USER_UPDATE" />
                    <jsp:param name="color" value="black" />
                    <jsp:param name="size" value="small" />
                    <jsp:param name="marginTop" value="20" />
                </jsp:include>

                <%-- FORMS --%>
                <div id="FRAGMENT_USER_UPDATEformID">
                    <%-- MENU TAB --%>
                    <div class="tab-menu-bar"
                         style="margin-bottom:20px;">
                        <div id="userUpdateProfileMenuID" class="tab-menu-bar-item"
                             onclick="user_update_displaySelectedTab(this, FRAGMENT_USER_UPDATE_PROFILE)" >
                            <jstl-fmt:message key="label_user_my_profile" bundle="${msgprop}"/>
                        </div>
                        <div class="tab-menu-bar-item"
                             onclick="user_update_displaySelectedTab(this, FRAGMENT_USER_UPDATE_PROFILEIMAGE)" >
                            <jstl-fmt:message key="label_user_my_profileImage" bundle="${msgprop}"/>
                        </div>
                        <div class="tab-menu-bar-item"
                             onclick="user_update_displaySelectedTab(this, FRAGMENT_USER_UPDATE_ACCOUNT)" >
                            <jstl-fmt:message key="label_user_my_account" bundle="${msgprop}"/>
                        </div>
                    </div>

                    <input id="userUpdateUserID" type="hidden" value="<%=userLogin_forUserUpdate.getUserId()%>" />
                    <input id="userUpdateUsernameID" type="hidden" value="<%=userLogin_forUserUpdate.getUsername()%>" />

                    <%-- FORM: PROFILE --%>
                    <div id="userUpdateProfileFormContentID" class="body-form-input-field"
                         style="display:none;">
                        <div>
                            <span class="font-bold"><jstl-fmt:message key="label_user_fullname" bundle="${msgprop}"/></span><br/>
                            <input id="userUpdateFullnameOrigID" type="hidden" value="<%=userLogin_forUserUpdate.getFullname()%>" />
                            <input id="userUpdateFullnameInputID" 
                                   type="text" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME %>" />
                        </div>
                        <div>
                            <span class="font-bold"><jstl-fmt:message key="label_common_location" bundle="${msgprop}"/></span><br/>
                            <input id="userUpdateLocationOrigID" type="hidden" value="<%=userLogin_forUserUpdate.getLocation()%>" />
                            <input id="userUpdateLocationInputID" 
                                   type="text" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_LOCATION %>" />
                        </div>
                        <div>
                            <span class="font-bold"><jstl-fmt:message key="label_user_summary" bundle="${msgprop}"/></span><br/>
                            <input id="userUpdateSummaryOrigID" type="hidden" value="<%=userSummary_forUserUpdate %>" />
                            <textarea id="userUpdateSummaryInputID" 
                                      maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SUMMARY %>" 
                                      cols="50" rows="4" ></textarea>
                        </div>
                    </div>

                    <%-- FORM: PROFILE IMAGE --%>
                    <div id="userUpdateProfileImageFormContentID" class="body-form-input-field"
                         style="display:none;" >

                        <%-- USER IMAGE - SELECT FILE AND PREVIEW --%>
                        <jstl-fmt:message key="label_picture_select" var="labelText" bundle="${msgprop}"/>  
                        <jsp:include page="widget_imageFileSelect.jsp" >
                            <jsp:param name="labelText" value="${labelText}" />
                            <jsp:param name="imagePreviewWidthPercent" value="50" />
                            <jsp:param name="marginTop" value="10" />
                        </jsp:include>
                        <input id="imageBase64DataID" type="hidden" />
                        
                        <%-- USER IMAGE - INPUT WARNING MESSAGE --%>       
                        <jstl-fmt:message key="info_picture_create_greeting_format" var="warningMessage" bundle="${msgprop}"/>
                        <jsp:include page="widget_inputFieldWarning.jsp" >
                            <jsp:param name="widgetPrefixID" value="userUpdateProfileImageWarningID" />
                            <jsp:param name="warningMessage" value="${warningMessage}" />
                            <jsp:param name="isDisplay" value="true" />
                        </jsp:include>

                        <%-- USER IMAGE - CURRENT --%>
<%                      if(userImageURL_forUserUpdate != null) { %>
                            <div id="userUpdateProfileImageCurrentID"
                                 style="margin-top:20px; margin-bottom:20px;">
                                <div class="font-bold" style="margin-bottom:5px;"><jstl-fmt:message key="label_user_my_profileImage_current" bundle="${msgprop}"/></div>
                                <div class="image-profile body-position-center">
                                    <img id="userUpdateProfileImageID<%=userLogin_forUserUpdate.getUsername()%>"
                                         class="image"
                                         style="width:100%; height:auto;" 
                                         src="<%=userImageURL_forUserUpdate %>" />
                                </div>
                            </div>
<%                      } %>
                    </div>

                    <%-- FORM: ACCOUNT --%>
                    <div id="userUpdateAccountFormContentID" class="body-form-input-field"
                         style="display:none;">

                        <%-- EMAIL --%>
                        <input id="userUpdateEmailID" 
                               type="radio" 
                               name="account" 
                               value="accountEmail"
                               onclick="user_update_displaySelectedTab(null, FRAGMENT_USER_UPDATE_ACCOUNT_EMAIL)">
                        <label for="userUpdateEmailID" class="font-bold"><jstl-fmt:message key="label_user_email" bundle="${msgprop}"/></label>
                        <div id="userUpdateAccountEmailFormContentID"
                             style="margin-left:30px; display:none;">
                            <span>Current Email:</span><span style="margin-left:20px; color:grey;"><%=userLogin_forUserUpdate.getEmail()%></span>
                            <br/>
                            <span>Enter New Email</span>
                            <input id="userUpdateEmailInputID" 
                                   type="email" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL %>" />
                            <input id="userUpdateEmailInputReenterID" 
                                   type="email" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_EMAIL %>"
                                   placeholder="<jstl-fmt:message key="label_user_email_reenter" bundle="${msgprop}"/>"
                                   onchange="validate_InputVsReenterInput('userUpdateEmailInputID', this, false)" >
                                   
                            <%-- INPUT ERROR MESSAGE --%>       
                            <jstl-fmt:message key="error_user_invalidparam_confirmValueNotMatch" var="errorMessage" bundle="${msgprop}"/>
                            <jsp:include page="widget_inputFieldError.jsp" >
                                <jsp:param name="widgetPrefixID" value="userUpdateEmailInputReenterID" />
                                <jsp:param name="errorMessage" value="${errorMessage}" />
                            </jsp:include>
                        </div>

                        <div>&nbsp;</div>

                        <%-- PASSWORD --%>
                        <input id="userUpdatePwdID"
                               type="radio" 
                               name="account" 
                               value="accountPwd"
                               onclick="user_update_displaySelectedTab(null, FRAGMENT_USER_UPDATE_ACCOUNT_PWD)">
                        <label for="userUpdatePwdID" class="font-bold"><jstl-fmt:message key="label_user_password" bundle="${msgprop}"/></label>
                        <div id="userUpdateAccountPwdFormContentID"
                             style="margin-left:30px; display:none;">
                            <span>Enter Current Password</span>
                            <input id="userUpdatePwdCurrentInputID" type="password" maxlength="25" />
                            <br/>
                            <span>Enter New Password</span>
                            <jstl-fmt:message key="info_rule_charsBetween6And25" var="pwdPlaceholder1" bundle="${msgprop}"/>  
                            <jstl-fmt:message key="info_rule_caseSensitive" var="pwdPlaceholder2" bundle="${msgprop}"/>  
                            <input id="userUpdatePwdNewInputID" 
                                   type="password" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD %>" 
                                   placeholder="${pwdPlaceholder1}, ${pwdPlaceholder2}" />
                            <input id="userUpdatePwdNewInputReenterID" 
                                   type="password" 
                                   maxlength="<%=CommonConstants.STRING_VALUE_LENGTH_MAX_USER_PASSWORD %>"
                                   placeholder="<jstl-fmt:message key="label_user_password_reenter" bundle="${msgprop}"/>"
                                   onkeyup="validate_InputVsReenterInput('userUpdatePwdNewInputID', this, true)" >
                                   
                            <%-- INPUT ERROR MESSAGE --%>       
                            <jstl-fmt:message key="error_user_invalidparam_confirmValueNotMatch" var="errorMessage" bundle="${msgprop}"/>
                            <jsp:include page="widget_inputFieldError.jsp" >
                                <jsp:param name="widgetPrefixID" value="userUpdatePwdNewInputReenterID" />
                                <jsp:param name="errorMessage" value="${errorMessage}" />
                            </jsp:include>
                        </div>
                        
                        <%-- DEACTIVATE ACCOUNT --%>
                        <div style="margin-top:40px;">
                            <input id="userDeactivateCheckboxID"
                                   type="checkbox" 
                                   name="deactivateAccountCheckbox"
                                   onclick="user_update_displaySelectedTab(null, FRAGMENT_USER_UPDATE_DEACTIVATE)">
                            <label for="userDeactivateCheckboxID" class="font-bold"><jstl-fmt:message key="label_user_my_deactivate" bundle="${msgprop}"/></label>

                            <%-- INPUT WARNING MESSAGE --%>       
                            <jstl-fmt:message key="info_user_deactivate_warning" var="warningMessage" bundle="${msgprop}"/>
                            <jsp:include page="widget_inputFieldWarning.jsp" >
                                <jsp:param name="widgetPrefixID" value="userDeactivateCheckboxID" />
                                <jsp:param name="warningMessage" value="${warningMessage}" />
                            </jsp:include>
                        </div>
                    </div>

                    <%-- FUNCTIONAL BUTTONS --%>
                    <p/>
                    <jstl-fmt:message key="label_common_save" var="submitLabel" bundle="${msgprop}"/>  
                    <jsp:include page="widget_menuCancelAndSubmit.jsp" >
                        <jsp:param name="submitLabel" value="${submitLabel}" />
                        <jsp:param name="submitMethodName" value="user_update_validateAndSubmit()" />
                        <jsp:param name="cancelMethodName" value="user_update_cancel()" />
                    </jsp:include>
                </div><%-- END formID --%>
            
            </div><%-- END body-form-embedded-content --%>
        </div><%-- END body-form-embedded-container --%>
    </div><%-- END userUpdateLayoutID --%>
    
<%  } %>

<%-- END fragment_user_update_form.jsp --%>