<%-- WIDGET JSP COULD BE INCLUDED ONTO HOST (PAGE) JSP TO DISPLAY REUSABLE ENTITY/FEATURE.
     A WIDGET WAS INTENDED TO BE GENERIC ENOUGH THAT SOME OF ITS ATTRIBUTES 
     (ID, LABEL TEXT, CSS CLASSES, ETC) COULD BE CUSTOM EDITED BY EACH PAGE. --%>

<%-- ROW TEMPLATE: TO BE COPIED AND POPULATED INTO THE VIEW  
     -> Some of IDs will later be modified to be unique for each rows: noteID, etc.
--%>
<div id="userAuthTokenRowTemplateID" 
     style="vertical-align:top; display:none;">
    <table style="width:100%">
        <tr>
            <td id="deviceIdentityID"
                style="width:60%; vertical-align:top; padding-right:5px;"></td>

            <td id="createdDateID" style="vertical-align:top; padding-left:5px;"></td>
    
            <td style="width:53px; text-align:right; vertical-align:top; padding:0px;">
                <%-- NOTE: 
                     The menu-icon image is 48x48px, reserve the <TD> element with extra 5px. 
                     Then position the content at top-right corner of the TD element.
                --%>
                <div id="menuDeleteID" 
                     class="menu-icon font-color-menu"
                     style="display:inline-block;" 
                     onmousedown="menu_Clicked(this)"
                     onmouseup="menu_ClickedReset(this)"
                     onclick="" >
                    <span class="fa fa-trash" ></span>
                </div>
            </td>
        </tr>
    </table>
</div>

<%-- END of widget_row_userAuthToken.jsp --%>