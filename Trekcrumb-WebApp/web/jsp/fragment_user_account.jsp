<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>
<%@ page import="com.trekcrumb.common.bean.User" %>
<%@ page import="com.trekcrumb.common.utility.CommonConstants" %>
<%
    User user_forUserAccount = (User) request.getAttribute(CommonConstants.KEY_SESSIONOBJ_USER_SELECTED);
%>
<%  if(user_forUserAccount != null) { %>  
        <div class="font font-size-normal"
             style="margin-top:20px; padding:20px 20px 0px 20px; text-align:left;">
        
            <%-- HEADER AND SHOW/HIDE MENUS --%>
            <div id="headerUserDetailAccountID" class="body-clickable"
                 onclick="menu_detailLayout_showOrHide('UserDetailAccountID')">
                <span id="menuShowUserDetailAccountID" class="font-size-large fa fa-plus-square-o" 
                      style="vertical-align:middle; margin-right:10px;"></span>
                <span id="menuHideUserDetailAccountID" class="font-size-large fa fa-minus-square-o" 
                      style="vertical-align:middle; margin-right:10px; display:none;"></span>
                <span class="font-bold"><jstl-fmt:message key="label_user_my_account" bundle="${msgprop}"/></span>
            </div>
        
            <%-- CONTENTS --%>
            <div id="UserDetailAccountID"
                 style="margin-top:10px; display:none">
                 <table style="width:100%;">
                    <%-- USERNAME --%>
                    <tr>
                        <td><jstl-fmt:message key="label_user_username" bundle="${msgprop}"/></td>
                        <td class="font-bold"><%=user_forUserAccount.getUsername()%></td>
                    </tr>
        
                    <%-- EMAIL --%>
                    <tr>
                        <td><jstl-fmt:message key="label_user_email" bundle="${msgprop}"/></td>
                        <td class="font-bold"><%=user_forUserAccount.getEmail()%></td>
                    </tr>
                 </table>
            </div>
        </div>                
<%  } %>    

<%-- END fragment_user_account.jsp --%>