<%-- FRAGMENT JSP TO BE INCLUDED ONTO HOST (PAGE) JSP OR ADD FROM CODE DYNAMICALLY. 
     1. NOT TO BE USED DIRECTLY AS INDIVIDUAL PAGE 
     2. USE TAG: <%@ include file="xyz.jsp" %> TO CARRY ALL THE NECESSARY LIBRARIES FROM include_header.jsp 
--%>

<div id="homePlaceGalleryFragmentLayoutID" 
     style="text-align:center;">
    
    <%-- SUB-TITLE --%>
    <div class="body-position-center body-white-transparent font font-bold font-style-greeting" 
         style="margin-bottom:20px; padding:5px; display:inline-block;">
        <jstl-fmt:message key="info_home_greeting_placeGallery" bundle="${msgprop}"/>
    </div>

    <%-- PROGRESS WIDGET --%>
    <jsp:include page="widget_progressBar.jsp" >
        <jsp:param name="id" value="FRAGMENT_PLACE_LIST_HOME" />
        <jsp:param name="color" value="white" />
        <jsp:param name="size" value="small" />
        <jsp:param name="position" value="center-layout" />
        <jsp:param name="marginTop" value="60" />
    </jsp:include>
    
    <%-- PLACE LIST ROW TEMPLATE WIDGET --%> 
    <jsp:include page="widget_row_place_asTag.jsp" />

    <%-- PLACE LIST SECTION 
         -> We want to display the content starting at the center of the parent
            layout horizontally and vertically. To accomplish this, we must display the inner divs as 
            table and table-cell. 
    --%>
    <div style="display: table; width: 100%; height: 100%;">
        <div style="display: table-cell; vertical-align: middle;">
            <div id="homePlaceGalleryListContentID" ></div>
        </div>
    </div>
    
</div>

<%-- END fragment_home_place_gallery.jsp --%>