<%-- NOTE:
     1. Project's custom ROOT.war (the war name must be all in capital!) that deploys to any J2EE 
        web server running our Trekcrumb-WebApp.war thus replacing that web server default ROOT.war 
        application. This custom main objective is to redirect any HTTP request to web server root 
        URL. i.e. http:<hostname:port> such as: 
        "http://localhost:9008" or "http://myremoteserver.prod.com/" to our deployed application
        that is: "http:<hostname:port>/Trekcrumb-WebApp".
     
     2. Most J2EE web server such as Tomcat, JBoss or other 3rd party servers may reserve/implement
        their default ROOT.war as their help page, admin page, etc. In our environment, we do not
        need these but we do need to display our app home page by default.
        
     3. Since we are redirecting client browser to a different webapp, we cannot use the typical
        "jsp:forward" syntax. Instead, it must be a redirect.
--%>
<%
    String trekcrumbHome = request.getContextPath()  + "/Trekcrumb-WebApp";

    response.setStatus(response.SC_MOVED_PERMANENTLY);
    response.setHeader("Location", trekcrumbHome); 
%>
