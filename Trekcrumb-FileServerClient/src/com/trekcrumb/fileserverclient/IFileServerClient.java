package com.trekcrumb.fileserverclient;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;

/**
 * @author Val Triadi
 * @since 01/2015
 */
public interface IFileServerClient {
    public FileServerResponse write(FileServerRequest request);
    public FileServerResponse read(FileServerRequest request);
    public FileServerResponse delete(FileServerRequest request);

    
}
