package com.trekcrumb.fileserverclient;

import com.trekcrumb.common.utility.CommonConstants;

public final class FileServerClientFactory {
    private FileServerClientFactory() {}
    
    /*
     * For each app using FileServer Client component, let's keep ONE running instance (singleton)
     * so we do not re-create new one for every FS request. 
     */
    private static IFileServerClient mFileServerClient;
    
    public static IFileServerClient getFileServerClientInstance() {
        if(mFileServerClient == null) {
            //Initialize it:
            if(CommonConstants.FS_SERVER_TYPE.equalsIgnoreCase(CommonConstants.FS_SERVER_TYPE_SERVERSOCKET)) {
                mFileServerClient = new FileServerClientSocketImpl();
            
            } else if(CommonConstants.FS_SERVER_TYPE.equalsIgnoreCase(CommonConstants.FS_SERVER_TYPE_WEBSERVICE)) {
                mFileServerClient = new FileServerClientWebServiceImpl();
                    
            } else if(CommonConstants.FS_SERVER_TYPE.equalsIgnoreCase(CommonConstants.FS_SERVER_TYPE_GOOGLECLOUDSTORAGE)) {
                mFileServerClient = new FileServerClientGoogleCloudStorageImpl();

            } else {
                throw new IllegalArgumentException("System error! FS Server type is unknown: [" + CommonConstants.FS_SERVER_TYPE + "]");
            }
        }
        
        return mFileServerClient;
    }

}
