package com.trekcrumb.fileserverclient;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A client class to connect to Trekcrumb FileServer that runs as a Server Socket (Listener)
 * 
 * @author Val Triadi
 * @since 04/2014
 */
public class FileServerClientSocketImpl
implements IFileServerClient {
    private static final Log LOG = LogFactory.getLog("com.trekcrumb.fileserverclient.FileServerClientSocketImpl"); 

    public FileServerResponse write(FileServerRequest request) {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting ... ");
        }
        FileServerResponse response = null;
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.WRITE_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.WRITE_PROFILE_PICTURE)
                || ValidationUtil.isEmpty(request.getFilename())
                || request.getFileBytes() == null) {
            return CommonUtil.composeFSResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Write File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'WRITE_TREK_PICTURE' or 'WRITE_PROFILE_PICTURE'."
                    + " Also, the Filename and file bytes are required.");
        }
        try {
            response = sendRequestAndGetResponse(request);
        } catch(ConnectException ce) {
            LOG.error("Write File error: Connection with server failure due to error: " + ce.getMessage(), ce);
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(),                     
                    ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR,
                    ce.getMessage());
        } catch(Exception e) {
            LOG.error("Unexpected error: " + e.getMessage(), e);
            e.printStackTrace();
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(),                     
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    e.getMessage());
        }
        if(LOG.isDebugEnabled()) {
            LOG.debug("Completed. ");
        }
        return response;
    }
    
    public FileServerResponse read(FileServerRequest request) {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting ...");
        }
        FileServerResponse response = null;
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.READ_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.READ_PROFILE_PICTURE)
                || (ValidationUtil.isEmpty(request.getFilename())
                        && ValidationUtil.isEmpty(request.getFileURL()))) {
            return CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Read File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'READ_TREK_PICTURE' or 'READ_PROFILE_PICTURE'."
                    + " Also, the Filename or fileURL to read is required.");
        }
        try {
            response = sendRequestAndGetResponse(request);
        } catch(ConnectException ce) {
            LOG.error("Read File error: Connection with server failure due to error: " + ce.getMessage(), ce);
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(),                     
                    ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR,
                    ce.getMessage());
        } catch(Exception e) {
            LOG.error("Unexpected error: " + e.getMessage(), e);
            e.printStackTrace();
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(), 
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    e.getMessage());
        }
        if(LOG.isDebugEnabled()) {
            LOG.debug("Completed.");
        }
        return response;
    }
    
    public FileServerResponse delete(FileServerRequest request) {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting ...");
        }
        FileServerResponse response = null;
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.DELETE_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.DELETE_PROFILE_PICTURE)
                || (ValidationUtil.isEmpty(request.getFilename())
                        && ValidationUtil.isEmpty(request.getFileURL()))) {
            return CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Delete File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'DELETE_TREK_PICTURE' or 'DELETE_PROFILE_PICTURE'."
                    + " Also, the Filename or fileURL to delete is required.");
        }
        try {
            response = sendRequestAndGetResponse(request);
        } catch(ConnectException ce) {
            LOG.error("Delete File error: Connection with server failure due to error: " + ce.getMessage(), ce);
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(),                     
                    ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR,
                    ce.getMessage());
        } catch(Exception e) {
            LOG.error("Unexpected error: " + e.getMessage(), e);
            e.printStackTrace();
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(), 
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    e.getMessage());
        }
        if(LOG.isDebugEnabled()) {
            LOG.debug("Completed.");
        }
        return response;
    }

    private static FileServerResponse sendRequestAndGetResponse(FileServerRequest request) throws Exception {
        Socket socket = null;
        ObjectOutputStream output = null;
        ObjectInputStream input = null;
        String fsHostAddress = CommonConstants.FS_SOCKET_CLIENT_TO_CONNECT_HOST + ":" + CommonConstants.FS_SOCKET_CLIENT_TO_CONNECT_PORT;
        try {
            if(LOG.isDebugEnabled()) {
                LOG.debug("Creating socket connection to server: " + fsHostAddress);
            }
            
            socket = new Socket();  
            SocketAddress socketAddress =  new InetSocketAddress(
                    CommonConstants.FS_SOCKET_CLIENT_TO_CONNECT_HOST, 
                    CommonConstants.FS_SOCKET_CLIENT_TO_CONNECT_PORT);
            socket.connect(socketAddress, CommonConstants.NETWORK_TIMEOUT_CONNECTION);
            output = new ObjectOutputStream(socket.getOutputStream());  
            input = new ObjectInputStream(socket.getInputStream());  
            if(LOG.isDebugEnabled()) {
                LOG.debug("Successfully connected to server. Sending request ...");
            }
            output.writeObject(request);
            output.flush();

            if(LOG.isDebugEnabled()) {
                LOG.debug("Waiting for response ...");
            }
            return (FileServerResponse)input.readObject();
            
        } catch(Exception e) {
            String errMsg = "FAILED while connecting and sending request to remote server [" + fsHostAddress + "] with error: " + e.getMessage();
            LOG.error(errMsg, e);
            System.err.println(errMsg);
            throw e;
            
        } finally {
            //Clean resources in the reverse order they are created:
            if(input != null) {
                try {
                    input.close();  
                } catch(Exception e) {
                    //Nothing
                }
            }
            if(output != null) {
                try {
                    output.close();  
                } catch(Exception e) {
                    //Nothing
                }
            }
            if(socket != null) {
                try {
                    socket.close();
                } catch(Exception e) {
                    //Nothing
                }
            }
        }
    }
    
}
