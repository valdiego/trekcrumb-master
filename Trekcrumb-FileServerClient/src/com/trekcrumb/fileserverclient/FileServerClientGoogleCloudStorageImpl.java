package com.trekcrumb.fileserverclient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.ObjectAccessControl;
import com.google.api.services.storage.model.StorageObject;
import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A client class to connect to 3rd-party FileServer: Google Cloud Storage server.
 * 
 * Notes:
 * 1. On 10/2017, we MUST migrate our app servers away from previous remote servers (OPENSHIFT)
 *    to Heroku. The new cloud platform did not support any deployed apps to be a file storage. Instead,
 *    Heroku has "marketplace" where we may choose their partners' FileServers. Upon our analysis,
 *    we decided to use different platform as our new FileServer: Google Cloud Storage. Given this
 *    condition, henceforth, we no longer have any running (deployed) Trekcrumb-FileServer apps.
 *    
 * 2. To switch to the new FileServer, we set the following env.properties: 
 *    "env.properties.fileserver.type" = GOOGLE_CLOUDSTORAGE
 *    
 * 3. FileServer Client impl: This class is the implementation of our IFileServerClient that functions
 *    as the integration component to Google Cloud Storage server. Google platform provides various
 *    client API to use: 
 *    -> RESTful WebService (JSON/XML): Make direct HTTP requests to Google cloud platform.
 *    -> google.api.client (googleapiclient): Older API. Requires JDK 5.
 *    -> google.cloud client API: Latest API and recommended to use. Requires JDK 8. Not yet
 *       supportive for Android platform.  
 *  
 *    We chose to use the older google.api.client because it is simpler, and it supports our
 *    current development JDK 6/7. Using RESTful WebService could have been more ideal because
 *    it was consistent with our previous FS client impl., and care only to HTTPConnection
 *    requrest/response. BUT, we found challenges to construct/use Google Credential object as part
 *    of the HTTP request (JSON Web Token/JWT) and to parse the response. Besides, we believe behind
 *    the client library, Google used the same RESTful WS call anyway and deal with composing 
 *    requests/parsing responses for us. As for the google.cloud client API, it is beyond our current dev.
 *    NOTE: I found out Google is moving away from RESTFul WS calls, and into "Google Remote Procedure Call (gRPC)" 
 *  
 * 4. Conditions
 * (a) Application Name: 
 *     The name of our "Project" in Google Cloud Storage platform. This is the 1st step to register
 *     on the platform. Along with it, it sets Project ID, Number, quotas and billings. 
 *     
 * (b) FileServer Directory and Buckets
 *     To use the Storage, we must create new "directories" (Google called them "buckets") to where 
 *     our files will be saved and resides. For Trekcrumb, we created: "trcfile-photo" (trip pictures) 
 *     and "trcfile-profile" (user pictures).
 *  
 * (c) Google Credential:
 *     -> To integrate with Google Cloud server, we must include credentials for authentication/authorization
 *        in the form of OAuth 2.0 token. For our app, we use project's Service Account (SA) that would
 *        be constructed into GoogleCredential object. We cannot use API KEY (like Google Map) 
 *        nor GMAIL account (not recommended). We do not use JWT either (too complicated). 
 *        
 *     -> We must download and store the details of this SA as a JSON file. Google client API will
 *        then find-parse-compose it into a GoogleCredential object to pass in our requests.
 *        Using GoogleCredential object, Google would do the heavy-weight for its management
 *        such as recycle, refresh, and expiration.

 *     -> Google provides a number of ways to construct the credential object, NONE of them was easy.
 *        The method we ended using is the so-called "generic" by reading/using from the JSON file
 *        directly.
 *        
 *     -> REF:
 *        ->> https://cloud.google.com/storage/docs/json_api/v1/how-tos/authorizing
 *        ->> https://developers.google.com/api-client-library/java/google-api-java-client/oauth2
 *     
 * (d) Secret File setting:
 *     -> Since we use JSON file to store all details for our Google SA details, we did not set
 *        any form of username/password or "key" in our property files like previous old time.
 *        Instead, we specify the name of the file to read as InutStream: "trekcrumb-google-svcacct-secret.json"
 *        
 *     -> In our environment setting (parameters or property file), we specify the LOCATION of this
 *        secret JSON file. Ideally, however, the location may vary between developers' local machine, and
 *        between remote servers which is fine. And, we SHALL NOT include this secret file in our resource
 *        file commit/repository. Regretfully unfortunately, Heroku server did not allow us to store
 *        any file anywhere outside our deployed package. As the result, we MUST include the secret file
 *        as part of JAR/WAR package so our code can read it. But good thing is still we do not 
 *        include it as part of file commit/repository.
 *        
 *     -> REF:
 *        ->> build.xml - target: build.jar
 *        
 * (e) File Permission vs. Caching
 *     -> By default, all uploaded file into Storage would be set to PRIVATE. Hence, any client call
 *        to read the image would need to go thru client API or RESTful WS calls. Since we require
 *        direct read-access to these files (from browser), we must explicitly set their permissions
 *        to "PUBLIC - READ" during the upload (write). 

 *     -> By default, when a file permission is set to public, Google enables its caching with expiration 
 *        time 1 hour (3600 seconds). This is so that to optimize server reading time.
 *        
 *     -> Issue (bug) arises with permission = public vs. caching = 3600 secs that when we delete
 *        this file, any client can still read it from Google cloud caching mechanism. Unfortunately,
 *        there is NO API/method to clean the caching after delete. 
 *        ->> To solve this problem, we ought to explicitly DISABLE our file caching by setting 
 *            the value to: "private, max-age=0, no-transform" (during write). We would pay
 *            performance cost during server reading time since no file available in the caching repository.
 *        ->> Otherwise, keep the caching with benefit of performance and less server hits, but
 *            out test case(s) to "verify after delete" would all FAILED.
 *            
 * 
 * (f) Image URL:
 *     -> Google Storage API did not return the uploaded file's "public URL" that we typically stored
 *        in our DB as picture/profile's imageURL. It returned, however, "mediaLINK URL" which is to 
 *        download the picture as a file, and "selfLINK URL" which is all details about the file in
 *        JSON format. 
 *     -> As the result, we must modified our code to no longer depend/use imageURL from our Picture
 *        database object. Instead, whenever needed, we compose the URL on the fly based on our
 *        env.properties setting. The DB column now is NULLABLE. 
 * -
 * (f) Required Libraries: They are inside Base/lib/google
 * 
 * @author Val Triadi
 * @since 1/2018
 */
public class FileServerClientGoogleCloudStorageImpl
implements IFileServerClient {
    private static final String APPLICATION_NAME = "Trekcrumb";
    private static final String GOOGLE_SVCACCT_FILE = "trekcrumb-google-svcacct-secret.json";
    
    /*
     * Each FS client instance, generates GoogleCredential object only ONCE!
     */
    private Credential mGoogleCredential;
    
    public FileServerClientGoogleCloudStorageImpl() {
        super();
        mGoogleCredential = getGoogleCredential();
    }
 
    public FileServerResponse write(FileServerRequest request) {
        //Validate:
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.WRITE_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.WRITE_PROFILE_PICTURE)
                || ValidationUtil.isEmpty(request.getFilename())
                || request.getFileBytes() == null) {
            return CommonUtil.composeFSResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Write File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'WRITE_TREK_PICTURE' or 'WRITE_PROFILE_PICTURE'."
                    + " Also, the Filename and file bytes are required.");
        }

        //Else: 
        return sendRequestAndGetResponse(request);
    }
    
    public FileServerResponse read(FileServerRequest request) {
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.READ_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.READ_PROFILE_PICTURE)
                || ValidationUtil.isEmpty(request.getFilename())) {
            return CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Read File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'READ_TREK_PICTURE' or 'READ_PROFILE_PICTURE'."
                    + " Also, the Filename to read is required.");
        }
        
        //Else: 
        return sendRequestAndGetResponse(request);
    }
    
    public FileServerResponse delete(FileServerRequest request) {
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.DELETE_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.DELETE_PROFILE_PICTURE)
                || ValidationUtil.isEmpty(request.getFilename())) {
            return CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Delete File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'DELETE_TREK_PICTURE' or 'DELETE_PROFILE_PICTURE'."
                    + " Also, the Filename to delete is required.");
        }

        //Else: 
        return sendRequestAndGetResponse(request);
    }

    private FileServerResponse sendRequestAndGetResponse(FileServerRequest request) {
        FileServerResponse response = null;
        
        FileServerMenuEnum fileMenu = request.getFileMenu();
        String bucketName = null;
        if(fileMenu == FileServerMenuEnum.WRITE_TREK_PICTURE
                || fileMenu == FileServerMenuEnum.READ_TREK_PICTURE
                || fileMenu == FileServerMenuEnum.DELETE_TREK_PICTURE) {
            bucketName = CommonConstants.FS_PICTURE_TREK_DIRECTORY;
        } else {
            bucketName = CommonConstants.FS_PICTURE_PROFILE_DIRECTORY;
        }

        try {
            //Setup Storage Client:
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            Storage.Builder storageBuilder = new Storage.Builder(httpTransport, jsonFactory, mGoogleCredential);
            storageBuilder.setApplicationName(APPLICATION_NAME);
            Storage storageClient = storageBuilder.build();

            //Execute:
            switch(fileMenu) {
                case WRITE_PROFILE_PICTURE:
                case WRITE_TREK_PICTURE:
                    response = write(request, bucketName, storageClient);
                    break;
                    
                case READ_PROFILE_PICTURE:
                case READ_TREK_PICTURE:
                    response = read(request, bucketName, storageClient);
                    break;

                case DELETE_PROFILE_PICTURE:
                case DELETE_TREK_PICTURE:
                    response = delete(request, bucketName, storageClient);
                    break;

                default:
                    break;
            }
            
        } catch(IllegalArgumentException ile) {
            response = CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    ile.getMessage());
        } catch(ConnectException ce) {
            System.err.println("FileServerClientGoogleCloudStorageImpl.sendRequestAndGetResponse() - Connection Error: " + ce.getMessage());
            ce.printStackTrace();
            response = CommonUtil.composeFSResponseError(
                    request.getFileMenu(), 
                    ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR,
                    ce.getMessage());
        } catch(HttpResponseException httpre) {
            System.err.println("FileServerClientGoogleCloudStorageImpl.sendRequestAndGetResponse() - HTTP Response Error: " + httpre.getMessage());
            if(httpre.getMessage() != null && httpre.getMessage().trim().toLowerCase().contains("not found")) {
                response = CommonUtil.composeFSResponseError(
                        request.getFileMenu(), 
                        ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND,
                        httpre.getMessage());
            } else {
                httpre.printStackTrace();
                response = CommonUtil.composeFSResponseError(
                        request.getFileMenu(), 
                        ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                        httpre.getMessage());
            }
        } catch(Exception e) {
            System.err.println("FileServerClientGoogleCloudStorageImpl.sendRequestAndGetResponse() - Error: " + e.getMessage());
            e.printStackTrace();
            response = CommonUtil.composeFSResponseError(
                    request.getFileMenu(), 
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    e.getMessage());
        }
        
        return response;
    }    

    /*
     * There are several ways (4 or 5) to generate Google Credentials. The ony one we found to work
     * with the client API is to use the so-called "Generic Method". 
     */
    private GoogleCredential getGoogleCredential() {
        GoogleCredential credential = null;
        InputStream fileIS = null;
        try {
            /*
             * Read location for Google secret account file from system environment parameter. If
             * none, then try to read from current relative path of the component's JAR file.
             */
            String googleSvcAcctFile = System.getenv("GOOGLE_SVCACCT_FILE");
            if(googleSvcAcctFile == null) {
                googleSvcAcctFile = System.getProperty("GOOGLE_SVCACCT_FILE");
            }
            if(googleSvcAcctFile != null) {
                fileIS = new FileInputStream(googleSvcAcctFile);
            } else {
                fileIS = FileServerClientGoogleCloudStorageImpl.class.getClassLoader().getResourceAsStream(GOOGLE_SVCACCT_FILE);
            }
            if(fileIS == null) {
                throw new IllegalArgumentException("Failed to find required Google Credentials resource!");
            }
            
            /*
             * Let Google API parse the secret JSON file. Then, we MUST set the credential's scope (auth/auth)
             * how it will access our Storage. 
             */
            credential = GoogleCredential.fromStream(fileIS);
            credential = credential.createScoped(Collections.singleton(StorageScopes.DEVSTORAGE_FULL_CONTROL));
            return credential;
            
        } catch(IllegalArgumentException iae) {
            System.err.println(iae.getMessage());
            iae.printStackTrace();
            throw iae;
            
        } catch(FileNotFoundException fnfe) {
            String errMsg = "FAILED to find Google credential secret file! Error: " + fnfe.getMessage();
            System.err.println(errMsg);
            fnfe.printStackTrace();
            throw new IllegalArgumentException(errMsg);
            
        } catch(IOException ioe) {
            String errMsg = "FAILED while reading Google credential secret file, might be invalid or corrupted! Error: " + ioe.getMessage();
            System.err.println(errMsg);
            ioe.printStackTrace();
            throw new IllegalArgumentException(errMsg);
            
        } catch(Exception e) {
            String errMsg = "FAILED unexpectedly while creating Google credential! Error: " + e.getMessage();
            System.err.println(errMsg);
            e.printStackTrace();
            throw new IllegalArgumentException(errMsg);
            
        } finally {
            if(fileIS != null) {
                try {
                    fileIS.close();
                } catch(Exception e) {
                    //Umm, ok!
                }
            }
        }
    }
    
    /*
     * See class javadoc above.
     */
    private FileServerResponse write(
            FileServerRequest request,
            String bucketName,
            Storage storageClient) throws Exception {
        InputStreamContent mediaContent = new InputStreamContent("image/jpeg", new ByteArrayInputStream(request.getFileBytes()));
        
        //Compose the file to store:
        StorageObject storageObject = new StorageObject();
        storageObject.setBucket(bucketName);
        storageObject.setName(request.getFilename());
        storageObject.setCacheControl("public, max-age=86400");
        //storageObject.setCacheControl("private, max-age=0, no-transform");
        storageObject.setContentType("image/jpeg");
        storageObject.setContentDisposition("inline");
        
        //Make the file public:
        List<ObjectAccessControl> listOfACL = new ArrayList<ObjectAccessControl>();
        ObjectAccessControl aclAllUsers = new ObjectAccessControl();
        aclAllUsers.setBucket(bucketName);
        aclAllUsers.setObject(request.getFilename());
        aclAllUsers.setEntity("allUsers");
        aclAllUsers.setRole("READER");
        listOfACL.add(aclAllUsers);
        storageObject.setAcl(listOfACL);
        
        /*
         * Compose 'Object Insert'.
         * 1) We may set its filename and file permission here - but we have set them up as part of 
         *    StorageObject (i.e., the file to store). 
         * 2) The media uploader gzips content by default, and alters the Content-Encoding accordingly.
         *    GCS dutifully stores content as-uploaded. This line below disables the media uploader 
         *    behavior, so the service stores exactly what is in the InputStream, without transformation.
         */
        Storage.Objects.Insert objectToInsert = storageClient.objects().insert(bucketName, storageObject, mediaContent);
        //objectToInsert.setName(request.getFilename());
        //objectToInsert.setPredefinedAcl("publicRead");
        objectToInsert.getMediaHttpUploader().setDisableGZipContent(true);
        
        storageObject = objectToInsert.execute();
        /*System.out.println("FileServerClientGoogleCloudStorageImpl.write() - Returned StorageObject after insert:\n" 
                + "mediaLink = " + storageObject.getMediaLink() + "\n"
                + "selfLink = " + storageObject.getSelfLink()  + "\n");
        */
        
        FileServerResponse response = CommonUtil.composeFSResponseSuccess(request.getFileMenu());
        response.setFilename(request.getFilename());
        return response;
    }
    
    private FileServerResponse read(
            FileServerRequest request,
            String bucketName,
            Storage storageClient) throws Exception {
        Storage.Objects.Get objectToGet = storageClient.objects().get(bucketName, request.getFilename());
        objectToGet.getMediaHttpDownloader().setDirectDownloadEnabled(true);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        objectToGet.executeMediaAndDownloadTo(os); 
        
        FileServerResponse response = CommonUtil.composeFSResponseSuccess(request.getFileMenu());
        response.setFileBytes(os.toByteArray());
        return response;
    }

    /*
     * 1. When we delete non-existing file, Google Storage API would throw HttpResponseException
     *    with error code: "404 Not Found". To try deleting a non-existing file should not
     *    be an error - therefore, we must catch it and treat it as business as usual.
     * 2. When file caching is enabled, Google delete would not remove the file from the caching
     *    hence, causing issue that the file was still readable. See this class javadoc for detail.
     */
    private FileServerResponse delete(
            FileServerRequest request,
            String bucketName,
            Storage storageClient) throws Exception {
        try {
            Storage.Objects.Delete objectToDelete = storageClient.objects().delete(bucketName, request.getFilename());
            objectToDelete.execute();

            FileServerResponse response = CommonUtil.composeFSResponseSuccess(request.getFileMenu());
            return response;
            
        } catch(HttpResponseException httpre) {
            System.err.println("FileServerClientGoogleCloudStorageImpl.delete() - Error: " + httpre.getMessage());
            if(httpre.getMessage() != null && httpre.getMessage().trim().toLowerCase().contains("not found")) {
                FileServerResponse response = CommonUtil.composeFSResponseSuccess(request.getFileMenu());
                return response;
            } else {
                throw httpre;
            }
        }
    }

}
