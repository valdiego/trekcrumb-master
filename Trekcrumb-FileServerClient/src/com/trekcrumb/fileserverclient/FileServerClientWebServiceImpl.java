package com.trekcrumb.fileserverclient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.codehaus.jackson.map.ObjectMapper;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A client class to connect to Trekcrumb FileServer that runs as a WebService
 * 
 * @author Val Triadi
 * @since 01/2015
 */
public class FileServerClientWebServiceImpl
implements IFileServerClient {

    public FileServerResponse write(FileServerRequest request) {
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.WRITE_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.WRITE_PROFILE_PICTURE)
                || ValidationUtil.isEmpty(request.getFilename())
                || request.getFileBytes() == null) {
            return CommonUtil.composeFSResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Write File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'WRITE_TREK_PICTURE' or 'WRITE_PROFILE_PICTURE'."
                    + " Also, the Filename and file bytes are required.");
        }
        return sendRequestAndGetResponse(request);
    }
    
    public FileServerResponse read(FileServerRequest request) {
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.READ_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.READ_PROFILE_PICTURE)
                || (ValidationUtil.isEmpty(request.getFilename())
                        && ValidationUtil.isEmpty(request.getFileURL()))) {
            return CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Read File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'READ_TREK_PICTURE' or 'READ_PROFILE_PICTURE'."
                    + " Also, the Filename or fileURL to read is required.");
        }
        return sendRequestAndGetResponse(request);
    }
    
    public FileServerResponse delete(FileServerRequest request) {
        if(request == null
                || (request.getFileMenu() != FileServerMenuEnum.DELETE_TREK_PICTURE
                        && request.getFileMenu() != FileServerMenuEnum.DELETE_PROFILE_PICTURE)
                || (ValidationUtil.isEmpty(request.getFilename())
                        && ValidationUtil.isEmpty(request.getFileURL()))) {
            return CommonUtil.composeFSResponseError(
                    null, 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR,
                    "Delete File error: Request is invalid!"
                    + " It must define FileServerMenuEnum either 'DELETE_TREK_PICTURE' or 'DELETE_PROFILE_PICTURE'."
                    + " Also, the Filename or fileURL to delete is required.");
        }
        return sendRequestAndGetResponse(request);
    }

    private static FileServerResponse sendRequestAndGetResponse(FileServerRequest request) {
        FileServerResponse response = null;
        HttpURLConnection conn = null;
        OutputStream os = null;
        Scanner scanner = null;
        String requestJSONString = null;
        try {
            URL url = new URL(CommonConstants.FS_WEBSERVICE_HOST + "/fileServerService/serve");
            System.out.println("FileServerClientWebServiceImpl.sendRequestAndGetResponse() - Ready to send request with file menu [" + request.getFileMenu() + "] to server URL: " + url);
            
            //Compose JSON request:
            //NOTE: DO NOT print out the JSON string!!! It may cause Out Of Memory for big picture bytes.
            ObjectMapper jacksonObjMapper = new ObjectMapper();
            requestJSONString = jacksonObjMapper.writeValueAsString(request);
            
            //Prepare connection:
            System.out.println("Opening and sending connection ...");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(CommonConstants.NETWORK_TIMEOUT_CONNECTION);
            conn.setReadTimeout(CommonConstants.NETWORK_TIMEOUT_READ);

            //Connect and send:
            os = conn.getOutputStream();
            os.write(requestJSONString.getBytes());
            os.flush();
            
            //Immediately clean to release memory:
            try {
                System.out.println("Closing connection and cleaning resources ...");
                os.close();
                os = null;
                requestJSONString = null;
            } catch(Exception e) {
                //O well at least we try...
            }
            
            //Evaluate response:
            System.out.println("Evaluating response ...");
            if (conn.getResponseCode() != 200) {
                String errMsg = "Unexpected error response code [" + conn.getResponseCode() 
                                 + "] with message: " + conn.getResponseMessage();
                System.err.println(errMsg);
                if(conn.getResponseCode() == 404) {
                    return CommonUtil.composeFSResponseError(
                            request.getFileMenu(), 
                            ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR,
                            errMsg);
                }
                //Else:
                return CommonUtil.composeFSResponseError(
                        request.getFileMenu(), 
                        ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                        errMsg);
            } else {
                //Success:
                //NOTE: DO NOT print out the JSON string!!! It may cause Out Of Memory for big picture bytes.
                StringBuilder responseStringBuilder = new StringBuilder();
                BufferedReader bufferedReader = 
                        new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = bufferedReader.readLine();
                while(line != null){
                    responseStringBuilder.append(line);
                    line = bufferedReader.readLine();
                }
                
                String responseJSONString = responseStringBuilder.toString();
                response = jacksonObjMapper.readValue(responseJSONString, FileServerResponse.class);
            }
            
        } catch(ConnectException ce) {
            ce.printStackTrace();
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(), 
                    ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR,
                    ce.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeFSResponseError(
                    request.getFileMenu(), 
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR,
                    e.getMessage());
        } finally {
            //Clean resources
            if(os != null) {
                try {
                    os.close();
                    os = null;
                } catch(Exception e) {
                    //O well at least we try...
                }
            }
            if(scanner != null) {
                scanner.close();
                scanner = null;
            }
            if(conn != null) {
                conn.disconnect();
                conn = null;
            }
            requestJSONString = null;
        }
        
        return response;        
    }    

}
