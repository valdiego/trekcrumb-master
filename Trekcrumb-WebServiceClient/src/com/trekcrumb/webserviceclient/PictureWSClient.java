package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class PictureWSClient {
    public static ServiceResponse create(final Picture pictureToCreate) {
        return sendRequestAndGetResponse(pictureToCreate, ServiceTypeEnum.PICTURE_CREATE, null, -1, -1);
    }
    
    @Deprecated
    //Replaced by retrieve(final Picture pictureToRetrieve, ServiceTypeEnum retrieveBy, OrderByEnum orderBy, int offset, int numOfRows)
    public static ServiceResponse retrieve(final Picture pictureToRetrieve, ServiceTypeEnum retrieveBy) {
        return sendRequestAndGetResponse(pictureToRetrieve, retrieveBy, null, -1, -1);
    }

    public static ServiceResponse retrieve(final Picture pictureToRetrieve, ServiceTypeEnum retrieveBy, OrderByEnum orderBy, int offset, int numOfRows) {
        return sendRequestAndGetResponse(pictureToRetrieve, retrieveBy, orderBy, offset, numOfRows);
    }

    public static ServiceResponse delete(final Picture pictureToDelete, ServiceTypeEnum deleteBy) {
        return sendRequestAndGetResponse(pictureToDelete, deleteBy, null, -1, -1);
    }
    
    public static ServiceResponse update(final Picture pictureToUpdate) {
        return sendRequestAndGetResponse(pictureToUpdate, ServiceTypeEnum.PICTURE_UPDATE, null, -1, -1);
    }

    public static ServiceResponse imageUpload(final Picture pictureWithImage) {
        return sendRequestAndGetResponse(pictureWithImage, ServiceTypeEnum.PICTURE_IMAGE_UPLOAD, null, -1, -1);
    }

    public static ServiceResponse imageDownload(final Picture pictureWithImage) {
        return sendRequestAndGetResponse(pictureWithImage, ServiceTypeEnum.PICTURE_IMAGE_DOWNLOAD, null, -1, -1);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            Picture picture, 
            ServiceTypeEnum serviceType,
            OrderByEnum orderBy,
            int offset, 
            int numOfRows) {
        ServiceRequest request = null;
        ServiceResponse result = null;
        try {
            request = RequestUtil.prepareRequestForPictureWS(picture, serviceType, orderBy, offset, numOfRows);
            System.out.println("Request to send: " + request);
        } catch(Exception e) {
           e.printStackTrace();
           return CommonUtil.composeServiceResponseError(
                   serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                   "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectPictureWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
