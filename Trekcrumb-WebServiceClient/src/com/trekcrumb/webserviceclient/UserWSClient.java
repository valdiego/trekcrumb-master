package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class UserWSClient {

    public static ServiceResponse userCreate(final User user) {
        return userCreate(user, null);
    }
    
    public static ServiceResponse userCreate(final User user, final UserAuthToken userAuthToken) {
        return sendRequestAndGetResponse(user, userAuthToken, ServiceTypeEnum.USER_CREATE, -1, -1);
    }

    public static ServiceResponse userConfirm(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_CONFIRM, -1, -1);
    }
    
    public static ServiceResponse userLogin(final User user) {
        return userLogin(user, null);
    }

    public static ServiceResponse userLogin(final User user, final UserAuthToken userAuthToken) {
        return sendRequestAndGetResponse(user, userAuthToken, ServiceTypeEnum.USER_LOGIN, -1, -1);
    }
    
    public static ServiceResponse userUpdate(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_UPDATE, -1, -1);
    }
    
    public static ServiceResponse userDeActivate(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_DEACTIVATE, -1, -1);
    }

    public static ServiceResponse userReActivate(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_REACTIVATE, -1, -1);
    }

    public static ServiceResponse userPasswordForget(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_PASSWORD_FORGET, 0, 1);
    }

    public static ServiceResponse userPasswordReset(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_PASSWORD_RESET, 0, 1);
    }

    /* TODO (6/2015): Replace this with userRetrieve() below */
    @Deprecated
    public static ServiceResponse userRetrieveByID(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, 0, 1);
    }
    
    public static ServiceResponse userRetrieve(final User user, ServiceTypeEnum retrieveBy, int offset, int numOfRows) {
        return sendRequestAndGetResponse(user, null, retrieveBy, offset, numOfRows);
    }
    
    public static ServiceResponse userProfileImageDownload(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_PROFILE_IMAGE_DOWNLOAD, -1, -1);
    }

    public static ServiceResponse userContactUs(final User user) {
        return sendRequestAndGetResponse(user, null, ServiceTypeEnum.USER_CONTACT_US, -1, -1);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            User user, 
            UserAuthToken userAuthToken,
            ServiceTypeEnum serviceType,
            int offset, 
            int numOfRows) {
        ServiceRequest request = null;
        ServiceResponse result = null;
        try {
            request = RequestUtil.prepareRequestForUserWS(user, userAuthToken, serviceType, offset, numOfRows);
            System.out.println("Request to send: " + request);
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(
                    serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectUserWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
