package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class TripWSClient {
    public static ServiceResponse create(final Trip tripToCreate) {
        return sendRequestAndGetResponse(tripToCreate, null, ServiceTypeEnum.TRIP_CREATE, -1, -1);
    }
    
    public static ServiceResponse publish(final Trip tripToPublish) {
        return sendRequestAndGetResponse(tripToPublish, null, ServiceTypeEnum.TRIP_PUBLISH, -1, -1);
    }

    public static ServiceResponse retrieve(final Trip tripToRetrieve, ServiceTypeEnum retrieveBy, int offset, int numOfRows) {
        return sendRequestAndGetResponse(tripToRetrieve, null, retrieveBy, offset, numOfRows);
    }
    
    public static ServiceResponse retrieveSync(final Trip tripToRetrieve) {
        return sendRequestAndGetResponse(tripToRetrieve, null, ServiceTypeEnum.TRIP_RETRIEVE_SYNC, -1, -1);
    }

    public static ServiceResponse delete(final Trip tripToDelete, ServiceTypeEnum deleteBy) {
        return sendRequestAndGetResponse(tripToDelete, null, deleteBy, -1, -1);
    }

    public static ServiceResponse update(final Trip tripToUpdate) {
        return sendRequestAndGetResponse(tripToUpdate, null, ServiceTypeEnum.TRIP_UPDATE, -1, -1);
    }
    
    public static ServiceResponse search(final TripSearchCriteria searchCriteria) {
        return sendRequestAndGetResponse(null, searchCriteria, ServiceTypeEnum.TRIP_SEARCH, -1, -1);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            Trip trip,
            TripSearchCriteria searchCriteria,
            ServiceTypeEnum serviceType,
            int offset, 
            int numOfRows) {
        ServiceRequest request = null;
        ServiceResponse result = null;
        try {
            request = RequestUtil.prepareRequestForTripWS(trip, searchCriteria, serviceType, offset, numOfRows);
            System.out.println("Request to send: " + request);
        } catch(Exception e) {
           e.printStackTrace();
           return CommonUtil.composeServiceResponseError(
                   serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                   "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectTripWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
