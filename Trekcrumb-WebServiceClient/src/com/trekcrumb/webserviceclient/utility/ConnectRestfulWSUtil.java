package com.trekcrumb.webserviceclient.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Utility to take care of connection to Restful-based (JSON) WebService.
 * 
 * @author Val Triadi
 */
public class ConnectRestfulWSUtil {

    public static ServiceResponse connectUserWS(ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.USER_CREATE) {
                url = new URL(CommonConstants.WS_HOST + "/userService/create");
            } else if(serviceType == ServiceTypeEnum.USER_CONFIRM) {
                url = new URL(CommonConstants.WS_HOST + "/userService/confirm");
            } else if(serviceType == ServiceTypeEnum.USER_LOGIN) {
                url = new URL(CommonConstants.WS_HOST + "/userService/login");
            } else if(serviceType == ServiceTypeEnum.USER_PASSWORD_FORGET) {
                url = new URL(CommonConstants.WS_HOST + "/userService/passwordForget");
            } else if(serviceType == ServiceTypeEnum.USER_PASSWORD_RESET) {
                url = new URL(CommonConstants.WS_HOST + "/userService/passwordReset");
            } else if(serviceType == ServiceTypeEnum.USER_RETRIEVE_BY_USERID
                        || serviceType == ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME
                        || serviceType == ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL) {
                url = new URL(CommonConstants.WS_HOST + "/userService/retrieve");
            } else if(serviceType == ServiceTypeEnum.USER_UPDATE) {
                url = new URL(CommonConstants.WS_HOST + "/userService/update");
            } else if(serviceType == ServiceTypeEnum.USER_DEACTIVATE) {
                url = new URL(CommonConstants.WS_HOST + "/userService/deactivate");
            } else if(serviceType == ServiceTypeEnum.USER_REACTIVATE) {
                url = new URL(CommonConstants.WS_HOST + "/userService/reactivate");
            } else if(serviceType == ServiceTypeEnum.USER_PROFILE_IMAGE_DOWNLOAD) {
                url = new URL(CommonConstants.WS_HOST + "/userService/profileImageDownload");
            } else if(serviceType == ServiceTypeEnum.USER_CONTACT_US) {
                url = new URL(CommonConstants.WS_HOST + "/userService/contactUs");
                
            } else {
                String errMsg = "Unknown service type for User WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }
    
    public static ServiceResponse connectUserAuthTokenWS(ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN) {
                url = new URL(CommonConstants.WS_HOST + "/userAuthTokenService/authenticate");
                
            } else if(serviceType == ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID
                        || serviceType == ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID) {
                url = new URL(CommonConstants.WS_HOST + "/userAuthTokenService/delete");
            
            } else {
                String errMsg = "Unknown service type for UserAuthToken WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }
    
    public static ServiceResponse connectTripWS(
            ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.TRIP_CREATE) {
                url = new URL(CommonConstants.WS_HOST + "/tripService/create");
            } else if(serviceType == ServiceTypeEnum.TRIP_PUBLISH) {
                    url = new URL(CommonConstants.WS_HOST + "/tripService/publish");
            } else if(serviceType == ServiceTypeEnum.TRIP_UPDATE) {
                url = new URL(CommonConstants.WS_HOST + "/tripService/update");
                
            } else if(serviceType == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID
                        || serviceType == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME
                        || serviceType == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID
                        || serviceType == ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE) {
                url = new URL(CommonConstants.WS_HOST + "/tripService/retrieve");
                
            } else if(serviceType == ServiceTypeEnum.TRIP_RETRIEVE_SYNC) {
                url = new URL(CommonConstants.WS_HOST + "/tripService/retrieveSync");

            } else if(serviceType == ServiceTypeEnum.TRIP_DELETE_BY_USERID
                        || serviceType == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                url = new URL(CommonConstants.WS_HOST + "/tripService/delete");
                
            } else if(serviceType == ServiceTypeEnum.TRIP_SEARCH) {
                url = new URL(CommonConstants.WS_HOST + "/tripService/search");
                
            } else {
                String errMsg = "Unknown service type for Trip WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }
    
    public static ServiceResponse connectPlaceWS(ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.PLACE_CREATE) {
                url = new URL(CommonConstants.WS_HOST + "/placeService/create");
            } else if(serviceType == ServiceTypeEnum.PLACE_UPDATE) {
                url = new URL(CommonConstants.WS_HOST + "/placeService/update");
                
            } else if(serviceType == ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID
                        || serviceType == ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID) {
                url = new URL(CommonConstants.WS_HOST + "/placeService/retrieve");
                
            } else if(serviceType == ServiceTypeEnum.PLACE_DELETE_BY_USERID
                        || serviceType == ServiceTypeEnum.PLACE_DELETE_BY_TRIPID
                        || serviceType == ServiceTypeEnum.PLACE_DELETE_BY_PLACEID) {
                url = new URL(CommonConstants.WS_HOST + "/placeService/delete");
                
            } else {
                String errMsg = "Unknown service type for Place WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }    
    
    public static ServiceResponse connectPictureWS(ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.PICTURE_CREATE) {
                url = new URL(CommonConstants.WS_HOST + "/pictureService/create");
            } else if(serviceType == ServiceTypeEnum.PICTURE_UPDATE) {
                url = new URL(CommonConstants.WS_HOST + "/pictureService/update");
                
            } else if(serviceType == ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID
                        || serviceType == ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID
                        || serviceType == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME
                        || serviceType == ServiceTypeEnum.PICTURE_SEARCH) {
                url = new URL(CommonConstants.WS_HOST + "/pictureService/retrieve");
                
            } else if(serviceType == ServiceTypeEnum.PICTURE_DELETE_BY_USERID
                        || serviceType == ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID
                        || serviceType == ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID) {
                url = new URL(CommonConstants.WS_HOST + "/pictureService/delete");
                
            } else if(serviceType == ServiceTypeEnum.PICTURE_IMAGE_UPLOAD) {
                url = new URL(CommonConstants.WS_HOST + "/pictureService/imageUpload");
            } else if(serviceType == ServiceTypeEnum.PICTURE_IMAGE_DOWNLOAD) {
                url = new URL(CommonConstants.WS_HOST + "/pictureService/imageDownload");
                
            } else {
                String errMsg = "Unknown service type for Picture WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }    
    
    public static ServiceResponse connectFavoriteWS(ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.FAVORITE_CREATE) {
                url = new URL(CommonConstants.WS_HOST + "/favoriteService/create");
                
            } else if(serviceType == ServiceTypeEnum.FAVORITE_RETRIEVE) {
                url = new URL(CommonConstants.WS_HOST + "/favoriteService/retrieve");

            } else if(serviceType == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_TRIP) {
                url = new URL(CommonConstants.WS_HOST + "/favoriteService/retrieveByTrip");
                
            } else if(serviceType == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_USER) {
                url = new URL(CommonConstants.WS_HOST + "/favoriteService/retrieveByUser");

            } else if(serviceType == ServiceTypeEnum.FAVORITE_DELETE) {
                url = new URL(CommonConstants.WS_HOST + "/favoriteService/delete");
                
            } else {
                String errMsg = "Unknown service type for Favorite WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }
    
    public static ServiceResponse connectCommentWS(ServiceRequest request) {
        ServiceTypeEnum serviceType = request.getServiceType();
        URL url = null;
        try {
            if(serviceType == ServiceTypeEnum.COMMENT_CREATE) {
                url = new URL(CommonConstants.WS_HOST + "/commentService/create");
                
            } else if(serviceType == ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID
                    || serviceType == ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME
                    || serviceType == ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID) {
                url = new URL(CommonConstants.WS_HOST + "/commentService/retrieve");

            } else if(serviceType == ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID) {
                url = new URL(CommonConstants.WS_HOST + "/commentService/delete");
                
            } else if(serviceType == ServiceTypeEnum.COMMENT_DELETE_BY_USERID
                    || serviceType == ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID) {
                String errMsg = "Illegal Comment delete type!";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, errMsg);

            } else {
                String errMsg = "Unknown service type for Comment WS [" + serviceType + "]";
                System.err.println(errMsg);
                return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            }            
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return connectServer(serviceType, request, url);           
    }         
    
    /*
     * Connects to server and evaluates response code.
     * 1. JSON Mapper
     *    -> We need to "neutralize" any Non-ASCII chars inside incoming request (name, comment, etc)
     *       because otherwise, the server (WebService) would throw "JsonParseException" due to 
     *       invalid UTF-8 char. To do this, we configure the Mapper with:
     *       mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true)
     *       
     *    -> In our Business component, we have had "neutralize" any Non-ASCII chars in request
     *       before we process further to DB. But this stage is later in the back-end, much after
     *       the WebService. See: com.trekcrumb.business.utility.BusinessUtil.neutralizeStringValue()
     *    
     *    -> Ref: 
     *       ->> https://stackoverflow.com/questions/32594503/jackson-cannot-parse-control-character
     *       ->> https://github.com/FasterXML/jackson-databind/#commonly-used-features
     *       
     * 2. DO NOT System.out.print either the request String (requestJSONString) or response String 
     *    (responseJSONString) because its content could be YUGE (image bytes) and cause Out of Memory!
     */
    private static ServiceResponse connectServer(
            ServiceTypeEnum serviceType, 
            ServiceRequest request, 
            URL url) {
        System.out.println("ConnectRestfulWSUtil.connectServer() - Starting. Request [" 
                + request.getServiceType() + "] and Target URL: " + url);
        ServiceResponse result = null;
        HttpURLConnection conn = null;
        OutputStream os = null;
        Scanner scanner = null;
        String requestJSONString = null;
        try {
            //Set JSON Mapper:
            ObjectMapper jacksonObjMapper = new ObjectMapper();
            jacksonObjMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
            
            //Parse JSON request (Do NOT print):
            requestJSONString = jacksonObjMapper.writeValueAsString(request);
            
            //Prepare connection:
            System.out.println("ConnectRestfulWSUtil.connectServer() - Opening and sending connection .... ");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setConnectTimeout(CommonConstants.NETWORK_TIMEOUT_CONNECTION);
            conn.setReadTimeout(CommonConstants.NETWORK_TIMEOUT_READ);

            //Connect and send:
            os = conn.getOutputStream();
            os.write(requestJSONString.getBytes());
            os.flush();
            
            //Immediately clean to release memory:
            try {
                System.out.println("ConnectRestfulWSUtil.connectServer() - Closing connection and cleaning resources .... ");
                os.close();
                os = null;
                requestJSONString = null;
            } catch(Exception e) {
                //O well at least we try...
            }
            
            //Evaluate response:
            System.out.println("ConnectRestfulWSUtil.connectServer() - Evaluating response ...");
            int responseCode = conn.getResponseCode();
            if (responseCode != 200) {
                String errMsg = "Unexpected error response code [" + responseCode 
                                 + "] with message: " + conn.getResponseMessage();
                System.err.println(errMsg);
                if(responseCode == 404) {
                    return CommonUtil.composeServiceResponseError(
                            serviceType, ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR, errMsg);
                }
                //Else:
                return CommonUtil.composeServiceResponseError(
                        serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, errMsg);
            } else {
                //Success - parse response:
                StringBuilder responseStringBuilder = new StringBuilder();
                BufferedReader bufferedReader = 
                        new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
                String line = bufferedReader.readLine();
                while(line != null){
                    responseStringBuilder.append(line);
                    line = bufferedReader.readLine();
                }
                
                //Do NOT print!
                String responseJSONString = responseStringBuilder.toString();
                result = jacksonObjMapper.readValue(responseJSONString, ServiceResponse.class);
            }
            
        } catch(ConnectException ce) {
            ce.printStackTrace();
            return CommonUtil.composeServiceResponseError(
                    serviceType, ServiceErrorEnum.SYSTEM_SERVER_NOT_REACHABLE_ERROR, ce.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(
                    serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        } finally {
            //Clean resources
            if(os != null) {
                try {
                    os.close();
                    os = null;
                } catch(Exception e) {
                    //O well at least we try...
                }
            }
            if(scanner != null) {
                scanner.close();
                scanner = null;
            }
            if(conn != null) {
                /*
                 * Connection's inputstream would be opened when we invoke getResponseCode() as well
                 * as getInputStream(). Thus, close it to prevent any leaks.
                 */
                try {
                    if(conn.getInputStream() != null) {
                        conn.getInputStream().close();
                    }
                } catch(Exception e) {
                    //O well at least we try...
                }
                
                conn.disconnect();
                conn = null;
            }
            requestJSONString = null;
        }
        
        return result;
    }
}
