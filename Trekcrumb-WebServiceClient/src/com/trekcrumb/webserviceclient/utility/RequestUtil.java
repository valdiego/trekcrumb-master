package com.trekcrumb.webserviceclient.utility;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.DigitalSignature;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public class RequestUtil {
    
    public static ServiceRequest prepareRequestForUserWS(
            final User user,
            final UserAuthToken userAuthToken,
            ServiceTypeEnum serviceType,
            int offset, 
            int numOfRows) 
            throws Exception {
        User userCloned = user.clone();
        if(userAuthToken != null) {
            userCloned.setUserAuthToken(userAuthToken);
        }
        
        ServiceRequest request = new ServiceRequest();
        request.setUser(userCloned);
        request.setServiceType(serviceType);
        request.setOffset(offset);
        request.setNumOfRows(numOfRows);
        signRequest(request);

        //User password:
        Cryptographer cryptoInstance = Cryptographer.getInstance();
        String pwdOrig = userCloned.getPassword();
        String pwdCrypted = cryptoInstance.encrypt(pwdOrig);
        userCloned.setPassword(pwdCrypted);
        return request;
    }
    
    public static ServiceRequest prepareRequestForUserAuthTokenWS(
            final UserAuthToken userAuthToken,
            ServiceTypeEnum serviceType) 
            throws Exception {
        UserAuthToken authTokenCloned = userAuthToken.clone();

        Cryptographer cryptoInstance = Cryptographer.getInstance();
        String origUserID = authTokenCloned.getUserId();
        String cryptedUserID = cryptoInstance.encrypt(origUserID);
        authTokenCloned.setUserId(cryptedUserID);

        if(authTokenCloned.getAuthToken() != null) {
            String origAuthToken = authTokenCloned.getAuthToken();
            String cryptedAuthToken = cryptoInstance.encrypt(origAuthToken);
            authTokenCloned.setAuthToken(cryptedAuthToken);
        }
        
        ServiceRequest request = new ServiceRequest();
        request.setUserAuthToken(authTokenCloned);
        request.setServiceType(serviceType);
        signRequest(request);
        return request;
    }
    
    public static ServiceRequest prepareRequestForTripWS(
            final Trip trip,
            TripSearchCriteria searchCriteria,
            ServiceTypeEnum serviceType,
            int offset,
            int numOfRows) 
            throws Exception {
        ServiceRequest request = new ServiceRequest();
        if(serviceType == ServiceTypeEnum.TRIP_SEARCH) {
            request.setTripSearchCriteria(searchCriteria);
            request.setServiceType(serviceType);
            
        } else {
            request.setTrip(trip);
            request.setServiceType(serviceType);
            request.setOffset(offset);
            request.setNumOfRows(numOfRows);
        }

        signRequest(request);
        return request;
    }    

    public static ServiceRequest prepareRequestForPlaceWS(
            final Place place,
            ServiceTypeEnum serviceType) 
            throws Exception {
        ServiceRequest request = new ServiceRequest();
        request.setPlace(place);
        request.setServiceType(serviceType);
        signRequest(request);
        return request;
    }    

    public static ServiceRequest prepareRequestForPictureWS(
            final Picture picture,
            ServiceTypeEnum serviceType,
            OrderByEnum orderBy,
            int offset, 
            int numOfRows) 
            throws Exception {
        ServiceRequest request = new ServiceRequest();
        request.setPicture(picture);
        request.setServiceType(serviceType);
        request.setOrderBy(orderBy);
        request.setOffset(offset);
        request.setNumOfRows(numOfRows);
        signRequest(request);
        return request;
    }
    
    public static ServiceRequest prepareRequestForFavoriteWS(
            final Favorite favorite,
            final Trip trip,
            final User user,
            ServiceTypeEnum serviceType,
            OrderByEnum orderBy,
            int offset, int numOfRows) 
            throws Exception {
        ServiceRequest request = new ServiceRequest();
        if(serviceType == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_TRIP) {
            request.setTrip(trip);
        } else if(serviceType == ServiceTypeEnum.FAVORITE_RETRIEVE_BY_USER) {
            request.setUser(user);
        } else {
            request.setFavorite(favorite);
        }
        request.setServiceType(serviceType);
        request.setOrderBy(orderBy);
        request.setOffset(offset);
        request.setNumOfRows(numOfRows);
        signRequest(request);
        return request;
    }
    
    public static ServiceRequest prepareRequestForCommentWS(
            Comment comment,
            ServiceTypeEnum serviceType,
            OrderByEnum orderBy,
            int offset, int numOfRows) 
            throws Exception {
        ServiceRequest request = new ServiceRequest();
        request.setComment(comment);
        request.setServiceType(serviceType);
        request.setOrderBy(orderBy);
        request.setOffset(offset);
        request.setNumOfRows(numOfRows);
        signRequest(request);
        return request;
    }    
    
    private static void signRequest(ServiceRequest request) throws Exception {
        DigitalSignature digitalSign = new DigitalSignature();
        String signedMessage = String.valueOf(System.currentTimeMillis());
        digitalSign.setSignedMessage(signedMessage);

        Cryptographer cryptoInstance = Cryptographer.getInstance();
        String signature = cryptoInstance.sign(signedMessage);
        digitalSign.setSignature(signature);

        request.setDigitalSignature(digitalSign);
    }

}
