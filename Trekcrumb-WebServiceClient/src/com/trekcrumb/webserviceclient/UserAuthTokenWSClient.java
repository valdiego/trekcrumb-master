package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class UserAuthTokenWSClient {

    public static ServiceResponse authenticate(final UserAuthToken userAuthToken) {
        return sendRequestAndGetResponse(userAuthToken, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
    }
    
    public static ServiceResponse delete(final UserAuthToken userAuthToken, ServiceTypeEnum deleteBy) {
        return sendRequestAndGetResponse(userAuthToken, deleteBy);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            UserAuthToken userAuthToken,
            ServiceTypeEnum serviceType) {
        ServiceRequest request = null;
        ServiceResponse result = null;

        try {
            request = RequestUtil.prepareRequestForUserAuthTokenWS(userAuthToken, serviceType);
            System.out.println("Request to send: " + request);
        } catch(Exception e) {
           e.printStackTrace();
           return CommonUtil.composeServiceResponseError(
                   serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                   "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectUserAuthTokenWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
