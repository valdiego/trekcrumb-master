package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class PlaceWSClient {
    public static ServiceResponse create(final Place placeToCreate) {
        return sendRequestAndGetResponse(placeToCreate, ServiceTypeEnum.PLACE_CREATE);
    }
    
    public static ServiceResponse retrieve(final Place placeToRetrieve, ServiceTypeEnum retrieveBy) {
        return sendRequestAndGetResponse(placeToRetrieve, retrieveBy);
    }

    public static ServiceResponse delete(final Place placeToDelete, ServiceTypeEnum deleteBy) {
        return sendRequestAndGetResponse(placeToDelete, deleteBy);
    }

    public static ServiceResponse update(final Place placeToUpdate) {
        return sendRequestAndGetResponse(placeToUpdate, ServiceTypeEnum.PLACE_UPDATE);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            Place place, 
            ServiceTypeEnum serviceType) {
        ServiceRequest request = null;
        ServiceResponse result = null;
        try {
            request = RequestUtil.prepareRequestForPlaceWS(place, serviceType);
            System.out.println("Request to send: " + request);
        } catch(Exception e) {
           e.printStackTrace();
           return CommonUtil.composeServiceResponseError(
                   serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                   "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectPlaceWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
