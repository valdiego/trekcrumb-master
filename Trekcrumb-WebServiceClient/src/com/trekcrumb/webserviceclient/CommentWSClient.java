package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class CommentWSClient {
    public static ServiceResponse create(final Comment comment) {
        return sendRequestAndGetResponse(comment, ServiceTypeEnum.COMMENT_CREATE, null, -1, -1);
    }
    
    public static ServiceResponse retrieve(
            final Comment comment,
            ServiceTypeEnum retrieveBy,
            OrderByEnum orderBy, 
            int offset, int numOfRows) {
        return sendRequestAndGetResponse(comment, retrieveBy, orderBy, offset, numOfRows);
    }

    public static ServiceResponse delete(
            final Comment comment,
            ServiceTypeEnum deleteBy) {
        return sendRequestAndGetResponse(comment, deleteBy, null, -1, -1);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            Comment comment,
            ServiceTypeEnum serviceType,
            OrderByEnum orderBy, 
            int offset, int numOfRows) {
        ServiceRequest request = null;
        ServiceResponse result = null;
        try {
            request = RequestUtil.prepareRequestForCommentWS(comment, serviceType, orderBy, offset, numOfRows);
        } catch(Exception e) {
           e.printStackTrace();
           return CommonUtil.composeServiceResponseError(
                   serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                   "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectCommentWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
