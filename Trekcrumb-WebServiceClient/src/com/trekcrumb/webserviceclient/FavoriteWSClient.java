package com.trekcrumb.webserviceclient;

import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * A client to connect to and procsess response from WebService.
 * @author Val Triadi
 *
 */
public class FavoriteWSClient {
    public static ServiceResponse create(final Favorite faveToCreate) {
        return sendRequestAndGetResponse(faveToCreate, null, null, ServiceTypeEnum.FAVORITE_CREATE, null, -1, -1);
    }
    
    public static ServiceResponse retrieve(
            final Favorite faveToRetrieve, 
            OrderByEnum orderBy, 
            int offset, int numOfRows) {
        return sendRequestAndGetResponse(faveToRetrieve, null, null, ServiceTypeEnum.FAVORITE_RETRIEVE, orderBy, offset, numOfRows);
    }

    public static ServiceResponse retrieveByTrip(
            final Trip trip, 
            OrderByEnum orderBy, 
            int offset, int numOfRows) {
        return sendRequestAndGetResponse(null, trip, null, ServiceTypeEnum.FAVORITE_RETRIEVE_BY_TRIP, orderBy, offset, numOfRows);
    }

    public static ServiceResponse retrieveByUser(
            final User user, 
            OrderByEnum orderBy, 
            int offset, int numOfRows) {
        return sendRequestAndGetResponse(null, null, user, ServiceTypeEnum.FAVORITE_RETRIEVE_BY_USER, orderBy, offset, numOfRows);
    }

    public static ServiceResponse delete(final Favorite faveToDelete) {
        return sendRequestAndGetResponse(faveToDelete, null, null, ServiceTypeEnum.FAVORITE_DELETE, null, -1, -1);
    }

    private static ServiceResponse sendRequestAndGetResponse(
            Favorite favorite,
            Trip trip, 
            User user,
            ServiceTypeEnum serviceType,
            OrderByEnum orderBy, 
            int offset, int numOfRows) {
        ServiceRequest request = null;
        ServiceResponse result = null;
        try {
            request = RequestUtil.prepareRequestForFavoriteWS(favorite, trip, user, serviceType, orderBy, offset, numOfRows);
        } catch(Exception e) {
           e.printStackTrace();
           return CommonUtil.composeServiceResponseError(
                   serviceType, ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                   "Error when preparing service request: " + e.getMessage());
        }
        
        result = ConnectRestfulWSUtil.connectFavoriteWS(request);
        if(result != null && result.isSuccess()) {
            System.out.println("Response received: " + result);
        } else {
            System.err.println("Response received: " + result);
        }
        return result;        
    }    

}
