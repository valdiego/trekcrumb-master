package testperf.com.trekcrumb;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    testperf.com.trekcrumb.common.crypto.CryptographerPerfTest.class,

    testperf.com.trekcrumb.business.dao.impl.hibernate.UserDAOHibernateImplPerfTest.class,
    testperf.com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImplPerfTest.class,
    
    testperf.com.trekcrumb.business.manager.UserManagerPerfTest.class,
    testperf.com.trekcrumb.business.manager.UserAuthTokenManagerPerfTest.class,
    testperf.com.trekcrumb.business.manager.TripManagerPerfTest.class,

})
public class PerfTestSuites {}
