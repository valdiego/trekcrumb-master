package testperf.com.trekcrumb.business.manager;

import junit.extensions.RepeatedTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;

import test.com.trekcrumb.perftestsingle.TripManagerPerfTestSingle;
import testperf.com.trekcrumb.utility.PerfTestUtils;

import com.clarkware.junitperf.ConstantTimer;
import com.clarkware.junitperf.LoadTest;
import com.clarkware.junitperf.Timer;

/**
 * Performance tests for the given test cases.
 * Terms:
 * 1. TimedTest - Runs a given test case with maximum elapsed time specified and will fail if it exceeds
 *                the given elapsed time.
 * 2. LoadTestSequential - Runs a given test case with multiple iterations sequentially one after another
 *                         without delay.
 * 3. LoadTestConcurrent - Runs a given test case with multiple thread simultaneously at the same time
 *                         WITH delay between thread. (Note: The delay is necessary to avoid conflict
 *                         between unique IDs being inserted into DB).
 * 4. LoadTestSequentialAndConcurrent - Runs a given test case in combo of multiple iterations and threads
 * 5. LoadAndTimedTest - A combo timed and load test where it runs a given test case with multiple
 *                       iterations and/or threads, and a maximum elapsed time is specified. Test 
 *                       will fail if it exceeds the given elapsed time.
 * 
 * @author Val Triadi
 * @since 10/2014 
 */
public class TripManagerPerfTest extends TestCase {
        
    public static void testCreateRetrieveAndDelete_ByTripID_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new TripManagerPerfTestSingle("testCreateRetrieveAndDelete_ByTripID");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testCreateRetrieveAndDelete_ByTripID_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testCreateRetrieveAndDelete_ByTripID_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new TripManagerPerfTestSingle("testCreateRetrieveAndDelete_ByTripID");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testRetrieve_ByTripID_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testCreate_WithPlace_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new TripManagerPerfTestSingle("testCreate_WithPlace");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testCreate_WithPlace_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testCreate_WithPlace_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new TripManagerPerfTestSingle("testCreate_WithPlace");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testCreate_WithPlace_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testRetrieve_ByUser_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new TripManagerPerfTestSingle("testRetrieve_ByUser");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testRetrieve_ByUser_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testRetrieve_ByUser_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new TripManagerPerfTestSingle("testRetrieve_ByUser");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testRetrieve_ByUser_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }    
    
    public static void testUpdate_ALL_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new TripManagerPerfTestSingle("testUpdate_ALL");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testUpdate_ALL_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testUpdate_ALL_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new TripManagerPerfTestSingle("testUpdate_ALL");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testUpdate_ALL_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testPublish_WithPlaceAndPicture_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new TripManagerPerfTestSingle("testPublish_WithPlaceAndPicture");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testPublish_WithPlaceAndPicture_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testPublish_WithPlaceAndPicture_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new TripManagerPerfTestSingle("testPublish_WithPlaceAndPicture");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testPublish_WithPlaceAndPicture_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }      
    
    public static void testSearchAndCount_Default_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new TripManagerPerfTestSingle("testSearchAndCount_Default");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testSearchAndCount_Default_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testSearchAndCount_Default_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new TripManagerPerfTestSingle("testSearchAndCount_Default");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("TripManagerPerfTest.testSearchAndCount_Default_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }      
    
}
