package testperf.com.trekcrumb.business.manager;

import junit.extensions.RepeatedTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import test.com.trekcrumb.perftestsingle.UserAuthTokenManagerPerfTestSingle;
import testperf.com.trekcrumb.utility.PerfTestUtils;

import com.clarkware.junitperf.ConstantTimer;
import com.clarkware.junitperf.LoadTest;
import com.clarkware.junitperf.Timer;

/**
 * Performance tests for the given test cases.
 * Terms:
 * 1. TimedTest - Runs a given test case with maximum elapsed time specified and will fail if it exceeds
 *                the given elapsed time.
 * 2. LoadTestSequential - Runs a given test case with multiple iterations sequentially one after another
 *                         without delay.
 * 3. LoadTestConcurrent - Runs a given test case with multiple thread simultaneously at the same time
 *                         WITH delay between thread. (Note: The delay is necessary to avoid conflict
 *                         between unique IDs being inserted into DB).
 * 4. LoadTestSequentialAndConcurrent - Runs a given test case in combo of multiple iterations and threads
 * 5. LoadAndTimedTest - A combo timed and load test where it runs a given test case with multiple
 *                       iterations and/or threads, and a maximum elapsed time is specified. Test 
 *                       will fail if it exceeds the given elapsed time.
 * 
 * @author Val Triadi
 * @since 10/2014 
 */
public class UserAuthTokenManagerPerfTest extends TestCase {
        
    public static void testCreateRetrieveAndDelete_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new UserAuthTokenManagerPerfTestSingle("testCreateRetrieveAndDelete");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("UserAuthTokenManagerPerfTest.testCreateRetrieveAndDelete_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testCreateRetrieveAndDelete_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new UserAuthTokenManagerPerfTestSingle("testCreateRetrieveAndDelete");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("UserAuthTokenManagerPerfTest.testCreateRetrieveAndDelete_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testAuthenticate_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new UserAuthTokenManagerPerfTestSingle("testAuthenticate");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("UserAuthTokenManagerPerfTest.testAuthenticate_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

    public static void testAuthenticate_LoadTestConcurrent() {
        /*
         * Delay time (in ms) b/w concurent tests so that all tests do not run at the same time.
         */
        int numOfConcurrents = 100;
        Timer delayTime = new ConstantTimer(100);
        
        Test testCase = new UserAuthTokenManagerPerfTestSingle("testAuthenticate");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("UserAuthTokenManagerPerfTest.testAuthenticate_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }

}
