package testperf.com.trekcrumb.utility;

import java.util.Enumeration;

import junit.framework.Assert;
import junit.framework.TestFailure;
import junit.framework.TestResult;

public class PerfTestUtils {

    public static void verifyTestErrorsOrFailures(final TestResult result) {
        if(result.errorCount() > 0) {
            Enumeration<TestFailure> testErrors = result.errors();
            if(testErrors != null) {
                System.err.println("ERRORS total: " + result.errorCount());
                while(testErrors.hasMoreElements()) {
                    TestFailure testError = testErrors.nextElement();
                    System.err.println("ERROR: " + testError.toString());
                }
            }
        }
        if(result.failureCount() > 0) {
            Enumeration<TestFailure> testFailures = result.failures();
            if(testFailures != null) {
                System.err.println("FAILURES total: " + result.failureCount());
                while(testFailures.hasMoreElements()) {
                    TestFailure testFailure = testFailures.nextElement();
                    System.err.println("FAILURE: " + testFailure.toString());
                }
            }
        }
        
        if(result.errorCount() > 0
                || result.failureCount() > 0) {
            Assert.fail("Perf tests failed with errors [" 
                + result.errorCount() + "] and/or failures [" + result.failureCount() + "]");
        }
    }
    
    
}
