package testperf.com.trekcrumb.common.crypto;

import junit.extensions.RepeatedTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestResult;
import test.com.trekcrumb.perftestsingle.CryptographerPerfTestSingle;
import testperf.com.trekcrumb.utility.PerfTestUtils;

import com.clarkware.junitperf.ConstantTimer;
import com.clarkware.junitperf.LoadTest;
import com.clarkware.junitperf.Timer;

/**
 * Performance tests for the given test cases.
 * Terms:
 * 1. TimedTest - Runs a given test case with maximum elapsed time specified and will fail if it exceeds
 *                the given elapsed time.
 * 2. LoadTestSequential - Runs a given test case with multiple iterations sequentially one after another
 *                         without delay.
 * 3. LoadTestConcurrent - Runs a given test case with multiple thread simultaneously at the same time
 *                         WITH delay between thread. (Note: The delay is necessary to avoid conflict
 *                         between unique IDs being inserted into DB).
 * 4. LoadTestSequentialAndConcurrent - Runs a given test case in combo of multiple iterations and threads
 * 5. LoadAndTimedTest - A combo timed and load test where it runs a given test case with multiple
 *                       iterations and/or threads, and a maximum elapsed time is specified. Test 
 *                       will fail if it exceeds the given elapsed time.
 * 
 * @author Val Triadi
 * @since 10/2014 
 */
public class CryptographerPerfTest extends TestCase {

    public static void testEncrypt_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new CryptographerPerfTestSingle("testEncrypt");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testEncrypt_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testEncrypt_LoadTestConcurrent() {
        int numOfConcurrents = 200;
        Timer delayTime = new ConstantTimer(0);
        
        Test testCase = new CryptographerPerfTestSingle("testEncrypt");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testEncrypt_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testDecrypt_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new CryptographerPerfTestSingle("testDecrypt");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testDecrypt_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testDecrypt_LoadTestConcurrent() {
        int numOfConcurrents = 200;
        Timer delayTime = new ConstantTimer(0);
        
        Test testCase = new CryptographerPerfTestSingle("testDecrypt");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testDecrypt_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testSignAndVerifySign_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new CryptographerPerfTestSingle("testSignAndVerifySign");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testSignAndVerifySign_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testSignAndVerifySign_LoadTestConcurrent() {
        int numOfConcurrents = 200;
        Timer delayTime = new ConstantTimer(0);
        
        Test testCase = new CryptographerPerfTestSingle("testSignAndVerifySign");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testSignAndVerifySign_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testSaltAndHashThenValidate_LoadTestSequential() {
        int numOfConcurrents = 1;
        int numOfIterations = 200;
        
        Test testCase = new CryptographerPerfTestSingle("testSaltAndHashThenValidate");
        Test repeatedTest = new RepeatedTest(testCase, numOfIterations);
        Test loadTest = new LoadTest(repeatedTest, numOfConcurrents);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testSaltAndHashThenValidate_LoadTestSequential() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfIterations, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
    public static void testSaltAndHashThenValidate_LoadTestConcurrent() {
        int numOfConcurrents = 200;
        Timer delayTime = new ConstantTimer(0);
        
        Test testCase = new CryptographerPerfTestSingle("testSaltAndHashThenValidate");
        Test loadTest = new LoadTest(testCase, numOfConcurrents, delayTime);
        
        TestResult result = new TestResult();
        loadTest.run(result);
        System.out.println("CryptographerPerfTest.testSaltAndHashThenValidate_LoadTestConcurrent() - COMPLETED. Tests run: " + result.runCount());
        assertEquals("Invalid number of tests run!", numOfConcurrents, result.runCount());
        PerfTestUtils.verifyTestErrorsOrFailures(result);
    }
    
}
