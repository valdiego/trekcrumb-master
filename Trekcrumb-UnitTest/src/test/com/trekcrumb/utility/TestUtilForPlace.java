package test.com.trekcrumb.utility;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;

import com.trekcrumb.business.dao.IPlaceDAO;
import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.PlaceDAOHibernateImpl;
import com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQuery;
import com.trekcrumb.business.manager.PlaceManager;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.PlaceWSClient;

public class TestUtilForPlace {

    public static void placeCreate_byDB(
            String userID, String tripID, double latitude, double longitude, 
            String placeNote, String placeLocation) {
        Place placeToCreate = new Place(latitude, longitude);
        placeToCreate.setId(CommonUtil.generateID("TESTID"));
        placeToCreate.setUserID(userID);
        placeToCreate.setTripID(tripID);
        placeToCreate.setNote(placeNote);
        placeToCreate.setLocation(placeLocation);
        String currentDate = DateUtil.getCurrentTimeWithGMTTimezone();
        placeToCreate.setCreated(currentDate);
        placeToCreate.setUpdated(currentDate);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            PlaceDBQuery.create(session, null, placeToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Place Create: ERROR - " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    public static Place placeCreate_byDAOImpl(
            String userID, String tripID, double latitude, double longitude, 
            String placeNote, String placeLocation,
            boolean isValidateResult) {
        String id = CommonUtil.generateID("TESTID");
        Place placeToCreate = new Place(latitude, longitude);
        placeToCreate.setId(id);
        placeToCreate.setUserID(userID);
        placeToCreate.setTripID(tripID);
        placeToCreate.setNote(placeNote);
        placeToCreate.setLocation(placeLocation);
        String currentDate = DateUtil.getCurrentTimeWithGMTTimezone();
        placeToCreate.setCreated(currentDate);
        placeToCreate.setUpdated(currentDate);
        
        Place placeCreated =  null;
        try {
            IPlaceDAO daoImpl = new PlaceDAOHibernateImpl();
            placeCreated = daoImpl.create(placeToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Place Create: ERROR - " + e.getMessage());
        }
        
        if(isValidateResult) {
            validatePlace(placeToCreate, placeCreated);
            Assert.assertEquals("Place Create: Incorrect Place ID!", id, placeCreated.getId());
        }
        
        return placeCreated;
    }    
    
    public static Place placeCreate_byManager(
            String userID, String tripID, double latitude, double longitude, 
            String placeNote, String placeLocation,
            boolean isValidateResult) {
        Place placeToCreate = new Place(latitude, longitude);
        placeToCreate.setUserID(userID);
        placeToCreate.setTripID(tripID);
        placeToCreate.setNote(placeNote);
        placeToCreate.setLocation(placeLocation);

        Place placeCreated =  null;
        try {
            placeCreated = PlaceManager.create(placeToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Place Create: ERROR - " + e.getMessage());
        }
        
        if(isValidateResult) {
            validatePlace(placeToCreate, placeCreated);
        }

        return placeCreated;
    }    
    
    public static Place placeCreate_byManager_DefaultData(User user, Trip trip) throws Exception {
        return placeCreate_byManager(
                user.getUserId(), trip.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, 
                TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, 
                TestConstants.UNITTEST_PLACE_LOCATION,
                false); 
    }

    public static Place placeCreate_byWSClient(
            String userID, String tripID, double latitude, double longitude, 
            String placeNote, String placeLocation,
            boolean isValidateResult) {
        Place placeToCreate = new Place(latitude, longitude);
        placeToCreate.setUserID(userID);
        placeToCreate.setTripID(tripID);
        placeToCreate.setNote(placeNote);
        placeToCreate.setLocation(placeLocation);

        ServiceResponse result = PlaceWSClient.create(placeToCreate); 
        Assert.assertNotNull("Place create: Returned result can NOT be null!", result);
        Assert.assertTrue("Place create: Result should have been SUCCESS!\n" + result, result.isSuccess());
        if(result.getListOfPlaces() == null || result.getListOfPlaces().size() == 0) {
            Assert.fail("Place create: Result should have had the created Place!");
        }
        Place placeCreated = result.getListOfPlaces().get(0);
        
        if(isValidateResult) {
            validatePlace(placeToCreate, placeCreated);
        }
        
        return placeCreated;
    }    
    
    public static List<Place> placeRetrieve_byWSClient(
            Place placeToRetrieve,
            ServiceTypeEnum retrieveBy) {
        ServiceResponse result = PlaceWSClient.retrieve(placeToRetrieve, retrieveBy);
        Assert.assertNotNull("Place Retrieve: Returned result can NOT be null!", result);
        Assert.assertTrue("Place Retrieve: Result should have been SUCCESS!\n" + result, result.isSuccess());
        return result.getListOfPlaces();
    }
    
    public static Place placeUpdate_byWSClient(Place placeToUpdate) {
        ServiceResponse result = PlaceWSClient.update(placeToUpdate);
        Assert.assertNotNull("Place update: Returned result can NOT be null!", result);
        Assert.assertTrue("Place update: Result should have been SUCCESS!", result.isSuccess());
        if(result.getListOfPlaces() == null || result.getListOfPlaces().size() == 0) {
            Assert.fail("Place update: Result should have had the updated Place!");
        }
        return result.getListOfPlaces().get(0);
    }

    public static int placeDelete_byDB_ALLPlaces(String userID) {
        int deletedItem = 0;
        if(ValidationUtil.isEmpty(userID)) {
            return deletedItem;
        }
        Place placeToDeleteAll = new Place();
        placeToDeleteAll.setUserID(userID);
        Session session = null;
        try {
            session = HibernateManager.openSession();
            deletedItem = PlaceDBQuery.delete(session, null, placeToDeleteAll, ServiceTypeEnum.PLACE_DELETE_BY_USERID);
        } catch(Exception e) {
            System.err.println("Place Delete: ERROR - " + e.getMessage());
            e.printStackTrace();
        }
        return deletedItem;
    }
    
    public static void validatePlace(Place placeExpected, Place placeActual) {
        Assert.assertNotNull("Place Validation: Returned Place should NOT be null!", placeActual);
        Assert.assertNotNull("Place Validation: Returned Place should have its ID!", placeActual.getId());
        Assert.assertEquals("Place Validation: Incorrect user ID!", placeExpected.getUserID(), placeActual.getUserID());
        Assert.assertEquals("Place Validation: Incorrect Trip ID!", placeExpected.getTripID(), placeActual.getTripID());
        Assert.assertEquals("Place Validation: Incorrect latitude!", placeExpected.getLatitude(), placeActual.getLatitude());
        Assert.assertEquals("Place Validation: Incorrect longitude!", placeExpected.getLongitude(), placeActual.getLongitude());
        Assert.assertEquals("Place Validation: Incorrect location!", placeExpected.getLocation(), placeActual.getLocation());
        Assert.assertEquals("Place Validation: Incorrect note!", placeExpected.getNote(), placeActual.getNote());

        if(placeExpected.getCreated() != null) {
            Assert.assertEquals("Place Validation: Incorrect created date!", placeExpected.getCreated(), placeActual.getCreated());
        } else {
            Assert.assertNotNull("Place Validation: Created date should not NULL!", placeActual.getCreated());
        }
    }

}
