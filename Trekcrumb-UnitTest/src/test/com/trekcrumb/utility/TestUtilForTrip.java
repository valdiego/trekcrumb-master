package test.com.trekcrumb.utility;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;

import com.trekcrumb.business.dao.ITripDAO;
import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImpl;
import com.trekcrumb.business.dao.impl.hibernate.query.TripDBQuery;
import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.TripWSClient;

public class TestUtilForTrip {
    
    public static void tripCreate_byDB(
            String userID, String tripName, String tripNote, String tripLocation,
            TripStatusEnum status, TripPrivacyEnum privacy, TripPublishEnum publish,
            boolean isCopyFromLocalDevice) 
            throws Exception {
        Trip tripToCreate = new Trip();
        tripToCreate.setId(CommonUtil.generateID("TESTID_"));
        tripToCreate.setUserID(userID);
        tripToCreate.setName(tripName);
        tripToCreate.setNote(tripNote);
        tripToCreate.setLocation(tripLocation);
        tripToCreate.setStatus(status);
        tripToCreate.setPrivacy(privacy);
        tripToCreate.setPublish(publish);
        
        if(!isCopyFromLocalDevice) {
            tripToCreate.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
        }
        tripToCreate.setUpdated(tripToCreate.getCreated());
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            TripDBQuery.create(session, null, tripToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("tripCreateByTripDB FAILED: " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    public static int tripCreate_byDB_ManyTrips(String userID) throws Exception {
        /*
         * NOTE:
         * 1. Be aware, default search (without other filters) will retrieve ALL trips in the database 
         *    including existing non-test trips with default order CREATED desc. 
         *    Therefore, testing offset and num of rows could be tricky.
         * 2. There is time gaps (Thread.sleep) between creating test trips to enforce 
         *    diferent created dates for each trip due to search retrieves Trips order by CREATED date
         * 3. There are specific trips at specific index we want to identify:
         */
        //Setup data:
        int numOfTrips = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        for(int i = 0; i < numOfTrips; i++) {
            if(i == 0) {
                //1st trip created: The last trip to retrieve
                tripCreate_byDB(
                        userID, 
                        TestConstants.UNITTEST_TRIP_NAME + "_FIRSTTRIP_CREATED", 
                        TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                        TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                        false);
                Thread.sleep(1000);

            } else if(i == (numOfTrips - 1)) {
                //Last trip created: The first trip to retrieve
                Thread.sleep(1000);
                tripCreate_byDB(
                        userID, 
                        TestConstants.UNITTEST_TRIP_NAME + "_LASTTRIP_CREATED", 
                        TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                        TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                        false);
                
            } else {
                tripCreate_byDB(
                        userID, 
                        TestConstants.UNITTEST_TRIP_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                        TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                        false);
            }
        }
        return numOfTrips;
    }
    
    public static Trip tripCreate_byDAOImpl(
            String userID, String tripName, String tripNote, String tripLocation,
            TripStatusEnum status, TripPrivacyEnum privacy, TripPublishEnum publish,
            List<Place> listOfPlaces,
            boolean isValidateResult) {
        String id = CommonUtil.generateID("TESTID_");
        Trip tripToCreate = new Trip();
        tripToCreate.setId(id);
        tripToCreate.setUserID(userID);
        tripToCreate.setName(tripName);
        tripToCreate.setNote(tripNote);
        tripToCreate.setLocation(tripLocation);
        tripToCreate.setStatus(status);
        tripToCreate.setPrivacy(privacy);
        tripToCreate.setPublish(publish);
        tripToCreate.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
        tripToCreate.setUpdated(tripToCreate.getCreated());
        if(listOfPlaces != null) {
            tripToCreate.setListOfPlaces(listOfPlaces);
        }
        
        Trip tripCreated =  null;
        try {
            ITripDAO tripDAOImpl = new TripDAOHibernateImpl();
            tripCreated = tripDAOImpl.create(tripToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("tripCreateByTripDAOHibernateImpl FAILED: " + e.getMessage());
        }
        if(isValidateResult) {
            Assert.assertNotNull("Returned created Trip should NOT be null!", tripCreated);
            Assert.assertNotNull("Returned Trip should have its ID!", tripCreated.getId());
            Assert.assertEquals("Incorrect Trip ID!", id, tripCreated.getId());
            Assert.assertEquals("Incorrect user ID!", userID, tripCreated.getUserID());
            Assert.assertEquals("Incorrect Trip name!", tripName, tripCreated.getName());
            Assert.assertEquals("Incorrect Note!", TestConstants.UNITTEST_TRIP_NOTE, tripCreated.getNote());
            Assert.assertEquals("Incorrect Location!", TestConstants.UNITTEST_TRIP_LOCATION, tripCreated.getLocation());
            Assert.assertEquals("Incorrect Status!", TripStatusEnum.ACTIVE, tripCreated.getStatus());
            Assert.assertEquals("Incorrect Privacy!", TripPrivacyEnum.PRIVATE, tripCreated.getPrivacy());
            Assert.assertEquals("Incorrect Publish value!", TripPublishEnum.PUBLISH, tripCreated.getPublish());
            Assert.assertEquals("Record's created date and updated date should have been the same!", 
                    tripCreated.getCreated(), tripCreated.getUpdated());

        }
        return tripCreated;
    }
    
    public static Trip tripCreate_byManager(
            String userID, String tripName, String tripNote, String tripLocation,
            TripPrivacyEnum privacy,
            List<Place> listOfPlaces,
            boolean isValidateResult)
            throws Exception {
        Trip tripCreated =  null;

        Trip tripToCreate = new Trip();
        tripToCreate.setUserID(userID);
        tripToCreate.setName(tripName);
        tripToCreate.setNote(tripNote);
        tripToCreate.setLocation(tripLocation);
        tripToCreate.setPrivacy(privacy);
        if(listOfPlaces != null) {
            tripToCreate.setListOfPlaces(listOfPlaces);
        }
        
        try {
            tripCreated = TripManager.create(tripToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("tripCreateByManager FAILED: " + e.getMessage());
        }
        if(isValidateResult) {
            validateTrip(tripToCreate, tripCreated);
        }
        return tripCreated;
    }

    public static Trip tripCreate_byManager_DefaultData(User user, String tripName) throws Exception {
        if(ValidationUtil.isEmpty(tripName)) {
            tripName = TestConstants.UNITTEST_TRIP_NAME;
        }
        
        return tripCreate_byManager(
                user.getUserId(), 
                tripName, 
                TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PUBLIC, 
                null, false); 
    }
    
    public static Trip tripCreate_byWSClient(
            String userID, String tripName, String tripNote, String tripLocation,
            TripPrivacyEnum privacy,
            List<Place> listOfPlaces,
            boolean isValidateResult) {
        Trip tripCreated = null;
        
        Trip tripToCreate = new Trip();
        tripToCreate.setUserID(userID);
        tripToCreate.setName(tripName);
        tripToCreate.setNote(tripNote);
        tripToCreate.setLocation(tripLocation);
        tripToCreate.setPrivacy(privacy);
        if(listOfPlaces != null) {
            tripToCreate.setListOfPlaces(listOfPlaces);
        }
        
        ServiceResponse result = TripWSClient.create(tripToCreate);
        Assert.assertNotNull("Trip create: Result can NOT be null!", result);
        Assert.assertTrue("Trip create: Result should have been SUCCESS!\n" + result, result.isSuccess());
        if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
            Assert.fail("Trip create: Result should have newly Trip Created!");
        }

        tripCreated = result.getListOfTrips().get(0);
        if(isValidateResult) {
            validateTrip(tripToCreate, tripCreated);
        }
        
        return tripCreated;
    }
    
    public static int tripDelete_byDB_ALLTrips(String userID) {
        int deletedItem = 0;
        if(ValidationUtil.isEmpty(userID)) {
            return deletedItem;
        }
        Trip tripToDeleteAll = new Trip();
        tripToDeleteAll.setUserID(userID);
        Session session = null;
        try {
            session = HibernateManager.openSession();
            deletedItem = TripDBQuery.delete(session, null, tripToDeleteAll, ServiceTypeEnum.TRIP_DELETE_BY_USERID);
        } catch(Exception e) {
            System.err.println("tripDeleteALLByTripDB FAILED: " + e.getMessage());
            e.printStackTrace();
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
        return deletedItem;
    }
    
    public static List<Trip> tripRetrieve_byWSClient(
            String tripID,
            String userID,
            String username,
            ServiceTypeEnum retrieveBy) {
        List<Trip> tripListFound = null;
        
        //Set all input params regardless null or not:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(tripID);
        tripToRetrieve.setUserID(userID);
        tripToRetrieve.setUsername(username);

        ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, retrieveBy, 0, -1);
        Assert.assertNotNull("Trip Retrieve: Returned result can NOT be null!", result);
        Assert.assertTrue("Trip Retrieve: Result should have been SUCCESS!\n" + result, result.isSuccess());
        tripListFound = result.getListOfTrips();
        return tripListFound;
    }
    
    public static void validateTrip(Trip tripExpected, Trip tripActual) {
        Assert.assertNotNull("Trip Validation: Result Trip should NOT be null!", tripActual);
        Assert.assertNotNull("Trip Validation: Result Trip should have its ID!", tripActual.getId());
        Assert.assertEquals("Trip Validation: Incorrect Publish value!", TripPublishEnum.PUBLISH, tripActual.getPublish());
        
        if(!ValidationUtil.isEmpty(tripExpected.getUserID())) {
            Assert.assertEquals("Trip Validation: Incorrect user ID!", tripExpected.getUserID(), tripActual.getUserID());
        }
        if(!ValidationUtil.isEmpty(tripExpected.getName())) {
            Assert.assertEquals("Trip Validation: Incorrect Trip name!", tripExpected.getName(), tripActual.getName());
        }
        if(!ValidationUtil.isEmpty(tripExpected.getNote())) {
            Assert.assertEquals("Trip Validation: Incorrect Trip note!", tripExpected.getNote(), tripActual.getNote());
        }
        if(tripExpected.getPrivacy() != null) {
            Assert.assertEquals("Trip Validation: Incorrect Privacy!", tripExpected.getPrivacy(), tripActual.getPrivacy());
        }
        if(!ValidationUtil.isEmpty(tripExpected.getLocation())) {
            /*
             * On Trip Create, it is possible to pass empty tripLocation, but the back-end 
             * server will default it to specific default value
             */
            Assert.assertEquals("Trip Validation: Incorrect Location!", tripExpected.getLocation(), tripActual.getLocation());
        }
        
        if(tripExpected.getListOfPlaces() != null) {
            //Verify Place data:
            Place placeExpected = tripExpected.getListOfPlaces().get(0);
            
            Assert.assertNotNull("Trip Validation: Result Trip should have a Place!", tripActual.getListOfPlaces());
            Assert.assertTrue("Trip Validation: Result Trip should have a Place!", tripActual.getListOfPlaces().size() > 0);
            Assert.assertNotNull("Trip Validation: Result Trip should have a Place!", tripActual.getListOfPlaces().get(0));
            Place placeActual = tripActual.getListOfPlaces().get(0);
            Assert.assertNotNull("Trip Validation: Result Place should have an ID!", placeActual.getId());
            Assert.assertEquals("Trip Validation: Incorrect Place latitude!", placeExpected.getLatitude(), placeActual.getLatitude());
            Assert.assertEquals("Trip Validation: Incorrect Place longitude!", placeExpected.getLongitude(), placeActual.getLongitude());
            Assert.assertEquals("Trip Validation: Incorrect Place note!", placeExpected.getNote(), placeActual.getNote());
        }
    }
    
    public static Trip composeDummyTripUnpublished(String userID, String tripName) {
        //Dummy Non-Publish Trip (has ID):
        Trip tripToPublish = new Trip();

        String tripID = CommonUtil.generateID(userID);
        tripToPublish.setId(tripID);
        tripToPublish.setUserID(userID);
        
        if(tripName == null) {
            tripName = TestConstants.UNITTEST_TRIP_NAME;
        }
        
        tripToPublish.setName(tripName + "_NotPublished");
        tripToPublish.setNote(TestConstants.UNITTEST_TRIP_NOTE);
        tripToPublish.setLocation( TestConstants.UNITTEST_TRIP_LOCATION);
        tripToPublish.setStatus(TripStatusEnum.ACTIVE);
        tripToPublish.setPrivacy(TripPrivacyEnum.PUBLIC);
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        tripToPublish.setCreated(createdDate);
        tripToPublish.setUpdated(createdDate);

        return tripToPublish;
    }
    
}
