package test.com.trekcrumb.utility;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.business.dao.IUserDAO;
import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.UserDAOHibernateImpl;
import com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery;
import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.UserWSClient;

public class TestUtilForUser {
    
    public static void userCreate_byDB(
            String email, 
            String username,
            String fullname) {
        User userToCreate = new User();
        userToCreate.setUserId(CommonConstants.STRING_VALUE_EMPTY);
        userToCreate.setEmail(email);
        userToCreate.setUsername(username);
        userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
        userToCreate.setFullname(fullname);
        userToCreate.setActive(YesNoEnum.YES);
        userToCreate.setConfirmed(YesNoEnum.NO);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            UserDBQuery.create(session, transaction, userToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("userCreateByUserDBQuery FAILED: " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    public static User userCreate_byDAOImpl(
            String email, 
            String username,
            String fullname,
            boolean isValidateResult) {
        User userNew = new User();
        userNew.setUserId(CommonConstants.STRING_VALUE_EMPTY);
        userNew.setEmail(email);
        userNew.setUsername(username);
        userNew.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
        userNew.setFullname(fullname);
        userNew.setActive(YesNoEnum.YES);
        userNew.setConfirmed(YesNoEnum.NO);
        
        User userCreated = null;
        IUserDAO daoUserImpl = new UserDAOHibernateImpl();
        try {
            userCreated = daoUserImpl.create(userNew);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("userCreateByUserDAOHibernateImpl FAILED: " + e.getMessage());
        }
        if(isValidateResult) {
            Assert.assertNotNull("User Create: Returned User bean is NULL!", userCreated);
            Assert.assertNotNull("User Create: Returned User bean should have UserID!", userCreated.getUserId());
            Assert.assertEquals("User Create: Invalid username!", username.toUpperCase(), userCreated.getUsername());
            Assert.assertEquals("User Create: Invalid email!", email, userCreated.getEmail());
            Assert.assertEquals("User Create: Invalid fullname!", fullname, userCreated.getFullname());
            Assert.assertNotNull("User Create: Returned Password should NOT null!", userCreated.getPassword());
        }
        return userCreated;
    }
    
    public static User userCreate_byManager(
            String email, 
            String username, 
            String fullname,
            boolean isValidateResult) {
        User userNewRequest = new User();
        userNewRequest.setEmail(email);
        userNewRequest.setUsername(username);
        userNewRequest.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
        userNewRequest.setFullname(fullname);
        
        User userCreated = null;
        try {
            userCreated = UserManager.userCreate(userNewRequest);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("User Create: Unexpected error - " + e.getMessage());
        }

        if(isValidateResult) {
            Assert.assertNotNull("Create User via manager error: Returned User bean is NULL!", userCreated);
            Assert.assertNotNull("Create User via manager error: Returned User bean should have UserID!", userCreated.getUserId());
            Assert.assertTrue("User Create: Incorrect username! Expected [" + username + "] but [" + userCreated.getUsername() + "]", 
                    username.equalsIgnoreCase(userCreated.getUsername()));
            Assert.assertTrue("User Create: Incorrect email! Expected [" + email + "] but [" + userCreated.getEmail() + "]", 
                    email.equalsIgnoreCase(userCreated.getEmail()));
            
            validateFullname(fullname, userCreated.getFullname());
            
            //Private-Secure data:
            Assert.assertNull("User Create: Returned Password should have been NULL!", userCreated.getPassword());
        }
        return userCreated;
    }

    public static User userCreate_byManager_ReadyForLogin(
            String email, 
            String username, 
            String fullname) {
        User userCreated = userCreate_byManager(email, username, fullname, false);
        try {
            User userConfirm = new User();
            userConfirm.setUsername(username);
            userConfirm.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            boolean confirmResult = UserManager.userConfirm(userConfirm);
            Assert.assertTrue("userCreateAndReadyForLoginByUserManager FAILED since user confirm failed!", confirmResult);
        } catch(Exception e) {
            e.printStackTrace();
            userDelete_byDB(userCreated);
            Assert.fail("userCreateAndReadyForLoginByUserManager: " + e.getMessage());
        }
        return userCreated;
    }    
    
    public static User userCreate_byManager_DefaultData(String userFullname) throws Exception {
        String username = TestConstants.UNITTEST_USERNAME_PREFIX + String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        return userCreate_byManager_ReadyForLogin(email, username, userFullname);
    }

    public static User userRead_byDB(String username) {
        User userRead = null;
        
        Session session = null;
        Transaction transaction = null;
        try {
            User userToReadByUsername = new User();
            userToReadByUsername.setUsername(username);

            session = HibernateManager.openSession();
            List<User> usersReadList = UserDBQuery.read(session, transaction, userToReadByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            if(usersReadList != null) {
                userRead = usersReadList.get(0);
            }
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("userReadByUserDB FAILED: " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
        return userRead;
    }
    
    public static int userDelete_byDB(User user) {
        int deletedItem = -1;
        if(user == null) {
            return deletedItem;
        }
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            if(!ValidationUtil.isEmpty(user.getUserId())) {
                deletedItem = UserDBQuery.delete(session, transaction, user, ServiceTypeEnum.USER_DELETE_BY_USERID);
            } else if(!ValidationUtil.isEmpty(user.getUsername())) {
                deletedItem = UserDBQuery.delete(session, transaction, user, ServiceTypeEnum.USER_DELETE_BY_USERNAME);
            }
        } catch(Exception e) {
            System.err.println("userDeleteByUserDBQuery FAILED: " + e.getMessage());
            e.printStackTrace();
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
        return deletedItem;
    }
    
    public static void userDelete_byDB_ALLData(User user) {
        if(user == null || user.getUserId() == null) {
            //Do NOT fail because some tests may have empty User to delete!
            System.err.println("userDeleteALLData() - ERROR: UserID is required!");
            return;
        }
        
        TestUtilForUserAuthToken.userAuthTokenDelete_byDB(user.getUserId());
        TestUtilForComment.commentDeletebyCommentDB(user.getUserId());
        TestUtilForFavorite.faveDeleteALLbyFavoriteDB(user.getUserId());
        TestUtilForPlace.placeDelete_byDB_ALLPlaces(user.getUserId());
        TestUtilForPicture.pictureDelete_byDB_ALLPictures(user.getUserId());
        TestUtilForTrip.tripDelete_byDB_ALLTrips(user.getUserId());
        userDelete_byDB(user);
    }
    
    /**
     * Register User Forget. Behind the scene, the business component only sends email 
     * BUT does not return anything. We may need to retrieve the user directly from DB 
     * to return Security Token.
     */
    public static String userForgetRegister_byManager(String username, String email) throws Exception {
        User userWhoForget = new User();
        userWhoForget.setEmail(email);
        UserManager.userPasswordForget(userWhoForget);

        //Retrieve and verify Security Token:
        User userFound = TestUtilForUser.userRead_byDB(username);
        Assert.assertNotNull("User Password Forget: Should have found user in DB given username [" + username + "]!", userFound);
        String securityToken = userFound.getSecurityToken();        
        Assert.assertNotNull("User Password Forget: Returned user SecurityToken should NOT be null!", securityToken);
        return securityToken;
    }
    
    public static String userForgetRegister_byWSClient(String username, String email) throws Exception {
        User userWhoForget = new User();
        userWhoForget.setEmail(email);
        ServiceResponse result = UserWSClient.userPasswordForget(userWhoForget);
        Assert.assertNotNull("User Password Forget: Result should NOT be null!", result);
        Assert.assertTrue("User Password Forget: Result should have been a SUCCESS! \n" + result, result.isSuccess());
        
        //Retrieve and verify Security Token:
        User userFound = TestUtilForUser.userRead_byDB(username);
        Assert.assertNotNull("User Password Forget: Should have found user in DB given username [" + username + "]!", userFound);
        String securityToken = userFound.getSecurityToken();        
        Assert.assertNotNull("User Password Forget: Returned user SecurityToken should NOT be null!", securityToken);
        return securityToken;
    }
    
    public static void validateUserSecureData(User user, boolean isUserLogin) {
        Assert.assertNull("validateUserSecureData - User password should have been secured (empty)!", user.getPassword());
        Assert.assertNull("validateUserSecureData - User security token should have been secured (empty)!", user.getSecurityToken());
        if(isUserLogin) {
            if(user.getListOfAuthTokens() != null) {
                for(UserAuthToken userAuthToken : user.getListOfAuthTokens()) {
                    Assert.assertNull("validateUserSecureData - UserAuthToken should have been secured (empty)!", userAuthToken.getAuthToken());
                }
            }
        } else {
            Assert.assertNull("validateUserSecureData - User email should have been secured (empty)!", user.getEmail());
        }
    }
    
    /**
     * Validates user FULLNAME.
     * There are maximum chars limit for fullname to be saved in back-end. In order to compare
     * the original value (expected) vs. the actual value (from back-end), we must consider
     * possibility that the value is TOO LONG, henceforth truncated in the back-end.
     * 
     * @param fullnameExpected - The original value
     * @param fullnameActual - The value returned from back-end
     */
    public static void validateFullname(String fullnameExpected, String fullnameActual) {
        if(ValidationUtil.isEmpty(fullnameExpected)) {
            Assert.fail("The incoming expected Fullname is EMPTY!");
        }
        if(ValidationUtil.isEmpty(fullnameActual)) {
            Assert.fail("The incoming actual Fullname is EMPTY!");
        }
        
        if(fullnameExpected.length() <= CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME) {
            Assert.assertEquals("Fullname is invalid!", fullnameExpected, fullnameActual);
        } else {
            Assert.assertEquals("Fullname is invalid!", 
                    fullnameExpected.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME),
                    fullnameActual);
        }
    }


}
