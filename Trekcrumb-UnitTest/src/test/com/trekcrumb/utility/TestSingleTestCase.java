package test.com.trekcrumb.utility;

import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;

/**
 * Utility to enable running a single test case from a test class on command line.
 * Note:
 * 1. JUnit framework does not support ability to run a single test case (or method) from a test
 *    class. While we can do this easily using IDE such as Eclipse, there is no support to do it
 *    from a script or command line. It would always run entire test cases within the class.
 * 2. This is a util to enable the objective.
 * 3. Usage: 
 *    java -cp <ALL_REQUIRED_CLASSPATH> TestSingleTestCase com.company.test.TestClassName#testMethodName
 * 
 * 4. Ref: http://stackoverflow.com/questions/9288107/run-single-test-from-a-junit-class-using-command-line
 *  
 * @author Val Triadi
 * @since 06/2016
 */
public class TestSingleTestCase {
    
    public static void main(String... args) throws ClassNotFoundException {
        String testClassAndMethodNames = args[0];
        if(testClassAndMethodNames == null || testClassAndMethodNames.trim().length() == 0) {
            throw new ClassNotFoundException("You must specifiy test class name and its test case name!");
        }
        
        String[] classAndMethod = testClassAndMethodNames.split("#");
        Request request = Request.method(Class.forName(classAndMethod[0]), classAndMethod[1]);
        Result result = new JUnitCore().run(request);
        System.exit(result.wasSuccessful() ? 0 : 1);
    }
}
