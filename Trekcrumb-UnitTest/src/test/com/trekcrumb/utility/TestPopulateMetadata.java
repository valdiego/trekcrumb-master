package test.com.trekcrumb.utility;

import org.junit.Test;

import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.TripPrivacyEnum;

/**
 * Special Test cases to populate data into database for peculiar manual tests (UI, performance, etc.).
 * These test cases meant to be executed rarely or only once before it is deleted, or
 * stay in test database environment. 
 * 
 * DO NOT run these tests as regular unit tests.
 */
public class TestPopulateMetadata {
    
    /*
     * Populates a bunch of Trips.
     */
    @Test
    public void testPopulate_tripSearch() throws Exception {
        String userIDWhoOwnsTrip = "fc6db387-c4c9-11e3-b5b9-30f9edef96ec"; //Default user 'TestMetadataUser'
        int numOfTrips = 20;
        for(int i = 0; i < numOfTrips; i++) {
            TestUtilForTrip.tripCreate_byManager(
                    userIDWhoOwnsTrip, 
                    "Trip to Search " + (i+1), 
                    "Populated for Trip Search page", 
                    "Mars",
                    TripPrivacyEnum.PUBLIC, 
                    null, false); 

            //Make it time-sensitive so the order in list always appear consistent:
            Thread.sleep(1000);
        }
    }    

    /*
     * Populates a given Trip with plenty users data who favorite this trip.
     */
    @Test
    public void testPopulate_favoriteByTrip() throws Exception {
        String targetTripID = "d9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428"; //Any trip
        int numOfUsers = 20;
        for(int i = 0; i < numOfUsers; i++) {
            String username = "userForFavoriteByTrip" + (i+1);
            User user = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, "User " + username, false);
            TestUtilForFavorite.faveCreateByFavoriteeDB(targetTripID, user.getUserId());
            
            //Make it time-sensitive so the order in list always appear consistent:
            Thread.sleep(1000);
        }
    }    
    
    /*
     * Populates a given User with plenty trips data whom he favorites.
     */
    @Test
    public void testPopulate_favoriteByUser() throws Exception {
        String userIDWhoOwnsTrip = "fc6db387-c4c9-11e3-b5b9-30f9edef96ec"; //Default user 'TestMetadataUser'
        String userIDWhoFavoritesTrip = "d9228ad9-2074-11e5-b60c-30f9edef96ec"; //Any user
        int numOfTrips = 20;
        for(int i = 0; i < numOfTrips; i++) {
            Trip trip = TestUtilForTrip.tripCreate_byManager(
                    userIDWhoOwnsTrip, 
                    "Trip for FavoriteByUser " + (i+1), 
                    "Populated for user FAVORITES page", 
                    TestConstants.UNITTEST_TRIP_LOCATION,
                    TripPrivacyEnum.PUBLIC, 
                    null, false); 
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip.getId(), userIDWhoFavoritesTrip);

            //Make it time-sensitive so the order in list always appear consistent:
            Thread.sleep(1000);
        }
    }    
    
    /*
     * Populates a given Trip with plenty comments from a single user.
     */
    @Test
    public void testPopulate_comment() throws Exception {
        String targetTripID = "d9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428"; //Any trip
        String targetUserID = "fc6db387-c4c9-11e3-b5b9-30f9edef96ec"; //Default user 'TestMetadataUser'
        int numOfComments = 20;
        for(int i = 0; i < numOfComments; i++) {
            TestUtilForComment.commentCreateByCommentDB(targetTripID, targetUserID, "Hey this is my comment " + (i+1));
            
            //Make it time-sensitive so the order in list always appear consistent:
            Thread.sleep(1000);
        }
    }    
    
    
}
