package test.com.trekcrumb.utility;

import junit.framework.Assert;

import org.hibernate.Session;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQuery;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.utility.ValidationUtil;

public class TestUtilForFavorite {

    public static void faveCreateByFavoriteeDB(String tripID, String userID) {
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(tripID);
        faveToCreate.setUserID(userID);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            FavoriteDBQuery.create(session, null, faveToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("faveCreateByFavoriteeDB FAILED: " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    public static int faveDeleteALLbyFavoriteDB(String userID) {
        int deletedItem = 0;
        if(ValidationUtil.isEmpty(userID)) {
            return deletedItem;
        }
        
        Favorite faveToDelete = new Favorite();
        faveToDelete.setUserID(userID);
        Session session = null;
        try {
            session = HibernateManager.openSession();
            deletedItem = FavoriteDBQuery.delete(session, null, faveToDelete);
        } catch(Exception e) {
            System.err.println("faveDeleteALLbyFavoriteDB FAILED: " + e.getMessage());
            e.printStackTrace();
        }
        return deletedItem;
    }


}
