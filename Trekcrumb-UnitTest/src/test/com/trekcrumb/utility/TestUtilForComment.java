package test.com.trekcrumb.utility;

import junit.framework.Assert;

import org.hibernate.Session;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQuery;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

public class TestUtilForComment {

    public static String commentCreateByCommentDB(String tripID, String userID, String note) {
        Comment commentToCreate = new Comment();
        String commentID = CommonUtil.generateID(tripID);
        commentToCreate.setId(commentID);
        commentToCreate.setTripID(tripID);
        commentToCreate.setUserID(userID);
        commentToCreate.setNote(note);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            CommentDBQuery.create(session, null, commentToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("commentCreateByCommentDB FAILED: " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
        return commentID;
    }
    
    public static int commentDeletebyCommentDB(String userID) {
        int deletedItem = 0;
        Session session = null;
        
        Comment commentToDelete = new Comment();
        commentToDelete.setUserID(userID);
        try {
            session = HibernateManager.openSession();
            deletedItem = CommentDBQuery.delete(session, null, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
        } catch(Exception e) {
            System.err.println("commentDeletebyFavoriteDB FAILED: " + e.getMessage());
            e.printStackTrace();
        }
        return deletedItem;
    }
    



}
