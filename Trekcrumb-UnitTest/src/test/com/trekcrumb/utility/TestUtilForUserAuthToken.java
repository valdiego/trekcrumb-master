package test.com.trekcrumb.utility;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;

import com.trekcrumb.business.dao.IUserAuthTokenDAO;
import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.UserAuthTokenDAOHibernateImpl;
import com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQuery;
import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webserviceclient.UserAuthTokenWSClient;
import com.trekcrumb.webserviceclient.UserWSClient;

public class TestUtilForUserAuthToken {
    
    public static void userAuthTokenCreate_byDB(
            String userID,
            String username,
            String deviceIdentity,
            String authToken) throws Exception {
        UserAuthToken authTokenCreate = new UserAuthToken();
        authTokenCreate.setUserId(userID);
        authTokenCreate.setUsername(username);
        authTokenCreate.setDeviceIdentity(deviceIdentity);
        authTokenCreate.setAuthToken(authToken);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            UserAuthTokenDBQuery.create(session, null, authTokenCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("userAuthTokenCreateByUserAuthTokenDB FAILED: " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }    
    }
    
    public static UserAuthToken userAuthTokenCreate_byDAOImpl(
            String userID,
            String username,
            String deviceIdentity,
            String authToken,
            boolean isValidateResult) {
        UserAuthToken authTokenToCreate = new UserAuthToken();
        authTokenToCreate.setUserId(userID);
        authTokenToCreate.setUsername(username);
        authTokenToCreate.setDeviceIdentity(deviceIdentity);
        authTokenToCreate.setAuthToken(authToken);

        UserAuthToken authTokenCreated = null;
        IUserAuthTokenDAO authTokenImpl = new UserAuthTokenDAOHibernateImpl();
        try {
            authTokenCreated = authTokenImpl.create(authTokenToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("userAuthTokenCreateByUserAuthTokenDAOImpl FAILED: " + e.getMessage());
        }
        if(isValidateResult) {
            Assert.assertNotNull("AuthToken Create: Returned UserAuthToken should NOT NULL!", authTokenCreated);
            Assert.assertNotNull("AuthToken Create: Returned UserAuthToken should have its ID!", authTokenCreated.getId());
            Assert.assertEquals("AuthToken Create: Invalid returned UserId!", userID, authTokenCreated.getUserId());
            Assert.assertEquals("AuthToken Create: Invalid returned Username!", username, authTokenCreated.getUsername());
            Assert.assertEquals("AuthToken Create: Invalid returned Device identity!", deviceIdentity, authTokenCreated.getDeviceIdentity());
            Assert.assertEquals("AuthToken Create: Invalid returned Auth Token!", authToken, authTokenCreated.getAuthToken());
        }
        return authTokenCreated;
    }
    
    public static UserAuthToken userAuthTokenCreate_byManager(
            String userID,
            String username,
            String deviceIdentity,
            boolean isValidateResult) {
        UserAuthToken authTokenToCreate = new UserAuthToken();
        authTokenToCreate.setUserId(userID);
        authTokenToCreate.setUsername(username);
        authTokenToCreate.setDeviceIdentity(deviceIdentity);

        UserAuthToken authTokenCreated = null;
        try {
            authTokenCreated = UserAuthTokenManager.create(authTokenToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("UserAuthToken Create:: Unexpected ERROR!" + e.getMessage());
        }
        if(isValidateResult) {
            Assert.assertNotNull("UserAuthToken Create:: Returned UserAuthToken should NOT NULL!", authTokenCreated);
            Assert.assertNotNull("UserAuthToken Create: Returned UserAuthToken should have its ID!", authTokenCreated.getId());
            Assert.assertEquals("UserAuthToken Create: Invalid returned UserId!", userID, authTokenCreated.getUserId());
            Assert.assertEquals("UserAuthToken Create: Invalid returned Username!", username, authTokenCreated.getUsername());
            Assert.assertEquals("UserAuthToken Create: Invalid returned Device identity!", deviceIdentity, authTokenCreated.getDeviceIdentity());
            Assert.assertNotNull("UserAuthToken Create: Returned UserAuthToken should have Auth Token!", authTokenCreated.getAuthToken());
        }
        return authTokenCreated;
    }

    public static UserAuthToken userAuthTokenCreate_byWSClient(
            User userToLogin,
            String deviceIdentity,
            boolean isValidateResult,
            int numOfTokenExpected) {
        UserAuthToken uatCreated = null;
        
        //Create via login (only):
        UserAuthToken uatToCreate = new UserAuthToken();
        uatToCreate.setDeviceIdentity(deviceIdentity);
        ServiceResponse svcResponse = UserWSClient.userLogin(userToLogin, uatToCreate);
        
        if(isValidateResult) {
            Assert.assertNotNull("User Login: Response should NOT be null!", svcResponse);
            Assert.assertTrue("User Login: Response should have been a SUCCESS! \n" + svcResponse, svcResponse.isSuccess());
            Assert.assertNotNull("User Login: Response should have User bean in it!", svcResponse.getUser());
            
            uatCreated = svcResponse.getUser().getUserAuthToken();
            Assert.assertNotNull("User Login: Response should have a UserAuthToken bean in it!", uatCreated);
            Assert.assertNotNull("UserAuthToken Create: It should have an ID!", uatCreated.getId());
            Assert.assertEquals("UserAuthToken Create: Invalid returned Username!", userToLogin.getUsername(), uatCreated.getUsername());
            Assert.assertNotNull("UserAuthToken Create: It should have an Auth Token!", uatCreated.getAuthToken());
            Assert.assertEquals("UserAuthToken Create: Unexpected Device Identity!", deviceIdentity, uatCreated.getDeviceIdentity());
            
            List<UserAuthToken> listOfUserAuthTokens = svcResponse.getUser().getListOfAuthTokens();
            Assert.assertNotNull("UserAuthToken Create: Response should have User login's list of tokens!", listOfUserAuthTokens);
            Assert.assertEquals("UserAuthToken Create: Invalid number of User login's tokens!", numOfTokenExpected, listOfUserAuthTokens.size());
        }
        
        if(uatCreated == null) {
            if(svcResponse != null && svcResponse.getUser() != null) {
                uatCreated = svcResponse.getUser().getUserAuthToken();
            }
        }
        
        return uatCreated;
    }
    
    public static void userAuthTokenAuthenticate_byManager(
            String userID, 
            String authToken, 
            boolean isValidateResult,
            boolean isSuccessExpected) throws Exception {
        UserAuthToken uatToAuthenticate = new UserAuthToken();
        uatToAuthenticate.setUserId(userID);
        uatToAuthenticate.setAuthToken(authToken);
        
        if(isSuccessExpected) {
            User userAuthenticated = UserAuthTokenManager.authenticate(uatToAuthenticate);
            if(isValidateResult) {
                Assert.assertNotNull("UserAuthToken Authenticate: Returned authenticated USER should not empty!", userAuthenticated);
                Assert.assertEquals("UserAuthToken Authenticate: Unexpected User ID!", userID, userAuthenticated.getUserId());
            }
            
        } else {
            try {
                UserAuthTokenManager.authenticate(uatToAuthenticate);
                Assert.fail("UserAuthToken Authenticate: With invalid authToken should have FAILED!");
            } catch(UserNotFoundException unfe) {
                //Expected:
            }
        }
    }    

    public static void userAuthTokenAuthenticate_byWSClient(
            String userID, 
            String authToken, 
            boolean isValidateResult,
            boolean isSuccessExpected,
            ServiceErrorEnum serviceErrorExpected) {
        UserAuthToken uatToAuthenticate = new UserAuthToken();
        uatToAuthenticate.setUserId(userID);
        uatToAuthenticate.setAuthToken(authToken);
        ServiceResponse svcResponse = UserAuthTokenWSClient.authenticate(uatToAuthenticate);
        if(isValidateResult) {
            Assert.assertNotNull("UserAuthToken Authenticate: Response should NOT be null!", svcResponse);
            if(isSuccessExpected) {
                Assert.assertTrue("UserAuthToken Authenticate: Response should have been SUCCESS!", svcResponse.isSuccess());
                User userAuthenticated = svcResponse.getUser();
                Assert.assertNotNull("UserAuthToken Authenticate: Returned authenticated USER should not empty!", userAuthenticated);
                Assert.assertEquals("UserAuthToken Authenticate: Unexpected User ID!", userID, userAuthenticated.getUserId());
                
            } else {
                Assert.assertFalse("UserAuthToken Authenticate: Response should have been FAILED!", svcResponse.isSuccess());
                Assert.assertNotNull("UserAuthToken Authenticate: Response should have a ServiceError!", svcResponse.getServiceError());
                Assert.assertEquals("UserAuthToken Authenticate: Unexpected error code! Error text: " + svcResponse.getServiceError().getErrorText(), 
                        serviceErrorExpected, svcResponse.getServiceError().getErrorEnum());        
            }
        }
    }
    
    public static int userAuthTokenDelete_byDB(String userID) {
        int deletedItem = 0;
        if(ValidationUtil.isEmpty(userID)) {
            return deletedItem;
        }
        UserAuthToken userAuthTokenDeleteAll = new UserAuthToken();
        userAuthTokenDeleteAll.setUserId(userID);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            deletedItem = UserAuthTokenDBQuery.delete(
                    session, null, userAuthTokenDeleteAll, 
                    ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
        } catch(Exception e) {
            System.err.println("UserAuthToken Delete: Unexpectd FAILURE - " + e.getMessage());
            e.printStackTrace();
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }    
        return deletedItem;
    }

    public static int userAuthTokenDelete_byManager(String userAuthTokenID) throws Exception {
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setId(userAuthTokenID);
        return UserAuthTokenManager.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
    }    


    
}
