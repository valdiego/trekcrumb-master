package test.com.trekcrumb.utility;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.trekcrumb.business.email.EmailManagerFactory;
import com.trekcrumb.business.email.IEmailManager;

/**
 * Util class to read test results file defined in TestConstants.TEST_REPORT_FILENAME, and compose proper 
 * admin report 
 * 1. Must be run only after test scripts (UnitTest_Start.bat or unittest_start.sh) which would 
 *    generate the required test result file
 * 2. Evaluate if there is any error or failure after test run
 * 3. Send email to report the test results if any error/failure occurs
 * 
 * NOTE:
 * We used SAX XML parser library instead of DOM XML parser since it is more resource efficient.
 * - REF: http://howtodoinjava.com/xml/dom-vs-sax-parser-in-java/
 * - REF: http://www.mkyong.com/java/how-to-read-xml-file-in-java-sax-parser/
 * 
 * @author Val Triadi
 */
public class TestReporter {

    public static void main(String argv[]) {
        try {
            DefaultHandler handler = new TestReporter.TestReporterSAXtHandler();

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(TestConstants.TEST_REPORT_FILENAME, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Custom SAX handler where we override startElement() and endElement() operations with
     * our own business logic.
     * 
     * When parsing the test result file, it assumes the format of the content as follow:
         <testsuite errors="0" failures="2" tests="378" time="179.682" timestamp="2016-06-02T15:40:30">
             <testcase classname="test.com.trekcrumb.CryptographerTest" name="testEncryptAndDecrypt" time="0.129" />
             <testcase classname="test.com.trekcrumb.PictureManagerTest" name="testCreate_GIF" time="0.131">
                <failure message="Invalid argument to native writeImage"
                         type="testCreate_GIF(test.com.trekcrumb.business.manager.PictureManagerTest)">
                    javax.imageio.IIOException: Invalid argument to native writeImage
                    at com.sun.imageio.plugins.jpeg.JPEGImageWriter.writeImage(Native Method)
                    at com.trekcrumb.common.utility.FileUtil.readImageFromFileDirectory(FileUtil.java:74)
                </failure>
             </testcase>
         </testsuite>
     * 
     */
    public static class TestReporterSAXtHandler extends DefaultHandler {
        String mTestTimestamp = null;
        String mNumOfTestTotal = null;
        String mTestRunningTime = null;
        int mNumOfFailures = -1;
        int mNumOfErrors = -1;
        StringBuilder mListOfFailureBld = null;
        StringBuilder mListOfErrorBld = null;
        
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            //Start from element <testsuite>
            if(qName != null && qName.equalsIgnoreCase("testsuite")) {
                //Parse test suite summary which are <testsuite> attributes:
                mTestTimestamp = attributes.getValue("timestamp");
                mNumOfTestTotal = attributes.getValue("tests");
                mTestRunningTime = attributes.getValue("time");
                try {
                    mNumOfFailures = Integer.parseInt(attributes.getValue("failures"));
                } catch(NumberFormatException nfe) {
                    System.err.println("ERROR: While parsing values for number of failures: " + nfe.getMessage());
                }
                try {
                    mNumOfErrors = Integer.parseInt(attributes.getValue("errors"));
                } catch(NumberFormatException nfe) {
                    System.err.println("ERROR: While parsing values for number of errors: " + nfe.getMessage());
                }
            }
            
            //Get detail about failure if any:
            if(mNumOfFailures > 0) {
                if(mListOfFailureBld == null) {
                    mListOfFailureBld = new StringBuilder();
                    mListOfFailureBld.append("\nLIST OF TEST FAILURES");
                }
                if(qName != null && qName.equalsIgnoreCase("failure")) {
                    mListOfFailureBld.append("\n");
                    mListOfFailureBld.append("Test case: " + attributes.getValue("type"));
                    mListOfFailureBld.append(" --> ");
                    mListOfFailureBld.append("Fail message: " + attributes.getValue("message"));
                }
            }
            
            //Get detail about error if any:
            if(mNumOfErrors > 0) {
                //TODO
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if(qName != null && qName.equalsIgnoreCase("testsuite")) {
                //Reaching the closing of root element </testsuite> which means the end of XML file
                StringBuilder testSummaryBld = new StringBuilder();
                testSummaryBld.append("Test Result Summary\n");
                testSummaryBld.append("Time of test [" + mTestTimestamp + "]\n");
                testSummaryBld.append("Number to total tests run [" + mNumOfTestTotal + "]\n");
                testSummaryBld.append("Test running time [" + mTestRunningTime + " ms]\n");
                testSummaryBld.append("Number of failures [" + mNumOfFailures + "]\n");
                testSummaryBld.append("Number of error [" + mNumOfErrors + "]\n");
                if(mNumOfFailures > 0 && mListOfFailureBld != null) {
                    testSummaryBld.append(mListOfFailureBld.toString());
                }
                if(mNumOfErrors > 0 && mListOfErrorBld != null) {
                    testSummaryBld.append(mListOfErrorBld.toString());
                }
                
                //Print
                System.out.println("\n" + testSummaryBld.toString());
                
                //Send email (only if any failed)
                if(mNumOfErrors != 0 || mNumOfFailures != 0) {
                    try {
                        IEmailManager emailManager = EmailManagerFactory.getEmailManagerInstance();
                        emailManager.mailTestResults(testSummaryBld.toString());
                        System.out.println("\nThere are failures! Check Test Admin email.");
                    } catch(Exception e) {
                        System.err.println("ERROR: While trying to send email Admin for test result summary!");
                    }
                }
            }
        }
     }
    
    
}
