package test.com.trekcrumb.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestProperties {
    private static final String PROPERTIES_FILE_NAME = "trekcrumb.test.properties";
    
    private static Properties mProperties = null;

    static {
        loadPropertiesFromFile();
    }
    
    /**
     * Disallows instantiation of this static class. 
     */
    private TestProperties() {}

    /**
     * Loads the property file.
     *  
     * Note: Initially, loads properties from System. Then, if properties file is found,
     * reload them from the file.
     */
    public static void loadPropertiesFromFile() {
        if (mProperties == null) {
            mProperties = System.getProperties();
        }
        InputStream internalIS = null;
        try {
            ClassLoader classLoader = TestProperties.class.getClassLoader();
            internalIS = classLoader.getResourceAsStream(PROPERTIES_FILE_NAME);
            if(internalIS != null) {
                mProperties.load(internalIS);
            }
        } catch(IOException ioe) {
            System.err.println("TestProperties.loadPropertiesFromFile() - ERROR: " + ioe.getMessage());
            ioe.printStackTrace();
        } finally {
            if(internalIS != null) {
                try {
                    internalIS.close();
                } catch (Exception e) {
                    //Ignore
                }
            }
        }
    }
    
    /**
     * Returns requested property value given its key.
     * @exception Exception is thrown if property not found. 
     */
    public static String getProperty(String key) {
        if(mProperties == null) {
            loadPropertiesFromFile();
        }
        
        String value = mProperties.getProperty(key);
        if(value == null) {
            String msg = "Cannot find property from [" + PROPERTIES_FILE_NAME + "] given its key [" + key + "]!";
            throw new IllegalArgumentException(msg);
        }
        return value;
    }        
}
