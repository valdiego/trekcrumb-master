package test.com.trekcrumb.utility;

import java.util.Date;

import junit.framework.Assert;

import org.hibernate.Session;

import com.trekcrumb.business.dao.IPictureDAO;
import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.PictureDAOHibernateImpl;
import com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQuery;
import com.trekcrumb.business.manager.PictureManager;
import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.FileUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.fileserverclient.FileServerClientFactory;
import com.trekcrumb.fileserverclient.IFileServerClient;
import com.trekcrumb.webserviceclient.PictureWSClient;

public class TestUtilForPicture {

    public static void pictureCreate_byDB(
            String userID, String tripID, String imageName, String imageURL, 
            YesNoEnum isCoverPicture, String featurePlaceID, String note) {
        Picture pictureToCreate = new Picture();
        pictureToCreate.setId(CommonUtil.generateID("TESTID"));
        pictureToCreate.setUserID(userID);
        pictureToCreate.setTripID(tripID);
        pictureToCreate.setImageName(imageName);
        pictureToCreate.setImageURL(imageURL);
        pictureToCreate.setCoverPicture(isCoverPicture);
        pictureToCreate.setFeatureForPlaceID(featurePlaceID);
        pictureToCreate.setNote(note);
        
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        pictureToCreate.setCreated(createdDate);
        pictureToCreate.setUpdated(createdDate);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            PictureDBQuery.create(session, null, pictureToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Picture Create: ERROR - " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    public static void pictureCreate_byDB_DefaultData(String userID, String tripID) {
        Picture pictureToCreate = new Picture();
        pictureToCreate.setId(CommonUtil.generateID("TESTID"));
        pictureToCreate.setUserID(userID);
        pictureToCreate.setTripID(tripID);
        pictureToCreate.setImageName(TestConstants.UNITTEST_PICTURE_IMAGE_NAME);
        pictureToCreate.setImageURL(TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL);
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        pictureToCreate.setCreated(createdDate);
        pictureToCreate.setUpdated(createdDate);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            PictureDBQuery.create(session, null, pictureToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Picture Create: ERROR - " + e.getMessage());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    public static Picture pictureCreate_byDAOImpl(
            String userID, String tripID, String imageName, String imageURL,
            YesNoEnum isCoverPicture, String featurePlaceID, String note,
            boolean isValidateResult) {
        Picture pictureToCreate = new Picture();
        String id = CommonUtil.generateID("TESTID");
        pictureToCreate.setId(id);
        pictureToCreate.setUserID(userID);
        pictureToCreate.setTripID(tripID);
        pictureToCreate.setCoverPicture(isCoverPicture);
        pictureToCreate.setFeatureForPlaceID(featurePlaceID);
        pictureToCreate.setNote(note);
        pictureToCreate.setImageName(imageName);
        pictureToCreate.setImageURL(imageURL);
        
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        pictureToCreate.setCreated(createdDate);
        pictureToCreate.setUpdated(createdDate);
        
        Picture picCreated =  null;
        try {
            IPictureDAO daoImpl = new PictureDAOHibernateImpl();
            picCreated = daoImpl.create(pictureToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Picture Create: ERROR - " + e.getMessage());
        }
        if(isValidateResult) {
            validatePicture(pictureToCreate, picCreated);
            
            /*
             * On Manager, during "Picture Create" with input cover-picture = null, it defaults it to YesNoEnum.NO.
             * BUT on DAO and DBQuery, they do not set default, and the value saved into DB is null.
             */
            if(pictureToCreate.getCoverPicture() != null) {
                Assert.assertEquals("Picture Create: Invalid 'Cover Picture' value!", pictureToCreate.getCoverPicture(), picCreated.getCoverPicture());
            }
        }        
        
        return picCreated;
    }
    
    public static Picture pictureCreate_byManager(
            String className,
            String userID, String tripID, byte[] imageBytes,
            YesNoEnum isCoverPicture, String featurePlaceID, String note,
            boolean isValidateResult) {
        Picture pictureCreated = null;
        
        Picture pictureToCreate = new Picture();
        pictureToCreate.setUserID(userID);
        pictureToCreate.setTripID(tripID);
        pictureToCreate.setImageName(CommonUtil.generatePictureFilename(className));
        pictureToCreate.setImageBytes(imageBytes);
        pictureToCreate.setCoverPicture(isCoverPicture);
        pictureToCreate.setFeatureForPlaceID(featurePlaceID);
        pictureToCreate.setNote(note);
        try {
            pictureCreated = PictureManager.create(pictureToCreate);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Picture Create: ERROR - " + e.getMessage());
        }
        
        if(isValidateResult) {
            validatePicture(pictureToCreate, pictureCreated);
            
            /*
             * On Manager, during "Picture Create" with input cover-picture = null, it defaults it to YesNoEnum.NO.
             * BUT on DAO and DBQuery, they do not set default, and the value saved into DB is null.
             */
            if(pictureToCreate.getCoverPicture() != null) {
                Assert.assertEquals("Picture Create: Invalid 'Cover Picture' value!", pictureToCreate.getCoverPicture(), pictureCreated.getCoverPicture());
            } else {
                Assert.assertEquals("Picture Create: Invalid DEFAULT 'Cover Picture' value!", YesNoEnum.NO, pictureCreated.getCoverPicture());
            }


            
            Date createdDate = DateUtil.formatDate(pictureCreated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(pictureCreated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Picture Create: Record's created date and updated date should have been the same!", 
                    createdDate.equals(updatedDate));
        }
        
        return pictureCreated;
    }    
    
    public static Picture pictureCreate_byManager_DefaultData(
            String className, User user, Trip trip) 
            throws Exception {
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL);
        Assert.assertNotNull("Picture create: Image bytes from the file cannot be null!", imgBytes);
        
        return pictureCreate_byManager(className, user.getUserId(), trip.getId(), imgBytes, null, null, null, false);
    }
    
    public static Picture pictureCreate_byWSClient(
            String className,
            String userID, String tripID, byte[] imageBytes,
            YesNoEnum isCoverPicture, String featurePlaceID, String note,
            boolean isValidateResult) {
                
        Picture pictureToCreate = new Picture();
        pictureToCreate.setUserID(userID);
        pictureToCreate.setTripID(tripID);
        pictureToCreate.setImageName(CommonUtil.generatePictureFilename(className));
        pictureToCreate.setImageBytes(imageBytes);
        pictureToCreate.setNote(note);
        ServiceResponse result = PictureWSClient.create(pictureToCreate); 
        Assert.assertNotNull("Picture create: Returned result can NOT be null!", result);
        Assert.assertTrue("Picture create: Result should have been SUCCESS! \n" + result, result.isSuccess());
        if(result.getListOfPictures() == null || result.getListOfPictures().size() == 0) {
            Assert.fail("Picture create: Result should have had the created Picture!");
        }

        Picture pictureCreated = result.getListOfPictures().get(0);
        if(isValidateResult) {
            validatePicture(pictureToCreate, pictureCreated);
            
            /*
             * On Manager, during "Picture Create" with input cover-picture = null, it defaults it to YesNoEnum.NO.
             * BUT on DAO and DBQuery, they do not set default, and the value saved into DB is null.
             */
            if(pictureToCreate.getCoverPicture() != null) {
                Assert.assertEquals("Picture Create: Invalid 'Cover Picture' value!", pictureToCreate.getCoverPicture(), pictureCreated.getCoverPicture());
            } else {
                Assert.assertEquals("Picture Create: Invalid DEFAULT 'Cover Picture' value!", YesNoEnum.NO, pictureCreated.getCoverPicture());
            }

            Date createdDate = DateUtil.formatDate(pictureCreated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(pictureCreated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Picture Create: Record's created date and updated date should have been the same!", 
                    createdDate.equals(updatedDate));
        }
        
        return pictureCreated;
    }

    public static int pictureDelete_byDB_ALLPictures(String userID) {
        int deletedItem = 0;
        if(ValidationUtil.isEmpty(userID)) {
            return deletedItem;
        }
        Picture picToDeleteAll = new Picture();
        picToDeleteAll.setUserID(userID);
        Session session = null;
        try {
            session = HibernateManager.openSession();
            deletedItem = PictureDBQuery.delete(session, null, picToDeleteAll, ServiceTypeEnum.PICTURE_DELETE_BY_USERID);
        } catch(Exception e) {
            System.err.println("Picture Delete: ERROR - " + e.getMessage());
            e.printStackTrace();
        }
        return deletedItem;
    }
    
    public static int pictureDelete_byManager_ALLPictures(String userID) {
        int deletedItem = 0;
        if(ValidationUtil.isEmpty(userID)) {
            return deletedItem;
        }
        try {
            Picture picToDelete = new Picture();
            picToDelete.setUserID(userID);
            deletedItem = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_USERID);
        } catch(Exception e) {
            System.err.println("Picture Delete: ERROR - " + e.getMessage());
            e.printStackTrace();
        }
        return deletedItem;
    }
    
    public static FileServerResponse imageUpload_byManager(
            String userID, String targetFilename, byte[] imageBytes, boolean isTrekPicture) 
            throws Exception {
        if(isTrekPicture) {
            Picture pictureWithImageToUpload = new Picture();
            pictureWithImageToUpload.setImageName(targetFilename);
            pictureWithImageToUpload.setImageBytes(imageBytes);
            return PictureManager.imageUpload(pictureWithImageToUpload);
        } else {
            User userWithProfileImageToUpload = new User();
            userWithProfileImageToUpload.setUserId(userID);
            userWithProfileImageToUpload.setProfileImageName(targetFilename);
            userWithProfileImageToUpload.setProfileImageBytes(imageBytes);
            return UserManager.userProfileImageUpload(userWithProfileImageToUpload);
        }
    }
    
    public static FileServerResponse imageDownload_byManager(
            String targetFilename, boolean isTrekPicture) 
            throws Exception {
        if(isTrekPicture) {
            Picture pictureWithImageToDownload = new Picture();
            pictureWithImageToDownload.setImageName(targetFilename);
            return PictureManager.imageDownload(pictureWithImageToDownload);
        } else {
            User userWithProfileImageToDownload = new User();
            userWithProfileImageToDownload.setProfileImageName(targetFilename);
            return UserManager.userProfileImageDownload(userWithProfileImageToDownload);
        }
    }

    public static boolean imageRead_byFSClient(String targetFilename, boolean isTrekPicture) throws Exception {
        boolean isPicImageRead = false;
        FileServerRequest requestToRead = new FileServerRequest();
        if(isTrekPicture) {
            requestToRead.setFileMenu(FileServerMenuEnum.READ_TREK_PICTURE);
        } else {
            requestToRead.setFileMenu(FileServerMenuEnum.READ_PROFILE_PICTURE);
        }
        requestToRead.setFilename(targetFilename);
        System.out.println("Picture Image Read: Request to send: " + requestToRead);
        
        IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
        FileServerResponse response = fsClient.read(requestToRead);
        if(!response.isSuccess()) {
            if(response.getServiceError() != null
                    && response.getServiceError().getErrorEnum() == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND) {
                System.err.println("Picture Image Read: FileServer could not find image with given name: " + targetFilename);
                return isPicImageRead;
            }
            
            String errorMsg = "Picture Image Read: FileServer responded with an error: " + response.getServiceError();
            System.err.println(errorMsg);
            throw new Exception(errorMsg);
        }
        byte[] fileReadBytes = response.getFileBytes();
        if(fileReadBytes != null) {
            isPicImageRead = true;
        }
        return isPicImageRead;
    }
    
    public static void imageDelete_byFSClient(
            String targetFilename, 
            boolean isTrekPicture) throws Exception {
        FileServerRequest requestToDelete = new FileServerRequest();
        if(isTrekPicture) {
            requestToDelete.setFileMenu(FileServerMenuEnum.DELETE_TREK_PICTURE);
        } else {
            requestToDelete.setFileMenu(FileServerMenuEnum.DELETE_PROFILE_PICTURE);
        }
        requestToDelete.setFilename(targetFilename);
        System.out.println("Picture Image Delete: Request to send: " + requestToDelete);

        IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
        FileServerResponse response = fsClient.delete(requestToDelete);
        if(!response.isSuccess()) {
            String errMsg = null;
            if(response.getServiceError() != null) {
                if(response.getServiceError().getErrorEnum() == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND) {
                    System.out.println("Picture Image Delete: Image file [" + targetFilename + "] NOT found!");
                    return;
                }
                
                //Else:
                errMsg = "Picture Image Delete: FileServer responded with an error: " + response.getServiceError();
            }
            
            if(errMsg == null) {
                errMsg = "Picture Image Delete: FileServer responded with an error!";
            }
            System.err.println(errMsg);
            throw new Exception(errMsg);
        }
    }
    
    public static void validatePicture(Picture picExpected, Picture picActual) {
        Assert.assertNotNull("Picture Validation: Returned picture should NOT be null!", picActual);
        
        if(picExpected.getId() != null) {
            Assert.assertEquals("Picture Validation: Invalid returned picture ID!", picExpected.getId(), picActual.getId());
        } else {
            Assert.assertNotNull("Picture Validation: Returned picture should have an ID!", picActual.getId());
        }
        
        Assert.assertEquals("Picture Validation: Invalid User ID!", picExpected.getUserID(), picActual.getUserID());
        Assert.assertEquals("Picture Validation: Invalid Trip ID!", picExpected.getTripID(), picActual.getTripID());
        Assert.assertEquals("Picture Validation: Invalid Note!", picExpected.getNote(), picActual.getNote());
        Assert.assertEquals("Picture Validation: Invalid 'Feature for Place' value!", 
                picExpected.getFeatureForPlaceID(), picActual.getFeatureForPlaceID());
        
        if(picExpected.getImageName() != null) {
            Assert.assertEquals("Picture Validation: Invalid image name!", picExpected.getImageName(), picActual.getImageName());
        } else {
            Assert.assertNotNull("Picture Validation: Image Name cannot be null!", picActual.getImageName());
        }
        
        if(picExpected.getCreated() != null) {
            Assert.assertEquals("Picture Validation: Invalid created date!", picExpected.getCreated(), picActual.getCreated());
        } else {
            Assert.assertNotNull("Picture Validation: Created date shall not be null!", picActual.getCreated());
        }
        
        if(picExpected.getUpdated() != null) {
            Assert.assertEquals("Picture Validation: Invalid updated date!", picExpected.getUpdated(), picActual.getUpdated());
        } else {
            Assert.assertNotNull("Picture Validation: Updated date shall not be null!", picActual.getUpdated());
        }
        
        //Verify actual image file:
        if(picExpected.getImageBytes() != null) {
            String imageName = picActual.getImageName();
            try {
                Assert.assertTrue("Picture Validation: FAILED to find physical image file for [" + imageName +"] on File Server!", 
                        imageRead_byFSClient(imageName, true));
            } catch(Exception e) {
                Assert.fail("Picture Validation: ERROR while looking for physical image file for [" + imageName +"] on File Server. " 
                        + e.getMessage());
            }
        }
    }
    
    /**
     * WARNING: 
     * This will ONLY work if BOTH tests running on the same machine as the FileServer (local or otherwise)
     * because it attempts to read directly into a directory.
     * For tests against remote FileServer instances, this test will NOT work and cause failure. 
     */
    public static void verifyPictureFileExistOrNotByFileIO(
            String targetURLAbsolute,
            boolean isFileShouldExist,
            String customErrorMsg) {
        System.err.println("TestUtilForPicture.verifyPictureFileExistOrNotByFileIO() - WARNING: This ops is NOT supported anymore!");

        /**********************************
        boolean isPicImageExist = false;
        try {
            File picFileCreated = new File(targetURLAbsolute);
            isPicImageExist = picFileCreated.exists();
        } catch(Exception e) {
            isPicImageExist = false;
            System.err.println("pictureImageReadByFileIO ERROR while reading file [" 
                    + targetURLAbsolute + "] with message: " + e.getMessage());
            e.printStackTrace();
        }

        if(isPicImageExist) {
            if(!isFileShouldExist) {
                //Expecting file to not exist!
                Assert.fail(customErrorMsg);
            }
        } else {
            if(isFileShouldExist) {
                //Expecting file to exist!
                Assert.fail(customErrorMsg);
            }
        }
        ************************************/
    }    
    
    
}
