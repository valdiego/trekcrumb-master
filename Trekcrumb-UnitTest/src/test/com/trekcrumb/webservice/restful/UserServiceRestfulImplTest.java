package test.com.trekcrumb.webservice.restful;

import java.security.Signature;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.common.bean.DigitalSignature;
import com.trekcrumb.common.bean.ServiceError;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.crypto.StringBytesUtil;
import com.trekcrumb.common.crypto.TrekcrumbPrivateKey;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * Tests directly to the WebService focusing only on login() service.
 * For more complete/other test cases coverage, see the webserviceclient test package.
 *
 */
public class UserServiceRestfulImplTest {
    private static final String CLASS_NAME = "UserServiceRestfulImplTest";
    
    private static String username;
    private static User userCreated;

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME;
        userCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB(userCreated);
    }

    /**
     * Data setup called before each test case. 
     */
    @Before
    public void setUp() {
        //None
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        //None
    }

    @Test
    public void testLogin() {
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = prepareAndSendRequest(
                    userToLogin,
                    true,
                    true,
                    true,
                    true,
                    true);
            Assert.assertNotNull("The User Login result should NOT be null!", result);
            Assert.assertTrue("The User Login result should have been SUCCESS! Result error: " + result.getServiceError(), result.isSuccess());
    }

    @Test
    public void testLogin_WithoutUserData() {
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = prepareAndSendRequest(
                    userToLogin,
                    false,
                    true,
                    true,
                    true,
                    true);
            Assert.assertNotNull("The User Login result should NOT be null!", result);
            Assert.assertFalse("The User Login result should have been FAILED!", result.isSuccess());
            Assert.assertNotNull("The User Login result should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }

    @Test
    public void testLogin_WithoutSignedMessage() {
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = prepareAndSendRequest(
                    userToLogin,
                    true,
                    false,
                    true,
                    true,
                    true);
            Assert.assertNotNull("The User Login result should NOT be null!", result);
            Assert.assertFalse("The User Login result should have been FAILED!", result.isSuccess());
            Assert.assertNotNull("The User Login result should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }

    @Test
    public void testLogin_WithoutSignature() {
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = prepareAndSendRequest(
                    userToLogin,
                    true,
                    true,
                    false,
                    true,
                    true);
            Assert.assertNotNull("The User Login result should NOT be null!", result);
            Assert.assertFalse("The User Login result should have been FAILED!", result.isSuccess());
            Assert.assertNotNull("The User Login result should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }

    @Test
    public void testLogin_WithInvalidSignature() {
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = prepareAndSendRequest(
                    userToLogin,
                    true,
                    true,
                    true,
                    false,
                    true);
            Assert.assertNotNull("The User Login result should NOT be null!", result);
            Assert.assertFalse("The User Login result should have been FAILED!", result.isSuccess());
            Assert.assertNotNull("The User Login result should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, 
                    result.getServiceError().getErrorEnum());
    }

    @Test
    public void testLogin_WithoutCryptedPassword() {
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = prepareAndSendRequest(
                    userToLogin,
                    true,
                    true,
                    true,
                    true,
                    false);
            Assert.assertNotNull("The User Login result should NOT be null!", result);
            Assert.assertFalse("The User Login result should have been FAILED!", result.isSuccess());
            Assert.assertNotNull("The User Login result should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }

    private ServiceResponse prepareAndSendRequest(
            User user,
            boolean isWithUserDate,
            boolean isWithSignedMessage,
            boolean isSignTheMessage,
            boolean isSignWithValidSignature,
            boolean isEncryptPassword) {
        ServiceRequest request = null;
        ServiceResponse result = null;

        try {
            request = RequestUtil.prepareRequestForUserWS(user, null, ServiceTypeEnum.USER_LOGIN, 0, 1);
            if(request.getDigitalSignature() == null) {
                request.setDigitalSignature(new DigitalSignature());
            }
            System.out.println("Sending Request: \n" + request);
        } catch(Exception e) {
            e.printStackTrace();
            result = new ServiceResponse();
            result.setSuccess(false);
            ServiceError error = new ServiceError();
            error.setErrorEnum(ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR);
            error.setErrorText(e.getMessage());
            result.setServiceError(error);
            return result;
        }
        
        if(!isWithUserDate) {
            request.setUser(null);
        }
        
        if(!isWithSignedMessage) {
            request.getDigitalSignature().setSignedMessage(null);
        }
        
        if(!isSignTheMessage) {
            if(request.getDigitalSignature() == null) {
                request.setDigitalSignature(new DigitalSignature());
            }
            request.getDigitalSignature().setSignature(null);
        }
        
        if(!isSignWithValidSignature) {
            String origSignedMessage = request.getDigitalSignature().getSignedMessage();
            try {
                byte[] origSignedMessageBytes = origSignedMessage.getBytes("UTF8");
                TrekcrumbPrivateKey privateKey = TestConstants.TEST_HACKER_PRIVATE_KEY;
                Signature hackerSignature = Signature.getInstance("MD5WithRSA");
                hackerSignature.initSign(privateKey);
                hackerSignature.update(origSignedMessageBytes);
                byte[] signature = hackerSignature.sign();
                String hackerSignatureHexString = StringBytesUtil.bytesToHexString(signature);
                System.out.println("Hacker signature in hex String: " + hackerSignatureHexString);
                request.getDigitalSignature().setSignature(hackerSignatureHexString);
            } catch(Exception e) {
                Assert.fail("Unexpected test failure: " + e.getMessage());
            }
        }
        
        if(!isEncryptPassword) {
            String origPwd = user.getPassword();
            request.getUser().setPassword(origPwd);
        }
        
        result = ConnectRestfulWSUtil.connectUserWS(request);
        System.out.println("Receiving Response: " + result);
        return result;        
    }
    
}
