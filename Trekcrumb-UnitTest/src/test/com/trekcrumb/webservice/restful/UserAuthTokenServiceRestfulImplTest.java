package test.com.trekcrumb.webservice.restful;

import java.security.Signature;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;

import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.common.bean.DigitalSignature;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.crypto.StringBytesUtil;
import com.trekcrumb.common.crypto.TrekcrumbPrivateKey;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.webserviceclient.utility.ConnectRestfulWSUtil;
import com.trekcrumb.webserviceclient.utility.RequestUtil;

/**
 * Tests directly to the WebService focusing on authenticate() service.
 * For more complete/other test cases coverage, see the webserviceclient test package.
 */
public class UserAuthTokenServiceRestfulImplTest {
    private static final String CLASS_NAME = "UserAuthTokenServiceRestfulImplTest";
    
    private static User userCreated;
    private static UserAuthToken userAuthTokenCreated;
    
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME;
        userCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(userCreated);
    }

    /**
     * Data setup called before each test case. 
     */
    @Before
    public void setUp() {
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + System.currentTimeMillis();
        userAuthTokenCreated = 
                TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        userCreated.getUserId(), userCreated.getUsername(), deviceIdentity, false);
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        //None
    }

    @Test
    public void testAuthenticate() throws Exception {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        User userPreTestValidate = UserAuthTokenManager.authenticate(userAuthTokenToAuthenticate);
        Assert.assertNotNull("VIA MANAGER: Authenticate should be SUCCESS and returned a not-null User bean!", userPreTestValidate);
        
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                true,
                true,
                true,
                true,
                true,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertTrue("The UserAuthToken authentication result should have been SUCCESS!", result.isSuccess());
        Assert.assertNotNull("The UserAuthToken authentication result should contain authenticated User bean!", result.getUser());
    }

    @Test
    public void testAuthenticate_withoutUserAuthTokenData() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                false,
                true,
                true,
                true,
                true,
                true,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }

    @Test
    public void testAuthenticate_withoutSignedMessage() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                false,
                true,
                true,
                true,
                true,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }
    
    @Test
    public void testAuthenticate_withoutSignature() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                true,
                false,
                true,
                true,
                true,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }
    
    @Test
    public void testAuthenticate_withInvalidSignature() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                true,
                true,
                false,
                true,
                true,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, 
                result.getServiceError().getErrorEnum());
    }

    @Test
    public void testAuthenticate_withoutCyrptedUserID() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                true,
                true,
                true,
                false,
                true,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
    }

    @Test
    public void testAuthenticate_withoutCyrptedAuthToken() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                true,
                true,
                true,
                true,
                false,
                true);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());        
    }

    @Test
    public void testAuthenticate_withInvalidAuthToken() {
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(userCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        ServiceResponse result = prepareAndSendRequest(
                userAuthTokenToAuthenticate,
                true,
                true,
                true,
                true,
                true,
                true,
                false);
        Assert.assertNotNull("The UserAuthToken authentication result should NOT be null!", result);
        Assert.assertFalse("The UserAuthToken authentication result should have been FAILED!", result.isSuccess());
        Assert.assertNotNull("The result should have a ServiceError!", result.getServiceError());
        Assert.assertEquals("Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR, result.getServiceError().getErrorEnum());        
    }

    private ServiceResponse prepareAndSendRequest(
            UserAuthToken userAuthToken,
            boolean isWithUserAuthTokenData,
            boolean isWithSignedMessage,
            boolean isSignTheMessage,
            boolean isSignWithValidSignature,
            boolean isEncryptUserId,
            boolean isEncryptAuthToken,
            boolean isAuthTokenValid) {
        ServiceRequest request = null;
        ServiceResponse result = null;

        try {
            request = RequestUtil.prepareRequestForUserAuthTokenWS(
                    userAuthToken, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
            System.out.println("Test Request: \n" + request);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Unexpected test failure: " + e.getMessage());
            return null;
        }
        
        if(!isWithUserAuthTokenData) {
            request.setUserAuthToken(null);
        }
        
        if(!isWithSignedMessage) {
            if(request.getDigitalSignature() == null) {
                request.setDigitalSignature(new DigitalSignature());
            }
            request.getDigitalSignature().setSignedMessage(null);
        }
        
        if(!isSignTheMessage) {
            if(request.getDigitalSignature() == null) {
                request.setDigitalSignature(new DigitalSignature());
            }
            request.getDigitalSignature().setSignature(null);
        }
        
        if(!isSignWithValidSignature) {
            String origSignedMessage = request.getDigitalSignature().getSignedMessage();
            try {
                byte[] origSignedMessageBytes = origSignedMessage.getBytes("UTF8");
                TrekcrumbPrivateKey privateKey = TestConstants.TEST_HACKER_PRIVATE_KEY;
                Signature hackerSignature = Signature.getInstance("MD5WithRSA");
                hackerSignature.initSign(privateKey);
                hackerSignature.update(origSignedMessageBytes);
                byte[] signature = hackerSignature.sign();
                String hackerSignatureHexString = StringBytesUtil.bytesToHexString(signature);
                System.out.println("Hacker signature in hex String: " + hackerSignatureHexString);
                request.getDigitalSignature().setSignature(hackerSignatureHexString);
            } catch(Exception e) {
                Assert.fail("Unexpected test failure: " + e.getMessage());
            }
        }
        
        if(!isEncryptUserId) {
            String origUserId = userAuthToken.getUserId();
            request.getUserAuthToken().setUserId(origUserId);
        }
        
        if(!isEncryptAuthToken) {
            String origAuthToken = userAuthToken.getAuthToken();
            request.getUserAuthToken().setAuthToken(origAuthToken);
        }
        
        if(!isAuthTokenValid) {
            String invalidAuthToken = "INVALIDAUTHTOKEN_" +  System.currentTimeMillis();
            try {
                Cryptographer cryptoInstance = Cryptographer.getInstance();
                String cryptedInvalidAuthToken = cryptoInstance.encrypt(invalidAuthToken);
                request.getUserAuthToken().setAuthToken(cryptedInvalidAuthToken);
            } catch(Exception e) {
                e.printStackTrace();
                Assert.fail("Test failed due to unexpected error during cryptography: " + e.getMessage());
            }
        }

        result = ConnectRestfulWSUtil.connectUserAuthTokenWS(request);
        System.out.println("Returning result: " + result);
        return result;        
    }    
}
