package test.com.trekcrumb.webservice;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.webservice.restful.UserServiceRestfulImplTest.class,
    test.com.trekcrumb.webservice.restful.UserAuthTokenServiceRestfulImplTest.class,
    
})
public class AllTestsWebService {}
