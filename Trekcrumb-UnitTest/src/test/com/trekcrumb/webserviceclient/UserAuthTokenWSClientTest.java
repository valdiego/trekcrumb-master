package test.com.trekcrumb.webserviceclient;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.UserAuthTokenBaseTest;

/**
/**
 * WebService and WebServiceClient Components' tests for entity: UserAuthToken.
 * @see UserAuthTokenBaseTest
 */
public class UserAuthTokenWSClientTest
extends UserAuthTokenBaseTest {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = UserAuthTokenWSClientTest.class;
        mClassName = "UserAuthTokenWSClientTest";

        UserAuthTokenBaseTest.oneTimeSetUp();
    }
    
}
