package test.com.trekcrumb.webserviceclient;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForComment;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.webserviceclient.CommentWSClient;

/**
 * Tests the client who will connect to the WebService, thus in turn testing the WebService as well.
 *
 */
public class CommentWSClientTest {
    private static final String CLASS_NAME = "CommentWSClientTest";
    private static User mUserCreated;
    private static Trip mTripCreated;

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME); 
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForComment.commentDeletebyCommentDB(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreateRetrieveAndDelete() throws Exception {
        //Create:
        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
        ServiceResponse result = CommentWSClient.create(commentToCreate); 
        Assert.assertNotNull("Comment Create: Returned result can NOT be null!", result);
        Assert.assertTrue("Comment Create: Result should have been SUCCESS!", result.isSuccess());
        
        //Retrieve by Trip
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setTripID(mTripCreated.getId());
        result = CommentWSClient.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve by TripID: Returned result can NOT be null!", result);
        Assert.assertTrue("Comment Retrieve by TripID: Result should have been SUCCESS!", result.isSuccess());
        if(result.getListOfComments() == null || result.getListOfComments().size() == 0) {
            Assert.fail("Comment Retrieve by TripID: Result should have list of Comments!");
        }
        
        Comment commentFound = result.getListOfComments().get(0);
        Assert.assertNotNull("Comment Retrieve by TripID: Invalid returned Comment ID!", commentFound.getId());
        Assert.assertEquals("Comment Retrieve by TripID: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
        Assert.assertEquals("Comment Retrieve by TripID: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
        Assert.assertNotNull("Comment Retrieve by TripID: Returned created date shall not be null!", commentFound.getCreated());
        
        //Enrichment data:
        Assert.assertNull("Comment Retrieve: Returned UserID should be EMPTY!", commentFound.getUserID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
        Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
        
        //Retrieve by CommentID
        commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentFound.getId());
        result = CommentWSClient.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve by CommentID: Returned result can NOT be null!", result);
        Assert.assertTrue("Comment Retrieve by CommentID: Result should have been SUCCESS!", result.isSuccess());
        Assert.assertNotNull("Comment Retrieve by CommentID: Result should have list of Comments!", result.getListOfComments());
        Assert.assertEquals("Comment Retrieve by CommentID: Invalid number of Comments found!", 1, result.getListOfComments().size());
        
        //Retrieve by User
        commentToRetrieve = new Comment();
        commentToRetrieve.setUsername(mUserCreated.getUsername());
        result = CommentWSClient.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve by Username: Returned result can NOT be null!", result);
        Assert.assertTrue("Comment Retrieve by Username: Result should have been SUCCESS!", result.isSuccess());
        Assert.assertNotNull("Comment Retrieve by Username: Result should have list of Comments!", result.getListOfComments());
        Assert.assertEquals("Comment Retrieve by Username: Invalid number of Comments found!", 1, result.getListOfComments().size());
        
        //Delete by CommentID:
        Comment commentToDelete = new Comment();
        commentToDelete.setId(commentFound.getId());
        commentToDelete.setUserID(mUserCreated.getUserId());
        result = CommentWSClient.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
        Assert.assertNotNull("Comment Delete: Returned result can NOT be null!", result);
        Assert.assertTrue("Comment Delete: Result should have been SUCCESS!", result.isSuccess());

        //Validate:
        commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentFound.getId());
        result = CommentWSClient.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve by CommentID: Returned result can NOT be null!", result);
        Assert.assertTrue("Comment Retrieve by CommentID: Result should have been SUCCESS!", result.isSuccess());
        Assert.assertNull("Comment Retrieve by CommentID: Result should have EMPTY list of Comments!", result.getListOfComments());
    }
    
    @Test
    public void testDelete_illegalDeleteType() throws Exception {
        TestUtilForComment.commentCreateByCommentDB(
                mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);

        //Delete by User:
        Comment commentToDelete = new Comment();
        commentToDelete.setUserID(mUserCreated.getUserId());
        ServiceResponse result = CommentWSClient.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
        Assert.assertNotNull("Comment Delete by User: Returned result can NOT be null!", result);
        Assert.assertFalse("Comment Delete by User: Result should have been FAILED!", result.isSuccess());
        Assert.assertEquals("Comment Delete by User: Invalid returned ServiceErrorEnum!",  ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, result.getServiceError().getErrorEnum());

        //Delete by Trip:
        commentToDelete = new Comment();
        commentToDelete.setTripID(mTripCreated.getId());
        result = CommentWSClient.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
        Assert.assertNotNull("Comment Delete by Trip: Returned result can NOT be null!", result);
        Assert.assertFalse("Comment Delete by Trip: Result should have been FAILED!", result.isSuccess());
        Assert.assertEquals("Comment Delete by Trip: Invalid returned ServiceErrorEnum!",  ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, result.getServiceError().getErrorEnum());
    }    

}
