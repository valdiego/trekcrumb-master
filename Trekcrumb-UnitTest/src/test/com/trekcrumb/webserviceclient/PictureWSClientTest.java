package test.com.trekcrumb.webserviceclient;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.PictureBaseTest;

/**
 * WebService and WebServiceClient Components' tests for entity: Place.
 * @see PictureBaseTest
 */
public class PictureWSClientTest 
extends PictureBaseTest {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = PictureWSClientTest.class;
        mClassName = "PictureWSClientTest";

        PictureBaseTest.oneTimeSetUp();
    }
    

}


