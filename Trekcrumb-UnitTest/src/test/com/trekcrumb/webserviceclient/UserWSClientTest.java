package test.com.trekcrumb.webserviceclient;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.UserBaseTest;

/**
 * WebService and WebServiceClient Components' tests for entity: User.
 * @see UserBaseTest
 *
 */
public class UserWSClientTest 
extends UserBaseTest {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = UserWSClientTest.class;
        mClassName = "UserWSClientTest";

        UserBaseTest.oneTimeSetUp();
    }


}
