package test.com.trekcrumb.webserviceclient;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.TripBaseTest;

/**
 * WebService and WebServiceClient Components' tests for entity: Trip.
 * @see TripBaseTest
 */
public class TripWSClientTest 
extends TripBaseTest {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = TripWSClientTest.class;
        mClassName = "TripWSClientTest";

        TripBaseTest.oneTimeSetUp();
    }
    
}
