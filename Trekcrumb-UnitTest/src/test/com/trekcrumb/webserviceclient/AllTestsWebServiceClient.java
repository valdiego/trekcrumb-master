package test.com.trekcrumb.webserviceclient;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    //WebServiceClient:
    test.com.trekcrumb.webserviceclient.UserWSClientTest.class,
    test.com.trekcrumb.webserviceclient.UserAuthTokenWSClientTest.class,
    test.com.trekcrumb.webserviceclient.TripWSClientTest.class,
    test.com.trekcrumb.webserviceclient.PlaceWSClientTest.class,
    test.com.trekcrumb.webserviceclient.PictureWSClientTest.class,
    test.com.trekcrumb.webserviceclient.FavoriteWSClientTest.class,
    test.com.trekcrumb.webserviceclient.CommentWSClientTest.class,
    
})
public class AllTestsWebServiceClient {}
