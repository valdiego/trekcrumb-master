package test.com.trekcrumb.webserviceclient;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestUtilForFavorite;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.webserviceclient.FavoriteWSClient;

/**
 * Tests the client who will connect to the WebService, thus in turn testing the WebService as well.
 *
 */
public class FavoriteWSClientTest {
    private static final String CLASS_NAME = "FavoriteWSClientTest";
    private static User mUserCreated;
    private static Trip mTripCreated;

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME); 
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForFavorite.faveDeleteALLbyFavoriteDB(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreateRetrieveAndDelete() throws Exception {
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        ServiceResponse result = FavoriteWSClient.create(faveToCreate); 
        Assert.assertNotNull("Favorite Create: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Create: Result should have been SUCCESS!", result.isSuccess());
        
        //Retrieve
        Favorite favoriteToRetrieve = new Favorite();
        favoriteToRetrieve.setTripID(mTripCreated.getId());
        favoriteToRetrieve.setUserID(mUserCreated.getUserId());
        result = FavoriteWSClient.retrieve(favoriteToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Retrieve: Result should have been SUCCESS!", result.isSuccess());
        if(result.getListOfFavorites() == null || result.getListOfFavorites().size() == 0) {
            Assert.fail("Favorite Retrieve: Result should have list of Favorites!");
        }
        
        //Delete:
        Favorite faveToDelete = new Favorite();
        faveToDelete.setTripID(mTripCreated.getId());
        faveToDelete.setUserID(mUserCreated.getUserId());
        result = FavoriteWSClient.delete(faveToDelete);
        Assert.assertNotNull("Favorite Delete: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Delete: Result should have been SUCCESS!", result.isSuccess());

        //Validate:
        result = FavoriteWSClient.retrieve(favoriteToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Retrieve: Result should have been SUCCESS!", result.isSuccess());
        Assert.assertNull("Favorite Retrieve: Returned Favorite should be NULL!", result.getListOfFavorites());
    }
    
    @Test
    public void testRetrieveByTrip() throws Exception {
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        ServiceResponse result = FavoriteWSClient.create(faveToCreate); 
        Assert.assertNotNull("Favorite Create: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Create: Result should have been SUCCESS!", result.isSuccess());
        
        //Retrieve Favorite by Trip:
        Trip trip = new Trip();
        trip.setId(mTripCreated.getId());
        result = FavoriteWSClient.retrieveByTrip(trip, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve by Trip: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Retrieve by Trip: Result should have been SUCCESS!", result.isSuccess());
        if(result.getListOfUsers() == null || result.getListOfUsers().size() == 0) {
            Assert.fail("Favorite Retrieve by Trip: Result should have list of Users who favorite this Trip!");
        }
        Assert.assertEquals("Favorite Retrieve by Trip: Invalid returned username!", mUserCreated.getUsername(), result.getListOfUsers().get(0).getUsername());
        Assert.assertEquals("Favorite Retrieve by Trip: Invalid returned fullname!", mUserCreated.getFullname(), result.getListOfUsers().get(0).getFullname());
        
        //Validate secure User data are empty:
        //Assert.assertNull("Favorite Read by Trip: Returned user should have EMPTY userID!", result.getListOfUsers().get(0).getUserId());
        Assert.assertNull("Favorite Retrieve by Trip: Returned user should have EMPTY password!", result.getListOfUsers().get(0).getPassword());
        Assert.assertNull("Favorite Retrieve by Trip: Returned user should have EMPTY email!", result.getListOfUsers().get(0).getEmail());
        Assert.assertNull("Favorite Retrieve by Trip: Returned user should have EMPTY securityToken!", result.getListOfUsers().get(0).getSecurityToken());
    }
    
    @Test
    public void testRetrieveByUser() throws Exception {
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        ServiceResponse result = FavoriteWSClient.create(faveToCreate); 
        Assert.assertNotNull("Favorite Create: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Create: Result should have been SUCCESS!", result.isSuccess());
        
        //Retrieve Favorite by User:
        User user = new User();
        user.setUsername(mUserCreated.getUsername());
        result = FavoriteWSClient.retrieveByUser(user, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve by User: Returned result can NOT be null!", result);
        Assert.assertTrue("Favorite Retrieve by User: Result should have been SUCCESS!", result.isSuccess());
        if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
            Assert.fail("Favorite Retrieve by User: Result should have list of Trips!");
        }
        Assert.assertEquals("Favorite Retrieve by User: Trip ID invalid!", mTripCreated.getId(), result.getListOfTrips().get(0).getId());
        Assert.assertEquals("Favorite Retrieve by User: Trip Name invalid!", mTripCreated.getName(), result.getListOfTrips().get(0).getName());
        Assert.assertEquals("Favorite Retrieve by User: Trip Note invalid!", mTripCreated.getNote(), result.getListOfTrips().get(0).getNote());
        Assert.assertEquals("Favorite Retrieve by User: Trip Location invalid!", mTripCreated.getLocation(), result.getListOfTrips().get(0).getLocation());
        Assert.assertEquals("Favorite Retrieve by User: Trip User Fullname invalid!", mUserCreated.getFullname(), result.getListOfTrips().get(0).getUserFullname());
        Assert.assertEquals("Favorite Retrieve by User: Trip User username invalid!", mUserCreated.getUsername(), result.getListOfTrips().get(0).getUsername());
    }    
}
