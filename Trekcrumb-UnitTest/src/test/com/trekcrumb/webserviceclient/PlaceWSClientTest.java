package test.com.trekcrumb.webserviceclient;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.PlaceBaseTest;

/**
 * WebService and WebServiceClient Components' tests for entity: Place.
 * @see PlacepBaseTest
 */
public class PlaceWSClientTest 
extends PlaceBaseTest {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = PlaceWSClientTest.class;
        mClassName = "PlaceWSClientTest";

        PlaceBaseTest.oneTimeSetUp();
    }
    

}