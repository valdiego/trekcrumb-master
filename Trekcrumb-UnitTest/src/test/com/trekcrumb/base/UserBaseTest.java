package test.com.trekcrumb.base;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.business.manager.UserManagerTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForComment;
import test.com.trekcrumb.utility.TestUtilForFavorite;
import test.com.trekcrumb.utility.TestUtilForPicture;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;
import test.com.trekcrumb.webserviceclient.UserWSClientTest;

import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.UserDuplicateException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.FileUtil;
import com.trekcrumb.fileserverclient.FileServerClientFactory;
import com.trekcrumb.fileserverclient.IFileServerClient;
import com.trekcrumb.webserviceclient.UserWSClient;

/**
 * Base abstract test class for entity: User
 * 
 * This is abstract class to accomodate similar/duplicate use-cases among layer components - 
 * the most obvious ones are Business, WebService and WebServiceClient compoents. 
 * Test methods cover all possible use-cases for the entity in question. 
 * 
 * Tests on Business component ensure we cover the back-end code which include business 
 * logic/orchestration, database and possibly FileServer integration. WebService, in essence, is 
 * just a wrapper of Business component with exposure to JSON/SOAP technology - therefore it has
 * almost exactly the same use-cases. WebServiceClient is the implementation we built to connect
 * to WebService, hence testing the client is exactly testing the WebService itself. Furthermore,
 * the client use-cases are also similar to that of the other two components.
 * 
 * @author Val Triadi
 */
public abstract class UserBaseTest {
    @SuppressWarnings("rawtypes")
    protected static Class mTestClassType;
    
    protected static String mClassName;
    protected static User mUserCreated;
    
    private static boolean isManagerTest = false;
    private static boolean isWSClientTest = false;

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mTestClassType == UserManagerTest.class) {
            isManagerTest = true;
        
        } else if(mTestClassType == UserWSClientTest.class) {
            isWSClientTest = true;
        
        } else {
            Assert.fail("Cannot continue with test! Unknown incoming test class type: " + mTestClassType);
        }
    }    
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        if(mUserCreated != null) {
            TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        }
        mUserCreated = null;
    }
    
    @Test
    public void testUserCreateAndConfirmAndLogin() throws Exception {
        //Create
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserCreateAndConfirmAndLogin";
        if(isWSClientTest) {
            User userToCreate = new User();
            userToCreate.setEmail(email);
            userToCreate.setUsername(username);
            userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            userToCreate.setFullname(fullname);
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertTrue("User Create: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNull("User Create: Returned response should NOT have a ServiceError!", result.getServiceError());
            Assert.assertNotNull("User Create: Returned response should have been a User bean in it!", result.getUser());
            mUserCreated = result.getUser();
            Assert.assertNotNull("User Create: User ID after create should NOT been empty!", mUserCreated.getUserId());

        } else if(isManagerTest) {
            mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, true);
        }

        //Confirm:
        User userToConfirm = new User();
        userToConfirm.setUsername(username);
        if(isWSClientTest) {
            userToConfirm.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result =  UserWSClient.userConfirm(userToConfirm);
            Assert.assertNotNull("User Confirm: Returned response should NOT be null!", result);
            Assert.assertTrue("User Confirm: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());

        } else if(isManagerTest) {
            userToConfirm.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            boolean confirmResult = UserManager.userConfirm(userToConfirm);
            Assert.assertTrue("User Confirm: New user confirmation should have been SUCCESS!", confirmResult);
        }
        
        //Login:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result =  UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have been a User bean in it!", result.getUser());
            Assert.assertNull("User Login: Returned response should NOT have a UserAuthToken bean in it!", result.getUser().getUserAuthToken());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }

        //Verify user data after login:
        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
        Assert.assertNotNull("User Login: User ID should have not been NULL!", userLogin.getUserId());
        Assert.assertEquals("User Login: Incorrect user email address!", email.toLowerCase(), userLogin.getEmail());
        TestUtilForUser.validateFullname(fullname, userLogin.getFullname());
        Assert.assertEquals("User Login: Incorrect active status!", YesNoEnum.YES, userLogin.getActive());
        Assert.assertEquals("User Login: Incorrect confirmed status!", YesNoEnum.YES, userLogin.getConfirmed());
    }
    
    @Test
    public void testUserCreate_Username_Duplicate() throws Exception {
        //Existing user:
        String username = "UseR" + CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserCreate_Username_Duplicate";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToCreate_New = null;
        User userCreated_New = null;
        String usernameNew = null;
        try {
            //Create duplicate 1:
            usernameNew = username;
            userToCreate_New = new User();
            userToCreate_New.setUsername(usernameNew);
            userToCreate_New.setEmail(CommonUtil.generateID(null) + TestConstants.UNITTEST_EMAIL_SUFFIX);
            userToCreate_New.setFullname(fullname + "_NEW");
            if(isWSClientTest) {
                userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            } else if(isManagerTest) {
                try {
                    userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate username [" + usernameNew + "] error: " + ude.getMessage());
                }
            }
            
            //Create duplicate 2:
            usernameNew = username.toUpperCase();
            userToCreate_New.setUsername(usernameNew);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            } else if(isManagerTest) {
                try {
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate username [" + usernameNew + "] error: " + ude.getMessage());
                }
            }
            
            //Create duplicate 3:
            usernameNew = username.toLowerCase();
            userToCreate_New.setUsername(usernameNew);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            } else if(isManagerTest) {
                try {
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate username [" + usernameNew + "] error: " + ude.getMessage());
                }
            }

        } finally {
            if(userToCreate_New != null) {
                TestUtilForUser.userDelete_byDB(userToCreate_New);
            }
            if(userCreated_New != null) {
                TestUtilForUser.userDelete_byDB(userCreated_New);
            }
        }
    }
    
    @Test
    public void testUserCreate_UsernameAndEmail_Duplicate() throws Exception {
        //Existing user:
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserCreate_UsernameAndEmailDuplicate";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToCreate_New = null;
        User userCreated_New = null;
        try {
            //Create duplicate:
            userToCreate_New = new User();
            userToCreate_New.setUserId(CommonConstants.STRING_VALUE_EMPTY);
            userToCreate_New.setUsername(username);
            userToCreate_New.setEmail(email);
            userToCreate_New.setFullname(fullname + "_NEW");
            if(isWSClientTest) {
                userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            
            } else if(isManagerTest) {
                try {
                    userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate email [" + email + "] error: " + ude.getMessage());
                }
            }            

        } finally {
            if(userToCreate_New != null) {
                TestUtilForUser.userDelete_byDB(userToCreate_New);
            }
            if(userCreated_New != null) {
                TestUtilForUser.userDelete_byDB(userCreated_New);
            }
        }
    }        
    
    @Test
    public void testUserCreate_Username_Invalid() throws Exception {
        User userToCreate = new User();
        userToCreate.setEmail(CommonUtil.generateID("Email") + TestConstants.UNITTEST_EMAIL_SUFFIX);
        userToCreate.setFullname(mClassName + "_testUserCreate_Username_Invalid");

        //Invalid - empty:
        String usernameInvalid = "";
        userToCreate.setUsername(usernameInvalid);
        if(isWSClientTest) {
            userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for username [" + usernameInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for username [" + usernameInvalid + "] error: " + ile.getMessage());
            }
        }

        //Invalid - empty space:
        usernameInvalid = "user name";
        userToCreate.setUsername(usernameInvalid);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for username [" + usernameInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for username [" + usernameInvalid + "] error: " + ile.getMessage());
            }
        }

        //Invalid - chars:
        String[] invalidCharsArray = {
                "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=",
                "{", "}", "[", "]", "|", "\\", ":", ";", "\"", "'", "<", ">", ",", ".", "?", "/"}; 
        for(int i = 0; i < invalidCharsArray.length; i++) {
            if(isManagerTest) {
                try {
                    usernameInvalid = "username" + invalidCharsArray[i];
                    userToCreate.setUsername(usernameInvalid);
                    UserManager.userCreate(userToCreate);
                    Assert.fail("User Create: The system should have thrown validation error for username [" + usernameInvalid + "]!");
                } catch(IllegalArgumentException ile) {
                    System.out.println("Expected exception for username [" + usernameInvalid + "] error: " + ile.getMessage());
                }
            }
        }
    }    
    
    @Test
    public void testUserCreate_Email_Duplicate() throws Exception {
        //Existing user:
        String username = CommonUtil.generateID(null);
        String email = "EmaiL" + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserCreate_Email_Duplicate";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToCreate_New = null;
        User userCreated_New = null;
        String emailNew = null;
        try {
            //Create duplicate 1:
            emailNew = email;
            userToCreate_New = new User();
            userToCreate_New.setUsername(CommonUtil.generateID(null));
            userToCreate_New.setEmail(emailNew);
            userToCreate_New.setFullname(fullname + "_NEW");
            if(isWSClientTest) {
                userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            } else if(isManagerTest) {
                try {
                    userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate email [" + email + "] error: " + ude.getMessage());
                }
            }            
            
            //Create duplicate 2:
            emailNew = email.toUpperCase();
            userToCreate_New.setEmail(emailNew);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            } else if(isManagerTest) {
                try {
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate email [" + email + "] error: " + ude.getMessage());
                }
            }            
            
            //Create duplicate 3:
            emailNew = email.toLowerCase();
            userToCreate_New.setEmail(emailNew);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            } else if(isManagerTest) {
                try {
                    userCreated_New = UserManager.userCreate(userToCreate_New);
                    Assert.fail("User Create: The system should have thrown UserDuplicateException!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Create: Expected exception for duplicate email [" + email + "] error: " + ude.getMessage());
                }
            }
            
        } finally {
            if(userToCreate_New != null) {
                TestUtilForUser.userDelete_byDB(userToCreate_New);
            }
            if(userCreated_New != null) {
                TestUtilForUser.userDelete_byDB(userCreated_New);
            }
        }
    }    
    
    @Test
    public void testUserCreate_Email_Invalid() throws Exception {
        User userToCreate = new User();
        userToCreate.setUsername(CommonUtil.generateID(null));
        userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
        userToCreate.setFullname(mClassName + "_testUserCreate_Email_Invalid");

        //Invalid - empty:
        String emailInvalid = "";
        userToCreate.setEmail(emailInvalid);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for email [" + emailInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for email [" + emailInvalid + "] error: " + ile.getMessage());
            }
        }

        //Invalid - missing req. chars
        emailInvalid = "abracadabra";
        userToCreate.setEmail(emailInvalid);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for email [" + emailInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for email [" + emailInvalid + "] error: " + ile.getMessage());
            }
        }
        
        //Invalid - missing req. chars
        emailInvalid = "abracadabra@";
        userToCreate.setEmail(emailInvalid);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for email [" + emailInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for email [" + emailInvalid + "] error: " + ile.getMessage());
            }
        }

        //Invalid - missing req. chars
        emailInvalid = "abracadabra.com";
        userToCreate.setEmail(emailInvalid);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for email [" + emailInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for email [" + emailInvalid + "] error: " + ile.getMessage());
            }
        }

        //Invalid - chars
        String[] invalidCharsArray = {
                "~", "`", "!", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=",
                "{", "}", "[", "]", "|", "\\", ":", ";", "\"", "'", "<", ">", ",", "?", "/"}; 
        for(int i = 0; i < invalidCharsArray.length; i++) {
            if(isManagerTest) {
                try {
                    emailInvalid = "abracadabra" + invalidCharsArray[i] + "@test.com";
                    userToCreate.setEmail(emailInvalid);
                    UserManager.userCreate(userToCreate);
                    Assert.fail("User Create: The system should have thrown validation error for email [" + emailInvalid + "]!");
                } catch(IllegalArgumentException ile) {
                    System.out.println("Expected exception for email [" + emailInvalid + "] error: " + ile.getMessage());
                }
            }
        }
    }
    
    @Test
    public void testUserCreate_Password_Invalid() throws Exception {
        Cryptographer cryptoInstance = Cryptographer.getInstance();

        User userToCreate = new User();
        userToCreate.setUsername(CommonUtil.generateID(null));
        userToCreate.setEmail(CommonUtil.generateID("Email") + TestConstants.UNITTEST_EMAIL_SUFFIX);
        userToCreate.setFullname(mClassName + "_testUserCreate_Password_Invalid");

        //Invalid - empty
        String passwordInvalid = "";
        if(isWSClientTest) {
            userToCreate.setPassword(passwordInvalid);
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            userToCreate.setPassword(cryptoInstance.encrypt(passwordInvalid));
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for password [" + passwordInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for password [" + passwordInvalid + "] error: " + ile.getMessage());
            }
        }        
        
        //Invalid - format
        passwordInvalid = "1234567";
        if(isWSClientTest) {
            //Encrypted
            userToCreate.setPassword(cryptoInstance.encrypt(passwordInvalid));
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            //Not encrypted
            userToCreate.setPassword(passwordInvalid);
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for password [" + passwordInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for password [" + passwordInvalid + "] error: " + ile.getMessage());
            }
        }        

        //Invalid - less than min. length
        passwordInvalid = "123";
        if(isWSClientTest) {
            userToCreate.setPassword(passwordInvalid);
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            userToCreate.setPassword(cryptoInstance.encrypt(passwordInvalid));
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for password [" + passwordInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for password [" + passwordInvalid + "] error: " + ile.getMessage());
            }
        }        
        
        //Invalid - more than max. length
        passwordInvalid = "12345678901234567890123456";
        if(isWSClientTest) {
            userToCreate.setPassword(passwordInvalid);
            ServiceResponse result = UserWSClient.userCreate(userToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertFalse("User Create: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            userToCreate.setPassword(cryptoInstance.encrypt(passwordInvalid));
            try {
                mUserCreated = UserManager.userCreate(userToCreate);
                Assert.fail("User Create: The system should have thrown validation error for password [" + passwordInvalid + "]!");
            } catch(IllegalArgumentException ile) {
                System.out.println("Expected exception for password [" + passwordInvalid + "] error: " + ile.getMessage());
            }
        }        
    }        
    
    @Test
    public void testUserCreate_Fullname_Duplicate() throws Exception {
        //Existing user:
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserCreate_Fullname_Duplicate";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToCreate_New = null;
        User userCreated_New = null;
        try {
            userToCreate_New = new User();
            userToCreate_New.setUsername(CommonUtil.generateID(null));
            userToCreate_New.setEmail(CommonUtil.generateID(null) + TestConstants.UNITTEST_EMAIL_SUFFIX);
            userToCreate_New.setFullname(fullname);
            if(isWSClientTest) {
                userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                ServiceResponse result = UserWSClient.userCreate(userToCreate_New);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertTrue("User Create: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                Assert.assertNull("User Create: Returned response should NOT have a ServiceError!", result.getServiceError());
                userCreated_New = result.getUser();
                
            } else if(isManagerTest) {
                userToCreate_New.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                userCreated_New = UserManager.userCreate(userToCreate_New);
            }
            
            //Verify:
            Assert.assertNotNull("User Create: Returned User should NOT be null!", userCreated_New);
            TestUtilForUser.validateFullname(fullname, userCreated_New.getFullname());

        } finally {
            if(userCreated_New != null) {
                TestUtilForUser.userDelete_byDB(userCreated_New);
            }
        }
    }        
    
    @Test
    public void testUserCreate_Fullname_WithChar() throws Exception {
        String[] charsArray = {
                "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=",
                "{", "}", "[", "]", "|", "\\", ":", ";", "\"", "'", "<", ">", ",", ".", "?", "/"}; 
        String fullname = null;
        String username = null;
        User userCreated = null;
        User userToCreate = new User();
        for(int i = 0; i < charsArray.length; i++) {
            username = CommonUtil.generateID(null);
            userToCreate.setUsername(username);
            userToCreate.setEmail(username + TestConstants.UNITTEST_EMAIL_SUFFIX);

            fullname = charsArray[i] + "_testUserCreate_Fullname_WithChar";
            userToCreate.setFullname(fullname);
            try {
                if(isWSClientTest) {
                    userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                    ServiceResponse result = UserWSClient.userCreate(userToCreate);
                    Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                    Assert.assertTrue("User Create: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                    userCreated = result.getUser();
                    
                } else if(isManagerTest) {
                    userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                    userCreated = UserManager.userCreate(userToCreate);
                }
                
                Assert.assertNotNull("User Create: Returned updated User should NOT be null!", userCreated);
                TestUtilForUser.validateFullname(fullname, userCreated.getFullname());

            } finally {
                if(userCreated != null) {
                    TestUtilForUser.userDelete_byDB(userCreated);
                }
            }
        }
    }
    
    @Test
    public void testUserCreate_Fullname_Long() throws Exception {
        User userToCreate = new User();
        String username = CommonUtil.generateID(null);
        userToCreate.setUsername(username);
        userToCreate.setEmail(username + TestConstants.UNITTEST_EMAIL_SUFFIX);
        String fullnameLong = "testUserCreate_Fullname_Long 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        userToCreate.setFullname(fullnameLong);

        User userCreated = null;
        try {
            if(isWSClientTest) {
                userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                ServiceResponse result = UserWSClient.userCreate(userToCreate);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertTrue("User Create: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                userCreated = result.getUser();
                
            } else if(isManagerTest) {
                userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                userCreated = UserManager.userCreate(userToCreate);
            }

            Assert.assertNotNull("User Create: Returned updated User should NOT be null!", userCreated);
            Assert.assertEquals("User Create: Invalid size of fullname!", 
                    CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME, userCreated.getFullname().length());
            TestUtilForUser.validateFullname(fullnameLong, userCreated.getFullname());
            
        } finally {
            if(userCreated != null) {
                TestUtilForUser.userDelete_byDB(userCreated);
            }
        }
    }
    
    @Test
    public void testUserCreate_Fullname_WithNonASCIIChars() throws Exception {
        User userToCreate = new User();
        String username = CommonUtil.generateID(null);
        userToCreate.setUsername(username);
        userToCreate.setEmail(username + TestConstants.UNITTEST_EMAIL_SUFFIX);
        String fullnameNonASCII = "��� testUserCreate_Fullname_WithNonASCIIChars";
        userToCreate.setFullname(fullnameNonASCII);

        User userCreated = null;
        String fullnameASCII = "oau testUserCreate_Fullname_WithNonASCIIChars";
        try {
            if(isWSClientTest) {
                userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
                ServiceResponse result = UserWSClient.userCreate(userToCreate);
                Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
                Assert.assertTrue("User Create: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                userCreated = result.getUser();
                
            } else if(isManagerTest) {
                userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                userCreated = UserManager.userCreate(userToCreate);
            }
            
            Assert.assertNotNull("User Create: Returned updated User should NOT be null!", userCreated);
            TestUtilForUser.validateFullname(fullnameASCII, userCreated.getFullname());

        } finally {
            if(userCreated != null) {
                TestUtilForUser.userDelete_byDB(userCreated);
            }
        }
    }
    
    @Test
    public void testUserCreate_CreateUserAuthToken() throws Exception {
        User userToCreate = new User();
        String username = CommonUtil.generateID(null);
        userToCreate.setUsername(username);
        userToCreate.setEmail(username + TestConstants.UNITTEST_EMAIL_SUFFIX);
        userToCreate.setFullname(mClassName + "_testUserCreate_CreateUserAuthToken");

        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + System.currentTimeMillis();
        UserAuthToken userAuthTokenToCreate = new UserAuthToken();
        userAuthTokenToCreate.setDeviceIdentity(deviceIdentity);
        
        if(isWSClientTest) {
            userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userCreate(userToCreate, userAuthTokenToCreate);
            Assert.assertNotNull("User Create: Returned response should NOT be null!", result);
            Assert.assertTrue("User Create: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            mUserCreated = result.getUser();
            
        } else if(isManagerTest) {
            userToCreate.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userToCreate.setUserAuthToken(userAuthTokenToCreate);
            mUserCreated = UserManager.userCreate(userToCreate);
        }

        //Verify:
        Assert.assertNotNull("User Create: Returned User should NOT be null!", mUserCreated);
        UserAuthToken userAuthTokenCreated = mUserCreated.getUserAuthToken();
        Assert.assertNotNull("UserAuthToken after Create: Returned UserAuthToken should NOT be null!", userAuthTokenCreated);
        Assert.assertNotNull("UserAuthToken after Create: UserAuthToken ID should not be null!", userAuthTokenCreated.getId());
        Assert.assertNotNull("UserAuthToken after Create: UserAuthToken UserID should not be null!", userAuthTokenCreated.getUserId());
        Assert.assertNotNull("UserAuthToken after Create: UserAuthToken AuthToken should not be null!", userAuthTokenCreated.getAuthToken());
        Assert.assertEquals("UserAuthToken after Create: Incorrect device identity!", deviceIdentity, userAuthTokenCreated.getDeviceIdentity());
    }

    @Test
    public void testUserConfirm_Username_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserConfirm_Username_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToConfirm = new User();
        userToConfirm.setUsername("InvalidUsername");
        userToConfirm.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userConfirm(userToConfirm);
            Assert.assertNotNull("User Confirm: Returned response should NOT be null!", result);
            Assert.assertFalse("User Confirm: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Confirm: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Confirm: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
            
        } else if(isManagerTest) {
            try {
                UserManager.userConfirm(userToConfirm);
                Assert.fail("User Confirm: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Confirm: Expected exception: " + unce.getMessage());
            }
        }
    }    

    @Test
    public void testUserConfirm_Password_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserConfirm_Password_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        String invalidPassword = "InvalidPassword";
        String cryptedPwd = null;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            cryptedPwd = cryptoInstance.encrypt(invalidPassword);        
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to encrypt password: " + e.getMessage());
        }
        User userToConfirm = new User();
        userToConfirm.setUsername(username);
        userToConfirm.setPassword(cryptedPwd);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userConfirm(userToConfirm);
            Assert.assertNotNull("User Confirm: Returned response should NOT be null!", result);
            Assert.assertFalse("User Confirm: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Confirm: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Confirm: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
            
        } else if(isManagerTest) {
            try {
                UserManager.userConfirm(userToConfirm);
                Assert.fail("User Confirm: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Confirm: Expected exception: " + unce.getMessage());
            }
        }
    }    
    
    @Test
    public void testUserConfirm_UsernameAndPassword_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserConfirm_UsernameAndPassword_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        String invalidUsername = "InvalidUsername";
        String invalidPassword = "InvalidPassword";
        String cryptedPwd = null;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            cryptedPwd = cryptoInstance.encrypt(invalidPassword);        
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to encrypt password: " + e.getMessage());
        }
        
        User userToConfirm = new User();
        userToConfirm.setUsername(invalidUsername);
        userToConfirm.setPassword(cryptedPwd);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userConfirm(userToConfirm);
            Assert.assertNotNull("User Confirm: Returned response should NOT be null!", result);
            Assert.assertFalse("User Confirm: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Confirm: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Confirm: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                UserManager.userConfirm(userToConfirm);
                Assert.fail("User Confirm: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Confirm: Expected exception: " + unce.getMessage());
            }
        }
    }        

    @Test
    public void testUserLogin_Username_CaseInsensitive() throws Exception {
        String username = "AbC" + System.currentTimeMillis();
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_Username_CaseInsensitive";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Login - username UPPERCASE:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username.toUpperCase());
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result =  UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }

        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
        
        //Login - username LOWERCASE:
        userToLogin.setUsername(username.toLowerCase());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userLogin = UserManager.userLogin(userToLogin);
        }

        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
    }
    
    @Test
    public void testUserLogin_Username_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_Username_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        String invalidUsername = "InvalidUsername";
        User userToLogin = new User();
        userToLogin.setUsername(invalidUsername);
        userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertFalse("User Login: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Login: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                UserManager.userLogin(userToLogin);
                Assert.fail("User Login: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Login: Expected exception: " + unce.getMessage());
            }
        }
    }    
    
    @Test
    public void testUserLogin_UsernameAndPassword_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_UsernameAndPassword_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        String invalidUsername = "InvalidUsername";
        String invalidPassword = "InvalidPassword";
        String cryptedInvalidPwd = null;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            cryptedInvalidPwd = cryptoInstance.encrypt(invalidPassword);        
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to encrypt password: " + e.getMessage());
        }

        User userToLogin = new User();
        userToLogin.setUsername(invalidUsername);
        userToLogin.setPassword(cryptedInvalidPwd);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertFalse("User Login: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Login: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                UserManager.userLogin(userToLogin);
                Assert.fail("User Login: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Login: Expected exception: " + unce.getMessage());
            }
        }
    }        
    
    @Test
    public void testUserLogin_Password_CaseSensitive() throws Exception {
        Cryptographer cryptoInstance = Cryptographer.getInstance();

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "testUserLogin_Password_CaseSensitive";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Login - password UPPERCASE
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD.toUpperCase());
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertFalse("User Login: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Login: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            String pwdUppercase = cryptoInstance.encrypt(TestConstants.UNITTEST_USER_PASSWORD.toUpperCase());        
            userToLogin.setPassword(pwdUppercase);

            try {
                UserManager.userLogin(userToLogin);
                Assert.fail("User Login: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Login: Expected exception: " + unce.getMessage());
            }
        }
        
        //Login - password LOWERCASE
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD.toLowerCase());
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertFalse("User Login: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Login: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            String pwdUppercase = cryptoInstance.encrypt(TestConstants.UNITTEST_USER_PASSWORD.toLowerCase());        
            userToLogin.setPassword(pwdUppercase);

            try {
                UserManager.userLogin(userToLogin);
                Assert.fail("User Login: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Login: Expected exception: " + unce.getMessage());
            }
        }    
    }    

    @Test
    public void testUserLogin_Password_Invalid() throws Exception {
        Cryptographer cryptoInstance = Cryptographer.getInstance();

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_Password_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        String invalidPassword = "InvalidPassword";
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(invalidPassword);
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertFalse("User Login: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Login: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            String cryptedInvalidPwd = cryptoInstance.encrypt(invalidPassword);        
            userToLogin.setPassword(cryptedInvalidPwd);

            try {
                UserManager.userLogin(userToLogin);
                Assert.fail("User Login: The system should have thrown UserNotFoundException!");
            } catch(UserNotFoundException unce) {
                System.out.println("User Login: Expected exception: " + unce.getMessage());
            }
        }
    }

    @Test
    public void testUserLogin_CreateUserAuthToken() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_CreateUserAuthToken";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Prepare to login:
        User userLogin = null;
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + System.currentTimeMillis();
        UserAuthToken userAuthTokenToCreate = new UserAuthToken();
        userAuthTokenToCreate.setDeviceIdentity(deviceIdentity);

        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userLogin(userToLogin, userAuthTokenToCreate);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have been a User bean in it!", result.getUser());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userToLogin.setUserAuthToken(userAuthTokenToCreate);
            userLogin = UserManager.userLogin(userToLogin);
        }

        //Verify:
        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
        Assert.assertNotNull("User Login: UserAuthToken should have not been NULL!", userLogin.getUserAuthToken());
        UserAuthToken userAuthTokenCreated = userLogin.getUserAuthToken();
        Assert.assertNotNull("UserAuthToken after Login: UserAuthToken ID should not be null!", userAuthTokenCreated.getId());
        Assert.assertNotNull("UserAuthToken after Login: UserAuthToken UserID should not be null!", userAuthTokenCreated.getUserId());
        Assert.assertNotNull("UserAuthToken after Login: UserAuthToken AuthToken should not be null!", userAuthTokenCreated.getAuthToken());
        Assert.assertEquals("UserAuthToken after Login: Incorrect device identity!", deviceIdentity, userAuthTokenCreated.getDeviceIdentity());
    }
    
    @Test
    public void testUserLogin_CreateUserAuthToken_withUserAuthTokensList() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_CreateUserAuthToken_withUserAuthTokensList";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Add existing auth tokens:
        String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, true);

        String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, true);

        //Prepping to login:
        User userLogin = null;
        
        String deviceIdentity_3 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + System.currentTimeMillis();
        UserAuthToken userAuthTokenToCreate = new UserAuthToken();
        userAuthTokenToCreate.setDeviceIdentity(deviceIdentity_3);
        
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userLogin(userToLogin, userAuthTokenToCreate);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have been a User bean in it!", result.getUser());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userToLogin.setUserAuthToken(userAuthTokenToCreate);
            userLogin = UserManager.userLogin(userToLogin);
        }

        //Verify:
        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
        Assert.assertNotNull("User Login: UserAuthToken should have not been NULL!", userLogin.getUserAuthToken());
        UserAuthToken userAuthTokenCreated = userLogin.getUserAuthToken();
        Assert.assertNotNull("UserAuthToken after Login: UserAuthToken ID should not be null!", userAuthTokenCreated.getId());
        Assert.assertNotNull("UserAuthToken after Login: UserAuthToken UserID should not be null!", userAuthTokenCreated.getUserId());
        Assert.assertNotNull("UserAuthToken after Login: UserAuthToken AuthToken should not be null!", userAuthTokenCreated.getAuthToken());
        Assert.assertEquals("UserAuthToken after Login: Incorrect device identity!", deviceIdentity_3, userAuthTokenCreated.getDeviceIdentity());
        
        List<UserAuthToken> listOfUserAuthTokens = userLogin.getListOfAuthTokens();
        Assert.assertNotNull("UserAuthToken List after Login: The list should have not been NULL!", listOfUserAuthTokens);
        Assert.assertEquals("UserAuthToken List after Login: Incorrect number of UserAuthTokens!", 3, listOfUserAuthTokens.size());
    }
    
    @Test
    public void testUserLogin_WithEnrichementData() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_WithEnrichedData";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Add several trips:
        Trip tripPublic = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        Trip tripPrivate = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PRIVATE, null, false); 
        Trip tripFriendsOnly = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.FRIENDS, null, false); 
        Trip tripToUpdate = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Trip tripLastCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Add several pictures (no need for real img files):
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPublic.getId(), mClassName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPrivate.getId(), mClassName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripFriendsOnly.getId(), mClassName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        //Add favorite:
        TestUtilForFavorite.faveCreateByFavoriteeDB(tripPublic.getId(), mUserCreated.getUserId());

        //Add comments:
        TestUtilForComment.commentCreateByCommentDB(tripPublic.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        TestUtilForComment.commentCreateByCommentDB(tripPublic.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);

         //Edit tripToUpdate:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        tripToUpdate.setNote(TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED - testUserLogin_WithEnrichementData");
        Trip tripUpdated = TripManager.update(tripToUpdate);
        Assert.assertNotNull("Trip update: Returned updated Trip should NOT be null!", tripUpdated);

        //Login:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }
        
        //Verify:
        Assert.assertNotNull("User Login enrichment: Returned user should NOT be null!", userLogin);
        Assert.assertEquals("User Login enrichment: Incorrect number of total Trips!", 5, userLogin.getNumOfTotalTrips());

        Trip TripLastCreatedFromUser = userLogin.getLastCreatedTrip();
        Assert.assertNotNull("User Login enrichment: Trip last created should NOT be null!", TripLastCreatedFromUser);
        Assert.assertEquals("User Login enrichment: Invalid TripID for the Trip last created!", tripLastCreated.getId(), TripLastCreatedFromUser.getId());

        Trip tripLastUpdatedFromUser = userLogin.getLastUpdatedTrip();
        Assert.assertNotNull("User Login enrichment: Last updated Trip should NOT be null!", tripLastUpdatedFromUser);
        Assert.assertEquals("User Login enrichment: Invalid TripID for the Trip last updated!", tripUpdated.getId(), tripLastUpdatedFromUser.getId());

        Assert.assertEquals("User Login enrichment: Incorrect number of total Pictures!", 3, userLogin.getNumOfTotalPictures());
        Assert.assertEquals("User Login enrichment: Incorrect number of Favorites!", 1, userLogin.getNumOfFavorites());
        Assert.assertEquals("User Login enrichment: Incorrect number of Comments!", 2, userLogin.getNumOfComments());
    }
    
    @Test
    public void testUserLogin_withUserAuthTokenList() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_withUserAuthTokenList";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Add auth tokens:
        String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, true);

        String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, true);

        //Login:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }
        
        //Verify:
        Assert.assertNotNull("User Login enrichment: Returned user should NOT be null!", userLogin);
        Assert.assertNotNull("User Login enrichment: Incorrect number of UserAuthTokens!", userLogin.getListOfAuthTokens());
        Assert.assertEquals("User Login enrichment: Incorrect number of UserAuthTokens!", 2, userLogin.getListOfAuthTokens().size());
        for(UserAuthToken userAuthToken : userLogin.getListOfAuthTokens()) {
            Assert.assertNotNull("User Login UserAuthToken: Incorrect UserAuthToken ID!", userAuthToken.getId());
            Assert.assertEquals("User Login UserAuthToken: incorrect User ID!", userLogin.getUserId(), userAuthToken.getUserId());
            Assert.assertNotNull("User Login UserAuthToken: Incorrect device identity!", userAuthToken.getDeviceIdentity());
            Assert.assertNull("User Login UserAuthToken: AuthToken value must be EMPTY!", userAuthToken.getAuthToken());
        }            
    }
    
    @Test
    public void testUserRetrieve_ByUserID() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserRetrieve_ByUserID";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Retrieve:
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setUserId(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound.get(0));
        User userFound = listOfUsersFound.get(0);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());
        Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, userFound.getActive());
        Assert.assertTrue("User Create: Incorrect username! Expected [" + username + "] but [" + userFound.getUsername() + "]", 
                username.equalsIgnoreCase(userFound.getUsername()));
        TestUtilForUser.validateFullname(fullname, userFound.getFullname());

        //Verify 'private/secure data':
        Assert.assertNull("User Retrieve: Password should be NULL!", userFound.getPassword());
        Assert.assertNull("User Retrieve: Email should be NULL!", userFound.getEmail());
    }
    
    @Test
    public void testUserRetrieve_ByUsername() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserRetrieve_ByUsername";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Retrieve:
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setUsername(username);
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound.get(0));
        User userFound = listOfUsersFound.get(0);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());
        Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, userFound.getActive());
        Assert.assertTrue("User Create: Incorrect username! Expected [" + username + "] but [" + userFound.getUsername() + "]", 
                username.equalsIgnoreCase(userFound.getUsername()));
        TestUtilForUser.validateFullname(fullname, userFound.getFullname());
        
        //Verify 'private/secure data':
        Assert.assertNull("User Retrieve: Password should be NULL!", userFound.getPassword());
        Assert.assertNull("User Retrieve: Email should be NULL!", userFound.getEmail());
    }
    
    @Test
    public void testUserRetrieve_ByUsername_CaseInsensitive() throws Exception {
        String username = "AbC" + System.currentTimeMillis();
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserRetrieve_ByUsername_CaseInsensitive";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Retrieve - Uppercase
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setUsername(username.toUpperCase());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        User userFound = listOfUsersFound.get(0);
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", userFound);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());
        
        //Retrieve - Lowercase
        userToRetrieve.setUsername(username.toLowerCase());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        userFound = listOfUsersFound.get(0);
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", userFound);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());
    }    
    
    @Test
    public void testUserRetrieve_ByEmail() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserRetrieve_ByEmail";
        mUserCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, false);

        //Retrieve:
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setEmail(mUserCreated.getEmail());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound.get(0));
        User userFound = listOfUsersFound.get(0);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());
        Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, userFound.getActive());
        Assert.assertTrue("User Create: Incorrect username! Expected [" + username + "] but [" + userFound.getUsername() + "]", 
                username.equalsIgnoreCase(userFound.getUsername()));
        TestUtilForUser.validateFullname(fullname, userFound.getFullname());
        
        //Verify 'private/secure data':
        Assert.assertNull("User Retrieve: Password should be NULL!", userFound.getPassword());
        Assert.assertNull("User Retrieve: Email should be NULL!", userFound.getEmail());
    }
    
    @Test
    public void testUserRetrieve_ByEmail_CaseInsensitive() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserRetrieve_ByEmail_CaseInsensitive";
        mUserCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, false);

        //Retrieve - Uppercase:
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setEmail(email.toUpperCase());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        User userFound = listOfUsersFound.get(0);
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", userFound);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());

        //Retrieve - Lowercase:
        userToRetrieve.setEmail(email.toLowerCase());
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, false, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        userFound = listOfUsersFound.get(0);
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", userFound);
        Assert.assertEquals("User Retrieve: Invalid retrieved userID!", mUserCreated.getUserId(), userFound.getUserId());
    }    
    
    @Test
    public void testUserRetrieve_WithEnrichmentData() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserRetrieve_WithEnrichmentData";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Add several trips:
        Trip tripPublic = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        Trip tripPrivate = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PRIVATE, null, false);
        Trip tripFriendsOnly = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.FRIENDS, null, false);

        //Add several pictures (no need for real img files):
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPublic.getId(), mClassName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPrivate.getId(), mClassName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripFriendsOnly.getId(), mClassName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        //Add favorite:
        TestUtilForFavorite.faveCreateByFavoriteeDB(tripPublic.getId(), mUserCreated.getUserId());

        //Add comments:
        TestUtilForComment.commentCreateByCommentDB(tripPublic.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        TestUtilForComment.commentCreateByCommentDB(tripPublic.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);

        //Retrieve it:
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setUsername(username);
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, true, 0, -1);
        }
        
        //Verify:
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound);
        Assert.assertEquals("User Retrieve: Should have FOUND the user!", 1, listOfUsersFound.size());
        Assert.assertNotNull("User Retrieve: Should have FOUND the user!", listOfUsersFound.get(0));
        User userFound = listOfUsersFound.get(0);
        System.out.println("User found: " + userFound);
        Assert.assertEquals("User Retrieve enrichment: Incorrect number of total Trips!", 1, userFound.getNumOfTotalTrips());
        Assert.assertEquals("User Retrieve enrichment: Incorrect number of total Pictures!", 1, userFound.getNumOfTotalPictures());
        Assert.assertEquals("User Retrieve enrichment: Incorrect number of Favorites!", 1, userFound.getNumOfFavorites());
        Assert.assertEquals("User Retrieve enrichment: Incorrect number of Comments!", 2, userFound.getNumOfComments());
    }
    
    @Test
    public void testUserRetrieve_WithUserAuthToken() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "-testUserRetrieve_ByUserID";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Add auth tokens:
        String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, true);

        //Retrieve:
        List<User> listOfUsersFound = null;
        User userToRetrieve = new User();
        userToRetrieve.setUsername(username);
        if(isWSClientTest) {
            ServiceResponse result =  UserWSClient.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned response should NOT be null!", result);
            Assert.assertTrue("User Retrieve: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Retrieve: Returned response should have FOUND the user!", result.getListOfUsers());
            listOfUsersFound = result.getListOfUsers();

        } else if(isManagerTest) {
            listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, 0, -1);
        }
        
        User userFound = listOfUsersFound.get(0);
        Assert.assertNull("User Retrieve: User Auth Tokens should be NULL!", userFound.getUserAuthToken());
        if(userFound.getListOfAuthTokens() != null) {
            Assert.assertEquals("User Retrieve: UserAuthToken should NOT be returned!", 0, userFound.getListOfAuthTokens().size());
        }
    }
    
    @Test
    public void testUserUpdate_ALL() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "-testUserUpdate_ALL";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update:
        User userUpdated = null;
        String newEmail = "UpdatedEmail" + String.valueOf(System.currentTimeMillis()) + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String newFullname = "AfterUpdate_" + fullname;
        String newSummary = "Here comes Mr. Adventurer. RU ready?";
        String newLocation = "Barracuda, Ocean";
        String newUsername = "shouldNOTChanged";
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setEmail(newEmail);
        userToUpdate.setUsername(newUsername);
        userToUpdate.setFullname(newFullname);
        userToUpdate.setSummary(newSummary);
        userToUpdate.setLocation(newLocation);

        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:
        Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
        Assert.assertNotNull("User Update: User ID should have not been NULL!", userUpdated.getUserId());
        Assert.assertEquals("User Update: Username should NOT changed nor editable!", username.toLowerCase(), userUpdated.getUsername());
        Assert.assertEquals("User Update: Incorrect email address!", newEmail.toLowerCase(), userUpdated.getEmail());
        Assert.assertEquals("User Update: Incorrect summary!", newSummary, userUpdated.getSummary());
        Assert.assertEquals("User Update: Incorrect location!", newLocation, userUpdated.getLocation());
        TestUtilForUser.validateFullname(newFullname, userUpdated.getFullname());

    }           
    
    @Test
    public void testUserUpdate_Fullname_WithChar() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Fullname_WithChar";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update:
        ServiceResponse result = null;
        User userUpdated = null;
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        
        String[] charsArray = {
                "~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "-", "+", "=",
                "{", "}", "[", "]", "|", "\\", ":", ";", "\"", "'", "<", ">", ",", ".", "?", "/"}; 
        String newFullname = null;
        for(int i = 0; i < charsArray.length; i++) {
            newFullname = charsArray[i] + fullname;
            userToUpdate.setFullname(newFullname);
            
            if(isWSClientTest) {
                result = UserWSClient.userUpdate(userToUpdate);
                Assert.assertNotNull("" +
                		"User Update: Returned response should NOT be null for new fullname [" + newFullname + "]", 
                		result);
                Assert.assertTrue(
                        "User Update: Returned response should have been a SUCCESS for new fullname [" + newFullname + "]. Error: \n" 
                        + result, result.isSuccess());
                userUpdated = result.getUser();
            
            } else if(isManagerTest) {
                userUpdated = UserManager.userUpdate(userToUpdate);
            }
            
            Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
            TestUtilForUser.validateFullname(newFullname, userUpdated.getFullname());
        }
    }
    
    @Test
    public void testUserUpdate_StringWithChars() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_StringWithChars";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update:
        User userUpdated = null;
        String newLocation = "UserLocation ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /"; 
        String newSummary = "UserSummary ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /"; 

        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setLocation(newLocation);
        userToUpdate.setSummary(newSummary);
        
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:
        Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
        Assert.assertEquals("User Update: Invalid new location!", newLocation, userUpdated.getLocation());
        Assert.assertEquals("User Update: Invalid new summary!", newSummary, userUpdated.getSummary());
    }
    
    @Test
    public void testUserUpdate_StringEmpty() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_StringEmpty";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update
        User userUpdated = null;
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setFullname(" ");
        userToUpdate.setLocation(" ");
        userToUpdate.setSummary(" ");

        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:
        Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
        Assert.assertEquals("User Update: Invalid new location!", "", userUpdated.getLocation());
        Assert.assertEquals("User Update: Invalid new summary!", "", userUpdated.getSummary());
        
        //Fullname should not change if it was empty string
        TestUtilForUser.validateFullname(fullname, userUpdated.getFullname());
    }
    
    @Test
    public void testUserUpdate_StringNewLines() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_StringNewLines";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update:
        User userUpdated = null;
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setFullname("User fullname \n new line fullname");
        userToUpdate.setLocation("User location \n new line location");
        userToUpdate.setSummary("User summary \n new line summary");

        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:
        Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
        Assert.assertFalse("User Update: Invalid new Fullname: " + userUpdated.getFullname(), userUpdated.getFullname().contains("\n"));
        Assert.assertFalse("User Update: Invalid new location: " + userUpdated.getLocation(), userUpdated.getLocation().contains("\n"));
        Assert.assertFalse("User Update: Invalid new summary: " + userUpdated.getSummary(), userUpdated.getSummary().contains("\n"));
    }
    
    @Test
    public void testUserUpdate_StringLong() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_StringLong";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update:
        User userUpdated = null;
        String fullnameNew = "Fullname 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        String locationNew = 
                "Location 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        String summaryNew = 
                "Summary 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setFullname(fullnameNew);
        userToUpdate.setLocation(locationNew);
        userToUpdate.setSummary(summaryNew);
        
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:
        Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
        Assert.assertEquals("User Update: Invalid size of fullname!", CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME, userUpdated.getFullname().length());
        Assert.assertEquals("User Update: Invalid size of location!", CommonConstants.STRING_VALUE_LENGTH_MAX_USER_LOCATION, userUpdated.getLocation().length());
        Assert.assertEquals("User Update: Invalid size of summary!", CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SUMMARY, userUpdated.getSummary().length());
        Assert.assertEquals("User Update: Invalid new Fullname!", 
                fullnameNew.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME), 
                userUpdated.getFullname());
        Assert.assertEquals("User Update: Invalid new location!", 
                locationNew.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_LOCATION), 
                userUpdated.getLocation());
        Assert.assertEquals("User Update: Invalid new summary!", 
                summaryNew.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SUMMARY), 
                userUpdated.getSummary());
    }
    
    @Test
    public void testUserUpdate_StringWithNonASCIIChars() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_StringWithNonASCIIChars";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);

        //Update:
        User userUpdated = null;
        String fullnameNonASCII = "���" + fullname;
        String locationNonASCII = "Location ���";
        String summaryNonASCII = "Summary ���";
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setFullname(fullnameNonASCII);
        userToUpdate.setLocation(locationNonASCII);
        userToUpdate.setSummary(summaryNonASCII);
        
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:
        Assert.assertNotNull("User Update: Returned updated User should NOT be null!", userUpdated);
        String fullnameASCII = "oau" + fullname;
        String locationASCII = "Location oau";
        String summaryASCII = "Summary oau";
        Assert.assertEquals("User Update: Invalid User location!", locationASCII, userUpdated.getLocation());
        Assert.assertEquals("User Update: Invalid User summary!", summaryASCII, userUpdated.getSummary());
        TestUtilForUser.validateFullname(fullnameASCII, userUpdated.getFullname());
    }

    @Test
    public void testUserUpdate_Email_Duplicate() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Email_Duplicate";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User theOtherUserCreated = null;
        try {
            String otherUsername = "Other" + username;
            String otherEmail = otherUsername + TestConstants.UNITTEST_EMAIL_SUFFIX;
            theOtherUserCreated = TestUtilForUser.userCreate_byManager(otherEmail, otherUsername, fullname, false);
            
            //Update:
            User userToUpdate = new User();
            userToUpdate.setUserId(mUserCreated.getUserId());
            userToUpdate.setEmail(otherEmail);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
                Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
                Assert.assertFalse("User Update: Returned response should have been a FAILED!", result.isSuccess());
                Assert.assertNotNull("User Update: Returned response should have a ServiceError!", result.getServiceError());
                Assert.assertEquals("User Update - Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                        ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, result.getServiceError().getErrorEnum());
            
            } else if(isManagerTest) {
                try {
                    UserManager.userUpdate(userToUpdate);
                    Assert.fail("User Update: The system should have thrown Exception for duplicate email!");
                } catch(UserDuplicateException ude) {
                    System.out.println("User Update: Expected exception: " + ude.getMessage());
                }
            }

        } finally {
            if(theOtherUserCreated != null) {
                TestUtilForUser.userDelete_byDB(theOtherUserCreated);
            }
        }
    }
    
    @Test
    public void testUserUpdate_Email_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Email_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        
        userToUpdate.setEmail("InvalidFormat.hey");
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertFalse("User Update: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Update - Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userUpdate(userToUpdate);
                Assert.fail("User Update: The system should have thrown Exception with invalid format email!");
            } catch(IllegalArgumentException ile) {
                System.out.println("User Update: Expected exception: " + ile.getMessage());
            }
        }

        userToUpdate.setEmail("InvalidFormat@hey");
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertFalse("User Update: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Update - Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userUpdate(userToUpdate);
                Assert.fail("User Update: The system should have thrown Exception with invalid format email!");
            } catch(IllegalArgumentException ile) {
                System.out.println("User Update: Expected exception: " + ile.getMessage());
            }
        }
    }
    
    @Test
    public void testUserUpdate_Email_Empty() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Email_Empty";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update
        User userUpdated = null;
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setEmail(" ");
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userUpdated = result.getUser();
        
        } else if(isManagerTest) {
            userUpdated = UserManager.userUpdate(userToUpdate);
        }
        
        //Verify:        
        Assert.assertNotNull("After Update: Returned updated User should NOT be null!", userUpdated);
        Assert.assertEquals("After Update: Email should NOT changed if it was empty String!", email.toLowerCase(), userUpdated.getEmail());
    }    
    
    @Test
    public void testUserUpdate_Password() throws Exception {
        Cryptographer cryptoInstance = Cryptographer.getInstance();

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Password";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Update:
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());

        String newPassword = "NewPwd";
        String newPasswordCrypted = null;
        if(isWSClientTest) {
            userToUpdate.setPassword(newPassword);
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());

        } else if(isManagerTest) {
            newPasswordCrypted = cryptoInstance.encrypt(newPassword);        
            userToUpdate.setPassword(newPasswordCrypted);
            UserManager.userUpdate(userToUpdate);
        }
        
        //Verify by Login:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(newPassword);
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login after Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login after Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(newPasswordCrypted);
            userLogin = UserManager.userLogin(userToLogin);
        }
        
        Assert.assertNotNull("User Login after Update: Login should be SUCCESS with new password!", userLogin);    
    }
    
    @Test
    public void testUserUpdate_Password_Empty() throws Exception {
        Cryptographer cryptoInstance = Cryptographer.getInstance();

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Password_Empty";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        String newPassword = " ";
        String newPasswordCrypted = null;
        if(isWSClientTest) {
            userToUpdate.setPassword(newPassword);
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());

        } else if(isManagerTest) {
            newPasswordCrypted = cryptoInstance.encrypt(newPassword);        
            userToUpdate.setPassword(newPasswordCrypted);
            UserManager.userUpdate(userToUpdate);
        }
        
        //Verify by Login:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login after Update: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login after Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }
        
        Assert.assertNotNull("User Login after Update: Login should be SUCCESS with OLD password because password should NOT updated with empty String!", userLogin);    
    }
    
    @Test
    public void testUserUpdate_Password_Invalid() throws Exception {
        Cryptographer cryptoInstance = Cryptographer.getInstance();

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_Password_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        
        String newPwd = "123";
        if(isWSClientTest) {
            userToUpdate.setPassword(newPwd);
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertFalse("User Update: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Update: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            userToUpdate.setPassword(cryptoInstance.encrypt(newPwd));
            try {
                UserManager.userUpdate(userToUpdate);
                Assert.fail("User Update: The system should have thrown Exception with invalid password!");
            } catch(IllegalArgumentException ile) {
                System.out.println("User Update: Expected exception: " + ile.getMessage());
            }
        }
        
        newPwd = "12345678901234567890123456";
        if(isWSClientTest) {
            userToUpdate.setPassword(newPwd);
            ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
            Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
            Assert.assertFalse("User Update: Returned response should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Update: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            userToUpdate.setPassword(cryptoInstance.encrypt(newPwd));
            try {
                UserManager.userUpdate(userToUpdate);
                Assert.fail("User Update: The system should have thrown Exception with invalid password!");
            } catch(IllegalArgumentException ile) {
                System.out.println("User Update: Expected exception: " + ile.getMessage());
            }
        }
    }
    
    @Test
    public void testUserUpdate_ProfileImage_Create() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_ProfileImage_Create";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userUpdated = null;
        String profileImageName = null;
        try {
            //Prep request:
            profileImageName = CommonUtil.generatePictureFilename(mClassName);
            byte[] profileImageBytes = FileUtil.readImageFromFileDirectory(
                    TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL);
            Assert.assertNotNull("User profile image create: Image bytes from the file cannot be null!", profileImageBytes);

            User userToUpdate = new User();
            userToUpdate.setUserId(mUserCreated.getUserId());
            userToUpdate.setProfileImageName(profileImageName);
            userToUpdate.setProfileImageBytes(profileImageBytes);
            
            //Execute:
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
                Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
                Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                userUpdated = result.getUser();
            
            } else if(isManagerTest) {
                userUpdated = UserManager.userUpdate(userToUpdate);
            }            
            
            //Verify:
            Assert.assertNotNull("User profile image create: Returned updated User should NOT be null!", userUpdated);
            Assert.assertNotNull("User profile image create: Returned User ID should have not been NULL!", userUpdated.getUserId());
            Assert.assertEquals("User profile image create: Incorrect returned user profile image name!", profileImageName, userUpdated.getProfileImageName());

            //Verify file exists on FS:
            TestUtilForPicture.imageRead_byFSClient(profileImageName, false);

        } finally {
            //Clean up:
            if(profileImageName != null) {
                TestUtilForPicture.imageDelete_byFSClient(profileImageName, false);
            }
        }
    }
    
    /*
     * 2/2018: On 'HEROKU/GOOGLECLOUD" env., this test would fail on "retrieve after delete"
     * due to GOOGLECLOUD image caching/delete issue. 
     * See note on FileServerClientGoogleCloudStorageImpl.java
     */
    @Test
    public void testUserUpdate_ProfileImage_Edit() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserUpdate_ProfileImage_Edit";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userUpdated = null;
        String firstProfileImageName = null;
        String secondProfileImageName = null;
        try {
            byte[] profileImageBytes = FileUtil.readImageFromFileDirectory(
                    TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL);
            Assert.assertNotNull("User profile image create: Image bytes from the file cannot be null!", profileImageBytes);

            //1st profile image:
            firstProfileImageName = CommonUtil.generatePictureFilename(mClassName);
            User userToUpdate = new User();
            userToUpdate.setUserId(mUserCreated.getUserId());
            userToUpdate.setProfileImageName(firstProfileImageName);
            userToUpdate.setProfileImageBytes(profileImageBytes);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
                Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
                Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                userUpdated = result.getUser();
            
            } else if(isManagerTest) {
                userUpdated = UserManager.userUpdate(userToUpdate);
            }            
            
            //Verify:
            Assert.assertNotNull("User profile image create: Returned updated User should NOT be null!", userUpdated);
            Assert.assertNotNull("User profile image create: Returned User ID should have not been NULL!", userUpdated.getUserId());
            Assert.assertEquals("User profile image create: Incorrect returned user profile image name!", firstProfileImageName, userUpdated.getProfileImageName());
            TestUtilForPicture.imageRead_byFSClient(firstProfileImageName, false);
            
            //2nd profile image:
            secondProfileImageName = CommonUtil.generatePictureFilename(mClassName);
            userToUpdate = new User();
            userToUpdate.setUserId(mUserCreated.getUserId());
            userToUpdate.setProfileImageName(secondProfileImageName);
            userToUpdate.setProfileImageBytes(profileImageBytes);
            if(isWSClientTest) {
                ServiceResponse result = UserWSClient.userUpdate(userToUpdate);
                Assert.assertNotNull("User Update: Returned response should NOT be null!", result);
                Assert.assertTrue("User Update: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
                userUpdated = result.getUser();
            
            } else if(isManagerTest) {
                userUpdated = UserManager.userUpdate(userToUpdate);
            }            

            //Verify:
            Assert.assertNotNull("User profile image create: Returned updated User should NOT be null!", userUpdated);
            Assert.assertNotNull("User profile image create: Returned User ID should have not been NULL!", userUpdated.getUserId());
            Assert.assertEquals("User profile image create: Incorrect returned user profile image name!", secondProfileImageName, userUpdated.getProfileImageName());
            TestUtilForPicture.imageRead_byFSClient(secondProfileImageName, false);
            
            //Verify 1st file deleted on FS:
            FileServerRequest requestToRead = new FileServerRequest();
            requestToRead.setFileMenu(FileServerMenuEnum.READ_PROFILE_PICTURE);
            requestToRead.setFilename(firstProfileImageName);
            IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
            FileServerResponse response = fsClient.read(requestToRead);
            Assert.assertFalse("User profile image read after delete: FileServer response should have been a failure!", response.isSuccess());
            Assert.assertNotNull("User profile image read after delete: FileServer response should have ServiceError!", response.getServiceError());
            Assert.assertEquals("User profile image read after delete: FileServer response has invalid service error enum!", 
                    ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND, response.getServiceError().getErrorEnum());
            
        } finally {
            //Clean up:
            if(firstProfileImageName != null) {
                TestUtilForPicture.imageDelete_byFSClient(firstProfileImageName, false);
            }
            if(secondProfileImageName != null) {
                TestUtilForPicture.imageDelete_byFSClient(secondProfileImageName, false);
            }
        }
    }         
    
    @Test
    public void testUserDeactivateAndReactivate() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserDeactivateAndReactivate";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);

        //Deactivate:
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userDeActivate(userToUpdate);
            Assert.assertNotNull("User Deactivate: Result should NOT be null!", result);
            Assert.assertTrue("User Deactivate: Result should have been a SUCCESS! \n" + result, result.isSuccess());
        
        } else if(isManagerTest) {
            boolean isSuccess = UserManager.userDeActivate(userToUpdate);
            Assert.assertTrue("User Deactivate: Returned result should be success!", isSuccess);
        }            

        //Verify by Login: 
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login after Deactivate: Result should NOT be null!", result);
            Assert.assertFalse("User Login after Deactivate: Result after deactivate should be 'FALSE'!", result.isSuccess());
            Assert.assertNotNull("User Login after Deactivate: Result after deactivate should have ServiceError!", result.getServiceError());
            Assert.assertEquals("User Login after Deactivate: Result after deactivate has unexpected ServiceError!", 
                    ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR, result.getServiceError().getErrorEnum());
            userLogin = result.getUser();
        
        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }            
        
        Assert.assertNotNull("User Login after Deactivate: Returned user bean should NOT null!", userLogin);
        Assert.assertEquals("User Login after Deactivate: Unexpected return user active status!", YesNoEnum.NO, userLogin.getActive());
        Assert.assertNotNull("User Login after Deactivate: Returned user should have updated date!", userLogin.getUpdated());
        Assert.assertEquals("User Login after Deactivate: Returned user has incorrect username!", username.toLowerCase(), userLogin.getUsername());
        Assert.assertNull("User Login after Deactivate: Returned user should have NULL email!", userLogin.getEmail());
        Assert.assertNull("User Login after Deactivate: Returned user should have NULL fullname!", userLogin.getFullname());
        
        //Reactivate:
        User userReactivated = null;
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userReActivate(userToUpdate);
            Assert.assertNotNull("User Reactivate: Result should NOT be null!", result);
            Assert.assertTrue("User Reactivate: Result should have been a SUCCESS! \n" + result, result.isSuccess());
            userReactivated = result.getUser();
        
        } else if(isManagerTest) {
            userReactivated = UserManager.userReActivate(userToUpdate);        
        }            
        
        Assert.assertNotNull("User Reactivate: Returned user bean should NOT null!", userReactivated);
        Assert.assertEquals("User Reactivate: Unexpected return user active status!", YesNoEnum.YES, userReactivated.getActive());
        Assert.assertEquals("User Reactivate: Returned user has incorrect username!", username.toLowerCase(), userReactivated.getUsername());
        Assert.assertEquals("User Reactivate: Returned user has incorrect user email address!", email.toLowerCase(), userReactivated.getEmail());
        TestUtilForUser.validateFullname(fullname, userReactivated.getFullname());

        //Verify by Login: 
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login after Deactivate: Result should NOT be null!", result);
            Assert.assertTrue("User Login after Deactivate: Result should have been SUCCESS!", result.isSuccess());
            userLogin = result.getUser();
        
        } else if(isManagerTest) {
            userLogin = UserManager.userLogin(userToLogin);
        }            
        
        Assert.assertNotNull("User Login after Reactivate: Returned user bean should NOT null!", userLogin);
        Assert.assertEquals("User Login after Reactivate: Unexpected return user active status!", YesNoEnum.YES, userLogin.getActive());
    }
    

    @Test
    public void testProfileImage_UploadAndDownload_withFilename() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testProfileImage_UploadAndDownload_withFilename";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PROFILEPICTURE_FILE_JPG);
        Assert.assertNotNull("Profile Image Upload: Image bytes from the file cannot be null!", imgBytes);
        
        String imageFilename = CommonUtil.generatePictureFilename(mClassName);
        try {
            //Upload by Manager (only):
            FileServerResponse fsResponseUpload = 
                    TestUtilForPicture.imageUpload_byManager(mUserCreated.getUserId(), imageFilename, imgBytes, false);
            Assert.assertTrue("Profile Image Upload: The FileServer response should have been a SUCCESS! \n" + fsResponseUpload, fsResponseUpload.isSuccess());
            Assert.assertEquals("Profile Image Upload: Invalid returned filename!", imageFilename, fsResponseUpload.getFilename());

            //Download:
            if(isWSClientTest) {
                User userWithImageToDownload = new User();
                userWithImageToDownload.setProfileImageName(imageFilename);
                ServiceResponse svcResponseDownload = UserWSClient.userProfileImageDownload(userWithImageToDownload);
                Assert.assertTrue("User Profile Image Download: Returned response should have been a SUCCESS! \n" + svcResponseDownload, svcResponseDownload.isSuccess());
                Assert.assertNotNull("User Profile Image Download: Returned response should have User!", svcResponseDownload.getListOfUsers());
                User userFound = svcResponseDownload.getListOfUsers().get(0);
                Assert.assertNotNull("User Profile Image Download: Returned response should have User!", userFound);
                Assert.assertNotNull("User Profile Image Download: The image bytes should have NOT NULL!", userFound.getProfileImageBytes());
            
            } else if(isManagerTest) {
                FileServerResponse fsResponseDownload = TestUtilForPicture.imageDownload_byManager(imageFilename, false);
                Assert.assertTrue("Profile Image Download: The FileServer response should have been a SUCCESS! \n" + fsResponseDownload, fsResponseDownload.isSuccess());
                Assert.assertNotNull("Profile Image Download:  The image bytes should have NOT NULL!", fsResponseDownload.getFileBytes());
            }            
            
        } finally {
            //Delete:
            try {
                TestUtilForPicture.imageDelete_byFSClient(imageFilename, false);
            } catch(Exception e) {
                //do nothing
            }
        }
    }    

    @Test
    public void testProfileImage_UploadAndDownload_NoFilename() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testProfileImage_UploadAndDownload_NoFilename";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);

        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PROFILEPICTURE_FILE_JPG);
        Assert.assertNotNull("Profile Image Upload: Image bytes from the file cannot be null!", imgBytes);
        
        String imageFilename = null;
        try {
            //Upload by Manager (only):
            FileServerResponse fsResponseUpload = 
                    TestUtilForPicture.imageUpload_byManager(mUserCreated.getUserId(), null, imgBytes, false);
            Assert.assertTrue("Profile Image Upload: The FileServer response should have been a SUCCESS! \n" + fsResponseUpload, fsResponseUpload.isSuccess());
            Assert.assertNotNull("Profile Image Upload: The FileServer response should have filename!", fsResponseUpload.getFilename());
            imageFilename = fsResponseUpload.getFilename();

            //Download:
            if(isWSClientTest) {
                User userWithImageToDownload = new User();
                userWithImageToDownload.setProfileImageName(imageFilename);
                ServiceResponse svcResponseDownload = UserWSClient.userProfileImageDownload(userWithImageToDownload);
                Assert.assertTrue("User Profile Image Download: Returned response should have been a SUCCESS! \n" + svcResponseDownload, svcResponseDownload.isSuccess());
                Assert.assertNotNull("User Profile Image Download: Returned response should have User!", svcResponseDownload.getListOfUsers());
                User userFound = svcResponseDownload.getListOfUsers().get(0);
                Assert.assertNotNull("User Profile Image Download: Returned response should have User!", userFound);
                Assert.assertNotNull("User Profile Image Download: The image bytes should have NOT NULL!", userFound.getProfileImageBytes());
            
            } else if(isManagerTest) {
                FileServerResponse fsResponseDownload = TestUtilForPicture.imageDownload_byManager(imageFilename, false);
                Assert.assertTrue("Profile Image Download: The FileServer response should have been a SUCCESS! \n" + fsResponseDownload, fsResponseDownload.isSuccess());
                Assert.assertNotNull("Profile Image Download:  The image bytes should have NOT NULL!", fsResponseDownload.getFileBytes());
            }            
        } finally {
            //Delete:
            try {
                TestUtilForPicture.imageDelete_byFSClient(imageFilename, false);
            } catch(Exception e) {
                //do nothing
            }
        }
    }
    
    @Test
    public void testUserPasswordForgetAndReset() throws Exception {
        //Setup existing User:
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "-testUserPasswordForgetReset";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Register:
        String securityToken = null;
        if(isWSClientTest) {
            securityToken = TestUtilForUser.userForgetRegister_byWSClient(username, email);
        } else if(isManagerTest) {
            securityToken = TestUtilForUser.userForgetRegister_byManager(username, email);
        }            
        
        //Reset Password:
        User userReset = null;
        User userWhoResetPassword = new User();
        userWhoResetPassword.setEmail(email);
        userWhoResetPassword.setSecurityToken(securityToken);
        String passwordNew = "passwordIsNEW!";
        String passwordNewCrypted = null;
        if(isWSClientTest) {
            userWhoResetPassword.setPassword(passwordNew);
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertTrue("User Password Reset: Result should have been a SUCCESS! \n" + result, result.isSuccess());       
            Assert.assertNotNull("User Password Reset: Result should have a User bean!", result.getUser());
            userReset = result.getUser();
        
        } else if(isManagerTest) {
            try {
                Cryptographer cryptoInstance = Cryptographer.getInstance();
                passwordNewCrypted = cryptoInstance.encrypt(passwordNew);        
            } catch(Exception e) {
                e.printStackTrace();
                Assert.fail("Fail to encrypt password: " + e.getMessage());
            }
            userWhoResetPassword.setPassword(passwordNewCrypted);
            userReset = UserManager.userPasswordReset(userWhoResetPassword);
        }            
        
        Assert.assertNotNull("User Password Reset: Returned user should have NOT EMPTY!", userReset);
        Assert.assertEquals("User Password Reset: Returned username invalid!", username.toLowerCase(), userReset.getUsername());
        Assert.assertEquals("User Password Reset: Returned email invalid!", email.toLowerCase(), userReset.getEmail());
        Assert.assertNull("User Password Reset: Returned security token should have been EMPTY!", userReset.getSecurityToken());
        
        //Verify by Login
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(passwordNew);
            ServiceResponse result =  UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Result should NOT be null!", result);
            Assert.assertTrue("User Login: Result should have been a SUCCESS! \n" + result, result.isSuccess());
            userLogin = result.getUser();
        
        } else if(isManagerTest) {
            userToLogin.setPassword(passwordNewCrypted);
            userLogin = UserManager.userLogin(userToLogin);
        }            
        
        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
    }
    
    @Test
    public void testUserPasswordForget_Email_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "_testUserPasswordForget_Email_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Register - Email = EMPTY
        User userWhoForget = new User();
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userPasswordForget(userWhoForget);
            Assert.assertNotNull("User Password Forget: Result should NOT be null!", result);
            Assert.assertFalse("User Password Forget: Result should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Password Forget: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Forget: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userPasswordForget(userWhoForget);
                Assert.fail("User Password Forget: Test should have FAILED when input email is NULL!");
            } catch(IllegalArgumentException iae) {
                //Expected:
            }
        }            

        //Register - Email = NON-EXISTENCE
        userWhoForget.setEmail("emailNotExist@exes.ohs");
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userPasswordForget(userWhoForget);
            Assert.assertNotNull("User Password Forget: Result should NOT be null!", result);
            Assert.assertFalse("User Password Forget: Result should have been a FAILED!", result.isSuccess());
            Assert.assertNotNull("User Password Forget: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Forget: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            try {
                UserManager.userPasswordForget(userWhoForget);
                Assert.fail("User Password Forget: Test should have FAILED when input email is non existence!");
            } catch(UserNotFoundException unfe) {
                //Expected:
            }
        }            
    }
    
    @Test
    public void testUserPasswordReset_Email_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "_testUserPasswordReset_Email_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Register:
        String securityToken = null;
        if(isWSClientTest) {
            securityToken = TestUtilForUser.userForgetRegister_byWSClient(username, email);
        } else if(isManagerTest) {
            securityToken = TestUtilForUser.userForgetRegister_byManager(username, email);
        }            
        
        //Reset Password - Email = EMPTY:
        User userWhoResetPassword = new User();
        userWhoResetPassword.setSecurityToken(securityToken);
        userWhoResetPassword.setEmail(null);
        if(isWSClientTest) {
            userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED when email is NULL!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input email is NULL!");
            } catch(IllegalArgumentException iae) {
                //Expected:
            }
        }            
        
        //Reset Password - Email = NON EXISTENCE:
        userWhoResetPassword.setEmail("emailNoExist@exes.ohs");
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED is non existence!!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            try {
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input email is non existence!");
            } catch(UserNotFoundException unfe) {
                //Expected:
            }
        }            
    }
    
    @Test
    public void testUserPasswordReset_SecurityToken_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "_testUserPasswordReset_SecurityToken_Invalid()";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Register:
        String securityToken = null;
        if(isWSClientTest) {
            securityToken = TestUtilForUser.userForgetRegister_byWSClient(username, email);
        } else if(isManagerTest) {
            securityToken = TestUtilForUser.userForgetRegister_byManager(username, email);
        }            
        
        //Reset Password - SecurityToken = EMPTY
        User userWhoResetPassword = new User();
        userWhoResetPassword.setEmail(email);
        userWhoResetPassword.setSecurityToken(null);
        if(isWSClientTest) {
            userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED when input securityToken is NULL!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input securityToken is NULL!");
            } catch(IllegalArgumentException iae) {
                System.err.println("Expected error: " + iae.getMessage());
            }
        }            
        
        //Reset Password - SecurityToken = INVALID
        userWhoResetPassword.setSecurityToken("InvalidSecurityToken");
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED when input securityToken is invalid!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input securityToken is invalid!");
            } catch(IllegalArgumentException iae) {
                System.err.println("Expected error: " + iae.getMessage());
            }
        }            
        
        //Reset Password - Twice
        userWhoResetPassword.setSecurityToken(securityToken);
        if(isWSClientTest) {
            UserWSClient.userPasswordReset(userWhoResetPassword);
            
            //Reset again:
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Returned response should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Returned response should have FAILED when trying to reset it twice with the same securityToken!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            UserManager.userPasswordReset(userWhoResetPassword);

            //Reset again:
            try {
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when trying to reset it twice with the same securityToken!");
            } catch(IllegalArgumentException iae) {
                System.err.println("Expected error: " + iae.getMessage());
            }
        }            
    }
    
    @Test
    public void testUserPasswordReset_Password_Invalid() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "_testUserPasswordReset_Password_Invalid";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Register:
        String securityToken = null;
        if(isWSClientTest) {
            securityToken = TestUtilForUser.userForgetRegister_byWSClient(username, email);
        } else if(isManagerTest) {
            securityToken = TestUtilForUser.userForgetRegister_byManager(username, email);
        }            
        
        //Reset Password - Password = EMPTY
        User userWhoResetPassword = new User();
        userWhoResetPassword.setEmail(email);
        userWhoResetPassword.setSecurityToken(securityToken);
        userWhoResetPassword.setPassword(null);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED when input password is NULL!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input password is NULL!");
            } catch(IllegalArgumentException iae) {
                System.err.println("Expected error: " + iae.getMessage());
            }
        }            

        //Reset Password - Password = UNCRYPTED
        if(isManagerTest) {
            userWhoResetPassword.setPassword("pwdNewNotCrypted");
            try {
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input password is not crypted!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }
    }
    
    @Test
    public void testUserPasswordForgetAndReset_UserDeactivated() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "-testUserPasswordForgetReset";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Deactivate User:
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userDeActivate(userToUpdate);
            Assert.assertNotNull("User Deactivate: Result should NOT be null!", result);
            Assert.assertTrue("User Deactivate: Result should have been a SUCCESS! \n" + result, result.isSuccess());
        
        } else if(isManagerTest) {
            boolean isSuccess = UserManager.userDeActivate(userToUpdate);
            Assert.assertTrue("User Deactivate: Returned result should be success!", isSuccess);
        }        

        //Register:
        String securityToken = null;
        if(isWSClientTest) {
            securityToken = TestUtilForUser.userForgetRegister_byWSClient(username, email);
        } else if(isManagerTest) {
            securityToken = TestUtilForUser.userForgetRegister_byManager(username, email);
        }            
        
        //Reset Password:
        User userReset = null;
        User userWhoResetPassword = new User();
        userWhoResetPassword.setEmail(email);
        userWhoResetPassword.setSecurityToken(securityToken);
        if(isWSClientTest) {
            userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED when user has been 'deactivated'!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Result for deactivated user should have ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Result for deactivated user has unexpected ServiceError!", 
                    ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR, result.getServiceError().getErrorEnum());
            userReset = result.getUser();
        
        } else if(isManagerTest) {
            userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userReset = UserManager.userPasswordReset(userWhoResetPassword);
        }            

        Assert.assertNotNull("User Password Reset: Returned user should have NOT EMPTY!", userReset);
        Assert.assertNull("User Password Reset: Returned security token should have been EMPTY!", userReset.getSecurityToken());
        Assert.assertEquals("User Password Reset: Returned user should have been deactivated!", YesNoEnum.NO, userReset.getActive());
        Assert.assertEquals("User Password Reset: Returned reactivated user has incorrect username!", username.toLowerCase(), userReset.getUsername());
        Assert.assertNull("User Password Reset: Returned deactivated user should have NULL email!", userReset.getEmail());
        Assert.assertNull("User Password Reset: Returned deactivated user should have NULL fullname!", userReset.getFullname());
    }    
    
    @Test
    public void testUserPasswordForgetAndReset_LoginWithoutReset() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = TestConstants.UNITTEST_EMAIL_USER;
        String fullname = mClassName + "-testUserPasswordForgetReset";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        //Register:
        String securityToken = null;
        if(isWSClientTest) {
            securityToken = TestUtilForUser.userForgetRegister_byWSClient(username, email);
        } else if(isManagerTest) {
            securityToken = TestUtilForUser.userForgetRegister_byManager(username, email);
        }            
        
        //Login without Reset:
        User userLogin = null;
        User userToLogin = new User();
        userToLogin.setUsername(username);
        if(isWSClientTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result =  UserWSClient.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned response should NOT be null!", result);
            Assert.assertTrue("User Login: Returned response should have been a SUCCESS! \n" + result, result.isSuccess());
            Assert.assertNotNull("User Login: Returned response should have been a User bean in it!", result.getUser());
            Assert.assertNull("User Login: Returned response should NOT have a UserAuthToken bean in it!", result.getUser().getUserAuthToken());
            userLogin = result.getUser();

        } else if(isManagerTest) {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            userLogin = UserManager.userLogin(userToLogin);
        }
        
        Assert.assertNotNull("User Login: Returned user should NOT be null!", userLogin);
        Assert.assertNull("User Login: Returned user should NOT have any SecurityToken!", userLogin.getSecurityToken());
        
        //Verify back-end DB:
        User userFound = TestUtilForUser.userRead_byDB(username);
        Assert.assertNotNull("User Retrieve: Should have found user in DB given username [" + username + "]!", userFound);
        Assert.assertEquals("User Retrieve: Returned user SecurityToken should BE EMPTY!", "", userFound.getSecurityToken());
        
        //Reset Password:
        User userWhoResetPassword = new User();
        userWhoResetPassword.setEmail(email);
        userWhoResetPassword.setSecurityToken(securityToken);
        if(isWSClientTest) {
            userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            ServiceResponse result = UserWSClient.userPasswordReset(userWhoResetPassword);
            Assert.assertNotNull("User Password Reset: Result should NOT be null!", result);
            Assert.assertFalse("User Password Reset: Result should have been a FAILED when input securityToken is EXPIRED!", result.isSuccess());
            Assert.assertNotNull("User Password Reset: Result should have ServiceError!", result.getServiceError());
            Assert.assertEquals("User Password Reset: Result has unexpected ServiceError!", 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            userWhoResetPassword.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            try {
                UserManager.userPasswordReset(userWhoResetPassword);
                Assert.fail("User Password Reset: Test should have FAILED when input securityToken is EXPIRED!");
           } catch(IllegalArgumentException iae) {
                //Expected
           }
        }            
    }
    
    @Test
    public void testUserContactUs() throws Exception {
        User userWhoContactUs = new User();
        userWhoContactUs.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoContactUs.setFullname("Unit Test");
        userWhoContactUs.setSummary(TestConstants.UNITTEST_EMAIL_USERCONTACTUS_BODY);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userContactUs(userWhoContactUs);
            Assert.assertNotNull("User Contact Us: Result should NOT be null!", result);
            Assert.assertTrue("User Contact Us: Result should have been SUCCESS! \n" + result, result.isSuccess());
        
        } else if(isManagerTest) {
            UserManager.userContactUs(userWhoContactUs);
        }            
    }

    @Test
    public void testUserContactUs_Params_Invalid() throws Exception {
        User userWhoContactUs = new User();
        
        //Email = empty
        userWhoContactUs.setEmail(null);
        userWhoContactUs.setSummary(TestConstants.UNITTEST_EMAIL_USERCONTACTUS_BODY);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userContactUs(userWhoContactUs);
            Assert.assertNotNull("User Contact Us: Result should NOT be null!", result);
            Assert.assertFalse("User Contact Us: Result should have been a FAILED when email is empty!", result.isSuccess());
            Assert.assertNotNull("User Contact Us: Result should have ServiceError!", result.getServiceError());
            Assert.assertEquals("User Contact Us: Result has unexpected ServiceError!", 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userContactUs(userWhoContactUs);
                Assert.fail("User Contact Us: Test should have FAILED when input email is empty!");
           } catch(IllegalArgumentException iae) {
                //Expected
           }
        }
        
        //Summary = empty
        userWhoContactUs.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoContactUs.setSummary(null);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userContactUs(userWhoContactUs);
            Assert.assertNotNull("User Contact Us: Result should NOT be null!", result);
            Assert.assertFalse("User Contact Us: Result should have been a FAILED when summary is empty!", result.isSuccess());
            Assert.assertNotNull("User Contact Us: Result should have ServiceError!", result.getServiceError());
            Assert.assertEquals("User Contact Us: Result has unexpected ServiceError!", 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        
        } else if(isManagerTest) {
            try {
                UserManager.userContactUs(userWhoContactUs);
                Assert.fail("User Contact Us: Test should have FAILED when input summary is empty!");
           } catch(IllegalArgumentException iae) {
                //Expected
           }
        }

        //Fullname = empty
        userWhoContactUs.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoContactUs.setSummary(TestConstants.UNITTEST_EMAIL_USERCONTACTUS_BODY);
        if(isWSClientTest) {
            ServiceResponse result = UserWSClient.userContactUs(userWhoContactUs);
            Assert.assertNotNull("User Contact Us: Result should NOT be null!", result);
            Assert.assertTrue("User Contact Us: Result should have been SUCCESS even if fullname is empty! \n" + result, result.isSuccess());
        
        } else if(isManagerTest) {
            try {
                UserManager.userContactUs(userWhoContactUs);
           } catch(Exception e) {
               Assert.fail("User Contact Us: Test should have SUCCESS even if fullname is empty!");
           }
        }
    }
    
}
