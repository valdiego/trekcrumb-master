package test.com.trekcrumb.base;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.business.manager.TripManagerTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForComment;
import test.com.trekcrumb.utility.TestUtilForFavorite;
import test.com.trekcrumb.utility.TestUtilForPicture;
import test.com.trekcrumb.utility.TestUtilForPlace;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.webserviceclient.TripWSClientTest;

import com.trekcrumb.business.manager.CommentManager;
import com.trekcrumb.business.manager.FavoriteManager;
import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.webserviceclient.TripWSClient;

/**
 * Base abstract test class for entity: Trip
 * 
 * This is abstract class to accomodate similar/duplicate use-cases among layer components - 
 * the most obvious ones are Business, WebService and WebServiceClient compoents. 
 * Test methods cover all possible use-cases for the entity in question. 
 * 
 * Tests on Business component ensure we cover the back-end code which include business 
 * logic/orchestration, database and possibly FileServer integration. WebService, in essence, is 
 * just a wrapper of Business component with exposure to JSON/SOAP technology - therefore it has
 * almost exactly the same use-cases. WebServiceClient is the implementation we built to connect
 * to WebService, hence testing the client is exactly testing the WebService itself. Furthermore,
 * the client use-cases are also similar to that of the other two components.
 * 
 * @author Val Triadi
 */
public abstract class TripBaseTest {
    @SuppressWarnings("rawtypes")
    protected static Class mTestClassType;
    
    protected static String mClassName;
    protected static User mUserCreated;
    
    private static boolean isManagerTest = false;
    private static boolean isWSClientTest = false;
    

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mTestClassType == TripManagerTest.class) {
            isManagerTest = true;
        
        } else if(mTestClassType == TripWSClientTest.class) {
            isWSClientTest = true;
        
        } else {
            Assert.fail("Cannot continue with test! Unknown incoming test class type: " + mTestClassType);
        }

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName;
        mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static  void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForTrip.tripDelete_byDB_ALLTrips(mUserCreated.getUserId());
    }    
    
    @Test    
    public void testCreate() throws Exception {
        Trip tripCreated = null;
        if(isWSClientTest) {
            tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                    TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, true);
        } else if(isManagerTest) {
            tripCreated =  TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                    TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, true);
        }
        
        //Verify applicable-only to new trip create
        Assert.assertEquals("Trip Create: Incorrect Status!", TripStatusEnum.ACTIVE, tripCreated.getStatus());
        Assert.assertEquals("Trip Create: Result Trip created date and updated date should be THE SAME!", 
                tripCreated.getCreated(), tripCreated.getUpdated());
        
        //Trip enrichment data:
        Assert.assertEquals("Trip Create: Returned Trip must have user data: Fullname!", mUserCreated.getFullname(), tripCreated.getUserFullname());
        Assert.assertEquals("Trip Create: Returned Trip must have user data: Username!", mUserCreated.getUsername(), tripCreated.getUsername());
    }
    
    @Test    
    public void testCreate_Private() throws Exception {
        Trip tripCreated = null;
        
        if(isWSClientTest) {
            tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                    TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, null, true);
        
        } else if(isManagerTest) {
            tripCreated =  TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                    TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, null, true);
        }
        
        //Trip enrichment data:
        Assert.assertEquals("Trip Create: Returned Trip must have user data: Fullname!", mUserCreated.getFullname(), tripCreated.getUserFullname());
        Assert.assertEquals("Trip Create: Returned Trip must have user data: Username!", mUserCreated.getUsername(), tripCreated.getUsername());
    }    
    
    @Test
    public void testCreate_WithPlace() throws Exception {
        try {
            Place placeToCreate = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
            placeToCreate.setNote(TestConstants.UNITTEST_PLACE_NOTE);
            List<Place> listOfPlaces = new ArrayList<Place>();
            listOfPlaces.add(placeToCreate);

            if(isWSClientTest) {
                TestUtilForTrip.tripCreate_byWSClient(
                        mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                        TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, listOfPlaces, true);
            
            } else if(isManagerTest) {
                TestUtilForTrip.tripCreate_byManager(
                        mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                        TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, listOfPlaces, true); 
            }

        } finally {
            //Cleanup the places:
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
        }
    }
    
    @Test
    public void testCreate_withPlace_LocationFromPlace() throws Exception {
        try {
            String placeLocationString = "Place Location";
            Place placeToCreate = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
            placeToCreate.setNote(TestConstants.UNITTEST_PLACE_NOTE);
            placeToCreate.setLocation(placeLocationString);
            List<Place> listOfPlaces = new ArrayList<Place>();
            listOfPlaces.add(placeToCreate);
            
            String tripLocationEmpty = null;
            Trip tripCreated = null;
            if(isWSClientTest) {
                tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                        mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                        tripLocationEmpty, TripPrivacyEnum.PUBLIC, listOfPlaces, false);
            
            } else if(isManagerTest) {
                tripCreated =  TestUtilForTrip.tripCreate_byManager(
                        mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                        tripLocationEmpty, TripPrivacyEnum.PUBLIC, listOfPlaces, false);
            }
            
            //Verify:
            Assert.assertNotNull("Trip Create: Returned Trip should NOT be null!", tripCreated);
            Assert.assertNotNull("Trip Create: Location should have NOT be NULL!", tripCreated.getLocation());
            Assert.assertEquals("Trip Create: Invalid Location!", placeLocationString, tripCreated.getLocation());
        } finally {
            //Cleanup the places:
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
        }
    }    
    
    @Test
    public void testCreate_WithPlace_LocationEmpty() throws Exception {
        try {
            Place placeToCreate = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
            placeToCreate.setNote(TestConstants.UNITTEST_PLACE_NOTE);
            placeToCreate.setLocation(null);
            List<Place> listOfPlaces = new ArrayList<Place>();
            listOfPlaces.add(placeToCreate);
            
            String tripLocationEmpty = null;
            Trip tripCreated = null;
            if(isWSClientTest) {
                tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                        mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                        tripLocationEmpty, TripPrivacyEnum.PUBLIC, listOfPlaces, false);
            
            } else if(isManagerTest) {
                tripCreated =  TestUtilForTrip.tripCreate_byManager(
                        mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                        tripLocationEmpty, TripPrivacyEnum.PUBLIC, listOfPlaces, false);
            }

           //Verify:
            Assert.assertNotNull("Trip Create: Returned Trip should NOT be null!", tripCreated);
            Assert.assertNotNull("Trip Create: Location should have NOT be NULL!", tripCreated.getLocation());
            Assert.assertEquals("Trip Createe: Invalid Location!", 
                    "[" + placeToCreate.getLatitude() + "," + placeToCreate.getLongitude() + "]", 
                    tripCreated.getLocation());
        } finally {
            //Cleanup the places:
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
        }
    }    
    
    @Test
    public void testCreate_WithExistingActiveTrip() throws Exception {
        //Existing:
        TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PUBLIC, null, false); 

        //Create new:
        Trip tripCreated_New = null;
        if(isWSClientTest) {
            tripCreated_New = TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                    TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        
        } else if(isManagerTest) {
            tripCreated_New =  TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                    TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        }
        
        Assert.assertNotNull("Trip Create: Returned Trip should NOT be null!", tripCreated_New);
        Assert.assertEquals("Trip Create: Incorrect new Trip Status!", TripStatusEnum.ACTIVE, tripCreated_New.getStatus());
    }
    
    
    

    

    



    
    

    
    @Test
    public void testCreate_StringWithChars() throws Exception {
        String tripName = mClassName + " ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /"; 
        String tripNote = "TripNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        String tripLocation = "TripLocation ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        
        if(isWSClientTest) {
            TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), tripName, tripNote, tripLocation, TripPrivacyEnum.PUBLIC, null, true);
        
        } else if(isManagerTest) {
            TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), tripName, tripNote, tripLocation, TripPrivacyEnum.PUBLIC, null, true);
        }
    }
    
    @Test
    public void testCreate_StringNewLines() throws Exception {
        String tripName = mClassName + " \n new line TripName"; 
        String tripNote = "TripNote \n new line TripNote";
        String tripLocation = "TripLocation \n new line TripLocation";
        Trip tripCreated = null;
        if(isWSClientTest) {
            tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), tripName, tripNote, tripLocation, TripPrivacyEnum.PUBLIC, null, false);
        
        } else if(isManagerTest) {
            tripCreated = TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), tripName, tripNote, tripLocation, TripPrivacyEnum.PUBLIC, null, false);
        }

        Assert.assertNotNull("Trip Create: Returned Trip should NOT be null!", tripCreated);
        Assert.assertFalse("Trip Create: Invalid Trip name: " + tripCreated.getName(), tripCreated.getName().contains("\n"));
        Assert.assertTrue("Trip Create: Invalid Trip note: " + tripCreated.getNote(), tripCreated.getNote().contains("\n"));
        Assert.assertFalse("Trip Create: Invalid Trip location: " + tripCreated.getLocation(), tripCreated.getLocation().contains("\n"));
    }
    
    @Test
    public void testCreate_StringLong() throws Exception {
        String tripName = 
                mClassName + " 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        String tripLocation = 
                "TripLocation 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";

        Trip tripCreated = null;
        if(isWSClientTest) {
            tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), tripName, TestConstants.UNITTEST_TRIP_NOTE, tripLocation, TripPrivacyEnum.PUBLIC, null, false);
        
        } else if(isManagerTest) {
            tripCreated = TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), tripName, TestConstants.UNITTEST_TRIP_NOTE, tripLocation, TripPrivacyEnum.PUBLIC, null, false);
        }

        Assert.assertNotNull("Trip Create: Returned Trip should NOT be null!", tripCreated);
        Assert.assertEquals("Trip Create: Invalid size of name!", CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME, tripCreated.getName().length());
        Assert.assertEquals("Trip Create: Invalid size of location!", CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION, tripCreated.getLocation().length());
        Assert.assertEquals("Trip Create: Invalid name!", 
                tripName.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME), 
                tripCreated.getName());
        Assert.assertEquals("Trip Create: Invalid location!", 
                tripLocation.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION), 
                tripCreated.getLocation());
    }
    
    @Test
    public void testCreate_StringWithNonASCIIChars() throws Exception {
        String tripNameNonASCII = mClassName + " ���"; 
        String tripNoteNonASCII = "TripNote ���";
        String tripLocationNonASCII = "TripLocation ���";
        
        Trip tripCreated = null;
        if(isWSClientTest) {
            tripCreated = TestUtilForTrip.tripCreate_byWSClient(
                    mUserCreated.getUserId(), tripNameNonASCII, tripNoteNonASCII, tripLocationNonASCII, TripPrivacyEnum.PUBLIC, null, false);
        
        } else if(isManagerTest) {
            tripCreated = TestUtilForTrip.tripCreate_byManager(
                    mUserCreated.getUserId(), tripNameNonASCII, tripNoteNonASCII, tripLocationNonASCII, TripPrivacyEnum.PUBLIC, null, false);
        }

        Assert.assertNotNull("Trip Create: Returned Trip should NOT be null!", tripCreated);
        
        String tripNameASCII = mClassName + " oau"; 
        String tripNoteASCII = "TripNote oau";
        String tripLocationASCII = "TripLocation oau";
        Assert.assertEquals("Trip Create: Invalid Trip name!", tripNameASCII, tripCreated.getName());
        Assert.assertEquals("Trip Create: Invalid Trip note!", tripNoteASCII, tripCreated.getNote());
        Assert.assertEquals("Trip Create: Invalid Trip location!", tripLocationASCII, tripCreated.getLocation());
    }
    
    @Test
    public void testCreate_UserNotExists() throws Exception {
        Trip tripToCreate = new Trip();
        tripToCreate.setUserID("userNoExistID");
        tripToCreate.setName(TestConstants.UNITTEST_TRIP_NAME);
        tripToCreate.setNote(TestConstants.UNITTEST_TRIP_NOTE);
        tripToCreate.setLocation(TestConstants.UNITTEST_TRIP_LOCATION);
        tripToCreate.setPrivacy(TripPrivacyEnum.PUBLIC);
        
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.create(tripToCreate); 
            Assert.assertNotNull("Trip Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip Create: Result should have failed if trip to publish missing Trip ID!", result.isSuccess());
            Assert.assertNotNull("Trip Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
            
        } else if(isManagerTest) {
            try {
                TripManager.create(tripToCreate);
                Assert.fail("Trip Create: Should have FAILED when given UserID does not exist!");
            } catch(UserNotFoundException unfe) {
                //Expected
            }
        }
    }
    
    @Test    
    public void testRetrieve_ByID() throws Exception {
        Trip tripCreated =  TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        String tripID = tripCreated.getId();
        
        //Retrieve by ID:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(tripID, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripID);
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
        Trip tripFound = tripListFound.get(0);
        TestUtilForTrip.validateTrip(tripCreated, tripFound);
        
        //Verify applicable-only to new trip create
        Assert.assertEquals("Trip Retrieve: Incorrect Status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
        Assert.assertEquals("Trip Retrieve: Result Trip created date and updated date should be THE SAME!", 
                tripFound.getCreated(), tripFound.getUpdated());
    }
    
    @Test
    public void testRetrieve_ByID_MultipleTrips() throws Exception {
        //Existing
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        String tripName = mClassName + "_NotLast";
        Trip tripCreated_notLast = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), tripName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PUBLIC, null, false);
        
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated_notLast.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripCreated_notLast.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Trip retrieve: Incorrect number of Trips returned!", 1, tripListFound.size());
        Trip tripFound = tripListFound.get(0);
        TestUtilForTrip.validateTrip(tripCreated_notLast, tripFound);

        //Applicable to 'not last' trip:
        Assert.assertEquals("Trip Retrieve: Incorrect Status!", TripStatusEnum.COMPLETED, tripFound.getStatus());
    }    
    
    @Test
    public void testRetrieve_ByID_WithExistingActiveTrip() throws Exception {
        //Existing (Active):
        Trip tripExisting = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PUBLIC, null, false); 
        
        //Create new:
        TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        
        //Verify existing Trip status:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripExisting.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripExisting.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
        Assert.assertEquals("Trip Retrieve: Incorrect Trip status!", TripStatusEnum.COMPLETED, tripListFound.get(0).getStatus());
    }
    
    @Test
    public void testRetrieve_ByID_Private() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, null, false);

        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated.getId(), mUserCreated.getUserId(), null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripCreated.getId());
            tripToRetrieve.setUserID(mUserCreated.getUserId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Incorrect number of Trips returned!", 1, tripListFound.size());
        Assert.assertEquals("Trip Retrieve: Incorrect Privacy!", TripPrivacyEnum.PRIVATE, tripListFound.get(0).getPrivacy());
    }    
    
    @Test
    public void testRetrieve_ByID_Private_NoUserID() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PRIVATE, null, false);

        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripCreated.getId());
            tripToRetrieve.setUserID(null);
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNull("Trip Retrieve: Returned trip list should be EMPTY when trip is private and UserID is empty!", 
                tripListFound);
    }
    
    @Test
    public void testRetrieve_ByID_Private_NotUserLogin() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, null, false);

        String userOtherName = CommonUtil.generateID(null);
        User userOther = TestUtilForUser.userCreate_byManager(userOtherName + TestConstants.UNITTEST_EMAIL_SUFFIX, 
                userOtherName, mClassName + "_UserOther", false);
        
        //Retrieve:
        try {
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated.getId(), userOther.getUserId(), null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated.getId());
                tripToRetrieve.setUserID(userOther.getUserId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            Assert.assertNull("Trip Retrieve: Returned trip list should be EMPTY when trip is private and UserOther attempted to see it!", 
                    tripListFound);

        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }    
    }        
    
    @Test    
    public void testRetrieve_ByID_AfterDelete() throws Exception {
        Trip tripCreated =  TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        String tripID = tripCreated.getId();
        
        //Delete (by Manager):
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        tripToDelete.setId(tripID);
        TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        
        //Retrieve::
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripID, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripID);
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        if(tripListFound != null) {
            Assert.assertEquals("Trip Retrieve after Delete: Incorrect number of Trips returned!", 0, tripListFound.size());
        }
    }    

    @Test
    public void testRetrieve_ByID_WithEnrichmentData() throws Exception {
        try {
            Trip tripCreated_1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated_1);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated_1);
            TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), tripCreated_1.getId());
            TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), tripCreated_1.getId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(tripCreated_1.getId(), mUserCreated.getUserId());
            TestUtilForComment.commentCreateByCommentDB(tripCreated_1.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            TestUtilForComment.commentCreateByCommentDB(tripCreated_1.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);

            Trip tripCreated_2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated_2);
            TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), tripCreated_2.getId());

            //Retrieve Trip 1:
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated_1.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated_1.getId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            Assert.assertNotNull("Trip retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
            Assert.assertEquals("Trip retrieve: Incorrect number of Trips returned!", 1, tripListFound.size());
            Assert.assertNotNull("Trip retrieve: Returned Trip in the list should NOT be null!", tripListFound.get(0));
            Trip tripFound_1 = tripListFound.get(0);

            //Verify places:
            Assert.assertEquals("Trip retrieve: Returned Trip has invalid number of Places!", 2, tripFound_1.getNumOfPlaces());
            Assert.assertNotNull("Trip retrieve: Returned Trip should have NOT NULL list of places!", tripFound_1.getListOfPlaces());
            Place place1 = tripFound_1.getListOfPlaces().get(0);
            Assert.assertNotNull("Trip retrieve: Returned Trip's Place[0] should have NOT be NULL!", place1);
            Place place2 = tripFound_1.getListOfPlaces().get(1);
            Assert.assertNotNull("Trip retrieve: Returned Trip's Place[1] should have NOT be NULL!", place2);
            
            //Verify pictures:
            Assert.assertEquals("Trip retrieve: Returned Trip has invalid number of Pictures!", 2, tripFound_1.getNumOfPictures());
            Assert.assertNotNull("Trip retrieve: Returned Trip should have NOT NULL list of places!", tripFound_1.getListOfPictures());
            Picture pic1 = tripFound_1.getListOfPictures().get(0);
            Assert.assertNotNull("Trip retrieve: Returned Trip's Picture[0] should have NOT be NULL!", pic1);
            Picture pic2 = tripFound_1.getListOfPictures().get(1);
            Assert.assertNotNull("Trip retrieve: Returned Trip's Picture[1] should have NOT be NULL!", pic2);
            
            //Verify Favorites
            Assert.assertEquals("Trip retrieve: Returned Trip has invalid number of Favorites!", 1, tripFound_1.getNumOfFavorites());
            Assert.assertNotNull("Trip retrieve: Returned Trip has invalid number of Favorite Users!", tripFound_1.getListOfFavoriteUsers());
            Assert.assertEquals("Trip retrieve: Returned Trip has invalid number of Favorite Users!", 1, tripFound_1.getListOfFavoriteUsers().size());
            TestUtilForUser.validateUserSecureData(tripFound_1.getListOfFavoriteUsers().get(0), false);
            
            //Verify Comments
            Assert.assertEquals("Trip retrieve: Returned Trip has invalid number of Comments!", 2, tripFound_1.getNumOfComments());
            Assert.assertNotNull("Trip retrieve: Returned Trip has invalid number of Comments!", tripFound_1.getListOfComments());
            Assert.assertEquals("Trip retrieve: Returned Trip has invalid number of Comments!", 2, tripFound_1.getListOfComments().size());
            
        } finally {
            //NO actual image files created
            TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
            TestUtilForFavorite.faveDeleteALLbyFavoriteDB(mUserCreated.getUserId());
            TestUtilForComment.commentDeletebyCommentDB(mUserCreated.getUserId());
        }
    }
    
    @Test
    public void testRetrieve_ByID_withEnrichmentData_ManyComments() throws Exception {
        Trip tripCreated =  TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        String username = CommonUtil.generateID(null);
        User userWhoCommentInExcess = TestUtilForUser.userCreate_byManager(
                username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, 
                mClassName + "_UserOther", false);
        int numberOfComments = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        try {
            //Create Comments:
            Comment commentToCreate = new Comment();
            commentToCreate.setTripID(tripCreated.getId());
            commentToCreate.setUserID(userWhoCommentInExcess.getUserId());
            commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
            for(int i = 0; i < numberOfComments; i++) {
                CommentManager.create(commentToCreate);
            }
            
            //Retrieve Trip 
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated.getId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            Assert.assertNotNull("Trip Retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
            Assert.assertEquals("Trip Retrieve: Incorrect number of Trips returned!", 1, tripListFound.size());
            Assert.assertNotNull("Trip Retrieve: Returned Trip in the list should NOT be null!", tripListFound.get(0));
            Trip tripFound = tripListFound.get(0);
            
            //Specify special enrichment Comment data:
            Assert.assertEquals("Trip Retrieve: Returned Trip has invalid total number of Comments!", numberOfComments, tripFound.getNumOfComments());
            Assert.assertNotNull("Trip Retrieve: Returned Trip has invalid returned Comments!", tripFound.getListOfComments());
            Assert.assertEquals("Trip Retrieve: Returned Trip has invalid number of returned Comments!", 
                    CommonConstants.RECORDS_NUMBER_MAX, tripFound.getListOfComments().size());
        } finally {
            //Clean test data:
            TestUtilForUser.userDelete_byDB_ALLData(userWhoCommentInExcess);
        }
    }    
    
    @Test
    public void testRetrieve_ByID_withEnrichmentData_ManyFavorites() throws Exception {
        Trip tripCreated =  TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, true);

        int numberOfFaves = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        List<User> listOfUsers = new ArrayList<User> (0);
        try {
            for(int i = 0; i < numberOfFaves; i++) {
                String username = CommonUtil.generateID(null);
                User user = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, mClassName + "-user" + i, false);
                listOfUsers.add(user);
            }
            
            //Create Favorites:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(tripCreated.getId());
            for(int i = 0; i < numberOfFaves; i++) {
                faveToCreate.setUserID(listOfUsers.get(i).getUserId());
                FavoriteManager.create(faveToCreate);
            }
            
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated.getId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            Assert.assertNotNull("Trip Retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
            Assert.assertEquals("Trip Retrieve: Incorrect number of Trips returned!", 1, tripListFound.size());
            Assert.assertNotNull("Trip Retrieve: Returned Trip in the list should NOT be null!", tripListFound.get(0));
            Trip tripFound = tripListFound.get(0);
            
            //Specify special enrichment Favorite data:
            Assert.assertEquals("Trip Retrieve: Returned Trip has invalid total number of Favorites!", numberOfFaves, tripFound.getNumOfFavorites());
            Assert.assertNotNull("Trip Retrieve: Returned Trip has invalid returned Favorites!", tripFound.getListOfFavoriteUsers());
            Assert.assertEquals("Trip Retrieve: Returned Trip has invalid number of returned Favorites!", 
                    CommonConstants.RECORDS_NUMBER_MAX, tripFound.getListOfFavoriteUsers().size());
            for(User user : tripFound.getListOfFavoriteUsers()) {
                TestUtilForUser.validateUserSecureData(user, false);
            }
        } finally {
            //Clean test data:
            if(listOfUsers.size() > 0) {
                for(int i = 0; i < numberOfFaves; i++) {
                    TestUtilForUser.userDelete_byDB_ALLData(listOfUsers.get(i));
                }
            }
        }
    }    

    @Test
    public void testRetrieve_ByUserID() throws Exception {
        //Existing:
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        
        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    null, mUserCreated.getUserId(), null, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(mUserCreated.getUserId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve by User: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve by User: Invalid number of found trips!", 2, tripListFound.size());
        Assert.assertNotNull("Trip Retrieve by User: Trip entity [0] in the list should NOT be null!", tripListFound.get(0));
        Assert.assertNotNull("Trip Retrieve by User: Trip entity [1] in the list should NOT be null!", tripListFound.get(1));
    }    

    @Test
    public void testRetrieve_ByUsename() throws Exception {
        //Existing:
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        
        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    null, null, mUserCreated.getUsername(), ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUsername(mUserCreated.getUsername());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve by User: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve by User: Invalid number of found trips!", 2, tripListFound.size());
        Assert.assertNotNull("Trip Retrieve by User: Trip entity [0] in the list should NOT be null!", tripListFound.get(0));
        Assert.assertNotNull("Trip Retrieve by User: Trip entity [1] in the list should NOT be null!", tripListFound.get(1));
    }    

    @Test
    public void testRetrieve_ByUsername_withStatusPrivacyPublish() throws Exception {
        //Create (need tobe by DB to by pass default Status and Publish):
        String tripName_Active = mClassName + "_ACTIVE";
        String tripName_Private = mClassName + "_PRIVATE";
        String tripName_NotPublish = mClassName + "_NOTPUBLISH";
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Active, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Private, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_NotPublish, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        
        //Retrieve - ALL:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUsername(mUserCreated.getUsername());
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, 0, -1);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 3, tripListFound.size());
        
        //Retrieve - ACTIVE only:
        tripToRetrieve.setStatus(TripStatusEnum.ACTIVE);
        tripToRetrieve.setPrivacy(null);
        tripToRetrieve.setPublish(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, 0, -1);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve 'ACTIVE' only by User: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve 'ACTIVE' only by User: Invalid number of found trips!", 1, tripListFound.size());
        Assert.assertEquals("Trip Retrieve 'ACTIVE' only by User: Invalid trip name!", tripName_Active, tripListFound.get(0).getName());
        
        //Retrieve - PUBLIC only:
        tripToRetrieve.setStatus(null);
        tripToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
        tripToRetrieve.setPublish(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, 0, -1);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve 'PUBLIC' only by User: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve 'PUBLIC' only by User: Invalid number of found trips!", 2, tripListFound.size());

        //Retrieve - PRIVATE (It shall return ALL!):
        tripToRetrieve.setStatus(null);
        tripToRetrieve.setPrivacy(TripPrivacyEnum.PRIVATE);
        tripToRetrieve.setPublish(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, 0, -1);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve to include 'PRIVATE' Trips by User: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve to include 'PRIVATE' Trips by User: Invalid number of found trips!", 3, tripListFound.size());
        
        //Retrieve - NOT PUBLISH only:
        tripToRetrieve.setStatus(null);
        tripToRetrieve.setPrivacy(null);
        tripToRetrieve.setPublish(TripPublishEnum.NOT_PUBLISH);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, 0, -1);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve 'NOT PUBLISH' only by User: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve 'NOT PUBLISH' only by User: Invalid number of found trips!", 1, tripListFound.size());
        Assert.assertEquals("Trip Retrieve 'NOT PUBLISH' only by User: Invalid trip name!", tripName_NotPublish, tripListFound.get(0).getName());
        
        //Retrieve ALL specified:
        tripToRetrieve.setStatus(TripStatusEnum.ACTIVE);
        tripToRetrieve.setPrivacy(TripPrivacyEnum.PRIVATE);
        tripToRetrieve.setPublish(TripPublishEnum.NOT_PUBLISH);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, 0, -1);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        if(tripListFound != null) {
            Assert.assertEquals("Trip Retrieve 'All specified' only by User: Invalid number of found trips!", 0, tripListFound.size());
        }
    }
    
    @Test
    public void testRetrieve_byUsername_withOffsetAndLimit() throws Exception {
        for(int i = 0; i < 4; i++) {
            TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        }
        
        int offset = 0;
        int limit = 2;
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUsername(mUserCreated.getUsername());
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, offset, limit);
            Assert.assertNotNull("Trip retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip retrieve: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripListFound = result.getListOfTrips();
        } else if(isManagerTest) {
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, offset, limit);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Incorrect number of Trips returned!", limit, tripListFound.size());
    }
    
    @Test
    public void testRetrieve_ByUsername_withEnrichmentData() throws Exception {
        try {
            Trip tripCreated_1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated_1);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated_1);
            TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), tripCreated_1.getId());
            TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), tripCreated_1.getId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(tripCreated_1.getId(), mUserCreated.getUserId());
            TestUtilForComment.commentCreateByCommentDB(tripCreated_1.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            TestUtilForComment.commentCreateByCommentDB(tripCreated_1.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            
            Thread.sleep(1000); //Time gap due to dates sensitive validations
            
            Trip tripCreated_2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated_2);
            TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), tripCreated_2.getId());

            //Retrieve - Sorted by CREATED desc
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        null, null, mUserCreated.getUsername(), ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setUsername(mUserCreated.getUsername());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, OrderByEnum.ORDER_BY_CREATED_DATE, 0, -1);
            }
            Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
            Assert.assertEquals("Incorrect number of Trips returned!", 2, tripListFound.size());
            
            Trip tripFound_2 = tripListFound.get(0);
            Assert.assertNotNull("Trip[0] should NOT be null!", tripFound_2);
            Assert.assertEquals("Invalid TripID for Trip[0]!", tripCreated_2.getId(), tripFound_2.getId());
            Assert.assertEquals("Invalid number of Places for Trip[0]!", 1, tripFound_2.getNumOfPlaces());
            Assert.assertEquals("Invalid number of Pictures for Trip[0]!", 1, tripFound_2.getNumOfPictures());
            Assert.assertEquals("Invalid number of Favorites for Trip[0]!", 0, tripFound_2.getNumOfFavorites());
            Assert.assertEquals("Invalid number of Comments for Trip[0]!", 0, tripFound_2.getNumOfComments());
            
            Trip tripFound_1 = tripListFound.get(1);
            Assert.assertNotNull("Trip[1] should NOT be null!", tripFound_1);
            Assert.assertEquals("Invalid TripID for Trip[1]!", tripCreated_1.getId(), tripFound_1.getId());
            Assert.assertEquals("Invalid number of Places for Trip[1]!", 2, tripFound_1.getNumOfPlaces());
            Assert.assertEquals("Invalid number of Pictures for Trip[1]!", 2, tripFound_1.getNumOfPictures());
            Assert.assertEquals("Invalid number of Favorites for Trip[1]!", 1, tripFound_1.getNumOfFavorites());
            Assert.assertNull("Trip[1] should NOT return list of favorite User beans!", tripFound_1.getListOfFavoriteUsers());
            Assert.assertEquals("Invalid number of Comments for Trip[1]!", 2, tripFound_1.getNumOfComments());
            Assert.assertNull("Trip[1] should NOT return list of Comment beans!", tripFound_1.getListOfComments());
        } finally {
            //NO actual image files created
            TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
            TestUtilForFavorite.faveDeleteALLbyFavoriteDB(mUserCreated.getUserId());
            TestUtilForComment.commentDeletebyCommentDB(mUserCreated.getUserId());
        }
    }
    
    @Test
    public void testUpdate_ALL() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Update:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        String tripNameUpdated = mClassName + "-UPDATED";
        String tripNoteUpdated = TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED";
        String tripLocUpdated = TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED";
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(tripNameUpdated);
        tripToUpdate.setNote(tripNoteUpdated);
        tripToUpdate.setLocation(tripLocUpdated);
        tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
       
        //Validate:
        TestUtilForTrip.validateTrip(tripToUpdate, tripUpdated);        
        
        //Status does not change:
        Assert.assertEquals("Trip Update: Invalid status value!", TripStatusEnum.ACTIVE, tripUpdated.getStatus());
        
        //Validate dates (special):
        Date createdDate = DateUtil.formatDate(tripUpdated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
        Date updatedDate = DateUtil.formatDate(tripUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Trip Update: Record's updated date should be AFTER created date!", 
                updatedDate.after(createdDate));
    }
    
    @Test
    public void testUpdate_ThenRetrieve() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        String tripID = tripCreated.getId();

        //Update:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        String tripNameUpdated = mClassName + "-UPDATED";
        String tripNoteUpdated = TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED";
        String tripLocUpdated = TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED";
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripID);
        tripToUpdate.setName(tripNameUpdated);
        tripToUpdate.setNote(tripNoteUpdated);
        tripToUpdate.setLocation(tripLocUpdated);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip update: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            TripManager.update(tripToUpdate);
        }
       
        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(tripID, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripID);
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve after Update: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve after Update: Returned list of Trips incorrect number!", 1, tripListFound.size());
        Trip tripFound = tripListFound.get(0);
        TestUtilForTrip.validateTrip(tripToUpdate, tripFound);        
        
        //Status does not change:
        Assert.assertEquals("Trip Retrieve after Update: Invalid status value!", TripStatusEnum.ACTIVE, tripFound.getStatus());
        
        //Validate dates (special):
        Date createdDate = DateUtil.formatDate(tripFound.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
        Date updatedDate = DateUtil.formatDate(tripFound.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Trip Retrieve after Update: Record's updated date [" 
                + updatedDate + "] should be AFTER created date [" + createdDate + "]", 
                updatedDate.after(createdDate));
    }

    @Test
    public void testUpdate_Privacy() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        String tripID = tripCreated.getId();

        //Update:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripID);
        tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip update: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            TripManager.update(tripToUpdate);
        }
       
        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(tripID, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripID);
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        if(tripListFound != null) {
            Assert.assertEquals("Trip Retrieve after Update: Returned list of Trips should NOT EMPTY when it is PRIVATE and Trip Retrieve doesn't include its owner UserID!", 
                    0, tripListFound.size());
        }
    }

    @Test
    public void testUpdate_Status() throws Exception {
        //Create (default status is ACTIVE):
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        Assert.assertEquals("Trip Created: Invalid default status!", TripStatusEnum.ACTIVE, tripCreated.getStatus());

        //Update:
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(tripCreated.getUserID());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setStatus(TripStatusEnum.COMPLETED);
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertEquals("Trip Update: Returned updated Trip invalid status!", TripStatusEnum.COMPLETED, tripUpdated.getStatus());        
        
        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve after Update: Returned list of trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve after Update: Invalid number of found trips!", 1, tripListFound.size());
        Assert.assertNotNull("Trip Retrieve after Update: Trip entity in the list should NOT be null!", tripListFound.get(0));
        Assert.assertEquals("Trip Retrieve after Update: Invalid status value!", TripStatusEnum.COMPLETED, tripListFound.get(0).getStatus());

        //Attempt to update status again:
        tripToUpdate.setStatus(TripStatusEnum.ACTIVE);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            TripManager.update(tripToUpdate);
        }
        
        //Validate:
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertEquals("Trip Retrieve after Update: Trip Status should not be updateable once it is COMPLETED!", 
                TripStatusEnum.COMPLETED, tripListFound.get(0).getStatus());
    }
    
    @Test
    public void testUpdate_Publish() throws Exception {
        //Create (need tobe by DB to by pass default Publish):
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                mClassName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        
        //Get its ID:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUsername(mUserCreated.getUsername());
        List<Trip> tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        Trip tripCreated = tripListFound.get(0);
        Assert.assertEquals("Trip Create: Invalid publish value!", TripPublishEnum.NOT_PUBLISH, tripCreated.getPublish());
        
        //Update:
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(tripCreated.getUserID());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setPublish(TripPublishEnum.PUBLISH);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            TripManager.update(tripToUpdate);
        }

        //Validate (update publish is IGNORED!):
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            tripToRetrieve.setId(tripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertEquals("Trip Retrieved after Update: Trip Publish value should not be updateable!", 
                TripPublishEnum.NOT_PUBLISH, tripListFound.get(0).getPublish());
    }
    
    @Test
    public void testUpdate_StringWithChars() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Update:
        String newTripName = mClassName + " ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /"; 
        String newTripNote = "TripNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(newTripName);
        tripToUpdate.setNote(newTripNote);
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertEquals("Trip Update: Invalid name!", newTripName, tripUpdated.getName());
        Assert.assertEquals("Trip Update: Invalid note!", newTripNote, tripUpdated.getNote());
    }
    
    @Test
    public void testUpdate_StringEmpty() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Update:
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(" ");
        tripToUpdate.setNote(" ");
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertEquals("Trip Update: Trip name should NOT updateable with empty string!", tripCreated.getName(), tripUpdated.getName());
        Assert.assertEquals("Trip Update: Invalid note!", "", tripUpdated.getNote());
    }
    
    @Test
    public void testUpdate_StringNewLines() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Update:
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName("TripName \n new line TripName");
        tripToUpdate.setNote("TripNote \n new line TripNote");
        tripToUpdate.setLocation("TripLocation \n new line TripLocation");
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertFalse("Trip Update: Invalid Trip name: " + tripUpdated.getName(), tripUpdated.getName().contains("\n"));
        Assert.assertTrue("Trip Update: Invalid Trip note: " + tripUpdated.getNote(), tripUpdated.getNote().contains("\n"));
        Assert.assertFalse("Trip Update: Invalid Trip location: " + tripUpdated.getLocation(), tripUpdated.getLocation().contains("\n"));
    }
    
    @Test
    public void testUpdate_StringLong() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Update:
        String tripName = 
                "TripName 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        String tripLocation = 
                "TripLocation 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(tripName);
        tripToUpdate.setLocation(tripLocation);
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertEquals("Trip Update: Invalid size of name!", CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME, tripUpdated.getName().length());
        Assert.assertEquals("Trip Update: Invalid size of location!", CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION, tripUpdated.getLocation().length());
        Assert.assertEquals("Trip Update: Invalid name!", 
                tripName.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME), 
                tripUpdated.getName());
        Assert.assertEquals("Trip Update: Invalid location!", 
                tripLocation.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION), 
                tripUpdated.getLocation());
    }
    
    @Test
    public void testUpdate_StringWithNonASCIIChars() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Update:
        String tripNameNonASCII = "TripName ���"; 
        String tripNoteNonASCII = "TripNote ���";
        String tripLocationNonASCII = "TripLocation ���";

        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(tripNameNonASCII);
        tripToUpdate.setNote(tripNoteNonASCII);
        tripToUpdate.setLocation(tripLocationNonASCII);
        Trip tripUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Update: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() != null) {
                tripUpdated = result.getListOfTrips().get(0);
            }
        } else if(isManagerTest) {
            tripUpdated =  TripManager.update(tripToUpdate);
        }
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        
        //Expected ASCII values:
        String tripNameASCII = "TripName oau"; 
        String tripNoteASCII = "TripNote oau";
        String tripLocationASCII = "TripLocation oau";
        Assert.assertEquals("Trip Update: Invalid Trip name!", tripNameASCII, tripUpdated.getName());
        Assert.assertEquals("Trip Update: Invalid Trip note!", tripNoteASCII, tripUpdated.getNote());
        Assert.assertEquals("Trip Update: Invalid Trip location!", tripLocationASCII, tripUpdated.getLocation());
    }
    
    @Test    
    public void testDelete_ByID() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        String tripID = tripCreated.getId();
        
        //Delete by ID:
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        tripToDelete.setId(tripID);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID); 
            Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }

        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNull("Trip Retrieve after Delete: Returned trip list should be EMPTY!", tripListFound);
    }    
    
    @Test
    public void testDelete_ByID_MultipleTrips() throws Exception {
        Trip tripCreated_1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        Trip tripCreated_2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Delete first Trip
        Trip tripToDelete_1 = new Trip();
        tripToDelete_1.setUserID(mUserCreated.getUserId());
        tripToDelete_1.setId(tripCreated_1.getId());
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.delete(tripToDelete_1, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID); 
            Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = TripManager.delete(tripToDelete_1, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }

        //Retrieve all:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    null, null, mUserCreated.getUsername(), ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUsername(mUserCreated.getUsername());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve after Delete: Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve after Delete: Incorrect number of Trips returned!", 1, tripListFound.size());

        //Delete 2nd Trip
        Trip tripToDelete_2 = new Trip();
        tripToDelete_2.setUserID(mUserCreated.getUserId());
        tripToDelete_2.setId(tripCreated_2.getId());
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.delete(tripToDelete_2, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID); 
            Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = TripManager.delete(tripToDelete_2, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }
        
        //Retrieve all:
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    null, null, mUserCreated.getUsername(), ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUsername(mUserCreated.getUsername());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        }
        Assert.assertNull("Trip Retrieve after Delete: Returned list of retrieved Trip should have been EMPTY!", tripListFound);
    }
    
    /*
     * 2/2018: On 'HEROKU/GOOGLECLOUD" env., this test would fail on "retrieve after delete"
     * due to GOOGLECLOUD image caching/delete issue. 
     * See note on FileServerClientGoogleCloudStorageImpl.java
     */
    @Test
    public void testDelete_ByID_WithPlacesPictures() throws Exception {
        //Create:
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        String tripID = tripCreated.getId();

        String picImageName_1 = null;
        String picImageName_2 = null;
        try {
            //Add places and pictures:
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated);
            TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, tripCreated);
            Picture picCreated_1 = TestUtilForPicture.pictureCreate_byManager_DefaultData(mClassName, mUserCreated, tripCreated);
            picImageName_1 = picCreated_1.getImageName();
            Picture picCreated_2 = TestUtilForPicture.pictureCreate_byManager_DefaultData(mClassName, mUserCreated, tripCreated);
            picImageName_2 = picCreated_2.getImageName();
            
            //Validate pic images exists:
            Assert.assertTrue("Trip Create: Picture image [" + picImageName_1 +"] should have been created!", 
                    TestUtilForPicture.imageRead_byFSClient(picImageName_1, true));
            Assert.assertTrue("Trip Create: Picture image [" + picImageName_2 +"] should have been created!", 
                    TestUtilForPicture.imageRead_byFSClient(picImageName_2, true));

            //Delete Trip and all its contents:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripID);
            if(isWSClientTest) {
                ServiceResponse result = TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID); 
                Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
                Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
            } else if(isManagerTest) {
                int numDeleted = TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
                Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
            }
            
            //Validate:
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated.getId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            if(tripListFound != null) {
                Assert.assertEquals("Trip Retrieve after Delete: Incorrect number of Trips returned!", 0, tripListFound.size());
            }

            //Validate pic images have been deleted:
            Assert.assertFalse("Trip Delete: Picture image [" + picImageName_1 +"] should have been deleted!", 
                    TestUtilForPicture.imageRead_byFSClient(picImageName_1, true));
            Assert.assertFalse("Trip Delete: Picture image [" + picImageName_2 +"] should have been deleted!", 
                    TestUtilForPicture.imageRead_byFSClient(picImageName_2, true));
            
        } finally {
            TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
            try {
                TestUtilForPicture.imageDelete_byFSClient(picImageName_1, true);
                TestUtilForPicture.imageDelete_byFSClient(picImageName_1, true);
            } catch(Exception e) {
                //Ignore
            }
        }
    }
    
    @Test
    public void testDelete_ByID_WithFavorites() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        User user1 = null, user2 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, mClassName + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, mClassName + "-user2", false);
            
            //Create Favorites:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(tripCreated.getId());
            faveToCreate.setUserID(user1.getUserId());
            FavoriteManager.create(faveToCreate);

            faveToCreate.setUserID(user2.getUserId());
            FavoriteManager.create(faveToCreate);
            
            //Delete Trip and all its contents:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripCreated.getId());
            if(isWSClientTest) {
                ServiceResponse result = TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID); 
                Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
                Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
            } else if(isManagerTest) {
                int numDeleted = TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
                Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
            }
            
            //Validate:
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated.getId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            if(tripListFound != null) {
                Assert.assertEquals("Trip Retrieve after Delete: Incorrect number of Trips returned!", 0, tripListFound.size());
            }

        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
        }
    }        
    
    @Test
    public void testDelete_ByID_WithComments() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        User user1 = null, user2 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, mClassName + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, mClassName + "-user2", false);
            
            //Create Comments:
            TestUtilForComment.commentCreateByCommentDB(tripCreated.getId(), user1.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            TestUtilForComment.commentCreateByCommentDB(tripCreated.getId(), user2.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
           
            //Delete Trip and all its contents:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripCreated.getId());
            if(isWSClientTest) {
                ServiceResponse result = TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID); 
                Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
                Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
            } else if(isManagerTest) {
                int numDeleted = TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
                Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
            }
            
            //Validate:
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                        tripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripCreated.getId());
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            if(tripListFound != null) {
                Assert.assertEquals("Trip Retrieve after Delete: Incorrect number of Trips returned!", 0, tripListFound.size());
            }

        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
        }
    }      
    
    @Test
    public void testDelete_ByUserID() throws Exception {
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);

        //Delete ALL Trips
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID); 
            Assert.assertNotNull("Trip Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip Delete: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID);
            Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 2, numDeleted);
        }

        //Validate:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(
                    null, mUserCreated.getUserId(), null, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(mUserCreated.getUserId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
        }
        if(tripListFound != null) {
            Assert.assertEquals("Trip Retrieve after Delete: Incorrect number of Trips returned!", 0, tripListFound.size());
        }
    }    
    
    @Test
    public void testPublish() throws Exception {
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        String tripID = tripToPublish.getId();
        
        //Publish:
        Trip tripPublished = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip publish: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                Assert.fail("Trip publish: Result should have had the published Trip!");
            }
            tripPublished = result.getListOfTrips().get(0);
            
        } else if(isManagerTest) {
            tripPublished = TripManager.publish(tripToPublish);
        }
        TestUtilForTrip.validateTrip(tripToPublish, tripPublished);
        Assert.assertEquals("Trip publish: Incorrect Trip ID!", tripID, tripPublished.getId());
        Assert.assertEquals("Trip publish: Incorrect created date!", 
                tripToPublish.getCreated(), tripPublished.getCreated());
        
        //Retrieve:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(tripID, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(tripID);
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
        Trip tripFound = tripListFound.get(0);
        TestUtilForTrip.validateTrip(tripToPublish, tripFound);
        Assert.assertEquals("Trip publish: Incorrect Trip ID!", tripID, tripFound.getId());
        Assert.assertEquals("Trip publish: Incorrect created date!", 
                tripToPublish.getCreated(), tripFound.getCreated());
        Assert.assertNull("Trip Published: Returned Trip should NOT have a Place!", tripFound.getListOfPlaces());
        Assert.assertNull("Trip Published: Returned Trip should NOT have a Picture!", tripFound.getListOfPictures());
    }
    
    @Test
    public void testPublish_WithPlaceAndPicture() throws Exception {
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        String tripID = tripToPublish.getId();
 
        Place placeToPublish = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
        placeToPublish.setId(CommonUtil.generateID(tripToPublish.getId()));
        placeToPublish.setTripID(tripID);
        placeToPublish.setUserID(mUserCreated.getUserId());
        placeToPublish.setNote(TestConstants.UNITTEST_PLACE_NOTE);
        placeToPublish.setCreated(tripToPublish.getCreated());
        placeToPublish.setUpdated(tripToPublish.getUpdated());
        List<Place> listOfPlaces = new ArrayList<Place>();
        listOfPlaces.add(placeToPublish);
        tripToPublish.setListOfPlaces(listOfPlaces);
        
        Picture picToPublish = new Picture();
        picToPublish.setId(CommonUtil.generateID(tripToPublish.getId()));
        picToPublish.setTripID(tripID);
        picToPublish.setUserID(mUserCreated.getUserId());
        picToPublish.setImageName(mClassName);
        picToPublish.setCreated(tripToPublish.getCreated());
        picToPublish.setUpdated(tripToPublish.getUpdated());
        List<Picture> listOfPictures = new ArrayList<Picture>();
        listOfPictures.add(picToPublish);
        tripToPublish.setListOfPictures(listOfPictures);
        
        try {
            //Publish:
            Trip tripPublished = null;
            if(isWSClientTest) {
                ServiceResponse result = TripWSClient.publish(tripToPublish); 
                Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
                Assert.assertTrue("Trip publish: Result should have been SUCCESS!\n" + result, result.isSuccess());
                if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                    Assert.fail("Trip publish: Result should have had the published Trip!");
                }
                tripPublished = result.getListOfTrips().get(0);
                
            } else if(isManagerTest) {
                tripPublished = TripManager.publish(tripToPublish);
            }
            Assert.assertNotNull("Trip Publish: Returned Trip should NOT be null!", tripPublished);
            
            //Verify Place:
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Place!", tripPublished.getListOfPlaces());
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Place!", tripPublished.getListOfPlaces().get(0));
            Place placePublished = tripPublished.getListOfPlaces().get(0);
            Assert.assertEquals("Trip Published: Invalid place ID!", placeToPublish.getId(), placePublished.getId());
            Assert.assertEquals("Trip Published: Incorrect Place latitude!", placeToPublish.getLatitude(), placePublished.getLatitude());
            Assert.assertEquals("Trip Published: Incorrect Place longitude!", placeToPublish.getLongitude(), placePublished.getLongitude());
            Assert.assertEquals("Trip Published: Incorrect Place note!", placeToPublish.getNote(), placePublished.getNote());
            Assert.assertNotNull("Trip Published: Place location should NOT null!", placePublished.getLocation());
            Assert.assertEquals("Trip Published: Incorrect Place created date!", 
                    placeToPublish.getCreated(), placePublished.getCreated());
            Assert.assertEquals("Trip Published: Incorrect Place updated date!", 
                    placeToPublish.getUpdated(), placePublished.getUpdated());
            
            //Verify Picture:
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Picture!", tripPublished.getListOfPictures());
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Picture!", tripPublished.getListOfPictures().get(0));
            Picture picPublished = tripPublished.getListOfPictures().get(0);
            Assert.assertEquals("Trip Published: Invalid Picture ID!", picToPublish.getId(), picPublished.getId());
            Assert.assertEquals("Trip Published: Incorrect Picture image name!", picToPublish.getImageName(), picPublished.getImageName());
            Assert.assertEquals("Trip Published: Incorrect Picture created date!", 
                    picToPublish.getCreated(), picPublished.getCreated());
            Assert.assertEquals("Trip Published: Incorrect Picture updated date!", 
                    picToPublish.getUpdated(), picPublished.getUpdated());
    
            //Retrieve and verify again:
            List<Trip> tripListFound = null;
            if(isWSClientTest) {
                tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(tripID, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
            } else if(isManagerTest) {
                Trip tripToRetrieve = new Trip();
                tripToRetrieve.setId(tripID);
                tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            }
            Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
            Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
            Trip tripFound = tripListFound.get(0);

            //Verify Place:
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Place!", tripFound.getListOfPlaces());
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Place!", tripFound.getListOfPlaces().get(0));
            Place placeRead = tripFound.getListOfPlaces().get(0);
            Assert.assertEquals("Trip Published: Invalid place ID!", placeToPublish.getId(), placeRead.getId());
            Assert.assertEquals("Trip Published: Incorrect Place latitude!", placeToPublish.getLatitude(), placeRead.getLatitude());
            Assert.assertEquals("Trip Published: Incorrect Place longitude!", placeToPublish.getLongitude(), placeRead.getLongitude());
            Assert.assertEquals("Trip Published: Incorrect Place note!", placeToPublish.getNote(), placeRead.getNote());
            Assert.assertEquals("Trip Published: Incorrect Place created date!", 
                    placeToPublish.getCreated(), placeRead.getCreated());
            Assert.assertEquals("Trip Published: Incorrect Place updated date!", 
                    placeToPublish.getUpdated(), placeRead.getUpdated());
            
            //Verify Picture:
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Picture!", tripFound.getListOfPictures());
            Assert.assertNotNull("Trip Published: Retrieved Trip should have a Picture!", tripFound.getListOfPictures().get(0));
            Picture pictureRead = tripFound.getListOfPictures().get(0);
            Assert.assertEquals("Trip Published: Invalid Picture ID!", picToPublish.getId(), pictureRead.getId());
            Assert.assertEquals("Trip Published: Incorrect Picture image name!", picToPublish.getImageName(), pictureRead.getImageName());
            Assert.assertEquals("Trip Published: Incorrect Picture created date!", 
                    picToPublish.getCreated(), pictureRead.getCreated());
            Assert.assertEquals("Trip Published: Incorrect Picture updated date!", 
                    picToPublish.getUpdated(), pictureRead.getUpdated());

        } finally {
            TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
            TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
        }
    }    
    
    @Test
    public void testPublish_WithExistingActiveTrip_StatusActive() throws Exception {
        //Existing ACTIVE Trip:
        Trip tripCurrentActive = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName); 
        Assert.assertNotNull("Trip Created: Returned Trip current active should NOT be null!", tripCurrentActive);
        Assert.assertEquals("Trip Created: Incorrect Status!", TripStatusEnum.ACTIVE, tripCurrentActive.getStatus());
        
        Thread.sleep(1000); //Time gap due to dates sensitive validations

        //Publish:
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        Trip tripPublished = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip publish: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                Assert.fail("Trip publish: Result should have had the published Trip!");
            }
            tripPublished = result.getListOfTrips().get(0);
            
        } else if(isManagerTest) {
            tripPublished = TripManager.publish(tripToPublish);
        }
        Assert.assertNotNull("Trip Publish: Returned Trip should NOT be null!", tripPublished);
        Assert.assertEquals("Trip Publish: Incorrect Status!", 
                TripStatusEnum.ACTIVE, tripPublished.getStatus());
        
        //Retrieve existing Trip:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(tripCurrentActive.getId());
        List<Trip> tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
        Assert.assertNotNull("Trip Retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Existing Trip status should have updated to 'COMPLETED'!", 
                TripStatusEnum.COMPLETED, tripListFound.get(0).getStatus());
    }
    
    @Test
    public void testPublish_WithExistingActiveTrip_StatusActive_CreatedBefore() throws Exception {
        //Trip Non-Publish with EARLIER created date:
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        
        Thread.sleep(1000); //Time gap due to dates sensitive validations

        //Existing active Trip (after Trip Non-Publish):
        Trip tripCurrentActive = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName); 
        Assert.assertNotNull("Trip Created: Returned Trip current active should NOT be null!", tripCurrentActive);
        Assert.assertEquals("Trip Created: Incorrect Status!", TripStatusEnum.ACTIVE, tripCurrentActive.getStatus());

        //Publish:
        Trip tripPublished = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip publish: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                Assert.fail("Trip publish: Result should have had the published Trip!");
            }
            tripPublished = result.getListOfTrips().get(0);
            
        } else if(isManagerTest) {
            tripPublished = TripManager.publish(tripToPublish);
        }
        Assert.assertNotNull("Trip Publish: Returned Trip should NOT be null!", tripPublished);
        Assert.assertEquals("Trip Publish: Published Trip status should have changed to 'COMPLETED'!", 
                TripStatusEnum.COMPLETED, tripPublished.getStatus());

        //Retrieve existing Trip:
        List<Trip> tripListFound = 
                TripManager.retrieve(tripCurrentActive, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
        Assert.assertNotNull("Trip Retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Existing Trip status should have stayed 'ACTIVE'!", 
                TripStatusEnum.ACTIVE, tripListFound.get(0).getStatus());
    }    
    
    @Test
    public void testPublish_WithExistingActiveTrip_StatusCompleted() throws Exception {
        //Existing active trip:
        Trip tripCurrentActive = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName); 
        Assert.assertNotNull("Trip Created: Returned Trip current active should NOT be null!", tripCurrentActive);
        Assert.assertEquals("Trip Created: Incorrect Status!", TripStatusEnum.ACTIVE, tripCurrentActive.getStatus());
         
        Thread.sleep(1000); //Time gap due to dates sensitive validations

        //Trip Non-Publish with AFTER created date and status COMPLETED:
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setStatus(TripStatusEnum.COMPLETED);
        
        //Publish:
        Trip tripPublished = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip publish: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                Assert.fail("Trip publish: Result should have had the published Trip!");
            }
            tripPublished = result.getListOfTrips().get(0);
            
        } else if(isManagerTest) {
            tripPublished = TripManager.publish(tripToPublish);
        }
        Assert.assertNotNull("Trip Publish: Returned Trip should NOT be null!", tripPublished);
        Assert.assertEquals("Trip Publish: Incorrect published Trip status!", 
                TripStatusEnum.COMPLETED, tripPublished.getStatus());
        
        //Retrieve existing Trip:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(tripCurrentActive.getId());
        List<Trip> tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
        Assert.assertNotNull("Trip Retrieve: Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Existing active Trip status should have changed to 'COMPLETED'!", 
                TripStatusEnum.COMPLETED, tripListFound.get(0).getStatus());
    }
    
    @Test
    public void testPublish_MoreThanOnce() throws Exception {
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        String tripID = tripToPublish.getId();

        //Publish:
        TripManager.publish(tripToPublish);
        
        //Publish AGAIN:
        Trip tripPublished = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip publish: Result should have been SUCCESS!\n" + result, result.isSuccess());
            if(result.getListOfTrips() == null || result.getListOfTrips().size() == 0) {
                Assert.fail("Trip publish: Result should have had the published Trip!");
            }
            tripPublished = result.getListOfTrips().get(0);
            
        } else if(isManagerTest) {
            tripPublished = TripManager.publish(tripToPublish);
        }
        Assert.assertNotNull("Trip Publish: Returned Trip should NOT be null!", tripPublished);
        
        //Retrieve:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUserID(mUserCreated.getUserId());
        tripToRetrieve.setId(tripID);
        List<Trip> tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Trip Published: Retrieved Trip should NOT be null!!", tripListFound);
        Assert.assertNotNull("Trip Published: Retrieved Trip should NOT be null!!", tripListFound.get(0));
        Trip tripFound = tripListFound.get(0);
        Assert.assertEquals("Trip Published: Invalid Trip ID!", tripID, tripFound.getId());
        Assert.assertEquals("Trip Published: Incorrect Publish value!", TripPublishEnum.PUBLISH, tripFound.getPublish());
    }
    
    @Test
    public void testPublish_TripID_Empty() throws Exception {
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setId(null);
        
        //Publish:
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have failed if trip to publish missing Trip ID!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
            
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed if trip to publish missing Trip ID!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }
    }    
    
    @Test
    public void testPublish_RequiredData_Empty() throws Exception {
        //Missing - UserID
        Trip tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setUserID(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have FAILED when any required data is missing!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed when any required data is missing!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }
        
        //Missing - Trip Name
        tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setName(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have FAILED when any required data is missing!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed when any required data is missing!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }

        //Missing - Trip Status
        tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setStatus(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have FAILED when any required data is missing!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed when any required data is missing!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }

        //Missing - Trip Privacy
        tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setPrivacy(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have FAILED when any required data is missing!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed when any required data is missing!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }

        //Missing - Created Date
        tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setCreated(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have FAILED when any required data is missing!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed when any required data is missing!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }

        //Missing - Updated Date
        tripToPublish = TestUtilForTrip.composeDummyTripUnpublished(mUserCreated.getUserId(), mClassName);
        tripToPublish.setUpdated(null);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.publish(tripToPublish); 
            Assert.assertNotNull("Trip publish: Returned result can NOT be null!", result);
            Assert.assertFalse("Trip publish: Result should have FAILED when any required data is missing!", result.isSuccess());
            Assert.assertNotNull("Trip publish: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Trip publish: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                TripManager.publish(tripToPublish);
                Assert.fail("Trip Publish: Should have failed when any required data is missing!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }

    }    

    @Test
    public void testRetrieveSync() throws Exception {
        //Existing:
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        
        //Sync ALL:
        Trip tripToSync = new Trip();
        tripToSync.setUserID(mUserCreated.getUserId());
        List<Trip> listOfTripsSynced = null;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieveSync(tripToSync);
            Assert.assertNotNull("Trip RetrieveSync ALL: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip RetrieveSync ALL: Result should have been SUCCESS!\n" + result, result.isSuccess());
            listOfTripsSynced = result.getListOfTrips();

        } else if(isManagerTest) {
            listOfTripsSynced = TripManager.retrieveSync(tripToSync);
        }
        Assert.assertNotNull("Trip RetrieveSync ALL: Result should have the List Trip!", listOfTripsSynced);
        Assert.assertEquals("Trip RetrieveSync ALL: Invalid number of returned synced Trip!", 2, listOfTripsSynced.size());
    }    
    
    @Test
    public void testRetrieveSync_AfterUpdate() throws Exception {
        //Existing:
        Trip tripCreatedFirst = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        Trip tripCreatedLast = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName);
        
        //Sync - ALL:
        Trip tripToSync = new Trip();
        tripToSync.setUserID(mUserCreated.getUserId());
        List<Trip> listOfTripsSynced = TripManager.retrieveSync(tripToSync);
        Assert.assertNotNull("Trip RetrieveSync ALL: Result should have the List Trip!", listOfTripsSynced);
        Assert.assertEquals("Trip RetrieveSync ALL: Invalid number of returned synced Trip!", 2, listOfTripsSynced.size());
        
        //Update 1st Trip:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreatedFirst.getId());
        tripToUpdate.setNote(TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED - TestRetrieveSync");
        Trip tripUpdated = TripManager.update(tripToUpdate);
        Assert.assertEquals("Trip Update: Incorrect returned updated Trip!", 
                tripUpdated.getId(), tripCreatedFirst.getId());
        
        //Sync - UPDATED date is at 2nd (last) Trip CREATED date but before tripUpdated UPDATED date:
        tripToSync.setUpdated(tripCreatedLast.getUpdated());
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieveSync(tripToSync);
            Assert.assertNotNull("Trip RetrieveSync ALL: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip RetrieveSync ALL: Result should have been SUCCESS!\n" + result, result.isSuccess());
            listOfTripsSynced = result.getListOfTrips();

        } else if(isManagerTest) {
            listOfTripsSynced = TripManager.retrieveSync(tripToSync);
        }
        Assert.assertNotNull("Trip RetrieveSync: Result should have the List Trip!", listOfTripsSynced);
        Assert.assertEquals("Trip RetrieveSync: Invalid number of returned synced Trip!", 1, listOfTripsSynced.size());
        Trip tripSynced = listOfTripsSynced.get(0);
        Assert.assertEquals("Trip RetrieveSync: Incorrect returned synced Trip!", 
                tripUpdated.getId(), tripSynced.getId());
        
        //Sync - Again, at tripUpdated UPDATED date:
        tripToSync.setUpdated(tripUpdated.getUpdated());
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.retrieveSync(tripToSync);
            Assert.assertNotNull("Trip RetrieveSync: Returned result can NOT be null!", result);
            Assert.assertTrue("Trip RetrieveSync: Result should have been SUCCESS!\n" + result, result.isSuccess());
            listOfTripsSynced = result.getListOfTrips();

        } else if(isManagerTest) {
            listOfTripsSynced = TripManager.retrieveSync(tripToSync);
        }
        Assert.assertNull("Trip RetrieveSync: Returned list of synced Trip should have been EMPTY!", listOfTripsSynced);
    }        
    
    @Test
    public void testSearchAndCount_Default() throws Exception {
        int numOfTripsCreated = TestUtilForTrip.tripCreate_byDB_ManyTrips(mUserCreated.getUserId());

        //1. Search - Default (offset = 0, limit = CommonConstants.RECORDS_NUMBER_MAX)
        TripSearchCriteria searchCriteria = new TripSearchCriteria();
        List<Trip> tripsFoundList = null;
        int numOfTrips = 0;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
        }
        Assert.assertNotNull("Trip Search: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search: Invalid number of trips found!", 
                CommonConstants.RECORDS_NUMBER_MAX, tripsFoundList.size());
        Assert.assertEquals("Trip Search: Invalid Trip name for the FIRST Trip found!",  
                TestConstants.UNITTEST_TRIP_NAME + "_LASTTRIP_CREATED", tripsFoundList.get(0).getName());
        
        //2. Count (Be aware that search will return ANY public trip in DB!)
        if(isManagerTest) {
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertTrue("Trip Count: Incorrect number of trips found! Expected AT LEAST [" 
                + numOfTripsCreated + "] but actual [" + numOfTrips + "]", 
                numOfTrips >= numOfTripsCreated);
    }
    
    @Test
    public void testSearchAndCount_withOffsetAndLimit() throws Exception {
        int numOfTripsCreated = TestUtilForTrip.tripCreate_byDB_ManyTrips(mUserCreated.getUserId());

        //1. Search
        TripSearchCriteria searchCriteria = new TripSearchCriteria();
        searchCriteria.setOffset(CommonConstants.RECORDS_NUMBER_MAX);

        int numOfRowsLimit = numOfTripsCreated - CommonConstants.RECORDS_NUMBER_MAX;
        searchCriteria.setNumOfRows(numOfRowsLimit);
        
        List<Trip> tripsFoundList = null;
        int numOfTrips = 0;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
        }
        Assert.assertNotNull("Trip Search: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search: Invalid number of trips found!", 
                numOfRowsLimit, tripsFoundList.size());
        Assert.assertEquals("Trip Search: Invalid Trip name for the LAST Trip found!",  
                TestConstants.UNITTEST_TRIP_NAME + "_FIRSTTRIP_CREATED", tripsFoundList.get(numOfRowsLimit - 1).getName());
        
        /*
         * 2. Count.
         * Be aware that search will return ANY public trip in DB! No setting for offset/reset.
         */
        if(isManagerTest) {
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertTrue("Trip Count: Incorrect number of trips found! Expected AT LEAST [" 
                + numOfTripsCreated + "] but actual [" + numOfTrips + "]", 
                numOfTrips >= numOfTripsCreated);
    }
    
    @Test
    public void testSearchAndCount_WithCriteria() throws Exception {
        /*
         * NOTE:
         * 1. Be aware, default search (without other filters) will retrieve ALL trips in the database 
         *    including existing non-test trips with default order CREATED desc. 
         *    Therefore, testing offset and num of rows could be tricky.
         */
        String tripName = mClassName + "_testSearchAndCount_WithCriteria";
        String tripLocation = "Planet Pluto";
        TripStatusEnum tripStatus = TripStatusEnum.ACTIVE;
        String tripNameForStatus = "testSearch ACTIVEandCOMPLETE";
        String tripUsername = mUserCreated.getUsername();
        String tripUserFullname = mUserCreated.getFullname();
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                TestConstants.UNITTEST_TRIP_NAME, TestConstants.UNITTEST_TRIP_NOTE, tripLocation,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripNameForStatus, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                tripStatus, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripNameForStatus, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        
        //Search:
        TripSearchCriteria searchCriteria = new TripSearchCriteria();
        searchCriteria.setTripKeyword(tripName);
        List<Trip> tripsFoundList = null;
        int numOfTrips = 0;
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertNotNull("Trip Search by criteria trip keyword [" + tripName + "]: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search by criteria trip keyword [" + tripName + "]: Incorrect number of trips found!", 1, tripsFoundList.size());
        Assert.assertEquals("Trip Search by criteria trip keyword [" + tripName + "]: Unexpected Trip name!", tripName, tripsFoundList.get(0).getName());
        Assert.assertEquals("Trip Count by criteria trip keyword [" + tripName + "]: Incorrect number of trips found!", 1, numOfTrips);

        searchCriteria.clear();
        searchCriteria.setTripKeyword(tripLocation);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertNotNull("Trip Search by criteria trip keyword [" + tripLocation + "]: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search by criteria trip keyword [" + tripLocation + "]: Incorrect number of trips found!", 1, tripsFoundList.size());
        Assert.assertEquals("Trip Search by criteria trip keyword [" + tripLocation + "]: Unexpected Trip location!", tripLocation, tripsFoundList.get(0).getLocation());
        Assert.assertEquals("Trip Count by criteria trip keyword [" + tripLocation + "]: Incorrect number of trips found!", 1, numOfTrips);
    
        searchCriteria.clear();
        searchCriteria.setTripKeyword(tripNameForStatus);
        searchCriteria.setStatus(tripStatus);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertNotNull("Trip Search by criteria trip status: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search by criteria trip status: Incorrect number of trips found!", 1, tripsFoundList.size());
        Assert.assertEquals("Trip Search by criteria trip status: Unexpected Trip status!", tripStatus, tripsFoundList.get(0).getStatus());
        Assert.assertEquals("Trip Count by criteria trip status: Incorrect number of trips found!", 1, numOfTrips);
        
        searchCriteria.clear();
        searchCriteria.setUserKeyword(tripUsername);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertNotNull("Trip Search by criteria user keyword [" + tripUsername + "]: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search by criteria user keyword [" + tripUsername + "]: Incorrect number of trips found!", 4, tripsFoundList.size());
        Assert.assertEquals("Trip Count by criteria user keyword [" + tripUsername + "]: Incorrect number of trips found!", 4, numOfTrips);
        
        searchCriteria.clear();
        searchCriteria.setUserKeyword(tripUserFullname);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
            numOfTrips = TripManager.count(searchCriteria);
        }
        Assert.assertNotNull("Trip Search by criteria user keyword [" + tripUserFullname + "]: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Trip Search by criteria user keyword [" + tripUserFullname + "]: Incorrect number of trips found!", 4, tripsFoundList.size());
        Assert.assertEquals("Trip Count by criteria user keyword [" + tripUserFullname + "]: Incorrect number of trips found!", 4, numOfTrips);
        
        searchCriteria.clear();
        searchCriteria.setTripKeyword(tripName);
        searchCriteria.setUserKeyword(tripUsername);
        searchCriteria.setStatus(tripStatus);
        if(isWSClientTest) {
            ServiceResponse result = TripWSClient.search(searchCriteria);
            Assert.assertNotNull("Trip Search: Returned result should NOT be null!", result);
            Assert.assertTrue("Trip Search: Result should have been SUCCESS!\n" + result, result.isSuccess());
            tripsFoundList = result.getListOfTrips();
            numOfTrips = result.getNumOfRecords();

        } else if(isManagerTest) {
            tripsFoundList = TripManager.search(searchCriteria);
            numOfTrips = TripManager.count(searchCriteria);
        }
        if(tripsFoundList != null) {
            Assert.assertEquals("Trip Search by criteria ALL: Incorrect number of trips found!", 0, tripsFoundList.size());
        }
        Assert.assertEquals("Trip Search by criteria ALL: Incorrect number of trips found!", 0, numOfTrips);
    }        
    
    
}
