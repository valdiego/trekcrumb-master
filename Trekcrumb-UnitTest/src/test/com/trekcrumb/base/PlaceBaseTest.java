package test.com.trekcrumb.base;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.business.manager.PlaceManagerTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPlace;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.webserviceclient.PlaceWSClientTest;

import com.trekcrumb.business.manager.PlaceManager;
import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.TripNotFoundException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.webserviceclient.PlaceWSClient;

/**
 * Base abstract test class for entity: Place
 * 
 * This is abstract class to accomodate similar/duplicate use-cases among layer components - 
 * the most obvious ones are Business, WebService and WebServiceClient compoents. 
 * Test methods cover all possible use-cases for the entity in question. 
 * 
 * Tests on Business component ensure we cover the back-end code which include business 
 * logic/orchestration, database and possibly FileServer integration. WebService, in essence, is 
 * just a wrapper of Business component with exposure to JSON/SOAP technology - therefore it has
 * almost exactly the same use-cases. WebServiceClient is the implementation we built to connect
 * to WebService, hence testing the client is exactly testing the WebService itself. Furthermore,
 * the client use-cases are also similar to that of the other two components.
 * 
 * @author Val Triadi
 */
public abstract class PlaceBaseTest {
    @SuppressWarnings("rawtypes")
    protected static Class mTestClassType;
    
    protected static String mClassName;
    protected static User mUserCreated;
    protected static Trip mTripCreated;
    
    private static boolean isManagerTest = false;
    private static boolean isWSClientTest = false;

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mTestClassType == PlaceManagerTest.class) {
            isManagerTest = true;
        
        } else if(mTestClassType == PlaceWSClientTest.class) {
            isWSClientTest = true;
        
        } else {
            Assert.fail("Cannot continue with test! Unknown incoming test class type: " + mTestClassType);
        }

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName;
        mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName); 
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static  void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
        mTripCreated = null;
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
    }    
    
    @Test
    public void testCreate() throws Exception {
        Place placeCreated = null;
        if(isWSClientTest) {
            placeCreated = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, true);
            
        } else if(isManagerTest) {
            placeCreated = TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, true);
        }
        
        //Retrieve Trip:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(mTripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(mTripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
        Trip tripFound = tripListFound.get(0);
        Assert.assertNotNull("Trip Retrieve: Returned Trip should NOT be null!", tripFound);
        Assert.assertNotNull("Trip Retrieve: Trip should have list of Places!", tripFound.getListOfPlaces());
        Assert.assertEquals("Trip Retrieve: Trip has invalid number of Places!", 1, tripFound.getListOfPlaces().size());
        Assert.assertEquals("Trip Retrieve: Invalid Trip Location!", TestConstants.UNITTEST_PLACE_LOCATION, tripFound.getLocation());
        
        Place placeFound = tripFound.getListOfPlaces().get(0);
        TestUtilForPlace.validatePlace(placeCreated, placeFound);
    }
    
    @Test
    public void testCreate_multiplePlaces() throws Exception {
        Place placeCreated_2 = null;
        String placeLocation_2 = "placeLocation_2";
        if(isWSClientTest) {
            TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, false);
            Thread.sleep(1000);
            placeCreated_2 = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, placeLocation_2, true);
            
        } else if(isManagerTest) {
            TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, false);
            Thread.sleep(1000);
            placeCreated_2 = TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, placeLocation_2, true);
        }

        /*
         * Retrieve Trip
         * Note: Place Retrieve by default always: OrderByEnum.ORDER_BY_CREATED_DATE_ASC
         */
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(mTripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(mTripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
        Trip tripFound = tripListFound.get(0);
        Assert.assertNotNull("Trip Retrieve: Returned Trip should NOT be null!", tripFound);
        Assert.assertNotNull("Trip Retrieve: Trip should have list of Places!", tripFound.getListOfPlaces());
        Assert.assertEquals("Trip Retrieve: Trip has invalid number of Places!", 2, tripFound.getListOfPlaces().size());
        Assert.assertEquals("Trip Retrieve: Invalid Trip Location!", placeLocation_2, tripFound.getLocation());
        
        Place placeFound_2 = tripFound.getListOfPlaces().get(1);
        TestUtilForPlace.validatePlace(placeCreated_2, placeFound_2);
    }        
    
    @Test
    public void testCreate_StringWithChars() throws Exception {
        String placeNote = "PlaceNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        String placeLocation = "PlaceLocation ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        Place placeCreated = null;
        if(isWSClientTest) {
            placeCreated = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNote, placeLocation, false);
            
        } else if(isManagerTest) {
            placeCreated = TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNote, placeLocation, false);
        }
        Assert.assertNotNull("Place Create: Returned Place should NOT be null!", placeCreated);
        Assert.assertEquals("Place Create: Invalid Place note!", placeNote, placeCreated.getNote());
        Assert.assertEquals("Place Create: Invalid Place location!", placeLocation, placeCreated.getLocation());
    }
    
    @Test
    public void testCreate_StringNewLines() throws Exception {
        String placeNote = "PlaceNote \n new line PlaceNote";
        String placeLocation = "PlaceLocation \n new line PlaceLocation";
        Place placeCreated = null;
        if(isWSClientTest) {
            placeCreated = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNote, placeLocation, false);
            
        } else if(isManagerTest) {
            placeCreated = TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNote, placeLocation, false);
        }
        Assert.assertNotNull("Place Create: Returned Place should NOT be null!", placeCreated);
        Assert.assertFalse("Place Create: Invalid Place note: " + placeCreated.getNote(), placeCreated.getNote().contains("\n"));
        Assert.assertFalse("Place Create: Invalid Place location: " + placeCreated.getLocation(), placeCreated.getLocation().contains("\n"));
    }
    
    @Test
    public void testCreate_StringLong() throws Exception {
        String placeNote = 
                "PlaceNote 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        String placeLocation = 
                "PlaceLocation 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        Place placeCreated = null;
        if(isWSClientTest) {
            placeCreated = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNote, placeLocation, false);
            
        } else if(isManagerTest) {
            placeCreated = TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNote, placeLocation, false);
        }
        Assert.assertNotNull("Place Create: Returned Place should NOT be null!", placeCreated);
        Assert.assertEquals("Place Create: Invalid size of note!", 
                CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_NOTE, placeCreated.getNote().length());
        Assert.assertEquals("Place Create: Invalid size of location!", 
                CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_LOCATION, placeCreated.getLocation().length());
        Assert.assertEquals("Place Create: Invalid note!", 
                placeNote.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_NOTE), 
                placeCreated.getNote());
        Assert.assertEquals("Place Create: Invalid location!", 
                placeLocation.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_LOCATION), 
                placeCreated.getLocation());
    }

    @Test
    public void testCreate_StringWithNonASCIIChars() throws Exception {
        String placeNoteNonASCII = "PlaceNote ���";
        String placeLocationNonASCII = "PlaceLocation ���";
        Place placeCreated = null;
        if(isWSClientTest) {
            placeCreated = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNoteNonASCII, placeLocationNonASCII, false);
            
        } else if(isManagerTest) {
            placeCreated = TestUtilForPlace.placeCreate_byManager(
                    mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    placeNoteNonASCII, placeLocationNonASCII, false);
        }
        Assert.assertNotNull("Place Create: Returned Place should NOT be null!", placeCreated);
        
        String placeNoteASCII = "PlaceNote oau"; 
        String placeLocationASCII = "PlaceLocation oau";
        Assert.assertEquals("Place Create: Invalid Place note!", placeNoteASCII, placeCreated.getNote());
        Assert.assertEquals("Place Create: Invalid Place location!", placeLocationASCII, placeCreated.getLocation());
    }
    
    @Test
    public void testCreate_Params_Missing() throws Exception {
        //Set all required params:
        Place placeToCreate = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
        placeToCreate.setUserID(mUserCreated.getUserId());
        placeToCreate.setTripID(mTripCreated.getId());

        //Firstly, ensure happiness:
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Create: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            PlaceManager.create(placeToCreate); 
        }
        
        //Missing UserID
        placeToCreate.setUserID(null);
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Place Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PlaceManager.create(placeToCreate); 
                Assert.fail("Place Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }

        //Missing TripID
        placeToCreate.setUserID(mUserCreated.getUserId());
        placeToCreate.setTripID(null);
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Place Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PlaceManager.create(placeToCreate); 
                Assert.fail("Place Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }

        //Missing Latitude
        placeToCreate.setTripID(mTripCreated.getId());
        placeToCreate.setLatitude(0);
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Place Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PlaceManager.create(placeToCreate); 
                Assert.fail("Place Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }
        
        //Missing Longitude
        placeToCreate.setLatitude(TestConstants.UNITTEST_PLACE_LATITUDE);
        placeToCreate.setLongitude(0);
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Place Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PlaceManager.create(placeToCreate); 
                Assert.fail("Place Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }
    }
    
    @Test
    public void testCreate_Params_Invalid() throws Exception {
        //Set all required params:
        Place placeToCreate = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
        placeToCreate.setUserID(mUserCreated.getUserId());
        placeToCreate.setTripID(mTripCreated.getId());

        //Firstly, ensure happiness:
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Create: Result should have been SUCCESS!\n" + result, result.isSuccess());
        } else if(isManagerTest) {
            PlaceManager.create(placeToCreate); 
        }
        
        //Invalid UserID
        placeToCreate.setUserID("nonexistID");
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Place Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PlaceManager.create(placeToCreate); 
                Assert.fail("Place Create: Should have FAILED when required param(s) invalid!");
            } catch(UserNotFoundException unfe) {
                //Expected
            }
        }

        //Invalid TripID
        placeToCreate.setUserID(mUserCreated.getUserId());
        placeToCreate.setTripID("invalidTripID");
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.create(placeToCreate); 
            Assert.assertNotNull("Place Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Place Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.TRIP_SERVICE_TRIP_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PlaceManager.create(placeToCreate); 
                Assert.fail("Place Create: Should have FAILED when required param(s) invalid!");
            } catch(TripNotFoundException unfe) {
                //Expected
            }
        }
    }    

    @Test
    public void testRetrieve_ByID() throws Exception {
        //Existing:
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Retrieve:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        placeToRetrieve.setId(placeCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        }

        Assert.assertNotNull("Place Retrieve: Returned list of retrieved Place should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve: Incorrect number of Places returned!", 1, placeFoundList.size());

        Place placeFound = placeFoundList.get(0);
        TestUtilForPlace.validatePlace(placeCreated, placeFound);
    }    

    @Test
    public void testRetrieve_ByID_multiplePlaces() throws Exception {
        //Existing:
        double latitude_1 = 32.76243;
        double longitude_1 = -117.14818;
        String placeNote_1 = TestConstants.UNITTEST_PLACE_NOTE + "_1";
        String placeLocation_1 = TestConstants.UNITTEST_PLACE_LOCATION + "_1";
        Place placeCreated_1 = TestUtilForPlace.placeCreate_byManager(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                latitude_1, longitude_1, placeNote_1, placeLocation_1, false); 

        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Retrieve:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        placeToRetrieve.setId(placeCreated_1.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        }

        Assert.assertNotNull("Place Retrieve: Returned list of retrieved Place should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve: Incorrect number of Places returned!", 1, placeFoundList.size());
        
        Place placeFound = placeFoundList.get(0);
        TestUtilForPlace.validatePlace(placeCreated_1, placeFound);
    }    

    @Test
    public void testRetrieve_ByTripID() throws Exception {
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        }

        Assert.assertNotNull("Place Retrieve: Returned list of retrieved Place should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve: Incorrect number of Places returned!", 2, placeFoundList.size());
    }
    
    
    
    @Test
    public void testDelete_ByID() throws Exception {
		Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

		//Delete:
        Place placeToDelete = new Place();
        placeToDelete.setUserID(mUserCreated.getUserId());
        placeToDelete.setTripID(mTripCreated.getId());
        placeToDelete.setId(placeCreated.getId());
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID); 
            Assert.assertNotNull("Place Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Delete: Result should have been SUCCESS!", result.isSuccess());
            
        } else if(isManagerTest) {
            int numDeleted = PlaceManager.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }
        
        //Retrieve
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        placeToRetrieve.setId(placeCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        }
        if(placeFoundList != null) {
            Assert.assertEquals("Place Retrieve after Delete: Incorrect number of places returned!", 0, placeFoundList.size());
        }
		
        //Retrieve its Trip:
        List<Trip> tripListFound = null;
        if(isWSClientTest) {
            tripListFound = TestUtilForTrip.tripRetrieve_byWSClient(mTripCreated.getId(), null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID); 
        } else if(isManagerTest) {
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(mTripCreated.getId());
            tripListFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        }
        Assert.assertNotNull("Trip Retrieve: Returned list of Trips should NOT be null!", tripListFound);
        Assert.assertEquals("Trip Retrieve: Returned list of Trips incorrect number!", 1, tripListFound.size());
        if(tripListFound.get(0).getListOfPlaces() != null) {
            Assert.assertEquals("Trip Retrieve: Trip has invalid number of Places!", 0, tripListFound.get(0).getListOfPlaces().size());
        }
    }
    
    @Test
    public void testDelete_ByID_multiplePlaces() throws Exception {
        //Existing
        Place placeCreated_1 = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        
        //Delete one Place
        Place placeToDelete_1 = new Place();
        placeToDelete_1.setUserID(mUserCreated.getUserId());
        placeToDelete_1.setTripID(mTripCreated.getId());
        placeToDelete_1.setId(placeCreated_1.getId());
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.delete(placeToDelete_1, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID); 
            Assert.assertNotNull("Place Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Delete: Result should have been SUCCESS!", result.isSuccess());
            
        } else if(isManagerTest) {
            int numDeleted = PlaceManager.delete(placeToDelete_1, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }

        //Retrieve:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        }
        Assert.assertNotNull("Place Retrieve after Delete: Returned list of retrieved Places should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve after Delete: Incorrect number of Places returned!", 1, placeFoundList.size());
    }        
        
    @Test
    public void testDelete_ByTripID() throws Exception {
        //Existing:
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        
        //Delete (ALL Places)
        Place placeToDelete = new Place();
        placeToDelete.setUserID(mUserCreated.getUserId());
        placeToDelete.setTripID(mTripCreated.getId());
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_TRIPID); 
            Assert.assertNotNull("Place Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Delete: Result should have been SUCCESS!", result.isSuccess());
            
        } else if(isManagerTest) {
            int numDeleted = PlaceManager.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_TRIPID);
            Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 2, numDeleted);
        }
        
        //Retrieve:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        List<Place> placeListFound = null;
        if(isWSClientTest) {
            placeListFound = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        } else if(isManagerTest) {
            placeListFound = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        }
        if(placeListFound != null) {
            Assert.assertEquals("Place Retrieve after Delete: Incorrect number of places returned!", 0, placeListFound.size());
        }
    }
    
    @Test
    public void testDelete_ByUserID() throws Exception {
        //Existing:
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        
        //Delete:
        Place placeToDelete = new Place();
        placeToDelete.setUserID(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_USERID); 
            Assert.assertNotNull("Place Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Delete: Result should have been SUCCESS!", result.isSuccess());
            
        } else if(isManagerTest) {
            int numDeleted = PlaceManager.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_USERID);
            Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 2, numDeleted);
        }

        //Validate:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID);
        }
        if(placeFoundList != null) {
            Assert.assertEquals("Trip Retrieve: Incorrect number of Places returned!", 0, placeFoundList.size());
        }
    }
    
    @Test
    public void testUpdate() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        String noteUpdated = TestConstants.UNITTEST_PLACE_NOTE + "_UPDATED";
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote(noteUpdated);
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Assert.assertEquals("Place Update: Incorrect place note!", noteUpdated, placeUpdated.getNote());
        
        //Retrieve:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        placeToRetrieve.setId(placeCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        }

        Assert.assertNotNull("Place Retrieve: Returned list of retrieved Place should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve: Incorrect number of Places returned!", 1, placeFoundList.size());
        Assert.assertEquals("Place Retrieve: Incorrect place note after Update!", 
                noteUpdated, placeFoundList.get(0).getNote());
    }
    
    @Test
    public void testUpdate_StringWithChars() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        String noteUpdated = "PlaceNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote(noteUpdated);
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Assert.assertEquals("Place Update: Incorrect place note!", noteUpdated, placeUpdated.getNote());
    }

    @Test
    public void testUpdate_StringEmpty() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote(" ");
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Assert.assertEquals("Place Update: Incorrect place note!", "", placeUpdated.getNote());
    }
    
    @Test
    public void testUpdate_StringNewLines() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote("PlaceNote \n new line PlaceNote");
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Assert.assertFalse("Place Update: Invalid Place note: " + placeUpdated.getNote(), placeUpdated.getNote().contains("\n"));
    }
    
    @Test
    public void testUpdate_StringLong() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        String placeNoteNew = 
                "PlaceNote 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote(placeNoteNew);
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Assert.assertEquals("Place Update: Invalid size of note!", CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_NOTE, placeUpdated.getNote().length());
        Assert.assertEquals("Place Update: Invalid note!", 
                placeNoteNew.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_NOTE), 
                placeUpdated.getNote());
    }

    @Test
    public void testUpdate_StringWithNonASCIIChars() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        String placeNoteNonASCII = "PlaceNote ���";
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote(placeNoteNonASCII);
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);

        String placeNoteASCII = "PlaceNote oau"; 
        Assert.assertEquals("Place Update: Invalid note!", 
                placeNoteASCII, 
                placeUpdated.getNote());
    }
    
    @Test
    public void testUpdate_NonEditableFields() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);

        //Update:
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setLatitude(10);
        placeToUpdate.setLongitude(-10);
        placeToUpdate.setLocation("Location - UPDATED");
        if(isWSClientTest) {
            TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            PlaceManager.update(placeToUpdate);
        }
        
        //Retrieve:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setTripID(mTripCreated.getId());
        placeToRetrieve.setId(placeCreated.getId());
        List<Place> placeFoundList = null;
        if(isWSClientTest) {
            placeFoundList = TestUtilForPlace.placeRetrieve_byWSClient(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        } else if(isManagerTest) {
            placeFoundList = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        }

        Assert.assertNotNull("Place Retrieve: Returned list of retrieved Place should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve: Incorrect number of Places returned!", 1, placeFoundList.size());

        Place placeFound = placeFoundList.get(0);
        Assert.assertEquals("Place retrieve after update: : Invalid latitude!", TestConstants.UNITTEST_PLACE_LATITUDE, placeFound.getLatitude());
        Assert.assertEquals("Place retrieve after update: : Invalid longitude!", TestConstants.UNITTEST_PLACE_LONGITUDE, placeFound.getLongitude());
        Assert.assertEquals("Place retrieve after update: : Invalid location!", TestConstants.UNITTEST_PLACE_LOCATION, placeFound.getLocation());
    }
    
    @Test
    public void testUpdate_PlaceNotExist() throws Exception {
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId("invalidID");
        placeToUpdate.setNote("Illegal update!");
        Place placeUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.update(placeToUpdate);
            Assert.assertNotNull("Place update: Returned result can NOT be null!", result);
            Assert.assertFalse("Place update: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place update: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
            Assert.assertNull("Place Update after Delete: Returned updated place should be EMPTY!", placeUpdated);
        }    
    }
    
    @Test
    public void testUpdate_AfterDelete() throws Exception {
        Place placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        
        //Delete:
        Place placeToDelete = new Place();
        placeToDelete.setUserID(mUserCreated.getUserId());
        placeToDelete.setTripID(mTripCreated.getId());
        placeToDelete.setId(placeCreated.getId());
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID); 
            Assert.assertNotNull("Place Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Delete: Result should have been SUCCESS!", result.isSuccess());
            
        } else if(isManagerTest) {
            int numDeleted = PlaceManager.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }

        //Update:
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote("Illegal update!");
        Place placeUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.update(placeToUpdate);
            Assert.assertNotNull("Place update: Returned result can NOT be null!", result);
            Assert.assertFalse("Place update: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place update: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
            Assert.assertNull("Place Update after Delete: Returned updated place should be EMPTY!", placeUpdated);
        }
    }
    


    /**
     * Tests Place create, update and delete for Trip UPDATED date triggers.
     * This test is very time-sensitive since it compares timestamps between Place actions.
     * If test client machine vs. server database do not have sync-up time, it could cause test
     * to fail.
     */
    @Test
    public void testTripUpdatedTriggers() throws Exception {
        Date tripUpdatedDateOrig = DateUtil.formatDate(mTripCreated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);

        //Create:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Place placeCreated = null;
        if(isWSClientTest) {
            placeCreated = TestUtilForPlace.placeCreate_byWSClient(mUserCreated.getUserId(), mTripCreated.getId(), 
                    TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                    TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, false);
            
        } else if(isManagerTest) {
            placeCreated = TestUtilForPlace.placeCreate_byManager_DefaultData(mUserCreated, mTripCreated);
        }
        Date placeCreatedDate = DateUtil.formatDate(placeCreated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);

        //Verify - Compare Trip UPDATED date:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(mTripCreated.getId());
        List<Trip> tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Date tripUpdatedDateAfterPlaceCreated = DateUtil.formatDate(tripListFound.get(0).getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("After Place create: Trip UPDATED should have been changed!", 
                tripUpdatedDateAfterPlaceCreated.after(tripUpdatedDateOrig));
        Assert.assertTrue("After Place create: Trip UPDATED should be the same as Place CREATED!", 
                tripUpdatedDateAfterPlaceCreated.equals(placeCreatedDate));
        
        //Update & Verify:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Place placeToUpdate = new Place();
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setUserID(placeCreated.getUserID());
        placeToUpdate.setTripID(placeCreated.getTripID());
        placeToUpdate.setNote("Hello!");
        Place placeUpdated = null;
        if(isWSClientTest) {
            placeUpdated = TestUtilForPlace.placeUpdate_byWSClient(placeToUpdate);
        } else if(isManagerTest) {
            placeUpdated = PlaceManager.update(placeToUpdate);
        }
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Date placeUpdatedDate = DateUtil.formatDate(placeUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);

        tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Date tripUpdatedDateAfterPlaceUpdated = DateUtil.formatDate(tripListFound.get(0).getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("After Place update: Trip UPDATED should have been changed!", 
                tripUpdatedDateAfterPlaceUpdated.after(tripUpdatedDateAfterPlaceCreated));
        Assert.assertTrue("After Place update: Trip UPDATED should be the same as Picture UPDATED!", 
                tripUpdatedDateAfterPlaceUpdated.equals(placeUpdatedDate));

        //Delete & Verify:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Place placeToDelete = new Place();
        placeToDelete.setId(placeCreated.getId());
        placeToDelete.setUserID(placeCreated.getUserID());
        placeToDelete.setTripID(placeCreated.getTripID());
        if(isWSClientTest) {
            ServiceResponse result = PlaceWSClient.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID); 
            Assert.assertNotNull("Place Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Place Delete: Result should have been SUCCESS!", result.isSuccess());
            
        } else if(isManagerTest) {
            int numDeleted = PlaceManager.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        }

        tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Date tripUpdatedDateAfterPlaceDeleted = DateUtil.formatDate(tripListFound.get(0).getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("After Place delete: Trip UPDATED should have been changed! Previous date [" 
                + tripUpdatedDateAfterPlaceUpdated + "] vs. After delete date [" + tripUpdatedDateAfterPlaceDeleted + "]", 
                tripUpdatedDateAfterPlaceDeleted.after(tripUpdatedDateAfterPlaceUpdated));
    }
    

    


    
    
 
    


}
