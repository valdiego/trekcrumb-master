package test.com.trekcrumb.base;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import test.com.trekcrumb.business.manager.UserAuthTokenManagerTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;
import test.com.trekcrumb.webserviceclient.UserAuthTokenWSClientTest;

import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webserviceclient.UserAuthTokenWSClient;
import com.trekcrumb.webserviceclient.UserWSClient;

/**
 * Base abstract test class for entity: UserAuthToken
 * 
 * This is abstract class to accomodate similar/duplicate use-cases among layer components - 
 * the most obvious ones are Business, WebService and WebServiceClient compoents. 
 * Test methods cover all possible use-cases for the entity in question. 
 * 
 * Tests on Business component ensure we cover the back-end code which include business 
 * logic/orchestration, database and possibly FileServer integration. WebService, in essence, is 
 * just a wrapper of Business component with exposure to JSON/SOAP technology - therefore it has
 * almost exactly the same use-cases. WebServiceClient is the implementation we built to connect
 * to WebService, hence testing the client is exactly testing the WebService itself. Furthermore,
 * the client use-cases are also similar to that of the other two components.
 * 
 * @author Val Triadi
 */
public abstract class UserAuthTokenBaseTest {
    @SuppressWarnings("rawtypes")
    protected static Class mTestClassType;
    
    protected static String mClassName;
    protected static String mUsername;
    protected static User mUserCreated;
    protected static User mUserToLogin;
    
    private static boolean isManagerTest = false;
    private static boolean isWSClientTest = false;
   

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mTestClassType == UserAuthTokenManagerTest.class) {
            isManagerTest = true;
        
        } else if(mTestClassType == UserAuthTokenWSClientTest.class) {
            isWSClientTest = true;
        
        } else {
            Assert.fail("Cannot continue with test! Unknown incoming test class type: " + mTestClassType);
        }
        
        //Setup prereq data:
        mUsername = String.valueOf(System.currentTimeMillis());

        String email = mUsername + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName;
        mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, mUsername, fullname);
        
        mUserToLogin = new User();
        mUserToLogin.setUsername(mUsername);
        if(isWSClientTest) {
            mUserToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
        } else if(isManagerTest) {
            mUserToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
        }
    }    

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForUserAuthToken.userAuthTokenDelete_byDB(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreate() throws Exception {
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenCreate_byWSClient(
                    mUserToLogin, deviceIdentity, true, 1);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, true);
        }
    }
    
    @Test
    public void testCreate_SameUserSameDevice() throws Exception {
        //Existing:
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        UserAuthToken uat_existing = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, false);

        //Create duplicate with same device:
        UserAuthToken uatCreated_new = null;
        UserAuthToken uatToCreate = new UserAuthToken();
        uatToCreate.setDeviceIdentity(deviceIdentity);
        if(isWSClientTest) {
            uatCreated_new = TestUtilForUserAuthToken.userAuthTokenCreate_byWSClient(
                    mUserToLogin, deviceIdentity, true, 1);

        } else if(isManagerTest) {
            uatCreated_new = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, true);
        }               
        
        Assert.assertFalse("AuthToken Create: Returned auth token shall have been updated with a new one!", 
                uat_existing.getAuthToken().equalsIgnoreCase(uatCreated_new.getAuthToken()));
    }
    
    @Test
    public void testCreate_SameUserMultipleDevices() throws Exception {
        //Existing:
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), 
                        TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        
        //Create new:
        String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenCreate_byWSClient(
                    mUserToLogin, deviceIdentity_2, true, 2);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, true);
        }        
    }    

    @Ignore("Functionality has not been implemented. There is DEFECT in code!")
    @Test
    public void testCreate_DifferentUserSameDevice() throws Exception {
        Assert.fail("UserAuthToken Create: Different User Same Device - Not implemented!");
    }
    
    @Test
    public void testAuthenticate() throws Exception {
        //Create:
        UserAuthToken uatCreated = null;
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        if(isWSClientTest) {
            uatCreated = TestUtilForUserAuthToken.userAuthTokenCreate_byWSClient(
                    mUserToLogin, deviceIdentity, false, -1);

        } else if(isManagerTest) {
            uatCreated = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                            mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, false);
        }
        
        Assert.assertNotNull("UserAuthToken Create: Returned auth token shall NOT be empty!", uatCreated);
        Assert.assertNotNull("UserAuthToken Create: Returned auth token shall NOT be empty!", uatCreated.getAuthToken());
        
        //Authenticate:
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), uatCreated.getAuthToken(), true, true, null);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), uatCreated.getAuthToken(), true, true);
        }        
    }
    
    @SuppressWarnings("unused")
    @Test
    public void testAuthenticate_SameUserMultipleDevices() throws Exception {
        //Existing:
        UserAuthToken uatCreated_1 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        UserAuthToken uatCreated_2 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Authenticate:
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), uatCreated_1.getAuthToken(), true, true, null);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), uatCreated_1.getAuthToken(), true, true);
        }        
    }
    
    @Ignore("Functionality has not been implemented. There is DEFECT in code!")
    @Test
    public void testAuthenticate_DifferentUserSameDevice() throws Exception {
        Assert.fail("UserAuthToken Authenticate: Different User Same Device - Not implemented!");
    }
    
    @Test
    public void testAuthenticate_InvalidAuthToken() throws Exception {
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Authenticate:
        String someRandomAuthToken = CommonUtil.generateID(null);
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), someRandomAuthToken, true, false, 
                    ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), someRandomAuthToken, true, false);
        }        
    }
    
    @Test
    public void testAuthenticate_AfterDelete() throws Exception {
        UserAuthToken uatCreated = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Delete (by manager):
        TestUtilForUserAuthToken.userAuthTokenDelete_byManager(uatCreated.getId());
        
        //Authenticate:
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), uatCreated.getAuthToken(), true, false, 
                    ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), uatCreated.getAuthToken(), true, false);
        }        
    }                
    
    @Test
    public void testDelete_ByID() throws Exception {
        UserAuthToken uatCreated = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        
        //Delete by ID:
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setUserId(mUserCreated.getUserId());
        uatToDelete.setId(uatCreated.getId());
        if(isWSClientTest) {
            ServiceResponse svcResponse = 
                    UserAuthTokenWSClient.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
            Assert.assertNotNull("UserAuthToken Delete: Response should NOT be null!", svcResponse);
            Assert.assertTrue("UserAuthToken Delete: Response should have been SUCCESS!", svcResponse.isSuccess());
            Assert.assertNotNull("UserAuthToken Delete: Response record UserAuthToken list after delete shall NOT be null!", svcResponse.getListOfUserAuthTokens());
            Assert.assertEquals("UserAuthToken Delete: Invalid number of returned UserAuthToken records!", 0, svcResponse.getListOfUserAuthTokens().size());

        } else if(isManagerTest) {
            int numDeleted = TestUtilForUserAuthToken.userAuthTokenDelete_byManager(uatCreated.getId());
            Assert.assertEquals("UserAuthToken Delete: Invalid number of deleted record!", 1, numDeleted);    
        }
        
        //Verify by Authenticate:
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), uatCreated.getAuthToken(), true, false, 
                    ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), uatCreated.getAuthToken(), true, false);
        }        
    }    
    
    @Test
    public void testDelete_ByID_MultipleDevices() throws Exception {
        //Existing:
        UserAuthToken authTokenCreated_1 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        UserAuthToken authTokenCreated_2 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Delete:
        List<UserAuthToken> recordListAfterDelete = null;
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setUserId(mUserCreated.getUserId());
        uatToDelete.setId(authTokenCreated_1.getId());
        if(isWSClientTest) {
            ServiceResponse svcResponse = UserAuthTokenWSClient.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
            Assert.assertNotNull("UserAuthToken Delete: Response should NOT be null!", svcResponse);
            Assert.assertTrue("UserAuthToken Delete: Response should have been SUCCESS!", svcResponse.isSuccess());
            recordListAfterDelete = svcResponse.getListOfUserAuthTokens();

        } else if(isManagerTest) {
            recordListAfterDelete = UserAuthTokenManager.delete(uatToDelete);
        }
        
        Assert.assertNotNull("UserAuthToken Delete: Returned UserAuthToken remaining shall NOT be null!", 
                recordListAfterDelete);
        Assert.assertEquals("UserAuthToken Delete: Returned UserAuthToken remaining invalid size!", 
                1, recordListAfterDelete.size());
        Assert.assertEquals("UserAuthToken Delete: Returned UserAuthToken remaining invalid token ID!", 
                authTokenCreated_2.getId(), recordListAfterDelete.get(0).getId());
        Assert.assertEquals("UserAuthToken Delete: Returned UserAuthToken remaining invalid token value!", 
                authTokenCreated_2.getAuthToken(), recordListAfterDelete.get(0).getAuthToken());
        
        //Verify by Authenticate (good one):
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), authTokenCreated_2.getAuthToken(), true, true, null);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), authTokenCreated_2.getAuthToken(), true, true);
        }
        
        //Verify by Authenticate (bad one):
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), authTokenCreated_1.getAuthToken(), true, false, 
                    ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), authTokenCreated_1.getAuthToken(), true, false);
        }        
    }    
    
    @Test
    public void testDelete_ByUser() throws Exception {
        //Existing:
        UserAuthToken uatCreated_1 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Delete by User:
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setUserId(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse svcResponse = 
                    UserAuthTokenWSClient.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            Assert.assertNotNull("UserAuthToken Delete: Response should NOT be null!", svcResponse);
            Assert.assertTrue("UserAuthToken Delete: Response should have been SUCCESS!", svcResponse.isSuccess());

        } else if(isManagerTest) {
            int numDeleted = 
                    UserAuthTokenManager.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            Assert.assertEquals("Invalid number of deleted record!", 2, numDeleted);
        }
        
        //Verify by Authenticate:
        if(isWSClientTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byWSClient(
                    mUserCreated.getUserId(), uatCreated_1.getAuthToken(), true, false, 
                    ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR);

        } else if(isManagerTest) {
            TestUtilForUserAuthToken.userAuthTokenAuthenticate_byManager(
                    mUserCreated.getUserId(), uatCreated_1.getAuthToken(), true, false);
        }        
        
        //Verify all is gone:
        if(isWSClientTest) {
            ServiceResponse svcResponse = UserWSClient.userLogin(mUserToLogin);
            if(svcResponse != null && svcResponse.getUser() != null) {
                User userLogin = svcResponse.getUser();
                Assert.assertNotNull("UserAuthToken Delete: Returned User login should have list of tokens!", userLogin.getListOfAuthTokens());
                Assert.assertEquals("UserAuthToken Delete: Returned User login should have ZERO tokens!", 0, userLogin.getListOfAuthTokens().size());
            } else {
                Assert.fail("User Login: Unexpectedly FAILED! \n" + svcResponse);
            }

        } else if(isManagerTest) {
            UserAuthToken authTokenRetrieve = new UserAuthToken();
            authTokenRetrieve.setUserId(mUserCreated.getUserId());
            List<UserAuthToken> recordList = 
                    UserAuthTokenManager.retrieve(authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            if(recordList != null) {
                Assert.assertEquals("Returned record list shall BE empty!", 0, recordList.size());
            } else {
                //God Bless!
            }
        }
    }
    

}
