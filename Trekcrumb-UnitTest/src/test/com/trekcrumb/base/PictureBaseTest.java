package test.com.trekcrumb.base;

import java.util.Date;
import java.util.List;

import javax.naming.OperationNotSupportedException;

import junit.framework.Assert;

import org.apache.commons.codec.binary.Base64;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.business.manager.PictureManagerTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPicture;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.webserviceclient.PictureWSClientTest;

import com.trekcrumb.business.manager.PictureManager;
import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.TripNotFoundException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.FileUtil;
import com.trekcrumb.webserviceclient.PictureWSClient;

/**
 * Base abstract test class for entity: Picture
 * 
 * This is abstract class to accomodate similar/duplicate use-cases among layer components - 
 * the most obvious ones are Business, WebService and WebServiceClient compoents. 
 * Test methods cover all possible use-cases for the entity in question. 
 * 
 * Tests on Business component ensure we cover the back-end code which include business 
 * logic/orchestration, database and possibly FileServer integration. WebService, in essence, is 
 * just a wrapper of Business component with exposure to JSON/SOAP technology - therefore it has
 * almost exactly the same use-cases. WebServiceClient is the implementation we built to connect
 * to WebService, hence testing the client is exactly testing the WebService itself. Furthermore,
 * the client use-cases are also similar to that of the other two components.
 * 
 * @author Val Triadi
 */
public abstract class PictureBaseTest {
    @SuppressWarnings("rawtypes")
    protected static Class mTestClassType;
    
    protected static String mClassName;
    protected static User mUserCreated;
    protected static Trip mTripCreated;
    
    private static boolean isManagerTest = false;
    private static boolean isWSClientTest = false;

    private static byte[] mImageJPGSmallBytes;
    private Picture mPictureCreatedWithImage;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mTestClassType == PictureManagerTest.class) {
            isManagerTest = true;
        
        } else if(mTestClassType == PictureWSClientTest.class) {
            isWSClientTest = true;
        
        } else {
            Assert.fail("Cannot continue with test! Unknown incoming test class type: " + mTestClassType);
        }

        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName;
        mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName); 
        
        mImageJPGSmallBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL);
        Assert.assertNotNull("Test CANCELLED! Image bytes from the test file [" 
                + TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL + "] cannot be null!", mImageJPGSmallBytes);
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static  void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
        mTripCreated = null;
        mImageJPGSmallBytes = null;
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
        
        if(mPictureCreatedWithImage != null) {
            try {
                TestUtilForPicture.imageDelete_byFSClient(mPictureCreatedWithImage.getImageName(), true);
            } catch(Exception e) {
                //Ignore
            }
            mPictureCreatedWithImage = null;
        }
    }    
    
    @Test
    public void testCreate_JPG_Small() throws Exception {
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, TestConstants.UNITTEST_PICTURE_NOTE, true);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, TestConstants.UNITTEST_PICTURE_NOTE, true);
        }
    }
    
    @Test
    public void testCreate_JPG_Big() throws Exception {
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_JPG_BIG);
        Assert.assertNotNull("Picture create: Image bytes from the file cannot be null!", imgBytes);
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
        }
    }    

    @Test
    public void testCreate_BMP_24BIT() throws Exception {
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_BMP_24BIT);
        Assert.assertNotNull("Picture create: Image bytes from the file cannot be null!", imgBytes);
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
        }
    }    

    @Test
    public void testCreate_BMP_32BIT() throws Exception {
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_BMP_32BIT);
        Assert.assertNotNull("Picture create: Image bytes from the file cannot be null!", imgBytes);
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
        }
    }    

    @Test
    public void testCreate_PNG() throws Exception {
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_PNG);
        Assert.assertNotNull("Picture create: Image bytes from the file cannot be null!", imgBytes);
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
        }
    }
    
    /**
     * NOTE:
     * 1. 8/2016: Image format not supported
     * Known issue with Trekcrumb FileServer component when image format is GIF, 
     * java ImageIO.write() API would throw IIOException with error: "Invalid argument to native writeImage". 
     * The issue is JRE being used by the remote OPENSHIFT server if it is  OpenJDK, it has this bug. 
     * Hence, this was not an issue in local dev server. 
     * Solution is to either change the JRE or use GIF plug-in, including to support GIF animation.
     * 
     * Note: We do not read GIF image from file like other testcases because FileUtil.readImageFromFileDirectory()
     * by default would change the image stream as JPEG. We need its original format (GIF) info to
     * ensure the business logic takes place. Hence, it is from constant byte arrays.
     * 
     * 2. 1/2018: Image format is supported
     * Since we migrated to use Google Cloud Storage FileServer, this is no longer an issue.
     * Hence, the test expects operation to succeed.
     */
    @Test
    public void testCreate_GIF() throws Exception {
        //byte[] imgBytes = FileUtil.readImageFromFileDirectory(
        //        TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_GIF);
        byte[] imgBytes = Base64.decodeBase64(TestConstants.UNITTEST_PICTURE_FILE_GIF_BASE64DATA.getBytes());
        Assert.assertNotNull("Picture create: Image bytes from the file cannot be null!", imgBytes);
        
        /*
        //Per 1/2018: We now expect GIF to pass
        Picture pictureToCreate = new Picture();
        pictureToCreate.setUserID(mUserCreated.getUserId());
        pictureToCreate.setTripID(mTripCreated.getId());
        pictureToCreate.setImageBytes(imgBytes);
        pictureToCreate.setImageName(CommonUtil.generatePictureFilename(CLASS_NAME));
        try {
            mPictureCreated = PictureManager.create(pictureToCreate);
            Assert.fail("PictureManager.create() should have thrown ImageFormatNotSupportedException when image format is GIF!");
        } catch(ImageFormatNotSupportedException ifnse) {
            //Expected
        }
        */
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), imgBytes, 
                    null, null, null, true);
        }
    }    

    @Test
    public void testCreate_StringWithChars() throws Exception {
        String pictureNote = "PictureNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNote, false);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNote, false);
        }
        Assert.assertNotNull("Picture Create: Returned Picture should NOT be null!", mPictureCreatedWithImage);
        Assert.assertEquals("Picture Create: Invalid Picture note!", pictureNote, mPictureCreatedWithImage.getNote());
    }
    
    @Test
    public void testCreate_StringNewLines() throws Exception {
        String pictureNote = "PictureNote \n new line PictureNote";
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNote, false);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNote, false);
        }
        Assert.assertNotNull("Picture Create: Returned Picture should NOT be null!", mPictureCreatedWithImage);
        Assert.assertFalse("Picture Create: Invalid Picture note: " + mPictureCreatedWithImage.getNote(), mPictureCreatedWithImage.getNote().contains("\n"));
    }

    @Test
    public void testCreate_StringLong() throws Exception {
        String pictureNote = 
                "PictureNote 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNote, false);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNote, false);
        }
        Assert.assertNotNull("Picture Create: Returned Picture should NOT be null!", mPictureCreatedWithImage);
        Assert.assertEquals("Picture Create: Invalid size of note!", CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE, mPictureCreatedWithImage.getNote().length());
        Assert.assertEquals("Picture Create: Invalid note!", 
                pictureNote.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE), 
                mPictureCreatedWithImage.getNote());
    }
    
    @Test
    public void testCreate_StringWithNonASCIIChars() throws Exception {
        String pictureNoteNonASCII = "PictureNote ���";
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNoteNonASCII, false);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, pictureNoteNonASCII, false);
        }
        Assert.assertNotNull("Picture Create: Returned Picture should NOT be null!", mPictureCreatedWithImage);
        
        String pictureNoteASCII = "PictureNote oau"; 
        Assert.assertEquals("Picture Create: Invalid Picture note!", pictureNoteASCII, mPictureCreatedWithImage.getNote());
    }    
    
    @Test
    public void testCreate_Params_Missing() throws Exception {
        //Set all required params:
        Picture pictureToCreate = new Picture();
        pictureToCreate.setUserID(mUserCreated.getUserId());
        pictureToCreate.setTripID(mTripCreated.getId());
        pictureToCreate.setImageBytes(mImageJPGSmallBytes);

        //Missing UserID
        pictureToCreate.setUserID(null);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.create(pictureToCreate); 
            Assert.assertNotNull("Picture Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PictureManager.create(pictureToCreate); 
                Assert.fail("Picture Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }

        //Missing TripID
        pictureToCreate.setUserID(mUserCreated.getUserId());
        pictureToCreate.setTripID(null);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.create(pictureToCreate); 
            Assert.assertNotNull("Picture Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PictureManager.create(pictureToCreate); 
                Assert.fail("Picture Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }

        //Missing image bytes
        pictureToCreate.setTripID(mTripCreated.getId());
        pictureToCreate.setImageBytes(null);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.create(pictureToCreate); 
            Assert.assertNotNull("Picture Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PictureManager.create(pictureToCreate); 
                Assert.fail("Picture Create: Should have FAILED when required param(s) missing!");
            } catch(IllegalArgumentException iae) {
                //Expected
            }
        }
    }
    
    @Test
    public void testCreate_Params_Invalid() throws Exception {
        //Set all required params:
        Picture pictureToCreate = new Picture();
        pictureToCreate.setUserID(mUserCreated.getUserId());
        pictureToCreate.setTripID(mTripCreated.getId());
        pictureToCreate.setImageBytes(mImageJPGSmallBytes);

        //Invalid UserID
        pictureToCreate.setUserID("nonexistID");
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.create(pictureToCreate); 
            Assert.assertNotNull("Picture Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PictureManager.create(pictureToCreate); 
                Assert.fail("Picture Create: Should have FAILED when required param(s) invalid!");
            } catch(UserNotFoundException unfe) {
                //Expected
            }
        }

        //Invalid TripID
        pictureToCreate.setUserID(mUserCreated.getUserId());
        pictureToCreate.setTripID("invalidTripID");
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.create(pictureToCreate); 
            Assert.assertNotNull("Picture Create: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Create: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Create: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Create: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.TRIP_SERVICE_TRIP_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
        } else if(isManagerTest) {
            try {
                PictureManager.create(pictureToCreate); 
                Assert.fail("Picture Create: Should have FAILED when required param(s) invalid!");
            } catch(TripNotFoundException tnfe) {
                //Expected
            }
        }
    }    

    @Test
    public void testRetrieve_ByID() throws Exception {
        //Existing:
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, TestConstants.UNITTEST_PICTURE_NOTE, false);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, TestConstants.UNITTEST_PICTURE_NOTE, false);
        }

        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setId(mPictureCreatedWithImage.getId());
        picToRetrieve.setTripID(mTripCreated.getId());
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 1, picListFound.size());
        Picture picFound = picListFound.get(0);
        TestUtilForPicture.validatePicture(mPictureCreatedWithImage, picFound);
    }
    
    @Test
    public void testRetrieve_ByTripID() throws Exception {
        //Existing (actual images are not necessary):
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());

        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setTripID(mTripCreated.getId());
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 1, picListFound.size());
    }    
    
    @Test
    public void testRetrieve_ByTripID_DefaultOrderBy() throws Exception {
        //Existing (actual images are not necessary):
        String pic1Note = "Pic 1";
        String pic2Note = "Pic 2";
        Picture pictureCreated_1 = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, pic1Note, false);
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Picture pictureCreated_2 = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, pic2Note, false);

        //Retrieve - Default Order-By = OrderByEnum.ORDER_BY_CREATED_DATE_ASC
        Picture picToRetrieve = new Picture();
        picToRetrieve.setTripID(mTripCreated.getId());
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 2, picListFound.size());
        
        Picture picFound_1 = picListFound.get(0);
        TestUtilForPicture.validatePicture(pictureCreated_1, picFound_1);

        Picture picFound_2 = picListFound.get(1);
        TestUtilForPicture.validatePicture(pictureCreated_2, picFound_2);
    }        
    
    @Test
    public void testRetrieve_ByUserID() throws Exception {
        //Existing (actual images are not necessary):
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());

        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setUserID(mUserCreated.getUserId());
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Retrieve: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Retrieve: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Retrieve: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            try {
                PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID, null, 0, -1);
                Assert.fail("Picture Retrieve: Should have FAILED! Retrieve by USERID is not allowed from outside!");
            } catch(OperationNotSupportedException onse) {
                //Expected
            }
        }
    }
    
    @Test
    public void testRetrieve_ByUsername() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, TestConstants.UNITTEST_PICTURE_NOTE, false);

        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setUsername(mUserCreated.getUsername());
        picToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 1, picListFound.size());
        Picture picFound = picListFound.get(0);
        TestUtilForPicture.validatePicture(pictureCreated, picFound);
        
        //Enrichment Data:
        Assert.assertEquals("Picture Retrieve: Invalid Trip name!", mTripCreated.getName(), picFound.getTripName());
        Assert.assertEquals("Picture Retrieve: Invalid Username!", mUserCreated.getUsername(), picFound.getUsername());
        Assert.assertEquals("Picture Retrieve: Invalid User fullname!", mUserCreated.getFullname(), picFound.getUserFullname());
        if(mUserCreated.getProfileImageName() != null) {
            Assert.assertEquals("Picture Retrieve: Invalid User profile imagename!", 
                    mUserCreated.getProfileImageName(), picFound.getUserImageName());
        }
    }           
        
    @Test
    public void testRetrieve_ByUsername_DefaultOrderBy() throws Exception {
        //Existing (actual images are not necessary):
        String pic1Note = "Pic 1";
        String pic2Note = "Pic 2";
        Picture pictureCreated_1 = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, pic1Note, false);
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Picture pictureCreated_2 = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, pic2Note, false);

        //Retrieve - Default Order-By = OrderByEnum.ORDER_BY_CREATED_DATE_ASC
        Picture picToRetrieve = new Picture();
        picToRetrieve.setUsername(mUserCreated.getUsername());
        picToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 2, picListFound.size());

        Picture picFound_1 = picListFound.get(0);
        TestUtilForPicture.validatePicture(pictureCreated_1, picFound_1);

        Picture picFound_2 = picListFound.get(1);
        TestUtilForPicture.validatePicture(pictureCreated_2, picFound_2);
    }           
        
    @Test
    public void testRetrieve_ByUsername_WithPrivacy() throws Exception {
        //Existing (actual images are not necessary):
        Trip tripPublic = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        Picture picPublic = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), tripPublic.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, "Pic Public", false);

        Trip tripFriendsOnly = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.FRIENDS, null, false);
        TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), tripFriendsOnly.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, "Pic Friends", false);

        Trip tripPrivate = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), mClassName, TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, null, false);
        TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), tripPrivate.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, "Pic Private", false);

        List<Picture> picListFound = null;
        Picture picToRetrieve = new Picture();
        picToRetrieve.setUsername(mUserCreated.getUsername());
        
        //Retrieve - Missing
        picToRetrieve.setPrivacy(null);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Retrieve: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Place update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Place update: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, result.getServiceError().getErrorEnum());

        } else if(isManagerTest) {
            try {
                PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1);
                Assert.fail("Picture Retrieve: By username, it should have failed due to missing 'Privacy' param!");
            } catch(IllegalArgumentException ile) {
                //Expected
            }
        }
        
        //Retrieve - PUBLIC only:
        picToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 1, picListFound.size());
        Picture picFound = picListFound.get(0);
        TestUtilForPicture.validatePicture(picPublic, picFound);
        
        //Retrieve - PUBLIC and FRIENDS only:
        picToRetrieve.setPrivacy(TripPrivacyEnum.FRIENDS);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 2, picListFound.size());
        
        //Retrieve - PUBLIC, FRIENDS and PRIVATE (ALL):
        picToRetrieve.setPrivacy(TripPrivacyEnum.PRIVATE);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 3, picListFound.size());
    }           
    
    @Test
    public void testRetrieve_ByUsername_WithOffsetLimit() throws Exception {
        //Existing (actual images are not necessary):
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());

        Picture picToRetrieve = new Picture();
        picToRetrieve.setUsername(mUserCreated.getUsername());
        picToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
        List<Picture> picListFound = null;
 
        //Retrieve - ALL
        int offset = 0;
        int limit = -1;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, offset, limit); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, offset, limit);
        }
        Assert.assertNotNull("Picture retrieved by username: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture retrieved: Invalid number of found items!", 3, picListFound.size());

        //Retrieve - Set OFFSET & LIMIT
        offset = 1;
        limit = CommonConstants.RECORDS_NUMBER_MAX;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, offset, limit); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, offset, limit);
        }
        Assert.assertNotNull("Picture retrieved by username: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture retrieved: Invalid number of found items!", 2, picListFound.size());
        
        //Retrieve - Set LIMIT ONLY
        offset = -1;
        limit = 1;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, offset, limit); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME, null, offset, limit);
        }
        Assert.assertNotNull("Picture retrieved by username: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture retrieved: Invalid number of found items!", 1, picListFound.size());
    }        
    
    @Test
    public void testSearch() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, TestConstants.UNITTEST_PICTURE_NOTE, false);

        /*
         * Search: 
         * WARNING: To validate exact number of items is not possible since it will return the last
         * CommonConstants.RECORDS_NUMBER_MAX records, which could be exactly that much or less
         * depending test data in the DB. But for sure, the last records MUST be this test data
         * (descending by created date).
         */
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            Picture pictureToSearch = new Picture();
            ServiceResponse result = PictureWSClient.retrieve(pictureToSearch, ServiceTypeEnum.PICTURE_SEARCH, null, 0, -1);
            Assert.assertNotNull("Picture Search: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Search: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(null, ServiceTypeEnum.PICTURE_SEARCH, null, 0, -1);
        }
        Assert.assertNotNull("Picture Search: Returned list should NOT be null!", picListFound);

        Picture picFound = picListFound.get(0);
        TestUtilForPicture.validatePicture(pictureCreated, picFound);
        
        //Enrichment Data:
        Assert.assertEquals("Picture Search: Invalid Trip name!", mTripCreated.getName(), picFound.getTripName());
        Assert.assertEquals("Picture Search: Invalid Username!", mUserCreated.getUsername(), picFound.getUsername());
        Assert.assertEquals("Picture Search: Invalid User fullname!", mUserCreated.getFullname(), picFound.getUserFullname());
        if(mUserCreated.getProfileImageName() != null) {
            Assert.assertEquals("Picture Search: Invalid User profile imagename!", 
                    mUserCreated.getProfileImageName(), picFound.getUserImageName());
        }
    }            
    
    @Test
    public void testSearch_DefaultOrderBy() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated_1 = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.YES, null, "Pic 1", false);
        
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Picture pictureCreated_2 = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, "Pic 2", false);

        //Search - Default Order-By = OrderByEnum.ORDER_BY_CREATED_DATE_DESC
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            Picture pictureToSearch = new Picture();
            ServiceResponse result = PictureWSClient.retrieve(pictureToSearch, ServiceTypeEnum.PICTURE_SEARCH, null, 0, -1);
            Assert.assertNotNull("Picture Search: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Search: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(null, ServiceTypeEnum.PICTURE_SEARCH, null, 0, -1);
        }
        Assert.assertNotNull("Picture Search: Returned list should NOT be null!", picListFound);

        Picture picFound_2 = picListFound.get(0);
        TestUtilForPicture.validatePicture(pictureCreated_2, picFound_2);

        Picture picFound_1 = picListFound.get(1);
        TestUtilForPicture.validatePicture(pictureCreated_1, picFound_1);
    }
        
    @Test
    public void testUpdate_ALL() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        String noteNew = "Being updated.";
        YesNoEnum isPictureCover = YesNoEnum.YES;
        String featurePlaceIDNew = "12345";
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(noteNew);
        picToUpdate.setCoverPicture(isPictureCover);
        picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        TestUtilForPicture.validatePicture(picToUpdate, picUpdated);
    }
    
    @Test
    public void testUpdate_ALL_ThenRetrieve() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        String noteNew = "Being updated.";
        YesNoEnum isPictureCover = YesNoEnum.YES;
        String featurePlaceIDNew = "12345";
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(noteNew);
        picToUpdate.setCoverPicture(isPictureCover);
        picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        
        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setId(pictureCreated.getId());
        picToRetrieve.setTripID(mTripCreated.getId());
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 0, -1);
        }
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", picListFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 1, picListFound.size());
        
        Picture picFound = picListFound.get(0);
        TestUtilForPicture.validatePicture(picToUpdate, picFound);
        TestUtilForPicture.validatePicture(picUpdated, picFound);
    }
    
    @Test
    public void testUpdate_StringWithChars() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        String newNote = "PictureNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(newNote);
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update: Invalid updated note!", newNote, picUpdated.getNote());
    }
    
    @Test
    public void testUpdate_StringEmpty() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(" ");
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update: Invalid updated note!", "", picUpdated.getNote());
    }
    
    @Test
    public void testUpdate_StringNewLines() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote("PictureNote \n new line PictureNote");
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertFalse("Picture Update: Invalid Picture note: " + picUpdated.getNote(), picUpdated.getNote().contains("\n"));
    }
    
    @Test
    public void testUpdate_StringLong() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        String pictureNoteNew = 
                "PictureNote 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(pictureNoteNew);
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update: Invalid size of note!", 
                CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE, picUpdated.getNote().length());
        Assert.assertEquals("Picture Update: Invalid note!", 
                pictureNoteNew.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE), 
                picUpdated.getNote());
    }
    
    @Test
    public void testUpdate_StringWithNonASCIIChars() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        
        //Update:
        String pictureNoteNonASCII = "PictureNote ���";
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(pictureNoteNonASCII);
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        
        String pictureNoteASCII = "PictureNote oau"; 
        Assert.assertEquals("Picture Create: Invalid Picture note!", pictureNoteASCII, picUpdated.getNote());
    }
    
    @Test
    public void testUpdate_CoverPicture() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, YesNoEnum.NO, null, null, false);
        Assert.assertEquals("Picture Create: Invalid value of 'cover picture'!", 
                YesNoEnum.NO, pictureCreated.getCoverPicture());

        //Update - YES:
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setCoverPicture(YesNoEnum.YES);
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update: Invalid 'cover picture' value!", YesNoEnum.YES, picUpdated.getCoverPicture());
        
        //Update - NULL:
        picToUpdate.setCoverPicture(null);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update: Invalid 'cover picture' value!", YesNoEnum.YES, picUpdated.getCoverPicture());

        //Update - NO:
        picToUpdate.setCoverPicture(YesNoEnum.NO);
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update 2: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update 2: Invalid updated 'is_cover_picture' value!",  YesNoEnum.NO, picUpdated.getCoverPicture());
    }
        
    @Test
    public void testDelete_ByID() throws Exception {
        //Existing:
        mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                null, null, TestConstants.UNITTEST_PICTURE_NOTE, false);

        //Delete:
        Picture picToDelete = new Picture();
        picToDelete.setId(mPictureCreatedWithImage.getId());
        picToDelete.setUserID(mUserCreated.getUserId());
        picToDelete.setTripID(mTripCreated.getId());
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertNotNull("Picture Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Delete: Result should have been SUCCESS! \n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertEquals("Picture Delete: Invalid number Pictures deleted!", 1, numDeleted);
        }
        
        //Verify physical image is REMOVED:
        Assert.assertFalse("Picture Delete: Picture image [" + mPictureCreatedWithImage.getImageName() +"] should have been deleted!", 
                TestUtilForPicture.imageRead_byFSClient(mPictureCreatedWithImage.getImageName(), true));
    }
    
    @Test
    public void testDelete_ByID_ThenRetrieve() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, null, null, null, false);

        //Delete:
        Picture picToDelete = new Picture();
        picToDelete.setId(pictureCreated.getId());
        picToDelete.setUserID(pictureCreated.getUserID());
        picToDelete.setTripID(pictureCreated.getTripID());
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertNotNull("Picture Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Delete: Result should have been SUCCESS! \n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertEquals("Picture Delete: Invalid number Pictures deleted!", 1, numDeleted);
        }
        
        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setId(pictureCreated.getId());
        picToRetrieve.setTripID(pictureCreated.getId());
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 0, -1);
        }
        Assert.assertNull("Picture Retrieve after Delete: Returned picture list should have been EMPTY!", picListFound);
    }

    @Test
    public void testDelete_ByID_ThenUpdate() throws Exception {
        //Existing (actual images are not necessary):
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), CommonUtil.generatePictureFilename(mClassName), 
                null, null, null, null, false);
        
        //Delete:
        Picture picToDelete = new Picture();
        picToDelete.setId(pictureCreated.getId());
        picToDelete.setUserID(pictureCreated.getUserID());
        picToDelete.setTripID(pictureCreated.getTripID());
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertNotNull("Picture Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Delete: Result should have been SUCCESS! \n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertEquals("Picture Delete: Invalid number Pictures deleted!", 1, numDeleted);
        }

        //Update:
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(pictureCreated.getUserID());
        picToUpdate.setTripID(pictureCreated.getTripID());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote("Illegal update!");
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertFalse("Picture Update: Result should have been FAILURE!", result.isSuccess());
            Assert.assertNotNull("Picture Update: Returned response should have a ServiceError!", result.getServiceError());
            Assert.assertEquals("Picture Update: Unexpected error code! Error text: " + result.getServiceError().getErrorText(), 
                    ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR, result.getServiceError().getErrorEnum());
            
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
            Assert.assertNull("Picture Update after Delete: Returned picture should be EMPTY!", picUpdated);
        }
    }    
    
    @Test
    public void testDelete_ByTripID() throws Exception {
        try {
            //Existing:
            Picture pictureCreated_1 = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, null, false);
            Picture pictureCreated_2 = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, null, false);

            //Delete:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setTripID(mTripCreated.getId());
            if(isWSClientTest) {
                ServiceResponse result = PictureWSClient.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
                Assert.assertNotNull("Picture Delete: Returned result can NOT be null!", result);
                Assert.assertTrue("Picture Delete: Result should have been SUCCESS! \n" + result, result.isSuccess());
            } else if(isManagerTest) {
                int numDeleted = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
                Assert.assertEquals("Picture Delete: Invalid number Pictures deleted!", 2, numDeleted);
            }
            
            //Verify physical image is REMOVED:
            Assert.assertFalse("Picture Delete: Picture image [" + pictureCreated_1.getImageName() +"] should have been deleted!", 
                    TestUtilForPicture.imageRead_byFSClient(pictureCreated_1.getImageName(), true));
            Assert.assertFalse("Picture Delete: Picture image [" + pictureCreated_2.getImageName() +"] should have been deleted!", 
                    TestUtilForPicture.imageRead_byFSClient(pictureCreated_2.getImageName(), true));
        } finally {
            //Cleanup:
            TestUtilForPicture.pictureDelete_byManager_ALLPictures(mUserCreated.getUserId());
        }
    }    
    
    @Test
    public void testDelete_ByTripID_ThenRetrieve() throws Exception {
        //Existing (actual images are not necessary):
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());
        TestUtilForPicture.pictureCreate_byDB_DefaultData(mUserCreated.getUserId(), mTripCreated.getId());

        //Delete:
        Picture picToDelete = new Picture();
        picToDelete.setUserID(mUserCreated.getUserId());
        picToDelete.setTripID(mTripCreated.getId());
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            Assert.assertNotNull("Picture Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Delete: Result should have been SUCCESS! \n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            Assert.assertEquals("Picture Delete: Invalid number Pictures deleted!", 2, numDeleted);
        }

        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setTripID(mTripCreated.getId());
        List<Picture> picListFound = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, 0, -1); 
            Assert.assertNotNull("Picture Retrieve: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Retrieve: Result should have been SUCCESS! \n" + result, result.isSuccess());
            picListFound = result.getListOfPictures();
        } else if(isManagerTest) {
            picListFound = PictureManager.retrieve(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, 0, -1);
        }
        
        if(picListFound != null) {
            Assert.assertEquals("Picture Retrieve after Delete: Result should have EMPTY list of Pictures!", 0, picListFound.size());

        }
    }        
    
    /**
     * Tests Picture create, update and delete for Trip UPDATED date triggers.
     * This test is very time-sensitive since it compares timestamps between Picture actions.
     * If test client machine vs. server database do not have sync-up time, it could cause test
     * to fail.
     */
    @Test
    public void testTripUpdatedTriggers() throws Exception {
        Date tripUpdatedDateOrig = DateUtil.formatDate(mTripCreated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);

        //Create:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        if(isWSClientTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byWSClient(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, TestConstants.UNITTEST_PICTURE_NOTE, false);
                    
        } else if(isManagerTest) {
            mPictureCreatedWithImage = TestUtilForPicture.pictureCreate_byManager(
                    mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                    null, null, TestConstants.UNITTEST_PICTURE_NOTE, false);
        }
        Date picCreatedDate = DateUtil.formatDate(mPictureCreatedWithImage.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);

        //Verify - Compare Trip UPDATED date:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(mTripCreated.getId());
        List<Trip> tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Date tripUpdatedDateAfterPictureCreated = DateUtil.formatDate(tripListFound.get(0).getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Picture create: Trigger FAILURE - Trip UPDATED should have been changed!", 
                tripUpdatedDateAfterPictureCreated.after(tripUpdatedDateOrig));
        Assert.assertTrue("Picture create: Trigger FAILURE - Trip UPDATED should be the same as Picture CREATED!", 
                tripUpdatedDateAfterPictureCreated.equals(picCreatedDate));
        
        //Update:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Picture picToUpdate = new Picture();
        picToUpdate.setId(mPictureCreatedWithImage.getId());
        picToUpdate.setUserID(mPictureCreatedWithImage.getUserID());
        picToUpdate.setTripID(mPictureCreatedWithImage.getTripID());
        picToUpdate.setNote("Hello!");
        Picture picUpdated = null;
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.update(picToUpdate);
            Assert.assertNotNull("Picture Update: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Update: Result should have been SUCCESS! \n" + result, result.isSuccess());
            if(result.getListOfPictures() != null) {
                picUpdated = result.getListOfPictures().get(0);
            }
        } else if(isManagerTest) {
            picUpdated = PictureManager.update(picToUpdate);
        }
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Date picUpdatedDate = DateUtil.formatDate(picUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        
        //Verify - Compare Trip UPDATED date:
        tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Date tripUpdatedDateAfterPictureUpdated = DateUtil.formatDate(tripListFound.get(0).getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Picture update: Trigger FAILURE - Trip UPDATED should have been changed!", 
                tripUpdatedDateAfterPictureUpdated.after(tripUpdatedDateAfterPictureCreated));
        Assert.assertTrue("Picture update: Trigger FAILURE - Trip UPDATED should be the same as Picture UPDATED!", 
                tripUpdatedDateAfterPictureUpdated.equals(picUpdatedDate));

        //Delete:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Picture picToDelete = new Picture();
        picToDelete.setId(mPictureCreatedWithImage.getId());
        picToDelete.setUserID(mPictureCreatedWithImage.getUserID());
        picToDelete.setTripID(mPictureCreatedWithImage.getTripID());
        if(isWSClientTest) {
            ServiceResponse result = PictureWSClient.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertNotNull("Picture Delete: Returned result can NOT be null!", result);
            Assert.assertTrue("Picture Delete: Result should have been SUCCESS! \n" + result, result.isSuccess());
        } else if(isManagerTest) {
            int numDeleted = PictureManager.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertEquals("Picture Delete: Invalid number Pictures deleted!", 1, numDeleted);
        }

        //Verify - Compare Trip UPDATED date:
        tripListFound = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Date tripUpdatedDateAfterPictureDeleted = DateUtil.formatDate(tripListFound.get(0).getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Picture Delete: Trigger FAILURE - Trip UPDATED should have been changed! Previous date [" 
                + tripUpdatedDateAfterPictureUpdated + "] vs. After delete date [" + tripUpdatedDateAfterPictureDeleted + "]", 
                tripUpdatedDateAfterPictureDeleted.after(tripUpdatedDateAfterPictureUpdated));
    }
    
    @Test
    public void testPictureImage_UploadAndDownload() throws Exception {
        String imageFilename = CommonUtil.generatePictureFilename(mClassName + "_testPictureImage_UploadAndDownload");
        try {
            //Upload:
            if(isWSClientTest) {
                Picture pictureWithImageToUpload = new Picture();
                pictureWithImageToUpload.setImageName(imageFilename);
                pictureWithImageToUpload.setImageBytes(mImageJPGSmallBytes);
                ServiceResponse svcResponseUpload = PictureWSClient.imageUpload(pictureWithImageToUpload);
                Assert.assertTrue("Picture Image Upload: The server response should have been a SUCCESS! \n" + svcResponseUpload, svcResponseUpload.isSuccess());

            } else if(isManagerTest) {
                FileServerResponse fsResponseUpload = TestUtilForPicture.imageUpload_byManager(
                        mUserCreated.getUserId(), imageFilename, mImageJPGSmallBytes, true);
                Assert.assertTrue("Picture Image Upload: The FileServer response should have been a SUCCESS! \n" + fsResponseUpload, fsResponseUpload.isSuccess());
            }

            //Download:
            if(isWSClientTest) {
                Picture pictureWithImageToDownload = new Picture();
                pictureWithImageToDownload.setImageName(imageFilename);
                ServiceResponse svcResponseDownload = PictureWSClient.imageDownload(pictureWithImageToDownload);
                Assert.assertTrue("Picture Image Download: The server response should have been a SUCCESS! \n" + svcResponseDownload, svcResponseDownload.isSuccess());
                Assert.assertNotNull("Picture Image Download: The image bytes should have NOT NULL!", svcResponseDownload.getListOfPictures());
                Assert.assertNotNull("Picture Image Download: The image bytes should have NOT NULL!", svcResponseDownload.getListOfPictures().get(0));
                Assert.assertNotNull("Picture Image Download: The image bytes should have NOT NULL!", svcResponseDownload.getListOfPictures().get(0).getImageBytes());

            } else if(isManagerTest) {
                FileServerResponse fsResponseDownload = TestUtilForPicture.imageDownload_byManager(imageFilename, true);
                Assert.assertTrue("Picture Image Download: The FileServer response should have been a SUCCESS! \n" + fsResponseDownload, fsResponseDownload.isSuccess());
                Assert.assertNotNull("Picture Image Download: The image bytes should have NOT NULL!", fsResponseDownload.getFileBytes());
            }
            
        } finally {
            //Clean-up:
            try {
                TestUtilForPicture.imageDelete_byFSClient(imageFilename, true);
            } catch(Exception e) {
                //do nothing
            }
        }
    }    
    
}
