package test.com.trekcrumb.perftestsingle;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;

/**
 * Test class to support performance tests.
 *
 * This contains single test cases that will be used by each corresponding performance tests 
 * in various ways (concurrent, sequential, high volumes, time sensitive, etc.). The purpose of this
 * being part of regular test package is that we can run each single test cases regularly as part 
 * of daily unit tests, separate from performance tests run. 
 * 
 * Note that its test cases may look identical with some regular unit tests. But it differs in a
 * way to meet requirements as described below:
 * 1. Must extends the older JUnit framework TestCase in order to use JUnitPerf framework 
 *    where this test class MUST expend TestCase
 * 2. Must implement constructor with the name of testCase to run
 * 3. Must NOT use any JUnit 4 tags (not applicable)
 * 4. Implement a simple static{} block to mirror one-time setup/teardown operations, when required
 * 5. The actual performance tests are inside package: testperf.com.trekcrumb
 *    
 * @author Val Triadi
 * @since 10/2014 
 */
public class TripManagerPerfTestSingle extends TestCase {
    private static final String CLASS_NAME = "TripManagerPerfTestSingle";
    private static int mNumOfTripsCreated;
    private static User mUserCreated;
    
    public TripManagerPerfTestSingle() {
        super();
    }
    
    /**
     * Constructor to enable running a specific test case.
     * @param testCase - The name of test method (case) to run.
     */
    public TripManagerPerfTestSingle(String testCase) {
        super(testCase);
    }
    
    static {
        //Equivalent ops for oneTimeSetUp():
        if(mUserCreated == null) {
            String username = String.valueOf(System.currentTimeMillis());
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME;
            mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        }
        
        //Create sample trips for search:
        try {
            mNumOfTripsCreated = TestUtilForTrip.tripCreate_byDB_ManyTrips(mUserCreated.getUserId());
        } catch(Exception e) {
            Assert.fail("FAILED while trying to setup data: Trips create. Error: " + e.getMessage());
        }

        //Equivalent ops for oneTimeTeardown():
        Runtime.getRuntime().addShutdownHook(new TestOneTimeTeardownThread(mUserCreated));
    }    
    
    public void testCreateRetrieveAndDelete_ByTripID() throws Exception {
        //Create:
        Trip tripCreated =  TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), CLASS_NAME,TestConstants.UNITTEST_TRIP_NOTE, 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, true);
        String tripID = tripCreated.getId();
        
        //Retrieve by ID:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUserID(mUserCreated.getUserId());
        tripToRetrieve.setId(tripID);
        List<Trip> tripRetrievedList = 
                TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripRetrievedList);
        Assert.assertEquals("Incorrect number of Trips returned!", 1, tripRetrievedList.size());
        Trip tripRetrieved = tripRetrievedList.get(0);
        Assert.assertNotNull("Returned Trip should NOT be null!", tripRetrieved);
        Assert.assertEquals("Incorrect Trip ID!", tripID, tripRetrieved.getId());
        
        //Delete by ID:
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        tripToDelete.setId(tripID);
        int numDeleted = TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        Assert.assertEquals("Trip Delete: Incorrect number of deleted Trip!", 1, numDeleted);
    }
    
    public void testCreate_WithPlace() throws Exception {
        Place placeToCreate = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
        placeToCreate.setNote(TestConstants.UNITTEST_PLACE_NOTE);
        List<Place> listOfPlaces = new ArrayList<Place>();
        listOfPlaces.add(placeToCreate);
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), CLASS_NAME, 
                TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripPrivacyEnum.PRIVATE, listOfPlaces, false); 

        //Verify Place data:
        Assert.assertNotNull("Trip Create: Returned created Trip should have a Place!", tripCreated.getListOfPlaces());
        Assert.assertTrue("Trip Create: Returned created Trip should have a Place!", tripCreated.getListOfPlaces().size() > 0);
        Assert.assertNotNull("Trip Create: Returned created Trip should have a Place!", tripCreated.getListOfPlaces().get(0));
    }
    
    public void testRetrieve_ByUser() throws Exception {
        Trip tripToRetrieveByUser = new Trip();
        tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
        List<Trip> tripRetrievedList = 
                TripManager.retrieve(tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
        Assert.assertNotNull("Trip Retrieve by User: Returned list of trips should NOT be null!", tripRetrievedList);
        Assert.assertTrue("Trip Retrieve by User: Invalid number of found trips: " + tripRetrievedList.size(), 
                tripRetrievedList.size() >= mNumOfTripsCreated);
    }
    
    public void testUpdate_ALL() throws Exception {
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);

        //Update:
        String tripNameUpdated = CLASS_NAME + "-UPDATED";
        String tripNoteUpdated = TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED";
        String tripLocUpdated = TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED";
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(tripNameUpdated);
        tripToUpdate.setNote(tripNoteUpdated);
        tripToUpdate.setLocation(tripLocUpdated);
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Trip tripUpdated = TripManager.update(tripToUpdate);
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertEquals("Trip Update: Invalid name!", tripNameUpdated, tripUpdated.getName());
        Assert.assertEquals("Trip Update: Invalid note!", tripNoteUpdated, tripUpdated.getNote());
        Assert.assertEquals("Trip Update: Invalid location!", tripLocUpdated, tripUpdated.getLocation());
    }

    public void testPublish_WithPlaceAndPicture() throws Exception {
        //Prepare data:
        String tripID = CommonUtil.generateID(mUserCreated.getUserId());
        Trip tripToPublish = new Trip();
        tripToPublish.setId(tripID);
        tripToPublish.setUserID(mUserCreated.getUserId());
        tripToPublish.setName(CLASS_NAME + "_testPublish");
        tripToPublish.setNote(TestConstants.UNITTEST_TRIP_NOTE);
        tripToPublish.setLocation( TestConstants.UNITTEST_TRIP_LOCATION);
        tripToPublish.setStatus(TripStatusEnum.ACTIVE);
        tripToPublish.setPrivacy(TripPrivacyEnum.PUBLIC);
        tripToPublish.setPublish(TripPublishEnum.PUBLISH);
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        tripToPublish.setCreated(createdDate);
        tripToPublish.setUpdated(createdDate);
 
        Place placeToPublish = new Place(TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE);
        placeToPublish.setId(CommonUtil.generateID(tripToPublish.getId()));
        placeToPublish.setTripID(tripID);
        placeToPublish.setUserID(mUserCreated.getUserId());
        placeToPublish.setNote(TestConstants.UNITTEST_PLACE_NOTE);
        placeToPublish.setCreated(createdDate);
        placeToPublish.setUpdated(createdDate);
        List<Place> listOfPlaces = new ArrayList<Place>();
        listOfPlaces.add(placeToPublish);
        tripToPublish.setListOfPlaces(listOfPlaces);
        
        Picture picToPublish = new Picture();
        picToPublish.setId(CommonUtil.generateID(tripToPublish.getId()));
        picToPublish.setTripID(tripID);
        picToPublish.setUserID(mUserCreated.getUserId());
        picToPublish.setImageName(CLASS_NAME);
        picToPublish.setCreated(createdDate);
        picToPublish.setUpdated(createdDate);
        List<Picture> listOfPictures = new ArrayList<Picture>();
        listOfPictures.add(picToPublish);
        tripToPublish.setListOfPictures(listOfPictures);
        
        //Publish:
        Trip tripPublished = TripManager.publish(tripToPublish);
        Assert.assertNotNull("Trip Published: Returned Trip should NOT be null!", tripPublished);
        Assert.assertNotNull("Trip Published: Retrieved Trip should have a Place!", tripPublished.getListOfPlaces());
        Assert.assertNotNull("Trip Published: Retrieved Trip should have a Place!", tripPublished.getListOfPlaces().get(0));
        Assert.assertNotNull("Trip Published: Retrieved Trip should have a Picture!", tripPublished.getListOfPictures());
        Assert.assertNotNull("Trip Published: Retrieved Trip should have a Picture!", tripPublished.getListOfPictures().get(0));
    }    

    public void testSearchAndCount_Default() throws Exception {
        //Search:
        TripSearchCriteria searchCriteria = new TripSearchCriteria();
        List<Trip> tripsFoundList = TripManager.search(searchCriteria);
        Assert.assertNotNull("Search default: List of trips should NOT be null!", tripsFoundList);
        Assert.assertEquals("Search default: Incorrect number of trips found!", 
                CommonConstants.RECORDS_NUMBER_MAX, tripsFoundList.size());
        
        //Count:
        int numOfTripsCounted = TripManager.count(searchCriteria);
        Assert.assertTrue("Count default: Incorrect number of trips found! Expected [" 
                + mNumOfTripsCreated + "] but actual [" + numOfTripsCounted + "]", 
                numOfTripsCounted >= mNumOfTripsCreated);
    }
 
}        

