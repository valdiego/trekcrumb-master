package test.com.trekcrumb.perftestsingle;

import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;

import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Test class to support performance tests.
 *
 * This contains single test cases that will be used by each corresponding performance tests 
 * in various ways (concurrent, sequential, high volumes, time sensitive, etc.). The purpose of this
 * being part of regular test package is that we can run each single test cases regularly as part 
 * of daily unit tests, separate from performance tests run. 
 * 
 * Note that its test cases may look identical with some regular unit tests. But it differs in a
 * way to meet requirements as described below:
 * 1. Must extends the older JUnit framework TestCase in order to use JUnitPerf framework 
 *    where this test class MUST expend TestCase
 * 2. Must implement constructor with the name of testCase to run
 * 3. Must NOT use any JUnit 4 tags (not applicable)
 * 4. Implement a simple static{} block to mirror one-time setup/teardown operations, when required
 * 5. The actual performance tests are inside package: testperf.com.trekcrumb
 *    
 * @author Val Triadi
 * @since 10/2014 
 */
public class UserAuthTokenManagerPerfTestSingle extends TestCase {
    private static final String CLASS_NAME = "UserAuthTokenManagerPerfTestSingle";
    private static User mUserCreated;
    
    public UserAuthTokenManagerPerfTestSingle() {
        super();
    }
    
    /**
     * Constructor to enable running a specific test case.
     * @param testCase - The name of test method (case) to run.
     */
    public UserAuthTokenManagerPerfTestSingle(String testCase) {
        super(testCase);
    }
    
    static {
        //Equivalent ops for oneTimeSetUp():
        if(mUserCreated == null) {
            String username = String.valueOf(System.currentTimeMillis());
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME;
            mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        }
        
        //Equivalent ops for oneTimeTeardown():
        Runtime.getRuntime().addShutdownHook(new TestOneTimeTeardownThread(mUserCreated));
    }    
    
    public void testCreateRetrieveAndDelete() throws Exception {
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        UserAuthToken authTokenCreated = 
                TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, true);
        
        //Retrieve (by device):
        UserAuthToken authTokenRetrieve = new UserAuthToken();
        authTokenRetrieve.setUserId(mUserCreated.getUserId());
        authTokenRetrieve.setDeviceIdentity(deviceIdentity);
        List<UserAuthToken> recordList = 
                UserAuthTokenManager.retrieve(authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
        Assert.assertNotNull("AuthToken Retrieve: Returned record list shall NOT be null!", recordList);
        Assert.assertEquals("AuthToken Retrieve: Invalid number of returned records!", 1, recordList.size());

        //Delete (by ID):
        UserAuthToken authTokenToDelete = new UserAuthToken();
        authTokenToDelete.setId(authTokenCreated.getId());
        int numDeleted = 
                UserAuthTokenManager.delete(authTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
        Assert.assertEquals("AuthToken Delete: Invalid number of deleted record!", 1, numDeleted);    
    }

    public void testAuthenticate() throws Exception {
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        UserAuthToken authTokenCreated = 
                TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, false);

        //Authenticate:
        UserAuthToken authTokenToAuthenticate = new UserAuthToken();
        authTokenToAuthenticate.setUserId(mUserCreated.getUserId());
        authTokenToAuthenticate.setAuthToken(authTokenCreated.getAuthToken());
        User userAuthenticated = UserAuthTokenManager.authenticate(authTokenToAuthenticate);
        Assert.assertNotNull("Authenticate: Should be SUCCESS and returned a not-null User bean!", userAuthenticated);
    }
 
}        


