package test.com.trekcrumb.perftestsingle;

import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.ITripDAO;
import com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImpl;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;

/**
 * Test class to support performance tests.
 *
 * This contains single test cases that will be used by each corresponding performance tests 
 * in various ways (concurrent, sequential, high volumes, time sensitive, etc.). The purpose of this
 * being part of regular test package is that we can run each single test cases regularly as part 
 * of daily unit tests, separate from performance tests run. 
 * 
 * Note that its test cases may look identical with some regular unit tests. But it differs in a
 * way to meet requirements as described below:
 * 1. Must extends the older JUnit framework TestCase in order to use JUnitPerf framework 
 *    where this test class MUST expend TestCase
 * 2. Must implement constructor with the name of testCase to run
 * 3. Must NOT use any JUnit 4 tags (not applicable)
 * 4. Implement a simple static{} block to mirror one-time setup/teardown operations, when required
 * 5. The actual performance tests are inside package: testperf.com.trekcrumb
 *    
 * @author Val Triadi
 * @since 10/2014 
 */
public class TripDAOHibernateImplPerfTestSingle extends TestCase {
    private static final String CLASS_NAME = "TripDAOHibernateImplPerfTestSingle";
    private static User mUserCreated;

    public TripDAOHibernateImplPerfTestSingle() {
        super();
    }

    /**
     * Constructor to enable running a specific test case.
     * @param testCase - The name of test method (case) to run.
     */
    public TripDAOHibernateImplPerfTestSingle(String testCase) {
        super(testCase);
    }

    static {
        //Equivalent ops for oneTimeSetUp():
        if(mUserCreated == null) {
            String username = String.valueOf(System.currentTimeMillis());
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME;
            mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        }

        //Create sample trips for search:

        //Equivalent ops for oneTimeTeardown():
        Runtime.getRuntime().addShutdownHook(new TestOneTimeTeardownThread(mUserCreated));
    }    

    /**
     * Data clean-up called after each test case. 
     */
    public void tearDown() {
        //None
        //TestUtilForTrip.tripDeleteALLByTripDB(mUserCreated.getUserId());
    }

    public void testCreateReadAndDelete_ByTripID() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        //Create:
        Trip tripCreated = TestUtilForTrip.tripCreate_byDAOImpl(
                mUserCreated.getUserId(), CLASS_NAME, 
                TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, null,
                false); 

        //Read
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUserID(mUserCreated.getUserId());
        tripToRetrieve.setId(tripCreated.getId());
        List<Trip> tripListFound = 
                tripDAOImpl.read(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Incorrect number of Trips returned!", 1, tripListFound.size());

        //Delete:
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        tripToDelete.setId(tripCreated.getId());
        int numDeleted = tripDAOImpl.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        Assert.assertEquals("Incorrect number of deleted Trip!", 1, numDeleted);
    }

    public void testUpdate_ALL() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        //Create:
        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);

        //Update:
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(CLASS_NAME + "-UPDATED");
        tripToUpdate.setNote(TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED");
        tripToUpdate.setLocation(TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED");
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Trip tripUpdated = tripDAOImpl.update(tripToUpdate);
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
    }

    public void testPublish() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        String tripID = CommonUtil.generateID(mUserCreated.getUserId());
        Trip tripToPublish = new Trip();
        tripToPublish.setId(tripID);
        tripToPublish.setUserID(mUserCreated.getUserId());
        tripToPublish.setName(CLASS_NAME + "_testPublish");
        tripToPublish.setNote(TestConstants.UNITTEST_TRIP_NOTE);
        tripToPublish.setLocation( TestConstants.UNITTEST_TRIP_LOCATION);
        tripToPublish.setStatus(TripStatusEnum.ACTIVE);
        tripToPublish.setPrivacy(TripPrivacyEnum.PUBLIC);
        tripToPublish.setPublish(TripPublishEnum.PUBLISH);
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        tripToPublish.setCreated(createdDate);
        tripToPublish.setUpdated(createdDate);

        Trip tripPublished = tripDAOImpl.publish(tripToPublish);
        Assert.assertNotNull("Trip Published: Returned Trip should NOT be null!", tripPublished);
    }
}    

