package test.com.trekcrumb.perftestsingle;

import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.After;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Test class to support performance tests.
 *
 * This contains single test cases that will be used by each corresponding performance tests 
 * in various ways (concurrent, sequential, high volumes, time sensitive, etc.). The purpose of this
 * being part of regular test package is that we can run each single test cases regularly as part 
 * of daily unit tests, separate from performance tests run. 
 * 
 * Note that its test cases may look identical with some regular unit tests. But it differs in a
 * way to meet requirements as described below:
 * 1. Must extends the older JUnit framework TestCase in order to use JUnitPerf framework 
 *    where this test class MUST expend TestCase
 * 2. Must implement constructor with the name of testCase to run
 * 3. Must NOT use any JUnit 4 tags (not applicable)
 * 4. Implement a simple static{} block to mirror one-time setup/teardown operations, when required
 * 5. The actual performance tests are inside package: testperf.com.trekcrumb
 *    
 * @author Val Triadi
 * @since 10/2014 
 */
public class UserManagerPerfTestSingle extends TestCase {
    private static final String CLASS_NAME = "UserManagerPerfTestSingle";
    
    public UserManagerPerfTestSingle() {
        super();
    }
    
    /**
     * Constructor to enable running a specific test case.
     * @param testCase - The name of test method (case) to run.
     */
    public UserManagerPerfTestSingle(String testCase) {
        super(testCase);
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        //None
    }
    
    public void testUserCreateAndConfirmAndLogin() throws Exception {
        /*
         * userCreated should be local and unique for each test case run, and should NOT be the test class
         * variable which can cause concurrency issues.
         */
        User userCreated = null;
        try {
            String username = CommonUtil.generateID(null);
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME + "-testCreateAndLogin";
            userCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, true);

            //Confirm:
            User userToConfirm = new User();
            userToConfirm.setUsername(username);
            userToConfirm.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            boolean confirmResult = UserManager.userConfirm(userToConfirm);
            Assert.assertTrue("User Confirm: New user confirm should have been SUCCESS!", confirmResult);

            //Login:
            User userToLogin = new User();
            userToLogin.setUsername(username);
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD_CRYPTED);
            User userLoggedIn = UserManager.userLogin(userToLogin);
            Assert.assertNotNull("User Login: Returned user should NOT be null!", userLoggedIn);
            Assert.assertEquals("User Login: Invalid returned User ID!", userCreated.getUserId(), userLoggedIn.getUserId());
        } finally {
            if(userCreated != null) {
                TestUtilForUser.userDelete_byDB(userCreated);
            }
        }
    }
    
    public void testUserRetrieve_ByUserID() throws Exception {
        User userCreated = null;
        try {
            String username = CommonUtil.generateID(null);
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME + "-testUserRetrieve_ByUserID";
            userCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
            
            User userToRetrieve = new User();
            userToRetrieve.setUserId(userCreated.getUserId());
            List<User> listOfUsersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, false, 0, -1);
            Assert.assertNotNull("User Retrieve: Manager should have FOUND the user!", listOfUsersFound);
            Assert.assertEquals("User Retrieve: Manager should have FOUND the user!", 1, listOfUsersFound.size());
            Assert.assertNotNull("User Retrieve: Manager should have FOUND the user!", listOfUsersFound.get(0));
            User userFound = listOfUsersFound.get(0);
            Assert.assertEquals("User Retrieve: Invalid retrieved User ID!!", userCreated.getUserId(), userFound.getUserId());
        } finally {
            if(userCreated != null) {
                TestUtilForUser.userDelete_byDB(userCreated);
            }
        }
    }
}
