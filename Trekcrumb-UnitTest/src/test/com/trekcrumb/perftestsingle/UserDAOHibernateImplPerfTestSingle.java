package test.com.trekcrumb.perftestsingle;

import java.util.List;

import junit.framework.Assert;
import junit.framework.TestCase;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.UserDAOHibernateImpl;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;

/**
 * Test class to support performance tests.
 *
 * This contains single test cases that will be used by each corresponding performance tests 
 * in various ways (concurrent, sequential, high volumes, time sensitive, etc.). The purpose of this
 * being part of regular test package is that we can run each single test cases regularly as part 
 * of daily unit tests, separate from performance tests run. 
 * 
 * Note that its test cases may look identical with some regular unit tests. But it differs in a
 * way to meet requirements as described below:
 * 1. Must extends the older JUnit framework TestCase in order to use JUnitPerf framework 
 *    where this test class MUST expend TestCase
 * 2. Must implement constructor with the name of testCase to run
 * 3. Must NOT use any JUnit 4 tags (not applicable)
 * 4. Implement a simple static{} block to mirror one-time setup/teardown operations, when required
 * 5. The actual performance tests are inside package: testperf.com.trekcrumb
 *    
 * @author Val Triadi
 * @since 10/2014 
 */public class UserDAOHibernateImplPerfTestSingle extends TestCase {
    private static final String CLASS_NAME = "UserDAOHibernateImplPerfTestSingle";

    public UserDAOHibernateImplPerfTestSingle() {
        super();
    }

    /**
     * Constructor to enable running a specific test case.
     * @param testCase - The name of test method (case) to run.
     */
    public UserDAOHibernateImplPerfTestSingle(String testCase) {
        super(testCase);
    }

    public void testCreateAndRead() {
        UserDAOHibernateImpl daoUserImpl = new UserDAOHibernateImpl();

        User userCreated = null;
        try {
            //Create:
            String username = String.valueOf(System.currentTimeMillis());
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME + "-testCreateAndRetrieveAndDeleteUser";
            userCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, false);
            Assert.assertNotNull("User Create: Returned user entity should NOT be null!", userCreated);

            //Retrieve: 
            User userToFindByUsername = new User();
            userToFindByUsername.setUsername(username);
            List<User> foundUsers = 
                    daoUserImpl.read(userToFindByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, false, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned user should have NOT be null!", foundUsers);
            User userRead = foundUsers.get(0);
            Assert.assertNotNull("User Retrieve: Returned user should have NOT be null!", userRead);
            Assert.assertEquals("User Retrieve: Invalid user ID!", userCreated.getUserId(), userRead.getUserId());
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Test FAILED with exception: " + e.getMessage());
        } finally {
            if(userCreated != null) {
                try {
                    TestUtilForUser.userDelete_byDB(userCreated); 
                } catch(Exception e) {
                    //No need action
                }
            }
        }
    }
}

    
