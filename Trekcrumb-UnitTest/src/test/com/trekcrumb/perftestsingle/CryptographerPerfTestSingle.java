package test.com.trekcrumb.perftestsingle;

import junit.framework.Assert;
import junit.framework.TestCase;
import test.com.trekcrumb.utility.TestConstants;

import com.trekcrumb.common.crypto.Cryptographer;

/**
 * Test class to support performance tests.
 *
 * This contains single test cases that will be used by each corresponding performance tests 
 * in various ways (concurrent, sequential, high volumes, time sensitive, etc.). The purpose of this
 * being part of regular test package is that we can run each single test cases regularly as part 
 * of daily unit tests, separate from performance tests run. 
 * 
 * Note that its test cases may look identical with some regular unit tests. But it differs in a
 * way to meet requirements as described below:
 * 1. Must extends the older JUnit framework TestCase in order to use JUnitPerf framework 
 *    where this test class MUST expend TestCase
 * 2. Must implement constructor with the name of testCase to run
 * 3. Must NOT use any JUnit 4 tags (not applicable)
 * 4. Implement a simple static{} block to mirror one-time setup/teardown operations, when required
 * 5. The actual performance tests are inside package: testperf.com.trekcrumb
 *    
 * @author Val Triadi
 * @since 10/2014 
 */
public class CryptographerPerfTestSingle extends TestCase {
    public CryptographerPerfTestSingle() {
        super();
    }
    
    /**
     * Constructor to enable running a specific test case.
     * @param testCase - The name of test method (case) to run.
     */
    public CryptographerPerfTestSingle(String testCase) {
        super(testCase);
    }
    
    public void testEncrypt() {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        String nonCryptedPwd = TestConstants.UNITTEST_USER_PASSWORD;
        String cryptedPwd = null;
        try {
            cryptedPwd = cryptoInstance.encrypt(nonCryptedPwd);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to encrypt password: " + e.getMessage());
        }
        Assert.assertFalse("Original password and encrypted one should have NOT the same! Password: " 
                + nonCryptedPwd + " vs. encryptedPwd: " + cryptedPwd, 
                nonCryptedPwd.equals(cryptedPwd));
    }
   
    public void testDecrypt() {
       Cryptographer cryptoInstance = null;
       try {
           cryptoInstance = Cryptographer.getInstance();
       } catch(Exception e) {
           e.printStackTrace();
           Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
       }
       
       String cryptedPwd = TestConstants.UNITTEST_USER_PASSWORD_CRYPTED;
       String decryptedPwd = null;
       try {
           decryptedPwd = cryptoInstance.decrypt(cryptedPwd);
       } catch(Exception e) {
           e.printStackTrace();
           Assert.fail("Fail when trying to decrypt password: " + e.getMessage());
       }
       Assert.assertEquals("Original password and decrypted password should be the SAME!", 
               TestConstants.UNITTEST_USER_PASSWORD, decryptedPwd);
    }
   
    public void testSignAndVerifySign() throws Exception {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        String originalMsg = "This is my request. Process it quick!";
        String signature = null;
        try {
            signature = cryptoInstance.sign(originalMsg);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to digital sign: " + e.getMessage());
        }
        Assert.assertNotNull("Output signature should NOT NULL!", signature);
        
        try {
            boolean isVerified = cryptoInstance.verifySign(originalMsg, signature);
            Assert.assertTrue("Signature should have been successfully verified!", isVerified);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to verify signature: " + e.getMessage());
        }
    }

    public void testSaltAndHashThenValidate() throws Exception {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        String pwdOrig = TestConstants.UNITTEST_USER_PASSWORD;
        String pwdSaltedAndHashed = null;
        try {
            pwdSaltedAndHashed = cryptoInstance.saltAndHash(pwdOrig);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to salt-n-hash password: " + e.getMessage());
        }
        
        Assert.assertFalse("Original password and salted-n-hashed one should have NOT the same! Password: " 
                + pwdOrig + " vs. salted-n-hashed: " + pwdSaltedAndHashed, 
                pwdOrig.equals(pwdSaltedAndHashed));
        
        Assert.assertTrue("Validation of the password [" 
                + pwdOrig + "] against its salted-n-hashed value [" 
                + pwdSaltedAndHashed + "] should have been SUCCESS!", 
                cryptoInstance.verifySaltAndHash(pwdOrig, pwdSaltedAndHashed));
    }
}    

