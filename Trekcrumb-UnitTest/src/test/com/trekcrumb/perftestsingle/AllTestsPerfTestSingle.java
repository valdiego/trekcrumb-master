package test.com.trekcrumb.perftestsingle;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.perftestsingle.CryptographerPerfTestSingle.class,
    
    test.com.trekcrumb.perftestsingle.TripDAOHibernateImplPerfTestSingle.class,
    test.com.trekcrumb.perftestsingle.UserDAOHibernateImplPerfTestSingle.class,
    
    test.com.trekcrumb.perftestsingle.TripManagerPerfTestSingle.class,
    test.com.trekcrumb.perftestsingle.UserAuthTokenManagerPerfTestSingle.class,
    test.com.trekcrumb.perftestsingle.UserManagerPerfTestSingle.class,
    
})
public class AllTestsPerfTestSingle {}
