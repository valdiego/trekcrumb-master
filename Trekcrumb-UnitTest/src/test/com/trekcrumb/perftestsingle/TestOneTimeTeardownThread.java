package test.com.trekcrumb.perftestsingle;

import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.common.bean.User;

/**
 * A worker class - to shutdown test by cleaning up necessary data and/or resources.
 * 
 * @author Val Triadi
 * @since 10/2014
 *
 */
public class TestOneTimeTeardownThread extends Thread {
    private User mUser;
    
    public TestOneTimeTeardownThread(final User user){
        mUser = user;
    }

    @Override
    public void run() {
        System.out.println("TestOneTimeTeardownThread.run() - Performing one time teardown to clean test data ...");
        TestUtilForUser.userDelete_byDB_ALLData(mUser);
        mUser = null;
        System.out.println("TestOneTimeTeardownThread.run() - Completed.");
    }
}
