package test.com.trekcrumb.business.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.UserDAOHibernateImpl;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;

/**
 * DAO Component's tests for entity: User.
 * 
 * This test cover only BASIC use-cases to DAO CRUD functionality. For more comprehensive tests
 * regarding the entity, see the Business (Manager) test classes.
 */
public class UserDAOHibernateImplTest {
    private static final String CLASS_NAME = "UserDAOHibernateImplTest";
    
    private User mUserCreated;
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForUser.userDelete_byDB(mUserCreated);
        mUserCreated = null;
    }
    
    @Test
    public void testCreateAndRead() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testCreateAndRead";
        mUserCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, true);

        //Retrieve by username:
        UserDAOHibernateImpl daoUserImpl = new UserDAOHibernateImpl();
        User userToFindByUsername = new User();
        userToFindByUsername.setUsername(username);
        List<User> foundUsers = 
                daoUserImpl.read(userToFindByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, false, 0, 1);
        Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, foundUsers);
        User userRead = foundUsers.get(0);
        Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userRead);
        Assert.assertNotNull("User ID should have not been NULL!", userRead.getUserId());
        Assert.assertEquals("Incorrect user email address!", email, userRead.getEmail());
        Assert.assertEquals("Incorrect password!", TestConstants.UNITTEST_USER_PASSWORD, userRead.getPassword());
        Assert.assertEquals("Incorrect fullname!", fullname, userRead.getFullname());
        Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, userRead.getActive());
        Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, userRead.getConfirmed());
    }
    
    @Test
    public void testRead_ByUserID() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testRead_ByUserID";
        mUserCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, false);
        String userID = mUserCreated.getUserId();

        //Retrieve by UserId:
        UserDAOHibernateImpl daoUserImpl = new UserDAOHibernateImpl();
        User userToFindById = new User();
        userToFindById.setUserId(userID);
        List<User> foundUsers = 
                daoUserImpl.read(userToFindById, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, false, false, 0, 1);
        Assert.assertNotNull("DB should have NOT returned NULL for user with userid: " + userID, foundUsers);
        User userRead = foundUsers.get(0);
        Assert.assertNotNull("DB should have NOT returned NULL for user with userid: " + userID, userRead);
        Assert.assertNotNull("User ID should have not been NULL!", userRead.getUserId());
        Assert.assertEquals("Incorrect user email address!", email, userRead.getEmail());
        Assert.assertEquals("Incorrect fullname!", fullname, userRead.getFullname());
        Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, userRead.getActive());
        Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, userRead.getConfirmed());
    }
    
    @Test
    public void testRead_ByEmail() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testRead_ByEmail";
        mUserCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, false);

        //Retrieve by email:
        UserDAOHibernateImpl daoUserImpl = new UserDAOHibernateImpl();
        User userToFindByEmail = new User();
        userToFindByEmail.setEmail(email);
        List<User> foundUsers = 
                daoUserImpl.read(userToFindByEmail, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, false, false, 0, 1);
        Assert.assertNotNull("DB should have NOT returned NULL for user with email: " + email, foundUsers);
        Assert.assertEquals("DB should have NOT returned NULL for user with email: " + email, 1, foundUsers.size());
        User userRead = foundUsers.get(0);
        Assert.assertNotNull("DB should have NOT returned NULL for user with email: " + email, userRead);
        Assert.assertNotNull("User ID should have not been NULL!", userRead.getUserId());
        Assert.assertEquals("Incorrect user email address!", email, userRead.getEmail());
        Assert.assertEquals("Incorrect fullname!", fullname, userRead.getFullname());
        Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, userRead.getActive());
        Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, userRead.getConfirmed());
    }    
    
    @Test
    public void testUpdate_ALL() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testUpdate_ALL";
        mUserCreated = TestUtilForUser.userCreate_byDAOImpl(email, username, fullname, false);
        String userID = mUserCreated.getUserId();

        //Update:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        UserDAOHibernateImpl daoUserImpl = new UserDAOHibernateImpl();
        String userUpdatedUsername = "usernameShouldNotEditable";
        String updatedPassword = "newUpdatedPassword";
        String userUpdatedEmail = "newUpdatedEmail";
        String updatedFullname = "Updated testUpdateUser_ALL";
        String updatedSummary = "Updated summary, how ya doin?";
        String updatedLocation = "Cardiff by the Sea, California, USA";

        User userToUpdate = new User();
        userToUpdate.setUserId(userID);
        userToUpdate.setUsername(userUpdatedUsername);
        userToUpdate.setPassword(updatedPassword);
        userToUpdate.setEmail(userUpdatedEmail);
        userToUpdate.setActive(YesNoEnum.NO);
        userToUpdate.setConfirmed(YesNoEnum.YES);
        userToUpdate.setFullname(updatedFullname);
        userToUpdate.setSummary(updatedSummary);
        userToUpdate.setLocation(updatedLocation);
        User userUpdated = daoUserImpl.update(userToUpdate);
        Assert.assertNotNull("Returned user updated should NOT be null!", userUpdated);

        //Verify:
        Assert.assertEquals("After update: Incorrect UserID!", userID, userUpdated.getUserId());
        Assert.assertEquals("After update: Username should NOT changed nor editable!", username, userUpdated.getUsername());
        Assert.assertEquals("After update: Incorrect user email address!", userUpdatedEmail, userUpdated.getEmail());
        Assert.assertEquals("After update: Incorrect password!", updatedPassword, userUpdated.getPassword());
        Assert.assertEquals("After update: Incorrect fullname!", updatedFullname, userUpdated.getFullname());
        Assert.assertEquals("After update: Incorrect active status after update!", YesNoEnum.NO, userUpdated.getActive());
        Assert.assertEquals("After update: Incorrect confirmed status after update!", YesNoEnum.YES, userUpdated.getConfirmed());
        Assert.assertEquals("After update: Incorrect summary after update!",updatedSummary, userUpdated.getSummary());
        Assert.assertEquals("After update: Incorrect Location after update!", updatedLocation, userUpdated.getLocation());

        //Validate dates (special):
        Date createdDate = DateUtil.formatDate(userUpdated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
        Date updatedDate = DateUtil.formatDate(userUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Record's updated date should have been AFTER created date!", 
                updatedDate.after(createdDate));
    }
    


    
    
}
