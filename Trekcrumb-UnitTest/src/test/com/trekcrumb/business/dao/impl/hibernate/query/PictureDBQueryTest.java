package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPicture;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQuery;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.DateUtil;

public class PictureDBQueryTest {
    private static final String CLASS_NAME = "PictureDBQueryTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mTripCreated = null;
        mUserCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
    }

    @Test
    public void testCreateAndReadAndDelete_ByTrip_WithLocalTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 1, listOfPicsFound.size());
            Assert.assertNotNull("Retrieve by Trip: Picture entity in the list should NOT be null!", listOfPicsFound.get(0));

            Picture picFound = listOfPicsFound.get(0);
            Assert.assertNotNull("Found Picture: Found picture ID should NOT be null!", picFound.getId());
            Assert.assertEquals("Found Picture: Invalid Trip ID!", mTripCreated.getId(), picFound.getTripID());
            Assert.assertEquals("Found Picture: Invalid image Name!", CLASS_NAME, picFound.getImageName());
            Assert.assertEquals("Found Picture: Invalid image URL!", TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, picFound.getImageURL());
            Assert.assertNull("Found Picture: Note should be NULL!", picFound.getNote());
            Assert.assertNull("Found Picture: Value 'is_cover_picture' should be NULL!", picFound.getCoverPicture());
            Assert.assertNull("Found Picture: 'Feature for PlaceID' should be NULL!", picFound.getFeatureForPlaceID());
            
            Assert.assertNotNull("Found Picture: Created date shall not be null!", picFound.getCreated());
            Assert.assertNotNull("Found Picture: Updated date shall not be null!", picFound.getUpdated());
            Date createdDate = DateUtil.formatDate(picFound.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(picFound.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's created date and updated date should have been the same!", 
                    createdDate.equals(updatedDate));
            
            //Delete by Trip:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setTripID(mTripCreated.getId());
            int numOfDeleted = PictureDBQuery.delete(session, null, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            Assert.assertEquals("Delete Picture: Invalid number of deleted items!", 1, numOfDeleted);
            
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Delete Picture: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture: Invalid number of found items!", 0, listOfPicsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreateAndReadAndDelete_ByTrip_WithClientTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 1, listOfPicsFound.size());
            Assert.assertNotNull("Retrieve by Trip: Picture entity in the list should NOT be null!", listOfPicsFound.get(0));

            Picture picFound = listOfPicsFound.get(0);
            Assert.assertNotNull("Found Picture: Found picture ID should NOT be null!", picFound.getId());
            Assert.assertEquals("Found Picture: Invalid Trip ID!", mTripCreated.getId(), picFound.getTripID());
            Assert.assertEquals("Found Picture: Invalid image Name!", CLASS_NAME, picFound.getImageName());
            Assert.assertEquals("Found Picture: Invalid image URL!", TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, picFound.getImageURL());
            Assert.assertNull("Found Picture: Note should be NULL!", picFound.getNote());
            Assert.assertNull("Found Picture: Value 'is_cover_picture' should be NULL!", picFound.getCoverPicture());
            Assert.assertNull("Found Picture: 'Feature for PlaceID' should be NULL!", picFound.getFeatureForPlaceID());
            
            Assert.assertNotNull("Found Picture: Created date shall not be null!", picFound.getCreated());
            Assert.assertNotNull("Found Picture: Updated date shall not be null!", picFound.getUpdated());
            Date createdDate = DateUtil.formatDate(picFound.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(picFound.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's created date and updated date should have been the same!", 
                    createdDate.equals(updatedDate));
            
            //Delete by Trip:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            int numOfDeleted = PictureDBQuery.delete(session, transaction, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            transaction.commit();
            Assert.assertEquals("Delete Picture: Invalid number of deleted items!", 1, numOfDeleted);
            
            //Validate:
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Delete Picture: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture: Invalid number of found items!", 0, listOfPicsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByTrip_MultiPics_WithLocalTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 2, listOfPicsFound.size());

            //Delete by Trip:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setTripID(mTripCreated.getId());
            int numOfDeleted = PictureDBQuery.delete(session, null, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            Assert.assertEquals("Delete Picture: Invalid number of deleted items!", 2, numOfDeleted);
            
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Delete Picture: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture: Invalid number of found items!", 0, listOfPicsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByTrip_MultiPics_WithClientTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 2, listOfPicsFound.size());

            //Delete by Trip:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            int numOfDeleted = PictureDBQuery.delete(session, transaction, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            transaction.commit();
            Assert.assertEquals("Delete Picture: Invalid number of deleted items!", 2, numOfDeleted);
            
            //Validate:
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Delete Picture: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture: Invalid number of found items!", 0, listOfPicsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testReadAndDelete_ByIDAndOrderBy_WithLocalTransaction() throws Exception {
        //Create:
        String firstPicNote = "Main Picture";
        String secondPicNote = "Featured Place Picture";
        String secondPicPlaceID = "123456";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME,
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.YES, null, firstPicNote);
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME,
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.NO, secondPicPlaceID, secondPicNote);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 2, listOfPicsFound.size());

            //Order by created date asc:
            Picture firstPic = listOfPicsFound.get(0);
            Assert.assertNotNull("Found 1st Picture: Main pic should NOT be null!", firstPic);
            Assert.assertEquals("Found 1st Picture: Invalid 'is_cover_picture' value!", YesNoEnum.YES, firstPic.getCoverPicture());
            Assert.assertEquals("Found 1st Picture: Invalid note!", firstPicNote, firstPic.getNote());

            Picture secondPic = listOfPicsFound.get(1);
            Assert.assertNotNull("Found 2nd Picture: Second pic should NOT be null!", secondPic);
            Assert.assertEquals("Found 2nd Picture: Invalid 'is_cover_picture' value!", YesNoEnum.NO, secondPic.getCoverPicture());
            Assert.assertEquals("Found 2nd Picture: Invalid note!", secondPicNote, secondPic.getNote());
            Assert.assertEquals("Found 2nd Picture: Invalid feature placeID!", secondPicPlaceID, secondPic.getFeatureForPlaceID());
            
            //Retrieve by ID
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(firstPic.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by ID: Invalid number of found items!", 1, listOfPicsFound.size());        
            Picture picMainReRetrieved = listOfPicsFound.get(0);
            Assert.assertEquals("Retrieve by ID: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picMainReRetrieved.getCoverPicture());
            Assert.assertEquals("Retrieve by ID: Invalid note!", firstPicNote, picMainReRetrieved.getNote());
            
            //Delete by ID:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setId(firstPic.getId());
            int numOfDeleted = PictureDBQuery.delete(session, null, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertEquals("Delete Picture by ID: Invalid number of deleted items!", 1, numOfDeleted);
            
            //Verify:
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            Assert.assertNotNull("Delete Picture by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture by ID: Invalid number of found items!", 0, listOfPicsFound.size());        

            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Delete Picture by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture by ID: Invalid number of found items!", 1, listOfPicsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testReadAndDelete_ByIDAndOrderBy_WithClientTransaction() throws Exception {
        //Create:
        String firstPicNote = "Main Picture";
        String secondPicNote = "Featured Place Picture";
        String secondPicPlaceID = "123456";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME,
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.YES, null, firstPicNote);
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME,
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.NO, secondPicPlaceID, secondPicNote);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 2, listOfPicsFound.size());

            //Order by created date asc:
            Picture firstPic = listOfPicsFound.get(0);
            Assert.assertNotNull("Found 1st Picture: Main pic should NOT be null!", firstPic);
            Assert.assertEquals("Found 1st Picture: Invalid 'is_cover_picture' value!", YesNoEnum.YES, firstPic.getCoverPicture());
            Assert.assertEquals("Found 1st Picture: Invalid note!", firstPicNote, firstPic.getNote());

            Picture secondPic = listOfPicsFound.get(1);
            Assert.assertNotNull("Found 2nd Picture: Second pic should NOT be null!", secondPic);
            Assert.assertEquals("Found 2nd Picture: Invalid 'is_cover_picture' value!", YesNoEnum.NO, secondPic.getCoverPicture());
            Assert.assertEquals("Found 2nd Picture: Invalid note!", secondPicNote, secondPic.getNote());
            Assert.assertEquals("Found 2nd Picture: Invalid feature placeID!", secondPicPlaceID, secondPic.getFeatureForPlaceID());
            
            //Retrieve by ID
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(firstPic.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by ID: Invalid number of found items!", 1, listOfPicsFound.size());        
            Picture picMainReRetrieved = listOfPicsFound.get(0);
            Assert.assertEquals("Retrieve by ID: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picMainReRetrieved.getCoverPicture());
            Assert.assertEquals("Retrieve by ID: Invalid note!", firstPicNote, picMainReRetrieved.getNote());
            
            //Delete by ID:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setId(firstPic.getId());
            transaction = session.beginTransaction();
            int numOfDeleted = PictureDBQuery.delete(session, transaction, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            transaction.commit();
            Assert.assertEquals("Delete Picture by ID: Invalid number of deleted items!", 1, numOfDeleted);
            
            //Verify:
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            Assert.assertNotNull("Delete Picture by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture by ID: Invalid number of found items!", 0, listOfPicsFound.size());        

            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Delete Picture by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Delete Picture by ID: Invalid number of found items!", 1, listOfPicsFound.size());
            transaction.commit();
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByUserID_WithLocalTransaction() throws Exception {
        //Create:
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME,
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.YES, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME,
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.NO, null, null);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve:
            Picture picToRetrieve = new Picture();
            picToRetrieve.setUserID(mUserCreated.getUserId());
            List<Picture> listPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID, 
                                        null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Picture retrieved: Returned list should NOT be null!", listPicsFound);
            Assert.assertEquals("Picture retrieved: Invalid number of found items!", 2, listPicsFound.size());
            
            //Delete:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            int numDeleted = PictureDBQuery.delete(session, null, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_USERID);
            Assert.assertEquals("Picture deleted: Invalid number of deleted items!", 2, numDeleted);
            
            //Verify
            listPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID, 
                                        null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Delete Picture by ID: Returned list should NOT be null!", listPicsFound);
            Assert.assertEquals("Delete Picture by ID: Invalid number of found items!", 0, listPicsFound.size());        
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testCount_ByTripID_WithLocalTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        Session session = null;
        try {
            session = HibernateManager.openSession();

            Picture picToRetrieve = new Picture();
            picToRetrieve.setTripID(mTripCreated.getId());
            int numOfPics = PictureDBQuery.count(session, null, picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID);
            Assert.assertEquals("Picture Count: Invalid number of found items!", 2, numOfPics);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCount_ByUsername_WithLocalTransaction() throws Exception {
        Trip tripPublic =  TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), CLASS_NAME, "testRetrieve_ByUsername() - PUBLIC", 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPublic.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        Trip tripFriendsOnly = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), CLASS_NAME, "testRetrieve_ByUsername() - FRIENDS ONLY", 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.FRIENDS, null, false);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripFriendsOnly.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Trip tripPrivate = TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), CLASS_NAME, "testRetrieve_ByUsername() - PRIVATE", 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PRIVATE, null, false);
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPrivate.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Count PUBLIC only:
            Picture picToRetrieve = new Picture();
            picToRetrieve.setUsername(mUserCreated.getUsername());
            picToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
            int numOfPics = PictureDBQuery.count(session, null, picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Picture Count: Invalid number of found items!", 1, numOfPics);
            
            //Count FRIENDS only:
            picToRetrieve.setPrivacy(TripPrivacyEnum.FRIENDS);
            numOfPics = PictureDBQuery.count(session, null, picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Picture Count: Invalid number of found items!", 2, numOfPics);

            //Count ALL:
            picToRetrieve.setPrivacy(TripPrivacyEnum.PRIVATE);
            numOfPics = PictureDBQuery.count(session, null, picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Picture Count: Invalid number of found items!", 3, numOfPics);
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testSearch_WithLocalTransaction() throws Exception {
        //Create:
        Trip tripPublic =  TestUtilForTrip.tripCreate_byManager(
                mUserCreated.getUserId(), CLASS_NAME, "testRetrieve_ByUsername() - PUBLIC", 
                TestConstants.UNITTEST_TRIP_LOCATION, TripPrivacyEnum.PUBLIC, null, false);
        String pic1_imageName = "PictureOneImageName.JPG";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPublic.getId(), pic1_imageName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        String pic2_imageName = "PictureTwoImageName.PNG";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), tripPublic.getId(), pic2_imageName, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);

        /*
         * Search: 
         * To validate exact number of items is not possible since it will return the last
         * CommonConstants.RECORDS_NUMBER_MAX records, which could be exactly that much or less
         * depending test data in the DB. But for sure, the last records MUST be this test data
         * (descending by created date).
         */ 
        Session session = null;
        try {
            session = HibernateManager.openSession();
            List<Picture> listPicsFound = 
                    PictureDBQuery.read(session, null, null, ServiceTypeEnum.PICTURE_SEARCH, 
                                        null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Picture Search: Returned list should NOT be null!", listPicsFound);
            Assert.assertTrue("Picture Search: Invalid number of found items!", listPicsFound.size() >= 2);
            
            Picture firstPicInList = listPicsFound.get(0);
            Assert.assertEquals("Picture Search: Invalid returned first Picture in the list!", pic2_imageName, firstPicInList.getImageName());

            Picture secondPicInList = listPicsFound.get(1);
            Assert.assertEquals("Picture Search: Invalid returned second Picture in the list!", pic1_imageName, secondPicInList.getImageName());
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testUpdate_ALL_WithLocalTransaction() throws Exception {
        String featurePlaceIDOrig = "12345";
        String noteOrig = "Nice mugshot";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.YES, featurePlaceIDOrig, noteOrig);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 1, listOfPicsFound.size());
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertNotNull("Picture created: Main pic should NOT be null!", picCreated);
            Assert.assertNotNull("Picture created: PictureID should NOT be null!", picCreated.getId());
            Assert.assertEquals("Picture created: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picCreated.getCoverPicture());
            Assert.assertEquals("Picture created: Invalid Featured Place ID!", featurePlaceIDOrig, picCreated.getFeatureForPlaceID());
            Assert.assertEquals("Picture created: Invalid note!", noteOrig, picCreated.getNote());

            //Update:
            String featurePlaceIDNew = "";
            String noteNew = "Being updated.";
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setNote(noteNew);
            picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
            picToUpdate.setCoverPicture(YesNoEnum.NO);
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            int numUpdated = PictureDBQuery.update(session, null, picToUpdate);
            Assert.assertEquals("Picture update: Invalid number of updated items!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            //Validate:
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Assert.assertNotNull("Retrieve by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by ID: Invalid number of found items!", 1, listOfPicsFound.size());        
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update: Invalid 'is_cover_picture' value!", YesNoEnum.NO, picAfterUpdate.getCoverPicture());
            Assert.assertEquals("Picture update: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());
            Assert.assertEquals("Picture update: Invalid note!", noteNew, picAfterUpdate.getNote());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_ALL_WithClientTransaction() throws Exception {
        String featurePlaceIDOrig = "12345";
        String noteOrig = "Nice mugshot";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, YesNoEnum.YES, featurePlaceIDOrig, noteOrig);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found items!", 1, listOfPicsFound.size());
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertNotNull("Picture created: Main pic should NOT be null!", picCreated);
            Assert.assertNotNull("Picture created: PictureID should NOT be null!", picCreated.getId());
            Assert.assertEquals("Picture created: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picCreated.getCoverPicture());
            Assert.assertEquals("Picture created: Invalid Featured Place ID!", featurePlaceIDOrig, picCreated.getFeatureForPlaceID());
            Assert.assertEquals("Picture created: Invalid note!", noteOrig, picCreated.getNote());

            //Update:
            String featurePlaceIDNew = "";
            String noteNew = "Being updated.";
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setNote(noteNew);
            picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
            picToUpdate.setCoverPicture(YesNoEnum.NO);
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            int numUpdated = PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            Assert.assertEquals("Picture update: Invalid number of updated items!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            //Validate:
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by ID: Returned list should NOT be null!", listOfPicsFound);
            Assert.assertEquals("Retrieve by ID: Invalid number of found items!", 1, listOfPicsFound.size());        
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update: Invalid 'is_cover_picture' value!", YesNoEnum.NO, picAfterUpdate.getCoverPicture());
            Assert.assertEquals("Picture update: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());
            Assert.assertEquals("Picture update: Invalid note!", noteNew, picAfterUpdate.getNote());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_PictureCover_WithLocalTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertNull("Picture created: Value 'is_cover_picture' should be NULL!", picCreated.getCoverPicture());

            //Update 1:
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setCoverPicture(YesNoEnum.YES);
            int numUpdated = PictureDBQuery.update(session, null, picToUpdate);
            Assert.assertEquals("Picture update 1: Invalid number of updated items!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update 1: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picAfterUpdate.getCoverPicture());
            
            //Update 2:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setCoverPicture(null);
            PictureDBQuery.update(session, null, picToUpdate);

            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update 2: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picAfterUpdate.getCoverPicture());

            //Update 3:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setCoverPicture(YesNoEnum.NO);
            PictureDBQuery.update(session, null, picToUpdate);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update 3: Invalid 'is_cover_picture' value!", YesNoEnum.NO, picAfterUpdate.getCoverPicture());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_PictureCover_WithClientTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            transaction.commit();
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertNull("Picture created: Value 'is_cover_picture' should be NULL!", picCreated.getCoverPicture());

            //Update 1:
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setCoverPicture(YesNoEnum.YES);
            transaction = session.beginTransaction();
            int numUpdated = PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            Assert.assertEquals("Picture update 1: Invalid number of updated items!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update 1: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picAfterUpdate.getCoverPicture());
            
            //Update 2:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setCoverPicture(null);
            transaction = session.beginTransaction();
            PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();

            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update 2: Invalid 'is_cover_picture' value!", YesNoEnum.YES, picAfterUpdate.getCoverPicture());

            //Update 3:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setCoverPicture(YesNoEnum.NO);
            transaction = session.beginTransaction();
            PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update 3: Invalid 'is_cover_picture' value!", YesNoEnum.NO, picAfterUpdate.getCoverPicture());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_FeaturedPlace_WithLocalTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertEquals("Picture created: Invalid Featured Place ID!", null, picCreated.getFeatureForPlaceID());

            //Update 1:
            String featurePlaceIDNew = "123456";
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
            PictureDBQuery.update(session, null, picToUpdate);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture updated 1: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());
            
            //Update 2:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setFeatureForPlaceID(null);
            PictureDBQuery.update(session, null, picToUpdate);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture updated 2: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());

            //Update 3:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setFeatureForPlaceID("");
            PictureDBQuery.update(session, null, picToUpdate);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture updated 3: Invalid Featured Place ID!", "", picAfterUpdate.getFeatureForPlaceID());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_FeaturedPlace_WithClientTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertEquals("Picture created: Invalid Featured Place ID!", null, picCreated.getFeatureForPlaceID());

            //Update 1:
            String featurePlaceIDNew = "123456";
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
            transaction = session.beginTransaction();
            PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture updated 1: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());
            
            //Update 2:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setFeatureForPlaceID(null);
            transaction = session.beginTransaction();
            PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture updated 2: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());

            //Update 3:
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setFeatureForPlaceID("");
            transaction = session.beginTransaction();
            PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture updated 3: Invalid Featured Place ID!", "", picAfterUpdate.getFeatureForPlaceID());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_Note_WithLocalTransaction() throws Exception {
        String noteOrig = "Nice mugshot";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, noteOrig);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertEquals("Picture created: Invalid note!", noteOrig, picCreated.getNote());

            //Update:
            String noteNew = "";
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setNote(noteNew);
            PictureDBQuery.update(session, null, picToUpdate);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update: Invalid note!", noteNew, picAfterUpdate.getNote());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_Note_WithClientTransaction() throws Exception {
        String noteOrig = "Nice mugshot";
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, noteOrig);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            transaction.commit();
            Picture picCreated = listOfPicsFound.get(0);
            Assert.assertEquals("Picture created: Invalid note!", noteOrig, picCreated.getNote());

            //Update:
            String noteNew = "";
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setNote(noteNew);
            transaction = session.beginTransaction();
            PictureDBQuery.update(session, transaction, picToUpdate);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            Picture picToRetrieveByID = new Picture();
            picToRetrieveByID.setId(picCreated.getId());
            picToRetrieveByID.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPicsFound = 
                    PictureDBQuery.read(session, transaction, picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            transaction.commit();
            Picture picAfterUpdate = listOfPicsFound.get(0);
            Assert.assertEquals("Picture update: Invalid note!", noteNew, picAfterUpdate.getNote());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testUpdate_AfterDelete_WithLocalTransaction() throws Exception {
        TestUtilForPicture.pictureCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), CLASS_NAME, 
                TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL, null, null, null);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by Trip
            Picture picToRetrieveByTrip = new Picture();
            picToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Picture> listOfPicsFound = 
                    PictureDBQuery.read(session, null, picToRetrieveByTrip, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
            Picture picCreated = listOfPicsFound.get(0);
            
            //Delete:
            Picture picToDelete = new Picture();
            picToDelete.setUserID(mUserCreated.getUserId());
            picToDelete.setId(picCreated.getId());
            int numOfDeleted = PictureDBQuery.delete(session, null, picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
            Assert.assertEquals("Picture Delete: Invalid number of deleted items!", 1, numOfDeleted);
            
            //Update:
            Picture picToUpdate = new Picture();
            picToUpdate.setId(picCreated.getId());
            picToUpdate.setUserID(picCreated.getUserID());
            picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            picToUpdate.setNote("Illegal update!");
            int numUpdated = PictureDBQuery.update(session, null, picToUpdate);
            Assert.assertEquals("Picture Update after Delete: Invalid returned number of updated!", 0, numUpdated);
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
}
