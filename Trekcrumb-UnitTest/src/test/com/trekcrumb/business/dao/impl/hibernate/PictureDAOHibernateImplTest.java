package test.com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPicture;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.IPictureDAO;
import com.trekcrumb.business.dao.impl.hibernate.PictureDAOHibernateImpl;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.DateUtil;

/**
 * Note:
 * Mostly basic functionaly test cases. More advanced test case scenarios are in PictureManagerTest.
 *
 */
public class PictureDAOHibernateImplTest {
    private static final String CLASS_NAME = "PictureDAOHibernateImplTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME); 
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
        mTripCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForPicture.pictureDelete_byDB_ALLPictures(mUserCreated.getUserId());
    }    
    
    @Test
    public void testCreateRetrieveAndDelete_ByID() throws Exception {
        IPictureDAO daoImpl = new PictureDAOHibernateImpl();
        
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(),
                CLASS_NAME, TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL,
                null, null, TestConstants.UNITTEST_PICTURE_NOTE, true);

        //Retrieve:
        Picture picToRetrieve = new Picture();
        picToRetrieve.setId(pictureCreated.getId());
        picToRetrieve.setTripID(mTripCreated.getId());
        List<Picture> listPicsFound = 
                daoImpl.read(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 
                             OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
        Assert.assertNotNull("Picture Retrieve: Returned list should NOT be null!", listPicsFound);
        Assert.assertEquals("Picture Retrieve: Invalid number of found items!", 1, listPicsFound.size());
        Assert.assertNotNull("Picture Retrieve: Picture entity in the list should NOT be null!", listPicsFound.get(0));
        Picture picFound = listPicsFound.get(0);
        Assert.assertEquals("Picture Retrieve: Invalid image name!", pictureCreated.getImageName(), picFound.getImageName());
        
        //Delete:
        Picture picToDelete = new Picture();
        picToDelete.setId(pictureCreated.getId());
        picToDelete.setUserID(mUserCreated.getUserId());
        int numDeleted = daoImpl.delete(picToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID);
        Assert.assertEquals("Picture Delete: Invalid number of deleted items!", 1, numDeleted);
        
        //Validate:
        listPicsFound = daoImpl.read(picToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, 
                                     OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
        Assert.assertNull("Picture Retrieve after Delete: Returned picture list should have been EMPTY!", listPicsFound);
    }
    
    @Test
    public void testUpdate_ALL() throws Exception {
        IPictureDAO daoImpl = new PictureDAOHibernateImpl();
        
        Picture pictureCreated = TestUtilForPicture.pictureCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(),
                CLASS_NAME, TestConstants.UNITTEST_PICTURE_IMAGE_TARGET_URL,
                YesNoEnum.NO, null, TestConstants.UNITTEST_PICTURE_NOTE, true);
        
        //Update:
        String featurePlaceIDNew = "12345";
        String noteNew = "Being updated.";
        Picture picToUpdate = new Picture();
        picToUpdate.setUserID(mUserCreated.getUserId());
        picToUpdate.setTripID(mTripCreated.getId());
        picToUpdate.setId(pictureCreated.getId());
        picToUpdate.setNote(noteNew);
        picToUpdate.setCoverPicture(YesNoEnum.YES);
        picToUpdate.setFeatureForPlaceID(featurePlaceIDNew);
        picToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Picture picUpdated = daoImpl.update(picToUpdate);
        Assert.assertNotNull("Picture Update: Returned updated picture should NOT be null!", picUpdated);
        Assert.assertEquals("Picture Update: Invalid Picture ID!", pictureCreated.getId(), picUpdated.getId());
        Assert.assertEquals("Picture Update: Invalid updated note!", noteNew, picUpdated.getNote());
        Assert.assertEquals("Picture Update: Invalid Picture Cover value!", YesNoEnum.YES, picUpdated.getCoverPicture());
        Assert.assertEquals("Picture Update: Invalid updated Featured Place ID!", featurePlaceIDNew, picUpdated.getFeatureForPlaceID());
        
        //Validate:
        Picture picToRetrieveByID = new Picture();
        picToRetrieveByID.setId(pictureCreated.getId());
        picToRetrieveByID.setTripID(mTripCreated.getId());
        List<Picture> listOfPicsFound = daoImpl.read(picToRetrieveByID, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
        Assert.assertNotNull("Picture Retrieve after Update: Returned list should NOT be null!", listOfPicsFound);
        Assert.assertEquals("Picture Retrieve after Update: Invalid number of found items!", 1, listOfPicsFound.size());        
        Picture picAfterUpdate = listOfPicsFound.get(0);
        Assert.assertEquals("Picture Retrieve after Update: Invalid note!", noteNew, picAfterUpdate.getNote());
        Assert.assertEquals("Picture Update: Invalid Picture Cover value!", YesNoEnum.YES, picAfterUpdate.getCoverPicture());
        Assert.assertEquals("Picture Retrieve after Update: Invalid Featured Place ID!", featurePlaceIDNew, picAfterUpdate.getFeatureForPlaceID());
    }
    
}
