package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.TripDBQuery;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.DateUtil;

public class TripDBQueryTest {
    private static final String CLASS_NAME = "TripDBQueryTest";
    private static User mUserCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mUserCreated == null) {
            String username = String.valueOf(System.currentTimeMillis());
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME;
            mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
        }
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForTrip.tripDelete_byDB_ALLTrips(mUserCreated.getUserId());
    }

    @Test
    public void testCreateAndReadAndDelete_WithLocalTransaction() throws Exception {
		//Create:
		TestUtilForTrip.tripCreate_byDB(
				mUserCreated.getUserId(), 
				CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
				TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
				false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Found Trip: Found Trip ID should NOT be null!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid user ID!", mUserCreated.getUserId(), tripFound.getUserID());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.PUBLISH, tripFound.getPublish());

            //Validate dates (special):
            Date createdDate = DateUtil.formatDate(tripFound.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(tripFound.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's created date and updated date should have been the same!", 
                    createdDate.equals(updatedDate));

            //Delete by ID:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripFound.getId());
            int numOfDeleted = TripDBQuery.delete(session, null, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 1, numOfDeleted);
            
            //Verify:
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Delete Trip: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Delete Trip: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreateAndReadAndDelete_WithClientTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Found Trip: Found Trip ID should NOT be null!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid user ID!", mUserCreated.getUserId(), tripFound.getUserID());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.PUBLISH, tripFound.getPublish());

            //Validate dates (special):
            Date createdDate = DateUtil.formatDate(tripFound.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(tripFound.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's created date and updated date should have been the same!", 
                    createdDate.equals(updatedDate));

            //Delete by ID:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripFound.getId());
            transaction = session.beginTransaction();
            int numOfDeleted = TripDBQuery.delete(session, transaction, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            transaction.commit();
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 1, numOfDeleted);
            
            //Verify:
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Delete Trip: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Delete Trip: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testRetrieveAndDelete_ByUserID_WithLocalTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve by UserID
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity [0] in the list should NOT be null!", listOfTripsFound.get(0));
            Assert.assertNotNull("Retrieve by User: Trip entity [1] in the list should NOT be null!", listOfTripsFound.get(1));

            //Delete:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = TripDBQuery.delete(session, null, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID);
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 2, numOfDeleted);
            
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Delete Trip: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Delete Trip: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    

    @Test
    public void testRetrieveAndDelete_ByUserID_WithClientTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve by UserID
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity [0] in the list should NOT be null!", listOfTripsFound.get(0));
            Assert.assertNotNull("Retrieve by User: Trip entity [1] in the list should NOT be null!", listOfTripsFound.get(1));

            //Delete:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numOfDeleted = TripDBQuery.delete(session, transaction, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID);
            transaction.commit();
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 2, numOfDeleted);
            
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Delete Trip: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Delete Trip: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testRetrieveAndDelete_ByUsername_WithLocalTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve by Username
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUsername(mUserCreated.getUsername());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity [0] in the list should NOT be null!", listOfTripsFound.get(0));
            Assert.assertNotNull("Retrieve by User: Trip entity [1] in the list should NOT be null!", listOfTripsFound.get(1));

            //Delete:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = TripDBQuery.delete(session, null, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID);
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 2, numOfDeleted);
            
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
            Assert.assertNotNull("Delete Trip: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Delete Trip: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    

    @Test
    public void testRetrieveAndDelete_ByUsername_WithClientTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve by UserID
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUsername(mUserCreated.getUsername());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity [0] in the list should NOT be null!", listOfTripsFound.get(0));
            Assert.assertNotNull("Retrieve by User: Trip entity [1] in the list should NOT be null!", listOfTripsFound.get(1));

            //Delete:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numOfDeleted = TripDBQuery.delete(session, transaction, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_USERID);
            transaction.commit();
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 2, numOfDeleted);
            
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Delete Trip: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Delete Trip: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }        
    
    @Test
    public void testRetrieveAndDelete_ByTripID_WithLocalTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve by UserID
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            //Retrieve by TripID
            Trip tripFound1 = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by User: One of Trips should never has NULL ID!", tripFound1.getId());

            Trip tripToRetrieveByTripID = new Trip();
            tripToRetrieveByTripID.setUserID(mUserCreated.getUserId());
            tripToRetrieveByTripID.setId(tripFound1.getId());
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByTripID, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            //Delete:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripFound1.getId());
            int numOfDeleted = TripDBQuery.delete(session, null, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            Assert.assertEquals("Delete by TripID: Invalid number of deleted Trip!", 1, numOfDeleted);
            
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testRetrieveAndDelete_ByTripID_WithClientTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            //Retrieve by UserID
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            //Retrieve by TripID
            Trip tripFound1 = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by User: One of Trips should never has NULL ID!", tripFound1.getId());

            Trip tripToRetrieveByTripID = new Trip();
            tripToRetrieveByTripID.setUserID(mUserCreated.getUserId());
            tripToRetrieveByTripID.setId(tripFound1.getId());
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByTripID, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            //Delete:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(tripFound1.getId());
            transaction = session.beginTransaction();
            int numOfDeleted = TripDBQuery.delete(session, transaction, tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            transaction.commit();
            Assert.assertEquals("Delete by TripID: Invalid number of deleted Trip!", 1, numOfDeleted);
            
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testRetrieve_ByStatusAndPrivacyAndPublish_WithLocalTransaction() throws Exception {
        //Create:
        String tripName_Active = CLASS_NAME + "_ACTIVE";
        String tripName_Private = CLASS_NAME + "_PRIVATE";
        String tripName_NotPublish = CLASS_NAME + "_NOTPUBLISH";
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Active, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Private, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_NotPublish, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 3, listOfTripsFound.size());
            
            //Retrieve 'ACTIVE' only:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(null);
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve 'ACTIVE' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'ACTIVE' only by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertEquals("Retrieve 'ACTIVE' only by User: Invalid trip name!", tripName_Active, listOfTripsFound.get(0).getName());
            
            //Retrieve 'COMPLETED' only:
            tripToRetrieveByUser.setStatus(TripStatusEnum.COMPLETED);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(null);
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve 'ACTIVE' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'ACTIVE' only by User: Invalid number of found trips!", 2, listOfTripsFound.size());

            //Retrieve 'PUBLIC' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PUBLIC);
            tripToRetrieveByUser.setPublish(null);
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve 'PUBLIC' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'PUBLIC' only by User: Invalid number of found trips!", 2, listOfTripsFound.size());

            //Retrieve ALL (including 'PRIVATE' trips):
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(null);
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve to include 'PRIVATE' Trips by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("RRetrieve to include 'PRIVATE' Trips by User: Invalid number of found trips!", 3, listOfTripsFound.size());
            
            //Retrieve 'NOT PUBLISH' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve 'NOT PUBLISH' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'NOT PUBLISH' only by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertEquals("Retrieve 'NOT PUBLISH' only by User: Invalid trip name!", tripName_NotPublish, listOfTripsFound.get(0).getName());
            
            //Retrieve ALL specified:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve 'ALL specified' by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'All specified' only by User: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testRetrieve_ByStatusAndPrivacyAndPublish_WithClientTransaction() throws Exception {
        //Create:
        String tripName_Active = CLASS_NAME + "_ACTIVE";
        String tripName_Private = CLASS_NAME + "_PRIVATE";
        String tripName_NotPublish = CLASS_NAME + "_NOTPUBLISH";
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Active, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Private, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_NotPublish, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 3, listOfTripsFound.size());
            
            //Retrieve 'ACTIVE' only:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(null);
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve 'ACTIVE' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'ACTIVE' only by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertEquals("Retrieve 'ACTIVE' only by User: Invalid trip name!", tripName_Active, listOfTripsFound.get(0).getName());
            
            //Retrieve 'COMPLETED' only:
            tripToRetrieveByUser.setStatus(TripStatusEnum.COMPLETED);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(null);
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve 'ACTIVE' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'ACTIVE' only by User: Invalid number of found trips!", 2, listOfTripsFound.size());

            //Retrieve 'PUBLIC' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PUBLIC);
            tripToRetrieveByUser.setPublish(null);
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve 'PRIVATE' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'PRIVATE' only by User: Invalid number of found trips!", 2, listOfTripsFound.size());

            //Retrieve ALL (including 'PRIVATE' trips):
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(null);
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve to include 'PRIVATE' Trips by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("RRetrieve to include 'PRIVATE' Trips by User: Invalid number of found trips!", 3, listOfTripsFound.size());

            //Retrieve 'NOT PUBLISH' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve 'NOT PUBLISH' only by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'NOT PUBLISH' only by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertEquals("Retrieve 'NOT PUBLISH' only by User: Invalid trip name!", tripName_NotPublish, listOfTripsFound.get(0).getName());
            
            //Retrieve ALL specified:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve 'ALL specified' by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve 'All specified' only by User: Invalid number of found trips!", 0, listOfTripsFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testRetrieve_ByWhereClause_TripStatus_WithLocalTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            String whereClause = "where STATUS = " + TripStatusEnum.COMPLETED.getId() + " and USERID = '" + mUserCreated.getUserId() + "'";
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, whereClause, null, 0, -1);
            Assert.assertNotNull("Retrieve by keyword: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by keyword: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by keyword: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by keyword: One of Trips should never has NULL ID!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.COMPLETED, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PRIVATE, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.NOT_PUBLISH, tripFound.getPublish());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testRetrieve_ByWhereClause_TripStatus_WithClientTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            String whereClause = "where STATUS = " + TripStatusEnum.COMPLETED.getId() + " and USERID = '" + mUserCreated.getUserId() + "'";
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, whereClause, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by keyword: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by keyword: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by keyword: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by keyword: One of Trips should never has NULL ID!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.COMPLETED, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PRIVATE, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.NOT_PUBLISH, tripFound.getPublish());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testRetrieve_ByWhereClause_TripPrivacy_WithLocalTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            String whereClause = "where PRIVACY = " + TripPrivacyEnum.PUBLIC.getId() + " and USERID = '" + mUserCreated.getUserId() + "'";
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, whereClause, null, 0, -1);
            Assert.assertNotNull("Retrieve by keyword: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by keyword: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by keyword: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by keyword: One of Trips should never has NULL ID!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.PUBLISH, tripFound.getPublish());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }        

    @Test
    public void testRetrieve_ByWhereClause_TripPrivacy_WithClientTransaction() throws Exception {
        //Create:
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            String whereClause = "where PRIVACY = " + TripPrivacyEnum.PUBLIC.getId() + " and USERID = '" + mUserCreated.getUserId() + "'";
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, whereClause, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by keyword: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by keyword: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by keyword: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by keyword: One of Trips should never has NULL ID!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.PUBLISH, tripFound.getPublish());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }        
    
    @Test
    public void testRetrieve_ByWhereClause_TripPublish_WithLocalTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            String whereClause = "where PUBLISH = " + TripPublishEnum.PUBLISH.getId() + " and USERID = '" + mUserCreated.getUserId() + "'";
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, whereClause, null, 0, -1);
            Assert.assertNotNull("Retrieve by keyword: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by keyword: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by keyword: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by keyword: One of Trips should never has NULL ID!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.PUBLISH, tripFound.getPublish());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testRetrieve_ByWhereClause_TripPublish_WithClientTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            String whereClause = "where PUBLISH = " + TripPublishEnum.PUBLISH.getId() + " and USERID = '" + mUserCreated.getUserId() + "'";
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, whereClause, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by keyword: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by keyword: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by keyword: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));

            Trip tripFound = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by keyword: One of Trips should never has NULL ID!", tripFound.getId());
            Assert.assertEquals("Found Trip: Invalid name!", CLASS_NAME, tripFound.getName());
            Assert.assertEquals("Found Trip: Invalid note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
            Assert.assertEquals("Found Trip: Invalid location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
            Assert.assertEquals("Found Trip: Invalid status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
            Assert.assertEquals("Found Trip: Invalid privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
            Assert.assertEquals("Found Trip: Invalid publish status!", TripPublishEnum.PUBLISH, tripFound.getPublish());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testRetrieve_WithOffsetAndLimit_WithLocalTransaction() throws Exception {
        for(int i=0; i<4; i++) {
            TestUtilForTrip.tripCreate_byDB(
                    mUserCreated.getUserId(), 
                    CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                    TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        }
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            int offset = 0;
            int limit = 2;
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(mUserCreated.getUserId());
            List<Trip> tripListFound = 
                    TripDBQuery.read(session, null, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, offset, limit);
            Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
            Assert.assertEquals("Incorrect number of Trips returned!", limit, tripListFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testRetrieve_WithOffsetAndLimit_WithClientTransaction() throws Exception {
        for(int i=0; i<4; i++) {
            TestUtilForTrip.tripCreate_byDB(
                    mUserCreated.getUserId(), 
                    CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                    TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        }
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            int offset = 0;
            int limit = 2;
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> tripListFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, offset, limit);
            transaction.commit();
            Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
            Assert.assertEquals("Incorrect number of Trips returned!", limit, tripListFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }    

    @Test
    public void testCount_ByStatusAndPrivacyAndPublish_WithLocalTransaction() throws Exception {
        String tripName_Active = CLASS_NAME + "_ACTIVE";
        String tripName_Private = CLASS_NAME + "_PRIVATE";
        String tripName_NotPublish = CLASS_NAME + "_NOTPUBLISH";
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Active, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Private, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_NotPublish, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            int numOfTrips = TripDBQuery.count(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count by User: Invalid returned number of Trips!", 3, numOfTrips);
            
            //Count 'ACTIVE' only:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(null);
            numOfTrips = TripDBQuery.count(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count 'ACTIVE' only by User: Invalid returned number of Trips!", 1, numOfTrips);        
            
            //Count 'PUBLIC' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PUBLIC);
            tripToRetrieveByUser.setPublish(null);
            numOfTrips = TripDBQuery.count(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count 'PUBLIC' only by User: Invalid returned number of Trips!", 2, numOfTrips);        

            //Count ALL including 'PRIVATE' Trips:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(null);
            numOfTrips = TripDBQuery.count(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count ALL to include 'PRIVATE' Trips by User: Invalid returned number of Trips!", 3, numOfTrips);
            
            //Count 'NOT PUBLISH' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            numOfTrips = TripDBQuery.count(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count 'NOT PUBLISH' only by User: Invalid returned number of Trips!", 1, numOfTrips);
            
            //Count ALL specified:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            numOfTrips = TripDBQuery.count(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count 'ALL specified' by User: Invalid returned number of Trips!", 0, numOfTrips);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testCount_ByStatusAndPrivacyAndPublish_WithClientTransaction() throws Exception {
        String tripName_Active = CLASS_NAME + "_ACTIVE";
        String tripName_Private = CLASS_NAME + "_PRIVATE";
        String tripName_NotPublish = CLASS_NAME + "_NOTPUBLISH";
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Active, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_Private, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName_NotPublish, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numOfTrips = TripDBQuery.count(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            transaction.commit();
            Assert.assertEquals("Count by User: Invalid returned number of Trips!", 3, numOfTrips);
            
            //Count 'ACTIVE' only:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            transaction = session.beginTransaction();
            numOfTrips = TripDBQuery.count(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            transaction.commit();
            Assert.assertEquals("Count 'ACTIVE' only by User: Invalid returned number of Trips!", 1, numOfTrips);        
            
            //Count 'PUBLIC' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PUBLIC);
            tripToRetrieveByUser.setPublish(null);
            transaction = session.beginTransaction();
            numOfTrips = TripDBQuery.count(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            transaction.commit();
            Assert.assertEquals("Count 'PUBLIC' only by User: Invalid returned number of Trips!", 2, numOfTrips);        

            //Count ALL including 'PRIVATE' Trips:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(null);
            transaction = session.beginTransaction();
            numOfTrips = TripDBQuery.count(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            Assert.assertEquals("Count ALL to include 'PRIVATE' Trips by User: Invalid returned number of Trips!", 3, numOfTrips);
            transaction.commit();

            //Count 'NOT PUBLISH' only:
            tripToRetrieveByUser.setStatus(null);
            tripToRetrieveByUser.setPrivacy(null);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            transaction = session.beginTransaction();
            numOfTrips = TripDBQuery.count(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            transaction.commit();
            Assert.assertEquals("Count 'NOT PUBLISH' only by User: Invalid returned number of Trips!", 1, numOfTrips);
            
            //Count ALL specified:
            tripToRetrieveByUser.setStatus(TripStatusEnum.ACTIVE);
            tripToRetrieveByUser.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToRetrieveByUser.setPublish(TripPublishEnum.NOT_PUBLISH);
            transaction = session.beginTransaction();
            numOfTrips = TripDBQuery.count(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null);
            transaction.commit();
            Assert.assertEquals("Count 'ALL specified' by User: Invalid returned number of Trips!", 0, numOfTrips);
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testUpdate_ALL_WithLocalTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripCreated = listOfTripsFound.get(0);
            
            //Update:
            Thread.sleep(1000); //Time gap due to dates sensitive validations
            String tripNameUpdated = CLASS_NAME + "-UPDATED";
            String tripNoteUpdated = TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED";
            String tripLocUpdated = TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED";
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(tripCreated.getUserID());
            tripToUpdate.setId(tripCreated.getId());
            tripToUpdate.setName(tripNameUpdated);
            tripToUpdate.setNote(tripNoteUpdated);
            tripToUpdate.setLocation(tripLocUpdated);
            tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            TripDBQuery.update(session, null, tripToUpdate, null);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
           
            //Validate:
            Trip tripToRetrieveByID = new Trip();
            tripToRetrieveByID.setUserID(mUserCreated.getUserId());
            tripToRetrieveByID.setId(tripCreated.getId());
            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByID, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripUpdated = listOfTripsFound.get(0);

            Assert.assertEquals("Trip after Update: Invalid name!", tripNameUpdated, tripUpdated.getName());
            Assert.assertEquals("Trip after Update: Invalid note!", tripNoteUpdated, tripUpdated.getNote());
            Assert.assertEquals("Trip after Update: Invalid location!", tripLocUpdated, tripUpdated.getLocation());
            Assert.assertEquals("Trip after Update: Invalid privacy value!", TripPrivacyEnum.PRIVATE, tripUpdated.getPrivacy());

            //Validate dates (special):
            Date createdDate = DateUtil.formatDate(tripUpdated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(tripUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's updated date should be AFTER created date!", 
                    updatedDate.after(createdDate));
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
 
    @Test
    public void testUpdate_ALL_WithClientTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripCreated = listOfTripsFound.get(0);
            
            //Update:
            Thread.sleep(1000); //Time gap due to dates sensitive validations
            String tripNameUpdated = CLASS_NAME + "-UPDATED";
            String tripNoteUpdated = TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED";
            String tripLocUpdated = TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED";
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(tripCreated.getUserID());
            tripToUpdate.setId(tripCreated.getId());
            tripToUpdate.setName(tripNameUpdated);
            tripToUpdate.setNote(tripNoteUpdated);
            tripToUpdate.setLocation(tripLocUpdated);
            tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            TripDBQuery.update(session, transaction, tripToUpdate, null);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
           
            //Validate:
            Trip tripToRetrieveByID = new Trip();
            tripToRetrieveByID.setUserID(mUserCreated.getUserId());
            tripToRetrieveByID.setId(tripCreated.getId());
            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByID, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripUpdated = listOfTripsFound.get(0);

            Assert.assertEquals("Trip after Update: Invalid name!", tripNameUpdated, tripUpdated.getName());
            Assert.assertEquals("Trip after Update: Invalid note!", tripNoteUpdated, tripUpdated.getNote());
            Assert.assertEquals("Trip after Update: Invalid location!", tripLocUpdated, tripUpdated.getLocation());
            Assert.assertEquals("Trip after Update: Invalid privacy value!", TripPrivacyEnum.PRIVATE, tripUpdated.getPrivacy());

            //Validate dates (special):
            Date createdDate = DateUtil.formatDate(tripUpdated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(tripUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's updated date should be AFTER created date!", 
                    updatedDate.after(createdDate));
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testUpdate_Status_WithLocalTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripCreated = listOfTripsFound.get(0);
            
            //Update:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(tripCreated.getUserID());
            tripToUpdate.setId(tripCreated.getId());
            tripToUpdate.setStatus(TripStatusEnum.COMPLETED);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            TripDBQuery.update(session, null, tripToUpdate, null);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(tripCreated.getUserID());
            tripToRetrieve.setId(tripCreated.getId());
           listOfTripsFound = 
                   TripDBQuery.read(session, null, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripUpdated = listOfTripsFound.get(0);
            Assert.assertEquals("Trip after Update: Invalid status value!", TripStatusEnum.COMPLETED, tripUpdated.getStatus());

            //Attempt to update status again:
            tripToUpdate.setStatus(TripStatusEnum.ACTIVE);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            TripDBQuery.update(session, null, tripToUpdate, null);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            Assert.assertEquals("Trip after Update: Status should not updateable once it is COMPLETED!", 
                    TripStatusEnum.COMPLETED, listOfTripsFound.get(0).getStatus());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }    
 
    @Test
    public void testUpdate_Status_WithClientTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripCreated = listOfTripsFound.get(0);
            
            //Update:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(tripCreated.getUserID());
            tripToUpdate.setId(tripCreated.getId());
            tripToUpdate.setStatus(TripStatusEnum.COMPLETED);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            TripDBQuery.update(session, transaction, tripToUpdate, null);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(tripCreated.getUserID());
            tripToRetrieve.setId(tripCreated.getId());
            transaction = session.beginTransaction();
            listOfTripsFound = 
                   TripDBQuery.read(session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripUpdated = listOfTripsFound.get(0);
            Assert.assertEquals("Trip after Update: Invalid status value!", TripStatusEnum.COMPLETED, tripUpdated.getStatus());

            //Attempt to update status again:
            tripToUpdate.setStatus(TripStatusEnum.ACTIVE);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            TripDBQuery.update(session, transaction, tripToUpdate, null);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            transaction = session.beginTransaction();
            listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            transaction.commit();
            Assert.assertEquals("Trip after Update: Status should not updateable once it is COMPLETED!", 
                    TripStatusEnum.COMPLETED, listOfTripsFound.get(0).getStatus());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }    
    
    @Test
    public void testUpdate_Publish_WithLocalTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripCreated = listOfTripsFound.get(0);
            
            //Update:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(tripCreated.getUserID());
            tripToUpdate.setId(tripCreated.getId());
            tripToUpdate.setPublish(TripPublishEnum.PUBLISH);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            TripDBQuery.update(session, null, tripToUpdate, null);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            //Validate (update publish is IGNORED!):
           listOfTripsFound = 
                   TripDBQuery.read(session, null, tripCreated, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripUpdated = listOfTripsFound.get(0);
            Assert.assertEquals("Trip after Update: Invalid publish value!", TripPublishEnum.NOT_PUBLISH, tripUpdated.getPublish());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }        
    
    @Test
    public void testUpdate_Publish_WithClientTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by User: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripCreated = listOfTripsFound.get(0);
            
            //Update:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(tripCreated.getUserID());
            tripToUpdate.setId(tripCreated.getId());
            tripToUpdate.setPublish(TripPublishEnum.PUBLISH);
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            TripDBQuery.update(session, transaction, tripToUpdate, null);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            //Validate (update publish is IGNORED!):
            transaction = session.beginTransaction();
            listOfTripsFound = 
                   TripDBQuery.read(session, transaction, tripCreated, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by TripID: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by TripID: Invalid number of found trips!", 1, listOfTripsFound.size());
            Assert.assertNotNull("Retrieve by TripID: Trip entity in the list should NOT be null!", listOfTripsFound.get(0));
            Trip tripUpdated = listOfTripsFound.get(0);
            Assert.assertEquals("Trip after Update: Invalid publish value!", TripPublishEnum.NOT_PUBLISH, tripUpdated.getPublish());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }        
    
    @Test
    public void testUpdate_StatusAllCompleted_WithLocalTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Update:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(mUserCreated.getUserId());
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            TripDBQuery.update(session, null, tripToUpdate, ServiceTypeEnum.TRIP_UPDATE_FOR_ALLSTATUSCOMPLETED);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, null, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());

            Trip tripCreated_1 = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by User: Trip 1 should NOT be null!", tripCreated_1);
            Assert.assertEquals("Trip after Update: Invalid Trip 1 status value!", TripStatusEnum.COMPLETED, tripCreated_1.getStatus());
            
            Trip tripCreated_2 = listOfTripsFound.get(1);
            Assert.assertNotNull("Retrieve by User: Trip 2 should NOT be null!", tripCreated_2);
            Assert.assertEquals("Trip after Update: Invalid Trip 2 status value!", TripStatusEnum.COMPLETED, tripCreated_2.getStatus());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }    
    
    @Test
    public void testUpdate_StatusAllCompleted_WithClientTransaction() throws Exception {
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.NOT_PUBLISH, false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                CLASS_NAME, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.ACTIVE, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH, false);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            //Update:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(mUserCreated.getUserId());
            tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            TripDBQuery.update(session, transaction, tripToUpdate, ServiceTypeEnum.TRIP_UPDATE_FOR_ALLSTATUSCOMPLETED);
            transaction.commit();
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Trip tripToRetrieveByUser = new Trip();
            tripToRetrieveByUser.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = 
                    TripDBQuery.read(session, transaction, tripToRetrieveByUser, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
            transaction.commit();
            Assert.assertNotNull("Retrieve by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Retrieve by User: Invalid number of found trips!", 2, listOfTripsFound.size());

            Trip tripCreated_1 = listOfTripsFound.get(0);
            Assert.assertNotNull("Retrieve by User: Trip 1 should NOT be null!", tripCreated_1);
            Assert.assertEquals("Trip after Update: Invalid Trip 1 status value!", TripStatusEnum.COMPLETED, tripCreated_1.getStatus());
            
            Trip tripCreated_2 = listOfTripsFound.get(1);
            Assert.assertNotNull("Retrieve by User: Trip 2 should NOT be null!", tripCreated_2);
            Assert.assertEquals("Trip after Update: Invalid Trip 2 status value!", TripStatusEnum.COMPLETED, tripCreated_2.getStatus());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }    
    
    /**
     * NOTE: Be aware, default search (without other filters) will retrieve ALL trips in the database 
     *       including existing non-test trips with default order CREATED desc. 
     *       Therefore, testing count, offset and num of rows could be tricky.
     */
    @Test
    public void testSearchAndCount_Default_WithLocalTransaction() throws Exception {
        int numberOfTrips = TestUtilForTrip.tripCreate_byDB_ManyTrips(mUserCreated.getUserId());

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //First Offset
            //1. Search:
            TripSearchCriteria searchCriteria = new TripSearchCriteria();
            List<Trip> listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Trip Search default - First Offset: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Trip Search default - First Offset: Invalid number of trips found!", 
                    CommonConstants.RECORDS_NUMBER_MAX, listOfTripsFound.size());
            Assert.assertEquals("Trip Search default - First Offset: Invalid Trip name for the FIRST Trip found!",  
                    TestConstants.UNITTEST_TRIP_NAME + "_LASTTRIP_CREATED", listOfTripsFound.get(0).getName());
            
            //2. Count: Be aware that search will return ANY public trip in DB 
            int numOfTrips = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertTrue("Trip Count default - First Offset: Incorrect number of trips found! Expected [" 
                    + numberOfTrips + "] but actual [" + numOfTrips + "]", 
                    numOfTrips >= numberOfTrips);
            
            //Second Offset and Limit:
            int numOfRowsLimit = numberOfTrips - CommonConstants.RECORDS_NUMBER_MAX;
            searchCriteria.setOffset(CommonConstants.RECORDS_NUMBER_MAX);
            searchCriteria.setNumOfRows(numOfRowsLimit);
            listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Trip Search default - Second Offset with limit: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Trip Search default - Second Offset with limit: Incorrect number of trips found!", numOfRowsLimit, listOfTripsFound.size());   
            Assert.assertEquals("Trip Search default - Second Offset with limit: Invalid Trip name for the LAST Trip found!",  
                    TestConstants.UNITTEST_TRIP_NAME + "_FIRSTTRIP_CREATED", listOfTripsFound.get(numOfRowsLimit - 1).getName());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testSearchAndCount_Default_WithClientTransaction() throws Exception {
        int numberOfTrips = TestUtilForTrip.tripCreate_byDB_ManyTrips(mUserCreated.getUserId());

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            TripSearchCriteria searchCriteria = new TripSearchCriteria();
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            transaction.commit();
            Assert.assertNotNull("Trip Search default - First Offset: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Trip Search default - First Offset: Invalid number of trips found!", 
                    CommonConstants.RECORDS_NUMBER_MAX, listOfTripsFound.size());
            Assert.assertEquals("Trip Search default - First Offset: Invalid Trip name for the FIRST Trip found!",  
                    TestConstants.UNITTEST_TRIP_NAME + "_LASTTRIP_CREATED", listOfTripsFound.get(0).getName());
            
            //Count:
            transaction = session.beginTransaction();
            int numOfTrips = TripDBQuery.count(session, transaction, searchCriteria);
            transaction.commit();
            Assert.assertTrue("Trip Count default - First Offset: Incorrect number of trips found! Expected [" 
                    + numberOfTrips + "] but actual [" + numOfTrips + "]", 
                    numOfTrips >= numberOfTrips);
            
            //Search with offset and limit:
            int numOfRowsLimit = numberOfTrips - CommonConstants.RECORDS_NUMBER_MAX;
            searchCriteria.setOffset(CommonConstants.RECORDS_NUMBER_MAX);
            searchCriteria.setNumOfRows(numOfRowsLimit);
            transaction = session.beginTransaction();
            listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            transaction.commit();
            Assert.assertNotNull("Trip Search default - Second Offset with limit: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Trip Search default - Second Offset with limit: Incorrect number of trips found!", numOfRowsLimit, listOfTripsFound.size());   
            Assert.assertEquals("Trip Search default - Second Offset with limit: Invalid Trip name for the LAST Trip found!",  
                    TestConstants.UNITTEST_TRIP_NAME + "_FIRSTTRIP_CREATED", listOfTripsFound.get(numOfRowsLimit - 1).getName());
            
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Test
    public void testSearchAndCount_WithCriteria_WithLocalTransaction() throws Exception {
        /*
         * NOTE:
         * 1. Be aware, default search (without other filters) will retrieve ALL trips in the database 
         *    including existing non-test trips with default order CREATED desc. 
         *    Therefore, testing offset and num of rows could be tricky.
         */
        String tripName = "testSearch_WithCriteria";
        String tripLocation = "Planet Pluto";
        TripStatusEnum tripStatus = TripStatusEnum.ACTIVE;
        String tripNameForStatus = "testSearch ACTIVEandCOMPLETE";
        String tripUsername = mUserCreated.getUsername();
        String tripUserFullname = mUserCreated.getFullname();
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                TestConstants.UNITTEST_TRIP_NAME, TestConstants.UNITTEST_TRIP_NOTE, tripLocation,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripNameForStatus, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                tripStatus, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripNameForStatus, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        
        //Search:
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            TripSearchCriteria searchCriteria = new TripSearchCriteria();
            searchCriteria.setTripKeyword(tripName);
            List<Trip> listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Search by criteria trip keyword [" + tripName + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria trip keyword [" + tripName + "]: Incorrect number of trips found!", 1, listOfTripsFound.size());
            Assert.assertEquals("Search by criteria trip keyword [" + tripName + "]: Unexpected Trip name!", tripName, listOfTripsFound.get(0).getName());
            int numOfTrip = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertEquals("Count by criteria trip keyword [" + tripName + "]: Incorrect number of trips found!", 1, numOfTrip);

            searchCriteria.clear();
            searchCriteria.setTripKeyword(tripLocation);
            listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Search by criteria trip keyword [" + tripLocation + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria trip keyword [" + tripLocation + "]: Incorrect number of trips found!", 1, listOfTripsFound.size());
            Assert.assertEquals("Search by criteria trip keyword [" + tripLocation + "]: Unexpected Trip location!", tripLocation, listOfTripsFound.get(0).getLocation());
            numOfTrip = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertEquals("Count by criteria trip keyword [" + tripLocation + "]: Incorrect number of trips found!", 1, numOfTrip);
        
            searchCriteria.clear();
            searchCriteria.setTripKeyword(tripNameForStatus);
            searchCriteria.setStatus(tripStatus);
            listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Search by criteria trip status: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria trip status: Incorrect number of trips found!", 1, listOfTripsFound.size());
            Assert.assertEquals("Search by criteria trip status: Unexpected Trip status!", tripStatus, listOfTripsFound.get(0).getStatus());
            numOfTrip = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertEquals("Count by criteria trip status: Incorrect number of trips found!", 1, numOfTrip);
            
            searchCriteria.clear();
            searchCriteria.setUserKeyword(tripUsername);
            listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Search by criteria user keyword [" + tripUsername + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria user keyword [" + tripUsername + "]: Incorrect number of trips found!", 4, listOfTripsFound.size());
            numOfTrip = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertEquals("Count by criteria user keyword [" + tripUsername + "]: Incorrect number of trips found!", 4, numOfTrip);
            
            searchCriteria.clear();
            searchCriteria.setUserKeyword(tripUserFullname);
            listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Search by criteria user keyword [" + tripUserFullname + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria user keyword [" + tripUserFullname + "]: Incorrect number of trips found!", 4, listOfTripsFound.size());
            numOfTrip = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertEquals("Count by criteria user keyword [" + tripUserFullname + "]: Incorrect number of trips found!", 4, numOfTrip);
            
            searchCriteria.clear();
            searchCriteria.setTripKeyword(tripName);
            searchCriteria.setUserKeyword(tripUsername);
            searchCriteria.setStatus(tripStatus);
            listOfTripsFound = TripDBQuery.search(session, null, searchCriteria);
            Assert.assertNotNull("Search by criteria ALL: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria ALL: Incorrect number of trips found!", 0, listOfTripsFound.size());
            numOfTrip = TripDBQuery.count(session, null, searchCriteria);
            Assert.assertEquals("Count by criteria ALL: Incorrect number of trips found!", 0, numOfTrip);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }        
    
    @Test
    public void testSearchAndCount_WithCriteria_WithClientTransaction() throws Exception {
        /*
         * NOTE:
         * 1. Be aware, default search (without other filters) will retrieve ALL trips in the database 
         *    including existing non-test trips with default order CREATED desc. 
         *    Therefore, testing offset and num of rows could be tricky.
         */
        String tripName = "testSearch_WithCriteria";
        String tripLocation = "Planet Pluto";
        TripStatusEnum tripStatus = TripStatusEnum.ACTIVE;
        String tripNameForStatus = "testSearch ACTIVEandCOMPLETE";
        String tripUsername = mUserCreated.getUsername();
        String tripUserFullname = mUserCreated.getFullname();
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripName, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                TestConstants.UNITTEST_TRIP_NAME, TestConstants.UNITTEST_TRIP_NOTE, tripLocation,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripNameForStatus, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                tripStatus, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        TestUtilForTrip.tripCreate_byDB(
                mUserCreated.getUserId(), 
                tripNameForStatus, TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
                TripStatusEnum.COMPLETED, TripPrivacyEnum.PUBLIC, TripPublishEnum.PUBLISH,
                false);
        
        //Search:
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            TripSearchCriteria searchCriteria = new TripSearchCriteria();
            searchCriteria.setTripKeyword(tripName);
            transaction = session.beginTransaction();
            List<Trip> listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            Assert.assertNotNull("Search by criteria trip keyword [" + tripName + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria trip keyword [" + tripName + "]: Incorrect number of trips found!", 1, listOfTripsFound.size());
            Assert.assertEquals("Search by criteria trip keyword [" + tripName + "]: Unexpected Trip name!", tripName, listOfTripsFound.get(0).getName());
            int numOfTrip = TripDBQuery.count(session, transaction, searchCriteria);
            Assert.assertEquals("Count by criteria trip keyword [" + tripName + "]: Incorrect number of trips found!", 1, numOfTrip);
            transaction.commit();

            searchCriteria.clear();
            searchCriteria.setTripKeyword(tripLocation);
            transaction = session.beginTransaction();
            listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            Assert.assertNotNull("Search by criteria trip keyword [" + tripLocation + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria trip keyword [" + tripLocation + "]: Incorrect number of trips found!", 1, listOfTripsFound.size());
            Assert.assertEquals("Search by criteria trip keyword [" + tripLocation + "]: Unexpected Trip location!", tripLocation, listOfTripsFound.get(0).getLocation());
            numOfTrip = TripDBQuery.count(session, transaction, searchCriteria);
            Assert.assertEquals("Count by criteria trip keyword [" + tripLocation + "]: Incorrect number of trips found!", 1, numOfTrip);
            transaction.commit();
        
            searchCriteria.clear();
            searchCriteria.setTripKeyword(tripNameForStatus);
            searchCriteria.setStatus(tripStatus);
            transaction = session.beginTransaction();
            listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            Assert.assertNotNull("Search by criteria trip status: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria trip status: Incorrect number of trips found!", 1, listOfTripsFound.size());
            Assert.assertEquals("Search by criteria trip status: Unexpected Trip status!", tripStatus, listOfTripsFound.get(0).getStatus());
            numOfTrip = TripDBQuery.count(session, transaction, searchCriteria);
            Assert.assertEquals("Count by criteria trip status: Incorrect number of trips found!", 1, numOfTrip);
            transaction.commit();
            
            searchCriteria.clear();
            searchCriteria.setUserKeyword(tripUsername);
            transaction = session.beginTransaction();
            listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            Assert.assertNotNull("Search by criteria user keyword [" + tripUsername + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria user keyword [" + tripUsername + "]: Incorrect number of trips found!", 4, listOfTripsFound.size());
            numOfTrip = TripDBQuery.count(session, transaction, searchCriteria);
            Assert.assertEquals("Count by criteria user keyword [" + tripUsername + "]: Incorrect number of trips found!", 4, numOfTrip);
            transaction.commit();
            
            searchCriteria.clear();
            searchCriteria.setUserKeyword(tripUserFullname);
            transaction = session.beginTransaction();
            listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            Assert.assertNotNull("Search by criteria user keyword [" + tripUserFullname + "]: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria user keyword [" + tripUserFullname + "]: Incorrect number of trips found!", 4, listOfTripsFound.size());
            numOfTrip = TripDBQuery.count(session, transaction, searchCriteria);
            Assert.assertEquals("Count by criteria user keyword [" + tripUserFullname + "]: Incorrect number of trips found!", 4, numOfTrip);
            transaction.commit();
            
            searchCriteria.clear();
            searchCriteria.setTripKeyword(tripName);
            searchCriteria.setUserKeyword(tripUsername);
            searchCriteria.setStatus(tripStatus);
            transaction = session.beginTransaction();
            listOfTripsFound = TripDBQuery.search(session, transaction, searchCriteria);
            Assert.assertNotNull("Search by criteria ALL: List of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Search by criteria ALL: Incorrect number of trips found!", 0, listOfTripsFound.size());
            numOfTrip = TripDBQuery.count(session, transaction, searchCriteria);
            Assert.assertEquals("Count by criteria ALL: Incorrect number of trips found!", 0, numOfTrip);
            transaction.commit();

        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }        
    
    
}
