package test.com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestUtilForFavorite;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.IFavoriteDAO;
import com.trekcrumb.business.dao.impl.hibernate.FavoriteDAOHibernateImpl;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;

public class FavoriteDAOHibernateImplTest {
    private static final String CLASS_NAME = "FavoriteDAOHibernateImplTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mTripCreated = null;
        mUserCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForFavorite.faveDeleteALLbyFavoriteDB(mUserCreated.getUserId());
    }

    @Test
    public void testCreateAndReadAndDelete() throws Exception {
        IFavoriteDAO faveDAOImpl = new FavoriteDAOHibernateImpl();

        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        faveDAOImpl.create(faveToCreate);
        
        //Retrieve:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(mTripCreated.getId());
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        List<Favorite> listOfFavesFound = faveDAOImpl.read(faveToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
        Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
        Assert.assertNotNull("Favorite Retrieve: Favorite entity in the list should NOT be null!", listOfFavesFound.get(0));
        Favorite faveFound = listOfFavesFound.get(0);
        Assert.assertEquals("Favorite Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), faveFound.getTripID());
        Assert.assertEquals("Favorite Retrieve: Invalid returned User ID!", mUserCreated.getUserId(), faveFound.getUserID());
        Assert.assertNotNull("Favorite Retrieve: Returned created date shall not be null!", faveFound.getCreated());

        //Delete:
        Favorite faveToDelete = new Favorite();
        faveToDelete.setTripID(mTripCreated.getId());
        faveToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = faveDAOImpl.delete(faveToDelete);
        Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 1, numOfDeleted);

        //Retrieve after delete:
        listOfFavesFound = faveDAOImpl.read(faveToRetrieve, null, -1, -1);
        Assert.assertNull("Favorite Retrieve after Delete: Returned list of favorites should be NULL!", listOfFavesFound);
    }
    

}
