package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.DateUtil;

public class UserDBQueryTest {
    private static final String CLASS_NAME = "UserDBQueryTest";
    
    private User mUserCreated;

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForUser.userDelete_byDB(mUserCreated);
        mUserCreated = null;
    }
    
    @Test
    public void testCreateReadAndDelete_WithLocalTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testCreateReadAndDelete_WithLocalTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by username:
            User userToReadByUsername = new User();
            userToReadByUsername.setUsername(username);
            List<User> foundUsers = UserDBQuery.read(session, null, userToReadByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, foundUsers);
            mUserCreated = foundUsers.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, mUserCreated);
            Assert.assertNotNull("User ID should have not been NULL!", mUserCreated.getUserId());
            Assert.assertEquals("Incorrect user email address!", email, mUserCreated.getEmail());
            Assert.assertEquals("Incorrect password!", TestConstants.UNITTEST_USER_PASSWORD, mUserCreated.getPassword());
            Assert.assertEquals("Incorrect fullname!", fullname, mUserCreated.getFullname());
            Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, mUserCreated.getActive());
            Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, mUserCreated.getConfirmed());

            //Delete by UserID:
            User userToDelete = new User();;
            userToDelete.setUserId(mUserCreated.getUserId());
            int numDeleted = UserDBQuery.delete(session, null, userToDelete, ServiceTypeEnum.USER_DELETE_BY_USERID);
            Assert.assertEquals("User Delete: Incorrect number record deleted!", 1, numDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreateReadAndDelete_WithClientTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testCreateReadAndDelete_WithClientTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by username:
            User userToReadByUsername = new User();
            userToReadByUsername.setUsername(username);
            transaction = session.beginTransaction();
            List<User> foundUsers = UserDBQuery.read(session, transaction, userToReadByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            transaction.commit();
            
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, foundUsers);
            mUserCreated = foundUsers.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, mUserCreated);
            Assert.assertNotNull("User ID should have not been NULL!", mUserCreated.getUserId());
            Assert.assertEquals("Incorrect user email address!", email, mUserCreated.getEmail());
            Assert.assertEquals("Incorrect password!", TestConstants.UNITTEST_USER_PASSWORD, mUserCreated.getPassword());
            Assert.assertEquals("Incorrect fullname!", fullname, mUserCreated.getFullname());
            Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, mUserCreated.getActive());
            Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, mUserCreated.getConfirmed());

            //Delete by UserID:
            User userToDelete = new User();;
            userToDelete.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numDeleted = UserDBQuery.delete(session, transaction, userToDelete, ServiceTypeEnum.USER_DELETE_BY_USERID);
            transaction.commit();
            
            Assert.assertEquals("User Delete: Incorrect number record deleted!", 1, numDeleted);
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testRead_ByUserID_WithLocalTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testRead_ByUserID_WithLocalTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            User userToFindByUsername = new User();
            userToFindByUsername.setUsername(username);
            List<User> foundUsers = UserDBQuery.read(session, null, userToFindByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, foundUsers);
            Assert.assertEquals("DB should have NOT returned NULL for user with username: " + username, 1, foundUsers.size());
            mUserCreated = foundUsers.get(0);

            //Retrieve by UserId:
            User userToFindById = new User();
            userToFindById.setUserId(mUserCreated.getUserId());
            foundUsers = UserDBQuery.read(session, null, userToFindById, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1);
            Assert.assertNotNull("DB should have NOT returned NULL for user with userid: " + mUserCreated.getUserId(), foundUsers);
            mUserCreated = foundUsers.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with userid: " + mUserCreated.getUserId(), mUserCreated);
            Assert.assertNotNull("User ID should have not been NULL!", mUserCreated.getUserId());
            Assert.assertEquals("Incorrect user email address!", email, mUserCreated.getEmail());
            Assert.assertEquals("Incorrect fullname!", fullname, mUserCreated.getFullname());
            Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, mUserCreated.getActive());
            Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, mUserCreated.getConfirmed());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    public void testRead_ByUserID_WithClientTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testRead_ByUserID_WithClientTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            User userToFindByUsername = new User();
            userToFindByUsername.setUsername(username);
            transaction = session.beginTransaction();
            List<User> foundUsers = UserDBQuery.read(session, transaction, userToFindByUsername, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            transaction.commit();
            
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, foundUsers);
            Assert.assertEquals("DB should have NOT returned NULL for user with username: " + username, 1, foundUsers.size());
            mUserCreated = foundUsers.get(0);

            //Retrieve by UserId:
            User userToFindById = new User();
            userToFindById.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            foundUsers = UserDBQuery.read(session, transaction, userToFindById, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1);
            transaction.commit();
            
            Assert.assertNotNull("DB should have NOT returned NULL for user with userid: " + mUserCreated.getUserId(), foundUsers);
            mUserCreated = foundUsers.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with userid: " + mUserCreated.getUserId(), mUserCreated);
            Assert.assertNotNull("User ID should have not been NULL!", mUserCreated.getUserId());
            Assert.assertEquals("Incorrect user email address!", email.toUpperCase(), mUserCreated.getEmail());
            Assert.assertEquals("Incorrect fullname!", fullname, mUserCreated.getFullname());
            Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, mUserCreated.getActive());
            Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, mUserCreated.getConfirmed());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    

    @Test
    public void testRead_ByEmail_WithLocalTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testRead_ByEmail_WithLocalTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by email:
            User userToFindByEmail = new User();
            userToFindByEmail.setEmail(email);
            List<User> foundUsers = UserDBQuery.read(session, null, userToFindByEmail, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, null, 0, 1);
            Assert.assertNotNull("DB should have NOT returned NULL for user with email: " + email, foundUsers);
            Assert.assertEquals("DB should have NOT returned NULL for user with email: " + email, 1, foundUsers.size());
            mUserCreated = foundUsers.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with email: " + email, mUserCreated);
            Assert.assertNotNull("User ID should have not been NULL!", mUserCreated.getUserId());
            Assert.assertEquals("Incorrect user email address!", email, mUserCreated.getEmail());
            Assert.assertEquals("Incorrect fullname!", fullname, mUserCreated.getFullname());
            Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, mUserCreated.getActive());
            Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, mUserCreated.getConfirmed());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testRead_ByEmail_WithClientTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testRead_ByEmail_WithClientTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by email:
            User userToFindByEmail = new User();
            userToFindByEmail.setEmail(email);
            transaction = session.beginTransaction();
            List<User> foundUsers = UserDBQuery.read(session, transaction, userToFindByEmail, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, null, 0, 1);
            transaction.commit();
            
            Assert.assertNotNull("DB should have NOT returned NULL for user with email: " + email, foundUsers);
            Assert.assertEquals("DB should have NOT returned NULL for user with email: " + email, 1, foundUsers.size());
            mUserCreated = foundUsers.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with email: " + email, mUserCreated);
            Assert.assertNotNull("User ID should have not been NULL!", mUserCreated.getUserId());
            Assert.assertEquals("Incorrect user email address!", email, mUserCreated.getEmail());
            Assert.assertEquals("Incorrect fullname!", fullname, mUserCreated.getFullname());
            Assert.assertEquals("Incorrect active status!", YesNoEnum.YES, mUserCreated.getActive());
            Assert.assertEquals("Incorrect confirmed status!", YesNoEnum.NO, mUserCreated.getConfirmed());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testUpdate_ALL_WithLocalTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testUpdateUser_ALL_WithLocalTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        String userID = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            User userToFind = new User();
            userToFind.setUsername(username);
            List<User> userFoundList = UserDBQuery.read(session, null, userToFind, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userFoundList);
            Assert.assertEquals("DB should have NOT returned NULL for user with username: " + username, 1, userFoundList.size());
            mUserCreated = userFoundList.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, mUserCreated);
            userID = mUserCreated.getUserId();
            
            //Update:
            Thread.sleep(1000); //Time gap due to dates sensitive validations
            String userUpdatedUsername = "usernameShouldNotEditable";
            String userUpdatedEmail = "newUpdatedEmail" + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String updatedPassword = "newUpdatedPassword";
            String updatedFullname = "Updated testUpdateUser_ALL";
            String updatedSummary = "Updated summary, how ya doin?";
            String updatedLocation = "Cardiff by the Sea, California, USA";
            
            User userToUpdate = new User();
            userToUpdate.setUserId(userID);
            userToUpdate.setUsername(userUpdatedUsername);
            userToUpdate.setEmail(userUpdatedEmail);
            userToUpdate.setPassword(updatedPassword);
            userToUpdate.setActive(YesNoEnum.NO);
            userToUpdate.setConfirmed(YesNoEnum.YES);
            userToUpdate.setFullname(updatedFullname);
            userToUpdate.setSummary(updatedSummary);
            userToUpdate.setLocation(updatedLocation);
            int updatedItem = UserDBQuery.update(session, null, userToUpdate);
            Assert.assertEquals("Incorrect number of updated items!", 1, updatedItem);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Verify:
            List<User> userFoundAfterUpdateList = UserDBQuery.read(session, null, userToFind, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userFoundAfterUpdateList);
            User userAfterUpdate = userFoundAfterUpdateList.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userAfterUpdate);
            Assert.assertNotNull("DB should have returned User with username: " + username, userAfterUpdate);
            Assert.assertEquals("After update: Incorrect UserID!", userID, userAfterUpdate.getUserId());
            Assert.assertEquals("After update: Username should NOT changed nor editable!", username, userAfterUpdate.getUsername());
            Assert.assertEquals("After update: Incorrect user email address!", userUpdatedEmail, userAfterUpdate.getEmail());
            Assert.assertEquals("After update: Incorrect password!", updatedPassword, userAfterUpdate.getPassword());
            Assert.assertEquals("After update: Incorrect fullname!", updatedFullname, userAfterUpdate.getFullname());
            Assert.assertEquals("After update: Incorrect active status after update!", YesNoEnum.NO, userAfterUpdate.getActive());
            Assert.assertEquals("After update: Incorrect confirmed status after update!", YesNoEnum.YES, userAfterUpdate.getConfirmed());
            Assert.assertEquals("After update: Incorrect summary after update!",updatedSummary, userAfterUpdate.getSummary());
            Assert.assertEquals("After update: Incorrect Location after update!", updatedLocation, userAfterUpdate.getLocation());
            
            //Validate dates (special):
            Date createdDate = DateUtil.formatDate(userAfterUpdate.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(userAfterUpdate.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's updated date should have been AFTER created date!", 
                    updatedDate.after(createdDate));
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testUpdate_ALL_WithClientTransaction() throws Exception {
        String username = String.valueOf(System.currentTimeMillis());
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testUpdate_ALL_WithClientTransaction";
        TestUtilForUser.userCreate_byDB(email, username, fullname);

        String userID = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            User userToFind = new User();
            userToFind.setUsername(username);
            transaction = session.beginTransaction();
            List<User> userFoundList = UserDBQuery.read(session, transaction, userToFind, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            transaction.commit();
            
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userFoundList);
            Assert.assertEquals("DB should have NOT returned NULL for user with username: " + username, 1, userFoundList.size());
            mUserCreated = userFoundList.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, mUserCreated);
            userID = mUserCreated.getUserId();
            
            //Update:
            Thread.sleep(1000); //Time gap due to dates sensitive validations
            String userUpdatedUsername = "usernameShouldNotEditable";
            String userUpdatedEmail = "newUpdatedEmail" + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String updatedPassword = "newUpdatedPassword";
            String updatedFullname = "Updated testUpdateUser_ALL";
            String updatedSummary = "Updated summary, how ya doin?";
            String updatedLocation = "Cardiff by the Sea, California, USA";
            
            User userToUpdate = new User();
            userToUpdate.setUserId(userID);
            userToUpdate.setUsername(userUpdatedUsername);
            userToUpdate.setEmail(userUpdatedEmail);
            userToUpdate.setPassword(updatedPassword);
            userToUpdate.setActive(YesNoEnum.NO);
            userToUpdate.setConfirmed(YesNoEnum.YES);
            userToUpdate.setFullname(updatedFullname);
            userToUpdate.setSummary(updatedSummary);
            userToUpdate.setLocation(updatedLocation);
            transaction = session.beginTransaction();
            int updatedItem = UserDBQuery.update(session, transaction, userToUpdate);
            transaction.commit();
            Assert.assertEquals("Incorrect number of updated items!", 1, updatedItem);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Verify:
            transaction = session.beginTransaction();
            List<User> userFoundAfterUpdateList = UserDBQuery.read(session, transaction, userToFind, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
            transaction.commit();
            
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userFoundAfterUpdateList);
            User userAfterUpdate = userFoundAfterUpdateList.get(0);
            Assert.assertNotNull("DB should have NOT returned NULL for user with username: " + username, userAfterUpdate);
            Assert.assertNotNull("DB should have returned User with username: " + username, userAfterUpdate);
            Assert.assertEquals("After update: Incorrect UserID!", userID, userAfterUpdate.getUserId());
            Assert.assertEquals("After update: Username should NOT changed nor editable!", username, userAfterUpdate.getUsername());
            Assert.assertEquals("After update: Incorrect user email address!", userUpdatedEmail, userAfterUpdate.getEmail());
            Assert.assertEquals("After update: Incorrect password!", updatedPassword, userAfterUpdate.getPassword());
            Assert.assertEquals("After update: Incorrect fullname!", updatedFullname, userAfterUpdate.getFullname());
            Assert.assertEquals("After update: Incorrect active status after update!", YesNoEnum.NO, userAfterUpdate.getActive());
            Assert.assertEquals("After update: Incorrect confirmed status after update!", YesNoEnum.YES, userAfterUpdate.getConfirmed());
            Assert.assertEquals("After update: Incorrect summary after update!",updatedSummary, userAfterUpdate.getSummary());
            Assert.assertEquals("After update: Incorrect Location after update!", updatedLocation, userAfterUpdate.getLocation());
            
            //Validate dates (special):
            Date createdDate = DateUtil.formatDate(userAfterUpdate.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date updatedDate = DateUtil.formatDate(userAfterUpdate.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Record's updated date should have been AFTER created date!", 
                    updatedDate.after(createdDate));
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    

}
