package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForComment;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQuery;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public class CommentDBQueryTest {
    private static final String CLASS_NAME = "CommentDBQueryTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mTripCreated = null;
        mUserCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForComment.commentDeletebyCommentDB(mUserCreated.getUserId());
    }

    @Test
    public void testCreateAndReadAndDelete_byCommentID_WithLocalTransaction() throws Exception {
        String commentID = TestUtilForComment.commentCreateByCommentDB(
                mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        Session session = null;
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentID);
        try {
            session = HibernateManager.openSession();
            List<Comment> listOfComments = CommentDBQuery.read(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
            Assert.assertNotNull("Comment Retrieve: Comment entity in the list should NOT be null!", listOfComments.get(0));
            Comment commentFound = listOfComments.get(0);
            Assert.assertEquals("Comment Retrieve: Invalid returned Comment ID!", commentID, commentFound.getId());
            Assert.assertEquals("Comment Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
            Assert.assertEquals("Comment Retrieve: Invalid returned User ID!", mUserCreated.getUserId(), commentFound.getUserID());
            Assert.assertEquals("Comment Retrieve: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
            Assert.assertNotNull("Comment Retrieve: Returned created date shall not be null!", commentFound.getCreated());
            
            //Enrichment data:
            Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
            Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
            Assert.assertEquals("Comment Retrieve: Invalid returned Trip Name!", mTripCreated.getName(), commentFound.getTripName());
           
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
        
        //Delete:
        try {
            session = HibernateManager.openSession();
            Comment commentToDelete = new Comment();
            commentToDelete.setId(commentID);
            commentToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = CommentDBQuery.delete(session, null, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
            
        //Retrieve and verify after delete:
        try {
            session = HibernateManager.openSession();
            List<Comment> listOfComments = CommentDBQuery.read(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve after Delete: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve after Delete: Invalid number of found Comments!", 0, listOfComments.size());
         } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreateAndReadAndDelete_byUser_WithLocalTransaction() throws Exception {
        String commentID = TestUtilForComment.commentCreateByCommentDB(
                mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        Session session = null;
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setUsername(mUserCreated.getUsername());
        try {
            session = HibernateManager.openSession();
            List<Comment> listOfComments = CommentDBQuery.read(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
            Assert.assertNotNull("Comment Retrieve: Comment entity in the list should NOT be null!", listOfComments.get(0));
            Comment commentFound = listOfComments.get(0);
            Assert.assertEquals("Comment Retrieve: Invalid returned Comment ID!", commentID, commentFound.getId());
            Assert.assertEquals("Comment Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
            Assert.assertEquals("Comment Retrieve: Invalid returned User ID!", mUserCreated.getUserId(), commentFound.getUserID());
            Assert.assertEquals("Comment Retrieve: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
            Assert.assertNotNull("Comment Retrieve: Returned created date shall not be null!", commentFound.getCreated());
            
            //Enrichment data:
            Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
            Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
            Assert.assertEquals("Comment Retrieve: Invalid returned Trip Name!", mTripCreated.getName(), commentFound.getTripName());
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }

        //Count:
        try {
            session = HibernateManager.openSession();
            int numOfComments = CommentDBQuery.count(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, numOfComments);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
        
        //Delete:
        try {
            session = HibernateManager.openSession();
            Comment commentToDelete = new Comment();
            commentToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = CommentDBQuery.delete(session, null, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
            
        //Retrieve and verify after delete:
        try {
            session = HibernateManager.openSession();
            List<Comment> listOfComments = CommentDBQuery.read(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve after Delete: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve after Delete: Invalid number of found Comments!", 0, listOfComments.size());
            
            int numOfComments = CommentDBQuery.count(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve after Delete: Invalid number of found Comments!", 0, numOfComments);
         } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testCreateAndReadAndDelete_byTripID_WithLocalTransaction() throws Exception {
        String commentID = TestUtilForComment.commentCreateByCommentDB(
                mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        Session session = null;
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setTripID(mTripCreated.getId());
        try {
            session = HibernateManager.openSession();
            List<Comment> listOfComments = CommentDBQuery.read(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
            Assert.assertNotNull("Comment Retrieve: Comment entity in the list should NOT be null!", listOfComments.get(0));
            Comment commentFound = listOfComments.get(0);
            Assert.assertEquals("Comment Retrieve: Invalid returned Comment ID!", commentID, commentFound.getId());
            Assert.assertEquals("Comment Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
            Assert.assertEquals("Comment Retrieve: Invalid returned User ID!", mUserCreated.getUserId(), commentFound.getUserID());
            Assert.assertEquals("Comment Retrieve: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
            Assert.assertNotNull("Comment Retrieve: Returned created date shall not be null!", commentFound.getCreated());
            
            //Enrichment data:
            Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
            Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
            Assert.assertEquals("Comment Retrieve: Invalid returned Trip Name!", mTripCreated.getName(), commentFound.getTripName());
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }

        //Count:
        try {
            session = HibernateManager.openSession();
            int numOfComments = CommentDBQuery.count(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, numOfComments);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
 
        //Delete:
        try {
            session = HibernateManager.openSession();
            Comment commentToDelete = new Comment();
            commentToDelete.setTripID(mTripCreated.getId());
            int numOfDeleted = CommentDBQuery.delete(session, null, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
            
        //Retrieve and verify after delete:
        try {
            session = HibernateManager.openSession();
            List<Comment> listOfComments = CommentDBQuery.read(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve after Delete: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve after Delete: Invalid number of found Comments!", 0, listOfComments.size());

            int numOfComments = CommentDBQuery.count(session, null, commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID);
            Assert.assertEquals("Comment Retrieve after Delete: Invalid number of found Comments!", 0, numOfComments);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
}
