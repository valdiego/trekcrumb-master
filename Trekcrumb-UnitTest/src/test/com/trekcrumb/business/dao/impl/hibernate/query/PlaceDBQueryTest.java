package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPlace;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQuery;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.DateUtil;

public class PlaceDBQueryTest {
    private static final String CLASS_NAME = "PlaceDBQueryTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mTripCreated = null;
        mUserCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
    }

    @Test
    public void testCreateAndReadAndDelete_WithLocalTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 1, listOfPlacesFound.size());
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertNotNull("Found Place: Found Place ID should NOT be null!", placeFound.getId());
            Assert.assertEquals("Found Place: Invalid Trip ID!", mTripCreated.getId(), placeFound.getTripID());
            Assert.assertEquals("Found Place: Invalid latitude!", TestConstants.UNITTEST_PLACE_LATITUDE, placeFound.getLatitude());
            Assert.assertEquals("Found Place: Invalid longitude!", TestConstants.UNITTEST_PLACE_LONGITUDE, placeFound.getLongitude());
            Assert.assertEquals("Found Place: Invalid note!", TestConstants.UNITTEST_PLACE_NOTE, placeFound.getNote());
            Assert.assertEquals("Found Place: Invalid location!", TestConstants.UNITTEST_PLACE_LOCATION, placeFound.getLocation());
            Assert.assertNotNull("Found Place: Created date shall not be null!", placeFound.getCreated());

            //Delete by ID:
            Place placeToDelete = new Place();
            placeToDelete.setId(placeFound.getId());
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = PlaceDBQuery.delete(session, null, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 1, numOfDeleted);
            
            listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Delete Place: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Delete Place: Invalid number of found places!", 0, listOfPlacesFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreateAndReadAndDelete_WithClientTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 1, listOfPlacesFound.size());
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertNotNull("Found Place: Found Place ID should NOT be null!", placeFound.getId());
            Assert.assertEquals("Found Place: Invalid Trip ID!", mTripCreated.getId(), placeFound.getTripID());
            Assert.assertEquals("Found Place: Invalid latitude!", TestConstants.UNITTEST_PLACE_LATITUDE, placeFound.getLatitude());
            Assert.assertEquals("Found Place: Invalid longitude!", TestConstants.UNITTEST_PLACE_LONGITUDE, placeFound.getLongitude());
            Assert.assertEquals("Found Place: Invalid note!", TestConstants.UNITTEST_PLACE_NOTE, placeFound.getNote());
            Assert.assertEquals("Found Place: Invalid location!", TestConstants.UNITTEST_PLACE_LOCATION, placeFound.getLocation());
            Assert.assertNotNull("Found Place: Created date shall not be null!", placeFound.getCreated());

            //Delete by ID:
            Place placeToDelete = new Place();
            placeToDelete.setId(placeFound.getId());
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numOfDeleted = PlaceDBQuery.delete(session, transaction, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            transaction.commit();
            Assert.assertEquals("Delete Trip: Invalid number of deleted Trip!", 1, numOfDeleted);
            
            //Validate:
            transaction = session.beginTransaction();
            listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Delete Place: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Delete Place: Invalid number of found places!", 0, listOfPlacesFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreate_LocationEmpty() throws Exception {
        String locationEmpty = null;
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, locationEmpty);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 1, listOfPlacesFound.size());
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertNotNull("Found Place: Found Place ID should NOT be null!", placeFound.getId());
            Assert.assertNotNull("Found Place: Location should have NOT be NULL!", placeFound.getLocation());
            Assert.assertEquals("Found Place: Invalid Location!", 
                    "[" + placeFound.getLatitude() + "," + placeFound.getLongitude() + "]", 
                    placeFound.getLocation());

        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    

    @Test
    public void testReadAndDelete_ByTripID_WithLocalTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by TripID
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 2, listOfPlacesFound.size());
            Assert.assertNotNull("Retrieve by Trip: Place entity [0] in the list should NOT be null!", listOfPlacesFound.get(0));
            Assert.assertNotNull("Retrieve by Trip: Place entity [1] in the list should NOT be null!", listOfPlacesFound.get(1));

            //Delete:
            Place placeToDelete = new Place();
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = PlaceDBQuery.delete(session, null, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_TRIPID);
            Assert.assertEquals("Delete Place: Invalid number of deleted Places!", 2, numOfDeleted);
            
            //Verify:
            listOfPlacesFound = PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Delete Place: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Delete Place: Invalid number of found places!", 0, listOfPlacesFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByTripID_WithClientTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by TripID
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 2, listOfPlacesFound.size());
            Assert.assertNotNull("Retrieve by Trip: Place entity [0] in the list should NOT be null!", listOfPlacesFound.get(0));
            Assert.assertNotNull("Retrieve by Trip: Place entity [1] in the list should NOT be null!", listOfPlacesFound.get(1));

            //Delete:
            Place placeToDelete = new Place();
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numOfDeleted = PlaceDBQuery.delete(session, transaction, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_TRIPID);
            transaction.commit();
            Assert.assertEquals("Delete Place: Invalid number of deleted Places!", 2, numOfDeleted);
            
            //Verify:
            transaction = session.beginTransaction();
            listOfPlacesFound = PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Delete Place: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Delete Place: Invalid number of found places!", 0, listOfPlacesFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByPlaceID_WithLocalTransaction() throws Exception {
        double latitude_1 = 32.76243;
        double longitude_1 = -117.14818;
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                latitude_1, longitude_1,  
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Thread.sleep(1000); //Time gap due to dates sensitive validations
        double latitude_2 = 32.75142;
        double longitude_2 = -117.15112;
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                latitude_2, longitude_2,  
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by TripID
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 2, listOfPlacesFound.size());

            //DB query is sorted by CREATED ascending:
            Place placeFound_1 = listOfPlacesFound.get(0);
            Assert.assertNotNull("Retrieve by Trip: The 1st found Place in List should NOT null!", placeFound_1);
            Assert.assertNotNull("Retrieve by Trip: The 1st found Place in List should never has NULL ID!", placeFound_1.getId());
            Assert.assertEquals("Retrieve by Trip: Invalid 1st latitude!", latitude_1, placeFound_1.getLatitude());
            Assert.assertEquals("Retrieve by Trip: Invalid 1st longitude!", longitude_1, placeFound_1.getLongitude());

            Place placeFound_2 = listOfPlacesFound.get(1);
            Assert.assertNotNull("Retrieve by Trip: The 2nd found Place in List should NOT null!", placeFound_2);
            Assert.assertNotNull("Retrieve by Trip: The 2nd found Place in List should never has NULL ID!", placeFound_2.getId());
            Assert.assertEquals("Retrieve by Trip: Invalid 2nd latitude!", latitude_2, placeFound_2.getLatitude());
            Assert.assertEquals("Retrieve by Trip: Invalid 2nd longitude!", longitude_2, placeFound_2.getLongitude());

            Date placeFound1_createdDate = DateUtil.formatDate(placeFound_1.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date placeFound2_createdDate = DateUtil.formatDate(placeFound_2.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Place[1]'s created date should have been BEFORE Place[0]'s created date!", 
                    placeFound1_createdDate.before(placeFound2_createdDate));
            
            //Retrieve by PlaceID
            Place placeToRetrieveByID = new Place();
            placeToRetrieveByID.setTripID(mTripCreated.getId());
            placeToRetrieveByID.setId(placeFound_1.getId());
            listOfPlacesFound = PlaceDBQuery.read(session, null, placeToRetrieveByID, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
            Assert.assertNotNull("Retrieve by PlaceID: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by PlaceID: Invalid number of found places!", 1, listOfPlacesFound.size());
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertNotNull("Retrieve by PlaceID: Place entity in the list should NOT be null!", placeFound);
            Assert.assertEquals("Retrieve by PlaceID: Invalid Place ID!", placeFound_1.getId(), placeFound.getId());
            Assert.assertEquals("Retrieve by Trip: Invalid latitude!", latitude_1, placeFound.getLatitude());
            Assert.assertEquals("Retrieve by Trip: Invalid longitude!", longitude_1, placeFound.getLongitude());
            
            //Delete by PlaceID:
            Place placeToDelete = new Place();
            placeToDelete.setUserID(mUserCreated.getUserId());
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setId(placeFound_2.getId());
            int numOfDeleted = PlaceDBQuery.delete(session, null, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Delete by PlaceID: Invalid number of deleted Places!", 1, numOfDeleted);
            
            listOfPlacesFound = PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 1, listOfPlacesFound.size());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByPlaceID_WithClientTransaction() throws Exception {
        double latitude_1 = 32.76243;
        double longitude_1 = -117.14818;
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                latitude_1, longitude_1,  
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Thread.sleep(1000); //Time gap due to dates sensitive validations
        double latitude_2 = 32.75142;
        double longitude_2 = -117.15112;
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                latitude_2, longitude_2,  
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Retrieve by TripID
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 2, listOfPlacesFound.size());

            //DB query is sorted by CREATED ascending:
            Place placeFound_1 = listOfPlacesFound.get(0);
            Assert.assertNotNull("Retrieve by Trip: The 1st found Place in List should NOT null!", placeFound_1);
            Assert.assertNotNull("Retrieve by Trip: The 1st found Place in List should never has NULL ID!", placeFound_1.getId());
            Assert.assertEquals("Retrieve by Trip: Invalid 1st latitude!", latitude_1, placeFound_1.getLatitude());
            Assert.assertEquals("Retrieve by Trip: Invalid 1st longitude!", longitude_1, placeFound_1.getLongitude());

            Place placeFound_2 = listOfPlacesFound.get(1);
            Assert.assertNotNull("Retrieve by Trip: The 2nd found Place in List should NOT null!", placeFound_2);
            Assert.assertNotNull("Retrieve by Trip: The 2nd found Place in List should never has NULL ID!", placeFound_2.getId());
            Assert.assertEquals("Retrieve by Trip: Invalid 2nd latitude!", latitude_2, placeFound_2.getLatitude());
            Assert.assertEquals("Retrieve by Trip: Invalid 2nd longitude!", longitude_2, placeFound_2.getLongitude());

            Date placeFound1_createdDate = DateUtil.formatDate(placeFound_1.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Date placeFound2_createdDate = DateUtil.formatDate(placeFound_2.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
            Assert.assertTrue("Place[1]'s created date should have been BEFORE Place[0]'s created date!", 
                    placeFound1_createdDate.before(placeFound2_createdDate));
            
            //Retrieve by PlaceID
            Place placeToRetrieveByID = new Place();
            placeToRetrieveByID.setTripID(mTripCreated.getId());
            placeToRetrieveByID.setId(placeFound_1.getId());
            transaction = session.beginTransaction();
            listOfPlacesFound = PlaceDBQuery.read(session, transaction, placeToRetrieveByID, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by PlaceID: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by PlaceID: Invalid number of found places!", 1, listOfPlacesFound.size());
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertNotNull("Retrieve by PlaceID: Place entity in the list should NOT be null!", placeFound);
            Assert.assertEquals("Retrieve by PlaceID: Invalid Place ID!", placeFound_1.getId(), placeFound.getId());
            Assert.assertEquals("Retrieve by Trip: Invalid latitude!", latitude_1, placeFound.getLatitude());
            Assert.assertEquals("Retrieve by Trip: Invalid longitude!", longitude_1, placeFound.getLongitude());
            
            //Delete by PlaceID:
            Place placeToDelete = new Place();
            placeToDelete.setUserID(mUserCreated.getUserId());
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setId(placeFound_2.getId());
            transaction = session.beginTransaction();
            int numOfDeleted = PlaceDBQuery.delete(session, transaction, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            transaction.commit();
            Assert.assertEquals("Delete by PlaceID: Invalid number of deleted Places!", 1, numOfDeleted);
            
            //Validate:
            transaction = session.beginTransaction();
            listOfPlacesFound = PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Retrieve by Trip: Invalid number of found places!", 1, listOfPlacesFound.size());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
        
    @Test
    public void testCount_ByTripID_WithLocalTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Count by TripID
            Place placeToCount = new Place();
            placeToCount.setTripID(mTripCreated.getId());
            int numOfPlaces = PlaceDBQuery.count(session, null, placeToCount);
            Assert.assertEquals("Count by Trip: Invalid returned number of places!", 2, numOfPlaces);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
 
    @Test
    public void testCount_ByTripID_WithClientTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Count by TripID
            Place placeToCount = new Place();
            placeToCount.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            int numOfPlaces = PlaceDBQuery.count(session, transaction, placeToCount);
            transaction.commit();
            Assert.assertEquals("Count by Trip: Invalid returned number of places!", 2, numOfPlaces);
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testUpdate_WithLocalTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeCreated = listOfPlacesFound.get(0);

            //Update:
            String noteUpdated = TestConstants.UNITTEST_PLACE_NOTE + "_UPDATED";
            Place placeToUpdate = new Place();
            placeToUpdate.setUserID(mUserCreated.getUserId());
            placeToUpdate.setId(placeCreated.getId());
            placeToUpdate.setNote(noteUpdated);
            placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            int numUpdated = PlaceDBQuery.update(session, null, placeToUpdate);
            Assert.assertEquals("Place update: Invalid number of updated items!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Place placeToRetrieveById = new Place();
            placeToRetrieveById.setId(placeCreated.getId());
            placeToRetrieveById.setTripID(mTripCreated.getId());
            listOfPlacesFound = PlaceDBQuery.read(session, null, placeToRetrieveById, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
            Assert.assertNotNull("Place retrieve after update: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Place retrieve after update: Invalid number of found places!", 1, listOfPlacesFound.size());
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertEquals("Place retrieve after update: Incorrect place note!", noteUpdated, placeFound.getNote());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_WithClientTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeCreated = listOfPlacesFound.get(0);

            //Update:
            String noteUpdated = TestConstants.UNITTEST_PLACE_NOTE + "_UPDATED";
            Place placeToUpdate = new Place();
            placeToUpdate.setUserID(mUserCreated.getUserId());
            placeToUpdate.setId(placeCreated.getId());
            placeToUpdate.setNote(noteUpdated);
            placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            transaction = session.beginTransaction();
            int numUpdated = PlaceDBQuery.update(session, transaction, placeToUpdate);
            transaction.commit();
            Assert.assertEquals("Place update: Invalid number of updated items!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Place placeToRetrieveById = new Place();
            placeToRetrieveById.setId(placeCreated.getId());
            placeToRetrieveById.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPlacesFound = PlaceDBQuery.read(session, transaction, placeToRetrieveById, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
            transaction.commit();
            Assert.assertNotNull("Place retrieve after update: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Place retrieve after update: Invalid number of found places!", 1, listOfPlacesFound.size());
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertEquals("Place retrieve after update: Incorrect place note!", noteUpdated, placeFound.getNote());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdate_NonEditableFields_WithLocalTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeCreated = listOfPlacesFound.get(0);

            //Update:
            Place placeToUpdate = new Place();
            placeToUpdate.setUserID(mUserCreated.getUserId());
            placeToUpdate.setId(placeCreated.getId());
            placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            placeToUpdate.setLatitude(10);
            placeToUpdate.setLongitude(-10);
            placeToUpdate.setLocation("Location - UPDATED");
            PlaceDBQuery.update(session, null, placeToUpdate);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Place placeToRetrieveById = new Place();
            placeToRetrieveById.setId(placeCreated.getId());
            placeToRetrieveById.setTripID(mTripCreated.getId());
            listOfPlacesFound = PlaceDBQuery.read(session, null, placeToRetrieveById, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
            Assert.assertNotNull("Place retrieve after update: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Place retrieve after update: Invalid number of found places!", 1, listOfPlacesFound.size());
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertEquals("Place retrieve after update: : Invalid latitude!", TestConstants.UNITTEST_PLACE_LATITUDE, placeFound.getLatitude());
            Assert.assertEquals("Place retrieve after update: : Invalid longitude!", TestConstants.UNITTEST_PLACE_LONGITUDE, placeFound.getLongitude());
            Assert.assertEquals("Place retrieve after update: : Invalid location!", TestConstants.UNITTEST_PLACE_LOCATION, placeFound.getLocation());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    @Test
    public void testUpdate_NonEditableFields_WithClientTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, transaction, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            transaction.commit();
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeCreated = listOfPlacesFound.get(0);

            //Update:
            Place placeToUpdate = new Place();
            placeToUpdate.setUserID(mUserCreated.getUserId());
            placeToUpdate.setId(placeCreated.getId());
            placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            placeToUpdate.setLatitude(10);
            placeToUpdate.setLongitude(-10);
            placeToUpdate.setLocation("Location - UPDATED");
            transaction = session.beginTransaction();
            PlaceDBQuery.update(session, transaction, placeToUpdate);
            transaction.commit();

            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();
            
            //Validate:
            Place placeToRetrieveById = new Place();
            placeToRetrieveById.setId(placeCreated.getId());
            placeToRetrieveById.setTripID(mTripCreated.getId());
            transaction = session.beginTransaction();
            listOfPlacesFound = PlaceDBQuery.read(session, transaction, placeToRetrieveById, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
            transaction.commit();
            Assert.assertNotNull("Place retrieve after update: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertEquals("Place retrieve after update: Invalid number of found places!", 1, listOfPlacesFound.size());
            Place placeFound = listOfPlacesFound.get(0);
            Assert.assertEquals("Place retrieve after update: : Invalid latitude!", TestConstants.UNITTEST_PLACE_LATITUDE, placeFound.getLatitude());
            Assert.assertEquals("Place retrieve after update: : Invalid longitude!", TestConstants.UNITTEST_PLACE_LONGITUDE, placeFound.getLongitude());
            Assert.assertEquals("Place retrieve after update: : Invalid location!", TestConstants.UNITTEST_PLACE_LOCATION, placeFound.getLocation());
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    

    @Test
    public void testUpdate_AfterDelete_WithLocalTransaction() throws Exception {
        TestUtilForPlace.placeCreate_byDB(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE, 
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION);

        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Place placeToRetrieveByTrip = new Place();
            placeToRetrieveByTrip.setTripID(mTripCreated.getId());
            List<Place> listOfPlacesFound = 
                    PlaceDBQuery.read(session, null, placeToRetrieveByTrip, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
            Assert.assertNotNull("Retrieve by Trip: Returned list of places should NOT be null!", listOfPlacesFound);
            Assert.assertNotNull("Retrieve by Trip: Place entity in the list should NOT be null!", listOfPlacesFound.get(0));
            Place placeCreated = listOfPlacesFound.get(0);

            //Delete:
            Place placeToDelete = new Place();
            placeToDelete.setUserID(mUserCreated.getUserId());
            placeToDelete.setTripID(mTripCreated.getId());
            placeToDelete.setId(placeCreated.getId());
            int numOfDeleted = PlaceDBQuery.delete(session, null, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
            Assert.assertEquals("Delete by PlaceID: Invalid number of deleted Places!", 1, numOfDeleted);
           
            //Update:
            Place placeToUpdate = new Place();
            placeToUpdate.setUserID(mUserCreated.getUserId());
            placeToUpdate.setId(placeCreated.getId());
            placeToUpdate.setNote("Illegal update!");
            placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
            int numUpdated = PlaceDBQuery.update(session, null, placeToUpdate);
            Assert.assertEquals("Place Update after Delete: Invalid returned number of updated!", 0, numUpdated);
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }    
}
