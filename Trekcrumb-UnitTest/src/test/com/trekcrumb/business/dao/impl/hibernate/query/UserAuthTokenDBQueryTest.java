package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQuery;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

public class UserAuthTokenDBQueryTest {
    private static final String CLASS_NAME = "UserAuthTokenDBQueryTest";
    
    private User mUserCreated;

    /**
     * Data setup before each test case. 
     */
    @Before
    public void setUp() {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = CLASS_NAME + "-testCreateAndReadAndDelete";
        TestUtilForUser.userCreate_byDB(email, username, fullname);
        
        mUserCreated = TestUtilForUser.userRead_byDB(username);
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForUserAuthToken.userAuthTokenDelete_byDB(mUserCreated.getUserId());
        TestUtilForUser.userDelete_byDB(mUserCreated);
        mUserCreated = null;
    }
    
    @Test
    public void testCreateReadAndDelete_ByUser_WithLocalTransaction() throws Exception {
        String uniqueId = CommonUtil.generateID(null);;
        String deviceIdentity =TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
        String authToken = uniqueId;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Create:
            UserAuthToken authTokenCreate = new UserAuthToken();
            authTokenCreate.setUserId(mUserCreated.getUserId());
            authTokenCreate.setUsername(mUserCreated.getUsername());
            authTokenCreate.setDeviceIdentity(deviceIdentity);
            authTokenCreate.setAuthToken(authToken);
            UserAuthTokenDBQuery.create(session, null, authTokenCreate);
            
            //Read by User
            UserAuthToken authTokenToRead = new UserAuthToken();
            authTokenToRead.setUserId(mUserCreated.getUserId());
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, null, authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertTrue("Returned record list should have a record!", recordList.size() > 0);
            
            UserAuthToken authTokenRead =  recordList.get(0);
            Assert.assertNotNull("Returned UserAuthToken shall have UserRememberMeID!", authTokenRead.getId());
            Assert.assertEquals("Invalid returned UserId!", mUserCreated.getUserId(), authTokenRead.getUserId());
            Assert.assertEquals("Invalid returned Username!", mUserCreated.getUsername(), authTokenRead.getUsername());
            Assert.assertEquals("Invalid returned Device identity!", deviceIdentity, authTokenRead.getDeviceIdentity());
            Assert.assertEquals("Invalid returned Auth token!", authToken, authTokenRead.getAuthToken());
            
            //Delete by User:
            UserAuthToken authTokenToDelete = new UserAuthToken();
            authTokenToDelete.setUserId(mUserCreated.getUserId());
            int numDeleted = UserAuthTokenDBQuery.delete(session, null, authTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            Assert.assertEquals("Invalid number of deleted record!", 1, numDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    @Test
    public void testCreateReadAndDelete_ByUser_WithClientTransaction() throws Exception {
        String uniqueId = CommonUtil.generateID(null);;
        String deviceIdentity =TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
        String authToken = uniqueId;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Create:
            UserAuthToken authTokenCreate = new UserAuthToken();
            authTokenCreate.setUserId(mUserCreated.getUserId());
            authTokenCreate.setUsername(mUserCreated.getUsername());
            authTokenCreate.setDeviceIdentity(deviceIdentity);
            authTokenCreate.setAuthToken(authToken);
            transaction = session.beginTransaction();
            UserAuthTokenDBQuery.create(session, transaction, authTokenCreate);
            transaction.commit();
            
            //Read by User
            UserAuthToken authTokenToRead = new UserAuthToken();
            authTokenToRead.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, transaction, authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            transaction.commit();
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertTrue("Returned record list should have a record!", recordList.size() > 0);
            
            UserAuthToken authTokenRead =  recordList.get(0);
            Assert.assertNotNull("Returned UserAuthToken shall have UserRememberMeID!", authTokenRead.getId());
            Assert.assertEquals("Invalid returned UserId!", mUserCreated.getUserId(), authTokenRead.getUserId());
            Assert.assertEquals("Invalid returned Username!", mUserCreated.getUsername(), authTokenRead.getUsername());
            Assert.assertEquals("Invalid returned Device identity!", deviceIdentity, authTokenRead.getDeviceIdentity());
            Assert.assertEquals("Invalid returned Auth token!", authToken, authTokenRead.getAuthToken());
            
            //Delete by User:
            UserAuthToken authTokenToDelete = new UserAuthToken();
            authTokenToDelete.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numDeleted = UserAuthTokenDBQuery.delete(session, transaction, authTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            transaction.commit();
            Assert.assertEquals("Invalid number of deleted record!", 1, numDeleted);
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByUser_Multiple_WithLocalTransaction() throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Create 1:
            String uniqueId_1 = CommonUtil.generateID(null);;
            String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_1;
            String authToken_1 = uniqueId_1;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, authToken_1);
            
            //Create 2:
            String uniqueId_2 = CommonUtil.generateID(null);;
            String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_2;
            String authToken_2 = uniqueId_2;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, authToken_2);

            //Retrieve by User
            UserAuthToken authTokenRetrieve = new UserAuthToken();
            authTokenRetrieve.setUserId(mUserCreated.getUserId());
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, null, authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 2, recordList.size());
            
            //Delete by User:
            UserAuthToken authTokenDelete = new UserAuthToken();
            authTokenDelete.setUserId(mUserCreated.getUserId());
            int numDeleted = 
                    UserAuthTokenDBQuery.delete(session, null, authTokenDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            Assert.assertEquals("Invalid number of deleted record!", 2, numDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadAndDelete_ByUser_Multiple_WithClientTransaction() throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Create 1:
            String uniqueId_1 = CommonUtil.generateID(null);;
            String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_1;
            String authToken_1 = uniqueId_1;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, authToken_1);
            
            //Create 2:
            String uniqueId_2 = CommonUtil.generateID(null);;
            String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_2;
            String authToken_2 = uniqueId_2;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, authToken_2);

            //Retrieve by User
            UserAuthToken authTokenRetrieve = new UserAuthToken();
            authTokenRetrieve.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, transaction, authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            transaction.commit();
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 2, recordList.size());
            
            //Delete by User:
            UserAuthToken authTokenDelete = new UserAuthToken();
            authTokenDelete.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numDeleted = 
                    UserAuthTokenDBQuery.delete(session, transaction, authTokenDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            transaction.commit();
            Assert.assertEquals("Invalid number of deleted record!", 2, numDeleted);
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadByDeviceAndDeleteById_WithLocalTransaction() throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Create 1:
            String uniqueId_1 = CommonUtil.generateID(null);
            String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_1;
            String authToken_1 = uniqueId_1;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, authToken_1);
            
            //Create 2:
            String uniqueId_2 = CommonUtil.generateID(null);
            String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_2;
            String authToken_2 = uniqueId_2;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, authToken_2);

            //Retrieve by Device
            UserAuthToken authTokenToRetrieve = new UserAuthToken();
            authTokenToRetrieve.setUserId(mUserCreated.getUserId());
            authTokenToRetrieve.setDeviceIdentity(deviceIdentity_2);
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, null, authTokenToRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
            UserAuthToken authTokenRetrieved =  recordList.get(0);
            Assert.assertNotNull("Returned UserAuthToken bean shall have its new ID!", authTokenRetrieved.getId());
            Assert.assertEquals("Invalid returned UserId!", mUserCreated.getUserId(), authTokenRetrieved.getUserId());
            Assert.assertEquals("Invalid returned Username!", mUserCreated.getUsername(), authTokenRetrieved.getUsername());
            Assert.assertEquals("Invalid returned Device identity!", deviceIdentity_2, authTokenRetrieved.getDeviceIdentity());
            Assert.assertEquals("Invalid returned Auth token!", authToken_2, authTokenRetrieved.getAuthToken());

            //Delete by ID:
            UserAuthToken authTokenDelete = new UserAuthToken();
            authTokenDelete.setId(authTokenRetrieved.getId());
            int numDeleted = UserAuthTokenDBQuery.delete(session, null, authTokenDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
            Assert.assertEquals("Invalid number of deleted record!", 1, numDeleted);
            
            //Verify:
            recordList = UserAuthTokenDBQuery.read(session, null, authTokenToRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
            if(recordList != null) {
                Assert.assertEquals("Returned record list shall BE empty!", 0, recordList.size());
            }
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadByDeviceAndDeleteById_WithClientTransaction() throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Create 1:
            String uniqueId_1 = CommonUtil.generateID(null);
            String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_1;
            String authToken_1 = uniqueId_1;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, authToken_1);
            
            //Create 2:
            String uniqueId_2 = CommonUtil.generateID(null);
            String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_2;
            String authToken_2 = uniqueId_2;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, authToken_2);

            //Retrieve by Device
            UserAuthToken authTokenToRetrieve = new UserAuthToken();
            authTokenToRetrieve.setUserId(mUserCreated.getUserId());
            authTokenToRetrieve.setDeviceIdentity(deviceIdentity_2);
            transaction = session.beginTransaction();
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, transaction, authTokenToRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
            transaction.commit();
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
            UserAuthToken authTokenRetrieved =  recordList.get(0);
            Assert.assertNotNull("Returned UserAuthToken bean shall have its new ID!", authTokenRetrieved.getId());
            Assert.assertEquals("Invalid returned UserId!", mUserCreated.getUserId(), authTokenRetrieved.getUserId());
            Assert.assertEquals("Invalid returned Username!", mUserCreated.getUsername(), authTokenRetrieved.getUsername());
            Assert.assertEquals("Invalid returned Device identity!", deviceIdentity_2, authTokenRetrieved.getDeviceIdentity());
            Assert.assertEquals("Invalid returned Auth token!", authToken_2, authTokenRetrieved.getAuthToken());

            //Delete by ID:
            UserAuthToken authTokenDelete = new UserAuthToken();
            authTokenDelete.setId(authTokenRetrieved.getId());
            transaction = session.beginTransaction();
            int numDeleted = UserAuthTokenDBQuery.delete(session, transaction, authTokenDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
            transaction.commit();
            Assert.assertEquals("Invalid number of deleted record!", 1, numDeleted);
            
            //Verify:
            transaction = session.beginTransaction();
            recordList = UserAuthTokenDBQuery.read(session, transaction, authTokenToRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
            transaction.commit();
            if(recordList != null) {
                Assert.assertEquals("Returned record list shall BE empty!", 0, recordList.size());
            }
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testRead_ByAuthToken_WithLocalTransaction() throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Create:
            String uniqueId = CommonUtil.generateID(null);;
            String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
            String authToken = uniqueId;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken);
            
            //Retrieve:
            UserAuthToken authTokenAuthToRead = new UserAuthToken();
            authTokenAuthToRead.setUserId(mUserCreated.getUserId());
            authTokenAuthToRead.setAuthToken(authToken);
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, null, authTokenAuthToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());

        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testRead_ByAuthToken_WithClientTransaction() throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Create:
            String uniqueId = CommonUtil.generateID(null);;
            String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
            String authToken = uniqueId;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken);
            
            //Retrieve:
            UserAuthToken authTokenAuthToRead = new UserAuthToken();
            authTokenAuthToRead.setUserId(mUserCreated.getUserId());
            authTokenAuthToRead.setAuthToken(authToken);
            transaction = session.beginTransaction();
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, transaction, authTokenAuthToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
            transaction.commit();
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());

        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testRead_ByAuthToken_InvalidAuthToken_WithLocalTransaction() throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Create:
            String uniqueId = CommonUtil.generateID(null);;
            String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
            String authToken = uniqueId;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken);
            
            //Retrieve:
            String invalidAuthToken = "Invalid" + System.currentTimeMillis();
            UserAuthToken authTokenAuthToRead = new UserAuthToken();
            authTokenAuthToRead.setUserId(mUserCreated.getUserId());
            authTokenAuthToRead.setAuthToken(invalidAuthToken);
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, null, authTokenAuthToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
            if(recordList != null) {
                Assert.assertEquals("Returned record list shall BE empty!", 0, recordList.size());
            }
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testRead_ByAuthToken_InvalidAuthToken_WithClientTransaction() throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();

            //Create:
            String uniqueId = CommonUtil.generateID(null);;
            String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
            String authToken = uniqueId;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken);
            
            //Retrieve:
            String invalidAuthToken = "Invalid" + System.currentTimeMillis();
            UserAuthToken authTokenAuthToRead = new UserAuthToken();
            authTokenAuthToRead.setUserId(mUserCreated.getUserId());
            authTokenAuthToRead.setAuthToken(invalidAuthToken);
            transaction = session.beginTransaction();
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, transaction, authTokenAuthToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
            transaction.commit();
            if(recordList != null) {
                Assert.assertEquals("Returned record list shall BE empty!", 0, recordList.size());
            }
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdateUserToken_WithLocalTransaction() throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Create:
            String uniqueId = CommonUtil.generateID(null);;
            String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
            String authToken = uniqueId;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken);
            
            //Retrieve by User:
            UserAuthToken authTokenRetrieve = new UserAuthToken();
            authTokenRetrieve.setUserId(mUserCreated.getUserId());
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, null, authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
            UserAuthToken authTokenRetrieve_0 =  recordList.get(0);
            Assert.assertNotNull("Returned UserAuthToken shall have UserRememberMeID!", authTokenRetrieve_0.getId());

            //Update:
            String newAuthToken = "" + System.currentTimeMillis();
            UserAuthToken authTokenToUpdate = new UserAuthToken();
            authTokenToUpdate.setId(authTokenRetrieve_0.getId());
            authTokenToUpdate.setAuthToken(newAuthToken);
            int numUpdated = UserAuthTokenDBQuery.update(session, null, authTokenToUpdate);
            Assert.assertEquals("Invalid number of updated record!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            //Verify:
            recordList = UserAuthTokenDBQuery.read(session, null, authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
            authTokenRetrieve_0 =  recordList.get(0);         
            Assert.assertEquals("Invalid returned Auth token after update!", newAuthToken, authTokenRetrieve_0.getAuthToken());
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testUpdateUserToken_WithClientTransaction() throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            
            //Create:
            String uniqueId = CommonUtil.generateID(null);;
            String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
            String authToken = uniqueId;
            TestUtilForUserAuthToken.userAuthTokenCreate_byDB(
                    mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken);
            
            //Retrieve by User:
            UserAuthToken authTokenRetrieve = new UserAuthToken();
            authTokenRetrieve.setUserId(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            List<UserAuthToken> recordList = 
                    UserAuthTokenDBQuery.read(session, transaction, authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            transaction.commit();
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
            UserAuthToken authTokenRetrieve_0 =  recordList.get(0);
            Assert.assertNotNull("Returned UserAuthToken shall have UserRememberMeID!", authTokenRetrieve_0.getId());

            //Update:
            String newAuthToken = "" + System.currentTimeMillis();
            UserAuthToken authTokenToUpdate = new UserAuthToken();
            authTokenToUpdate.setId(authTokenRetrieve_0.getId());
            authTokenToUpdate.setAuthToken(newAuthToken);
            transaction = session.beginTransaction();
            int numUpdated = UserAuthTokenDBQuery.update(session, transaction, authTokenToUpdate);
            transaction.commit();
            Assert.assertEquals("Invalid number of updated record!", 1, numUpdated);
            
            //NOTE (11/2014): Without clearing the Session, the re-read same data after update will return old data
            session.clear();

            //Verify:
            transaction = session.beginTransaction();
            recordList = UserAuthTokenDBQuery.read(session, transaction, authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
            transaction.commit();
            Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
            Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
            authTokenRetrieve_0 =  recordList.get(0);         
            Assert.assertEquals("Invalid returned Auth token after update!", newAuthToken, authTokenRetrieve_0.getAuthToken());
            
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
    }    
    
    
}
