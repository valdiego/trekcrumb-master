package test.com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;

import com.trekcrumb.business.dao.IUserAuthTokenDAO;
import com.trekcrumb.business.dao.impl.hibernate.UserAuthTokenDAOHibernateImpl;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * DAO Component's tests for entity: UserAuthToken.
 * 
 * This test cover only BASIC use-cases to DAO CRUD functionality. For more comprehensive tests
 * regarding the entity, see the Business (Manager) test classes.
 */
public class UserAuthTokenDAOHibernateImplTest {
    private static final String CLASS_NAME = "UserAuthTokenDAOHibernateImplTest";
    
    private static User mUserCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mUserCreated == null) {
            String username = CommonUtil.generateID(null);
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME;
            mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
        }
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static  void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForUserAuthToken.userAuthTokenDelete_byDB(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreateReadAndDelete_ByUser() throws Exception {
        IUserAuthTokenDAO authDAOImpl = new UserAuthTokenDAOHibernateImpl();

        //Create:
        String uniqueId = CommonUtil.generateID(null);;
        String deviceIdentity =TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
        String authToken = uniqueId;
        TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken, true);
        
        //Read by User ID:
        UserAuthToken authTokenToRead = new UserAuthToken();
        authTokenToRead.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> authTokenList = authDAOImpl.read(authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        Assert.assertNotNull("AuthToken Read: Returned record list shall NOT be null!", authTokenList);
        Assert.assertTrue("AuthToken Read: Returned record list should have a record!", authTokenList.size() > 0);
        UserAuthToken authTokenRead =  authTokenList.get(0);
        Assert.assertNotNull("AuthToken Read: Returned UserAuthToken shall have UserRememberMeID!", authTokenRead.getId());
        Assert.assertEquals("AuthToken Read: Invalid returned UserId!", mUserCreated.getUserId(), authTokenRead.getUserId());
        Assert.assertEquals("AuthToken Read: Invalid returned Username!", mUserCreated.getUsername(), authTokenRead.getUsername());
        Assert.assertEquals("AuthToken Read: Invalid returned Device identity!", deviceIdentity, authTokenRead.getDeviceIdentity());
        Assert.assertEquals("AuthToken Read: Invalid returned AuthToken!", authToken, authTokenRead.getAuthToken());

        //Delete by User ID:
        UserAuthToken authTokenToDelete = new UserAuthToken();
        authTokenToDelete.setUserId(mUserCreated.getUserId());
        int numDeleted = authDAOImpl.delete(authTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
        Assert.assertEquals("AuthToken Delete: Invalid number of deleted record!", 1, numDeleted);            
        
        //Validate after delete:
        authTokenList = authDAOImpl.read(authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        if(authTokenList != null) {
            Assert.assertEquals("AuthToken Read after Delete: Invalid number of records found!", 
                    0, authTokenList.size());
        }
    }
    
    @Test
    public void testCreate_SameUserSameDevice() throws Exception {
        IUserAuthTokenDAO authDAOImpl = new UserAuthTokenDAOHibernateImpl();

        //Create existing one:
        String uniqueId = CommonUtil.generateID(null);
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
        String authToken_orig = uniqueId;
        TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken_orig, true);

        //Create new one:
        String authToken_new = CommonUtil.generateID(null);
        UserAuthToken userAuthTokenToCreate_new = new UserAuthToken();
        userAuthTokenToCreate_new.setUserId(mUserCreated.getUserId());
        userAuthTokenToCreate_new.setUsername(mUserCreated.getUsername());
        userAuthTokenToCreate_new.setDeviceIdentity(deviceIdentity);
        userAuthTokenToCreate_new.setAuthToken(authToken_new);
        UserAuthToken userAuthTokenCreated_new = authDAOImpl.create(userAuthTokenToCreate_new);
        Assert.assertNotNull("AuthToken Create: Returned UserAuthToken shall NOT be null!", userAuthTokenCreated_new);
        Assert.assertEquals("AuthToken Create: Invalid returned UserId!", mUserCreated.getUserId(), userAuthTokenCreated_new.getUserId());
        Assert.assertEquals("AuthToken Create: Invalid returned Device identity!", deviceIdentity, userAuthTokenCreated_new.getDeviceIdentity());
        Assert.assertEquals("AuthToken Create: Invalid returned AuthToken!", authToken_new, userAuthTokenCreated_new.getAuthToken());
    }
    
    @Test
    public void testAuthenticate() throws Exception {
        IUserAuthTokenDAO authDAOImpl = new UserAuthTokenDAOHibernateImpl();

        String uniqueId = CommonUtil.generateID(null);
        String deviceIdentity =TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId;
        String authToken = uniqueId;
        UserAuthToken userAuthTokenCreated = 
                TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, authToken, false);
        
        //Authenticate:
        UserAuthToken userAuthTokenToAuthenticate = new UserAuthToken();
        userAuthTokenToAuthenticate.setUserId(mUserCreated.getUserId());
        userAuthTokenToAuthenticate.setAuthToken(userAuthTokenCreated.getAuthToken());
        User userAuthenticated = authDAOImpl.authenticate(userAuthTokenToAuthenticate);
        Assert.assertNotNull("AuthToken Authenticate: Should be SUCCESS and returned a not-null User bean!", userAuthenticated);
        Assert.assertEquals("AuthToken Authenticate: Invalid returned UserId!", mUserCreated.getUserId(), userAuthenticated.getUserId());
        Assert.assertEquals("AuthToken Authenticate: Invalid returned Username!", mUserCreated.getUsername(), userAuthenticated.getUsername());
        Assert.assertEquals("AuthToken Authenticate: Invalid returned Email!", mUserCreated.getEmail(), userAuthenticated.getEmail());
        Assert.assertEquals("AuthToken Authenticate: Invalid returned Fullname!", mUserCreated.getFullname(), userAuthenticated.getFullname());
    }
    
    @Test
    public void testRead_ByUserAndByDevice() throws Exception {
        IUserAuthTokenDAO authDAOImpl = new UserAuthTokenDAOHibernateImpl();

        //Create 1:
        String uniqueId_1 = CommonUtil.generateID(null);
        String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_1;
        String authToken_1 = uniqueId_1;
        TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, authToken_1, false);
        
        //Create 2:
        String uniqueId_2 = CommonUtil.generateID(null);
        String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_2;
        String authToken_2 = uniqueId_2;
        TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, authToken_2, false);

        //Read by user:
        UserAuthToken authTokenToRead = new UserAuthToken();
        authTokenToRead.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> recordList = 
                authDAOImpl.read(authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        Assert.assertNotNull("AuthToken Read: Returned record list shall NOT be null!", recordList);
        Assert.assertEquals("AuthToken Read: Invalid number of returned records!", 2, recordList.size());
        
        //Read by device:
        authTokenToRead.setDeviceIdentity(deviceIdentity_1);
        recordList = 
                authDAOImpl.read(authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
        Assert.assertNotNull("AuthToken Read: Returned record list shall NOT be null!", recordList);
        Assert.assertEquals("AuthToken Read: Invalid number of returned records!", 1, recordList.size());
    }
    
    @Test
    public void testDelete_ByIdAndByUser() throws Exception {
        IUserAuthTokenDAO authDAOImpl = new UserAuthTokenDAOHibernateImpl();

        //Create 1:
        String uniqueId_1 = CommonUtil.generateID(null);
        String deviceIdentity_1 =TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_1;
        String authToken_1 = uniqueId_1;
        UserAuthToken authTokenCreated_1 = TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, authToken_1, false);
        
        //Create 2:
        String uniqueId_2 = CommonUtil.generateID(null);
        String deviceIdentity_2 =TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + uniqueId_2;
        String authToken_2 = uniqueId_2;
        TestUtilForUserAuthToken.userAuthTokenCreate_byDAOImpl(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, authToken_2, false);

        //Delete by ID:
        UserAuthToken userAuthTokenDelete = new UserAuthToken();
        userAuthTokenDelete.setUserId(mUserCreated.getUserId());
        userAuthTokenDelete.setId(authTokenCreated_1.getId());
        int numDeleted = 
                authDAOImpl.delete(userAuthTokenDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
        Assert.assertEquals("AuthToken Delete: Invalid number of deleted record!", 1, numDeleted);            
        
        //Verify:
        UserAuthToken authTokenRetrieve = new UserAuthToken();
        authTokenRetrieve.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> recordList = 
                authDAOImpl.read(authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        Assert.assertNotNull("AuthToken Read after Delete: Returned record list shall NOT be null!", recordList);
        Assert.assertEquals("AuthToken Read after Delete: Invalid number of returned records!", 1, recordList.size());

        //Delete by User:
        userAuthTokenDelete.setId(null);
        numDeleted = 
                authDAOImpl.delete(userAuthTokenDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
        Assert.assertEquals("AuthToken Delete: Invalid number of deleted record!", 1, numDeleted);
        
        //Verify all is gone:
        recordList = 
                authDAOImpl.read(authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        if(recordList != null) {
            Assert.assertEquals("AuthToken Read after Delete: Returned record list shall BE empty!", 0, recordList.size());
        } else {
            //God Bless!
        }
    }            

}
