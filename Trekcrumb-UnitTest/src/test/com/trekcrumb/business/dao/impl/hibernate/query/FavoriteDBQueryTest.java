package test.com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.List;

import junit.framework.Assert;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForFavorite;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.impl.hibernate.HibernateManager;
import com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQuery;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonUtil;

public class FavoriteDBQueryTest {
    private static final String CLASS_NAME = "FavoriteDBQueryTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mTripCreated = null;
        mUserCreated = null;
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForFavorite.faveDeleteALLbyFavoriteDB(mUserCreated.getUserId());
    }

    @Test
    public void testCreateAndReadAndDelete_WithLocalTransaction() throws Exception {
        /*
         * NOTE: In this test we ensure to use separate DB sessions between create, update/delete
         * and read such that the transaction is committed and done. Especially since there are 
         * multiple triggers executed during each transaction. Otherwise, if using a single session
         * the data could be outdated causing test to fail.
         */
        TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), mUserCreated.getUserId());
        
        Session session = null;
        
        //Retrieve and verify:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(mTripCreated.getId());
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        try {
            session = HibernateManager.openSession();
            List<Favorite> listOfFavesFound = FavoriteDBQuery.read(session, null, faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
            Assert.assertNotNull("Favorite Retrieve: Favorite entity in the list should NOT be null!", listOfFavesFound.get(0));
            Favorite faveFound = listOfFavesFound.get(0);
            Assert.assertEquals("Favorite Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), faveFound.getTripID());
            Assert.assertEquals("Favorite Retrieve: Invalid returned User ID!", mUserCreated.getUserId(), faveFound.getUserID());
            Assert.assertNotNull("Favorite Retrieve: Returned created date shall not be null!", faveFound.getCreated());
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }

        //Delete:
        try {
            session = HibernateManager.openSession();
            Favorite faveToDelete = new Favorite();
            faveToDelete.setTripID(mTripCreated.getId());
            faveToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = FavoriteDBQuery.delete(session, null, faveToDelete);
            Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 1, numOfDeleted);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
            
        //Retrieve and verify after delete:
        try {
            session = HibernateManager.openSession();
            List<Favorite> listOfFavesFound = FavoriteDBQuery.read(session, null, faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve after Delete: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve after Delete: Invalid number of found favorites!", 0, listOfFavesFound.size());
         } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testCreateAndReadAndDelete_WithClientTransaction() throws Exception {
        /*
         * NOTE: In this test we ensure to use separate DB sessions between create, update/delete
         * and read such that the transaction is committed and done. Especially since there are 
         * multiple triggers executed during each transaction. Otherwise, if using a single session
         * the data could be outdated causing test to fail.
         */
        TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), mUserCreated.getUserId());
        
        Session session = null;
        Transaction transaction = null;
        
        //Retrieve and verify:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(mTripCreated.getId());
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            List<Favorite> listOfFavesFound = FavoriteDBQuery.read(session, transaction, faveToRetrieve, null, -1, -1);
            transaction.commit();
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
            Assert.assertNotNull("Favorite Retrieve: Favorite entity in the list should NOT be null!", listOfFavesFound.get(0));
            Favorite faveFound = listOfFavesFound.get(0);
            Assert.assertEquals("Favorite Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), faveFound.getTripID());
            Assert.assertEquals("Favorite Retrieve: Invalid returned User ID!", mUserCreated.getUserId(), faveFound.getUserID());
            Assert.assertNotNull("Favorite Retrieve: Returned created date shall not be null!", faveFound.getCreated());
          
        } finally {
            transaction = null; 
            HibernateManager.shutdownSessions(session, null);
        }

        //Delete:
        try {
            session = HibernateManager.openSession();
            Favorite faveToDelete = new Favorite();
            faveToDelete.setTripID(mTripCreated.getId());
            faveToDelete.setUserID(mUserCreated.getUserId());
            transaction = session.beginTransaction();
            int numOfDeleted = FavoriteDBQuery.delete(session, transaction, faveToDelete);
            transaction.commit();
            Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 1, numOfDeleted);
        } finally {
            transaction = null; 
            HibernateManager.shutdownSessions(session, null);
        }
            
        //Retrieve and verify after delete:
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            List<Favorite> listOfFavesFound = FavoriteDBQuery.read(session, transaction, faveToRetrieve, null, -1, -1);
            transaction.commit();
            Assert.assertNotNull("Favorite Retrieve after Delete: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve after Delete: Invalid number of found favorites!", 0, listOfFavesFound.size());
        } finally {
            transaction = null; 
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Test
    public void testReadByTrip_WithLocalTransaction() throws Exception {
        Session session = null;
        User user1 = null, user2 = null, user3 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);

            String username3 = CommonUtil.generateID(null);
            user3 = TestUtilForUser.userCreate_byManager(username3 + TestConstants.UNITTEST_EMAIL_SUFFIX, username3, CLASS_NAME + "-user3", false);
            
            //Create Favorites:
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user1.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user2.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user3.getUserId());
            
            //Retrieve and verify:
            Trip trip = new Trip();
            trip.setId(mTripCreated.getId());
            session = HibernateManager.openSession();
            List<User> listOfUsersFound = FavoriteDBQuery.readByTrip(session, null, trip, -1, -1);
            if(listOfUsersFound == null || listOfUsersFound.size() == 0) {
                Assert.fail("Favorite Read by Trip: Returned list of users who favorite the Trip should NOT be null!");
            }
            
            User oneOfUsersFound = listOfUsersFound.get(0);
            Assert.assertNotNull("Favorite Read by Trip: Returned User in the list should not be null!", oneOfUsersFound);
            Assert.assertNotNull("Favorite Read by Trip: Returned User in the list should have username!", oneOfUsersFound.getUsername());
            Assert.assertNotNull("Favorite Read by Trip: Returned User in the list should have fullename!", oneOfUsersFound.getFullname());
        } finally {
            HibernateManager.shutdownSessions(session, null);

            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
            TestUtilForUser.userDelete_byDB_ALLData(user3);
        }
    }
    
    @Test
    public void testReadByUser_WithLocalTransaction() throws Exception {
        Session session = null;
        User userOther = null;
        try {
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);

            //Create Trips:
            Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip3 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            
            //Create Favorites:
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip1.getId(), userOther.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip2.getId(), userOther.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip3.getId(), userOther.getUserId());
            
            //Retrieve and verify:
            User user = new User();
            user.setUsername(username);
            session = HibernateManager.openSession();
            List<Trip> listOfTripsFound = FavoriteDBQuery.readByUser(session, null, user, -1, -1);
            if(listOfTripsFound == null || listOfTripsFound.size() == 0) {
                Assert.fail("Favorite Read by User: Returned list of trips should NOT be null!");
            }            
            Assert.assertEquals("Favorite Read by User: Invalid number of returned list of trips!", 3, listOfTripsFound.size());
        
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }    
    
}
