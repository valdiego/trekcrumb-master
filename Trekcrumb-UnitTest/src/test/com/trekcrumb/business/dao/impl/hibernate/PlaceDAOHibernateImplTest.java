package test.com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPlace;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.IPlaceDAO;
import com.trekcrumb.business.dao.impl.hibernate.PlaceDAOHibernateImpl;
import com.trekcrumb.business.manager.PlaceManager;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.DateUtil;

/**
 * Note:
 * Mostly basic functionaly test cases. More advanced test case scenarios are in PlaceManagerTest.
 *
 */
public class PlaceDAOHibernateImplTest {
    private static final String CLASS_NAME = "PlaceDAOHibernateImplTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME); 
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForPlace.placeDelete_byDB_ALLPlaces(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreateReadAndDelete_ByID() throws Exception {
        IPlaceDAO daoImpl = new PlaceDAOHibernateImpl();

		Place placeCreated = TestUtilForPlace.placeCreate_byDAOImpl(
		        mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, true);

		//Retrieve by ID:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setId(placeCreated.getId());
        placeToRetrieve.setTripID(mTripCreated.getId());
        List<Place> placeFoundList = 
                daoImpl.read(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
        Assert.assertNotNull("Place Retrieve: Returned list of retrieved Place should NOT be null!", placeFoundList);
        Assert.assertEquals("Place Retrieve: Incorrect number of Places returned!", 1, placeFoundList.size());
        Place placeFound = placeFoundList.get(0);
        Assert.assertNotNull("Place Retrieve: Returned Place should have its ID!", placeFound.getId());
        Assert.assertEquals("Place Retrieve: Incorrect user ID!", mUserCreated.getUserId(), placeFound.getUserID());
        Assert.assertEquals("Place Retrieve: Incorrect trip ID!", mTripCreated.getId(), placeFound.getTripID());
        Assert.assertEquals("Place Retrieve: Incorrect Latitude!", TestConstants.UNITTEST_PLACE_LATITUDE, placeFound.getLatitude());
        Assert.assertEquals("Place Retrieve: Incorrect Longitude!", TestConstants.UNITTEST_PLACE_LONGITUDE, placeFound.getLongitude());
        Assert.assertEquals("Place Retrieve: Incorrect Note!", TestConstants.UNITTEST_PLACE_NOTE, placeFound.getNote());
        Assert.assertEquals("Place Retrieve: Incorrect Location!", TestConstants.UNITTEST_PLACE_LOCATION, placeFound.getLocation());
        Assert.assertNotNull("Place Retrieve: Record's created date should NOT null!!", placeFound.getCreated());
		
        //Delete by ID:
        Place placeToDelete = new Place();
        placeToDelete.setUserID(mUserCreated.getUserId());
        placeToDelete.setTripID(mTripCreated.getId());
        placeToDelete.setId(placeCreated.getId());
        int numDeleted = daoImpl.delete(placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_PLACEID);
        Assert.assertEquals("Place Delete: Incorrect number of deleted Trip!", 1, numDeleted);
        
        //Validate
        placeFoundList = 
                daoImpl.read(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
        if(placeFoundList != null) {
            Assert.assertEquals("Place Retrieve after Delete: Incorrect number of places returned!", 0, placeFoundList.size());
        }
    }
    
    @Test
    public void testUpdate_ALL() throws Exception {
        IPlaceDAO daoImpl = new PlaceDAOHibernateImpl();

        Place placeCreated = TestUtilForPlace.placeCreate_byDAOImpl(
                mUserCreated.getUserId(), mTripCreated.getId(), 
                TestConstants.UNITTEST_PLACE_LATITUDE, TestConstants.UNITTEST_PLACE_LONGITUDE,
                TestConstants.UNITTEST_PLACE_NOTE, TestConstants.UNITTEST_PLACE_LOCATION, false);

        //Update:
        String noteUpdated = TestConstants.UNITTEST_PLACE_NOTE + "_UPDATED";
        Place placeToUpdate = new Place();
        placeToUpdate.setUserID(mUserCreated.getUserId());
        placeToUpdate.setTripID(mTripCreated.getId());
        placeToUpdate.setId(placeCreated.getId());
        placeToUpdate.setNote(noteUpdated);
        placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Place placeUpdated = daoImpl.update(placeToUpdate);
        Assert.assertNotNull("Place Update: Returned updated place should NOT be null!", placeUpdated);
        Assert.assertEquals("Place Update: Incorrect place note!", noteUpdated, placeUpdated.getNote());
        
        //Validate:
        Place placeToRetrieve = new Place();
        placeToRetrieve.setId(placeCreated.getId());
        placeToRetrieve.setTripID(mTripCreated.getId());
        List<Place> listOfPlacesFound = PlaceManager.retrieve(placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID);
        Assert.assertNotNull("Place Retrieve after Update: Returned list of places should NOT be null!", listOfPlacesFound);
        Assert.assertEquals("Place Retrieve after Update: Invalid number of found places!", 1, listOfPlacesFound.size());
        Assert.assertEquals("Place Retrieve after Update: Incorrect place note!", 
                noteUpdated, listOfPlacesFound.get(0).getNote());
    }





}
