package test.com.trekcrumb.business.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.dao.ITripDAO;
import com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImpl;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;

/**
 * DAO Component's tests for entity: Trip.
 * 
 * This test cover only BASIC use-cases to DAO CRUD functionality. For more comprehensive tests
 * regarding the entity, see the Business (Manager) test classes.
 */
public class TripDAOHibernateImplTest {
    private static final String CLASS_NAME = "TripDAOHibernateImplTest";
    private static User mUserCreated;

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        if(mUserCreated == null) {
            String username = CommonUtil.generateID(null);
            String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
            String fullname = CLASS_NAME;
            mUserCreated = TestUtilForUser.userCreate_byManager_ReadyForLogin(email, username, fullname);
        }
    }

    /***
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static  void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
        mUserCreated = null;
    }

    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForTrip.tripDelete_byDB_ALLTrips(mUserCreated.getUserId());
    }    
    
    @Test
    public void testCreateReadAndDelete_ByTripID() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        //Create:
		Trip tripCreated = TestUtilForTrip.tripCreate_byDAOImpl(
                mUserCreated.getUserId(), CLASS_NAME, 
                TestConstants.UNITTEST_TRIP_NOTE, TestConstants.UNITTEST_TRIP_LOCATION,
	            TripStatusEnum.ACTIVE, TripPrivacyEnum.PRIVATE, TripPublishEnum.PUBLISH, null,
	            true); 
        
        //User enrichment data:
        Assert.assertEquals("Trip Create: Returned Trip must have user fullname!", mUserCreated.getFullname(), tripCreated.getUserFullname());
        Assert.assertEquals("Trip Create: Returned Trip must have user username!", mUserCreated.getUsername(), tripCreated.getUsername());
        
        //Read
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(tripCreated.getId());
        List<Trip> tripListFound = 
                tripDAOImpl.read(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Incorrect number of Trips returned!", 1, tripListFound.size());
        
        Trip tripFound = tripListFound.get(0);
        Assert.assertNotNull("Returned Trip should NOT be null!", tripFound);
        Assert.assertEquals("Incorrect Trip ID!", tripToRetrieve.getId(), tripFound.getId());
        Assert.assertEquals("Incorrect user ID!", mUserCreated.getUserId(), tripFound.getUserID());
        Assert.assertEquals("Incorrect Trip name!", CLASS_NAME, tripFound.getName());
        Assert.assertEquals("Incorrect Note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
        Assert.assertEquals("Incorrect Location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
        Assert.assertEquals("Incorrect Status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
        Assert.assertEquals("Incorrect Privacy!", TripPrivacyEnum.PRIVATE, tripFound.getPrivacy());
        Assert.assertEquals("Incorrect Publish value!", TripPublishEnum.PUBLISH, tripFound.getPublish());
        Assert.assertEquals("Record's created date and updated date should have been the same!", 
                tripFound.getCreated(), tripFound.getUpdated());
        
        //Delete:
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        tripToDelete.setId(tripCreated.getId());
        int numDeleted = tripDAOImpl.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        Assert.assertEquals("Incorrect number of deleted Trip!", 1, numDeleted);
    }

    @Test
    public void testRead_ByUserID() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);

        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUserID(mUserCreated.getUserId());
        List<Trip> tripListFound = 
                tripDAOImpl.read(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null, null, 0, -1);
        Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Incorrect number of Trips returned!", 2, tripListFound.size());
    }
    
    @Test
    public void testRead_ByUsername() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
        TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);

        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setUsername(mUserCreated.getUsername());
        List<Trip> tripListFound = 
                tripDAOImpl.read(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME, null, null, 0, -1);
        Assert.assertNotNull("Returned list of retrieved Trip should NOT be null!", tripListFound);
        Assert.assertEquals("Incorrect number of Trips returned!", 2, tripListFound.size());
    }

    @Test
    public void testUpdate_ALL() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        Trip tripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);

        //Update:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        String tripNameUpdated = CLASS_NAME + "-UPDATED";
        String tripNoteUpdated = TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED";
        String tripLocUpdated = TestConstants.UNITTEST_TRIP_LOCATION + "-UPDATED";
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreated.getId());
        tripToUpdate.setName(tripNameUpdated);
        tripToUpdate.setNote(tripNoteUpdated);
        tripToUpdate.setLocation(tripLocUpdated);
        tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Trip tripUpdated = tripDAOImpl.update(tripToUpdate);
       
        //Validate:
        Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
        Assert.assertEquals("Trip Update: Invalid name!", tripNameUpdated, tripUpdated.getName());
        Assert.assertEquals("Trip Update: Invalid note!", tripNoteUpdated, tripUpdated.getNote());
        Assert.assertEquals("Trip Update: Invalid location!", tripLocUpdated, tripUpdated.getLocation());
        Assert.assertEquals("Trip Update: Invalid privacy value!", TripPrivacyEnum.PRIVATE, tripUpdated.getPrivacy());
        
        //Validate dates (special):
        Date createdDate = DateUtil.formatDate(tripUpdated.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
        Date updatedDate = DateUtil.formatDate(tripUpdated.getUpdated(), DateFormatEnum.GMT_STANDARD_LONG);
        Assert.assertTrue("Trip Update: Record's updated date should be AFTER created date!", 
                updatedDate.after(createdDate));
    }
    
    @Test
    public void testPublish() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();

        String tripID = CommonUtil.generateID(mUserCreated.getUserId());
        Trip tripToPublish = new Trip();
        tripToPublish.setId(tripID);
        tripToPublish.setUserID(mUserCreated.getUserId());
        tripToPublish.setName(CLASS_NAME + "_testPublish");
        tripToPublish.setNote(TestConstants.UNITTEST_TRIP_NOTE);
        tripToPublish.setLocation( TestConstants.UNITTEST_TRIP_LOCATION);
        tripToPublish.setStatus(TripStatusEnum.ACTIVE);
        tripToPublish.setPrivacy(TripPrivacyEnum.PUBLIC);
        tripToPublish.setPublish(TripPublishEnum.PUBLISH);
        String createdDate = DateUtil.getCurrentTimeWithGMTTimezone();
        tripToPublish.setCreated(createdDate);
        tripToPublish.setUpdated(createdDate);
        Trip tripPublished = tripDAOImpl.publish(tripToPublish);
        Assert.assertNotNull("Trip Published: Returned Trip should NOT be null!", tripPublished);
        
        //Retrieve and verify data:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(tripID);
        List<Trip> tripListFound = 
                tripDAOImpl.read(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Trip Published: Retrieved Trip should NOT be null!!", tripListFound);
        Assert.assertNotNull("Trip Published: Retrieved Trip should NOT be null!!", tripListFound.get(0));
        Trip tripFound = tripListFound.get(0);
        Assert.assertEquals("Trip Published: Invalid Trip ID!", tripID, tripFound.getId());
        Assert.assertEquals("Trip Published: Incorrect user ID!", mUserCreated.getUserId(), tripFound.getUserID());
        Assert.assertEquals("Trip Published: Incorrect Trip name!", CLASS_NAME + "_testPublish", tripFound.getName());
        Assert.assertEquals("Trip Published: Incorrect Note!", TestConstants.UNITTEST_TRIP_NOTE, tripFound.getNote());
        Assert.assertEquals("Trip Published: Incorrect Location!", TestConstants.UNITTEST_TRIP_LOCATION, tripFound.getLocation());
        Assert.assertEquals("Trip Published: Incorrect Status!", TripStatusEnum.ACTIVE, tripFound.getStatus());
        Assert.assertEquals("Trip Published: Incorrect Privacy!", TripPrivacyEnum.PUBLIC, tripFound.getPrivacy());
        Assert.assertEquals("Trip Published: Incorrect Publish value!", TripPublishEnum.PUBLISH, tripFound.getPublish());
        Assert.assertEquals("Trip Published: Incorrect created date!", createdDate, tripFound.getCreated());
        Assert.assertNull("Trip Published: Returned Trip should NOT have a Place!", tripFound.getListOfPlaces());
        Assert.assertNull("Trip Published: Returned Trip should NOT have a Picture!", tripFound.getListOfPictures());
    }
    
    @Test
    public void testReadSync() throws Exception {
        ITripDAO tripDAOImpl = new TripDAOHibernateImpl();
        
        Trip tripCreatedFirst = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
        Trip tripCreatedLast = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
        
        //Sync ALL:
        Trip tripToSync = new Trip();
        tripToSync.setUserID(mUserCreated.getUserId());
        List<Trip> listOfTripsSynced = tripDAOImpl.readSync(tripToSync);
        Assert.assertNotNull("Trip ReadSync ALL: Returned Trips should NOT be null!", listOfTripsSynced);
        Assert.assertEquals("Trip ReadSync ALL: Incorrect number of Trips returned!", 2, listOfTripsSynced.size());

        //Update tripCreatedFirst:
        Thread.sleep(1000); //Time gap due to dates sensitive validations
        Trip tripToUpdate = new Trip();
        tripToUpdate.setUserID(mUserCreated.getUserId());
        tripToUpdate.setId(tripCreatedFirst.getId());
        tripToUpdate.setNote(TestConstants.UNITTEST_TRIP_NOTE + "-UPDATED - TestRetrieveSync");
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        Trip tripUpdated = tripDAOImpl.update(tripToUpdate);
        Assert.assertEquals("Trip Update: Incorrect returned updated Trip!", 
                tripUpdated.getId(), tripCreatedFirst.getId());
        
        //Sync after last created but before last updated:
        tripToSync.setUpdated(tripCreatedLast.getUpdated());
        listOfTripsSynced = tripDAOImpl.readSync(tripToSync);
        Assert.assertNotNull("Trip ReadSync after last created: Returned Trips should NOT be null!", listOfTripsSynced);
        Assert.assertEquals("Trip ReadSync after last created: Incorrect number of Trips returned!", 1, listOfTripsSynced.size());
        Assert.assertNotNull("Trip ReadSync after last created: Incorrect number of Trips returned!", listOfTripsSynced.get(0));
        Trip tripSynced= listOfTripsSynced.get(0);
        Assert.assertEquals("Trip ReadSync after last created: Incorrect returned synced Trip!", 
                tripUpdated.getId(), tripSynced.getId());
        
        //Sync again after last update:
        tripToSync.setUpdated(tripUpdated.getUpdated());
        listOfTripsSynced = tripDAOImpl.readSync(tripToSync);
        Assert.assertNull("Trip ReadSync after last updated: Returned list of synced Trip should have been EMPTY!", listOfTripsSynced);
    }
    
}
