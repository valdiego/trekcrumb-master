package test.com.trekcrumb.business.manager;

import java.util.List;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.base.UserBaseTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUser;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;

import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Business Component's tests for entity: User.
 * @see UserBaseTest
 */
public class UserManagerTest 
extends UserBaseTest {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = UserManagerTest.class;
        mClassName = "UserManagerTest";

        UserBaseTest.oneTimeSetUp();
    }

    @Test
    public void testUserLogin_Password_NonCrypted() throws Exception {
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserLogin_Password_NonCrypted";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);
        
        User userToLogin = new User();
        userToLogin.setUsername(username);
        try {
            userToLogin.setPassword(TestConstants.UNITTEST_USER_PASSWORD);
            UserManager.userLogin(userToLogin);
            Assert.fail("User Login: The system should have thrown Exception for non-crypted password!");
        } catch(Exception e) {
            System.out.println("User Login: Expected error for non-encrypted password! " + e.getMessage());
        }

        try {
            userToLogin.setPassword("1234567890");
            UserManager.userLogin(userToLogin);
            Assert.fail("User Login: The system should have thrown Exception for non-crypted password!");
        } catch(Exception e) {
            System.out.println("User Login: Expected error for non-encrypted password! " + e.getMessage());
        }

        try {
            userToLogin.setPassword("12345678901234561234567890123456");
            UserManager.userLogin(userToLogin);
            Assert.fail("User Login: The system should have thrown Exception for non-crypted password!");
        } catch(Exception e) {
            System.out.println("User Login: Expected error for non-encrypted password! " + e.getMessage());
        }
    }
    
    @Test
    public void testUserDeactivate_withUserAuthToken() throws Exception {
        //Create:
        String username = CommonUtil.generateID(null);
        String email = username + TestConstants.UNITTEST_EMAIL_SUFFIX;
        String fullname = mClassName + "_testUserDeactivate_withUserAuthToken";
        mUserCreated = TestUtilForUser.userCreate_byManager(email, username, fullname, false);

        String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, true);
        
        String deviceIdentity_2 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_2, true);

        //Validate Before:
        UserAuthToken authTokenRetrieve = new UserAuthToken();
        authTokenRetrieve.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> authTokenList = 
                UserAuthTokenManager.retrieve(authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        Assert.assertNotNull("AuthToken Read: Returned record list shall NOT be null!", authTokenList);
        Assert.assertEquals("AuthToken Read: Invalid number of returned records!", 2, authTokenList.size());
          
        //Deactivate:
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        boolean isSuccess = UserManager.userDeActivate(userToUpdate);
        Assert.assertTrue("User Deactivate: Returned result should be success!", isSuccess);
        
        //Validate After: 
        authTokenList = UserAuthTokenManager.retrieve(authTokenRetrieve, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        if(authTokenList != null) {
            Assert.assertEquals("User Deactivate: Invalid number of UserAuthToken records found!", 
                    0, authTokenList.size());
        }
    }

}
