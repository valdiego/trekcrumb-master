package test.com.trekcrumb.business.manager;

import java.util.List;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.base.UserAuthTokenBaseTest;
import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForUserAuthToken;

import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Business Component's tests for entity: UserAuthToken.
 * @see UserAuthTokenBaseTest
 */
public class UserAuthTokenManagerTest 
extends UserAuthTokenBaseTest {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = UserAuthTokenManagerTest.class;
        mClassName = "UserAuthTokenManagerTest";

        UserAuthTokenBaseTest.oneTimeSetUp();
    }
    
    @Test
    public void testRetrieve_ByUser() throws Exception {
        //Existing:
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        
        //Retrieve:
        UserAuthToken authTokenToRead = new UserAuthToken();
        authTokenToRead.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> authTokenList = UserAuthTokenManager.retrieve(authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        Assert.assertNotNull("AuthToken Retrieve: Returned record list shall NOT be null!", authTokenList);
        Assert.assertEquals("AuthToken Retrieve: Invalid number of records!", 2, authTokenList.size());
    }
    
    @Test
    public void testRetrieve_ByAuthToken() throws Exception {
        //Create:
        String deviceIdentity = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        UserAuthToken uatCreated = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                        mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity, false);
        String authToken = uatCreated.getAuthToken();
        
        //Retrieve:
        UserAuthToken uatToRead = new UserAuthToken();
        uatToRead.setUserId(mUserCreated.getUserId());
        uatToRead.setAuthToken(uatCreated.getAuthToken());
        List<UserAuthToken> authTokenList = UserAuthTokenManager.retrieve(uatToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
        Assert.assertNotNull("AuthToken Retrieve: Returned record list shall NOT be null!", authTokenList);
        Assert.assertTrue("AuthToken Retrieve: Returned record list should have a record!", authTokenList.size() > 0);
        UserAuthToken authTokenRead =  authTokenList.get(0);
        Assert.assertNotNull("AuthToken Retrieve: Returned UserAuthToken shall have ID!", authTokenRead.getId());
        Assert.assertEquals("AuthToken Retrieve: Invalid returned UserId!", mUserCreated.getUserId(), authTokenRead.getUserId());
        Assert.assertEquals("AuthToken Retrieve: Invalid returned Username!", mUserCreated.getUsername(), authTokenRead.getUsername());
        Assert.assertEquals("AuthToken Retrieve: Invalid returned Device identity!", deviceIdentity, authTokenRead.getDeviceIdentity());
        Assert.assertEquals("AuthToken Retrieve: Invalid returned AuthToken!", authToken, authTokenRead.getAuthToken());
    }

    @Test
    public void testRetrieve_ByDevice() throws Exception {
        //Existing:
        String deviceIdentity_1 = TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null);
        UserAuthToken uatCreated_1 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), deviceIdentity_1, false);
        
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Retrieve by device:
        UserAuthToken uatToRead = new UserAuthToken();
        uatToRead.setUserId(mUserCreated.getUserId());
        uatToRead.setDeviceIdentity(deviceIdentity_1);
        List<UserAuthToken> recordList = UserAuthTokenManager.retrieve(uatToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
        Assert.assertNotNull("Returned record list shall NOT be null!", recordList);
        Assert.assertEquals("Invalid number of returned records!", 1, recordList.size());
        
        UserAuthToken uatRead = recordList.get(0);
        Assert.assertEquals("AuthToken Retrieve: Invalid returned Device Identity!", uatCreated_1.getDeviceIdentity(), uatRead.getDeviceIdentity());
        Assert.assertEquals("AuthToken Retrieve: Invalid returned AuthToken!", uatCreated_1.getAuthToken(), uatRead.getAuthToken());
    }    
    
    @Test
    public void testRetrieve_AfterDelete_ByID() throws Exception {
        UserAuthToken uatCreated = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
            mUserCreated.getUserId(), mUserCreated.getUsername(), 
            TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        
        //Delete (by ID):
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setId(uatCreated.getId());
        int numDeleted = UserAuthTokenManager.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
        Assert.assertEquals("AuthToken Delete: Invalid number of deleted record!", 1, numDeleted);    
        
        //Retrieve:
        UserAuthToken uatToRead = new UserAuthToken();
        uatToRead.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> authTokenList = UserAuthTokenManager.retrieve(uatToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        if(authTokenList != null) {
            Assert.assertEquals("AuthToken Read after Delete: Invalid number of records found!", 
                    0, authTokenList.size());
        }
    }
    
    @Test
    public void testRetrieve_AfterDelete_ByUser() throws Exception {
        //Existing:
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);

        //Delete:
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setUserId(mUserCreated.getUserId());
        int numDeleted = UserAuthTokenManager.delete(uatToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
        Assert.assertEquals("AuthToken Delete: Invalid number of deleted record!", 2, numDeleted);            
        
        //Validate after delete:
        UserAuthToken uatToRead = new UserAuthToken();
        uatToRead.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> authTokenList = UserAuthTokenManager.retrieve(uatToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        if(authTokenList != null) {
            Assert.assertEquals("AuthToken Read after Delete: Invalid number of records found!", 
                    0, authTokenList.size());
        }
    }
    
    @Test
    public void testRetrieve_AfterDelete_ByDefault() throws Exception {
        //Existing:
        UserAuthToken authTokenCreated_1 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(
                mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        UserAuthToken authTokenCreated_2 = TestUtilForUserAuthToken.userAuthTokenCreate_byManager(mUserCreated.getUserId(), mUserCreated.getUsername(), 
                TestConstants.UNITTEST_USERAUTH_DEVICE_IDENTITY + CommonUtil.generateID(null), false);
        
        //Delete by default:
        UserAuthToken uatToDelete = new UserAuthToken();
        uatToDelete.setUserId(mUserCreated.getUserId());
        uatToDelete.setId(authTokenCreated_1.getId());
        List<UserAuthToken> recordListAfterDelete = UserAuthTokenManager.delete(uatToDelete);
        
        Assert.assertNotNull("UserAuthToken Delete: Returned UserAuthToken remaining shall NOT be null!", 
                recordListAfterDelete);
        Assert.assertEquals("UserAuthToken Delete: Returned UserAuthToken remaining invalid size!", 
                1, recordListAfterDelete.size());
        Assert.assertEquals("UserAuthToken Delete: Returned UserAuthToken remaining invalid token ID!", 
                authTokenCreated_2.getId(), recordListAfterDelete.get(0).getId());
        Assert.assertEquals("UserAuthToken Delete: Returned UserAuthToken remaining invalid token value!", 
                authTokenCreated_2.getAuthToken(), recordListAfterDelete.get(0).getAuthToken());
          
        //Retrieve:
        UserAuthToken uatToRead = new UserAuthToken();
        uatToRead.setUserId(mUserCreated.getUserId());
        List<UserAuthToken> authTokenList = UserAuthTokenManager.retrieve(uatToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        Assert.assertNotNull("UserAuthToken Retrieve after Delete: Returned UserAuthToken remaining shall NOT be null!", authTokenList);
        Assert.assertEquals("UserAuthToken Retrieve after Delete: Invalid number of records found!", 1, authTokenList.size());
        
        UserAuthToken uatRead = authTokenList.get(0);
        Assert.assertEquals("UserAuthToken Retrieve after Delete: Invalid UserAuthToken ID!", 
                authTokenCreated_2.getId(), uatRead.getId());
        Assert.assertEquals("UserAuthToken Retrieve after Delete: Invalid UserAuthToken authToken value!", 
                authTokenCreated_2.getAuthToken(), uatRead.getAuthToken());
    }    

        
}
