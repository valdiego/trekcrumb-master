package test.com.trekcrumb.business.manager;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.PictureBaseTest;

/**
 * Business Component's tests for entity: Picture.
 * @see PictureBaseTest
 */
public class PictureManagerTest 
extends PictureBaseTest {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = PictureManagerTest.class;
        mClassName = "PictureManagerTest";

        PictureBaseTest.oneTimeSetUp();
    }
    

}


