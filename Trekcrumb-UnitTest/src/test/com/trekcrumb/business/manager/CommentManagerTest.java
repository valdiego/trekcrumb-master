package test.com.trekcrumb.business.manager;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForComment;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.manager.CommentManager;
import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Test cases that cover various happy-day and sad-day scenarios, thus covering all components
 * (dao, security, file systems, etc.) that is integrated by the business layer.
 * 
 */
public class CommentManagerTest {
    private static final String CLASS_NAME = "CommentManagerTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME); 
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForComment.commentDeletebyCommentDB(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreateRetrieveAndDelete_byCommentID() throws Exception {
        //Create:
        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
        String commentID = CommentManager.create(commentToCreate);
        Assert.assertNotNull("Comment Create: Returned commentID should NOT be null!", commentID);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentID);
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
        Assert.assertNotNull("Comment Retrieve: Comment entity in the list should NOT be null!", listOfComments.get(0));
        Comment commentFound = listOfComments.get(0);
        Assert.assertEquals("Comment Retrieve: Invalid returned Comment ID!", commentID, commentFound.getId());
        Assert.assertEquals("Comment Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
        Assert.assertNotNull("Comment Retrieve: Returned created date shall not be null!", commentFound.getCreated());
        
        //Enrichment data:
        Assert.assertNull("Comment Retrieve: Returned UserID should be EMPTY!", commentFound.getUserID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
        Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
        Assert.assertEquals("Comment Retrieve: Invalid returned Trip Name!", mTripCreated.getName(), commentFound.getTripName());
        
        //Delete:
        Comment commentToDelete = new Comment();
        commentToDelete.setId(commentID);
        commentToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
            
        //Retrieve and verify after delete:
        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments);
    }
    
    @Test
    public void testCreate_emptyParams() throws Exception {
        Comment commentToCreate = new Comment();
        commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
        try {
            CommentManager.create(commentToCreate);
            Assert.fail("Comment Create: Test should have failed when required params empty!");
        } catch(IllegalArgumentException iae) {
            System.out.println("Expected exception: " + iae.getMessage());
        }
        
        commentToCreate.setTripID(mTripCreated.getId());
        try {
            CommentManager.create(commentToCreate);
            Assert.fail("Comment Create: Test should have failed when required params empty!");
        } catch(IllegalArgumentException iae) {
            System.out.println("Expected exception: " + iae.getMessage());
        }
        
        commentToCreate.setTripID(null);
        commentToCreate.setUserID(mUserCreated.getUserId());
        try {
            CommentManager.create(commentToCreate);
            Assert.fail("Comment Create: Test should have failed when required params empty!");
        } catch(IllegalArgumentException iae) {
            System.out.println("Expected exception: " + iae.getMessage());
        }
    }
    
    @Test
    public void testCreate_emptyNote() throws Exception {
        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        String commentID = CommentManager.create(commentToCreate);
        Assert.assertNull("Comment Create: Returned commentID should be EMPTY when note is empty!", commentID);
        
        commentToCreate.setNote(" ");
        commentID = CommentManager.create(commentToCreate);
        Assert.assertNull("Comment Create: Returned commentID should be EMPTY when note is empty!", commentID);
    }
    
    @Test
    public void testCreate_StringWithChars() throws Exception {
        String commentNote = "CommentNote ~ ` ! @ # $ % ^ & * ( ) _ , - + = { } [] | \\ : ; \" ' < > , . ? /";

        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        commentToCreate.setNote(commentNote);
        String commentID = CommentManager.create(commentToCreate);
        Assert.assertNotNull("Comment Create: Returned commentID should NOT be null!", commentID);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentID);
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        if(listOfComments == null || listOfComments.size() == 0 || listOfComments.get(0) == null) {
            Assert.fail("Comment Create: Returned Comment should NOT be null!");
        }
        Assert.assertEquals("Comment Create: Invalid returned Comment Note!", commentNote, listOfComments.get(0).getNote());    
    }
    
    @Test
    public void testCreate_StringNewLines() throws Exception {
        String commentNote = "CommentNote \n new line CommentNote";
        
        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        commentToCreate.setNote(commentNote);
        String commentID = CommentManager.create(commentToCreate);
        Assert.assertNotNull("Comment Create: Returned commentID should NOT be null!", commentID);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentID);
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        if(listOfComments == null || listOfComments.size() == 0 || listOfComments.get(0) == null) {
            Assert.fail("Comment Create: Returned Comment should NOT be null!");
        }
        Assert.assertFalse("Comment Create: Invalid returned Comment Note!", listOfComments.get(0).getNote().contains("\n"));    
    }
    
    @Test
    public void testCreate_StringLong() throws Exception {
        String commentNote = 
                "CommentNote 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890  1234567890 1234567890 12341234567890567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 "
                + "1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 1234567890 ";

        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        commentToCreate.setNote(commentNote);
        String commentID = CommentManager.create(commentToCreate);
        Assert.assertNotNull("Comment Create: Returned commentID should NOT be null!", commentID);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentID);
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        if(listOfComments == null || listOfComments.size() == 0 || listOfComments.get(0) == null) {
            Assert.fail("Comment Create: Returned Comment should NOT be null!");
        }
        Assert.assertEquals("Comment Create: Invalid size of note!", CommonConstants.STRING_VALUE_LENGTH_MAX_COMMENT_NOTE, listOfComments.get(0).getNote().length());
        Assert.assertEquals("Comment Create: Invalid note!", 
                commentNote.substring(0, CommonConstants.STRING_VALUE_LENGTH_MAX_COMMENT_NOTE), 
                listOfComments.get(0).getNote());
    }

    @Test
    public void testCreate_StringWithNonASCIIChars() throws Exception {
        String commentNoteNonASCII = "CommentNote ���";

        Comment commentToCreate = new Comment();
        commentToCreate.setTripID(mTripCreated.getId());
        commentToCreate.setUserID(mUserCreated.getUserId());
        commentToCreate.setNote(commentNoteNonASCII);
        String commentID = CommentManager.create(commentToCreate);
        Assert.assertNotNull("Comment Create: Returned commentID should NOT be null!", commentID);
        
        //Retrieve and verify:
        String commentNoteASCII = "CommentNote oau"; 
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setId(commentID);
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
        if(listOfComments == null || listOfComments.size() == 0 || listOfComments.get(0) == null) {
            Assert.fail("Comment Create: Returned Comment should NOT be null!");
        }
        Assert.assertEquals("Comment Create: Invalid note!", commentNoteASCII, listOfComments.get(0).getNote());
    }
    
    @Test
    public void testRetrieve_emptyParams() throws Exception {
        try {
            CommentManager.retrieve(null, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }

        Comment commentToRetrieve = new Comment();
        try {
            CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
        
        try {
            CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }

        try {
            CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }
    
    @Test
    public void testRetrieveAndDelete_byUser() throws Exception {
        String commentID = TestUtilForComment.commentCreateByCommentDB(
                mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setUsername(mUserCreated.getUsername());
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
        Assert.assertNotNull("Comment Retrieve: Comment entity in the list should NOT be null!", listOfComments.get(0));
        Comment commentFound = listOfComments.get(0);
        Assert.assertEquals("Comment Retrieve: Invalid returned Comment ID!", commentID, commentFound.getId());
        Assert.assertEquals("Comment Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
        Assert.assertNotNull("Comment Retrieve: Returned created date shall not be null!", commentFound.getCreated());
        
        //Enrichment data:
        Assert.assertNull("Comment Retrieve: Returned UserID should be EMPTY!", commentFound.getUserID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
        Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
        Assert.assertEquals("Comment Retrieve: Invalid returned Trip Name!", mTripCreated.getName(), commentFound.getTripName());
        
        //Count:
        int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, numOfComments);
        
        //Delete:
        Comment commentToDelete = new Comment();
        commentToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
            
        //Retrieve and verify after delete:
        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
        Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments); 
        
        numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 0, numOfComments);        
    }

    @Test
    public void testRetrieveAndDelete_byUser_manyComments() throws Exception {
        String commentNote1 = TestConstants.UNITTEST_COMMENT_NOTE + "1";
        String commentID1 = TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), mUserCreated.getUserId(), commentNote1);
        Thread.sleep(1000);
        TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setUsername(mUserCreated.getUsername());
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, listOfComments.size());
        Comment commentFound = listOfComments.get(0);
        Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment ID!", commentID1, commentFound.getId());
        Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment Note!", commentNote1, commentFound.getNote());
        
        //Count:
        int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, numOfComments);
        
        //Delete and verify:
        Comment commentToDelete = new Comment();
        commentToDelete.setId(commentID1);
        commentToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);

        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 2, listOfComments.size());

        //Delete and verify:
        commentToDelete = new Comment();
        commentToDelete.setUserID(mUserCreated.getUserId());
        numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 2, numOfDeleted);
            
        //Retrieve and verify after delete:
        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
        Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments);        
    }    
    
    @Test
    public void testRetrieveAndDelete_byUser_manyTrips() throws Exception {
        User userOther = null;
        try {
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);

            //Create Trips:
            Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip3 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            
            //Create Comments (time sensitive to test orderBy):
            String commentNote1 = TestConstants.UNITTEST_COMMENT_NOTE + "1";
            String commentID1 = TestUtilForComment.commentCreateByCommentDB(trip1.getId(), userOther.getUserId(), commentNote1);
            Thread.sleep(1000);
            TestUtilForComment.commentCreateByCommentDB(trip2.getId(), userOther.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            Thread.sleep(1000);
            String commentNote3 = TestConstants.UNITTEST_COMMENT_NOTE + "3";
            String commentID3 = TestUtilForComment.commentCreateByCommentDB(trip3.getId(), userOther.getUserId(), commentNote3);
            
            //Retrieve with OrderByEnum.CREATED ASC (default):
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setUsername(userOther.getUsername());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, listOfComments.size());
            Comment commentFound = listOfComments.get(0);
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment ID!", commentID1, commentFound.getId());
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment TripID!", trip1.getId(), commentFound.getTripID());
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment Note!", commentNote1, commentFound.getNote());
            
            //Retrieve with OrderByEnum.CREATED DESC:
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, OrderByEnum.ORDER_BY_CREATED_DATE, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, listOfComments.size());
            commentFound = listOfComments.get(0);
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment ID!", commentID3, commentFound.getId());
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment TripID!", trip3.getId(), commentFound.getTripID());
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment Note!", commentNote3, commentFound.getNote());

            //Count:
            int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, numOfComments);

            //Delete and verify:
            Comment commentToDelete = new Comment();
            commentToDelete.setId(commentID1);
            commentToDelete.setUserID(userOther.getUserId());
            int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);

            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 2, listOfComments.size());

            //Delete and verify:
            commentToDelete = new Comment();
            commentToDelete.setUserID(userOther.getUserId());
            numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 2, numOfDeleted);
                
            //Retrieve and verify after delete:
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments);        
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }    
    
    @Test
    public void testRetrieve_byUser_pagination() throws Exception {
        int numberOfTrips = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        List<Trip> listOfTrips = new ArrayList<Trip> (0);
        User userOther = null;
        try {
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);

            //Create Trips:
            for(int i = 0; i < numberOfTrips; i++) {
                Trip trip = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
                listOfTrips.add(trip);
            }
            
            //Create Comments:
            Comment commentToCreate = new Comment();
            commentToCreate.setUserID(userOther.getUserId());
            commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
            for(int i = 0; i < numberOfTrips; i++) {
                commentToCreate.setTripID(listOfTrips.get(i).getId());
                CommentManager.create(commentToCreate);
            }
            
            //Retrieve with pagination (default is order by created ascending)
            int offset = 0;
            int numOfMaxRows = CommonConstants.RECORDS_NUMBER_MAX;
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setUsername(userOther.getUsername());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, offset, numOfMaxRows);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", numOfMaxRows, listOfComments.size());
            
            //Retrieve the next pagination (the rest):
            offset = numOfMaxRows;
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, offset, numOfMaxRows);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals(
                    "Comment Retrieve: Invalid number of found Comments!", 
                    (numberOfTrips - CommonConstants.RECORDS_NUMBER_MAX), 
                    listOfComments.size());
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }           

    @Test
    public void testRetrieve_byUser_tripBecomesPrivate() throws Exception {
        User user1 = null;
        try {
            //Create User and Trip:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            Trip trip = TestUtilForTrip.tripCreate_byManager_DefaultData(user1, CLASS_NAME);
        
            //Create:
            Comment commentToCreate = new Comment();
            commentToCreate.setTripID(trip.getId());
            commentToCreate.setUserID(user1.getUserId());
            commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
            CommentManager.create(commentToCreate);
            
            //Verify:
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setUsername(user1.getUsername());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
            
            int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, numOfComments);

            //Update Trip to private:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(user1.getUserId());
            tripToUpdate.setId(trip.getId());
            tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
            TripManager.update(tripToUpdate);
             
            //Verify:
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNull("Comment Retrieve after Trip private: Returned list of Comments should ne EMPTY!", listOfComments);
            
             numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve after Trip private: Invalid number of found Comments!", 0, numOfComments);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
        }
    }
    
    //public void testRetrieve_tripDeleted() throws Exception {
    @Test
    public void testRetrieve_byUser_tripDeleted() throws Exception {
        User user1 = null;
        try {
            //Create User and Trip:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            Trip trip = TestUtilForTrip.tripCreate_byManager_DefaultData(user1, CLASS_NAME);
        
            //Create:
            Comment commentToCreate = new Comment();
            commentToCreate.setTripID(trip.getId());
            commentToCreate.setUserID(user1.getUserId());
            commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
            CommentManager.create(commentToCreate);
           
            //Verify:
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setUsername(user1.getUsername());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
            
            int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, numOfComments);

            //Delete Trip:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(user1.getUserId());
            tripToUpdate.setId(trip.getId());
            TripManager.delete(tripToUpdate, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
             
            //Verify:
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME, null, -1, -1);
            Assert.assertNull("Comment Retrieve after Trip private: Returned list of Comments should ne EMPTY!", listOfComments);
            
             numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME);
            Assert.assertEquals("Comment Retrieve after Trip private: Invalid number of found Comments!", 0, numOfComments);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
        }
    }
    
    @Test
    public void testRetrieveAndDelete_byTripID() throws Exception {
        String commentID = TestUtilForComment.commentCreateByCommentDB(
                mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setTripID(mTripCreated.getId());
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
        Assert.assertNotNull("Comment Retrieve: Comment entity in the list should NOT be null!", listOfComments.get(0));
        Comment commentFound = listOfComments.get(0);
        Assert.assertEquals("Comment Retrieve: Invalid returned Comment ID!", commentID, commentFound.getId());
        Assert.assertEquals("Comment Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), commentFound.getTripID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Comment Note!", TestConstants.UNITTEST_COMMENT_NOTE, commentFound.getNote());
        Assert.assertNotNull("Comment Retrieve: Returned created date shall not be null!", commentFound.getCreated());
        
        //Enrichment data:
        Assert.assertNull("Comment Retrieve: Returned UserID should be EMPTY!", commentFound.getUserID());
        Assert.assertEquals("Comment Retrieve: Invalid returned Username!", mUserCreated.getUsername(), commentFound.getUsername());
        Assert.assertEquals("Comment Retrieve: Invalid returned User Fullname!", mUserCreated.getFullname(), commentFound.getUserFullname());
        Assert.assertEquals("Comment Retrieve: Invalid returned Trip Name!", mTripCreated.getName(), commentFound.getTripName());

        //Count:
        int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, numOfComments);

        //Delete:
        Comment commentToDelete = new Comment();
        commentToDelete.setTripID(mTripCreated.getId());
        int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
            
        //Retrieve and verify after delete:
        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
        Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments);        
    }    
    
    @Test
    public void testRetrieveAndDelete_byTripID_manyComments() throws Exception {
        //Create Comments:
        String commentNote1 = TestConstants.UNITTEST_COMMENT_NOTE + "1";
        String commentID1 = TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), mUserCreated.getUserId(), commentNote1);
        Thread.sleep(1000);
        TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), mUserCreated.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
        
        //Retrieve and verify:
        Comment commentToRetrieve = new Comment();
        commentToRetrieve.setTripID(mTripCreated.getId());
        List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, listOfComments.size());
       Comment commentFound = listOfComments.get(0);
        Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment ID!", commentID1, commentFound.getId());
        Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment Note!", commentNote1, commentFound.getNote());
        
        //Count:
        int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, numOfComments);
        
        //Delete and verify:
        Comment commentToDelete = new Comment();
        commentToDelete.setId(commentID1);
        commentToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);

        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
        Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
        Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 2, listOfComments.size());

        //Delete and verify:
        commentToDelete = new Comment();
        commentToDelete.setTripID(mTripCreated.getId());
        numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
        Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 2, numOfDeleted);
            
        //Retrieve and verify after delete:
        listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
        Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments);        
    }       
    
    @Test
    public void testRetrieveAndDelete_byTripID_manyUsers() throws Exception {
        User user1 = null, user2 = null, user3 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID("ONE");
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID("TWO");
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);
            
            String username3 = CommonUtil.generateID("THREE");
            user3 = TestUtilForUser.userCreate_byManager(username3 + TestConstants.UNITTEST_EMAIL_SUFFIX, username3, CLASS_NAME + "-user3", false);
            
            //Create Comments:
            String commentNote1 = TestConstants.UNITTEST_COMMENT_NOTE + "1";
            String commentID1 = TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), user1.getUserId(), commentNote1);
            Thread.sleep(1000);
            TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), user2.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), user3.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            
            //Retrieve and verify:
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setTripID(mTripCreated.getId());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, listOfComments.size());
            Comment commentFound = listOfComments.get(0);
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment ID!", commentID1, commentFound.getId());
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Username!", username1.toLowerCase(), commentFound.getUsername());
            Assert.assertEquals("Comment Retrieve: Invalid returned FIRST Comment Note!", commentNote1, commentFound.getNote());
            
            //Count:
            int numOfComments = CommentManager.count(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 3, numOfComments);

            //Delete and verify:
            Comment commentToDelete = new Comment();
            commentToDelete.setId(commentID1);
            commentToDelete.setUserID(user1.getUserId());
            int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 1, numOfDeleted);
    
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 2, listOfComments.size());
    
            //Delete and verify:
            commentToDelete = new Comment();
            commentToDelete.setTripID(mTripCreated.getId());
            numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 2, numOfDeleted);
                
            //Retrieve and verify after delete:
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNull("Comment Retrieve after Delete: Returned list of Comments should be EMPTY!!", listOfComments);        
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
            TestUtilForUser.userDelete_byDB_ALLData(user3);
        }
    }          
    
    @Test
    public void testRetrieve_byTripID_pagination() throws Exception {
        int numberOfUsers = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        List<User> listOfUsers = new ArrayList<User> (0);
        try {
            for(int i = 0; i < numberOfUsers; i++) {
                String username = CommonUtil.generateID(null);
                User user = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-user" + i, false);
                listOfUsers.add(user);
            }
            
            //Create Comments:
            Comment commentToCreate = new Comment();
            commentToCreate.setTripID(mTripCreated.getId());
            commentToCreate.setNote(TestConstants.UNITTEST_COMMENT_NOTE);
            for(int i = 0; i < numberOfUsers; i++) {
                commentToCreate.setUserID(listOfUsers.get(i).getUserId());
                CommentManager.create(commentToCreate);
            }
            
            //Retrieve with pagination 
            int offset = 0;
            int numOfMaxRows = CommonConstants.RECORDS_NUMBER_MAX;
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setTripID(mTripCreated.getId());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, offset, numOfMaxRows);
            Assert.assertNotNull("Comment Retrieve by Trip: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve by Trip: Invalid number of returned Comments!", numOfMaxRows, listOfComments.size());
            
            //Retrieve for the next pagination (the rest):
            offset = numOfMaxRows;
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, offset, numOfMaxRows);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals(
                    "Comment Retrieve: Invalid number of found Comments!", 
                    (numberOfUsers - CommonConstants.RECORDS_NUMBER_MAX), 
                    listOfComments.size());
        } finally {
            //Clean test data:
            if(listOfUsers.size() > 0) {
                for(int i = 0; i < numberOfUsers; i++) {
                    TestUtilForUser.userDelete_byDB_ALLData(listOfUsers.get(i));
                }
            }
        }
    }    
    
    @Test
    public void testRetrieve_byTripID_userDeactivated() throws Exception {
        User user1 = null, user2 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);
            
            //Create Comments:
            TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), user1.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), user2.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
           
            //Retrieve and verify:
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setTripID(mTripCreated.getId());
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 2, listOfComments.size());
          
            //Deactivate user:
            User userToDeactivate = new User();
            userToDeactivate.setUserId(user1.getUserId());
            UserManager.userDeActivate(userToDeactivate);
            
            //Verify:
            listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());            
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
        }
    }        
    
    @Test
    public void testDelete_emptyParams() throws Exception {
        try {
            CommentManager.delete(null, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }

        Comment commentToDelete = new Comment();
        try {
            CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
        
        commentToDelete = new Comment();
        commentToDelete.setId("someID");
        try {
            CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }        
        
        commentToDelete = new Comment();
        commentToDelete.setUserID(mUserCreated.getUserId());
        try {
            CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
        
        commentToDelete = new Comment();
        commentToDelete.setUserID(mUserCreated.getUserId());
        try {
            CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }

        commentToDelete = new Comment();
        commentToDelete.setTripID("someID");
        try {
            CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
            Assert.fail("Comment Retrieve: Test should have FAILED when required param is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }    
    
    //public void testDelete_invalidUser() throws Exception {
    @Test
    public void testDelete_invalidUser() throws Exception {
        User user1 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID("ONE");
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            //Create Comments:
            String commentID1 = TestUtilForComment.commentCreateByCommentDB(mTripCreated.getId(), user1.getUserId(), TestConstants.UNITTEST_COMMENT_NOTE);
            
            //Delete:
            Comment commentToDelete = new Comment();
            commentToDelete.setId(commentID1);
            commentToDelete.setUserID(mUserCreated.getUserId());
            int numOfDeleted = CommentManager.delete(commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID);
            Assert.assertEquals("Comment Delete: Invalid number of deleted Comment!", 0, numOfDeleted);
            
            //Verify:
            Comment commentToRetrieve = new Comment();
            commentToRetrieve.setId(commentID1);
            List<Comment> listOfComments = CommentManager.retrieve(commentToRetrieve, ServiceTypeEnum.COMMENT_RETRIEVE_BY_COMMENTID, null, -1, -1);
            Assert.assertNotNull("Comment Retrieve: Returned list of Comments should NOT be null!", listOfComments);
            Assert.assertEquals("Comment Retrieve: Invalid number of found Comments!", 1, listOfComments.size());
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
        }
    }          
}
