package test.com.trekcrumb.business.manager;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.TripBaseTest;

/**
 * Business Component's tests for entity: Trip.
 * @see TripBaseTest
 */
public class TripManagerTest 
extends TripBaseTest {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = TripManagerTest.class;
        mClassName = "TripManagerTest";

        TripBaseTest.oneTimeSetUp();
    }
    

}
