package test.com.trekcrumb.business.manager;

import org.junit.BeforeClass;

import test.com.trekcrumb.base.PlaceBaseTest;

/**
 * Business Component's tests for entity: Place.
 * @see PlaceBaseTest
 */
public class PlaceManagerTest 
extends PlaceBaseTest {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mTestClassType = PlaceManagerTest.class;
        mClassName = "PlaceManagerTest";

        PlaceBaseTest.oneTimeSetUp();
    }
    

}