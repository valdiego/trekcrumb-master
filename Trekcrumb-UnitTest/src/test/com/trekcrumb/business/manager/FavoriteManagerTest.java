package test.com.trekcrumb.business.manager;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForFavorite;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.manager.FavoriteManager;
import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;

/**
 * Test cases that cover various happy-day and sad-day scenarios, thus covering all components
 * (dao, security, file systems, etc.) that is integrated by the business layer.
 * 
 */
public class FavoriteManagerTest {
    private static final String CLASS_NAME = "FavoriteManagerTest";
    private static User mUserCreated;
    private static Trip mTripCreated;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(CLASS_NAME);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME); 
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
    }
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        TestUtilForFavorite.faveDeleteALLbyFavoriteDB(mUserCreated.getUserId());
    }
    
    @Test
    public void testCreateRetrieveAndDelete() throws Exception {
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        FavoriteManager.create(faveToCreate);

        //Retrieve and verify
        //1. Favorite:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(mTripCreated.getId());
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
        Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
        Assert.assertNotNull("Favorite Retrieve: Favorite entity in the list should NOT be null!", listOfFavesFound.get(0));
        Favorite faveFound = listOfFavesFound.get(0);
        Assert.assertEquals("Favorite Retrieve: Invalid returned Trip ID!", mTripCreated.getId(), faveFound.getTripID());
        Assert.assertNull("Favorite Retrieve: Returned User ID should be EMPTY!", faveFound.getUserID());
        Assert.assertNotNull("Favorite Retrieve: Returned created date shall not be null!", faveFound.getCreated());
        
        //2. Trip:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(mTripCreated.getId());
        List<Trip> tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripsFound);
        Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 1, tripsFound.size());
        Assert.assertNotNull("Trip Retrieve: Trip entity in the list should NOT be null!", tripsFound.get(0));
        Assert.assertEquals("Trip Retrieve: Invalid NUM_OF_FAVORITES value!", 1, tripsFound.get(0).getNumOfFavorites());            

        //3. User:
        User userToRetrieve = new User();
        userToRetrieve.setUserId(mUserCreated.getUserId());
        List<User> usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
        Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
        Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
        Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
        Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 1, usersFound.get(0).getNumOfFavorites());            

        //Delete:
        Favorite faveToDelete = new Favorite();
        faveToDelete.setTripID(mTripCreated.getId());
        faveToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = FavoriteManager.delete(faveToDelete);
        Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 1, numOfDeleted);

        //Retrieve and verify after delete:
        listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNull("Favorite Retrieve after Delete: Returned list of favorites should be NULL!", listOfFavesFound);

        tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertEquals("Trip Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 0, tripsFound.get(0).getNumOfFavorites());            

        usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, false, 0, 1);
        Assert.assertEquals("User Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 0, usersFound.get(0).getNumOfFavorites());            
    }
    
    @Test
    public void testCreateRetrieveAndDelete_oneTripMultipleUsers() throws Exception {
        User user1 = null, user2 = null, user3 = null;
        try {
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);

            String username3 = CommonUtil.generateID(null);
            user3= TestUtilForUser.userCreate_byManager(username3 + TestConstants.UNITTEST_EMAIL_SUFFIX, username3, CLASS_NAME + "-user3", false);
            
            //Create:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(mTripCreated.getId());
            faveToCreate.setUserID(user1.getUserId());
            FavoriteManager.create(faveToCreate);

            faveToCreate.setUserID(user2.getUserId());
            FavoriteManager.create(faveToCreate);
            
            //Retrieve and verify
            //1. Favorite:
            Favorite faveToRetrieve = new Favorite();
            faveToRetrieve.setTripID(mTripCreated.getId());
            List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 2, listOfFavesFound.size());

            faveToRetrieve.setUserID(user1.getUserId());
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
            
            faveToRetrieve.setUserID(user2.getUserId());
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());

            faveToRetrieve.setUserID(user3.getUserId());
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNull("Favorite Retrieve: Returned list of favorites should be NULL!", listOfFavesFound);

            //2. Trip:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(mTripCreated.getId());
            List<Trip> tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripsFound);
            Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 1, tripsFound.size());
            Assert.assertNotNull("Trip Retrieve: Trip entity in the list should NOT be null!", tripsFound.get(0));
            Assert.assertEquals("Trip Retrieve: Invalid NUM_OF_FAVORITES value!", 2, tripsFound.get(0).getNumOfFavorites());            
            
            //3. User1:
            User userToRetrieve = new User();
            userToRetrieve.setUserId(user1.getUserId());
            List<User> usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
            Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
            Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
            Assert.assertEquals("User Retrieve: Invalid returned Username!!", user1.getUsername(), usersFound.get(0).getUsername());            
            Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 1, usersFound.get(0).getNumOfFavorites());            
            
            //4. User2:
            userToRetrieve.setUserId(user2.getUserId());
            usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
            Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
            Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
            Assert.assertEquals("User Retrieve: Invalid returned Username!", user2.getUsername(), usersFound.get(0).getUsername());            
            Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 1, usersFound.get(0).getNumOfFavorites());            
            
            //5. User3:
            userToRetrieve.setUserId(user3.getUserId());
            usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
            Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
            Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
            Assert.assertEquals("User Retrieve: Invalid returned Username!", user3.getUsername(), usersFound.get(0).getUsername());            
            Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 0, usersFound.get(0).getNumOfFavorites());            
            
            //Delete (user1 only):
            Favorite faveToDelete = new Favorite();
            faveToDelete.setTripID(mTripCreated.getId());
            faveToDelete.setUserID(user1.getUserId());
            int numOfDeleted = FavoriteManager.delete(faveToDelete);
            Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 1, numOfDeleted);

            //Retrieve and verify after delete:
            //1. Favorite:
            faveToRetrieve.setTripID(mTripCreated.getId());
            faveToRetrieve.setUserID(null);
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve after Delete: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve after Delete: Invalid number of found favorites!", 1, listOfFavesFound.size());

            //2. Trip:
            tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertEquals("Trip Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 1, tripsFound.get(0).getNumOfFavorites());            

            //3. User1:
            userToRetrieve.setUserId(user1.getUserId());
            usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertEquals("User Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 0, usersFound.get(0).getNumOfFavorites());                   
            
            //4. User2:
            userToRetrieve.setUserId(user2.getUserId());
            usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertEquals("User Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 1, usersFound.get(0).getNumOfFavorites());                   
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
            TestUtilForUser.userDelete_byDB_ALLData(user3);
        }
    }    
    
    @Test
    public void testCreateRetrieveAndDelete_oneUserMultipleTrips() throws Exception {
        User userOther = null;
        try {
            Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip3 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);

            //Create:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setUserID(userOther.getUserId());
            faveToCreate.setTripID(trip1.getId());
            FavoriteManager.create(faveToCreate);
            
            faveToCreate.setTripID(trip2.getId());
            FavoriteManager.create(faveToCreate);
            
            //Retrieve and verify
            //1. Favorite:
            Favorite faveToRetrieve = new Favorite();
            faveToRetrieve.setUserID(userOther.getUserId());
            List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 2, listOfFavesFound.size());

            faveToRetrieve.setTripID(trip1.getId());
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
            
            faveToRetrieve.setTripID(trip2.getId());
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());

            faveToRetrieve.setTripID(trip3.getId());
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNull("Favorite Retrieve: Returned list of favorites should be EMPTY!", listOfFavesFound);

            //2. Trips:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setId(trip1.getId());
            List<Trip> tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripsFound);
            Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 1, tripsFound.size());
            Assert.assertNotNull("Trip Retrieve: Trip entity in the list should NOT be null!", tripsFound.get(0));
            Assert.assertEquals("Trip Retrieve: Invalid NUM_OF_FAVORITES value!", 1, tripsFound.get(0).getNumOfFavorites());            
            
            tripToRetrieve.setId(trip2.getId());
            tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripsFound);
            Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 1, tripsFound.size());
            Assert.assertNotNull("Trip Retrieve: Trip entity in the list should NOT be null!", tripsFound.get(0));
            Assert.assertEquals("Trip Retrieve: Invalid NUM_OF_FAVORITES value!", 1, tripsFound.get(0).getNumOfFavorites());            

            tripToRetrieve.setId(trip3.getId());
            tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripsFound);
            Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 1, tripsFound.size());
            Assert.assertNotNull("Trip Retrieve: Trip entity in the list should NOT be null!", tripsFound.get(0));
            Assert.assertEquals("Trip Retrieve: Invalid NUM_OF_FAVORITES value!", 0, tripsFound.get(0).getNumOfFavorites());            

            //3. User:
            User userToRetrieve = new User();
            userToRetrieve.setUserId(userOther.getUserId());
            List<User> usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
            Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
            Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
            Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 2, usersFound.get(0).getNumOfFavorites());            
            
            //Delete (trip1 only):
            Favorite faveToDelete = new Favorite();
            faveToDelete.setTripID(trip1.getId());
            faveToDelete.setUserID(userOther.getUserId());
            int numOfDeleted = FavoriteManager.delete(faveToDelete);
            Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 1, numOfDeleted);

            //Retrieve and verify after delete:
            //1. Favorite:
            faveToRetrieve.setUserID(userOther.getUserId());
            faveToRetrieve.setTripID(null);
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve after Delete: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve after Delete: Invalid number of found favorites!", 1, listOfFavesFound.size());

            //2. Trips:
            tripToRetrieve.setId(trip1.getId());
            tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertEquals("Trip Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 0, tripsFound.get(0).getNumOfFavorites());            

            tripToRetrieve.setId(trip2.getId());
            tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            Assert.assertEquals("Trip Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 1, tripsFound.get(0).getNumOfFavorites());            

            //3. User:
            usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
            Assert.assertEquals("User Retrieve after Delete: Invalid NUM_OF_FAVORITES value!", 1, usersFound.get(0).getNumOfFavorites());                   
        
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }
    }
        
    @Test
    public void testCreate_duplicate() throws Exception {
        //Create twice:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        FavoriteManager.create(faveToCreate);
        
        try {
            FavoriteManager.create(faveToCreate);
            Assert.fail("Favorite Create: Should have thrown ConstraintViolationException when duplicate entry!");
        } catch(org.hibernate.exception.ConstraintViolationException cve) {
            //Expected
            System.err.println("Favorite Create: Expected Error - " + cve.getMessage());
        }
        
        //Retrieve and verify
        //1. Favorite:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(mTripCreated.getId());
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve: Returned list of favorites should NOT be null!", listOfFavesFound);
        Assert.assertEquals("Favorite Retrieve: Invalid number of found favorites!", 1, listOfFavesFound.size());
        
        //2. Trip:
        Trip tripToRetrieve = new Trip();
        tripToRetrieve.setId(mTripCreated.getId());
        List<Trip> tripsFound = TripManager.retrieve(tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, -1);
        Assert.assertNotNull("Trip Retrieve: Returned list of trips should NOT be null!", tripsFound);
        Assert.assertEquals("Trip Retrieve: Invalid number of found trips!", 1, tripsFound.size());
        Assert.assertNotNull("Trip Retrieve: Trip entity in the list should NOT be null!", tripsFound.get(0));
        Assert.assertEquals("Trip Retrieve: Invalid NUM_OF_FAVORITES value!", 1, tripsFound.get(0).getNumOfFavorites());            

        //3. User:
        User userToRetrieve = new User();
        userToRetrieve.setUserId(mUserCreated.getUserId());
        List<User> usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, true, 0, 1);
        Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
        Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
        Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
        Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 1, usersFound.get(0).getNumOfFavorites());    
    }
    
    @Test
    public void testCreate_paramsEmpty() throws Exception {
        Favorite faveToCreate = new Favorite();
        try {
            FavoriteManager.create(faveToCreate);
            Assert.fail("Favorite Create: Should have thrown IllegalArgumentException when tripID and userID EMPTY!");            
        } catch(IllegalArgumentException iae) {
            //Expected
        }
        
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(null);
        try {
            FavoriteManager.create(faveToCreate);
            Assert.fail("Favorite Create: Should have thrown IllegalArgumentException when userID EMPTY!");            
        } catch(IllegalArgumentException iae) {
            //Expected
        }

        faveToCreate.setTripID(null);
        faveToCreate.setUserID(mUserCreated.getUserId());
        try {
            FavoriteManager.create(faveToCreate);
            Assert.fail("Favorite Create: Should have thrown IllegalArgumentException when tripID EMPTY!");            
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }

    @Test
    public void testCreate_tripNotExist() throws Exception {
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID("tripIDNotExist");
        faveToCreate.setUserID(mUserCreated.getUserId());
        try {
            FavoriteManager.create(faveToCreate);
            Assert.fail("Favorite Create: Should have thrown ConstraintViolationException when tripID Not Exist!");
        } catch(org.hibernate.exception.ConstraintViolationException cve) {
            //Expected
            System.err.println("Favorite Create: Expected Error - " + cve.getMessage());
        }

        //Retrieve and verify
        //1. Favorite:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID("tripIDNotExist");
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNull("Favorite Retrieve: Returned list of favorites should be NULL!", listOfFavesFound);
        
        //2. User:
        User userToRetrieve = new User();
        userToRetrieve.setUserId(mUserCreated.getUserId());
        List<User> usersFound = UserManager.userRetrieve(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, false, 0, 1);
        Assert.assertNotNull("User Retrieve: Returned list of users should NOT be null!", usersFound);
        Assert.assertEquals("User Retrieve: Invalid number of found users!", 1, usersFound.size());
        Assert.assertNotNull("User Retrieve: User entity in the list should NOT be null!", usersFound.get(0));
        Assert.assertEquals("User Retrieve: Invalid NUM_OF_FAVORITES value!", 0, usersFound.get(0).getNumOfFavorites());  
    }
    
    @Test
    public void testRetrieve_paramsEmpty() throws Exception {
        Favorite faveToRetrieve = new Favorite();
        try {
            FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.fail("Favorite Retrieve: Should have thrown IllegalArgumentException when tripID and userID EMPTY!");            
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }
    
    @Test
    public void testRetrieve_byUserID() throws Exception {
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        FavoriteManager.create(faveToCreate);

        //Retrieve and verify
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve by UserID: Returned list of favorites should NOT be null!", listOfFavesFound);
        Assert.assertEquals("Favorite Retrieve by UserID: Invalid number of found favorites!", 1, listOfFavesFound.size());
    }
    
    @Test
    public void testRetrieve_byTripID() throws Exception {
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(mTripCreated.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        FavoriteManager.create(faveToCreate);

        //Retrieve and verify
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(mTripCreated.getId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve by TripID: Returned list of favorites should NOT be null!", listOfFavesFound);
        Assert.assertEquals("Favorite Retrieve by TripID: Invalid number of found favorites!", 1, listOfFavesFound.size());
    }
    
    @Test
    public void testRetrieveAndCount_tripBecomesPrivate() throws Exception {
        User user1 = null;
        try {
            //Create User and Trip:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            Trip trip = TestUtilForTrip.tripCreate_byManager_DefaultData(user1, CLASS_NAME);

            //Create Favorite:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(trip.getId());
            faveToCreate.setUserID(user1.getUserId());
            FavoriteManager.create(faveToCreate);

            //Verify:
            Favorite faveToRetrieve = new Favorite();
            faveToRetrieve.setTripID(trip.getId());
            List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve and Count: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, listOfFavesFound.size());
            
            int numOfTrips = FavoriteManager.count(faveToRetrieve);
            Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, numOfTrips);
            
            //Update Trip to private:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(user1.getUserId());
            tripToUpdate.setId(trip.getId());
            tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
            TripManager.update(tripToUpdate);
            
            //Verify:
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve and Count: Returned list of favorites should NOT empty!", listOfFavesFound);
            
            numOfTrips = FavoriteManager.count(faveToRetrieve);
            Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, numOfTrips);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
        }
    }

    @Test
    public void testRetrieveAndCount_tripDeleted() throws Exception {
        //Create Trip:
        Trip trip = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);

        //Create Favorite:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setTripID(trip.getId());
        faveToCreate.setUserID(mUserCreated.getUserId());
        FavoriteManager.create(faveToCreate);

        //Verify:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setTripID(trip.getId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNotNull("Favorite Retrieve and Count: Returned list of favorites should NOT be null!", listOfFavesFound);
        Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, listOfFavesFound.size());
        
        int numOfTrips = FavoriteManager.count(faveToRetrieve);
        Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, numOfTrips);
        
        //Delete Trip:
        Trip tripToDelete = new Trip();
        tripToDelete.setUserID(mUserCreated.getUserId());
        tripToDelete.setId(trip.getId());
        TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
        
        //Verify:
        listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNull("Favorite Retrieve and Count: Returned list of favorites should be EMPTY!", listOfFavesFound);
        
        numOfTrips = FavoriteManager.count(faveToRetrieve);
        Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 0, numOfTrips);
    }
    
    @Test
    public void testRetrieveAndCount_userDeactivated() throws Exception {
        User user1 = null;
        try {
            //Create User:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            //Create Favorite:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(mTripCreated.getId());
            faveToCreate.setUserID(user1.getUserId());
            FavoriteManager.create(faveToCreate);
            
            //Verify:
            Favorite faveToRetrieve = new Favorite();
            faveToRetrieve.setTripID(mTripCreated.getId());
            List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNotNull("Favorite Retrieve and Count: Returned list of favorites should NOT be null!", listOfFavesFound);
            Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, listOfFavesFound.size());
            
            int numOfTrips = FavoriteManager.count(faveToRetrieve);
            Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 1, numOfTrips);
            
            //Deactivate User:
            User userToDeactivate = new User();
            userToDeactivate.setUserId(user1.getUserId());
            UserManager.userDeActivate(userToDeactivate);
            
            //Verify:
            listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNull("Favorite Retrieve and Count: Returned list of favorites should be EMPTY!", listOfFavesFound);
            
            numOfTrips = FavoriteManager.count(faveToRetrieve);
            Assert.assertEquals("Favorite Retrieve and Count: Invalid number of found favorites!", 0, numOfTrips);            
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
        }
    }    
    
    @Test
    public void testDelete_byUserID() throws Exception {
        Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
        Trip trip2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
        
        //Create:
        Favorite faveToCreate = new Favorite();
        faveToCreate.setUserID(mUserCreated.getUserId());
        faveToCreate.setTripID(trip1.getId());
        FavoriteManager.create(faveToCreate);
        
        faveToCreate.setTripID(trip2.getId());
        FavoriteManager.create(faveToCreate);

        //Delete:
        Favorite faveToDelete = new Favorite();
        faveToDelete.setUserID(mUserCreated.getUserId());
        int numOfDeleted = FavoriteManager.delete(faveToDelete);
        Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 2, numOfDeleted);
        
        //Retrieve and verify after Delete:
        Favorite faveToRetrieve = new Favorite();
        faveToRetrieve.setUserID(mUserCreated.getUserId());
        List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
        Assert.assertNull("Favorite Retrieve by UserID: Returned list of favorites should be EMPTY!", listOfFavesFound);
    }
    
    @Test
    public void testDelete_byTripID() throws Exception {
        User user1 = null, user2 = null;
        try {
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);
            
            //Create:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(mTripCreated.getId());
            faveToCreate.setUserID(user1.getUserId());
            FavoriteManager.create(faveToCreate);

            faveToCreate.setUserID(user2.getUserId());
            FavoriteManager.create(faveToCreate);

            //Delete:
            Favorite faveToDelete = new Favorite();
            faveToDelete.setTripID(mTripCreated.getId());
            int numOfDeleted = FavoriteManager.delete(faveToDelete);
            Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 2, numOfDeleted);
            
            //Retrieve and verify after Delete:
            Favorite faveToRetrieve = new Favorite();
            faveToRetrieve.setTripID(mTripCreated.getId());
            List<Favorite> listOfFavesFound = FavoriteManager.retrieve(faveToRetrieve, null, -1, -1);
            Assert.assertNull("Favorite Retrieve by TripID: Returned list of favorites should be EMPTY!", listOfFavesFound);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
        }
    }
    
    @Test
    public void testDelete_paramsEmpty() throws Exception {
        Favorite faveToDelete = new Favorite();
        try {
            FavoriteManager.delete(faveToDelete);
            Assert.fail("Favorite Delete: Should have thrown IllegalArgumentException when tripID and userID EMPTY!");            
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }
    
    @Test
    public void testDelete_noRecords() throws Exception {
        Favorite faveToDelete = new Favorite();
        faveToDelete.setTripID(mTripCreated.getId());
        faveToDelete.setTripID(mUserCreated.getUserId());
        int numOfDeleted = FavoriteManager.delete(faveToDelete);
        Assert.assertEquals("Favorite Delete: Invalid number of deleted Favorite!", 0, numOfDeleted);
    }
    
    @Test
    public void testRetrieveByTrip() throws Exception {
        User user1 = null, user2 = null, user3 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);
            
            String username3 = CommonUtil.generateID(null);
            user3 = TestUtilForUser.userCreate_byManager(username3 + TestConstants.UNITTEST_EMAIL_SUFFIX, username3, CLASS_NAME + "-user3", false);
            
            //Create Favorites:
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user1.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user2.getUserId());
            Thread.sleep(1000); //Delay the last one (time-sensitive test)
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user3.getUserId());
            
            //Retrieve and verify (default is order by created descending):
            Trip trip = new Trip();
            trip.setId(mTripCreated.getId());
            List<User> listOfUsersFound = FavoriteManager.retrieveByTrip(trip, -1, -1);
            Assert.assertNotNull("Favorite Retrieve by Trip: Returned list of users who favorite the Trip should NOT be null!", listOfUsersFound);
            Assert.assertEquals("Favorite Retrieve by Trip: Invalid returned number of users!", 3, listOfUsersFound.size());
            
            User userFirstInList = listOfUsersFound.get(0);
            Assert.assertNotNull("Favorite Retrieve by Trip: First user in list should not be null!", userFirstInList);
            Assert.assertEquals("Favorite Retrieve by Trip: First user in list invalid username!", user3.getUsername(), userFirstInList.getUsername());
            Assert.assertNotNull("Favorite Retrieve by Trip: First user in list should have Fullname!", userFirstInList.getFullname());
            
            //Validate secure User data are empty:
            for(User user : listOfUsersFound) {
                //Assert.assertNull("Favorite Read by Trip: Returned user should have EMPTY userID!", user.getUserId());
                Assert.assertNull("Favorite Retrieve by Trip: Returned user should have EMPTY password!", user.getPassword());
                Assert.assertNull("Favorite Retrieve by Trip: Returned user should have EMPTY email!", user.getEmail());
                Assert.assertNull("Favorite Retrieve by Trip: Returned user should have EMPTY securityToken!", user.getSecurityToken());
            }
            
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
            TestUtilForUser.userDelete_byDB_ALLData(user3);
        }
    }
    
    @Test
    public void testRetrieveByTrip_emptyParam() throws Exception {
        try {
            Trip trip = new Trip();
            FavoriteManager.retrieveByTrip(trip, -1, -1);
            Assert.fail("Favorite Read by Trip: Should have failed when TripID is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }
    
    @Test
    public void testRetrieveByTrip_noFavorites() throws Exception {
        Trip trip = new Trip();
        trip.setId(mTripCreated.getId());
        List<User> listOfUsersFound = FavoriteManager.retrieveByTrip(trip, -1, -1);
        Assert.assertNull("Favorite Read by Trip: Returned list of users should be EMPTY!", listOfUsersFound);
    }        
    
    @Test
    public void testRetrieveByTrip_pagination() throws Exception {
        int numberOfUsers = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        List<User> listOfUsers = new ArrayList<User> (0);
        try {
            for(int i = 0; i < numberOfUsers; i++) {
                String username = CommonUtil.generateID(null);
                User user = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-user" + i, false);
                listOfUsers.add(user);
            }
            
            //Create:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setTripID(mTripCreated.getId());
            for(int i = 0; i < numberOfUsers; i++) {
                faveToCreate.setUserID(listOfUsers.get(i).getUserId());
                FavoriteManager.create(faveToCreate);
            }
            
            //Retrieve by Trip with pagination 
            int offset = 0;
            int numOfMaxRows = CommonConstants.RECORDS_NUMBER_MAX;
            Trip trip = new Trip();
            trip.setId(mTripCreated.getId());
            List<User> listUsersFound = FavoriteManager.retrieveByTrip(trip, offset, numOfMaxRows);
            Assert.assertNotNull("Favorite Retrieve by Trip: Returned list of Users should NOT be null!", listUsersFound);
            Assert.assertEquals("Favorite Retrieve by Trip: Invalid number of returned Users!", numOfMaxRows, listUsersFound.size());
            
            //Retrieve by Trip for the next pagination (the rest):
            offset = numOfMaxRows;
            listUsersFound = FavoriteManager.retrieveByTrip(trip, offset, numOfMaxRows);
            Assert.assertNotNull("Favorite Retrieve by TripID: Returned list of favorites should NOT be null!", listUsersFound);
            Assert.assertEquals(
                    "Favorite Retrieve by Trip: Invalid number of returned Users!", 
                    (numberOfUsers - CommonConstants.RECORDS_NUMBER_MAX), 
                    listUsersFound.size());
            
        } finally {
            //Clean test data:
            if(listOfUsers.size() > 0) {
                for(int i = 0; i < numberOfUsers; i++) {
                    TestUtilForUser.userDelete_byDB_ALLData(listOfUsers.get(i));
                }
            }
        }
    }
    
    @Test
    public void testRetrieveByTrip_userDeactivated() throws Exception {
        User user1 = null, user2 = null;
        try {
            //Create Users:
            String username1 = CommonUtil.generateID(null);
            user1 = TestUtilForUser.userCreate_byManager(username1 + TestConstants.UNITTEST_EMAIL_SUFFIX, username1, CLASS_NAME + "-user1", false);
            
            String username2 = CommonUtil.generateID(null);
            user2 = TestUtilForUser.userCreate_byManager(username2 + TestConstants.UNITTEST_EMAIL_SUFFIX, username2, CLASS_NAME + "-user2", false);
            
            //Create Favorites:
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user1.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(mTripCreated.getId(), user2.getUserId());
            
            //Retrieve and verify:
            Trip trip = new Trip();
            trip.setId(mTripCreated.getId());
            List<User> listOfUsersFound = FavoriteManager.retrieveByTrip(trip, -1, -1);
            Assert.assertNotNull("Favorite Retrieve by Trip: Returned list of users who favorite the Trip should NOT be null!", listOfUsersFound);
            Assert.assertEquals("Favorite Retrieve by Trip: Invalid returned number of users!", 2, listOfUsersFound.size());
            
            //Deactivate user:
            User userToDeactivate = new User();
            userToDeactivate.setUserId(user2.getUserId());
            UserManager.userDeActivate(userToDeactivate);
            
            //Verify:
            listOfUsersFound = FavoriteManager.retrieveByTrip(trip, -1, -1);
            Assert.assertNotNull("Favorite Retrieve by Trip: Returned list of users who favorite the Trip should NOT be null!", listOfUsersFound);
            Assert.assertEquals("Favorite Retrieve by Trip: Invalid returned number of users!", 1, listOfUsersFound.size());            
            
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(user1);
            TestUtilForUser.userDelete_byDB_ALLData(user2);
        }
    }    
    
    @Test
    public void testRetrieveAndCountByUser() throws Exception {
        User userOther = null;
        try {
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);

            //Create Trips:
            Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip2 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            Trip trip3 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            
            //Create Favorites:
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip1.getId(), userOther.getUserId());
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip2.getId(), userOther.getUserId());
            Thread.sleep(1000); //Delay the last one (time-sensitive test)
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip3.getId(), userOther.getUserId());
            
            //Retrieve and verify:
            User user = new User();
            user.setUsername(username);
            List<Trip> listOfTripsFound = FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.assertNotNull("Favorite Read by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Favorite Read by User: Invalid number of returned list of trips!", 3, listOfTripsFound.size());
            
            Trip tripFirstInList = listOfTripsFound.get(0);
            Assert.assertNotNull("Favorite Retrieve by User: First Trip in list should not be null!", tripFirstInList);
            Assert.assertEquals("Favorite Retrieve by User: Trip ID invalid!", trip3.getId(), tripFirstInList.getId());
            Assert.assertEquals("Favorite Retrieve by User: Trip Name invalid!", trip3.getName(), tripFirstInList.getName());
            Assert.assertEquals("Favorite Retrieve by User: Trip Note invalid!", trip3.getNote(), tripFirstInList.getNote());
            Assert.assertEquals("Favorite Retrieve by User: Trip Location invalid!", trip3.getLocation(), tripFirstInList.getLocation());
            Assert.assertEquals("Favorite Retrieve by User: Trip User Fullname invalid!", mUserCreated.getFullname(), tripFirstInList.getUserFullname());
            Assert.assertEquals("Favorite Retrieve by User: Trip User username invalid!", mUserCreated.getUsername(), tripFirstInList.getUsername());
            
            //Count and verify:
            int numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", 3, numFound);
        
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }        
    
    @Test
    public void testRetrieveAndCountByUser_emptyParam() throws Exception {
        try {
            User user = new User();
            FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.fail("Favorite Read by User: Should have failed when Username is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
        
        try {
            User user = new User();
            FavoriteManager.countByUser(user);
            Assert.fail("Favorite Count by User: Should have failed when Username is empty!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }
    }        
    
    @Test
    public void testRetrieveAndCountByUser_noFavorites() throws Exception {
        User userOther = null;
        try {
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);
            
            //Retrieve and verify:
            User user = new User();
            user.setUsername(username);
            List<Trip> listOfTripsFound = FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.assertNull("Favorite Read by User: Returned list of trips should be EMPTY!", listOfTripsFound);
            
            //Count and verify:
            int numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", 0, numFound);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }
    
    @Test
    public void testRetrieveAndCountByUser_tripBecomesPrivate() throws Exception {
        User userOther = null;
        try {
            //Create:
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);
            Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip1.getId(), userOther.getUserId());
            
            //Retrieve and verify:
            User user = new User();
            user.setUsername(username);
            List<Trip> listOfTripsFound = FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.assertNotNull("Favorite Read by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Favorite Read by User: Invalid number of returned list of trips!", 1, listOfTripsFound.size());
            
            int numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", 1, numFound);

            //Update Trip to private:
            Trip tripToUpdate = new Trip();
            tripToUpdate.setUserID(mUserCreated.getUserId());
            tripToUpdate.setId(trip1.getId());
            tripToUpdate.setPrivacy(TripPrivacyEnum.PRIVATE);
            Trip tripUpdated = TripManager.update(tripToUpdate);
            Assert.assertNotNull("Trip Update: Returned updated Trip should NOT be null!", tripUpdated);
            Assert.assertEquals("Trip Update: Invalid privacy value!", TripPrivacyEnum.PRIVATE, tripUpdated.getPrivacy());            
            
            //Retrieve and verify:
            listOfTripsFound = FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.assertNull("Favorite Read by User: Returned list of trips should be EMPTY!", listOfTripsFound);  
            
            //Count and verify:
            numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", 0, numFound);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }
    
    @Test
    public void testRetrieveAndCountByUser_tripDeleted() throws Exception {
        User userOther = null;
        try {
            //Create:
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);
            Trip trip1 = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
            TestUtilForFavorite.faveCreateByFavoriteeDB(trip1.getId(), userOther.getUserId());
            
            //Retrieve and verify:
            User user = new User();
            user.setUsername(username);
            List<Trip> listOfTripsFound = FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.assertNotNull("Favorite Read by User: Returned list of trips should NOT be null!", listOfTripsFound);
            Assert.assertEquals("Favorite Read by User: Invalid number of returned list of trips!", 1, listOfTripsFound.size());
            
            //Count and verify:
            int numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", 1, numFound);

            //Delete Trip:
            Trip tripToDelete = new Trip();
            tripToDelete.setUserID(mUserCreated.getUserId());
            tripToDelete.setId(trip1.getId());
            TripManager.delete(tripToDelete, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
            
            //Retrieve and verify:
            listOfTripsFound = FavoriteManager.retrieveByUser(user, -1, -1);
            Assert.assertNull("Favorite Read by User after Trip Delete: Returned list of trips should be EMPTY!", listOfTripsFound);        

            //Count and verify:
            numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", 0, numFound);
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }         
    
    @Test
    public void testRetrieveAndCountByUser_pagination() throws Exception {
        int numberOfTrips = CommonConstants.RECORDS_NUMBER_MAX + (CommonConstants.RECORDS_NUMBER_MAX / 2);
        List<Trip> listOfTrips = new ArrayList<Trip> (0);
        User userOther = null;
        try {
            String username = CommonUtil.generateID(null);
            userOther = TestUtilForUser.userCreate_byManager(username + TestConstants.UNITTEST_EMAIL_SUFFIX, username, CLASS_NAME + "-userOther", false);

            //Create Trips:
            for(int i = 0; i < numberOfTrips; i++) {
                Trip trip = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, CLASS_NAME);
                listOfTrips.add(trip);
            }
            
            //Create Favorites:
            Favorite faveToCreate = new Favorite();
            faveToCreate.setUserID(userOther.getUserId());
            for(int i = 0; i < numberOfTrips; i++) {
                faveToCreate.setTripID(listOfTrips.get(i).getId());
                FavoriteManager.create(faveToCreate);
            }
            
            //Count and verify:
            User user = new User();
            user.setUsername(username);
            int numFound = FavoriteManager.countByUser(user);
            Assert.assertEquals("Favorite Count by User: Invalid number of found favorites!", numberOfTrips, numFound);

            //Retrieve with pagination (default is order by created descending)
            int offset = 0;
            int numOfMaxRows = CommonConstants.RECORDS_NUMBER_MAX;
            List<Trip> listTripsFound = FavoriteManager.retrieveByUser(user, offset, numOfMaxRows);
            Assert.assertNotNull("Favorite Retrieve by User: Returned list of favorites should NOT be null!", listTripsFound);
            Assert.assertEquals("Favorite Retrieve by User: Invalid number of found favorites!", numOfMaxRows, listTripsFound.size());
            
            //Retrieve the next pagination (the rest):
            offset = numOfMaxRows;
            listTripsFound = FavoriteManager.retrieveByUser(user, offset, numOfMaxRows);
            Assert.assertNotNull("Favorite Retrieve by User: Returned list of trips should NOT be null!", listTripsFound);
            Assert.assertEquals(
                    "Favorite Retrieve by User: Invalid number of found trips!", 
                    (numberOfTrips - CommonConstants.RECORDS_NUMBER_MAX), 
                    listTripsFound.size());
            
        } finally {
            TestUtilForUser.userDelete_byDB_ALLData(userOther);
        }        
    }
        
}
