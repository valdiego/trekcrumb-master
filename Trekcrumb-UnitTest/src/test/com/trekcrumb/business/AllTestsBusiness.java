package test.com.trekcrumb.business;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.business.dao.impl.hibernate.query.UserDBQueryTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQueryTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.query.TripDBQueryTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQueryTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQueryTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQueryTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQueryTest.class,
    
    test.com.trekcrumb.business.dao.impl.hibernate.UserDAOHibernateImplTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.UserAuthTokenDAOHibernateImplTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImplTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.PlaceDAOHibernateImplTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.PictureDAOHibernateImplTest.class,
    test.com.trekcrumb.business.dao.impl.hibernate.FavoriteDAOHibernateImplTest.class,

    test.com.trekcrumb.business.email.EmailManagerGmailImplTest.class,
    test.com.trekcrumb.business.email.EmailManagerMailgunImplTest.class,
    
    test.com.trekcrumb.business.manager.UserManagerTest.class,
    test.com.trekcrumb.business.manager.UserAuthTokenManagerTest.class,
    test.com.trekcrumb.business.manager.TripManagerTest.class,
    test.com.trekcrumb.business.manager.PlaceManagerTest.class,
    test.com.trekcrumb.business.manager.PictureManagerTest.class,
    test.com.trekcrumb.business.manager.FavoriteManagerTest.class,
    test.com.trekcrumb.business.manager.CommentManagerTest.class,
        
})
public class AllTestsBusiness {}
