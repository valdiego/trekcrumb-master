package test.com.trekcrumb.business.email;

import org.junit.Assume;
import org.junit.BeforeClass;

import com.trekcrumb.business.email.EmailManagerGmailImpl;
import com.trekcrumb.business.utility.BusinessConstants;

/**
 * @see EmailManagerTestBase
 *
 */
public class EmailManagerGmailImplTest 
extends EmailManagerTestBase {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        /*
         * Validate if system config has EmailManager set as 'GMAIL'. Ignore/halt entire
         * test suite if false.
         */
        Assume.assumeTrue(BusinessConstants.EMAIL_MANAGER_INSTANCE_GMAIL.equalsIgnoreCase(BusinessConstants.EMAIL_MANAGER_INSTANCE));
        
        //Continue:
        mEmailManagerImpl = new EmailManagerGmailImpl();
    }

}
