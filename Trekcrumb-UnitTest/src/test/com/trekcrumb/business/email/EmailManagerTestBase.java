package test.com.trekcrumb.business.email;

import junit.framework.Assert;

import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;

import com.trekcrumb.business.email.IEmailManager;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonUtil;

public abstract class EmailManagerTestBase {
    protected static IEmailManager mEmailManagerImpl;
    
    @Test
    public void testMailUserPasswordForget() throws Exception {
        User userWhoForget = new User();
        userWhoForget.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoForget.setSecurityToken(CommonUtil.generateSecurityToken());
        mEmailManagerImpl.mailUserPasswordForget(userWhoForget);
    }

    @Test
    public void testMailUserContactUs() throws Exception {
        User userWhoContactUs = new User();
        userWhoContactUs.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoContactUs.setFullname("Unit Test");
        userWhoContactUs.setSummary(TestConstants.UNITTEST_EMAIL_USERCONTACTUS_BODY);
        mEmailManagerImpl.mailUserContactUs(userWhoContactUs);
    }
    
    @Test
    public void testMailUserContactUs_emptyFullname() throws Exception {
        User userWhoContactUs = new User();
        userWhoContactUs.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoContactUs.setSummary(TestConstants.UNITTEST_EMAIL_USERCONTACTUS_BODY);
        mEmailManagerImpl.mailUserContactUs(userWhoContactUs);
    }

    @Test
    public void testMailUserContactUs_emptyParams() throws Exception {
        User userWhoContactUs = new User();
        userWhoContactUs.setSummary(TestConstants.UNITTEST_EMAIL_USERCONTACTUS_BODY);
        try {
            mEmailManagerImpl.mailUserContactUs(userWhoContactUs);
            Assert.fail("User ContactUs: Test should have FAILED when input email is EMPTY!");
        } catch(IllegalArgumentException iae) {
            //Expected
        }

        userWhoContactUs.setEmail(TestConstants.UNITTEST_EMAIL_USER);
        userWhoContactUs.setSummary("");
        try {
            mEmailManagerImpl.mailUserContactUs(userWhoContactUs);
            Assert.fail("User ContactUs: Test should have FAILED when input message is EMPTY!");
       } catch(IllegalArgumentException iae) {
            //Expected
       }
    }

}
