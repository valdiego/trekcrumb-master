package test.com.trekcrumb.business.email;

import org.junit.Assume;
import org.junit.BeforeClass;

import com.trekcrumb.business.email.EmailManagerMailgunImpl;
import com.trekcrumb.business.utility.BusinessConstants;

/**
 * @see EmailManagerTestBase
 *
 */
public class EmailManagerMailgunImplTest 
extends EmailManagerTestBase {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        /*
         * Validate if system config has EmailManager set as 'MAILGUN'. Ignore/halt entire
         * test suite if false.
         */
        Assume.assumeTrue(BusinessConstants.EMAIL_MANAGER_INSTANCE_MAILGUN.equalsIgnoreCase(BusinessConstants.EMAIL_MANAGER_INSTANCE));
        
        //Continue:
        mEmailManagerImpl = new EmailManagerMailgunImpl();
    }

}
