package test.com.trekcrumb.common.utility;

import junit.framework.Assert;

import org.junit.Test;

import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;

public class CommonUtilTest {
    
    @Test
    public void testGenerateID() {
        String idOne = CommonUtil.generateID(null);
        String idTwo = CommonUtil.generateID(null);
        
        System.out.println("ID one [" + idOne + "] vs. ID two [" + idTwo + "]");
        Assert.assertFalse("The output IDs should be unique!", idOne.equalsIgnoreCase(idTwo));
    }

    @Test
    public void testgeneratePictureFilename() {
        String filenameOne = CommonUtil.generatePictureFilename(null);
        String filenameTwo = CommonUtil.generatePictureFilename(null);
        
        System.out.println("Filename one [" + filenameOne + "] vs. Filename two [" + filenameTwo + "]");
        Assert.assertFalse("The output filenames should be unique!", filenameOne.equalsIgnoreCase(filenameTwo));
        Assert.assertEquals("Invalid filename extenstion!", 
                CommonConstants.PICTURE_FILENAME_EXT,
                filenameOne.substring(filenameOne.length() - 4));
    }
    
    @Test
    public void testGenerateSecurityToken() {
        String tokenOne = CommonUtil.generateSecurityToken();
        String tokenTwo = CommonUtil.generateSecurityToken();
        
        System.out.println("Token one [" + tokenOne + "] vs. Token two [" + tokenTwo + "]");
        Assert.assertFalse("The output Tokens should be unique!", tokenOne.equals(tokenTwo));
        Assert.assertEquals("Invalid length of token!", 
                CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SECURITY_TOKEN,
                tokenOne.length());
    }


}
