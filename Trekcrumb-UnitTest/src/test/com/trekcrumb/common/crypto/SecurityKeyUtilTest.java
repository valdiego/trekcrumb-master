package test.com.trekcrumb.common.crypto;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import javax.crypto.SecretKey;

import junit.framework.Assert;

import org.junit.Test;

import com.trekcrumb.common.crypto.SecurityKeyUtil;
import com.trekcrumb.common.crypto.StringBytesUtil;

public class SecurityKeyUtilTest {
    
    @Test
    public void testGenerateSecretKey() {
        SecretKey newSecretKey = SecurityKeyUtil.generateSecretKey();
        Assert.assertNotNull("New generated secret key should NOT be null!", newSecretKey);
        System.out.println("Generated SecretKey: \n" 
                + "Algorithm: " + newSecretKey.getAlgorithm() + "\n"
                + "Format: " + newSecretKey.getFormat() + "\n"
                + "Encoded in bytes[]: " + newSecretKey.getEncoded());
        Assert.assertEquals("Wrong secret key algorithm!", SecurityKeyUtil.SECRETKEY_ALGORITHM, newSecretKey.getAlgorithm());
        Assert.assertEquals("Wrong secret key format!", SecurityKeyUtil.SECRETKEY_FORMAT, newSecretKey.getFormat());
        
        byte[] encodedKey = newSecretKey.getEncoded();
        String encodedKeyString = StringBytesUtil.bytesToHexString(encodedKey);
        System.out.println("Generated SecretKey encoded String hex: " + encodedKeyString);
    }
    
    @Test
    public void testGenerateKeyPair() {
        KeyPair newKeyPair = SecurityKeyUtil.generateKeyPair();
        Assert.assertNotNull("New generated KeyPair should NOT be null!", newKeyPair);

        //Verify the public key:
        PublicKey publicKey = newKeyPair.getPublic();
        Assert.assertNotNull("The public key from new KeyPair should NOT be null!", publicKey);
        System.out.println("Generated PublicKey: \n" 
                + "Algorithm: " + publicKey.getAlgorithm() + "\n"
                + "Format: " + publicKey.getFormat() + "\n"
                + "Encoded in bytes[]: " + publicKey.getEncoded() + "\n"
                + "Encoded in Hex String: " + StringBytesUtil.bytesToHexString(publicKey.getEncoded()));
        
        Assert.assertEquals("Wrong public key algorithm!", SecurityKeyUtil.KEYPAIR_ALGORITHM, publicKey.getAlgorithm());
        Assert.assertEquals("Wrong public key format!", SecurityKeyUtil.KEYPAIR_FORMAT_PUBLIC, publicKey.getFormat());
        Assert.assertTrue("Invalid instance of generated public key. Expected: RSAPublicKey", publicKey instanceof RSAPublicKey);
        System.out.println("Generated PublicKey is instanceof RSAPublicKey: \n" 
                    + "Modulus: " + ((RSAPublicKey)publicKey).getModulus() + "\n"
                    + "PublicExponent: " + ((RSAPublicKey)publicKey).getPublicExponent());
        
        //Verify the private key:
        PrivateKey privateKey = newKeyPair.getPrivate();
        System.out.println("Generated PrivateKey: \n" 
                + "Algorithm: " + privateKey.getAlgorithm() + "\n"
                + "Format: " + privateKey.getFormat() + "\n"
                + "Encoded in bytes[]: " + privateKey.getEncoded()+ "\n"
                + "Encoded in Hex String: " + StringBytesUtil.bytesToHexString(privateKey.getEncoded()));
        
        Assert.assertEquals("Wrong private key algorithm!", SecurityKeyUtil.KEYPAIR_ALGORITHM, privateKey.getAlgorithm());
        Assert.assertEquals("Wrong private key format!", SecurityKeyUtil.KEYPAIR_FORMAT_PRIVATE, privateKey.getFormat());
        Assert.assertTrue("Invalid instance of generated private key. Expected: RSAPrivateKey", privateKey instanceof RSAPrivateKey);
        System.out.println("Generated PrivateKey is instanceof RSAPrivateKey: \n" 
                    + "Modulus: " + ((RSAPrivateKey)privateKey).getModulus() + "\n"
                    + "PrivateExponent: " + ((RSAPrivateKey)privateKey).getPrivateExponent());
    }
    
    @Test
    public void testGenerateSalt() {
        byte[] newSalt = SecurityKeyUtil.generateSalt();
        Assert.assertNotNull("New generated salt should NOT be null!", newSalt);
        Assert.assertEquals("Invalid size of new salt!", SecurityKeyUtil.HASH_SALT_SIZE, newSalt.length);
        System.out.println("Generated Salt: \n" 
                + "as byte[]: " + newSalt + "\n"
                + "as String hex: " + StringBytesUtil.bytesToHexString(newSalt));

        byte[] anotherNewSalt = SecurityKeyUtil.generateSalt();
        Assert.assertFalse("Generated salts should always be random and unique!",
                StringBytesUtil.compareTwoBytes(newSalt, anotherNewSalt));

    }

}
