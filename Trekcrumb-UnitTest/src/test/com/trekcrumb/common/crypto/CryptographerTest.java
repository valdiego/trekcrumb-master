package test.com.trekcrumb.common.crypto;

import java.security.Signature;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import junit.framework.Assert;

import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;

import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.crypto.SecurityKeyUtil;
import com.trekcrumb.common.crypto.StringBytesUtil;
import com.trekcrumb.common.crypto.TrekcrumbPrivateKey;

public class CryptographerTest {
    
    @Test
    public void testEncryptAndDecrypt() {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        System.out.println("Orig password: " + TestConstants.UNITTEST_USER_PASSWORD);
        String cryptedPwd = null;
        try {
            cryptedPwd = cryptoInstance.encrypt(TestConstants.UNITTEST_USER_PASSWORD);
            System.out.println("Crypted password: " + cryptedPwd);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to encrypt password: " + e.getMessage());
        }
        Assert.assertFalse("Original password and encrypted one should have NOT the same! Password: " 
                + TestConstants.UNITTEST_USER_PASSWORD + " vs. encryptedPwd: " + cryptedPwd, 
                TestConstants.UNITTEST_USER_PASSWORD.equals(cryptedPwd));
        
        String decryptedPwd = null;
        try {
            decryptedPwd = cryptoInstance.decrypt(cryptedPwd);
            System.out.println("Decrypted password: " + decryptedPwd);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to decrypt password: " + e.getMessage());
        }
        Assert.assertEquals("Original password and decrypted password should be the SAME!", 
                TestConstants.UNITTEST_USER_PASSWORD, decryptedPwd);
    }

    @Test
    public void testEncryptAndDecrypt_InvalidSecretKey() {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        System.out.println("Orig password: " + TestConstants.UNITTEST_USER_PASSWORD);
        String cryptedPwd = null;
        try {
            cryptedPwd = cryptoInstance.encrypt(TestConstants.UNITTEST_USER_PASSWORD);
            System.out.println("Crypted password: " + cryptedPwd);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to encrypt password: " + e.getMessage());
        }
        Assert.assertFalse("Original password and encrypted one should have NOT the same! Password: " 
                + TestConstants.UNITTEST_USER_PASSWORD + " vs. encryptedPwd: " + cryptedPwd, 
                TestConstants.UNITTEST_USER_PASSWORD.equals(cryptedPwd));
        
        try {
            SecretKeySpec secretKeySpec = TestConstants.TEST_HACKER_SECRET_KEY_SPEC;
            Cipher cipher = Cipher.getInstance(SecurityKeyUtil.SECRETKEY_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            
            byte[] inputBytes = StringBytesUtil.hexStringToBytes(cryptedPwd);
            cipher.doFinal(inputBytes);
            Assert.fail("Cipher decryption using hacker secret key should have been FAILED!");
        } catch(BadPaddingException bpe) {
            //It is expected.
            System.out.println("Expected error: " + bpe.getMessage());
            
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Unexpected error when trying to decrypt password with hacker key: " + e.getMessage());
        }
    }
    
    @Test
    public void testSignAndVerifySign() throws Exception {
        String originalMsg = "This is my request. Process it quick!";
        System.out.println("Orig message: " + originalMsg);

        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        String signature = null;
        try {
            signature = cryptoInstance.sign(originalMsg);
            System.out.println("Signature: " + signature);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to digital sign: " + e.getMessage());
        }
        Assert.assertNotNull("Output signature should not NULL!", signature);
        
        try {
            boolean isVerified = cryptoInstance.verifySign(originalMsg, signature);
            Assert.assertTrue("Signature should have been successfully verified!", isVerified);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to verify signature: " + e.getMessage());
        }
    }

    @Test
    public void testSignAndVerifySign_InvalidPrivateKey() throws Exception {
        String originalMsg = "This is my request. Process it quick!";
        System.out.println("Orig message: " + originalMsg);
        byte[] origMsgBytes = originalMsg.getBytes("UTF8");
        
        //Hacker side:
        TrekcrumbPrivateKey privateKey = TestConstants.TEST_HACKER_PRIVATE_KEY;
        Signature hackerSignature = Signature.getInstance("MD5WithRSA");
        hackerSignature.initSign(privateKey);
        hackerSignature.update(origMsgBytes);
        byte[] signature = hackerSignature.sign();
        String hackerSignatureHexString = StringBytesUtil.bytesToHexString(signature);
        System.out.println("Hacker signature in hex String: " + hackerSignatureHexString);
        System.out.println("Hacker signature in UTF8 String: " + new String(signature, "UTF8") );

        //Server side:
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        try {
            boolean isVerified = cryptoInstance.verifySign(originalMsg, new String(signature, "UTF8") );
            Assert.assertFalse("Hacker signature should have NOT been verified!", isVerified);
        } catch(Exception e) {
            //It is expected.
            System.out.println("Expected error: " + e.getMessage());
        }
        
        try {
            boolean isVerified = cryptoInstance.verifySign(originalMsg, hackerSignatureHexString);
            Assert.assertFalse("Hacker signature should have NOT been verified!", isVerified);
        } catch(Exception e) {
            //It is expected.
            System.out.println("Expected error: " + e.getMessage());
        }
    }
    
    @Test
    public void testSaltAndHashThenValidate() throws Exception {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        String pwdClearText = TestConstants.UNITTEST_USER_PASSWORD;
        System.out.println("Orig password: " + pwdClearText);
        String saltedAndHashedPwd = null;
        try {
            saltedAndHashedPwd = cryptoInstance.saltAndHash(pwdClearText);
            System.out.println("Salted-n-hashed password: " + saltedAndHashedPwd);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to salt-n-hash password: " + e.getMessage());
        }
        
        Assert.assertFalse("Original password and salted-n-hashed one should have NOT the same! Password: " 
                + pwdClearText + " vs. salted-n-hashed: " + saltedAndHashedPwd, 
                pwdClearText.equals(saltedAndHashedPwd));
        
        Assert.assertTrue("Validation of the password [" 
                + pwdClearText + "] against its salted-n-hashed value [" 
                + saltedAndHashedPwd + "] should have been SUCCESS!", 
                cryptoInstance.verifySaltAndHash(pwdClearText, saltedAndHashedPwd));
    }
    
    @Test
    public void testSaltAndHashThenValidate_invalidPassword() throws Exception {
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to instantiate Cryptographer: " + e.getMessage());
        }
        
        System.out.println("Orig password: " + TestConstants.UNITTEST_USER_PASSWORD);
        String saltedAndHashedPwd = null;
        try {
            saltedAndHashedPwd = cryptoInstance.saltAndHash(TestConstants.UNITTEST_USER_PASSWORD);
            System.out.println("Salted-n-hashed password: " + saltedAndHashedPwd);
        } catch(Exception e) {
            e.printStackTrace();
            Assert.fail("Fail when trying to salt-n-hash password: " + e.getMessage());
        }
        
        String invalidPwd = "jUnitPASSW00RD?";
        Assert.assertFalse("Validation of invalid password [" 
                + invalidPwd + "] against a valid salted-n-hashed value [" 
                + saltedAndHashedPwd + "] should have been FAILED!", 
                cryptoInstance.verifySaltAndHash(invalidPwd, saltedAndHashedPwd));
    }

}
