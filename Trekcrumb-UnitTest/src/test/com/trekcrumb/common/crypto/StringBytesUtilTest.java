package test.com.trekcrumb.common.crypto;

import junit.framework.Assert;

import org.junit.Test;

import com.trekcrumb.common.crypto.StringBytesUtil;

public class StringBytesUtilTest {
    
    @Test
    public void testBytesToStringAndStringToBytes_128Bits() throws Exception {
        String origHexString = "6BFE8902C7817947E20B5DCEB083AD4B";
        System.out.println("Original hexString: " + origHexString);
        
        byte[] bytesArray = StringBytesUtil.hexStringToBytes(origHexString);
        System.out.println("Output bytes: " + bytesArray);
        Assert.assertNotNull("Output byte array should NOT be null!", bytesArray);
        
        String hexString = StringBytesUtil.bytesToHexString(bytesArray);
        System.out.println("Output hexString: " + hexString);
        Assert.assertEquals("Output hexString should be the same as the original one!", origHexString, hexString);
    }

    @Test
    public void testBytesToStringAndStringToBytes_256Bits() throws Exception {
        String origHexString = "4ADC989630D99A833012CE2F409E4D7030A2774DEFFC185CBE1F142947400D93";
        System.out.println("Original hexString: " + origHexString);
        
        byte[] bytesArray = StringBytesUtil.hexStringToBytes(origHexString);
        System.out.println("Output bytes: " + bytesArray);
        Assert.assertNotNull("Output byte array should NOT be null!", bytesArray);
        
        String hexString = StringBytesUtil.bytesToHexString(bytesArray);
        System.out.println("Output hexString: " + hexString);
        Assert.assertEquals("Output hexString should be the same as the original one!", origHexString, hexString);
    }



}
