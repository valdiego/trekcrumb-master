package test.com.trekcrumb.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.common.crypto.CryptographerTest.class,
    test.com.trekcrumb.common.crypto.SecurityKeyUtilTest.class,
    test.com.trekcrumb.common.crypto.StringBytesUtilTest.class,
    
})
public class AllTestsCommon {}
