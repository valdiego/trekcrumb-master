package test.com.trekcrumb.fileserverclient;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPicture;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.FileUtil;
import com.trekcrumb.fileserverclient.IFileServerClient;

/**
 * Tests the client who will connect to the FileServer, thus in turn testing the server as well.
 *
 * WARNING:
 * By design, this test is on a client-webservice level, thus must be executable from any client computer 
 * ANYWHERE regardless the where remote server hosting the service is. Hence, there should NEVER 
 * any operation that assumed direct access to remote server's file or directory. All test ops and 
 * validations must be done thru the webservice in question.
 * 
 * In contrast, low-level components tests (such as DB, DAO or manager) requires the tests to
 * be executed at the same machine as that of the components being tested. Thus, tests expects
 * direct access/permission to the machine's file/directory etc.
 * 
 */
public abstract class FileServerClientTestBase {
    protected static String mClassName;
    protected static IFileServerClient mFSClient;
    protected String mFilepath;
    
    /**
     * Data clean-up called after each test case. 
     */
    @After
    public void tearDown() {
        /*
         * WARNING - Clean Up:
         * We delete any created file in finally() instead of overall teardown() because
         * we need to keep the variables (filename, isTripPicture) local inside test case, not
         * global static on class level. This is important for performance tests.
         */
    }
    
    /*
     * 2/2018: On 'HEROKU/GOOGLECLOUD" env., this test would fail on "retrieve after delete"
     * due to GOOGLECLOUD image caching/delete issue. 
     * See note on FileServerClientGoogleCloudStorageImpl.java
     */
    @Test
    public void testWriteReadDelete_TrekPicture() throws Exception {
        String targetFilename = CommonUtil.generatePictureFilename(mClassName);
        writeReadDelete_HappyPath(TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL, targetFilename, true);
    }
    
    /*
     * 2/2018: On 'HEROKU/GOOGLECLOUD" env., this test would fail on "retrieve after delete"
     * due to GOOGLECLOUD image caching/delete issue. 
     * See note on FileServerClientGoogleCloudStorageImpl.java
     */
    @Test
    public void testWriteReadDelete_ProfilePicture() throws Exception {
        String targetFilename = CommonUtil.generatePictureFilename(mClassName + "_Profile");
        writeReadDelete_HappyPath(TestConstants.UNITTEST_PROFILEPICTURE_FILE_JPG, targetFilename, false);
    }
    
    @Test
    public void testWrite_InvalidFileMenu() throws Exception {
        FileServerRequest request = new FileServerRequest();
        request.setFilename("dummy");
        request.setFileBytes(new byte[1]);
        
        FileServerResponse response = mFSClient.write(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
        
        request.setFileMenu(FileServerMenuEnum.READ_TREK_PICTURE);
        response = mFSClient.write(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
    }

    @Test
    public void testWrite_InvalidFilename() throws Exception {
        FileServerRequest request = new FileServerRequest();
        request.setFileMenu(FileServerMenuEnum.WRITE_TREK_PICTURE);
        request.setFileBytes(new byte[1]);
        
        FileServerResponse response = mFSClient.write(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());

        request.setFilename(" ");
        response = mFSClient.write(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
    }
    
    @Test
    public void testWrite_InvalidFilebytes() throws Exception {
        FileServerRequest request = new FileServerRequest();
        request.setFileMenu(FileServerMenuEnum.WRITE_TREK_PICTURE);
        request.setFilename("NewFile");
        
        FileServerResponse response = mFSClient.write(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
    }
    
    @Test
    public void testRead_InvalidFileMenu() throws Exception {
        FileServerRequest request = new FileServerRequest();
        request.setFilename("dummy");
        
        FileServerResponse response = mFSClient.read(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
        
        request.setFileMenu(FileServerMenuEnum.WRITE_TREK_PICTURE);
        response = mFSClient.read(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
    }    
    
    @Test
    public void testRead_invalidFilename() throws Exception {
        FileServerRequest request = new FileServerRequest();
        request.setFileMenu(FileServerMenuEnum.READ_TREK_PICTURE);
        FileServerResponse response = mFSClient.read(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());

        request.setFilename("FileNotExist.jpg");
        response = mFSClient.read(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());
    }    

    @Test
    public void testDelete_InvalidFilename() throws Exception {
        FileServerRequest request = new FileServerRequest();
        request.setFileMenu(FileServerMenuEnum.DELETE_TREK_PICTURE);
        FileServerResponse response = mFSClient.read(request);
        Assert.assertFalse("FileServer response should have been a failure!", response.isSuccess());
        Assert.assertNotNull("FileServer response should have ServiceError!", response.getServiceError());
        Assert.assertEquals("FileServer response returned unexpected service error enum! \n" + response.getServiceError(), 
                ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, response.getServiceError().getErrorEnum());
        System.out.println("Expected FileServer response error message: " + response.getServiceError().getErrorText());

        request.setFilename("FileNotExist.jpg");
        response = mFSClient.delete(request);
        Assert.assertTrue("FileServer response should have been a SUCCESS! \n" + response.getServiceError(), 
                response.isSuccess());
    }
    
    @Test
    public void testRead_ServerMultithreading() throws Exception {
        //Specific test to validate multithreading capacity of the FS server:
        int numOfRequests = 10;
        ArrayList<FileServerRequest> listOfConcurrentRequests = new ArrayList<FileServerRequest>();
        for(int i=0; i<numOfRequests; i++) {
            FileServerRequest request = new FileServerRequest();
            request.setFileMenu(FileServerMenuEnum.READ_TREK_PICTURE);
            request.setFilename("FileNotExistForMultiThreadTest_" + i + ".jpg");
            listOfConcurrentRequests.add(request);
        }
        
        for(FileServerRequest request : listOfConcurrentRequests) {
            mFSClient.read(request);
        }
    }        
    
    private void writeReadDelete_HappyPath(
            String sourceTestFilename,
            String targetFilename,
            boolean isTripPicture) throws Exception {
        //Source image: 
        String imgLocation = TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + sourceTestFilename;
        byte[] imgBytes = FileUtil.readImageFromFileDirectory(imgLocation);
        Assert.assertNotNull("Image upload: CANNOT complete the test. Image bytes from the file cannot be null: " + imgLocation, imgBytes);
        
        try {
            //Write:
            FileServerRequest requestToWrite = new FileServerRequest();
            if(isTripPicture) {
                requestToWrite.setFileMenu(FileServerMenuEnum.WRITE_TREK_PICTURE);
            } else {
                requestToWrite.setFileMenu(FileServerMenuEnum.WRITE_PROFILE_PICTURE);
            }
            requestToWrite.setFilename(targetFilename);
            requestToWrite.setFileBytes(imgBytes);
            FileServerResponse response = mFSClient.write(requestToWrite);
            if(!response.isSuccess()) {
                Assert.fail("Image Write: FileServer responded with an error: " + response.getServiceError());
            }

            //Read:
            boolean isPicImageRead = TestUtilForPicture.imageRead_byFSClient(targetFilename, isTripPicture);
            Assert.assertTrue("Image Read: FileServer should have found image: " + targetFilename, isPicImageRead);
            
            //Delete:
            TestUtilForPicture.imageDelete_byFSClient(targetFilename, isTripPicture);   
            
            //Verify after delete:
            isPicImageRead = TestUtilForPicture.imageRead_byFSClient(targetFilename, isTripPicture);
            Assert.assertFalse("Image after Delete: FileServer should have NOT found image: " + targetFilename, isPicImageRead);
            
        } finally {
            try {
                TestUtilForPicture.imageDelete_byFSClient(targetFilename, isTripPicture);   
            } catch(Exception e) {
                //Do nothing at this stage
            }
        }
    }

}
