package test.com.trekcrumb.fileserverclient;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.fileserverclient.FileServerClientSocketImplTest.class,
    test.com.trekcrumb.fileserverclient.FileServerClientWebServiceImplTest.class,
    test.com.trekcrumb.fileserverclient.FileServerClientGoogleCloudStorageImplTest.class,
    
    test.com.trekcrumb.fileserverclient.servletclient.ServletClientForFSServletTest.class,
    test.com.trekcrumb.fileserverclient.servletclient.ServletClientForGoogleCloudStorageTest.class,
    
})
public class AllTestsFSClient {}
