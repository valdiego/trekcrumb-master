package test.com.trekcrumb.fileserverclient.servletclient;

import org.junit.Assume;
import org.junit.BeforeClass;

import com.trekcrumb.common.utility.CommonConstants;

/**
 * Tests direct connections to GoogleCloud Storage "public links" implementation.
 * 
 * NOTE:
 * We do not have an "implementation client class" to specifically connect/integrate with
 * the servlet. This test acs just like any regular client that connects to internet, and
 * evaluate the responses.
 *
 */
public class ServletClientForGoogleCloudStorageTest 
extends ServletClientTestBase {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        /*
         * Validate if system config has FileServer set as 'Web Service'. Ignore/halt entire
         * test suite if false.
         */
        Assume.assumeTrue(CommonConstants.FS_SERVER_SERVLET_TYPE.equalsIgnoreCase(CommonConstants.FS_SERVER_SERVLET_TYPE_GOOGLECLOUDSTORAGE));
        
        //Continue:
        mClassName = "ServletClientForGoogleCloudStorageTest";
        ServletClientTestBase.oneTimeSetUp();
    }

    
}
