package test.com.trekcrumb.fileserverclient.servletclient;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import test.com.trekcrumb.utility.TestConstants;
import test.com.trekcrumb.utility.TestUtilForPicture;
import test.com.trekcrumb.utility.TestUtilForTrip;
import test.com.trekcrumb.utility.TestUtilForUser;

import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.FileUtil;

/**
 * Tests direct connections to File Server (storage) direct internet URLs without any client API.
 *
 * NOTE:
 * We do not have an "implementation client class" to specifically connect/integrate with
 * the servlet. This test acs just like any regular client that connects to internet, and
 * evaluate the responses.
 *
 * WARNING:
 * By design, this test is on a client-webservice level, thus must be executable from any client computer 
 * ANYWHERE regardless the where remote server hosting the service is. Hence, there should NEVER 
 * any operation that assumed direct access to remote server's file or directory. All test ops and 
 * validations must be done thru the webservice in question.
 * 
 * In contrast, low-level components tests (such as DB, DAO or manager) requires the tests to
 * be executed at the same machine as that of the components being tested. Thus, tests expects
 * direct access/permission to the machine's file/directory etc.
 * 
 */
public abstract class ServletClientTestBase {
    protected static String mClassName;
    protected static User mUserCreated;
    protected static Trip mTripCreated;
    protected static byte[] mImageJPGSmallBytes;
    protected static Picture mPictureCreated;
    protected static String mUserImageName;
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        mUserCreated = TestUtilForUser.userCreate_byManager_DefaultData(mClassName);
        mTripCreated = TestUtilForTrip.tripCreate_byManager_DefaultData(mUserCreated, mClassName); 
        
        mImageJPGSmallBytes = FileUtil.readImageFromFileDirectory(
                TestConstants.UNITTEST_PICTURE_IMAGE_SOURCE_URL + TestConstants.UNITTEST_PICTURE_FILE_JPG_SMALL);
        Assert.assertNotNull("Picture Create: Image bytes from the file cannot be null!", mImageJPGSmallBytes);
        
        mPictureCreated = TestUtilForPicture.pictureCreate_byManager(
                mClassName, mUserCreated.getUserId(), mTripCreated.getId(), mImageJPGSmallBytes, 
                null, null, TestConstants.UNITTEST_PICTURE_NOTE, true);
        
        mUserImageName = CommonUtil.generatePictureFilename(mClassName);
        User userToUpdate = new User();
        userToUpdate.setUserId(mUserCreated.getUserId());
        userToUpdate.setProfileImageBytes(mImageJPGSmallBytes);
        userToUpdate.setProfileImageName(mUserImageName);
        User userUpdated = UserManager.userUpdate(userToUpdate);
        Assert.assertNotNull("User profile image create: Returned updated User should NOT be null!", userUpdated);
        Assert.assertNotNull("User profile image create: Returned user profile image name should NOT be null!", userUpdated.getProfileImageName());
    }

    /**
     * Data clean-up after ALL test cases done.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        if(mPictureCreated != null) {
            try {
                TestUtilForPicture.imageDelete_byFSClient(mPictureCreated.getImageName(), true);
            } catch(Exception e) {
                //Ignore
                System.err.println(mClassName + ".oneTimeTearDown() - Unexpected ERROR while trying to delete Picture image: " + e.getMessage());
                e.printStackTrace();
            }
            mPictureCreated = null;
        }
        
        if(mUserCreated != null) {
            try {
                TestUtilForUser.userDelete_byDB_ALLData(mUserCreated);
            } catch(Exception e) {
                //Ignore:
                System.err.println(mClassName + ".oneTimeTearDown() - Unexpected ERROR while trying to invoke TestUtilForUser.userDeleteALLData: " + e.getMessage());
                e.printStackTrace();
            }
        }
        
        try {
            TestUtilForPicture.imageDelete_byFSClient(mUserImageName, false);
        } catch(Exception e) {
            //Ignore
            System.err.println(mClassName + ".oneTimeTearDown() - Unexpected ERROR while trying to delete User Profile image: " + e.getMessage());
            e.printStackTrace();
        }

        mUserCreated = null;
        mTripCreated = null;
        mImageJPGSmallBytes = null;
    }
    
    @Test
    public void testGet_userImage() throws Exception {
        HttpURLConnection httpConn = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            String urlAddres = CommonConstants.FS_SERVLET_URL_GETIMAGE_PROFILE + mUserImageName;
            httpConn = connectServer("GET", urlAddres);
            
            //Evaluate response:
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            if(httpConn.getResponseCode() != HttpServletResponse.SC_OK) {
                Assert.fail("Returned response code invalid: " + httpConn.getResponseCode() + ":" + httpConn.getResponseMessage());
            }
            Assert.assertEquals("Returned Content-Type invalid!", "image/jpeg", httpConn.getContentType());

            /*
            //TODO: HOW TO READ IMAGE BYTES AND COMPARE IT TO THE ACTUAL IMAGE? 
            inputStream = httpConn.getInputStream();
            byte[] buffer = new byte[4096];
            int n = - 1;
            outputStream = new FileOutputStream();
            while ( (n = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, n);
            }
            */
            
        } finally {
            if(inputStream != null) {
                inputStream.close();
            }
            if(outputStream != null) {
                outputStream.close();
            }
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }
    }
    
    @Test
    public void testGet_pictureImage() throws Exception {
        HttpURLConnection httpConn = null;
        try {
            String urlAddres = CommonConstants.FS_SERVLET_URL_GETIMAGE_PICTURE + mPictureCreated.getImageName();
            httpConn = connectServer("GET", urlAddres);
            
            //Evaluate response:
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            if(httpConn.getResponseCode() != HttpServletResponse.SC_OK) {
                Assert.fail("Returned response code invalid: " + httpConn.getResponseCode() + ":" + httpConn.getResponseMessage());
            }
            Assert.assertEquals("Returned Content-Type invalid!", "image/jpeg", httpConn.getContentType());
        } finally {
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }    
    }

    protected static HttpURLConnection connectServer(String requestMethod, String urlAddress) throws Exception {
        System.out.println("FileServerServletTest.connectServer() - Opening and sending connection for [" + requestMethod + "] to target URL [" + urlAddress + "]");
        
        URL url = new URL(urlAddress);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(requestMethod);
        conn.setConnectTimeout(CommonConstants.NETWORK_TIMEOUT_CONNECTION);
        conn.setReadTimeout(CommonConstants.NETWORK_TIMEOUT_READ);
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 Chrome/49.0.2623.87 Safari/537.36");
        return conn;
    }
    
}
