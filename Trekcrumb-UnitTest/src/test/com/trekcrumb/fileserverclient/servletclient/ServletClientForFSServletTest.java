package test.com.trekcrumb.fileserverclient.servletclient;

import java.net.HttpURLConnection;

import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import com.trekcrumb.common.utility.CommonConstants;

/**
 * @see ServletClientTestBase
 * 
 * NOTE:
 * This test class has more test cases that are only applicable to Trekcrumb FileServer as Servlet
 * implementation.
 */
public class ServletClientForFSServletTest 
extends ServletClientTestBase {

    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        /*
         * Validate if system config has FileServer set as 'Web Service'. Ignore/halt entire
         * test suite if false.
         */
        Assume.assumeTrue(CommonConstants.FS_SERVER_SERVLET_TYPE.equalsIgnoreCase(CommonConstants.FS_SERVER_SERVLET_TYPE_TREKCRUMBFS));
        
        //Continue:
        mClassName = "ServletClientForFSServletTest";
        ServletClientTestBase.oneTimeSetUp();
    }

    @Test
    public void testGet_paramsMissing() throws Exception {
        HttpURLConnection httpConn = null;
        try {
            //Missing imageType:
            String urlAddres = CommonConstants.FS_SERVLET_HOST;
            httpConn = connectServer("GET", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_BAD_REQUEST, httpConn.getResponseCode());
            
            //Missing imageName:
            urlAddres = CommonConstants.FS_SERVLET_HOST 
                    + CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE + "=" + CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE_PICTURE;
            httpConn = connectServer("GET", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_BAD_REQUEST, httpConn.getResponseCode());
            
            urlAddres = CommonConstants.FS_SERVLET_HOST 
                    + CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE + "=" + CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE_PICTURE
                    + "&"
                    + "notValidParamName=" +  mUserImageName;
            httpConn = connectServer("GET", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_BAD_REQUEST, httpConn.getResponseCode());
        } finally {
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }    
    }
    
    @Test
    public void testGet_paramsInvalid() throws Exception {
        HttpURLConnection httpConn = null;
        try {
            //Invalid imageType:
            String urlAddres = CommonConstants.FS_SERVLET_HOST 
                    + CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE + "=invalidImageType"
                    + "&"
                    + CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_NAME + "=" +  mUserImageName;
            httpConn = connectServer("GET", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_BAD_REQUEST, httpConn.getResponseCode());
            
        } finally {
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }    
    }
    
    @Test
    public void testGet_userImage_notFound() throws Exception {
        HttpURLConnection httpConn = null;
        try {
            String urlAddres = CommonConstants.FS_SERVLET_URL_GETIMAGE_PROFILE + "notExist.JPG";
            httpConn = connectServer("GET", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_NOT_FOUND, httpConn.getResponseCode());
        } finally {
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }    
    }
    
    @Test
    public void testGet_pictureImage_notFound() throws Exception {
        HttpURLConnection httpConn = null;
        try {
            String urlAddres = CommonConstants.FS_SERVLET_URL_GETIMAGE_PICTURE + "notExist.JPG";
            httpConn = connectServer("GET", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_NOT_FOUND, httpConn.getResponseCode());
        } finally {
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }    
    }
    
    @Test
    public void testPost() throws Exception {
        HttpURLConnection httpConn = null;
        try {
            String urlAddres = CommonConstants.FS_SERVLET_HOST;
            httpConn = connectServer("POST", urlAddres);
            Assert.assertNotNull("Returned HTTP Connection should NOT be null!", httpConn);
            Assert.assertEquals("Returned response code invalid!", HttpServletResponse.SC_METHOD_NOT_ALLOWED, httpConn.getResponseCode());
        } finally {
            if(httpConn != null) {
                httpConn.disconnect();
                httpConn = null;
            }
        }    
    }
    
}
