package test.com.trekcrumb.fileserverclient;

import org.junit.Assume;
import org.junit.BeforeClass;

import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.fileserverclient.FileServerClientSocketImpl;

/**
 * @see FileServerClientTestBase
 *
 */
public class FileServerClientSocketImplTest 
extends FileServerClientTestBase {
    
    /**
     * Data setup to be used by ALL test cases.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        /*
         * Validate if system config has FileServer set as 'Server Socket'. Ignore/halt entire
         * test suite if false.
         */
        Assume.assumeTrue(CommonConstants.FS_SERVER_TYPE.equalsIgnoreCase(CommonConstants.FS_SERVER_TYPE_SERVERSOCKET));
        
        //Continue:
        mClassName = "FileServerClientWebServiceImplTest";
        mFSClient = new FileServerClientSocketImpl();
    }

}
