package test.com.trekcrumb;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * TestSuitesForClient - Contains and Runs ONLY selected test cases that mirror 'high-level'
 * client calls (such as web services, etc.). It excludes low-level component test cases
 * that client machine may not have access to pass the tests.
 * 
 * @author Val Triadi
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.webservice.AllTestsWebService.class,
    test.com.trekcrumb.webserviceclient.AllTestsWebServiceClient.class,
    test.com.trekcrumb.fileserverclient.AllTestsFSClient.class,
    
    /* 
     * TODO
     * The following tests are on lower-level (Business component) but: 
     * (1) Their test cases are identical with test cases for WebServiceClient component
     * (2) But NOT all of them are implemented in the WebServcieClient tests (need refactoring)
     * 
     *  Hence, we must include them here to provide more complete test coverage on this Suite (TODO!)
     *  Nonetheless, we ensure none of these low-level tests do any low-level access that could
     *  not be accomplished from client machine.
     */
    test.com.trekcrumb.business.manager.FavoriteManagerTest.class,
    test.com.trekcrumb.business.manager.CommentManagerTest.class,
  
})
public class TestSuitesForClient {}
