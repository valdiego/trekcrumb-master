package test.com.trekcrumb;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * TestSuites - Contains and Runs ALL unit test cases.
 * 
 * @author Val Triadi
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    test.com.trekcrumb.common.AllTestsCommon.class,
    test.com.trekcrumb.business.AllTestsBusiness.class,
    test.com.trekcrumb.webservice.AllTestsWebService.class,
    test.com.trekcrumb.webserviceclient.AllTestsWebServiceClient.class,
    test.com.trekcrumb.fileserverclient.AllTestsFSClient.class,
    test.com.trekcrumb.perftestsingle.AllTestsPerfTestSingle.class,
})
public class TestSuites {}
