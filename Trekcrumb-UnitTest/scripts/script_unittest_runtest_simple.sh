#!/bin/sh
#######################################
# UNIX shell script to start UnitTest > UserWSClientTest.unittest_start_testUserCreateAndConfirmAndLogin()
# Note:
# 1. To execute, from UNIX (bourne) shell, 
#    - Give proper permission: chmod 755 <script_filename>
#    - Type: ./<script_filename>
# 2. You may execute build.xml target: 'ant runtest' command but it requires ANT to be intalled in 
#    the environment. Otherwise, use the 'java' command as default.
#######################################
LIB_DIR=libs
export CLASSPATH=.:$LIB_DIR/junit_4.10.0.jar:$LIB_DIR/junitperf-1.9.1.jar:$LIB_DIR/org.hamcrest.core_1.1.0.jar::$LIB_DIR/JUnitXmlFormatter.jar
export CLASSPATH=$CLASSPATH:$LIB_DIR/Trekcrumb-UnitTest.jar:$LIB_DIR/Trekcrumb-Common.jar:$LIB_DIR/Trekcrumb-Business.jar:$LIB_DIR/Trekcrumb-WebServiceClient.jar:$LIB_DIR/Trekcrumb-FileServerClient.jar
export CLASSPATH=$CLASSPATH:$LIB_DIR/antlr-2.7.7.jar:$LIB_DIR/dom4j-1.6.1.jar:$LIB_DIR/hibernate-commons-annotations-4.0.1.Final.jar:$LIB_DIR/hibernate-core-4.1.7.Final.jar:$LIB_DIR/javassist-3.15.0-GA.jar:$LIB_DIR/javax-transaction-api_1.1_spec-1.0.0.Final.jar:$LIB_DIR/jboss-logging-3.1.0.GA.jar:$LIB_DIR/javax-jpa-2.0-api-1.0.1.Final.jar
export CLASSPATH=$CLASSPATH:$LIB_DIR/mysql-connector-java-5.1.22-bin.jar:$LIB_DIR/jackson-core-asl-1.9.13.jar:$LIB_DIR/jackson-mapper-asl-1.9.13.jar
export CLASSPATH=$CLASSPATH:$LIB_DIR/commons-logging-1.1.1.jar:$LIB_DIR/commons-codec-1.10.jar:$LIB_DIR/javax.mail_1.5.5.jar

#### RUN TESTS ####
java test.com.trekcrumb.utility.TestSingleTestCase test.com.trekcrumb.webserviceclient.UserWSClientTest#testUserCreateAndConfirmAndLogin