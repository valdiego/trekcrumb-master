@ECHO OFF
REM #######################################
REM # WINDOWS script to start Unit Test
REM # Note:
REM # 1. You may execute build.xml target: 'ant runtest' command but it requires ant intalled in 
REM      the environment. Otherwise, use the 'java' command as default.
REM #######################################
SET LIB_DIR=libs
SET CLASSPATH=%CLASSPATH%;%LIB_DIR%/junit_4.10.0.jar;%LIB_DIR%/junitperf-1.9.1.jar;%LIB_DIR%/org.hamcrest.core_1.1.0.jar;%LIB_DIR%/JUnitXmlFormatter.jar
SET CLASSPATH=%CLASSPATH%;%LIB_DIR%/Trekcrumb-UnitTest.jar;%LIB_DIR%/Trekcrumb-Common.jar;%LIB_DIR%/Trekcrumb-Business.jar;%LIB_DIR%/Trekcrumb-WebServiceClient.jar;%LIB_DIR%/Trekcrumb-FileServerClient.jar
SET CLASSPATH=%CLASSPATH%;%LIB_DIR%/antlr-2.7.7.jar;%LIB_DIR%/dom4j-1.6.1.jar;%LIB_DIR%/hibernate-commons-annotations-4.0.1.Final.jar;%LIB_DIR%/hibernate-core-4.1.7.Final.jar;%LIB_DIR%/javassist-3.15.0-GA.jar;%LIB_DIR%/javax-transaction-api_1.1_spec-1.0.0.Final.jar;%LIB_DIR%/jboss-logging-3.1.0.GA.jar;%LIB_DIR%/javax-jpa-2.0-api-1.0.1.Final.jar
SET CLASSPATH=%CLASSPATH%;%LIB_DIR%/mysql-connector-java-5.1.22-bin.jar;%LIB_DIR%/jackson-core-asl-1.9.13.jar;%LIB_DIR%/jackson-mapper-asl-1.9.13.jar
SET CLASSPATH=%CLASSPATH%;%LIB_DIR%/commons-logging-1.1.1.jar;%LIB_DIR%/commons-codec-1.10.jar;%LIB_DIR%/javax.mail_1.5.5.jar
SET CLASSPATH=%CLASSPATH%;%LIB_DIR%/google-api-client-1.23.0.jar;%LIB_DIR%/google-api-client-jackson2-1.23.0.jar;%LIB_DIR%/google-api-services-storage-v1-rev116-1.23.0.jar;%LIB_DIR%/google-http-client-1.23.0.jar;%LIB_DIR%/google-http-client-jackson2-1.23.0.jar;%LIB_DIR%/google-oauth-client-1.23.0.jar;%LIB_DIR%/google-oauth-client-java6-1.23.0.jar;%LIB_DIR%/google-oauth-client-jetty-1.23.0.jar;%LIB_DIR%/jackson-core-2.1.3.jar

REM #### RUN TESTS ####
REM # 1. WITH ANT TARGET
REM ant runTest
REM ant runTest -DtestClass=test.com.trekcrumb.webserviceclient.UserWSClientTest
REM ant runTest -DtestClass=test.com.trekcrumb.webserviceclient.UserWSClientTest -DtestMethod=testUserCreate_Fullname_WithNonASCIIChars

REM # 2. WITH REGULAR JAVA (NO REPORT FILE)
REM java org.junit.runner.JUnitCore test.com.trekcrumb.TestSuites

REM # 3. WITH SINGLE TEST CASE EXECUTOR (NO REPORT FILE)
REM java test.com.trekcrumb.utility.TestSingleTestCase test.com.trekcrumb.webserviceclient.UserWSClientTest#testUserCreateAndConfirmAndLogin
REM java test.com.trekcrumb.utility.TestSingleTestCase test.com.trekcrumb.fileserverclient.FileServerClientGoogleCloudStorageImplTest#testWriteReadDelete_TripPicture_ByFilename

REM # 4. WITH 3RD PARTY LIB AND GENERATE REPORT FILE
REM java -Dorg.schmant.task.junit4.target=TestReports.xml barrypitman.junitXmlFormatter.Runner test.com.trekcrumb.TestSuites
REM java -Dorg.schmant.task.junit4.target=TestReports.xml barrypitman.junitXmlFormatter.Runner test.com.trekcrumb.TestSuitesForClient
java -Dorg.schmant.task.junit4.target=TestReports.xml barrypitman.junitXmlFormatter.Runner test.com.trekcrumb.webserviceclient.UserWSClientTest

REM #### REPORT TEST SUMMARY ####
java test.com.trekcrumb.utility.TestReporter
