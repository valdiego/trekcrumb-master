<?xml version="1.0" encoding="UTF-8"?>
<project name="Trekcrumb-WebService" basedir="." default="buildWAR">

<!-- *********************
     Property/Config Files
     ********************* -->
    <property file="../Base/properties/build.properties"/>
    <property file="../Base/properties/env.properties" prefix="ENV"/>
    
<!-- ******************
     Libraries
     ****************** -->
    <path id="library.source">
        <!-- All JARs in the lib are part of source-code and required to only compile (not run) the app. -->
        <fileset dir="${base-dir.lib.apachecfx}">
            <include name="**/*"/>
        </fileset>
        <fileset dir="${base-dir.lib.spring}">
            <include name="org.springframework.aop-3.1.3.RELEASE.jar"/>
            <include name="org.springframework.asm-3.1.3.RELEASE.jar"/>
            <include name="org.springframework.beans-3.1.3.RELEASE.jar"/>
            <include name="org.springframework.context-3.1.3.RELEASE.jar"/>
            <include name="org.springframework.core-3.1.3.RELEASE.jar"/>
            <include name="org.springframework.expression-3.1.3.RELEASE.jar"/>
            <include name="org.springframework.web-3.1.3.RELEASE.jar"/>
        </fileset>
        <fileset dir="${base-dir.lib.javax}">
            <include name="javax.ws.rs-api-2.0-m10.jar"/>
        </fileset>        
        <fileset dir="${base-dir.lib.common}">
            <include name="commons-logging-1.1.1.jar"/>
        </fileset>
        <fileset dir="${base-dir.lib.json}">
            <include name="jackson-core-asl-1.9.13.jar"/>
            <include name="jackson-mapper-asl-1.9.13.jar"/>
            <include name="jackson-jaxrs-1.9.13.jar"/>
        </fileset>
    </path>
    <path id="library.subProjects">
        <fileset dir="${base-dir.distribution}">
            <include name="${common-jarName}"/>
            <include name="${business-jarName}"/>
        </fileset>
    </path>
    
<!-- ******************
     Targets 
     ****************** -->
    <target name="deleteClasses">
        <delete includeemptydirs="true">
            <fileset dir="${webservice-dir.build}">
                <include name="**/*"/>
            </fileset>
        </delete>
    </target>
    <target name="cleanWEBINF">
        <delete includeemptydirs="true">
            <fileset dir="${webservice-dir.webinf}">
                <include name="**/*"/>
            </fileset>
        </delete>
    </target>
    <target name="deleteWAR">
        <delete file="${base-dir.distribution}/${webservice-warName}"/>
    </target>
    <target name="deleteALL" depends="deleteClasses, deleteWAR, cleanWEBINF" />
    
    <target name="compileJava">
        <mkdir dir="${webservice-dir.build}"/>
        <javac destdir="${webservice-dir.build}" 
               includeantruntime="false"
               debug="true"
               failonerror="true">
            <src path="${webservice-dir.src}"/>
            <classpath>
                <path refid="library.source" />
                <path refid="library.subProjects" />
            </classpath>
        </javac>
    </target>
    
    <!-- Private target -->
    <target name="-buildWEBINF">
        <mkdir dir="${webservice-dir.webinf}"/>
        <mkdir dir="${webservice-dir.webinf.classes}"/>
        <mkdir dir="${webservice-dir.webinf.lib}"/>
        <copy todir="${webservice-dir.webinf}">
            <fileset dir="${webservice-dir.properties}">
                <include name="**/*.xml"/>
            </fileset>
        </copy>
        <copy todir="${webservice-dir.webinf.classes}">
            <fileset dir="${webservice-dir.build}">
                <include name="**/*"/>
            </fileset>
            <fileset dir="${webservice-dir.properties}">
                <include name="commons-logging.properties"/>
                <include name="log4j.properties"/>
            </fileset>
        </copy>
        
        <copy todir="${webservice-dir.webinf.lib}">
            <!-- TREKCRUMBS COMPONENTS -->
            <fileset dir="${base-dir.distribution}">
                <include name="${common-jarName}"/>
                <include name="${fsclient-jarName}"/>
                <include name="${business-jarName}"/>
            </fileset>
            
            <!-- JARS FOR CFX WEBSERVICE -->
            <fileset dir="${base-dir.lib.apachecfx}">
                <include name="**/*"/>
            </fileset>
            <fileset dir="${base-dir.lib.common}">
                <include name="commons-logging-1.1.1.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.log4j}">
                <include name="log4j-1.2.17.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.spring}">
                <include name="org.springframework.aop-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.asm-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.beans-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.context-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.core-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.expression-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.web-3.1.3.RELEASE.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.javax}">
                <include name="javax.ws.rs-api-2.0-m10.jar"/>
            </fileset>        
            
            <fileset dir="${base-dir.lib.json}">
                <include name="jackson-core-asl-1.9.13.jar"/>
                <include name="jackson-mapper-asl-1.9.13.jar"/>
                <include name="jackson-jaxrs-1.9.13.jar"/>
            </fileset>
 
            <!-- JARS FOR HIBERNATE (USED BY TreakCrumbs-Business) -->
            <fileset dir="${base-dir.lib.hibernate}">
                <include name="**/*.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.mysql}">
                <include name="mysql-connector-java-5.1.22-bin.jar"/>
            </fileset>
            
            <!--  JARS FOR JAVAMAIL (USED BY TreakCrumbs-Business) -->
            <fileset dir="${base-dir.lib.javax}">
                <include name="javax.mail_1.5.5.jar"/>
            </fileset>
            
            <!--  JARS FOR GOOGLE CLOUD STORAGE API (FileServer) -->
            <fileset dir="${base-dir.lib}/google" includes="**/*.jar" />
        </copy>
    </target>
    
    <target name="buildWAR">
        <echo message="**** BUILDING WEBSERVICE COMPONENT ****"/>
        <antcall target="deleteALL" />
        <antcall target="compileJava" />
        <antcall target="-buildWEBINF" />
        <war destfile="${base-dir.distribution}/${webservice-warName}"
             webxml="${webservice-dir.webinf}/web.xml">
            <fileset dir="${webservice-dir.web}">
                <include name="**/*.*"/>
            </fileset>
            <manifest>
                <attribute name="Solution-Name" value="${webservice-appName}"/>
                <attribute name="Built-By" value="BeachPorch Software Inc."/>
                <attribute name="Implementation-Vendor" value="BeachPorch Software Inc."/>
            </manifest>
        </war>
    </target>
    
    <target name="deployWAR-Local" depends="buildWAR">
        <echo message="Deploying WebService to Local Web Server ..."/>
        
        <delete file="${ENV.env.properties.webservice.deployDir.LOCAL}/${webservice-warName}"/>
        <delete dir="${ENV.env.properties.webservice.deployDir.LOCAL}/Trekcrumb-WebService"/>
        
        <copy todir="${ENV.env.properties.webservice.deployDir.LOCAL}" 
              preservelastmodified="true">
            <fileset dir="${base-dir.distribution}">
                <include name="${webservice-warName}"/>
            </fileset>
        </copy>
    </target>
    
</project>