package com.trekcrumb.webservice;

import javax.jws.WebService;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;

/**
 * Interface for Comment Service.
 * NOTE:
 * 1. For CFX WebService SOAP implementation, it is required to declare the interface with @WebService tag.
 * 
 * @author Val Diego
 * @since 02/2016
 */
@WebService
public interface ICommentService {
    public ServiceResponse create(ServiceRequest request);
    public ServiceResponse retrieve(ServiceRequest request);
    public ServiceResponse delete(ServiceRequest request);
}
