package com.trekcrumb.webservice.restful;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.ImageFormatNotSupportedException;
import com.trekcrumb.common.exception.UserDuplicateException;
import com.trekcrumb.common.exception.UserNotConfirmedException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webservice.IUserService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the IUserService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 07/2013
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/userService")
public class UserServiceRestfulImpl 
implements IUserService {
    private Log log = LogFactory.getLog(getClass()); 
    
    /**
     * Creates a new User.
     * @param request - A ServiceRequest that must contain:
     *  1. user: The user bean containing the values of all required parameters such as  
     *           username, email, password and so on.
     *           
     * @return Newly created User via ServiceResponse.getUser().
     */
    @POST
    @Path("/create")
    public ServiceResponse create(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        //Process request:
        try {
            User userCreated = UserManager.userCreate(request.getUser());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setUser(userCreated);
        } catch(UserDuplicateException ude) {
            log.error("UserService Create error: " + ude.getMessage());
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, ude.getMessage());
        } catch(UserNotFoundException unfe) {
            log.error("UserService Create error: " + unfe.getMessage());
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Create error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }

        return result;
    }
    
    /**
     * Confirms a new User.
     * @param request - A ServiceRequest that must contain:
     *  1. user: The user bean containing the values of required parameters to confirm the User
     *           (UserID).
     *           
     * @return Success is true or false via ServiceResponse.isSuccess().
     */
    @POST
    @Path("/confirm")
    public ServiceResponse confirm(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        //Process request:
        try {
            User userRequest = request.getUser();
            UserManager.userConfirm(userRequest);
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(UserNotFoundException unfe) {
            log.error("UserService Confirm error: " + unfe.getMessage());
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Confirm error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    /**
     * Logs in a User.
     * @param request - A ServiceRequest that must contain:
     *  1. user: The user bean containing the values of all required parameters such as  
     *           username and password.
     *           
     * @return Logged in User via ServiceResponse.getUser().
     */
    @POST
    @Path("/login")
    public ServiceResponse login(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
                
        //Process request:
        try {
            User userRequest = request.getUser();
            User userLogin = UserManager.userLogin(userRequest);
            if(userLogin == null ) {
                throw new UserNotFoundException("UserService Login error: After login successful, the returned User bean is unexpectedly NULL!");
            }
            if(userLogin.getActive() == YesNoEnum.YES) {
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR, "User has been deactivated!");
            }
            result.setUser(userLogin);
            
        } catch(UserNotFoundException unfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(UserNotConfirmedException unce) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_CONFIRMED_ERROR, unce.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Login error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        
        return result;
    }
    
    /**
     * Updates User data.
     * @param request - A ServiceRequest that contains:
     *  1. user: The user bean containing ONLY the data to update such as new password, fullname,
     *           location and so on. If the bean contains no data to update, nothing is updated.
     *           
     * @return Updated User via ServiceResponse.getUser().
     */
    @POST
    @Path("/update")
    public ServiceResponse update(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        //Process request:
        try {
            User userRequest = request.getUser();
            User userResponse = UserManager.userUpdate(userRequest);
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setUser(userResponse);
        } catch(UserDuplicateException ude) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, ude.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(ImageFormatNotSupportedException ifnse) {
            /*
             * TODO (8/2016): Image format not supported
             * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
             * with error: "Invalid argument to native writeImage". The issue is JRE being used by
             * the server if it is  OpenJDK, it has this bug. Solution is to either change the JRE or use
             * GIF plug-in, including to support GIF animation.
             */
            log.error("UserService Update error: " + ifnse.getMessage());
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.PICTURE_SERVICE_IMAGE_FORMAT_NOT_SUPPORTED, ifnse.getMessage());
        } catch(Exception e) {
            log.error("UserService Update error: " + e.getMessage());
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }    
    
    /**
     * Deactivates a User.
     * @param request - A ServiceRequest that must contain:
     *  1. user: The user bean containing the values of all required parameters (UserID).
     *           
     * @return Success is true or false via ServiceResponse.isSuccess().
     */
    @POST
    @Path("/deactivate")
    public ServiceResponse deactivate(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
                
        //Process request:
        try {
            User userRequest = request.getUser();
            UserManager.userDeActivate(userRequest);
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Deactivate error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }        
    
    /**
     * Reactivates a User (that was previously deactivated).
     * @param request - A ServiceRequest that contains:
     *  1. user: The user bean containing the values of all required parameters (UserID).
     *           
     * @return Reactivated User via ServiceResponse.getUser().
     */
    @POST
    @Path("/reactivate")
    public ServiceResponse reactivate(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
                
        //Process request:
        try {
            User userRequest = request.getUser();
            User userUpdated = UserManager.userReActivate(userRequest);
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setUser(userUpdated);
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Reactivate error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/passwordForget")
    public ServiceResponse passwordForget(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
                
        try {
            UserManager.userPasswordForget(request.getUser());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            
        } catch(UserNotFoundException unfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Password Forget error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }    
    
    @POST
    @Path("/passwordReset")
    public ServiceResponse passwordReset(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
                
        try {
            User userFound = UserManager.userPasswordReset(request.getUser());
            if(userFound == null ) {
                throw new UserNotFoundException("UserService Password Reset error: User not found!");
            }

            if(userFound.getActive() == YesNoEnum.YES) {
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_DEACTIVATED_ERROR, "User has been deactivated!");
            }
            result.setUser(userFound);
            
        } catch(UserNotFoundException unfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Password Reset error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }    
    
    /**
     * Retrieves a list of Users given the search criteria.
     * @param request - A ServiceRequest that must contain:
     *  1. serviceType: ServiceTypeEnum to identify retrieveBy parameter such as USER_RETRIEVE_BY_USERID,
     *                  USER_RETRIEVE_BY_USERNAME, and USER_RETRIEVE_BY_EMAIL
     *  2. user: The user bean containing the value of the required parameters such as UserID, 
     *           username or email, which depends on the serviceType.
     *           
     * @return List of Users via ServiceResponse.getListOfUsers().
     */
    @POST
    @Path("/retrieve")
    public ServiceResponse retrieve(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getUser() == null
                || request.getServiceType() == null) {
            String errMsg = "UserService Retrieve error: Missing required data User bean and ServiceTypeEnum retrieveBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        //Process request:
        try {
            List<User> listOfUsersFound = 
                    UserManager.userRetrieve(request.getUser(), request.getServiceType(), true, request.getOffset(), request.getNumOfRows());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfUsers(listOfUsersFound);
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Retrieve error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/profileImageDownload")
    public ServiceResponse userProfileImageDownload(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getUser() == null) {
            String errMsg = "User Profile Image Download error: Missing required data User bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            User userWithImageToDownload = request.getUser();
            FileServerResponse businessResponse = UserManager.userProfileImageDownload(userWithImageToDownload);
            if(businessResponse.isSuccess()) {
                if(businessResponse.getFileBytes() == null) {
                    result = CommonUtil.composeServiceResponseError(
                            request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                            "User Profile Image Download error: Returned file is empty!");
                } else {
                    result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
                    userWithImageToDownload.setProfileImageBytes(businessResponse.getFileBytes());
                    List<User> listOfUsers = new ArrayList<User>();
                    listOfUsers.add(userWithImageToDownload);
                    result.setListOfUsers(listOfUsers);
                }
                
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                        "User Profile Image Download error: system failure!");
            }
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("User Profile Image Download error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;    
    }    
    
    @POST
    @Path("/contactUs")
    public ServiceResponse contactUs(ServiceRequest request) {
        ServiceResponse result = null;

        result = validateRequest(request);
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
                
        try {
            UserManager.userContactUs(request.getUser());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            log.error("UserService Contact Us error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }    
    
    private ServiceResponse validateRequest(ServiceRequest request) {
        if(request.getUser() == null) {
            String errMsg = "UserService request validation error: Missing User data!";
            log.error(errMsg);
            return CommonUtil.composeServiceResponseError(
                    request.getServiceType(), 
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    errMsg);
        }
        return WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
    }
       
}
