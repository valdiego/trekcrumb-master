package com.trekcrumb.webservice.restful;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.TripManager;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webservice.ITripService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the ITripService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 07/2013
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/tripService")
public class TripServiceRestfulImpl 
implements ITripService {
    private Log log = LogFactory.getLog(getClass()); 
    
    @POST
    @Path("/create")
    public ServiceResponse create(ServiceRequest request) {
       ServiceResponse result = null;
        if(request.getTrip() == null) {
            log.error("TripService Create error: Missing required data Trip bean!");
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, "TripService Create error: Missing required data!");
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Trip tripCreated = TripManager.create(request.getTrip());

            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            List<Trip> listOfTrips = new ArrayList<Trip>();;
            listOfTrips.add(tripCreated);
            result.setListOfTrips(listOfTrips);
        } catch(IllegalArgumentException ile) {
            log.error("TripService Create error: " + ile.getMessage() + " \n" + request);
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
            
        } catch(UserNotFoundException unfe) {
            log.error("TripService Create error: " + unfe.getMessage() + " \n" + request);
            unfe.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
            
        } catch(Exception e) {
            log.error("TripService Create error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/publish")
    public ServiceResponse publish(ServiceRequest request) {
       ServiceResponse result = null;
        if(request.getTrip() == null) {
            String errMsg = "TripService Publish error: Missing required data Trip bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Trip tripPublished = TripManager.publish(request.getTrip());

            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            List<Trip> listOfTrips = new ArrayList<Trip>();;
            listOfTrips.add(tripPublished);
            result.setListOfTrips(listOfTrips);
            
            /*
             * TODO 11/2014: The clients must call directly for latest (refresh) User data themselves 
            refreshUserLoginEnrichmentData(tripPublished.getUserID(), result);
            */

        } catch(IllegalArgumentException ile) {
            log.error("TripService Publish error: " + ile.getMessage() + " \n" + request);
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("TripService Publish error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }    
    
    @POST
    @GET
    @Path("/retrieve")
    public ServiceResponse retrieve(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getTrip() == null
                || request.getServiceType() == null) {
            String errMsg = "TripService Retrieve error: Missing required data Trip bean and ServiceTypeEnum retrieveBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            List<Trip> listOfTrips = TripManager.retrieve(request.getTrip(), 
                                                          request.getServiceType(), 
                                                          request.getWhereClause(), 
                                                          OrderByEnum.ORDER_BY_CREATED_DATE,
                                                          request.getOffset(), 
                                                          request.getNumOfRows());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfTrips(listOfTrips);

        } catch(IllegalArgumentException ile) {
            log.error("TripService Retrieve error: " + ile.getMessage() + " \n" + request);
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("TripService Retrieve error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @GET
    @Path("/search")
    public ServiceResponse search(ServiceRequest request) {
        ServiceResponse result = null;
        
        if(request.getTripSearchCriteria() == null) {
            String errMsg = "TripService Search error: Missing required data in TripSearchCriteria!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            int numOfTotalTrips = 0;
            List<Trip> tripsFound = TripManager.search(request.getTripSearchCriteria());
            if(tripsFound != null 
                    && tripsFound.size() > 0) {
                numOfTotalTrips = TripManager.count(request.getTripSearchCriteria());
            }

            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfTrips(tripsFound);
            result.setNumOfRecords(numOfTotalTrips);
            
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("TripService Search error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        
        return result;
    }    
    
    @POST
    @GET
    @Path("/retrieveSync")
    public ServiceResponse retrieveSync(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getTrip() == null
                || request.getTrip().getUserID() == null) {
            String errMsg = "TripService RetrieveSync error: Missing required data Trip bean and UserID!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            List<Trip> listOfTrips = TripManager.retrieveSync(request.getTrip());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfTrips(listOfTrips);
        } catch(IllegalArgumentException ile) {
            log.error("TripService RetrieveSync error: " + ile.getMessage() + " \n" + request);
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("TripService RetrieveSync error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/delete")
    public ServiceResponse delete(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getTrip() == null
                || request.getServiceType() == null) {
            String errMsg = "TripService Delete error: Missing required data Trip bean and ServiceTypeEnum deleteBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            TripManager.delete(request.getTrip(), request.getServiceType());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            
            /*
             * TODO 11/2014: The clients must call directly for latest (refresh) User data themselves 
            if(!ValidationUtil.isEmpty(request.getTrip().getUserID())) {
                refreshUserLoginEnrichmentData(request.getTrip().getUserID(), result);
            }
            */

        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("TripService Delete error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/update")
    public ServiceResponse update(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getTrip() == null) {
            String errMsg = "TripService Update error: Missing required data Trip bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Trip tripUpdated = TripManager.update(request.getTrip());
            if(tripUpdated != null) {
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());

                List<Trip> listOfUpdatedTrips = new ArrayList<Trip>();
                listOfUpdatedTrips.add(tripUpdated);
                result.setListOfTrips(listOfUpdatedTrips);
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR, "Trip to update NOT found!");
            }
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("TripService Update error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;    
    }
    
}
