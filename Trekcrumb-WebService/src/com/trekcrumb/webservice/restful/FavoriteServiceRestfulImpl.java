package com.trekcrumb.webservice.restful;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.FavoriteManager;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webservice.IFavoriteService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the IFavoriteService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 07/2013
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/favoriteService")
public class FavoriteServiceRestfulImpl 
implements IFavoriteService {
    private Log log = LogFactory.getLog(getClass()); 
    
    @POST
    @Path("/create")
    public ServiceResponse create(ServiceRequest request) {
       ServiceResponse result = null;
        if(request.getFavorite() == null) {
            String errMsg = "Favorite Create error: Missing required Favorite bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            FavoriteManager.create(request.getFavorite());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Favorite Create error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/retrieve")
    public ServiceResponse retrieve(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getFavorite() == null
                || ValidationUtil.isEmpty(request.getFavorite().getUserID()) 
                || ValidationUtil.isEmpty(request.getFavorite().getTripID())) {
            String errMsg = "Favorite Retrieve error: BOTH UserID and TripID are required!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        try {
            List<Favorite> listOfFavesFound = FavoriteManager.retrieve(request.getFavorite(), null, -1, -1);
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfFavorites(listOfFavesFound);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Favorite Retrieve error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
        
    @POST
    @Path("/retrieveByTrip")
    public ServiceResponse retrieveByTrip(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getTrip() == null
                || ValidationUtil.isEmpty(request.getTrip().getId())) {
            String errMsg = "Favorite Retrieve by Trip error: Missing required Trip ID!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
         
        try {
            List<User> listOfUsers = 
                    FavoriteManager.retrieveByTrip(request.getTrip(), request.getOffset(), request.getNumOfRows());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfUsers(listOfUsers);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Favorite Retrieve by Trip error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/retrieveByUser")
    public ServiceResponse retrieveByUser(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getUser() == null
                || ValidationUtil.isEmpty(request.getUser().getUsername())) {
            String errMsg = "Favorite Retrieve by User error: Missing required Username!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
         
        try {
            List<Trip> listOfTrips = FavoriteManager.retrieveByUser(request.getUser(), request.getOffset(), request.getNumOfRows());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfTrips(listOfTrips);
            
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Favorite Retrieve by User error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }    
    
    @POST
    @Path("/delete")
    public ServiceResponse delete(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getFavorite() == null
                || ValidationUtil.isEmpty(request.getFavorite().getUserID()) 
                || ValidationUtil.isEmpty(request.getFavorite().getTripID())) {
            //Do not allow client to delete favorite by Trip or by User:
            String errMsg = "Favorite Delete error: BOTH UserID and TripID are required!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            FavoriteManager.delete(request.getFavorite());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Favorite Delete error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    
}
