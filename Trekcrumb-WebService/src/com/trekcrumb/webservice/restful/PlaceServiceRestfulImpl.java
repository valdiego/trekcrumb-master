package com.trekcrumb.webservice.restful;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.PlaceManager;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.exception.TripNotFoundException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webservice.IPlaceService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the IPlaceService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 07/2013
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/placeService")
public class PlaceServiceRestfulImpl 
implements IPlaceService {
    private Log log = LogFactory.getLog(getClass()); 
    
    @POST
    @Path("/create")
    public ServiceResponse create(ServiceRequest request) {
       ServiceResponse result = null;
        if(request.getPlace() == null) {
            String errMsg = "Place Create error: Missing required data Place bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Place placeCreated = PlaceManager.create(request.getPlace());

            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            List<Place> listOfPlaces = new ArrayList<Place>();;
            listOfPlaces.add(placeCreated);
            result.setListOfPlaces(listOfPlaces);
            
        } catch(UserNotFoundException unfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(TripNotFoundException tnfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.TRIP_SERVICE_TRIP_NOT_FOUND_ERROR, tnfe.getMessage());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Place Create error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/retrieve")
    public ServiceResponse retrieve(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPlace() == null
                || request.getServiceType() == null) {
            String errMsg = "Place Retrieve error: Missing required data Place bean and retrieveBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            List<Place> listOfPlaces = PlaceManager.retrieve(request.getPlace(), request.getServiceType());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfPlaces(listOfPlaces);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Place Retrieve error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/delete")
    public ServiceResponse delete(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPlace() == null
                || request.getServiceType() == null) {
            String errMsg = "Place Delete error: Missing required data Place bean and deleteBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            PlaceManager.delete(request.getPlace(), request.getServiceType());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Place Delete error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/update")
    public ServiceResponse update(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPlace() == null) {
            String errMsg = "Place Update error: Missing required data Trip bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Place placeUpdated = PlaceManager.update(request.getPlace());
            if(placeUpdated != null) {
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
                List<Place> listOfPlaces = new ArrayList<Place>();;
                listOfPlaces.add(placeUpdated);
                result.setListOfPlaces(listOfPlaces);
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR, "Place to update NOT found!");
            }
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Place Update error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;    
    }



    
}
