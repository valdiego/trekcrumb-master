package com.trekcrumb.webservice.restful;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.UserAuthTokenManager;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.webservice.IUserAuthTokenService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the IUserAuthTokenService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 07/2013
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/userAuthTokenService")
public class UserAuthTokenServiceRestfulImpl 
implements IUserAuthTokenService {
    private Log log = LogFactory.getLog(getClass()); 
    
    @POST
    @Path("/authenticate")
    public ServiceResponse authenticate(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getUserAuthToken() == null
                || ValidationUtil.isEmpty(request.getUserAuthToken().getUserId())
                || ValidationUtil.isEmpty(request.getUserAuthToken().getAuthToken())) {
            log.error("UserAuthToken Authenticate error: Missing required data UserID and AuthToken!");
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, "UserAuthToken Authenticate error: Missing required data!");
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }

        //Decrypt both UserID and AuthToken:
        String decryptedUserID = null;
        String decryptedAuthToken = null;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            decryptedUserID = cryptoInstance.decrypt(request.getUserAuthToken().getUserId());
            decryptedAuthToken = cryptoInstance.decrypt(request.getUserAuthToken().getAuthToken());
            
            request.getUserAuthToken().setUserId(decryptedUserID);
            request.getUserAuthToken().setAuthToken(decryptedAuthToken);
        } catch(Exception e) {
            log.error("UserAuthToken Authenticate error: Cryptography processing - " + e.getMessage());
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, e.getMessage());
            return result;
        }

        //Process request:
        try {
            User userAuthenticated = UserAuthTokenManager.authenticate(request.getUserAuthToken());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setUser(userAuthenticated);
            
        } catch(IllegalArgumentException ile) {
            log.error("Invalid parameter(s) in the request: " + ile.getMessage() + " \n" + request);
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(UserNotFoundException unfe) {
            log.error(unfe.getMessage());
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_AUTHENTICATION_ERROR, unfe.getMessage());
        } catch(Exception e) {
            log.error("UserAuthToken Authenticate error: Unexpected - " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result; 
    }
    
    @POST
    @Path("delete")
    public ServiceResponse delete(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getUserAuthToken() == null
                || request.getServiceType() == null) {
            log.error("UserAuthToken Delete error: Missing required data UserAuthToken bean and ServiceTypeEnum deleteBy!");
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, "UserAuthToken Delete error: Missing required data!");
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        //Decrypt UserID:
        String decryptedUserID = null;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            decryptedUserID = cryptoInstance.decrypt(request.getUserAuthToken().getUserId());
            request.getUserAuthToken().setUserId(decryptedUserID);
        } catch(Exception e) {
            log.error("UserAuthToken Authenticate error: Cryptography processing - " + e.getMessage());
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, e.getMessage());
            return result;
        }

        //Process request:
        try {
            if(request.getServiceType() == ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID) {
                List<UserAuthToken> userAuthTokenListRemains = UserAuthTokenManager.delete(request.getUserAuthToken());
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
                result.setListOfUserAuthTokens(userAuthTokenListRemains);
            
            } else {
                UserAuthTokenManager.delete(request.getUserAuthToken(), request.getServiceType());
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            }
            
        } catch(IllegalArgumentException ile) {
            log.error("Invalid parameter(s) in the request: " + ile.getMessage() + " \n" + request);
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("UserAuthToken Authenticate error: Unexpected - " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;         
    }
    
}
