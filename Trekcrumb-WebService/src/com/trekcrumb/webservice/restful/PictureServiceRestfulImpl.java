package com.trekcrumb.webservice.restful;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.PictureManager;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.ImageFormatNotSupportedException;
import com.trekcrumb.common.exception.TripNotFoundException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webservice.IPictureService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the ITPlaceService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 03/2014
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/pictureService")
public class PictureServiceRestfulImpl 
implements IPictureService {
    private Log log = LogFactory.getLog(getClass()); 
    
    @POST
    @Path("/create")
    public ServiceResponse create(ServiceRequest request) {
       ServiceResponse result = null;
        if(request.getPicture() == null) {
            String errMsg = "Picture Create error: Missing required data Picture bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Picture picCreated = PictureManager.create(request.getPicture());

            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            List<Picture> listOfPics = new ArrayList<Picture>();
            listOfPics.add(picCreated);
            result.setListOfPictures(listOfPics);
            
        } catch(UserNotFoundException unfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(TripNotFoundException tnfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.TRIP_SERVICE_TRIP_NOT_FOUND_ERROR, tnfe.getMessage());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(ImageFormatNotSupportedException ifnse) {
            /*
             * TODO (8/2016): Image format not supported
             * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
             * with error: "Invalid argument to native writeImage". The issue is JRE being used by
             * the server if it is  OpenJDK, it has this bug. Solution is to either change the JRE or use
             * GIF plug-in, including to support GIF animation.
             */
            log.error("Picture Create error: " + ifnse.getMessage());
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.PICTURE_SERVICE_IMAGE_FORMAT_NOT_SUPPORTED, ifnse.getMessage());
            
        } catch(Exception e) {
            log.error("Picture Create error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/retrieve")
    public ServiceResponse retrieve(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPicture() == null
                || request.getServiceType() == null) {
            String errMsg = "Picture Retrieve error: Missing required data Picture bean and retrieveBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            List<Picture> listOfPics = PictureManager.retrieve(
                    request.getPicture(), request.getServiceType(),
                    request.getOrderBy(), request.getOffset(), request.getNumOfRows());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfPictures(listOfPics);
            
            if(listOfPics != null 
                    && listOfPics.size() > 0
                    && request.getServiceType() == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME) {
                int numOfTotalPictures = PictureManager.count(request.getPicture(), request.getServiceType());
                result.setNumOfRecords(numOfTotalPictures);
            }
            
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Picture Retrieve error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/delete")
    public ServiceResponse delete(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPicture() == null
                || request.getServiceType() == null) {
            String errMsg = "Picture Delete error: Missing required data Picture bean and deleteBy!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            PictureManager.delete(request.getPicture(), request.getServiceType());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Picture Delete error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/update")
    public ServiceResponse update(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPicture() == null) {
            String errMsg = "Picture Update error: Missing required data Picture bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Picture picUpdated = PictureManager.update(request.getPicture());
            if(picUpdated != null) {
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
                List<Picture> listOfPics = new ArrayList<Picture>();
                listOfPics.add(picUpdated);
                result.setListOfPictures(listOfPics);
                
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.SYSTEM_RECORD_NOT_FOUND_ERROR, "Picture to update NOT found!");
            }
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Picture Update error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;    
    }
    
    @POST
    @Path("/imageUpload")
    public ServiceResponse imageUpload(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPicture() == null) {
            String errMsg = "Picture Image Upload error: Missing required data Picture bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            FileServerResponse businessResponse = PictureManager.imageUpload(request.getPicture());
            if(businessResponse.isSuccess()) {
                result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            } else {
                result = CommonUtil.composeServiceResponseError(
                        request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                        "Picture Image Upload error: system failure!");
            }
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Picture Image Upload error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;    
    }

    @POST
    @Path("/imageDownload")
    public ServiceResponse imageDownload(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getPicture() == null) {
            String errMsg = "Picture Image Download error: Missing required data Picture bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            Picture pictureWithImageToDownload = request.getPicture();
            FileServerResponse fsResponse = PictureManager.imageDownload(pictureWithImageToDownload);
            if(fsResponse.isSuccess()) {
                if(fsResponse.getFileBytes() == null) {
                    log.error("ERROR: Picture Image Download failed! Server response is SUCCESS but image bytes is EMPTY!");
                    result = CommonUtil.composeServiceResponseError(
                            request.getServiceType(), ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND, 
                            "Image not found!");
                } else {
                    result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
                    pictureWithImageToDownload.setImageBytes(fsResponse.getFileBytes());
                    List<Picture> listOfPics = new ArrayList<Picture>();
                    listOfPics.add(pictureWithImageToDownload);
                    result.setListOfPictures(listOfPics);
                }
                
            } else {
                //Handle error:
                if(fsResponse.getServiceError() != null) {
                    result = CommonUtil.composeServiceResponseError(
                            request.getServiceType(), 
                            fsResponse.getServiceError().getErrorEnum(), 
                            fsResponse.getServiceError().getErrorText());
                } else {
                    result = CommonUtil.composeServiceResponseError(request.getServiceType(), null, null);
                }
            }
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("ERROR: Picture Image Download failed: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;    
    }
    
    
}
