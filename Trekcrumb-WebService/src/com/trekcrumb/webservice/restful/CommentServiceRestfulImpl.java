package com.trekcrumb.webservice.restful;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.manager.CommentManager;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webservice.ICommentService;
import com.trekcrumb.webservice.util.WebServiceUtil;

/**
 * Concrete impl of the ICommentService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 07/2013
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/commentService")
public class CommentServiceRestfulImpl 
implements ICommentService {
    private Log log = LogFactory.getLog(getClass()); 
    
    @POST
    @Path("/create")
    public ServiceResponse create(ServiceRequest request) {
       ServiceResponse result = null;
        if(request.getComment() == null) {
            String errMsg = "Comment Create error: Missing required Comment bean!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            CommentManager.create(request.getComment());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Comment Create error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    @POST
    @Path("/retrieve")
    public ServiceResponse retrieve(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getComment() == null
                || request.getServiceType() == null) {
            String errMsg = "Comment Retrieve error: Comment bean and retrieveBy type are required!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
        
        try {
            List<Comment> listOfComments = CommentManager.retrieve(request.getComment(), request.getServiceType(), request.getOrderBy(), request.getOffset(), request.getNumOfRows());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setListOfComments(listOfComments);
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Commment Retrieve error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
        
    @POST
    @Path("/delete")
    public ServiceResponse delete(ServiceRequest request) {
        ServiceResponse result = null;
        if(request.getComment() == null
                || request.getServiceType() == null) {
            String errMsg = "Comment Delete error: Comment bean and deleteBy type are required!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, errMsg);
            return result;
        }
        if(request.getServiceType() != ServiceTypeEnum.COMMENT_DELETE_BY_COMMENTID) {
            //Do not allow client to delete comments by Trip or by User:
            String errMsg = "Comment Delete error: Illegal deleteBy type!";
            log.error(errMsg);
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, errMsg);
            return result;
            
        }
        result = WebServiceUtil.validateDigitalSignature(request.getDigitalSignature());
        if(result != null && !result.isSuccess()) {
            result.setServiceType(request.getServiceType());
            return result;
        }
 
        try {
            CommentManager.delete(request.getComment(), request.getServiceType());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
        } catch(IllegalArgumentException ile) {
            ile.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, ile.getMessage());
        } catch(Exception e) {
            log.error("Comment Delete error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    
}
