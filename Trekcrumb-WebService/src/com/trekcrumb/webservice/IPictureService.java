package com.trekcrumb.webservice;

import javax.jws.WebService;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;

/**
 * Interface for Picture Service.
 * NOTE:
 * 1. For CFX WebService SOAP implementation, it is required to declare the interface with @WebService tag.
 * 
 * @author Val Diego
 * @since 03/2014
 */
@WebService
public interface IPictureService {
    
    public ServiceResponse create(ServiceRequest request);
    public ServiceResponse retrieve(ServiceRequest request);
    public ServiceResponse update(ServiceRequest request);
    public ServiceResponse delete(ServiceRequest request);
    
    public ServiceResponse imageUpload(ServiceRequest request);
    public ServiceResponse imageDownload(ServiceRequest request);
    
}
