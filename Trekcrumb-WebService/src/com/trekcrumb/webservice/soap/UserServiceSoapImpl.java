package com.trekcrumb.webservice.soap;

import javax.jws.WebService;

import com.trekcrumb.business.manager.UserManager;
import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.exception.UserDuplicateException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.webservice.IUserService;

/**
 * Concrete impl of the IUserService interface with SOAP technology.
 * 
 * @author Val Diego
 * @since 07/2013
 */

@WebService(endpointInterface = "com.trekcrumb.webservice.IUserService")
public class UserServiceSoapImpl 
implements IUserService {

    public ServiceResponse create(ServiceRequest request) {
        ServiceResponse result = null;
        try {
            User userCreated = UserManager.userCreate(request.getUser());
            result = CommonUtil.composeServiceResponseSuccess(request.getServiceType());
            result.setUser(userCreated);
        } catch(UserDuplicateException ude) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_DUPLICATE_USER_ERROR, ude.getMessage());
        } catch(UserNotFoundException unfe) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.USER_SERVICE_USER_NOT_FOUND_ERROR, unfe.getMessage());
        } catch(IllegalArgumentException iae) {
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, iae.getMessage());
        } catch(Exception e) {
            e.printStackTrace();
            result = CommonUtil.composeServiceResponseError(
                    request.getServiceType(), ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, e.getMessage());
        }
        return result;
    }
    
    public ServiceResponse confirm(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
    public ServiceResponse login(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
    public ServiceResponse update(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
    public ServiceResponse retrieve(ServiceRequest request){
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
    public ServiceResponse reactivate(ServiceRequest request){
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
    public ServiceResponse deactivate(ServiceRequest request){
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
    public ServiceResponse passwordForget(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }

    public ServiceResponse passwordReset(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }

    public ServiceResponse userProfileImageDownload(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }

    public ServiceResponse contactUs(ServiceRequest request) {
        throw new UnsupportedOperationException("Service not implemented!");
    }
    
}
