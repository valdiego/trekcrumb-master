package com.trekcrumb.webservice;

import javax.jws.WebService;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;

/**
 * Interface for UserService.
 * NOTE:
 * 1. For CFX WebService SOAP implementation, it is required to declare the interface with @WebService tag.
 * 
 * @author Val Diego
 * @since 07/2013
 */
@WebService
public interface IUserService {
    public ServiceResponse create(ServiceRequest request);
    public ServiceResponse confirm(ServiceRequest request);
    public ServiceResponse login(ServiceRequest request);
    public ServiceResponse update(ServiceRequest request);
    public ServiceResponse retrieve(ServiceRequest request);
    public ServiceResponse reactivate(ServiceRequest request);
    public ServiceResponse deactivate(ServiceRequest request);
    public ServiceResponse passwordForget(ServiceRequest request);
    public ServiceResponse passwordReset(ServiceRequest request);
    public ServiceResponse userProfileImageDownload(ServiceRequest request);
    public ServiceResponse contactUs(ServiceRequest request);
    
    
}
