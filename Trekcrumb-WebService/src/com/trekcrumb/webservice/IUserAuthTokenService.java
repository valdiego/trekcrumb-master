package com.trekcrumb.webservice;

import javax.jws.WebService;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;

/**
 * Interface for UserRememberMeService.
 * NOTE:
 * 1. For CFX WebService SOAP implementation, it is required to declare the interface with @WebService tag.
 *    For Restful, the tag is not needed.
 * 
 * @author Val Diego
 * @since 09/2013
 */
@WebService
public interface IUserAuthTokenService {
    public ServiceResponse authenticate(ServiceRequest request);
    public ServiceResponse delete(ServiceRequest request);
    
    
}
