package com.trekcrumb.webservice.util;

import com.trekcrumb.common.bean.DigitalSignature;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

public class WebServiceUtil {

    /**
     * Validates digital signature.
     * @param digitalSign - The digital signature entity containing the signedMessage (the exchanged 
     *        message that is signed) and the digital signature.
     * @return ServiceError with the detail error message if it is fails. Otherwise, returns null.
     */
    public static ServiceResponse validateDigitalSignature(DigitalSignature digitalSign) {
        if(digitalSign == null
                || ValidationUtil.isEmpty(digitalSign.getSignedMessage())
                || ValidationUtil.isEmpty(digitalSign.getSignature())) {
            return CommonUtil.composeServiceResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR, 
                    "Request validation failed: Missing digital signature!");
        }
        
        Cryptographer cryptoInstance = null;
        try {
            cryptoInstance = Cryptographer.getInstance();
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR, 
                    "Request validation failed: Unexpected error when preparing system cryptography!");
        }
        
        try {
            if(!cryptoInstance.verifySign(digitalSign.getSignedMessage(), digitalSign.getSignature())) {
                return CommonUtil.composeServiceResponseError(
                        null,
                        ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, 
                        "Request is from illegal party!");
            }
        } catch(Exception e) {
            e.printStackTrace();
            return CommonUtil.composeServiceResponseError(
                    null,
                    ServiceErrorEnum.SYSTEM_UNAUTHORIZED_REQUEST_ERROR, 
                    "Request is from illegal party!");
        }
        
        return null;
    }    
    
}
