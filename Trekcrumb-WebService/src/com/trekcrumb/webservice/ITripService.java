package com.trekcrumb.webservice;

import javax.jws.WebService;

import com.trekcrumb.common.bean.ServiceRequest;
import com.trekcrumb.common.bean.ServiceResponse;

/**
 * Interface for TripService.
 * NOTE:
 * 1. For CFX WebService SOAP implementation, it is required to declare the interface with @WebService tag.
 * 
 * @author Val Diego
 * @since 07/2013
 */
@WebService
public interface ITripService {
    
    public ServiceResponse create(ServiceRequest request);
    public ServiceResponse publish(ServiceRequest request);
    public ServiceResponse retrieve(ServiceRequest request);
    public ServiceResponse retrieveSync(ServiceRequest request);
    public ServiceResponse update(ServiceRequest request);
    public ServiceResponse delete(ServiceRequest request);
    public ServiceResponse search(ServiceRequest request);
    
}
