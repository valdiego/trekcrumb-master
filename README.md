# **Trekcrumb Application**
## **About**
Trekcrumb is an application that enable users to record and share their trip locations real-time (live) to their contacts via text messaging (SMS, WhatsApp, etc) and to the Web via Social-Media feeds (Twitter, Instagram, etc). 

The solution enables users to:

* Track their geo-location movements in multi-connected places real-time
* Mark their current position, add notes to each location as part of the trip journals, and add pictures along the way
* View their entire trip in an interactive map
* Share and let others to view their posts (trips, journals and maps)
* Set privacy to their content (public/private) such that they could limit who can view their trip posts
* Interact with their audience in the form of "like/favorites" and comments features

The solution comes in two flavors: 

* Web application running our our cloud platform
* Mobile app running on users' local devices. Currently, the solution supports Android mobile ecosystem. 


## **Solution Architecture**
![Trekcrumb solution & system architecture](/Trekcrumb_SolutionArchitecture.jpg)


## **Solution Components**
Trekcrumb solution consists of distributed multi-layer components. The following are the main layers/components within the application:
### **A. Common Library**
* **Base** : The package contains required properties, configuration & setting, and assets that are used throughout by all layers within the solution.

* **Trekcrumb-Common** : The package contains core/low-level Java API that are required and re-usable to all layers within the solution. This includes Java bean/POJO and common utility functions. 

### **B. Server/Back-End Layers**
The server layers are Trekcrumb components that are running on our distributed servers.

* **Trekcrumb-Business** : The package contains solution core business logic and orchestration library to manage the application CRUD (create, read, update, delete) functions, as well as other business logics. It manages integration into other systems such as database, email systems, micro-services (FileServer), user authentication mehcanism, event-driven platform (if any), etc. 

* **Trekcrumb-WebService** : The package is the user-facing Web/Micro Services that wrap the Trekcrumb-Business package in it, therefore this package is very light-weight and easily modified without affecting the core service that much. It is REST-ful (and optionally, SOAP) based solution that accept HTTP/HTTPS request-response protocol, using Apache CFX-JAXRS framework. This package will run on its own J2EE server.

* **Trekcrumb-FileServer** : The package provides Trekcrumb mechanism for user file management to do CRUD (create, read, update, delete) operations. Currently, this manage user images (JPG/PNG) that they will include in their trip posts. The FileServer can run as either Web Service, or Servlet or Socket Listener; depending on deployment preference. The FileServer is accessable to any client platform (Web app, Mobile app, etc) by following its request-response contract setting. This package will run on its own J2EE server.

### **C. Client (Common) Layers**
* **Trekcrumb-WebServiceClient** : The package provides disributable library (in Java) to integrate with Trekcrumb-WebService solution running on our server. It is distributable such that any Front-End layers (Web app and Android app) can plug it in and use it to interact with Trekcrumb back-end to manage user posts (trips, comments, user authentication, etc)

* **Trekcrumb-FileServerClient** : The package provides distributable library (in Java) to integrate with Trekcrumb-FileServer solution (via Web Services, Servlet or Socket). It is distributable such that any Front-End layers (Web app and Android app) can plug it in and use it to interact with Trekcrumb back-end servers to manage user images.

### **D. Front-End/UI Layers**
* **Trekcrumb-WebApp** : Trekcrumb front-end application that user can access anywhere by a browser. The web app is responsive-UI, meaning its look-and-feel accomodates and adjusts to run on regular wide-screen display (laptop/desktop) as well as small screen (mobile phone, etc) without losing arrangements and/or any function. Currently, this package uses J2EE JSP and Spring MVC platforms, and have a lot of Javascript/CSS coding to support the responsive-UI feature. Behind the scene, the Web app integrate with Trekcrumb back-end layers including Trekcrumb-WebService and Trekcrumb-FileServer utilizing the "C. Client (common) Layers" described above.

* **Trekcrumb-Android** : Trekcrumb front-end application running on Android mobile devices. This app is built using Android SDK, with look-and-feel, features and behaviors that mimick exactly those of Trekcrumb-WebApp solution. Behind the scene, the Web app integrate with Trekcrumb back-end layers including Trekcrumb-WebService and Trekcrumb-FileServer utilizing the "C. Client (common) Layers" described above.

### **E. Test Framework**
* **Trekcrumb-UnitTest** : A complete set of unit-test and layer-integration-test that ensure the stability of Trekcrumb server-side components including tests to all the web services and file server APIs.


## **Development & Copyright**
Trekcrumb codebase and libraries are not public/open repository, but a private proprietary software. The detail in regard to development setting & configuration for others/public community other than Trekcrumb team members is currently not available. We may update/change this section policy in the future.

