NOTE:
1. JUnitXmlFormatter.jar
   This is our generated jar for 3rd party source code: Barry Pitman's JUnitXmlFormatter to enable
   generating XML result reports when we execute JUnit tests via java command line.
   REF: 
   - http://stackoverflow.com/questions/12445582/generating-junit-reports-from-the-command-line
   - https://github.com/barrypitman/JUnitXmlFormatter
   
2. junitper-1.9.1.jar
   The jar from 3rd party source JUnitPerf project to run JUnit as performance testing
   
 