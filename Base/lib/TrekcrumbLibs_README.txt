*******************************
* REQUIRED 3RD PARTY LIBRARIES
* (JARS, XMLS, CSS, ETC.)
*******************************
lib/
-> android
   1. google-play-services_lib (V.2 , 1/2013)
      -> This is a library required for Google Map API.
      -> The whole directory must be a copy from (and keep the same original name):
         <android-sdk-folder>/extras/google/google_play_services/libproject/google-play-services_lib
      -> Create "New Android Project with Existing Code" and choose the entire google-play-services_lib
         directory as its source code (Here, you can rename the new project anything you wished). 
         Then mark the project as library: properties > android > isLibrary = TRUE (checked)
      -> Clean-Build the project as a regular Android project. This will generate necessary compiled
         files, artifacts and JARs in directories: /bin and /gen.
      -> Any Android project that needs Map API must include/import google-play-services_lib as follow:
         properties > android > library > Add: Select the google-play-services_lib from the list.
         Click refresh. Each project then must have Java Build Path > Libraries > Android Dependency: 
         Automatically updated with the JAR resulted from google-play-services_lib build. 

   2. google-play-services_lib (V.10.2.1 , 3/2017)
      -> It has been 5 years since we used the map API V.2. So much updates had been going on. On of 
         our latest impl was to have 'DASHED" polyline in our Map but this is N/A in V2 while it is
         available on MapAPI for Web V.3 (Javascript). Google just released Google Play Service library
         V.10.2.1 (or 'Revision 39") where there is specific API to just do the dash polyline (among 
         many things). We tried to upgrade/install this latest version with so much effort to get it
         compiled. Unfortunately at the end we found Google has CHANGED their Map API that methods
         and Listeners we used (MapFragment.getMap(), etc.) were no longer there, hence broke our
         code and would require a lot of recode in our classes. Hence, we CANCELED this upgrade
         at this time of writing. Note that after we upgraded Google Play Servive to rev. 39, it 
         completely removed our original V.2 artifact inside Google SDK dir; lucky we store a 
         copy in Base/lib.
      -> Upgrade Note:
         --> To update, we used Android SDK Manager, updated "Google Play Service" to latest (rev 39).
             This would delete all previous Google Play Service installed then gave you EMPTY
             folders with only samples and javadoc (DAMN!). Then, you must install new "Google Repository"
             latest rev 45 which will give you "<SDK_INSTALL_DIR>\extras\google\m2repository"
             (NOT to be confused with the other Android Support Repository 
             "<SDK_INSTALL_DIR>\extras\android\m2repository" !!!)
         --> Inside, dig deep into: "..\google\m2repository\com\google\android\gms"
             All play service components are no longer in a single package like V2, but broken apart
             into separate individual folders/components. Not only that, each component come further
             with ALL versions! NO source files but bunch of AAR files.
         --> To use/install Google Map, find folder "play-services-maps", select target version: "10.2.1"
             and copy-paste one: "play-services-maps-10.2.1.aar" into your project folder. Using
             ZIP tool app, extract its content and rename the new main folder to 
             "google-play-services-maps-v10.2.1". Create new sub-folder in it named "libs". Then,
             take its entire compiled classes: "classes.jar" -> rename it to "google-play-services-maps-v10.2.1.jar"
             and paste it into this libs directory.
         --> You are NOT DONE, damnit! This thing depends on another jar which is Google play service "core"
             classes. And that would be: "play-services-basement". Find this beast on the same
             source installed directory, select the same version "10.2.1", extract its "classes.jar"
             from its AAR file. Then, copy-paste this core jar into the same "libs" sub-folder 
             you created above, and rename it in such "google-play-services-basement-v10.2.1.jar".
         --> Now that you did all these pre-work, import the entire "google-play-services-maps-v10.2.1"
             folder as a new "New Android Project with Existing Code" by following the same steps
             as google-play-services_lib V.2 that we did 5 years ago, OK?
      -> REF: 
         => http://stackoverflow.com/questions/37310684/missing-sdk-extras-google-google-play-services-libproject-folder-after-updat

   3. Other jars:
        - Android app requires all dependency jars be directly placed inside [ANDROID_PROJECT]/libs 
        - Android APK is looking for this directory, and automatically include all of them in
          Eclipse > Android project > properties > build path dependency JARs

-> json

-> javascript
    1. json2.js
        - Source: https://github.com/douglascrockford/JSON-js
        - This is JSON javascript functions that creates a "JSON" property in the global object, if there isn't already one, to provide
          support for JSON functions on javascript. Old broswers do not have this library, but modern browsers do have built-in JSON.
          The library sets its value to an object containing a stringify method and a parse method. The parse method uses the eval method to do the parsing, guarding 
          it with several regular expressions to defend against accidental code execution hazards. On current browsers, this file does nothing,
          prefering the built-in JSON object.
        - Used By: WebApp for old browser compatibility
        
    2. moment.js
        - Source: http://momentjs.com/
        - 3rd party library to parse, validate, manipulate, and display dates in JavaScript.        
        
