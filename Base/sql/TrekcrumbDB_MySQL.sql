/***************************************************************
 * SQL Query to create database for the app on MYSQL DB Server
 * Trekcrumb, Inc. 
 * Version 1.0
 * July 2013
 *
 * NOTE:
 * - Login as ROOT or admin user
 * - The step to create 'USER' syntax and setup its privileges below is to create
 *	 login credentials for any app into the DB, preventing using the default admin account
 *
 * WARNING: 
 * 1. When using CLOUD DB SERVER, we do NOT set the SQL server ourselves but they do.
 *    It is possible they would give VERY LIMITED PRIVILEGES to our MySQL account, thus
 *    disallowing us to create our own new database ('TREKCRUMB), nor USER ('trekcrumbAdmin'),
 *    nor even EVENT_SCHEDULER. 
 *    Example: When using OpenShift Cloud, they gave us SUPER privileges so we could do what we needed it.
 *    in contrast, using Heroku Cloud, they provided very limited privileges.
 ***************************************************************/

###################################
# CLEAN UP
###################################
drop USER 'trekcrumbAdmin'@'localhost';
drop database if exists TREKCRUMB;


###################################
# DATABASE
###################################
create database TREKCRUMB;
use TREKCRUMB;


###################################
# APP ADMIN USER
# NOTES:
# 1. The queries below are for localhost and remote server(s). 
#    Make sure you comment/un-comment appropriate queries.
###################################
#LOCALHOST
#create USER 'trekcrumbAdmin'@'localhost' identified by 'letMEin2013';
#grant SELECT,INSERT,UPDATE,DELETE,CREATE,DROP on TREKCRUMB.* to 'trekcrumbAdmin'@'localhost';

#REMOTE:OPENSHIFT
#create USER 'trekcrumbAdmin'@'557b6337e0b8cd62a5000131-trekcrumbs.rhcloud.com' identified by 'letMEin2013';
#grant SELECT,INSERT,UPDATE,DELETE,CREATE,DROP on TREKCRUMB.* to 'trekcrumbAdmin'@'557b6337e0b8cd62a5000131-trekcrumbs.rhcloud.com';

#REMOTE: HEROKU
create USER 'trekcrumbAdmin'@'ehc1u4pmphj917qf.cbetxkdyhwsb.us-east-1.rds.amazonaws.com' identified by 'letMEin2013';
grant SELECT,INSERT,UPDATE,DELETE,CREATE,DROP on TREKCRUMB.* to 'trekcrumbAdmin'@'ehc1u4pmphj917qf.cbetxkdyhwsb.us-east-1.rds.amazonaws.com';


###################################
# USER Table and all its triggers
# NOTES:
# 1. USERID: Alternatively may use int with autoincrement as follow:
#    USERID int(10) unsigned NOT NULL AUTO_INCREMENT,
# 2. SECURITY_TOKEN: A placeholder column to store a temporary pseudo-secondary-authentication for 
#    users who cannot authenticate with their normal username/password combo. One use case for this
#    is user forgets password where our system will not reset his password for him (to prevent
#    spammer/fake requests), but provide this security token the user must enter before he can reset 
#    his own password without login. SECURITY_TOKEN should not be persisted longer than necessary.
# 3. TIMESTAMP columns: we are using UTC_TIMESTAMP (GMT), and not CURRENT_TIMESTAMP, such that it is 
#    always guaranteed to be consistent regardless the server location. Also, in MySQL there is
#    no easy way to retrieve the server's timezone (!).
# 3. CREATED and UPDATED: In order to fill in UTC_TIMESTAMP, they must be 'NULL-able', O/w, MySQL server
#    strangely always fill them with current system timestamp.
#
###################################
drop table if exists USER;
create table USER (
    USERID             CHAR(100)         NOT NULL PRIMARY KEY,
    USERNAME           VARCHAR(25)       NOT NULL,
    EMAIL              VARCHAR(100)      NOT NULL,
    PASSWORD           CHAR(255)         NOT NULL,
    FULLNAME           VARCHAR(100)      NOT NULL,
    SUMMARY            VARCHAR(200)      NULL,
    LOCATION           VARCHAR(100)      NULL,
    PROFILE_IMAGE_NAME VARCHAR(200)      NULL,
    PROFILE_IMAGE_URL  VARCHAR(400)      NULL,
    SECURITY_TOKEN     VARCHAR(20)       NULL,
    IS_ACTIVE          TINYINT(1)        NOT NULL,
    IS_CONFIRM         TINYINT(1)        NOT NULL,
    CREATED            TIMESTAMP         NULL,
    UPDATED            TIMESTAMP         NULL
);
alter table USER add constraint UsernameUniqueConstraint unique (Username);
alter table USER add constraint EmailUniqueConstraint unique (Email);

drop trigger if exists triggerUserInsert;
delimiter |
create trigger triggerUserInsert before insert on USER for each row 
BEGIN
	set new.USERID = UUID();
	set new.CREATED = UTC_TIMESTAMP();
	set new.UPDATED = new.CREATED;
END;
| delimiter ;

drop trigger if exists triggerUserUpdate;
delimiter |
create trigger triggerUserUpdate before update on USER for each row 
BEGIN
	set new.CREATED = old.CREATED;
	set new.UPDATED = UTC_TIMESTAMP();
END;
|
delimiter ;

##############################################
# USER_AUTH_TOKEN Table and all its triggers
# NOTES:
# 1. CREATED: In order to fill in UTC_TIMESTAMP, they must be 'NULL-able', O/w, MySQL server
#    strangely always fill them with current system timestamp.
#
##############################################
drop table if exists USER_AUTH_TOKEN;
create table USER_AUTH_TOKEN (
    USER_AUTH_TOKEN_ID  CHAR(100)       NOT NULL PRIMARY KEY,
    USERID              CHAR(100)       NOT NULL,
    USERNAME            VARCHAR(25)     NOT NULL,
    DEVICE_IDENTITY     VARCHAR(500)    NOT NULL,
    AUTH_TOKEN          VARCHAR(500)    NOT NULL,
    CREATED             TIMESTAMP       NULL,
    FOREIGN KEY (USERID) REFERENCES USER(USERID)
);
drop trigger if exists triggerUserAuthTokenInsert;
delimiter |
create trigger triggerUserAuthTokenInsert before insert on USER_AUTH_TOKEN for each row 
BEGIN
	set new.USER_AUTH_TOKEN_ID = UUID();
	set new.CREATED = UTC_TIMESTAMP();
END;
| delimiter ;


###################################
# TRIP Table and all its triggers
# NOTES:
# 1. The date columns (CREATED/UPDATED/COMPLETED) do not have auto-populate triggers because 
#    their values may come from existing Trips saved on user's local device and then published 
#    to the server. Component(s) who insert the values are responsible to provide these date values.
# 2. The date columns (CREATED/UPDATED/COMPLETED) are, by default, recorded as UTC (GMT) timezone.
# 3. The date columns (CREATED/UPDATED/COMPLETED) are treated as regular
#    String not only because their values come from the caller (no auto-populate),
#    but also it can store the timezone value (which is default to 'GMT').
# 4. TRIPID is not auto-filled/increment, but must be provided by the caller and it must be
#    unique. Hence no trigger to do UUID. The reason is to enable the caller to immediately 
#    retrieve any newly created Trip by the new ID, which the caller must know.
#    Otherwise, no way for DB to return a self-generating ID back to caller.
###################################
drop table if exists TRIP;
create table TRIP (
    TRIPID            CHAR(100)         NOT NULL PRIMARY KEY,
    USERID            CHAR(100)         NOT NULL,
    NAME              VARCHAR(100)      NOT NULL,
    NOTE              VARCHAR(1000)     NULL,
    LOCATION          VARCHAR(400)      NULL,
    STATUS            TINYINT(1)        NOT NULL,
    PRIVACY           TINYINT(1)        NOT NULL,
    PUBLISH           TINYINT(1)        NOT NULL,
    CREATED           VARCHAR(100)      NOT NULL,
    UPDATED           VARCHAR(100)      NOT NULL,
    COMPLETED         VARCHAR(100)      NULL,
    FOREIGN KEY (USERID) REFERENCES USER(USERID)
);


###################################
# PLACE Table and all its triggers
# NOTES:
# 1. The date columns (CREATED, UPDATED): See note on TRIP table.
# 2. LATITUDE stores latitude values whose range is from -90 to 90, with 8-digit decimals provides
#    precision within 1 mm. Hence, the datatype DECIMAL(10, 8).
# 3. LONGITUDE stores longitude values whose range from -180 to 180, with 8-digit decimals provides
#    precision within 1 mm. Hence, the datatype DECIMAL(11, 8).
# 4. PLACEID is not auto-filled/increment, but must be provided by the caller and it must be
#    unique. Hence no trigger to do UUID. The reason is to enable the caller to immediately 
#    retrieve any newly created Place by the new ID, which the caller must know.
#    Otherwise, no way for DB to return a self-generating ID back to caller.
# 5. For each Place create/update/delete, its parent Trip must have its UPDATED column updated with
#    the time the change occurs. The DB auto-trigger would do such task BUT with the time values
#    come from the input parameter (not defaulted UTC_TIMESTAMP()) due to requirement to 
#    maintan synced updated times between server and any local device (phone, etc.)
###################################
drop table if exists PLACE;
create table PLACE (
    PLACEID    CHAR(100)         NOT NULL PRIMARY KEY,
    USERID     CHAR(100)         NOT NULL,
    TRIPID     CHAR(100)         NOT NULL,
    LATITUDE   DECIMAL(10,8)     NOT NULL,
    LONGITUDE  DECIMAL(11,8)     NOT NULL,
    LOCATION   VARCHAR(400)      NULL,
    NOTE       VARCHAR(200)      NULL,
    CREATED    VARCHAR(100)      NOT NULL,
    UPDATED    VARCHAR(100)      NOT NULL,
    FOREIGN KEY (USERID) REFERENCES USER(USERID),
    FOREIGN KEY (TRIPID) REFERENCES TRIP(TRIPID)
);
drop trigger if exists triggerPlaceInsert;
delimiter |
create trigger triggerPlaceInsert after insert on PLACE for each row 
BEGIN
	update TRIP set LOCATION = new.LOCATION, UPDATED = new.CREATED where TRIP.TRIPID = new.TRIPID; 
END;
| delimiter ;

drop trigger if exists triggerPlaceUpdate;
delimiter |
create trigger triggerPlaceUpdate after update on PLACE for each row 
BEGIN
	update TRIP set UPDATED = new.UPDATED where TRIP.TRIPID = old.TRIPID; 
END;
| delimiter ;

drop trigger if exists triggerPlaceDelete;
delimiter |
create trigger triggerPlaceDelete after delete on PLACE for each row 
BEGIN
	update TRIP set UPDATED = UTC_TIMESTAMP() where TRIP.TRIPID = old.TRIPID; 
END;
| delimiter ;


###################################
# PICTURE Table and all its triggers
# NOTES:
# 1. The date columns (CREATED, UPDATED): See note on TRIP table.
# 2. PICTUREID is not auto-filled/increment, but must be provided by the caller and it must be
#    unique. Hence no trigger to do UUID. The reason is to enable the caller to immediately 
#    retrieve any newly created Picture by the new ID, which the caller must know.
#    Otherwise, no way for DB to return a self-generating ID back to caller.
# 3. For each Picture create/update/delete, its parent Trip must have its UPDATED column updated with
#    the time the change occurs. The DB auto-trigger would do such task BUT with the time values
#    come from the input parameter (not defaulted UTC_TIMESTAMP()) due to requirement to 
#    maintan synced updated times between server and any local device (phone, etc.)
###################################
drop table if exists PICTURE;
create table PICTURE (
    PICTUREID           CHAR(100)              NOT NULL PRIMARY KEY,
    USERID              CHAR(100)              NOT NULL,
    TRIPID              CHAR(100)              NOT NULL,
    IMAGE_NAME          VARCHAR(200)           NOT NULL,
    IMAGE_URL           VARCHAR(400)           NULL,
    IS_COVER_PICTURE    TINYINT(1) DEFAULT 0   NULL,
    FEATURE_FOR_PLACEID CHAR(100)              NULL,
    NOTE                VARCHAR(200)           NULL,
    CREATED             VARCHAR(100)           NOT NULL,
    UPDATED             VARCHAR(100)           NOT NULL,
    FOREIGN KEY (USERID) REFERENCES USER(USERID),
    FOREIGN KEY (TRIPID) REFERENCES TRIP(TRIPID)
);
drop trigger if exists triggerPictureInsert;
delimiter |
create trigger triggerPictureInsert after insert on PICTURE for each row 
BEGIN
	update TRIP set UPDATED = new.CREATED where TRIP.TRIPID = new.TRIPID; 
END;
| delimiter ;

drop trigger if exists triggerPictureUpdate;
delimiter |
create trigger triggerPictureUpdate after update on PICTURE for each row 
BEGIN
	update TRIP set UPDATED = new.UPDATED where TRIP.TRIPID = old.TRIPID; 
END;
| delimiter ;

drop trigger if exists triggerPictureDelete;
delimiter |
create trigger triggerPictureDelete after delete on PICTURE for each row 
BEGIN
	update TRIP set UPDATED = UTC_TIMESTAMP() where TRIP.TRIPID = old.TRIPID; 
END;
| delimiter ;


###################################
# FAVORITE Table and all its triggers
###################################
drop table if exists FAVORITE;
create table FAVORITE (
    FAVORITEID         CHAR(100)         NOT NULL PRIMARY KEY,
    TRIPID             CHAR(100)         NOT NULL,
    USERID             CHAR(100)         NOT NULL,
    CREATED            TIMESTAMP         NULL,
    FOREIGN KEY (TRIPID) REFERENCES TRIP(TRIPID),
    FOREIGN KEY (USERID) REFERENCES USER(USERID),
    CONSTRAINT favorite_uniqueConstraint UNIQUE (TRIPID, USERID)
);

drop trigger if exists triggerFavoriteInsert;
delimiter |
create trigger triggerFavoriteInsert before insert on FAVORITE for each row 
BEGIN
    set new.FAVORITEID = UUID();
	set new.CREATED = UTC_TIMESTAMP();
END;
| delimiter ;


###################################
# USER COMMENT Table and all its triggers
# NOTES:
# 1. 'COMMENT' is MySQL reserved word, so we cannot use it
# 2. USERCOMMENTID is not auto-filled/increment, but must be provided by the caller and it must be
#    unique. Hence no trigger to do UUID. The reason is to enable the caller to immediately 
#    retrieve any newly created Comment by the new ID, which the caller must know. 
#    Otherwise, no way for DB to return a self-generating ID back to caller.
###################################
drop table if exists USERCOMMENT;
create table USERCOMMENT (
    USERCOMMENTID      CHAR(100)         NOT NULL PRIMARY KEY,
    TRIPID             CHAR(100)         NOT NULL,
    USERID             CHAR(100)         NOT NULL,
    NOTE               VARCHAR(200)      NULL,
    CREATED            TIMESTAMP         NULL,
    FOREIGN KEY (TRIPID) REFERENCES TRIP(TRIPID),
    FOREIGN KEY (USERID) REFERENCES USER(USERID)
);

drop trigger if exists triggerCommentInsert;
delimiter |
create trigger triggerCommentInsert before insert on USERCOMMENT for each row 
BEGIN
	set new.CREATED = UTC_TIMESTAMP();
END;
| delimiter ;


###################################
# EVENT SCHEDULER
# 1. Do the business rule immediately after the affected record is created, then every xxx interval.
# 2. Business Rules/schedulers:
#    1) TRIP_AUTO_COMPLETE
#       - Run every 6 hours
#       - Update any Trip to status = 'complete' if it is still 'active' but has not been updated for more than 6 hours
#       - UPDATED date format must match with how it is stored in DB by default (as String)
#    2) USERAUTHTOKEN_AUTO_DELETE
#       - Run every 1 day
#       - Delete any UserAuthToken that is more than 14 days old.
#       - CREATED date format must match with how it is stored in DB by default (as String)
#
# 3. Ref: 
#    - http://dev.mysql.com/doc/refman/5.7/en/events-configuration.html
#    - http://dev.mysql.com/doc/refman/5.7/en/create-event.html
###################################
set GLOBAL event_scheduler = ON;
drop event if exists TRIP_AUTO_COMPLETE;
create event TRIP_AUTO_COMPLETE
on schedule every 6 hour starts CURRENT_TIMESTAMP
enable
do 
update TRIP set STATUS = 2 where STATUS = 1 
       and str_to_date(UPDATED, '%Y-%m-%dT%H:%i:%S+0000') < (UTC_TIMESTAMP() - interval 6 hour);

drop event if exists USERAUTHTOKEN_AUTO_DELETE;
create event USERAUTHTOKEN_AUTO_DELETE
on schedule every 1 day starts CURRENT_TIMESTAMP
enable
do
delete from USER_AUTH_TOKEN where str_to_date(CREATED, '%Y-%m-%d %H:%i:%S') < (UTC_TIMESTAMP() - interval 14 day);


###################################
# LOG
# 1. This is OPTIONAL, 
#    for troubleshooting only 
###################################
drop table if exists LOG;
create table LOG (
    CREATED      TIMESTAMP         NULL,
    LOG_SOURCE   VARCHAR(200)      NOT NULL,
	LOG_MESSAGE  VARCHAR(500)      NULL
);

drop trigger if exists triggerLogInsert;
delimiter |
create trigger triggerLogInsert 
before insert 
    on LOG for each row 
BEGIN
	set new.CREATED = UTC_TIMESTAMP();
END;
| delimiter ;


##############################################
# METADATA
# NOTE:
# 1. Clear text password = dontDELm3
##############################################
delete from USER where username = 'metadatauser';
insert into USER
  (USERID, USERNAME, EMAIL, PASSWORD, FULLNAME, SUMMARY, LOCATION, IS_ACTIVE, IS_CONFIRM)
values
  (' ', 'metauser', 'metauser@trekcrumb.com', 
   '34661B2039F545F6C613870D7AC698743F8939D9994FA23F:8E0D4A2B2F2B88250099AF0F83AEB7D2CF68D1A5C770B679', 
   'Trekcrumb Default Meta User',
   'Default user metadata for testing and what have you.', 'California, USA', '1', '1');

