/*
 * Trekcrumb, Inc. 
 * Version 1.0
 * April 2013
 */

--************
-- USER
--************
INSERT INTO USER
  (_id, Username, Email, Password, Firstname, Lastname, Location, Summary, IsActive)
VALUES
  ('1', 'BarkingMeow', 'USERTEST@TEST.ORG', 'password', 'MetaUser', 'BarkingMeow', 'California, USA', 
   'Little-town-cruiser enthusiast. Love to share little nice stories along the way. Or, just lazy weekends with my pups BarkBark and MeowMeow.',
   '1'
);

SELECT * FROM USER;

SELECT * FROM USER WHERE Username = 'BarkingMeow';

SELECT COUNT(*) FROM USER;

DELETE FROM USER WHERE USER._id = '1';


--***********
-- TRIP
--***********
INSERT INTO TRIP
  (_id, ServerTripID, UserID, Name, Status, Privacy, Note, Location)
VALUES
  ('100', 'WAIT_SYNCH', '1', 'Sunday morning trip to local Farmers Market', 'OPEN', 'PUBLIC', 'Heffa lots of fun!', 'San Diego, USA'
);

UPDATE TRIP SET Status = 'COMPLETED' WHERE _id = '100002';

SELECT * FROM TRIP;

SELECT * FROM TRIP WHERE UserID = '1' ORDER BY CreatedDate;

DELETE FROM TRIP WHERE UserID = '1';


--***********
--Test Places
--***********
INSERT INTO PLACE
  (_id, ServerPlaceID, TripID, Latitude, Longitude, Location, Note)
VALUES
  ('1000', 'WAIT_SYNCH', '100', '32.76243', '-117.14818', 'University Height, San Diego, CA', 'Our starting point'
);

SELECT * FROM PLACE WHERE TripID = '100' ORDER BY CreatedDate;

--***********
--MISCALLENOUS
--***********
attach 'C:\ALeagueOfMyOwn\XProjects\Trekcrumb\Junks\database\TrekcrumbDB_UpgradeBackup.sqlite' as UPGRADE_BACKUP_DB;

insert into USER(_id, USERNAME, FULLNAME, SUMMARY, LOCATION, PROFILE_IMAGE_NAME, PROFILE_IMAGE_URL, CREATED, UPDATED)
select _id, USERNAME, FULLNAME, SUMMARY, LOCATION, PROFILE_IMAGE_NAME, PROFILE_IMAGE_URL, CREATED, UPDATED from UPGRADE_BACKUP_DB.USER;

insert into USER_AUTH_TOKEN(_id, USERID, USERNAME, DEVICE_IDENTITY, AUTH_TOKEN, CREATED)
select _id, USERID, USERNAME, DEVICE_IDENTITY, AUTH_TOKEN, CREATED from UPGRADE_BACKUP_DB.USER_AUTH_TOKEN;

detach database UPGRADE_BACKUP_DB;

select * from USER;

select * from USER_AUTH_TOKEN;



