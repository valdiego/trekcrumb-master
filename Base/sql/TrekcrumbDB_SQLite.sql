/*
 * SQL Query to create database for the app on Android Platform (SQLite DB Server)
 * Trekcrumb, Inc. 
 * Version 1.0
 * April 2013
 */
/*********************************************************************
 Required Android Table: 'android_metadata' with a 'locale' column
*******************************************************************/
drop table if exists android_metadata;
create table android_metadata (locale TEXT default 'en_US');
insert into android_metadata values ('en_US');


/********************************************************************
 USER Table and all its triggers
 Note: 
 1. Not all User attributes should be saved locally on device DB 
    (e.g., password, email, etc.)
 2. The original records must come from the server (as central
    repository), hence the ID, CREATED and UPDATED dates are server's values.
    Therefore, no triggers to auto populate these columns.
*******************************************************************/
drop table if exists USER;
create table USER (
    _id                TEXT NOT NULL PRIMARY KEY, --VarChar. Accepts string and numeric
    USERNAME           TEXT NOT NULL UNIQUE,      --Max. 25 chars
    FULLNAME           TEXT NOT NULL,             --Max. 100 chars
    SUMMARY            TEXT,                      --Max. 100 chars
    LOCATION           TEXT,                      --Max. 100 chars
    PROFILE_IMAGE_NAME TEXT NULL,                 --Max. 200 chars
    PROFILE_IMAGE_URL  TEXT NULL,                 --Max. 400 chars
    CREATED            TEXT NOT NULL,             --Format is YYYY-MM-DD HH:MM:SS (GMT)
    UPDATED            TEXT NOT NULL              --Format is YYYY-MM-DD HH:MM:SS (GMT)
);


/********************************************************************
 USER_AUTH_TOKEN Table and all its triggers
 Note: 
 1. The original records must come from the server (as central
    repository), hence the ID and CREATED are server's values.
    Therefore, no triggers to auto populate these columns.
*******************************************************************/
drop table if exists USER_AUTH_TOKEN;
create table USER_AUTH_TOKEN (
    _id              TEXT NOT NULL PRIMARY KEY,  
    USERID           TEXT NOT NULL,
    USERNAME         TEXT NOT NULL,
    DEVICE_IDENTITY  TEXT NOT NULL,
    AUTH_TOKEN       TEXT NOT NULL,
    CREATED          TEXT NOT NULL,               --Format is YYYY-MM-DD HH:MM:SS (GMT)
    FOREIGN KEY (USERID) REFERENCES USER(_id)
);


/********************************************************************
 TRIP Table and all its triggers
 Note: 
 1. If Trip is PUBLISH, then the record is first recorded on
    the server with all its automated default data (TRIPID, CREATED, etc.).
    Then the same record is copied on the local DB with data from the server.
    
    Else, Trip is NOT_PUBLISH, the record is first recorded
    on local device. The caller must provide all the required data for
    TRIPID, and all the dates. 
    
    Hence, no triggers to auto populate the TRIPID, and all the dates.
    
 2. The dates column (CREATED/UPDATED/COMPLETED) are treated as regular
    String not only because their values come from the caller (no auto-populate),
    but also it must store the timezone value (which is default to 'GMT').
********************************************************************/
drop table if exists TRIP;
create table TRIP (
    _id             TEXT NOT NULL PRIMARY KEY,   --VarChar. Accepts string and numeric
    USERID          TEXT NOT NULL,               --Foreign key to USER._id
    NAME            TEXT NOT NULL,                          
    NOTE            TEXT,
    LOCATION        TEXT,
    STATUS          TEXT NOT NULL,
    PUBLISH         TEXT NOT NULL, 
    PRIVACY         TEXT NOT NULL,
    CREATED         TEXT NOT NULL,               --Format is YYYY-MM-DD HH:MM:SS GMT
    UPDATED         TEXT NOT NULL,               --Format is YYYY-MM-DD HH:MM:SS GMT
    COMPLETED       TEXT,                        --Format is YYYY-MM-DD HH:MM:SS GMT
    FOREIGN KEY (USERID) REFERENCES USER(_id)
);


/********************************************************************
 PLACE Table and all its triggers
 Note: 
 1. The dates column (CREATED/UPDATED): See note on TRIP table.
 2. For each Place update/delete, its parent Trip must have its UPDATED column updated with
    the time the change occurs. There is no DB auto-trigger to do such update 
    (example: with UTC_TIMESTAMP()) due to requirement to maintan synced updated times between
    server and any local device (phone, etc.)
 *******************************************************************/
drop table if exists PLACE;
create table PLACE (
    _id         TEXT NOT NULL PRIMARY KEY,      --VarChar. Accepts string and numeric
    USERID      TEXT NOT NULL,
    TRIPID      TEXT NOT NULL,
    LATITUDE    TEXT NOT NULL,
    LONGITUDE   TEXT NOT NULL,
    LOCATION    TEXT,
    NOTE        TEXT,
    CREATED     TEXT NOT NULL,                  --Format is YYYY-MM-DD HH:MM:SS GMT
    UPDATED     TEXT NOT NULL,                  --Format is YYYY-MM-DD HH:MM:SS GMT
    FOREIGN KEY (USERID) REFERENCES USER(_id),
    FOREIGN KEY (TRIPID) REFERENCES TRIP(_id)
);


/********************************************************************
 PICTURE Table and all its triggers
 Note: 
 1. The dates column (CREATED/UPDATED): See note on TRIP table.
 2. For each Picture update/delete, its parent Trip must have its UPDATED column updated with
    the time the change occurs. There is no DB auto-trigger to do such update 
    (example: with UTC_TIMESTAMP()) due to requirement to maintan synced updated times between
    server and any local device (phone, etc.)
*******************************************************************/
drop table if exists PICTURE;
create table PICTURE (
    _id                 TEXT NOT NULL PRIMARY KEY,   
    USERID              TEXT NOT NULL,
    TRIPID              TEXT NOT NULL,               --Foreign key to TRIP._id where this PICTURE belongs to
    IMAGE_NAME          TEXT NOT NULL,               --Filename on SD card or hard drive.
    IMAGE_URL           TEXT NOT NULL,               --URL to image location on SD card or hard drive.
    IS_COVER_PICTURE    INTEGER DEFAULT 0,           --Enum(0,1)
    FEATURE_FOR_PLACEID TEXT,
    NOTE                TEXT,
    CREATED             TEXT NOT NULL,               --Format is YYYY-MM-DD HH:MM:SS GMT
    UPDATED             TEXT NOT NULL,               --Format is YYYY-MM-DD HH:MM:SS GMT
    FOREIGN KEY (USERID) REFERENCES USER(_id),
    FOREIGN KEY (TRIPID) REFERENCES TRIP(_id)
);

