/*
 * Sample queries to read/select/etc. data from App database
 * for testing/verify purpose.
 * Hint: Execute the 'USE [database_name_here]' query first to make sure you are
 *       accessing that database.
 * Login as: trekcrumbAdmin
 *
 */
#USE TREKCRUMB;
USE bfvoatvof8duy9jc;

/********************************************
 * USER records
 *******************************************/
#alter table USER add PROFILE_IMAGE_NAME VARCHAR(200) NULL after LOCATION;
#alter table USER add PROFILE_IMAGE_URL VARCHAR(400) NULL after PROFILE_IMAGE_NAME;
#alter table USER add SECURITY_TOKEN VARCHAR(20) NULL after PROFILE_IMAGE_URL;
#alter table USER add NUM_OF_FAVORITES MEDIUMINT DEFAULT 0 after IS_CONFIRM;
#ALTER TABLE USER MODIFY FULLNAME VARCHAR(100);
#alter table USER drop PROFILE_ICON_URL, drop PROFILE_ICON_BLOB;
alter table USER drop NUM_OF_FAVORITES;
#ALTER TABLE USER MODIFY SECURITY_TOKEN VARCHAR(20);

insert into USER
  (USERID, USERNAME, EMAIL, PASSWORD, FULLNAME, SUMMARY, LOCATION, IS_ACTIVE, IS_CONFIRM)
values
  (' ', 'testOffsetAndLimitUser-10', 'user-10test.org', 
   '34661B2039F545F6C613870D7AC698743F8939D9994FA23F:8E0D4A2B2F2B88250099AF0F83AEB7D2CF68D1A5C770B679', 
   'testOffsetAndLimitUser',
   'Default test user metadata for unit testing', 'California, USA', '1', '1');

insert into USER
  (USERID, USERNAME, EMAIL, PASSWORD, FULLNAME, SUMMARY, LOCATION, IS_ACTIVE, IS_CONFIRM)
values
  (' ', 'userCreatedViaDBUI', 'userCreatedViaDBUI@test.org', 
   '34661B2039F545F6C613870D7AC698743F8939D9994FA23F:8E0D4A2B2F2B88250099AF0F83AEB7D2CF68D1A5C770B679', 
   'User Created Via DB UI 1',
   'Chillin', 'California, USA', '1', '1');

select * from USER order by USER.CREATED desc;

select * from USER order by USER.CREATED desc limit 5, 10;

select * from USER where USERID = '5d26fc22-12dc-11e5-8873-22000b558e24';

select PROFILE_IMAGE_NAME, PROFILE_IMAGE_URL from USER;

select 
    USERID, USERNAME, EMAIL, PASSWORD, FULLNAME, SUMMARY, LOCATION, PROFILE_ICON_URL, IS_ACTIVE, IS_CONFIRM, 
    DATE_FORMAT(CREATED,'%Y-%m-%d %H:%i:%S:%f GMT') as CREATED, 
    DATE_FORMAT(UPDATED,'%Y-%m-%d %H:%i:%S%f GMT') as UPDATED
    from USER order by USER.UPDATED desc;

select * from USER where USERNAME = 'testmetadatauser';

select * from USER where FULLNAME LIKE '%Diegan%' and USERNAME LIKE '%sd%';

select CREATED, UPDATED from USER order by USER.UPDATED desc;
select * from USER where EMAIL = 'ntriadi@hotmail.com';

delete from USER where EMAIL LIKE '%UNITTEST.TREKCRUMB.COM%';

#delete from USER;

delete from USER where FULLNAME LIKE '%Test%';

#delete from USER where USERID = 'efd0c562-c50d-11e5-a69f-30f9edef96ec';

#update USER set LOCATION = 'Galapagos Island, Madagascar' where USERID = '0972e93d-7d60-11e3-85bb-30f9edef96ec';

#update USER set SUMMARY = 'Me love. Some more yes.' where USERID = '0972e93d-7d60-11e3-85bb-30f9edef96ec';

update USER set IS_ACTIVE = 1 where USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec';

update USER set SUMMARY = 'Numpang lewat muter-muter kota Betawi.  New line (not supposed to): Mampir ayo bawaan \'jagung\' dan "beras".' where USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec';

#update USER set PASSWORD = 'FD550EA97BB24EB8B505870B0D5B919E8C4B792B87F3BEF6:960F221F8B82B416299957AB74E6BF98ADAE497FE59F8A47' where USERID = '5d26fc22-12dc-11e5-8873-22000b558e24';
#update USER set PROFILE_IMAGE_URL = '';

/*
 * Delete EVERYTHING based on USER's username:
 * (Must use 'LEFT JOIN'. If use regular 'JOIN', nothing will be deleted if 
 * one of table doesn't have a match.
 * REF: http://stackoverflow.com/questions/3331992/how-to-delete-from-multiple-tables-in-mysql
*/
DELETE t2, t3, t4, t5, t1 FROM USER as t0
LEFT JOIN USERCOMMENT as t2 on t0.USERID = t2.USERID
LEFT JOIN FAVORITE as t3 on t0.USERID = t3.USERID
LEFT JOIN PICTURE as t4 on t0.USERID = t4.USERID
LEFT JOIN PLACE as t5 on t0.USERID = t5.USERID
LEFT JOIN TRIP as t1 on t0.USERID = t1.USERID
#WHERE t0.USERNAME = '1486284592391';
#WHERE t0.USERID = '05fa549d-e46a-11e6-9c96-0ab0d6349de9';
WHERE t0.FULLNAME = 'TripManagerPerfTestSingle';


/********************************************
 * USER AUTH TOKEN records
 *******************************************/
select * from USER_AUTH_TOKEN order by CREATED desc;

select * from USER_AUTH_TOKEN where USERID = '8b3899d2-711c-11e3-91a5-30f9edef96ec' AND AUTH_TOKEN='TESTDEVICE_1388385325634_1388385325635';

insert into USER_AUTH_TOKEN
(USER_AUTH_TOKEN_ID, USERID, USERNAME, DEVICE_IDENTITY, AUTH_TOKEN)
values
('', 'd9228ad9-2074-11e5-b60c-30f9edef96ec', 'bangbetawi', 'Test Device via SQL 13', '1470209982832QA8920');


delete from USER_AUTH_TOKEN where DEVICE_IDENTITY like '%UNITTEST%';

#delete from USER_AUTH_TOKEN where USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec';

#delete from USER_AUTH_TOKEN where str_to_date(CREATED, '%Y-%m-%d %H:%i:%S') < (UTC_TIMESTAMP() - interval 5 minute);

/********************************************
 * TRIP records
 *******************************************/
#alter table TRIP modify NOTE VARCHAR(1000);
#alter table TRIP add NUM_OF_SHARES INTEGER DEFAULT 0 after PUBLISH;
#alter table TRIP add NUM_OF_FAVORITES MEDIUMINT DEFAULT 0 after PUBLISH;
#alter table TRIP drop NUM_OF_FAVORITES;

insert into TRIP 
(USERID, NAME, STATUS, PRIVACY, PUBLISH, NOTE, LOCATION, CREATED, UPDATED, COMPLETED, TRIPID) 
values ('fc6db387-c4c9-11e3-b5b9-30f9edef96ec', 'Trip to Search 12', 1, 1, 1, 'Populated for search page', 'Mars', UTC_TIMESTAMP(), UTC_TIMESTAMP(), UTC_TIMESTAMP(), RAND());

INSERT INTO `TRIP` VALUES 
('14506726885446710','5d26fc22-12dc-11e5-8873-22000b558e24','Xmast House Lights at Candy Cane Lane','','Poway, California, United States',2,1,1,'2015-12-21T04:38:08+0000','2015-12-21T07:39:56+0000','2015-12-21T07:27:31+0000'
);

SELECT * from TRIP order by CREATED DESC;

SELECT * from TRIP where USERID = 'fc6db387-c4c9-11e3-b5b9-30f9edef96ec' order by CREATED DESC;

SELECT * from TRIP where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14492584131548558';

select * from TRIP where NAME = 'Pasadena';

select * from TRIP where name LIKE '%test 23%';

select NAME, PRIVACY from TRIP where PRIVACY != 2;
select NAME, PRIVACY from TRIP where PRIVACY <> 2;
select * from TRIP where STATUS = 1;

select count(*) from TRIP, USER where TRIP.USERID = USER.USERID and USER.USERNAME = 'bangbetawi';

#Trip select (regular):
select 
	TRIPID, USERID, NAME, NOTE, LOCATION, TRIP.STATUS, PRIVACY, PUBLISH, 
#	date_format(CREATED, '%Y-%m-%d %H:%i:%S GMT') as CREATED, 
	date_format(CREATED, '%Y-%m-%dT%H:%i:%S+0000') as CREATED, 
	date_format(UPDATED, '%Y-%m-%d %H:%i:%S GMT') as UPDATED, 
	date_format(COMPLETED, '%Y-%m-%d %H:%i:%S GMT') as COMPLETED
#    Count(*) 
from TRIP 
#where USERID = '258f2ad9-1f28-11e4-9837-30f9edef96ec'
#where USERID = '0972e93d-7d60-11e3-85bb-30f9edef96ec' and UPDATED > '2014-02-18 20:18:42 GMT'
#order by UPDATED DESC;
#group by USERID
order by CREATED DESC;
#limit 0, 1 ;

#Trip select and its components: Use JOIN
select TRIP.TRIPID, TRIP.NAME as TRIP_NAME, TRIP.NOTE as TRIP_NOTE, 
       date_format(TRIP.CREATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_CREATED, 
	   date_format(TRIP.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_UPDATED, 
       PLACE.PLACEID, PLACE.NOTE as PLACE_NOTE,
       date_format(PLACE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PLACE_CREATED,
       date_format(PLACE.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as PLACE_UPDATED
from TRIP
join PLACE
on PLACE.TRIPID = TRIP.TRIPID
where TRIP.USERID = 'd08a34b8-c4e1-11e3-9ac6-30f9edef96ec'
order by TRIP.CREATED desc, PLACE.CREATED asc;

#Trip select and its components: Without JOIN
select USER.USERNAME,
       TRIP.TRIPID, TRIP.NAME as TRIP_NAME, TRIP.NOTE as TRIP_NOTE, 
       date_format(TRIP.CREATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_CREATED, 
	   date_format(TRIP.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_UPDATED, 
       PLACE.PLACEID, PLACE.NOTE as PLACE_NOTE,
       date_format(PLACE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PLACE_CREATED,
       date_format(PLACE.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as PLACE_UPDATED,
	   PICTURE.PICTUREID, PICTURE.NOTE as PICTURE_NOTE, PICTURE.IMAGE_NAME,
       date_format(PICTURE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PICTURE_CREATED,
       date_format(PICTURE.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as PICTURE_UPDATED
from USER, TRIP, PLACE, PICTURE
where USERNAME = 'SDUSER' and TRIP.USERID = USER.USERID and PLACE.TRIPID = TRIP.TRIPID and PICTURE.TRIPID = TRIP.TRIPID
order by TRIP.CREATED desc, PLACE.CREATED asc, PICTURE.CREATED asc;

#Trip Search - Select Trip given trip name or location:
select TRIP.USERID, TRIP.TRIPID, TRIP.NAME as TRIP_NAME, TRIP.NOTE as TRIP_NOTE, TRIP.LOCATION as TRIP_LOCATION,
       date_format(TRIP.CREATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_CREATED, 
	   date_format(TRIP.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_UPDATED
       #USER.USERNAME, USER.FULLNAME, USER.USERID
from USER, TRIP
where USER.IS_ACTIVE = 1 and TRIP.PUBLISH = 1 and TRIP.PRIVACY = 3
      and TRIP.USERID = USER.USERID
	  and (TRIP.NAME LIKE '%san diego%' or TRIP.LOCATION LIKE '%san diego%')
order by TRIP.CREATED desc;

#Trip Search - Select Trip given username or fullname:
/*select TRIP.USERID, TRIP.TRIPID, TRIP.NAME as TRIP_NAME, TRIP.NOTE as TRIP_NOTE, TRIP.LOCATION as TRIP_LOCATION,
       date_format(TRIP.CREATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_CREATED, 
	   date_format(TRIP.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_UPDATED
       #USER.USERNAME, USER.FULLNAME, USER.USERID*/
select count(*)
from USER, TRIP
where TRIP.PUBLISH = 1 and TRIP.PRIVACY = 3 and USER.IS_ACTIVE = 1
	  and TRIP.USERID = USER.USERID
#      and (USER.USERNAME LIKE '%sd%' or USER.FULLNAME LIKE '%sd%')
order by TRIP.CREATED desc;

#Trip Search - Select Trip given Trip keyword and user keyword:
select TRIP.USERID, TRIP.TRIPID, TRIP.NAME as TRIP_NAME, TRIP.NOTE as TRIP_NOTE, TRIP.LOCATION as TRIP_LOCATION,
       date_format(TRIP.CREATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_CREATED, 
	   date_format(TRIP.UPDATED, '%Y-%m-%d %H:%i:%S GMT') as TRIP_UPDATED
       #USER.USERNAME, USER.FULLNAME, USER.USERID
from TRIP, USER
where TRIP.PUBLISH = 1 and TRIP.PRIVACY = 3 and USER.IS_ACTIVE = 1
	  and TRIP.USERID = USER.USERID
	  and (TRIP.NAME LIKE '%san diego%' or TRIP.LOCATION LIKE '%san diego%')
      and (USER.USERNAME LIKE '%jkt%' or USER.FULLNAME LIKE '%jkt%')
order by TRIP.CREATED desc;

SELECT * from TRIP where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14492584131548558';

select TRIPID from TRIP 
#update TRIP set NOTE = 'Trip note is updated during test for event schedule query - 1'
where TRIPID = (
select TRIP.TRIPID
from TRIP, PLACE
where TRIP.STATUS = 1
	  and TRIP.TRIPID = PLACE.TRIPID
      and STR_TO_DATE(PLACE.CREATED, '%Y-%m-%dT%H:%i:%S+0000') < UTC_TIMESTAMP() - INTERVAL 6 HOUR
order by PLACE.CREATED desc limit 1)
;

update TRIP set STATUS = 2
where STATUS = 1
      and str_to_date(UPDATED, '%Y-%m-%dT%H:%i:%S+0000') < UTC_TIMESTAMP() - interval 6 hour
;

update TRIP set NOTE = 'Update 4 - insert character single-quote : Toy\'s and backlash: \\ and chinese char: 是', UPDATED = NOW() where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14492584131548558';
update TRIP set NOTE = 'Contents inserted via MySQL client with new line: \n This is New line', UPDATED = NOW() where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14492584131548558';

#update TRIP set PRIVACY = 3 where NAME LIKE '%Trip to Search%';

update TRIP set LOCATION = 'Pasadena, California, United States' where TRIPID = '15025466320431X6067';

update TRIP set STATUS = 1 where TRIPID = '1463597248157296372';

#delete from TRIP where LOCATION like '%UnitTest Trip Location%';
#delete from TRIP where USERID = '05fa549d-e46a-11e6-9c96-0ab0d6349de9';
#delete from TRIP where NOTE LIKE '%Populated for search page%';
#delete from TRIP;

/********************************************
 * PLACE records
 *******************************************/
select * from PLACE order by PLACE.CREATED desc;

#alter table PLACE modify NOTE VARCHAR(200);

select date_format(PLACE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PLACE_CREATED from PLACE order by PLACE.CREATED desc;

select * from PLACE where TRIPID = '15025466320431X6067';

select PLACEID, TRIPID, NOTE, UPDATED from PLACE where TRIPID = '8729a662-aad6-11e3-b686-30f9edef96ec';

insert into PLACE
  (PLACEID, USERID, TRIPID, LATITUDE, LONGITUDE, CREATED, UPDATED, NOTE)
values
  (' ', '0972e93d-7d60-11e3-85bb-30f9edef96ec', '8729a662-aad6-11e3-b686-30f9edef96ec', 
   '32.75366274', '-117.13980921',
   '2014-03-16 17:30:00 GMT', '2014-03-16 17:30:00 GMT', 'Added 5');

update PLACE 
#set NOTE = 'update 1', 
#	UPDATED = '2014-03-15 21:55:53 GMT' 
set LATITUDE = 34.134806,
    LONGITUDE = -118.126147,
    LOCATION = 'Pasadena, California, United States'
where PLACEID = '15025466320431X606715025466320699J9220' 
and TRIPID = '15025466320431X6067';

#delete from PLACE where TRIPID = '979a3bd2-8d64-11e3-aef9-30f9edef96ec';

#delete from PLACE where PLACEID = '623cdb50-ad30-11e3-b686-30f9edef96ec';

#delete from PLACE where LOCATION LIKE '%UnitTest%';

#update PLACE set LOCATION = 'San Diego 3, CA' where PLACEID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec1446313794502342814465020751609458';



/********************************************
 * PICTURE records
 *******************************************/
#alter table PICTURE add IS_COVER_PICTURE TINYINT(1) DEFAULT 0 NULL after IMAGE_URL;

#alter table PICTURE drop IS_MAIN_PICTURE;

#alter table PICTURE modify NOTE VARCHAR(200);

alter table PICTURE modify IMAGE_URL varchar(400) null;

select * from PICTURE order by CREATED desc;

select * from PICTURE where USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec';

#Picture select based on their User and Trip: Without JOIN
select USER.USERID,
       TRIP.TRIPID, TRIP.PRIVACY,
	   PICTURE.PICTUREID, PICTURE.IMAGE_NAME, PICTURE.IMAGE_URL, PICTURE.NOTE as PICTURE_NOTE,
       date_format(PICTURE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PICTURE_CREATED
from USER, TRIP, PICTURE
where USER.USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec' and (TRIP.PRIVACY = 1 or TRIP.PRIVACY = 2)
      and TRIP.USERID = USER.USERID and PICTURE.TRIPID = TRIP.TRIPID
order by PICTURE.CREATED desc;
#limit 5, 10;

#Picture select based on USERID and TRIP Privacy: Without JOIN
select PICTURE.USERID, PICTURE.TRIPID, PICTURE.PICTUREID, 
       PICTURE.IMAGE_NAME, PICTURE.IMAGE_URL, PICTURE.NOTE as PICTURE_NOTE,
       date_format(PICTURE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PICTURE_CREATED,
       TRIP.PRIVACY
from TRIP, PICTURE
where PICTURE.USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec' and (TRIP.PRIVACY = 1 or TRIP.PRIVACY = 2)
      and PICTURE.TRIPID = TRIP.TRIPID
order by PICTURE.CREATED desc;
#limit 0, 2;

#Picture select based on their Username and Trip: Without JOIN
#select USER.USERNAME,
#	   TRIP.PRIVACY,
select PICTURE.USERID, PICTURE.TRIPID, PICTURE.PICTUREID, 
       PICTURE.IMAGE_NAME, PICTURE.IMAGE_URL, PICTURE.NOTE as PICTURE_NOTE,
       date_format(PICTURE.CREATED, '%Y-%m-%d %H:%i:%S GMT') as PICTURE_CREATED,
       TRIP.NAME as TRIP_NAME
from USER, TRIP, PICTURE
where USER.USERNAME = 'bangbetawi' and (TRIP.PRIVACY = 1 or TRIP.PRIVACY = 2)
      and PICTURE.USERID = USER.USERID and PICTURE.TRIPID = TRIP.TRIPID
order by PICTURE.CREATED desc;
#limit 0, 2;

select count(*)
from USER, TRIP, PICTURE
where USER.USERNAME = 'bangbetawi' and (TRIP.PRIVACY = 1 or TRIP.PRIVACY = 2)
      and PICTURE.USERID = USER.USERID and PICTURE.TRIPID = TRIP.TRIPID
order by PICTURE.CREATED desc;

#update PICTURE set IMAGE_URL = '';

#delete from PICTURE where IMAGE_NAME = 'TripManagerTest';


/**********************************************
 * FAVORITE queries
 *********************************************/
#ALTER TABLE FAVORITE ADD CONSTRAINT favorite_uniqueConstraint UNIQUE (TRIPID, USERID);
#ALTER TABLE FAVORITE DROP PRIMARY KEY, ADD PRIMARY KEY(FAVORITEID);

/* TEST DATA:
   TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14491822244897604'
			'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428'
   USER:
   COUSER ID = d1976351-3dd8-11e4-b314-30f9edef96ec
   JKTUSER ID = 258f2ad9-1f28-11e4-9837-30f9edef96ec
   SDUSER = d08a34b8-c4e1-11e3-9ac6-30f9edef96ec
*/
insert into FAVORITE (FAVORITEID, TRIPID, USERID) values ('', 'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428', 'd1976351-3dd8-11e4-b314-30f9edef96ec');
insert into FAVORITE (FAVORITEID, TRIPID, USERID) values ('', 'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428', '258f2ad9-1f28-11e4-9837-30f9edef96ec');
insert into FAVORITE (FAVORITEID, TRIPID, USERID) values ('', 'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428', 'd08a34b8-c4e1-11e3-9ac6-30f9edef96ec');

#update TRIP set NUM_OF_FAVORITES = 0;
#update USER set NUM_OF_FAVORITES = 0;

select * from FAVORITE where TRIPID = '258f2ad9-1f28-11e4-9837-30f9edef96ec_1409774459523' order by CREATED desc;
select NUM_OF_FAVORITES from TRIP;
select NUM_OF_FAVORITES from TRIP where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428';
select NUM_OF_FAVORITES from USER;
select NUM_OF_FAVORITES from USER where USERID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec';
select NUM_OF_FAVORITES from USER where USERID = '258f2ad9-1f28-11e4-9837-30f9edef96ec';
select NUM_OF_FAVORITES from USER where USERID = 'd08a34b8-c4e1-11e3-9ac6-30f9edef96ec';

#delete from FAVORITE where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14491822244897604' and USERID = 'd1976351-3dd8-11e4-b314-30f9edef96ec';
#delete from FAVORITE where TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14491822244897604';

#select FAVORITE.*
select count(*)
from FAVORITE, USER, TRIP
where FAVORITE.TRIPID = TRIP.TRIPID and FAVORITE.USERID = USER.USERID
      and TRIP.PRIVACY <> 2
      and USER.IS_ACTIVE = 1
      and FAVORITE.TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428';


select USER.*, FAVORITE.CREATED as FAVORITE_CREATED
from FAVORITE, USER
where FAVORITE.USERID = USER.USERID and FAVORITE.TRIPID = 'd9228ad9-2074-11e5-b60c-30f9edef96ec14491822244897604'
order by FAVORITE.CREATED desc;

select TRIP.*, FAVORITE.CREATED as FAVORITE_CREATED
from FAVORITE, TRIP, USER
where FAVORITE.TRIPID = TRIP.TRIPID and FAVORITE.USERID = USER.USERID and USER.USERNAME = '258f2ad9-1f28-11e4-9837-30f9edef96ec'
order by FAVORITE.CREATED desc;


/**********************************************
 * USERCOMMENT queries
 *********************************************/
insert into USERCOMMENT (USERCOMMENTID, TRIPID, USERID, NOTE) values ('', 'd9228ad9-2074-11e5-b60c-30f9edef96ec14463137945023428', 'd9228ad9-2074-11e5-b60c-30f9edef96ec', 'Comment entered via MYSQL.');

select * from USERCOMMENT order by CREATED asc;

select * from USERCOMMENT where TRIPID = '14727142150966C3716' order by CREATED asc;

#delete from USERCOMMENT where TRIPID = '14727142150966C3716';

/**********************************************
 * LOG (troubleshooting) queries
 *********************************************/
select * from LOG;

delete from LOG;

/**********************************************
 * SYSTEM queries
 *********************************************/
show processlist;

SHOW GLOBAL VARIABLES;

#kill 1215;

#select concat('KILL ',id,';') from information_schema.processlist where User = 'trekcrumbAdmin' and Time > 500 into outfile 'C:/TempFiles/MySQL Workbench/AllProcessList.txt';

#source C:/TempFiles/MySQL Workbench/AllProcessList.txt;

select UTC_TIMESTAMP();
RENAME TABLE bfvoatvof8duy9jc.USER TO trekcrumb.USER
