package com.trekcrumb.fileserver.socket;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.ServiceError;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.fileserver.business.FileServerManager;

/**
 * A worker thread that will take care individual client's socket request and send back response.
 * 
 * @author Val Diego
 * @since 04/2014
 *
 */
public class FileServerWorkerThread extends Thread {
    private static final Log LOG = new Log4JLogger("com.trekcrumb.fileserver.FileServerWorkerThread"); 
    private static final String THREAD_NAME = "FileServerWorkerThread";

    private Socket mSocket = null;

    /** Creates a new thread of the worker thread.
     *  @param socket - The Socket that has been established between the
     *                  FileServerSocketListener and a client.
     */
    public FileServerWorkerThread(Socket socket) {
        super(THREAD_NAME);
        mSocket = socket;
    }
    
    public void run() {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting to care for the incoming request ...");
        }
        ObjectInputStream input = null;
        ObjectOutputStream output = null;
        FileServerRequest request = null;
        FileServerResponse response = null;
        try {
            input = new ObjectInputStream(mSocket.getInputStream());
            output = new ObjectOutputStream(mSocket.getOutputStream());

            //Get request object:
            Object requestObj = input.readObject();
            if (!(requestObj instanceof FileServerRequest)) {
                throw new IllegalArgumentException("Incoming request is invalid class type! Expecting FileServerRequest class.");  
            }
            request = (FileServerRequest)requestObj;
            
            //Process:
            response = FileServerManager.process(request);

            //Send response:
            output.writeObject(response);
            output.flush();
            if(LOG.isDebugEnabled()) {
                LOG.debug("Completed.");
            }

        } catch (Exception e) {
            String errMsg = "Unexpected error while processing request!" 
                    + "\nError message: " + e.getMessage()
                    + "\nOriginal request: " + request;
            LOG.error(errMsg, e);
            e.printStackTrace();
            
            //Try to send error response back to client:
            try {
                if(output == null) {
                    //In case this fails and throws exception:
                    output = new ObjectOutputStream(mSocket.getOutputStream());
                }
                if(output != null) {
                    response = new FileServerResponse();
                    response.setSuccess(false);
                    
                    ServiceError error = new ServiceError();
                    if(e instanceof IllegalArgumentException) {
                        error.setErrorEnum(ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR);
                    } else {
                        error.setErrorEnum(ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR);
                    }
                    error.setErrorText(errMsg);
                    response.setServiceError(error);

                    output.writeObject(response);
                    output.flush();
                }
            } catch(Exception e2) {
                LOG.error("Unexpected error when trying to send error response back to client: " + e2.getMessage() + "\n"
                           + "Original cause of failure: " + errMsg);
                e2.printStackTrace();
            }
        } finally {
            if(input != null) {
                try {
                    input.close();
                    input = null;
                } catch(Exception e) {
                    //O well
                }
            }
            if(output != null) {
                try {
                    output.close();
                    output = null;
                } catch(Exception e) {
                    //O well
                }
            }
            try {
                mSocket.close();
            } catch(Exception e) {
                //O well
            }
        }
    }
    

}
