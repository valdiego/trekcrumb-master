package com.trekcrumb.fileserver.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;

import com.trekcrumb.common.utility.CommonConstants;

/**
 * FileServer as a Server Socket (Listener).
 * 
 * As a server, it binds to a configued host and port. It then listens and accepts a Socket connection 
 * request from clients. Once a connection is accepted, the server will create a new thread to serve 
 * the particular client that has been connected and handle request/response from there. 
 * Then it continues to listen for next connections. 
 * 
 * There should only be ONE (singleton) server instance running on any JVM environment. 
 * The server does not require any App or Web Server container to run.
 * 
 * @author Val Diego
 * @since 04/2014
 *
 */
public class FileServerSocketListener {
    private static final Log LOG = new Log4JLogger("com.trekcrumb.fileserver.socket.FileServerSocketListener"); 

    private static FileServerSocketListener mFSSocketListener;
    private String mHost;
    private int mPort;
    private static volatile boolean mRunning = false;
    private static ServerSocket mServerSocket;
    private static Thread mFSListenerThread;
    private static ExecutorService mExecutorSvc;
 
    private FileServerSocketListener(String host, int port) {
        mHost = host;
        mPort = port;
    }
    
    public static FileServerSocketListener getInstance() {
        if(mFSSocketListener == null) {
            mFSSocketListener = new FileServerSocketListener(
                    CommonConstants.FS_SOCKET_TO_LISTEN_HOST,
                    CommonConstants.FS_SOCKET_TO_LISTEN_PORT);
        }
        return mFSSocketListener;
    }

    /**
     * Runs the FileServer socket listener.
     */
    public synchronized void start() {
        try {
            LOG.info("Starting FileServer Socket Listener on [" + mHost + ":" + mPort + "]");
            SocketAddress socketAddress = new InetSocketAddress(mHost, mPort);
            mServerSocket = new ServerSocket();
            mServerSocket.bind(socketAddress);
            
            mRunning = true;
            
            //Start a new thread pool with a specified max. thread numbers
            mExecutorSvc = Executors.newFixedThreadPool(CommonConstants.FS_THREAD_MAX);
            
            /*
             * Start a new thread for this FileServerSocketListener. The purpose here is so that
             * we can call stop() to stop FileServerSocketListener, and clean up resources if necessary
             * instead of a just CTRL-C call.
             */
            mFSListenerThread = new Thread(new FileServerListenerRunnable());
            mFSListenerThread.start();
            
            LOG.info("FileServer Socket Listener successfully started.");
        } catch(Exception e) {
            LOG.error("ERROR while starting FileServer Socket Listener: " + e.getMessage(), e);
            e.printStackTrace();
            stop();
            System.exit(Thread.NORM_PRIORITY);
        }
    }

    /**
     * Stops the FileServer socket listener.
     */
    public synchronized void stop() {
        try {
            LOG.info("Stopping FileServer from listening on [" + mHost + ":" + mPort + "]");
            if(mRunning) {
                LOG.info("Changing listener running status to 'false' ...");
                mRunning = false;
            }
            if(mServerSocket != null) {
                LOG.info("Shutting down server socket ...");
                mServerSocket.close();
                mServerSocket = null;
            }
            if(mFSListenerThread != null) {
                LOG.info("Waiting for main Thread to die off ...");
                mFSListenerThread.join();
            }
            if(mExecutorSvc != null) {
                LOG.info("Shutting down ExecutorService ...");
                mExecutorSvc.shutdown();
                while (!mExecutorSvc.isTerminated()) {
                    //Wait until it is completely terminated
                }
            }
            LOG.info("FileServer Socket Listener successfully stopped.");
        } catch (Exception e) {
            LOG.error("ERROR while stopping FileServer Socket Listener: " + e.getMessage(), e);
        }
    }
    
    /**
     * Allows executable from console command to start and stop FileServerSocketListener.
     */
    public static void main(String[] args) throws Exception {
        if(args.length > 0) {
            String mode = args[0];
            if(mode.equalsIgnoreCase("start")) {
                FileServerSocketListener listener = getInstance();
                listener.start();
                return;
                
            } else if(mode.equalsIgnoreCase("stop")) {
                FileServerSocketListener listener = getInstance();
                listener.stop();
                return;
            }
        }
        
        //Everything else:
        LOG.warn("INVALID command. You must specify start or stop the FileServer Socket Listener. Usage:\n"
                + "java com.trekcrumb.fileserver.socket.FileServerSocketListener start|stop");
    }
    
    /*
     * Runnable class that will run on its own thread, and whose job is to listen for any
     * incoming socket connection request to FileServerSocketListener's ServerSocket. 
     * If the server is running and a new connection is accepted, it will launch a worker 
     * thread in the form of FileServerWorkerThread to take care of the request further, while it 
     * continues to listen for other requests.
     */
    private class FileServerListenerRunnable implements Runnable {
        @Override
        public void run() {
            while(mRunning) {
                try {
                    Socket incomingSocket = mServerSocket.accept();
                    Thread worker = new FileServerWorkerThread(incomingSocket);
                    mExecutorSvc.execute(worker);
                    if(LOG.isDebugEnabled()) {
                        LOG.debug("Accepted an incoming socket request and communication is started.");
                    }
                } catch(IOException ioe) {
                    LOG.error("ERROR while accepting incoming socket: " + ioe.getMessage(), ioe);
                    continue;
                }
            }
        }
    }    
    
}
