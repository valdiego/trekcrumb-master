package com.trekcrumb.fileserver.webservice.restful;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.ServiceError;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.fileserver.business.FileServerManager;
import com.trekcrumb.fileserver.webservice.IFileServerService;

/**
 * Concrete impl of the IFileServerService interface with RESTful technology.
 * 
 * NOTE:
 * 1. "@Consumes" annotation specifies, the request is coming from the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 * 2. "@Produces" annotation specifies, the response is going to the client as either JSON type
 *             ("application/json") or XML type ("application/xml")
 *
 * 3. The framework uses JAXB to intrepret the beans as XML/JSON, and expects the bean class to be
 *    the XML root element. For JSON, the resulting format of bean is different from other JSON-standard
 *    where the bean classname (e.g., 'user' is included as the root element:
 *    {
 *      user:
 *      { "username" : "abc", .... }
 *    }  
 *      
 * @author Val Diego
 * @since 01/2015
 */
@Consumes("application/json")
@Produces("application/json")
@Path("/fileServerService")
public class FileServerServiceRestfulImpl 
implements IFileServerService {
    private static final Log LOG = LogFactory.getLog("com.trekcrumb.fileserver.webservice.restful.FileServerServiceRestfulImpl"); 
    
    @POST
    @GET
    @Path("/serve")
    public FileServerResponse serve(FileServerRequest request) {
        FileServerResponse response = null;
        
        try {
            response = FileServerManager.process(request);
        } catch(Exception e) {
            LOG.error("FileServerService Serve() error: " + e.getMessage() + " \n" + request);
            e.printStackTrace();
            response = new FileServerResponse();
            response.setSuccess(false);
            
            ServiceError error = new ServiceError();
            if(e instanceof IllegalArgumentException) {
                error.setErrorEnum(ServiceErrorEnum.SYSTEM_ILLEGAL_ARGUMENT_ERROR);
            } else {
                error.setErrorEnum(ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR);
            }
            error.setErrorText(e.getMessage());
            response.setServiceError(error);
        }
        
        return response;
    }
    
    
}
