package com.trekcrumb.fileserver.webservice;

import javax.jws.WebService;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;

/**
 * Interface for FileServer as a WebService.
 * NOTE:
 * 1. For CFX WebService SOAP implementation, it is required to declare the interface with @WebService tag.
 * 
 * @author Val Diego
 * @since 01/2015
 */
@WebService
public interface IFileServerService {
    public FileServerResponse serve(FileServerRequest request);
    
}
