package com.trekcrumb.fileserver.business;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.ServiceError;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.exception.ImageFormatNotSupportedException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.FileUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * A File manager to write, read and delete files to/from server.
 * 
 * @author Val Diego
 * @since 04/2014
 *
 */
public class FileServerManager {
    private static final Log LOG = new Log4JLogger("com.trekcrumb.fileserver.business.FileServerManager"); 
    
    public static FileServerResponse process(FileServerRequest request) throws Exception {
        FileServerResponse response = null;
        
        //Get menu:
        if(request == null ) {
            throw new IllegalArgumentException("Incoming request is NULL!");  
        }
        if(request.getFileMenu() == null) {
            throw new IllegalArgumentException("Incoming request has NULL FileServerMenuEnum!");  
        }
        FileServerMenuEnum menuSelected = request.getFileMenu();
        if(LOG.isDebugEnabled()) {
            LOG.debug("Menu received: " + menuSelected);
        }
        
        //Process based on FileServerMenuEnum:
        if(menuSelected == FileServerMenuEnum.WRITE_TREK_PICTURE
            || menuSelected == FileServerMenuEnum.WRITE_PROFILE_PICTURE) {
            response = write(request);
            
        } else if(menuSelected == FileServerMenuEnum.READ_TREK_PICTURE
                    || menuSelected == FileServerMenuEnum.READ_PROFILE_PICTURE) {
            response = read(request);
            
        } else if(menuSelected == FileServerMenuEnum.DELETE_TREK_PICTURE
                    || menuSelected == FileServerMenuEnum.DELETE_PROFILE_PICTURE) {
            response = delete(request);
        } else {
            throw new IllegalArgumentException("Incoming request has unknown FileServerMenuEnum: " + menuSelected);  
        }
        
        return response;
    }
    
    private static FileServerResponse write(FileServerRequest request) throws Exception {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting ...");
        }
        //Validate request:
        if(ValidationUtil.isEmpty(request.getFilename())) {
            throw new IllegalArgumentException("Write File error: Incoming request has invalid EMPTY Filename!");  
        }
        if(request.getFileBytes() == null) {
            throw new IllegalArgumentException("Write File error: Incoming request has invalid EMPTY file bytes to write!");  
        }
        
        //Process:
        String directory = null;
        if(request.getFileMenu() == FileServerMenuEnum.WRITE_TREK_PICTURE) {
            directory = CommonConstants.FS_PICTURE_TREK_DIRECTORY;
        } else {
            directory = CommonConstants.FS_PICTURE_PROFILE_DIRECTORY;
        }
        String fileURI = null;
        try {
            fileURI = FileUtil.writeImageToFileDirectory(directory, request.getFilename(), request.getFileBytes());
        } catch(ImageFormatNotSupportedException ifnse) {
            /*
             * TODO (8/2016): Image format not supported
             * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
             * with error: "Invalid argument to native writeImage". The issue is JRE being used by
             * the server if it is  OpenJDK, it has this bug. Solution is to either change the JRE or use
             * GIF plug-in, including to support GIF animation.
             */
            FileServerResponse response = new FileServerResponse();
            response.setSuccess(false);
            ServiceError error = new ServiceError();
            error.setErrorEnum(ServiceErrorEnum.PICTURE_SERVICE_IMAGE_FORMAT_NOT_SUPPORTED);
            error.setErrorText(ifnse.getMessage());
            response.setServiceError(error);
            return response;
        }
        
        if(LOG.isDebugEnabled()) {
            LOG.debug("Write File completed successfully. New file: " + fileURI);
        }
        FileServerResponse response = new FileServerResponse();
        response.setSuccess(true);
        response.setFilename(request.getFilename());
        response.setFileURL(fileURI);
        return response;
    }
    
    private static FileServerResponse read(FileServerRequest request) throws Exception {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting ...");
        }
        FileServerResponse response = null;
        String filepath = getTargetFilepath(request);
        byte[] fileBytes = FileUtil.readImageFromFileDirectory(filepath);
        if(fileBytes == null) {
            String errMsg = "Read File failed! Cannot find file given its location " + filepath;
            LOG.error(errMsg);

            response = new FileServerResponse();
            response.setSuccess(false);
            ServiceError error = new ServiceError();
            error.setErrorEnum(ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND);
            error.setErrorText(errMsg);
            response.setServiceError(error);
            return response;
        }
        
        response = new FileServerResponse();
        response.setSuccess(true);
        response.setFileBytes(fileBytes);
        if(LOG.isDebugEnabled()) {
            LOG.debug("Completed.");
        }
        return response;
    }
    
    private static FileServerResponse delete(FileServerRequest request) throws Exception {
        if(LOG.isDebugEnabled()) {
            LOG.debug("Starting ...");
        }
        String filepath = getTargetFilepath(request);
        FileUtil.deleteFile(filepath);

        FileServerResponse response = new FileServerResponse();
        response.setSuccess(true);
        if(LOG.isDebugEnabled()) {
            LOG.debug("Completed.");
        }
        return response;
    }    
    
    private static String getTargetFilepath(FileServerRequest request) {
        String filepath = null;
        if(ValidationUtil.isEmpty(request.getFileURL())
                && ValidationUtil.isEmpty(request.getFilename())) {
            throw new IllegalArgumentException("Incoming request must provide either filename or file URL!");  
        }
        
        if(!ValidationUtil.isEmpty(request.getFileURL())) {
            filepath = request.getFileURL();
        } else {
            if(request.getFileMenu() == FileServerMenuEnum.READ_TREK_PICTURE
                    || request.getFileMenu() == FileServerMenuEnum.DELETE_TREK_PICTURE) {
                filepath = CommonConstants.FS_PICTURE_TREK_DIRECTORY + request.getFilename();
            } else {
                filepath = CommonConstants.FS_PICTURE_PROFILE_DIRECTORY + request.getFilename();
            }
        }
        if(LOG.isDebugEnabled()) {
            LOG.debug("Target filepath to read/delete: " + filepath);
        }
        return filepath;
    }
}
