package com.trekcrumb.fileserver.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.fileserver.business.FileServerManager;

public class FileServerServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Log LOG = LogFactory.getLog("com.trekcrumb.fileserver.servlet.FileServletServlet"); 

    protected void doGet(
            HttpServletRequest servletRequest, 
            HttpServletResponse servletResponse) 
            throws ServletException, IOException {
        String imageType = servletRequest.getParameter(CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE);
        String imageName = servletRequest.getParameter(CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_NAME);
        if(ValidationUtil.isEmpty(imageType)
                || (!imageType.equalsIgnoreCase(CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE_PROFILE) 
                        && !imageType.equalsIgnoreCase(CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE_PICTURE))
                || ValidationUtil.isEmpty(imageName)) {
            LOG.error("ERROR: Invalid Request! Required params are not appropriate: ImageType [" + imageType + "], imageName [" + imageName + "]");
            servletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid Request!");
            return;
        }
        
        //Back-end components:
        FileServerResponse fsResponse = null;
        FileServerRequest fsRequest = new FileServerRequest();
        fsRequest.setFilename(imageName);
        if(imageType.equalsIgnoreCase(CommonConstants.FS_SERVLET_URI_PARAM_IMAGE_TYPE_PROFILE)) {
            fsRequest.setFileMenu(FileServerMenuEnum.READ_PROFILE_PICTURE);
        } else {
            fsRequest.setFileMenu(FileServerMenuEnum.READ_TREK_PICTURE);
        }
        try {
            fsResponse = FileServerManager.process(fsRequest);
        } catch(Exception e) {
            LOG.error("ERROR: During reading image file [" + imageName + "] via FileServerManager!", e);
            servletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error!");
            return;
        }

        //Evaluate response:
        if(fsResponse == null) {
            LOG.error("ERROR: Returned FileServerResponse is either NULL or failed! " + fsResponse);
            servletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error!");
            return;
        } else if(!fsResponse.isSuccess()) {
            if(fsResponse.getServiceError() != null && fsResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_NOT_FOUND) {
                servletResponse.sendError(HttpServletResponse.SC_NOT_FOUND, "Target image not found!");
                return;
            }
            
            //Else:
            LOG.error("ERROR: Returned FileServerResponse is either NULL or failed! " + fsResponse);
            servletResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error!");
            return;
        }
        
        //Success:
        servletResponse.setStatus(HttpServletResponse.SC_OK);
        if (imageName.toLowerCase().endsWith(".gif")) {
            servletResponse.setContentType("image/gif");
        } else if(imageName.toLowerCase().endsWith(".png")) {
            servletResponse.setContentType("image/png");
        } else {
            //Default
            servletResponse.setContentType("image/jpeg");
        }
        
        ServletOutputStream outputStream = null;
        byte[] bytes = null;
        try {
            bytes  = fsResponse.getFileBytes();
            servletResponse.setContentLength(bytes.length);
            outputStream = servletResponse.getOutputStream();
            outputStream.write(bytes, 0, bytes.length);
        } finally {
            if(outputStream != null) {
                outputStream.close();
            }
            
            bytes = null;
            fsResponse.setFileBytes(null);
        }
    }
 
    protected void doPost(
            HttpServletRequest servletRequest, 
            HttpServletResponse servletResponse) 
            throws ServletException, IOException {
        servletResponse.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Illegal Access!");
    }
}
