<project name="Trekcrumb-FileServer" basedir="." default="compileJava">

<!-- ******************
     Properties
     ****************** -->
    <property file="../Base/properties/build.properties"/>
    <property file="../Base/properties/env.properties" prefix="ENV"/>
    
<!-- ******************
     Libraries
     ****************** -->
    <path id="library.source">
        <!-- The following libraries are for compile only (not run) for both FileServer as ServerSocket and WebService -->
        <fileset dir="${base-dir.lib.common}">
            <include name="commons-logging-1.1.1.jar"/>
        </fileset>
        <fileset dir="${base-dir.lib.log4j}">
            <include name="log4j-1.2.17.jar"/>
        </fileset>
        
        <!-- FOR  FILESERVER AS WEBSERVICE
             1. They are used for compile only, but not to run since we would use WebServer deployment
         -->
        <fileset dir="${base-dir.lib.apachecfx}">
            <include name="**/*"/>
        </fileset>
        <fileset dir="${base-dir.lib.javax}">
            <include name="javax.ws.rs-api-2.0-m10.jar"/>
        </fileset>        
        <fileset dir="${base-dir.lib.json}">
            <include name="jackson-core-asl-1.9.13.jar"/>
            <include name="jackson-mapper-asl-1.9.13.jar"/>
            <include name="jackson-jaxrs-1.9.13.jar"/>
        </fileset>
        
        <!-- FOR  FILESERVER AS SERVLET
             1. They are used for compile only, but not to run since we would use WebServer deployment
         -->
        <fileset dir="${base-dir.lib.javax}">
            <include name="servlet-api.jar"/>
        </fileset>        
    </path>
    
    <path id="library.subProjects">
        <fileset dir="${base-dir.distribution}">
            <include name="${common-jarName}"/>
        </fileset>
    </path>
    
    
<!-- ******************
     Targets 
     ****************** -->
    <target name="deleteClasses">
        <delete includeemptydirs="true">
            <fileset dir="${fileserver-dir.build}">
                <include name="**/*"/>
            </fileset>
        </delete>        
    </target>
    <target name="deleteSocketPackage" depends="deleteClasses">
        <delete file="${base-dir.distribution}/${fileserver-jarName}"/>
        <delete file="${base-dir.distribution}/${fileserver-zipName}"/>
        <delete dir="${fileserver-dir.build.zipstructure}" />
    </target>
    <target name="deleteWebServicePackage" depends="deleteClasses">
        <delete file="${base-dir.distribution}/${fileserver-ws-warName}"/>
        <delete dir="${fileserver-webservice-dir.webinf}" />
    </target>
    <target name="deleteServletPackage" depends="deleteClasses">
        <delete file="${base-dir.distribution}/Trekcrumb-FileServer-Servlet.war"/>
        <delete dir="${fileserver-webservice-dir.webinf}" />
    </target>
    
    <target name="compileJava">
        <mkdir dir="${fileserver-dir.build}"/>   
        <javac destdir="${fileserver-dir.build}" 
               includeantruntime="false"
               debug="true"
               failonerror="true">
            <src path="${fileserver-dir.src}"/>
            <classpath>
                <path refid="library.source" />
                <path refid="library.subProjects" />
            </classpath>
        </javac>
    </target>

    <!-- ###########################################################################################
         Build FileServer component as SocketListener Server in a ZIP archive.
         This is used if we are to deploy FileServer as a Socket Server (Listener). Extract the ZIP file
         onto the targeted machine's directory. Once done, start the FileServer by executing
         the start script "fileserversocket_start" shell script ot batch file.   
         ####################################################################################### -->
    <target name="buildSocketServerZIP">
        <echo message="**** BUILDING FILE SERVER COMPONENT AS SOCKETLISTENER - ZIP ****"/>
        <antcall target="deleteSocketPackage" />
        <antcall target="compileJava" />
        
        <!-- Build JAR -->
       <copy todir="${fileserver-dir.build}">
            <fileset dir="${fileserver-dir.properties}">
                <include name="log4j.properties"/>
            </fileset>
        </copy>
        <jar destfile="${base-dir.distribution}/${fileserver-jarName}">
            <fileset dir="${fileserver-dir.build}">
                <include name="log4j.properties"/>
                <include name="**/com/trekcrumb/fileserver/business/*"/>
                <include name="**/com/trekcrumb/fileserver/socket/*"/>
                
                <exclude name="**/com/trekcrumb/fileserver/webservice/*"/>
                <exclude name="**/com/trekcrumb/fileserver/socket/*"/>
            </fileset>
            <manifest>
                <attribute name="Solution-Name" value="Trekcrumb-FileServer"/>
                <attribute name="Built-By" value="BeachPorch Software Inc."/>
                <attribute name="Implementation-Vendor" value="BeachPorch Software Inc."/>
            </manifest>
        </jar>
        
        <!-- Prepare -->
        <mkdir dir="${fileserver-dir.build.zipstructure}"/>   
        <mkdir dir="${fileserver-dir.build.zipstructure}/logs"/>   
        <mkdir dir="${fileserver-dir.build.zipstructure}/libs"/>
        
        <copy todir="${fileserver-dir.build.zipstructure}/libs">
            <fileset dir="${base-dir.distribution}" includes="${fileserver-jarName}"/>
            <fileset dir="${base-dir.distribution}" includes="${common-jarName}"/>
            <fileset dir="${base-dir.lib.common}" includes="commons-logging-1.1.1.jar"/>
            <fileset dir="${base-dir.lib.log4j}" includes="log4j-1.2.17.jar"/>
         </copy>
        <copy todir="${fileserver-dir.build.zipstructure}">
            <fileset dir="scripts">
                <include name="FileServerSocket_Start.bat"/>
                <include name="FileServerSocket_Stop.bat"/>
                <include name="FileServerSocket_Start.sh"/>
                <include name="FileServerSocket_Stop.sh"/>
            </fileset>
         </copy>
        <zip destfile="${base-dir.distribution}/${fileserver-socketserver-zipName}">
            <fileset dir="${fileserver-dir.build.zipstructure}"/>
        </zip>
        <delete dir="${fileserver-dir.build.zipstructure}" />
    </target>

    <target name="deployAsSocketServer-local" depends="buildSocketServerZIP">
        <echo message="**** DEPLOYING FILE SERVER COMPONENT AS SOCKETLISTENER - LOCAL ****"/>
        <unzip src="${base-dir.distribution}/${fileserver-socketserver-zipName}" 
               dest="${ENV.env.properties.fileserver.socketserver.deployDir.LOCAL}"/>
    </target>
    
    
    <!-- ###########################################################################################
         Build FileServer component as a WAR archive.
         This is used if we are to deploy FileServer as a WebService.    
         ####################################################################################### -->
    <target name="buildWebServiceWAR">
        <echo message="**** BUILDING FILE SERVER COMPONENT AS WEBSERVICE - WAR ****"/>
        <antcall target="deleteWebServicePackage" />
        <antcall target="compileJava" />
        
        <!-- Prepare -->
        <mkdir dir="${fileserver-webservice-dir.webinf}"/>
        <mkdir dir="${fileserver-webservice-dir.webinf.classes}"/>
        <mkdir dir="${fileserver-webservice-dir.webinf.lib}"/>
        
        <!-- Copy necessary files -->
        <copy todir="${fileserver-webservice-dir.webinf}">
            <fileset dir="${fileserver-dir.properties}">
                <include name="cxf.xml" />
                <include name="fs-ws-web.xml" />
            </fileset>
        </copy>
        <copy todir="${fileserver-webservice-dir.webinf.classes}">
            <fileset dir="${fileserver-dir.build}">
                <include name="com/trekcrumb/fileserver/business/**/*"/>
                <include name="com/trekcrumb/fileserver/webservice/**/*"/>
                
                <exclude name="com/trekcrumb/fileserver/servlet"/>
                <exclude name="com/trekcrumb/fileserver/socket"/>
                <exclude name="com/trekcrumb/fileserver/httpserver"/>
            </fileset>
            <fileset dir="${fileserver-dir.properties}">
                <include name="commons-logging.properties"/>
                <include name="log4j.properties"/>
            </fileset>
        </copy>
        <copy todir="${fileserver-webservice-dir.webinf.lib}">
            <!-- TREKCRUMBS COMPONENTS -->
            <fileset dir="${base-dir.distribution}">
                <include name="${common-jarName}"/>
            </fileset>
            
            <!-- JARS FOR CFX WEBSERVICE -->
            <fileset dir="${base-dir.lib.apachecfx}">
                <include name="**/*"/>
            </fileset>
            <fileset dir="${base-dir.lib.common}">
                <include name="commons-logging-1.1.1.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.log4j}">
                <include name="log4j-1.2.17.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.spring}">
                <include name="org.springframework.aop-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.asm-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.beans-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.context-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.core-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.expression-3.1.3.RELEASE.jar"/>
                <include name="org.springframework.web-3.1.3.RELEASE.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.javax}">
                <include name="javax.ws.rs-api-2.0-m10.jar"/>
            </fileset>        
            <fileset dir="${base-dir.lib.json}">
                <include name="jackson-core-asl-1.9.13.jar"/>
                <include name="jackson-mapper-asl-1.9.13.jar"/>
                <include name="jackson-jaxrs-1.9.13.jar"/>
            </fileset>
        </copy>        
        
        <!-- Package -->
        <war destfile="${base-dir.distribution}/${fileserver-ws-warName}"
             webxml="${fileserver-webservice-dir.webinf}/fs-ws-web.xml">
            <fileset dir="${fileserver-webservice-dir.web}">
                <include name="**/*.*"/>
            </fileset>
            <manifest>
                <attribute name="Solution-Name" value="${fileserver-appName}"/>
                <attribute name="Built-By" value="BeachPorch Software Inc."/>
                <attribute name="Implementation-Vendor" value="BeachPorch Software Inc."/>
            </manifest>
        </war>
        
        <!-- Clean up -->
        <delete dir="${fileserver-webservice-dir.webinf}" />
    </target>
    
    <target name="deployAsWebService-Local" depends="buildWebServiceWAR">
        <echo message="**** DEPLOYING FILE SERVER WEBSERVICE - LOCAL ****"/>
        <property file="../Base/env.properties" prefix="ENV"/>
        
        <delete file="${ENV.env.properties.webservice.deployDir.LOCAL}/${fileserver-ws-warName}"/>
        <delete dir="${ENV.env.properties.webservice.deployDir.LOCAL}/Trekcrumb-FileServer-WS"/>
        
        <copy todir="${ENV.env.properties.webservice.deployDir.LOCAL}" 
              preservelastmodified="true">
            <fileset dir="${base-dir.distribution}">
                <include name="${fileserver-ws-warName}"/>
            </fileset>
        </copy>
    </target>    
    
    <!-- ###########################################################################################
         Build FileServer component as a WAR archive.
         This is used if we are to deploy FileServer as a Servlet.    
         ####################################################################################### -->
    <target name="buildServletWAR">
        <echo message="**** BUILDING FILE SERVER COMPONENT AS SERVLET - WAR ****"/>
        <antcall target="deleteServletPackage" />
        <antcall target="compileJava" />
        
        <!-- Prepare -->
        <mkdir dir="${fileserver-webservice-dir.webinf}"/>
        <mkdir dir="${fileserver-webservice-dir.webinf.classes}"/>
        <mkdir dir="${fileserver-webservice-dir.webinf.lib}"/>
        
        <!-- Copy necessary files -->
        <copy todir="${fileserver-webservice-dir.webinf}">
            <fileset dir="${fileserver-dir.properties}">
                <include name="fs-servlet-web.xml"/>
            </fileset>
        </copy>
        <copy todir="${fileserver-webservice-dir.webinf.classes}">
            <fileset dir="${fileserver-dir.build}">
                <include name="com/trekcrumb/fileserver/business/**/*"/>
                <include name="com/trekcrumb/fileserver/servlet/**/*"/>
                
                <exclude name="com/trekcrumb/fileserver/webservice"/>
                <exclude name="com/trekcrumb/fileserver/socket"/>
                <exclude name="com/trekcrumb/fileserver/httpserver"/>
            </fileset>
            <fileset dir="${fileserver-dir.properties}">
                <include name="commons-logging.properties"/>
                <include name="log4j.properties"/>
            </fileset>
        </copy>
        <copy todir="${fileserver-webservice-dir.webinf.lib}">
            <!-- DO NOT include the  servlet-api.jar since WebServer has already had it -->
            <!-- TREKCRUMBS COMPONENTS -->
            <fileset dir="${base-dir.distribution}">
                <include name="${common-jarName}"/>
            </fileset>
            
            <!-- JARS FOR SERVLET -->
            <fileset dir="${base-dir.lib.common}">
                <include name="commons-logging-1.1.1.jar"/>
            </fileset>
            <fileset dir="${base-dir.lib.log4j}">
                <include name="log4j-1.2.17.jar"/>
            </fileset>
        </copy>        
        
        <!-- Package -->
        <war destfile="${base-dir.distribution}/${fileserver-servlet-warName}"
             webxml="${fileserver-webservice-dir.webinf}/fs-servlet-web.xml">
            <fileset dir="${fileserver-webservice-dir.web}">
                <include name="**/*.*"/>
            </fileset>
            <manifest>
                <attribute name="Solution-Name" value="${fileserver-appName}"/>
                <attribute name="Built-By" value="BeachPorch Software Inc."/>
                <attribute name="Implementation-Vendor" value="BeachPorch Software Inc."/>
            </manifest>
        </war>
        
        <!-- Clean up -->
        <delete dir="${fileserver-webservice-dir.webinf}" />
    </target>    
    
    <target name="deployAsServlet-Local" depends="buildServletWAR">
        <echo message="**** DEPLOYING FILE SERVER SERVLET - LOCAL ****"/>
        <property file="../Base/env.properties" prefix="ENV"/>
        
        <delete file="${ENV.env.properties.webservice.deployDir.LOCAL}/${fileserver-servlet-warName}"/>
        <delete dir="${ENV.env.properties.webservice.deployDir.LOCAL}/Trekcrumb-FileServer-Servlet"/>
        
        <copy todir="${ENV.env.properties.webservice.deployDir.LOCAL}" 
              preservelastmodified="true">
            <fileset dir="${base-dir.distribution}">
                <include name="${fileserver-servlet-warName}"/>
            </fileset>
        </copy>
    </target>    
    
</project>