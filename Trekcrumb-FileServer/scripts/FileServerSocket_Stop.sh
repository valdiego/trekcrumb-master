#!/bin/sh
##########################################################
# UNIX shell script to start FileServer as Socket Listener
# Note:
# 1. To execute, from UNIX (bourne) shell, type: 
     ./fileserver_stop.sh
##########################################################
LIB_DIR=libs
export CLASSPATH=$CLASSPATH:$LIB_DIR/Trekcrumb-FileServer.jar:$LIB_DIR/Trekcrumb-Common.jar:$LIB_DIR/commons-logging-1.1.1.jar:$LIB_DIR/log4j-1.2.17.jar

java com.trekcrumb.fileserver.socket.FileServerSocketListener stop
