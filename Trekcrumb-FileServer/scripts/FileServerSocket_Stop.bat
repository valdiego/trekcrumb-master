@ECHO OFF
REM ######################################################
REM # WINDOWS script to stop FileServer as Socket Listener
REM ######################################################
SET CLASSPATH=.;libs/Trekcrumb-FileServer.jar;libs/Trekcrumb-Common.jar;libs/commons-logging-1.1.1.jar;libs/log4j-1.2.17.jar

java com.trekcrumb.fileserver.socket.FileServerSocketListener stop
