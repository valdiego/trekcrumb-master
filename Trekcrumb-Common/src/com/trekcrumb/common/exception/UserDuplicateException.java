package com.trekcrumb.common.exception;

public class UserDuplicateException extends Exception {
    private static final long serialVersionUID = -1;
    
    public UserDuplicateException() { 
        super();
    }
   
    public UserDuplicateException(String msg) {
        super(msg);
    }
}
