package com.trekcrumb.common.exception;

public class UserNotConfirmedException extends Exception {
    private static final long serialVersionUID = -1;
    
    public UserNotConfirmedException() { 
        super();
    }
   
    public UserNotConfirmedException(String msg) {
        super(msg);
    }
}
