package com.trekcrumb.common.exception;

public class TripNotFoundException extends Exception {
    private static final long serialVersionUID = -1;
    
    public TripNotFoundException() { 
        super();
    }
   
    public TripNotFoundException(String msg) {
        super(msg);
    }
}
