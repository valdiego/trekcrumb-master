package com.trekcrumb.common.exception;

public class ImageFormatNotSupportedException extends Exception {
    private static final long serialVersionUID = -1;
    
    public ImageFormatNotSupportedException() { 
        super();
    }
   
    public ImageFormatNotSupportedException(String msg) {
        super(msg);
    }
}
