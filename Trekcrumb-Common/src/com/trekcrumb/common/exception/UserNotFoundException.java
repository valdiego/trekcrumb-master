package com.trekcrumb.common.exception;

public class UserNotFoundException extends Exception {
    private static final long serialVersionUID = -1;
    
    public UserNotFoundException() { 
        super();
    }
   
    public UserNotFoundException(String msg) {
        super(msg);
    }
}
