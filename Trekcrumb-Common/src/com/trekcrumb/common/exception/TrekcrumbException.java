package com.trekcrumb.common.exception;

public class TrekcrumbException extends Exception {
    private static final long serialVersionUID = -1;
    
    public TrekcrumbException() { 
        super();
    }
   
    public TrekcrumbException(String msg) {
        super(msg);
    }

}
