package com.trekcrumb.common.crypto;

import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Utility class to convert bytes to hexadecimal String, and vice-versa.
 *
 */
public class StringBytesUtil {
    private static final String HEX_VALUES = "0123456789ABCDEF";

    /**
     * Converts an array of bytes to a (hexadecimal) String.
     * 
     * NOTE:
     * Various ways to do it, with different performances.
     * http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
     */
    public static String bytesToHexString(byte [] bytesArray ) {
        if ( bytesArray == null ) {
            return null;
        }
        
        final StringBuilder strBuff = new StringBuilder( 2 * bytesArray.length );
        for ( final byte b : bytesArray ) {
            strBuff.append(HEX_VALUES.charAt((b & 0xF0) >> 4));
            strBuff.append(HEX_VALUES.charAt((b & 0x0F)));
        }
        return strBuff.toString();
    }
    
    /**
     * Converts a (hexadecimal) String to a byte array.
     */
    public static byte[] hexStringToBytes(String stringInput) {
        if(ValidationUtil.isEmpty(stringInput)) {
            return null;
        }
        
        char[] charsArray = stringInput.toCharArray();
        if(charsArray.length % 2 != 0) { 
            throw new IllegalArgumentException("Invalid String input [" + stringInput + "]; Must have even number of characters.");
        }
        
        int strLength = charsArray.length / 2;
        byte[] bytesArray = new byte[strLength];

        for (int i = 0; i < strLength; i++) {
            int high = Character.digit(charsArray[i * 2], 16);
            int low = Character.digit(charsArray[i * 2 + 1], 16);
            int value = (high << 4) | low;
            bytesArray[i] = (byte) value;
        }
        return bytesArray;
    }
    
    /**
     * Compares two byte arrays in a 'length-constant' time. 
     * This means that it will compare ALL the contents of the two bytes before returning
     * the result, therefore the time it takes to complete whether success or failure is the same.
     * 
     * NOTE:
     * 1. This comparison method is also called 'slow-compare', and commonly used to compare two bytes
     *    during credentials authentications. The 'length-constant' time prevents hackers to make
     *    'timing attack', i.e., guessing when first few bytes are the same, and continue to guess 
     *    the remaining bytes until all byte guesses are cracked (correct).
     * 2. Ref: https://crackstation.net/hashing-security.htm
     * 
     * @param firstBytes - The first byte array to compare
     * @param secondBytes - The second byte array to compare
     * @return 'true' if both bytes are exactly the same.
     */
    public static boolean compareTwoBytes(byte[] firstBytes, byte[] secondBytes) {
        if(firstBytes == null || firstBytes.length == 0
                || secondBytes == null || secondBytes.length == 0) {
            return false;
        }
        
        int diff = firstBytes.length ^ secondBytes.length;
        int firstByteLen = firstBytes.length;
        int secondByteLen = secondBytes.length;
        for(int i = 0; i < firstByteLen && i < secondByteLen; i++) {
            diff |= firstBytes[i] ^ secondBytes[i];
        }
        
        return diff == 0;
    }

    
    
}
