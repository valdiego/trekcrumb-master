package com.trekcrumb.common.crypto;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Contains the keys used by the app security and cryptography between server-side and client-side
 * components when exchanging messages. This key must be accessible to both components.
 * 
 * NOTE:
 * 1. List of valid algorithms:
 *    http://docs.oracle.com/javase/6/docs/technotes/guides/security/StandardNames.html#KeyGenerator
 *
 * 2. TODO 8/30/2013: Throw away all Trekcrumb***Key classes. Instead, create SecretKey/PrivateKey/PublicKey
 *    using KeyFactory and SecretKeyFactory classes!
 *    Ref: http://stackoverflow.com/questions/4600106/create-privatekey-from-byte-array
 *    
 * 3. TODO 8/30/2013: Store all re-usable/persistent keys in KeyStore for security.
 *    Ref: java.security.KeyStore
 *
 */
public class SecurityKeyUtil {
    public static final String SECRETKEY_ALGORITHM = "AES";
    public static final String SECRETKEY_FORMAT = "RAW";
    public static final int SECRETKEY_SIZE = 128;
    
    public static final String KEYPAIR_ALGORITHM = "RSA";
    public static final String KEYPAIR_FORMAT_PUBLIC = "X.509";
    public static final String KEYPAIR_FORMAT_PRIVATE = "PKCS#8";
    public static final int KEYPAIR_SIZE = 1024;
    
    public static final String DIGITAL_SIGNATURE_ALGORITHM = "MD5WithRSA";
    
    public static final String HASH_ALGORITHM = "PBKDF2WithHmacSHA1";
    public static final int HASH_SALT_SIZE = 24; //bytes
    public static final int HASH_HASH_SIZE = 24; //bytes
    public static final int HASH_ITERATIONS = 1000;
    public static final String HASH_SALT_SEPARATOR = ":";
    
    /*
     * Secret key with 128 bits key-length.
     * NOTE: Private key with 256 bits key-length cannot be used unless one must install 
     * Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy in its 
     * JRE/security environment. O/w, an exception 'Illegal key size' will be thrown.
     */
    private static final SecretKey SECRET_KEY = new TrekcrumbSecretKey(
            SECRETKEY_ALGORITHM, 
            SECRETKEY_FORMAT, 
            StringBytesUtil.hexStringToBytes("0DC670C9D8B6438BAFE725107538C272"));

    /*
     * A pair of public-private key.
     * NOTE: This pair must be from a single generated KeyPair for a match.
     */
    private static final PublicKey KEY_PAIR_PUBLIC = new TrekcrumbPublicKey(
            KEYPAIR_ALGORITHM, 
            KEYPAIR_FORMAT_PUBLIC,
            StringBytesUtil.hexStringToBytes("30819F300D06092A864886F70D010101050003818D00308189028181009973771EE3E7DD406235759B111CE533F32026151C977CD3A8B3CC8C53A3ACA131981EDE0891BD39E3F05FFB22281EC653363452173B94126BD21B64A8EE5F4E019E8883176474AD820B7629388CFE17E3EEFC8B44DE8D7C44746D8F598C0FD40FCA1B605AA08DBE744A4F54980265D1FD4B31F6B9617155F701BD578E817C1D0203010001"),
            new BigInteger("107756982285390348532353531009526664027514716883065433619757579563574308343555244709720582173232410059548539388855212276178477262494602330883175009412889306181934388845215541386850066876702711857120134097535115498034870939902082387117459573083648219394472909415155719759673652135547242136765569859320622971933"),
            new BigInteger("65537"));
    
    private static final PrivateKey KEY_PAIR_PRIVATE = new TrekcrumbPrivateKey(
            KEYPAIR_ALGORITHM, 
            KEYPAIR_FORMAT_PRIVATE,
            StringBytesUtil.hexStringToBytes("30820276020100300D06092A864886F70D0101010500048202603082025C020100028181009973771EE3E7DD406235759B111CE533F32026151C977CD3A8B3CC8C53A3ACA131981EDE0891BD39E3F05FFB22281EC653363452173B94126BD21B64A8EE5F4E019E8883176474AD820B7629388CFE17E3EEFC8B44DE8D7C44746D8F598C0FD40FCA1B605AA08DBE744A4F54980265D1FD4B31F6B9617155F701BD578E817C1D02030100010281800F8A46FD04DD9D6580CCA32D620D4814FBC13CBA149C2ED30C99B0D46140092285F03EE8037C26EAAEEFCFF891DBF849E37170D9036B50329162C4CD6BE2401A70CF7EDBFEC0AC22E0F564A72CA6BF01A988D1EBAAE9D264D20DDFDA831B519BFCB11A0536EEA0D1913B66B21FD89935F1C4FC693F1A424C4FB9BAD1B396BDE1024100E0B3CF002EF3FCA7830EF101B5D7336FF2EF3319358C22E85C779CA43C39AFCA2CB4F0BB790D0EB05685D306C7871DA327E326B968B5663083E2B4CFDA3B8F99024100AED30DECFEB53F1206D8335C4DAD5B8DCEBA9C426446C17E53CE9C5E71FE3907E88492951B708F5D082B2A8305B89BD2EB0C3C866D598FD96C8FFD18A404732502400309AB44BF5DC10EAAB0EB0530E625E5DF8CE84DF0F1620F6E2097D78890E7157EA13B97ED3D05F3D057A2D0B6A5D6FABBB15076B539AE95FD52754B5211F81102410084C68A770A63C14901A5C62C3E5A81844C13F28581FF3309ACC0E885DA71C802B797A8745268C4995590E332AB6FAFF91AC72A58B64825366E7B553017B1C17902400DF3AD9330CBE4E59271B780B75CBF0F0CDA8C92051C88740A718145F755D878E9B2049787ED5F67B628F9A58C87B3037812719B2B5CFA501B250C873B8B3A6F"),
            new BigInteger("107756982285390348532353531009526664027514716883065433619757579563574308343555244709720582173232410059548539388855212276178477262494602330883175009412889306181934388845215541386850066876702711857120134097535115498034870939902082387117459573083648219394472909415155719759673652135547242136765569859320622971933"),
            new BigInteger("10912661419169869588312409559641553155478816179454434638972352343919353715857853718333391883725887751426273035443063366907190649422107750889903909813896671899092504115532152361016071362993933078458193511860619101857583408273403441107697448388417007403400559967799469564538697718468621613671435196042524933601"));
    
    private SecurityKeyUtil() {}
    
    public static SecretKey getSecretKey() {
        return SECRET_KEY;
    }
    
    public static PublicKey getKeyPairPublic() {
        return KEY_PAIR_PUBLIC;
    }
    
    public static PrivateKey getKeyPairPrivate() {
        return KEY_PAIR_PRIVATE;
    }
    
    /**
     * Generates new private key using specified algorithm for this app.
     * @return A encoded String value of the newly generated key.
     */
    public static TrekcrumbSecretKey generateSecretKey() {
        TrekcrumbSecretKey secretKey = null;
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(SECRETKEY_ALGORITHM);
            keyGen.init(SECRETKEY_SIZE);
            SecretKey generatedSecretKey = keyGen.generateKey();
            secretKey = new TrekcrumbSecretKey(
                    generatedSecretKey.getAlgorithm(),
                    generatedSecretKey.getFormat(),
                    generatedSecretKey.getEncoded());
        } catch(Exception e) {
            System.err.println("ERROR when generateSecretKey(): " + e.getMessage());
            e.printStackTrace();
        }
        return secretKey;
    }

    /**
     * Generates new key-pair (public/private) using specified algorithm for this app.
     */
    public static KeyPair generateKeyPair() {
        KeyPair keyPair = null;
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(KEYPAIR_ALGORITHM);
            keyGen.initialize(KEYPAIR_SIZE);
            keyPair = keyGen.generateKeyPair();
        } catch(NoSuchAlgorithmException nsa) {
            System.err.println("ERROR when generateKeyPair(): " + nsa.getMessage());
            nsa.printStackTrace();
        }
        return keyPair;
    }
    
    /**
     * Generates a new random salt as a hashed byte array.
     */
    public static byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[HASH_SALT_SIZE];
        random.nextBytes(salt);
        return salt;
    }
    
}
