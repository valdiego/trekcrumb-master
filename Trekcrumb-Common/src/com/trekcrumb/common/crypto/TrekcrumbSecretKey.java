package com.trekcrumb.common.crypto;

import javax.crypto.SecretKey;

/**
 * App concrete implementation of SecretKey interface.
 * 
 * NOTE:
 * Each JVM container (web server, eclipse or android device) has their own java.security interface
 * implementations to process secret/public/private keys. Some of them may implement to restrict
 * what kind of interfaces their support (such as RSAPrivateKey, DSAPrivateKey, etc.) and will
 * throw InvalidKeyException if the key type is not what they support.
 *  
 * @author Val Triadi
 *
 */
@SuppressWarnings("serial")
public class TrekcrumbSecretKey implements SecretKey {
    private String algorithm;
    private String format;
    private byte[] encoded;
    
    public TrekcrumbSecretKey(
            String algorithm,
            String format,
            byte[] encoded) {
        this.algorithm = algorithm;
        this.format = format;
        this.encoded = encoded;
    }

    @Override
    public String getAlgorithm() {
        return algorithm;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public byte[] getEncoded() {
        return encoded;
    }
}
