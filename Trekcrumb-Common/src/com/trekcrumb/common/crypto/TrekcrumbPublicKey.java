package com.trekcrumb.common.crypto;

import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;

/**
 * App concrete implementation of PublicKey interface.
 * 
 * NOTE:
 * 1. Each JVM container (web server, eclipse or android device) has their own java.security interface
 *    implementations to process secret/public/private keys. Some of them may implement to restrict
 *    what kind of interfaces their support (such as RSAPrivateKey, DSAPrivateKey, etc.) and will
 *    throw InvalidKeyException if the key type is not what they support.
 * 
 * 2. The class extends RSAPublicKey because the algorithm used to generate it is 'RSA' algorithm.
 *
 * @author Val Triadi
 *
 */
@SuppressWarnings("serial")
public class TrekcrumbPublicKey implements RSAPublicKey {
    private String algorithm;
    private String format;
    private byte[] encoded;
    private BigInteger modulus;
    private BigInteger publicExponent;
    
    public TrekcrumbPublicKey(
            String algorithm,
            String format,
            byte[] encoded,
            BigInteger modulus,
            BigInteger publicExponent) {
        this.algorithm = algorithm;
        this.format = format;
        this.encoded = encoded;
        this.modulus = modulus;
        this.publicExponent = publicExponent;
    }

    @Override
    public String getAlgorithm() {
        return algorithm;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public byte[] getEncoded() {
        return encoded;
    }

    @Override
    public BigInteger getModulus() {
        return modulus;
    }

    @Override
    public BigInteger getPublicExponent() {
        return publicExponent;
    }

}
