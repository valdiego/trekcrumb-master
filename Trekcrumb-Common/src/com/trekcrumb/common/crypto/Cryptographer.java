package com.trekcrumb.common.crypto;

import java.security.InvalidKeyException;
import java.security.Signature;
import java.security.SignatureException;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Cryptography class which provides functionality associated with encryption and decryption.
 * 
 */
public class Cryptographer {
    private static Cryptographer mCryptographer;
    private Cipher mCipher;
    private Signature mSignature;
    private SecretKeyFactory mSecretKeyFactory;
    
    private static int mInstanceIndex = 0;

    protected Cryptographer() throws Exception {
        /*
         * Note (11/2014):
         * 1. Constructor is declared 'protected' such that any random client classes cannot create
         *    new instance of it, thus forcing a singleton. Yet, any class can extend this class
         *    and have access to the constructor thus breaking the 'singleton' pattern. 
         *    In contrast, if it was 'private', the class is not extendable and better declared 
         *    as a 'final class' as well.
         * 2. Any class in the same package can create new instance of this class, thus breaking
         *    the singleton pattern.
         * 3. Multi-threading issues:
         *    a. The init() must be invoked here in the constructor, not in the getInstance() after
         *       a new Cryptographer is created. This guarantees the first call to getInstance() results
         *       in completely initialized instance. If init() was done in getInstance(), a client may
         *       get the Cryptographer instance but with unfinished initialization (e.g., Cipher is null, etc.)
         *       because the first call is still processing the init() to complete, thus causing
         *       NullPointerException is many of its operation.
         *    b. Implements 'double-check locking (synchronized)' on the getInstance() to ensure not
         *       every client creates a new instance when their request occurs at the same time.
         *    c. Implements object locking (synchronized) for all class variables (Cipher, Signature,
         *       and SecretKeyFactory) during method calls because they are being re-init in each
         *       method calls. Without locking them, it will cause random conflicts and failures
         *       during concurrency perf tests.
         * 
         * 4. REF: http://www.javaworld.com/article/2073352/core-java/simply-singleton.html 
         */
        System.out.println("New Cryptographer instance created: " + mInstanceIndex);
        init();
    }

    /**
     * Returns singleton reference.
     */
    public static Cryptographer getInstance() throws Exception {
        if(mCryptographer == null) {
            synchronized(Cryptographer.class) {
                if(mCryptographer == null) {
                    mInstanceIndex++;
                    mCryptographer = new Cryptographer();
                }
             }
        }
        return mCryptographer;
    }
    
    /**
     * Initializes the cryptographer by reading in a secret key from a file and
     * setting up the cipher class.
     */
    private void init() throws Exception {
        if(mCipher == null) {
            mCipher = Cipher.getInstance(SecurityKeyUtil.SECRETKEY_ALGORITHM);
        }
        if(mSignature == null) {
            mSignature = Signature.getInstance(SecurityKeyUtil.DIGITAL_SIGNATURE_ALGORITHM);
        }
        if(mSecretKeyFactory == null) {
            mSecretKeyFactory = SecretKeyFactory.getInstance(SecurityKeyUtil.HASH_ALGORITHM);
        }
    }
    
    /**
     * Encrypts the provided input value using the app. secret private key.
     *
     * @param inputStr - The input value to encrypt.
     * @return The encrypted value in String.
     */
    public String encrypt(String inputStr) throws Exception {
        if(ValidationUtil.isEmpty(inputStr)) {
            return null;
        }
        
        byte[] encryptedBytes = null;
        synchronized(Cipher.class) {
            mCipher.init(Cipher.ENCRYPT_MODE, SecurityKeyUtil.getSecretKey());
            encryptedBytes = mCipher.doFinal(inputStr.getBytes());
         }
        String encryptedStr = StringBytesUtil.bytesToHexString(encryptedBytes);
        return encryptedStr;
    }

    /**
     * Decrypts the provided input value using the app. secret private key.
     *
     * @param inputStr - The input value to decrypt.
     * @return The decrypted value in String.
     */
    public String decrypt(String inputStr) throws Exception {
        if(ValidationUtil.isEmpty(inputStr)) {
            return null;
        }
        
        byte[] inputBytes = StringBytesUtil.hexStringToBytes(inputStr);
        byte[] decryptedBytes = null;
        synchronized(Cipher.class) {
            mCipher.init(Cipher.DECRYPT_MODE, SecurityKeyUtil.getSecretKey());
            decryptedBytes = mCipher.doFinal(inputBytes);
         }
        return new String(decryptedBytes);
    }

    /**
     * Digitally sign the provided input String.
     * @return The digital signature.
     */
    public String sign(String inputMsg) throws Exception {
        if(ValidationUtil.isEmpty(inputMsg)) {
            return null;
        }
        
        byte[] inputMsgBytes = inputMsg.getBytes("UTF8");
        byte[] signature = null;
        synchronized(Signature.class) {
            mSignature.initSign(SecurityKeyUtil.getKeyPairPrivate());
            mSignature.update(inputMsgBytes);
            signature = mSignature.sign();
        }
        return StringBytesUtil.bytesToHexString(signature);
    }

    /**
     * Validates the digital signature of the input String.
     * @return TRUE if the signature is valid. False otherwise.
     */
    public boolean verifySign(String inputMsg, String signature) throws Exception {
        if(ValidationUtil.isEmpty(inputMsg)
                || ValidationUtil.isEmpty(signature)) {
            return false;
        }

        byte[] inputMsgBytes = inputMsg.getBytes("UTF8");
        byte[] signBytes = StringBytesUtil.hexStringToBytes(signature);
        
        boolean isVerified = false;
        synchronized(Signature.class) {
            try {
                mSignature.initVerify(SecurityKeyUtil.getKeyPairPublic());
                mSignature.update(inputMsgBytes);
                isVerified = mSignature.verify(signBytes);
            } catch(InvalidKeyException ike) {
                ike.printStackTrace();
                isVerified = false;
            } catch(SignatureException se) {
                se.printStackTrace();
                isVerified = false;
            }
        }
        return isVerified;
    }
    
    /**
     * Adds secured salt and then hashes the input message String. 
     * NOTE:
     * 1. It is by design for security purpose that the salted and hashed String should NOT be 
     *    reversible into it original clear text.
     * 2. To compare a clear text against its salted-n-hashed text, see verifySaltAndHash().
     * 
     * @return A salted-n-hashed String.
     */
    public String saltAndHash(String inputMsg) throws Exception {
        if(ValidationUtil.isEmpty(inputMsg)) {
            return null;
        }

        char[] inputMsgChars = inputMsg.toCharArray();
        byte[] salt = SecurityKeyUtil.generateSalt();
        PBEKeySpec spec = new PBEKeySpec(inputMsgChars, 
                                         salt, 
                                         SecurityKeyUtil.HASH_ITERATIONS, 
                                         SecurityKeyUtil.HASH_HASH_SIZE * 8);
        byte[] hash =  mSecretKeyFactory.generateSecret(spec).getEncoded();
        return StringBytesUtil.bytesToHexString(salt) 
                + SecurityKeyUtil.HASH_SALT_SEPARATOR + StringBytesUtil.bytesToHexString(hash);
    }
    
    /**
     * Validates a clear-text String against a salted-n-hashed String if they are
     * the same 'clear-text' Strings.
     *
     * @param inputMsg - The clear-text String to validate
     * @param saltedAndHashedOtherMsg - The salted-n-hashed String as the base of comparison.
     * @return 'true' if the inputMsg is valid, i.e. the same String as the salted-n-hashed message.
     */
    public boolean verifySaltAndHash(
            String inputMsg, 
            String saltedAndHashedOtherMsg) 
            throws Exception {
        if(ValidationUtil.isEmpty(inputMsg)
                || ValidationUtil.isEmpty(saltedAndHashedOtherMsg)) {
            return false;
        }
        
        String[] decodedSaltedAndHashedStrings = saltedAndHashedOtherMsg.split(SecurityKeyUtil.HASH_SALT_SEPARATOR);
        if(decodedSaltedAndHashedStrings == null || decodedSaltedAndHashedStrings.length != 2) {
            return false;
        }
        byte[] saltOfOtherMsg = StringBytesUtil.hexStringToBytes(decodedSaltedAndHashedStrings[0]);
        byte[] hashOfOtherMsg = StringBytesUtil.hexStringToBytes(decodedSaltedAndHashedStrings[1]);

        PBEKeySpec spec = new PBEKeySpec(inputMsg.toCharArray(), 
                                         saltOfOtherMsg, 
                                         SecurityKeyUtil.HASH_ITERATIONS, 
                                         SecurityKeyUtil.HASH_HASH_SIZE * 8);
        byte[] hashedInputMsg =  mSecretKeyFactory.generateSecret(spec).getEncoded();
        return StringBytesUtil.compareTwoBytes(hashedInputMsg, hashOfOtherMsg);
    }
    
}
