package com.trekcrumb.common.bean;

import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Java bean for User Authentication token.
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. A Hibernate bean. A no-argument constructor is a required for a persistent class since
 *    Hibernate has to create objects using Java Reflection.
 * 3. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 4. As a JSON bean since it is JAXB compliant.
 * 5. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library. A no-argument constructor is a required.
 * 
 * @author Val Triadi
 * @since 03/2013
 */
public class UserAuthToken {
    private String id;
    private String userId;
    private String username;
    private String deviceIdentity;
    private String authToken;
    private String created;
    
    public UserAuthToken clone() {
        UserAuthToken clonedEntity = new UserAuthToken();
        clonedEntity.setId(id);
        clonedEntity.setUserId(userId);
        clonedEntity.setUsername(username);
        clonedEntity.setDeviceIdentity(deviceIdentity);
        clonedEntity.setAuthToken(authToken);
        clonedEntity.setCreated(created);
        return clonedEntity;
    }
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("UserAuthToken:\n");
        strBld.append("ID [" + id + "]");
        if(ValidationUtil.isEmpty(userId)) {
            strBld.append(", userId [null]");
        } else {
            strBld.append(", userId [protected]");
        }
        strBld.append(", username [" + username + "]");
        strBld.append(", deviceIdentity [" + deviceIdentity + "]");
        if(ValidationUtil.isEmpty(authToken)) {
            strBld.append(", authToken [null]");
        } else {
            strBld.append(", authToken [protected]");
        }
        strBld.append(", created [" + created + "]");
        return strBld.toString();
    }    

    public String getId() {
        return id;
    }
    public void setId(String userRememberMeId) {
        this.id = userRememberMeId;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getDeviceIdentity() {
        return deviceIdentity;
    }
    public void setDeviceIdentity(String deviceIdentity) {
        this.deviceIdentity = deviceIdentity;
    }
    public String getAuthToken() {
        return authToken;
    }
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    
}
