package com.trekcrumb.common.bean;

import java.io.Serializable;

import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.YesNoEnum;

/**
 * Representation of a Picture bean.
 *
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. A Hibernate bean. A no-argument constructor is a required for a persistent class since
 *    Hibernate has to create objects using Java Reflection.
 * 3. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 4. As a JSON bean since it is JAXB compliant.
 * 5. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library. A no-argument constructor is a required.
 * 
 * @author Val Triadi
 * @since 03/2013
 */
public class Picture implements Serializable {
    private static final long serialVersionUID = -1; 
     
    private String id;
    private String userID;
    private String tripID;
    private String imageName;
    private String imageURL;
    private byte[] imageBytes;
    private String imageBase64Data;
    private String note;
    private YesNoEnum coverPicture;
    private String featureForPlaceID;
    private String created;
    private String updated;

    //Non-DB persistent properties (e.g., for enrichment/display/etc.)
    private String username;
    private String userFullname;
    private String userImageName;
    private String tripName;
    private TripPrivacyEnum privacy;

    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("Picture:\n");
        strBld.append("Id [" + id + "]");
        strBld.append(", UserId [" + userID + "]");
        strBld.append(", TripId [" + tripID + "]");
        strBld.append(", Note [" + note + "]");
        strBld.append(", Image URL [" + imageURL + "]");
        strBld.append(", Image Name [" + imageName + "]");
        if(imageBytes != null) {
            strBld.append(", Image Bytes [PROVIDED]");
        } else {
            strBld.append(", Image Bytes [NULL]");
        }
        strBld.append(", is cover picture [" + coverPicture + "]");
        strBld.append(", created [" + created + "]");
        strBld.append(", updated [" + updated + "]");
        return strBld.toString();
    }

    public Picture clone() {
        Picture picCloned = new Picture();
        picCloned.setId(id);
        picCloned.setTripID(tripID);
        picCloned.setUserID(userID);
        picCloned.setImageBytes(imageBytes);
        picCloned.setImageName(imageName);
        picCloned.setImageURL(imageURL);
        picCloned.setCoverPicture(coverPicture);
        picCloned.setFeatureForPlaceID(featureForPlaceID);
        picCloned.setNote(note);
        picCloned.setCreated(created);
        picCloned.setUpdated(updated);
        return picCloned;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTripID() {
        return tripID;
    }

    public void setTripID(String tripID) {
        this.tripID = tripID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
    public String getImageURL() {
        return imageURL;
    }
    public void setImageURL(String imageLoc) {
        this.imageURL = imageLoc;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBLOB) {
        this.imageBytes = imageBLOB;
    }

    public String getImageBase64Data() {
        return imageBase64Data;
    }

    public void setImageBase64Data(String imageBase64Data) {
        this.imageBase64Data = imageBase64Data;
    }
    
    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public YesNoEnum getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(YesNoEnum coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getFeatureForPlaceID() {
        return featureForPlaceID;
    }

    public void setFeatureForPlaceID(String placeID) {
        this.featureForPlaceID = placeID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public TripPrivacyEnum getPrivacy() {
        return privacy;
    }

    public void setPrivacy(TripPrivacyEnum privacy) {
        this.privacy = privacy;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserImageName() {
        return userImageName;
    }

    public void setUserImageName(String userImageName) {
        this.userImageName = userImageName;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }



     
}
