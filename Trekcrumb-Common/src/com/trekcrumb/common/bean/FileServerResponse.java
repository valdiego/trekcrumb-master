package com.trekcrumb.common.bean;

import java.io.Serializable;

import com.trekcrumb.common.enums.FileServerMenuEnum;

/**
 * Java bean for Trekcrumb FileServer Response.
 * 
 * @author Val Triadi
 * @since 04/2014
 */
@SuppressWarnings("serial")
public class FileServerResponse implements Serializable {
    
    private FileServerMenuEnum fileMenu;
    private boolean isSuccess;
    private ServiceError serviceError;

    private String filename;
    private String fileURL;
    private byte[] fileBytes;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("FileServerResponse: ");
        strBld.append("File Menu: " + fileMenu + "\n");
        strBld.append("isSuccess [" + isSuccess + "]\n");
        strBld.append("serviceError: " + serviceError + "\n");
        if(fileBytes != null) {
            strBld.append("File Bytes [Not Null]");
        } else {
            strBld.append("File Bytes [Null]");
        }
        return strBld.toString();
    }    

    public boolean isSuccess() {
        return isSuccess;
    }
    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public String getFileURL() {
        return fileURL;
    }
    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }
    public byte[] getFileBytes() {
        return fileBytes;
    }
    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }
    public FileServerMenuEnum getFileMenu() {
        return fileMenu;
    }
    public void setFileMenu(FileServerMenuEnum fileMenu) {
        this.fileMenu = fileMenu;
    }
    public ServiceError getServiceError() {
        return serviceError;
    }
    public void setServiceError(ServiceError serviceError) {
        this.serviceError = serviceError;
    }

    
}
