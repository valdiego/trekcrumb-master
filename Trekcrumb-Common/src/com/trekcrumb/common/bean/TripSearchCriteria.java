package com.trekcrumb.common.bean;

import java.io.Serializable;

import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;

@SuppressWarnings("serial")
public class TripSearchCriteria implements Serializable {
    private int offset;
    private int numOfRows;
    private OrderByEnum orderBy;
    private String tripKeyword;
    private String userKeyword;
    private TripStatusEnum status;
    
    public void clear() {
        offset = 0;
        numOfRows = -1;
        orderBy = OrderByEnum.ORDER_BY_CREATED_DATE;
        tripKeyword = null;
        userKeyword = null;
        status = null;
   }
    
    public void setOrderBy(OrderByEnum orderBy) {
        this.orderBy = orderBy;
    }
    public OrderByEnum getOrderBy() {
        if(orderBy == null) {
            return OrderByEnum.ORDER_BY_CREATED_DATE;
        }
        return orderBy;
    }
    public int getOffset() {
        if(offset < 0) {
            return 0;
        }
        return offset;
    }
    public void setOffset(int offset) {
        this.offset = offset;
    }
    public int getNumOfRows() {
        if(numOfRows < 1) {
            return CommonConstants.RECORDS_NUMBER_MAX;
        }
        return numOfRows;
    }
    public void setNumOfRows(int numOfRows) {
        this.numOfRows = numOfRows;
    }
    
    public String getTripKeyword() {
        return tripKeyword;
    }

    public void setTripKeyword(String tripKeyword) {
        this.tripKeyword = tripKeyword;
    }

    public String getUserKeyword() {
        return userKeyword;
    }

    public void setUserKeyword(String userKeyword) {
        this.userKeyword = userKeyword;
    }
    
    public TripStatusEnum getStatus() {
        return status;
    }
    public void setStatus(TripStatusEnum status) {
        this.status = status;
    }


    
}
