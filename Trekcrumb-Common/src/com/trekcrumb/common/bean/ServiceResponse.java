package com.trekcrumb.common.bean;

import java.io.Serializable;
import java.util.List;

import com.trekcrumb.common.enums.ServiceTypeEnum;

/**
 * Java bean for Trekcrumb WebService Response.
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 3. As a JSON bean since it is JAXB compliant.
 * 4. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library.
 * 
 * @author Val Triadi
 * @since 08/2013
 */
@SuppressWarnings("serial")
public class ServiceResponse implements Serializable {
    private ServiceTypeEnum serviceType;
    private boolean isSuccess;
    private String successMessage;
    private ServiceError serviceError;
    
    private User user;
    private List<Trip> listOfTrips;
    private List<Place> listOfPlaces;
    private List<Picture> listOfPictures;
    private List<User> listOfUsers;
    private List<UserAuthToken> listOfUserAuthTokens;
    private List<Favorite> listOfFavorites;
    private List<Comment> listOfComments;
    
    private int numOfRecords;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("ServiceResponse:\n");
        strBld.append("ServiceType: " + serviceType + "\n");
        strBld.append("isSuccess: " + isSuccess + "\n");
        if(serviceError != null) {
            strBld.append(serviceError.toString() + "\n");
        } else {
            strBld.append("ServiceError: NULL" + "\n");
        }
        return strBld.toString();
    }
    
    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }
    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }
    public boolean isSuccess() {
        return isSuccess;
    }
    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }
    public String getSuccessMessage() {
        return successMessage;
    }
    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }
    public ServiceError getServiceError() {
        return serviceError;
    }
    public void setServiceError(ServiceError error) {
        this.serviceError = error;
    }
    
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public List<Trip> getListOfTrips() {
        return listOfTrips;
    }

    public void setListOfTrips(List<Trip> listOfTrips) {
        this.listOfTrips = listOfTrips;
    }

    public List<Place> getListOfPlaces() {
        return listOfPlaces;
    }

    public void setListOfPlaces(List<Place> listOfPlaces) {
        this.listOfPlaces = listOfPlaces;
    }

    public List<Picture> getListOfPictures() {
        return listOfPictures;
    }

    public void setListOfPictures(List<Picture> listOfPictures) {
        this.listOfPictures = listOfPictures;
    }

    public List<User> getListOfUsers() {
        return listOfUsers;
    }

    public void setListOfUsers(List<User> listOfUsers) {
        this.listOfUsers = listOfUsers;
    }

    public int getNumOfRecords() {
        return numOfRecords;
    }

    public void setNumOfRecords(int numOfRecords) {
        this.numOfRecords = numOfRecords;
    }

    public List<Favorite> getListOfFavorites() {
        return listOfFavorites;
    }

    public void setListOfFavorites(List<Favorite> listOfFavorites) {
        this.listOfFavorites = listOfFavorites;
    }

    public List<Comment> getListOfComments() {
        return listOfComments;
    }

    public void setListOfComments(List<Comment> listOfComments) {
        this.listOfComments = listOfComments;
    }

    public List<UserAuthToken> getListOfUserAuthTokens() {
        return listOfUserAuthTokens;
    }

    public void setListOfUserAuthTokens(List<UserAuthToken> listOfUserAuthTokens) {
        this.listOfUserAuthTokens = listOfUserAuthTokens;
    }

    
}
