package com.trekcrumb.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;

/**
 * Java bean for Trip (aka, 'Trek').
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. A Hibernate bean. A no-argument constructor is a required for a persistent class since
 *    Hibernate has to create objects using Java Reflection.
 * 3. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 4. As a JSON bean since it is JAXB compliant.
 * 5. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library. A no-argument constructor is a required.
 * 
 * @author Val Triadi
 * @since 03/2013
 */
public class Trip implements Serializable {
    private static final long serialVersionUID = -1; 
    
    private String id;
    private String userID;
    private String name;
    private String note;
    private String location;
    private TripStatusEnum status;
    private TripPrivacyEnum privacy;
    private TripPublishEnum publish;

    //For timestamp, we are using String instead of Date because hibernate only
    //returns mm-dd-yyyy and eliminates the time hh:mm:ss after retrieving from DB:
    private String created;
    private String completed;
    private String updated;
    
    /*
     * Non-DB persistent properties (e.g., for enrichment/display/etc.)
     * Note:
     * 1. We avoid Trip to contain its User object because User contains some Trips object too. SO avoid infinite reclusive situation.
     * 2. For numOfFavorites/numOfComments, it is total number of Trip's favorites/comments
     * 3. For listOfFavoriteUsers/listOfComments, it is supposedly only the first CommonConstants.RECORDS_NUMBER_MAX
     *    so they are handily ready without look-up to DB again.
     */
    private String username;
    private String userFullname;
    private String userImageName;
    private List<Place> listOfPlaces;
    private List<Picture> listOfPictures;
    private int numOfFavorites; 
    private int numOfComments; 
    private List<User> listOfFavoriteUsers; 
    private List<Comment> listOfComments; 
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("Trip:\n");
        strBld.append("tripId [" + id + "]");
        strBld.append(", name [" + name + "]");
        strBld.append(", note [" + note + "]");
        strBld.append(", location [" + location + "]");
        
        if(status != null) {
            strBld.append(", " + status.toString());
        } else {
            strBld.append(", status [NULL]");
        }
        if(privacy != null) {
            strBld.append(", " + privacy.toString());
        } else {
            strBld.append(", privacy [NULL]");
        }
        if(publish != null) {
            strBld.append(", " + publish.toString());
        } else {
            strBld.append(", publish [NULL]");
        }
        strBld.append(", created [" + created + "]");
        strBld.append(", updated [" + updated + "]");
        strBld.append(", completed [" + completed + "]");
        strBld.append(", Number of places [" + getNumOfPlaces() + "]");
        strBld.append(", Number of pictures [" + getNumOfPictures() + "]");
        
        return strBld.toString();
    }    
    
    public Trip clone() {
        Trip tripCloned = new Trip();
        tripCloned.setId(id);
        tripCloned.setName(name);
        tripCloned.setNote(note);
        tripCloned.setLocation(location);
        tripCloned.setStatus(status);
        tripCloned.setPrivacy(privacy);
        tripCloned.setPublish(publish);
        tripCloned.setCreated(created);
        tripCloned.setUpdated(updated);
        tripCloned.setCompleted(completed);
        
        tripCloned.setUserID(userID);
        tripCloned.setUsername(username);
        tripCloned.setUserFullname(userFullname);
        tripCloned.setUserImageName(userImageName);

        tripCloned.setListOfPlaces(listOfPlaces);
        tripCloned.setListOfPictures(listOfPictures);
        return tripCloned;
    }

    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public TripStatusEnum getStatus() {
        return status;
    }
    public void setStatus(TripStatusEnum status) {
        this.status = status;
    }
    public TripPrivacyEnum getPrivacy() {
        return privacy;
    }
    public void setPrivacy(TripPrivacyEnum privacy) {
        this.privacy = privacy;
    }
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }
    public String getCompleted() {
        return completed;
    }
    public void setCompleted(String completed) {
        this.completed = completed;
    }
    public String getUpdated() {
        return updated;
    }
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    //PLACES
    public List<Place> getListOfPlaces() {
        return listOfPlaces;
    }
    public void setListOfPlaces(List<Place> listOfPlaces) {
        this.listOfPlaces = listOfPlaces;
    }
    public void addPlace(Place place) {
        if(listOfPlaces == null) {
            listOfPlaces = new ArrayList<Place>();
        }
        listOfPlaces.add(place);
    }
    public int getNumOfPlaces() {
        if(listOfPlaces != null) {
            return listOfPlaces.size();
        }
        return 0;
    }
    public void setNumOfPlaces(int numOfPlaces) {
        //Not implemented
    }

    
    //PICTURES
    public List<Picture> getListOfPictures() {
        return listOfPictures;
    }
    public void setListOfPictures(List<Picture> listOfPictures) {
        this.listOfPictures = listOfPictures;
    }
    public void addPicture(Picture picture) {
        if(listOfPictures == null) {
            listOfPictures = new ArrayList<Picture>();
        }
        listOfPictures.add(picture);
    }
    public int getNumOfPictures() {
        if(listOfPictures != null) {
            return listOfPictures.size();
        }
        return 0;
    }
    public void setNumOfPictures(int numOfPictures) {
        //Not implemented
    }
    
    public TripPublishEnum getPublish() {
        return publish;
    }

    public void setPublish(TripPublishEnum publish) {
        this.publish = publish;
    }

    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserImageName() {
        return userImageName;
    }

    public void setUserImageName(String userImageName) {
        this.userImageName = userImageName;
    }

    public int getNumOfComments() {
        return numOfComments;
    }
    public void setNumOfComments(int numOfComments) {
        this.numOfComments = numOfComments;
    }
    public List<Comment> getListOfComments() {
        return listOfComments;
    }

    public void setListOfComments(List<Comment> listOfComments) {
        this.listOfComments = listOfComments;
    }
    public void addComment(Comment comment) {
        if(listOfComments == null) {
            listOfComments = new ArrayList<Comment>();
        }
        listOfComments.add(comment);
    }

    public int getNumOfFavorites() {
        return numOfFavorites;
    }
    public void setNumOfFavorites(int numOfFavorites) {
        this.numOfFavorites = numOfFavorites;
    }
    public List<User> getListOfFavoriteUsers() {
        return listOfFavoriteUsers;
    }
    public void setListOfFavoriteUsers(List<User> listOfFavoriteUsers) {
        this.listOfFavoriteUsers = listOfFavoriteUsers;
    }
    public void addFavoriteUser(User user) {
        if(listOfFavoriteUsers == null) {
            listOfFavoriteUsers = new ArrayList<User>();
        }
        listOfFavoriteUsers.add(user);
    }
    
      
}
