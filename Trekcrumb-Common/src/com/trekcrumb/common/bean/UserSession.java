package com.trekcrumb.common.bean;

import java.io.Serializable;

/**
 * A bean to represent User's current session in the application (Web, Mobile app, etc.) after h/she
 * logs in and starts navigates through the system.
 * 
 * @author Val Triadi
 *
 */
@SuppressWarnings("serial")
public class UserSession implements Serializable {
    private User user;
    private boolean isUserAuthenticated;
    private String currentPage;
    private Trip tripSelected;
    private boolean isTripSelectedUserFavorite;

    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("User Session:\n");
        if(user != null) {
            strBld.append(user.toString());
        } else {
            strBld.append("User: [NULL]");
        }
        strBld.append("Is authenticated [" + isUserAuthenticated + "]");
        strBld.append("Current page [" + currentPage + "]");
        return strBld.toString();
    }
    
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public String getCurrentPage() {
        return currentPage;
    }
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isUserAuthenticated() {
        return isUserAuthenticated;
    }

    public void setUserAuthenticated(boolean isUserAuthenticated) {
        this.isUserAuthenticated = isUserAuthenticated;
    }

    public Trip getTripSelected() {
        return tripSelected;
    }

    public void setTripSelected(Trip tripSelected) {
        this.tripSelected = tripSelected;
    }

    public boolean isTripSelectedUserFavorite() {
        if(tripSelected == null) {
            return false;
        }
        return isTripSelectedUserFavorite;
    }

    public void setTripSelectedUserFavorite(boolean isTripSelectedUserFavorite) {
        this.isTripSelectedUserFavorite = isTripSelectedUserFavorite;
    }


    
}
