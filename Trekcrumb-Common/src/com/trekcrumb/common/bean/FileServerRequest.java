package com.trekcrumb.common.bean;

import java.io.Serializable;

import com.trekcrumb.common.enums.FileServerMenuEnum;

/**
 * Java bean for Trekcrumb FileServer Request.
 * 
 * @author Val Triadi
 * @since 04/2014
 */
@SuppressWarnings("serial")
public class FileServerRequest implements Serializable {
    
    private FileServerMenuEnum fileMenu;
    private String filename;
    private String fileURL;
    private byte[] fileBytes;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("FileServerRequest: ");
        if(fileMenu != null) {
            strBld.append(fileMenu.toString());
        } else {
            strBld.append("FileServer Menu: NULL");
        }
        strBld.append(", Filename [" + filename + "]");
        strBld.append(", File URL [" + fileURL + "]");
        return strBld.toString();
    }    
    
    public FileServerMenuEnum getFileMenu() {
        return fileMenu;
    }
    public void setFileMenu(FileServerMenuEnum fileMenu) {
        this.fileMenu = fileMenu;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public byte[] getFileBytes() {
        return fileBytes;
    }
    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }
    


    
}
