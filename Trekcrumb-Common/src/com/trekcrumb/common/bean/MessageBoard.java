package com.trekcrumb.common.bean;

import java.io.Serializable;
import java.util.Date;

import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.utility.DateUtil;

/**
 * Representation of a message board (announcement) bean.
 *
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. A Hibernate bean. A no-argument constructor is a required for a persistent class since
 *    Hibernate has to create objects using Java Reflection.
 * 3. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 4. As a JSON bean since it is JAXB compliant.
 * 5. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library. A no-argument constructor is a required.
 * 
 * @author Val Triadi
 * @since 03/2013
 */
public class MessageBoard implements Serializable {
    private static final long serialVersionUID = -1; 
     
    private String id;
    private String message;
    private Date createdDate;
    private String tripID;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }
    public String getCreatedDateAsString() {
        return DateUtil.formatDate(createdDate, DateFormatEnum.LOCALE_UI_WITHOUT_TIME);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTripID() {
        return tripID;
    }

    public void setTripID(String tripID) {
        this.tripID = tripID;
    }


     
}
