package com.trekcrumb.common.bean;

import java.io.Serializable;

import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

/**
 * Java bean for Trekcrumb WebService Request.
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 3. As a JSON bean since it is JAXB compliant.
 * 4. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library.
 * 
 * @author Val Triadi
 * @since 08/2013
 */
@SuppressWarnings("serial")
public class ServiceRequest implements Serializable {
    private ServiceTypeEnum serviceType;
    private DigitalSignature digitalSignature;
    private User user;
    private UserAuthToken userAuthToken;
    private Trip trip;
    private Place place;
    private Picture picture;
    private Favorite favorite;
    private Comment comment;
    private String whereClause;
    private OrderByEnum orderBy;
    private int offset;
    private int numOfRows;
    
    private TripSearchCriteria tripSearchCriteria;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("ServiceRequest:\n");
        strBld.append("ServiceType: " + serviceType + "\n");
        if(digitalSignature != null) {
            strBld.append(digitalSignature.toString() + "\n");
        } else {
            strBld.append("DigitalSignature: NULL \n");
        }
        if(user != null) {
            strBld.append(user.toString() + "\n");
        } else {
            strBld.append("User: NULL \n");
        }
        if(userAuthToken != null) {
            strBld.append(userAuthToken.toString() + "\n");
        } else {
            strBld.append("UserAuthToken: NULL \n");
        }
        if(trip != null) {
            strBld.append(trip.toString() + "\n");
        } else {
            strBld.append("Trip: NULL \n");
        }
        if(picture != null) {
            strBld.append(picture.toString() + "\n");
        } else {
            strBld.append("Picture: NULL \n");
        }
        strBld.append("Where-clause: " + whereClause);
        strBld.append(", Offset: " + offset);
        strBld.append(", Number of rows: " + numOfRows);

        return strBld.toString();
    }
    
    public ServiceTypeEnum getServiceType() {
        return serviceType;
    }
    public void setServiceType(ServiceTypeEnum serviceType) {
        this.serviceType = serviceType;
    }    
    
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public DigitalSignature getDigitalSignature() {
        return digitalSignature;
    }

    public void setDigitalSignature(DigitalSignature digitalSignature) {
        this.digitalSignature = digitalSignature;
    }
    public UserAuthToken getUserAuthToken() {
        return userAuthToken;
    }
    public void setUserAuthToken(UserAuthToken userAuthToken) {
        this.userAuthToken = userAuthToken;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    public TripSearchCriteria getTripSearchCriteria() {
        return tripSearchCriteria;
    }

    public void setTripSearchCriteria(TripSearchCriteria tripSearchCriteria) {
        this.tripSearchCriteria = tripSearchCriteria;
    }

    public OrderByEnum getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderByEnum orderBy) {
        this.orderBy = orderBy;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getNumOfRows() {
        return numOfRows;
    }

    public void setNumOfRows(int numOfRows) {
        this.numOfRows = numOfRows;
    }

    public Favorite getFavorite() {
        return favorite;
    }

    public void setFavorite(Favorite favorite) {
        this.favorite = favorite;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }


    
}
