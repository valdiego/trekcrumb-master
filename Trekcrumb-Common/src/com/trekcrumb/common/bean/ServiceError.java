package com.trekcrumb.common.bean;

import java.io.Serializable;

import com.trekcrumb.common.enums.ServiceErrorEnum;

/**
 * Java bean for WebService Error.
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 3. As a JSON bean since it is JAXB compliant.
 * 4. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library.
 * 
 * @author Val Triadi
 * @since 08/2013
 */
@SuppressWarnings("serial")
public class ServiceError implements Serializable {
    private ServiceErrorEnum errorEnum;
    private String errorText;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("ServiceError:\n");
        strBld.append("ErrorText: " + errorText + "\n");
        if(errorEnum != null) {
            strBld.append(errorEnum.toString());
        } else {
            strBld.append("ServiceErrorEnum: NULL");
        }
        return strBld.toString();
    }
    
    public String getErrorText() {
        return errorText;
    }
    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }
    public ServiceErrorEnum getErrorEnum() {
        return errorEnum;
    }
    public void setErrorEnum(ServiceErrorEnum errorEnum) {
        this.errorEnum = errorEnum;
    }
    
}
