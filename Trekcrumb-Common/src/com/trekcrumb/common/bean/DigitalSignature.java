package com.trekcrumb.common.bean;

/**
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 3. As a JSON bean since it is JAXB compliant.
 * 4. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library.
 * 
 * @author Val Triadi
 * @since 08/2013
 */
public class DigitalSignature {
    private String signedMessage;
    private String signature;
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("DigitalSignature:\n");
        strBld.append("signedMessage [" + signedMessage + "], ");
        strBld.append("signature [" + signature + "]");
        return strBld.toString();
    }    
    
    public String getSignedMessage() {
        return signedMessage;
    }
    public void setSignedMessage(String signedMessage) {
        this.signedMessage = signedMessage;
    }
    public String getSignature() {
        return signature;
    }
    public void setSignature(String signature) {
        this.signature = signature;
    }

}
