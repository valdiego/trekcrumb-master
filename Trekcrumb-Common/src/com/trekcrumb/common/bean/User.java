package com.trekcrumb.common.bean;

import java.io.Serializable;
import java.util.List;

import com.trekcrumb.common.enums.YesNoEnum;

/**
 * Java bean for User.
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. A Hibernate bean. A no-argument constructor is a required for a persistent class since
 *    Hibernate has to create objects using Java Reflection.
 * 3. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 4. As a JSON bean since it is JAXB compliant.
 * 5. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library. A no-argument constructor is a required.
 * 
 * @author Val Triadi
 * @since 03/2013
 */
@SuppressWarnings("serial")
public class User implements Serializable {
    //DB persistent properties
    private String userId;
    private String username;
    private String email;
    private String password;
    private String fullname;
    private String location;
    private String summary;
    private String securityToken;
    private String profileImageName;
    private String profileImageURL;
    private YesNoEnum active;
    private YesNoEnum confirmed;
    
    /*
     * Timestamp:
     * For timestamp, we are using String instead of Date because hibernate only
     * returns mm-dd-yyyy and eliminates the time hh:mm:ss after retrieving from DB
     */
    private String created;
    private String updated;
    
    //Non-DB persistent properties (e.g., for enrichment, display, etc.)
    private String passwordNew;
    private byte[] profileImageBytes;
    private String profileImageBase64Data;
    private int numOfTotalTrips;
    private int numOfTotalPictures;
    private int numOfFavorites;
    private int numOfComments;
    private Trip lastCreatedTrip;
    private Trip lastUpdatedTrip;
    
    //UserAuthToken: Place holder for the bean to create a new one/return the newly created one
    private UserAuthToken userAuthToken;
    
    //List of UserAuthTokens: Place holder for all of User's existing UserAuthTokens (as part of enrichment)
    private List<UserAuthToken> listOfAuthTokens;
    
    public User clone() {
        User clonedUser = new User();
        clonedUser.setUserId(userId);
        clonedUser.setUsername(username);
        clonedUser.setEmail(email);
        clonedUser.setPassword(password);
        clonedUser.setFullname(fullname);
        clonedUser.setSummary(summary);
        clonedUser.setLocation(location);
        clonedUser.setProfileImageURL(profileImageURL);
        clonedUser.setProfileImageName(profileImageName);
        clonedUser.setProfileImageBytes(profileImageBytes);
        clonedUser.setActive(active);
        clonedUser.setConfirmed(confirmed);
        clonedUser.setCreated(created);
        clonedUser.setUpdated(updated);
        clonedUser.setSecurityToken(securityToken);
        
        //Enrichment data is NOT included!
        return clonedUser;
    }
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("User:\n");
        strBld.append("userId [protected]");
        strBld.append(", username [" + username + "]");
        strBld.append(", email [" + email + "]");
        strBld.append(", password [protected]");
        strBld.append(", fullname [" + fullname + "]");
        strBld.append(", location [" + location + "]");
        strBld.append(", summary [" + summary + "]");
        strBld.append(", profile image name [" + profileImageName + "]");
        strBld.append(", profile image URL [" + profileImageURL + "]");
        if(active != null) {
            strBld.append(", active [" + active.toString() + "]");
        } else {
            strBld.append(", active [null]");
        }
        if(confirmed != null) {
            strBld.append(", confirmed [" + confirmed.toString() + "]");
        } else {
            strBld.append(", confirmed [null]");
        }
        strBld.append(", created [" + created + "]");
        strBld.append(", updated [" + updated + "]");
        
        //Enrichment data:
        strBld.append(", number of total published Trips [" + numOfTotalTrips + "]");
        strBld.append(", number of total favorites [" + numOfFavorites + "]");
        strBld.append(", number of total comments [" + numOfComments + "]");
        strBld.append(", number of total pictures [" + numOfTotalPictures + "]");
        return strBld.toString();
    }
    
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public YesNoEnum getActive() {
        return active;
    }
    public void setActive(YesNoEnum active) {
        this.active = active;
    }
    
    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public YesNoEnum getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(YesNoEnum confirmed) {
        this.confirmed = confirmed;
    }

    public String getFullname() {
        return fullname;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getNumOfTotalTrips() {
        return numOfTotalTrips;
    }

    public void setNumOfTotalTrips(int numOfTrips) {
        this.numOfTotalTrips = numOfTrips;
    }

    public Trip getLastUpdatedTrip() {
        return lastUpdatedTrip;
    }

    public void setLastUpdatedTrip(Trip lastUpdatedTrip) {
        this.lastUpdatedTrip = lastUpdatedTrip;
    }

    public Trip getLastCreatedTrip() {
        return lastCreatedTrip;
    }

    public void setLastCreatedTrip(Trip lastCreatedTrip) {
        this.lastCreatedTrip = lastCreatedTrip;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public String getProfileImageName() {
        return profileImageName;
    }

    public void setProfileImageName(String filename) {
        this.profileImageName = filename;
    }

    public byte[] getProfileImageBytes() {
        return profileImageBytes;
    }

    public void setProfileImageBytes(byte[] profileImageBytes) {
        this.profileImageBytes = profileImageBytes;
    }

    public String getProfileImageBase64Data() {
        return profileImageBase64Data;
    }

    public void setProfileImageBase64Data(String profileImageBase64Data) {
        this.profileImageBase64Data = profileImageBase64Data;
    }

    public int getNumOfTotalPictures() {
        return numOfTotalPictures;
    }

    public void setNumOfTotalPictures(int numOfTotalPictures) {
        this.numOfTotalPictures = numOfTotalPictures;
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    public String getPasswordNew() {
        return passwordNew;
    }

    public void setPasswordNew(String passwordNew) {
        this.passwordNew = passwordNew;
    }

    public int getNumOfFavorites() {
        return numOfFavorites;
    }

    public void setNumOfFavorites(int numOfFavorites) {
        this.numOfFavorites = numOfFavorites;
    }

    public int getNumOfComments() {
        return numOfComments;
    }

    public void setNumOfComments(int numOfComments) {
        this.numOfComments = numOfComments;
    }

    public UserAuthToken getUserAuthToken() {
        return userAuthToken;
    }

    public void setUserAuthToken(UserAuthToken userAuthToken) {
        this.userAuthToken = userAuthToken;
    }

    public List<UserAuthToken> getListOfAuthTokens() {
        return listOfAuthTokens;
    }

    public void setListOfAuthTokens(List<UserAuthToken> listOfAuthTokens) {
        this.listOfAuthTokens = listOfAuthTokens;
    }


    
}
