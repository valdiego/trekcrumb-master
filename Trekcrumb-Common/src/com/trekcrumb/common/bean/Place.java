package com.trekcrumb.common.bean;

import java.io.Serializable;

/**
 * Representation of a place (aka, the 'crumb') bean.
 * 
 * NOTE:
 * This is an enhanced bean, serving multiple purposes:
 * 1. As regular POJO bean (with getters/setters) to pass around components and layers
 * 2. A Hibernate bean. A no-argument constructor is a required for a persistent class since
 *    Hibernate has to create objects using Java Reflection.
 * 3. As JAXB bean for WebServices consumsion (Apache CFX) where it can be conversed into an XML element 
 *    (declared with XmlRootElement) where the classname is the root element name, and all fields 
 *    automatically its XML field.
 * 4. As a JSON bean since it is JAXB compliant.
 * 5. As a Jackson JSON bean where the requirement is every variable must have set/get method, and 
 *    every set/get method must be a pair (unless they are marked by @JsonIgnoreProperties(ignoreUnknown = true),
 *    which requires jackon library. A no-argument constructor is a required.
 * 
 * @author Val Triadi
 * @since 03/2013
 */

public class Place implements Serializable {
    private static final long serialVersionUID = -1; 
     
    private String id;
    private String userID;
    private String tripID;
    private double latitude;
    private double longitude;
    private String location;
    private String note;
    private String created;
    private String updated;

    //Non-DB persistent properties (e.g., for enrichment, display, etc.)
    private Picture coverPicture;

    public Place() {
        //Since the class has its custom constructor Place(double, double), we must provide a
        //default constructor (see NOTE above). 
    }
    
    public Place(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    @Override
    public String toString() {
        StringBuilder strBld = new StringBuilder();
        strBld.append("Place:\n");
        strBld.append("Id [" + id + "]");
        strBld.append(", UserId [" + userID + "]");
        strBld.append(", TripId [" + tripID + "]");
        strBld.append(", note [" + note + "]");
        strBld.append(", latitude [" + latitude + "]");
        strBld.append(", longitude [" + longitude + "]");
        strBld.append(", created [" + created + "]");
        strBld.append(", updated [" + updated + "]");
        return strBld.toString();
    }
    
    public Place clone() {
        Place placeCloned = new Place();
        placeCloned.setId(id);
        placeCloned.setUserID(userID);
        placeCloned.setTripID(tripID);
        placeCloned.setLatitude(latitude);
        placeCloned.setLongitude(longitude);
        placeCloned.setLocation(location);
        placeCloned.setNote(note);
        placeCloned.setCreated(created);
        placeCloned.setUpdated(updated);
        return placeCloned;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    
    public String getLocation() {
        if(location == null || location.trim() == "") {
            StringBuilder locNameDefault = new StringBuilder();
            locNameDefault.append("[");
            locNameDefault.append(latitude);
            locNameDefault.append(",");
            locNameDefault.append(longitude);
            locNameDefault.append("]");
            location = locNameDefault.toString();
        }
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    
    public String getNote() {
        return note;
    }
    public void setNote(String note) {
        this.note = note;
    }
    public String getUserID() {
        return userID;
    }
    public void setUserID(String userID) {
        this.userID = userID;
    }
    public String getTripID() {
        return tripID;
    }
    public void setTripID(String tripID) {
        this.tripID = tripID;
    }
    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }
    public Picture getCoverPicture() {
        return coverPicture;
    }
    public void setCoverPicture(Picture coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }



}
