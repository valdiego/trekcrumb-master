package com.trekcrumb.common.utility;

import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.ServiceError;
import com.trekcrumb.common.bean.ServiceResponse;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public class CommonUtil {

    public static ServiceResponse composeServiceResponseSuccess(ServiceTypeEnum serviceType) {
        ServiceResponse response = new ServiceResponse();
        response.setSuccess(true);
        response.setServiceType(serviceType);
        response.setServiceError(null);
        return response;
    }

    public static ServiceResponse composeServiceResponseError(
            ServiceTypeEnum serviceType,
            ServiceErrorEnum errorEnum,
            String errorText) {
        ServiceResponse response = new ServiceResponse();
        response.setSuccess(false);
        response.setServiceType(serviceType);
        response.setServiceError(composeServiceError(errorEnum, errorText));
        return response;
    }
    
    public static FileServerResponse composeFSResponseSuccess(FileServerMenuEnum fileMenu) {
        FileServerResponse response = new FileServerResponse();
        response.setSuccess(true);
        response.setFileMenu(fileMenu);
        return response;
    }

    public static FileServerResponse composeFSResponseError(
            FileServerMenuEnum fileMenu,
            ServiceErrorEnum errorEnum,
            String errorText) {
        FileServerResponse response = new FileServerResponse();
        response.setSuccess(false);
        response.setFileMenu(fileMenu);
        response.setServiceError(composeServiceError(errorEnum, errorText));
        return response;
    }
    
    private static ServiceError composeServiceError(ServiceErrorEnum errorEnum, String errorText) {
        ServiceError error = new ServiceError();
        if(errorEnum != null) {
            error.setErrorEnum(errorEnum);
        } else {
            error.setErrorEnum(ServiceErrorEnum.SYSTEM_UNEXPECTED_ERROR);
        }
        
        if(!ValidationUtil.isEmpty(errorText)) {
            error.setErrorText(errorText);
        } else {
            error.setErrorText("Unexpected system failure!");
        }
        
        return error;
    }
    
    public static String generateID(String prefix) {
        StringBuilder idBuilder = new StringBuilder();
        if(!ValidationUtil.isEmpty(prefix)) {
            idBuilder.append(prefix);
        }
        idBuilder.append(String.valueOf(System.currentTimeMillis()));
        
        for(int i = 0; i < 2; i++ ) { 
            idBuilder.append(CommonConstants.ID_RANDOM_RANGE_ALPHANUM.charAt(
                    CommonConstants.ID_RANDOM_GENERATOR.nextInt(CommonConstants.ID_RANDOM_RANGE_ALPHANUM_SIZE)));
        }
        
        idBuilder.append(String.valueOf(CommonConstants.ID_RANDOM_GENERATOR.nextInt(CommonConstants.ID_RANDOM_RANGE_INTEGER)));
        return idBuilder.toString();
    }
    
    public static String generatePictureFilename(String prefix) {
        StringBuilder imgFilenameBuilder = new StringBuilder();
        imgFilenameBuilder.append(generateID(prefix));
        imgFilenameBuilder.append(CommonConstants.PICTURE_FILENAME_EXT);
        return imgFilenameBuilder.toString();
    }
    
    public static String generateSecurityToken() {
        StringBuilder idBuilder = new StringBuilder(CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SECURITY_TOKEN);
        for(int i = 0; i < CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SECURITY_TOKEN; i++ ) { 
            idBuilder.append(CommonConstants.ID_RANDOM_RANGE_ALPHANUM.charAt(
                    CommonConstants.ID_RANDOM_GENERATOR.nextInt(CommonConstants.ID_RANDOM_RANGE_ALPHANUM_SIZE)));
        }        
        return idBuilder.toString();
    }
    
}
