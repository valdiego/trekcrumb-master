package com.trekcrumb.common.utility;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import com.trekcrumb.common.exception.ImageFormatNotSupportedException;

public class FileUtil {
    
    /**
     * Writes (saves) an image (byte array) into a file given a target LOCAL location.
     * 'Local' means the directory is in the same host machine as the JVM running this code. 
     * This method does not support writing to remote location.
     * 
     * @param directory - The absolute (complete) target file directory
     * @param filename -  The name of new file.          
     * @param imageBytes - bytes array of the image to be saved.
     * @return The image URL (absolute location).
     */
    public static String writeImageToFileDirectory(
            String directory,
            String filename,
            byte[] imageBytes) throws Exception {
        validateImageFormat(null, imageBytes);
        
        String imgURL = null;
        File imgFileToWrite = null;
        try {
            imgFileToWrite = new File(directory, filename);
            BufferedImage imageBuffer = ImageIO.read(new ByteArrayInputStream(imageBytes));
            boolean isSuccess = ImageIO.write(imageBuffer, CommonConstants.PICTURE_DEFAULT_FORMAT, imgFileToWrite);
            if(!isSuccess) {
                System.err.println("FileUtil.writeImageToFileDirectory() - ERROR: Saving image bytes to a File has failed because ImageIO.write() returned false!]");
                return null;
            }
            imgURL = imgFileToWrite.getAbsolutePath();
        } finally {
            imgFileToWrite = null;
        }
        return imgURL;
    }
    
    /**
     * Reads an image file from a directory given its location.
     * Local means the directory is in the same host machine as the JVM running this code. 
     * This method does not support remote location.
     * 
     * @param filepath - The absolute file directory (complete, including root dir and filename) of the file to read.
     * @return bytes array of the image being read.
     */
    public static byte[] readImageFromFileDirectory(String filepath) throws Exception {
        byte[] imgBytes = null;
        File imgFileToRead = null;
        BufferedImage buffImg = null;
        ByteArrayOutputStream baos = null;
        try {
            imgFileToRead = new File(filepath);
            buffImg = ImageIO.read(imgFileToRead);
            if(buffImg == null) {
                System.err.println("FileUtil.readImageFileToBytes() - ERROR: Image file does NOT exist given path [" + filepath + "]");
                return imgBytes;
            }
            
            //Parse into bytes:
            baos = new ByteArrayOutputStream(1000);
            ImageIO.write(buffImg, CommonConstants.PICTURE_DEFAULT_FORMAT, baos);
            baos.flush();
            imgBytes = baos.toByteArray();
        } catch(IIOException iioe) {
            if(iioe.getMessage() != null 
                    && iioe.getMessage().contains("Can't read input file")) {
                System.err.println("FileUtil.readImageFileToBytes() - ERROR: Image file does NOT exist given path [" + filepath + "]");
                iioe.printStackTrace();
                return null;
            } else {
                throw iioe;
            }
            
        } finally {
            if(baos != null) {
                try {
                    baos.close();
                } catch(Exception e) {} //Cant do anything
            }
            buffImg = null;
            imgFileToRead = null;
        }
        return imgBytes;
    }
    
    /**
     * Delete a file.
     * @param filepath - The absolute file directory (complete, including root dir and filename) of the file to delete.
     */
    public static void deleteFile(String filepath) {
        File fileToDelete = new File(filepath);
        if(!fileToDelete.exists()) {
            System.out.println("FileUtil.deleteFile() - File to delete [" + filepath + "] does NOT exist. Quitting.");
            return;
        }
        
        boolean result = fileToDelete.delete();
        System.out.println("FileUtil.deleteFile() - Delete file [" + filepath + "] success: " + result);
        if(!result) {
            //Fail, try the hard way:
            String deleteCmd = "rm -r " + filepath;
            Runtime runtime = Runtime.getRuntime();
            try {
                runtime.exec(deleteCmd);
            } catch (IOException e) { 
                //Do nothing else
                e.printStackTrace();
            }
            System.out.println("FileUtil.deleteFile() - Re-try delete file [" + filepath + "] success: " + fileToDelete.exists());
        }
    }    
    
    /*
     * TODO (8/2016): Image format not supported
     * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
     * with error: "Invalid argument to native writeImage". The issue is JRE being used by
     * the server if it is  OpenJDK, it has this bug. Solution is to either change the JRE or use
     * GIF plug-in, including to support GIF animation.
     */
    private static void validateImageFormat(
            File imgFile,
            byte[] imgBytes) throws Exception {
        String imgFormat = null;
        ImageInputStream iis = null;
        ImageReader imageReader = null;
        
        if(imgFile != null) {
            iis = ImageIO.createImageInputStream(imgFile);
        } else if(imgBytes != null) {
            iis = ImageIO.createImageInputStream(new ByteArrayInputStream(imgBytes));
        } else {
            throw new IllegalArgumentException("Either image filename or byte array is required!");
        }

        Iterator<ImageReader> imgReaderIterator = ImageIO.getImageReaders(iis);
        try {
            while (imgReaderIterator.hasNext()) {
                imageReader = imgReaderIterator.next();
                if(imageReader.getFormatName() != null) {
                    imgFormat = imageReader.getFormatName();
                    break;
                }
            }
        } finally {
            if(imageReader != null) {
                imageReader.dispose();
                imageReader = null;
            }
            
            imgReaderIterator = null;
            
            if(iis != null) {
                iis.close();
                iis = null;
            }
        }
        
        if(imgFormat == null || imgFormat.trim().equalsIgnoreCase("gif")) {
            throw new ImageFormatNotSupportedException("Image format to create is not supported!");
        }
    }

}
