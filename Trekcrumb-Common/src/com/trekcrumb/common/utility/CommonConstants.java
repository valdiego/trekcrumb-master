package com.trekcrumb.common.utility;

import java.util.Random;
import java.util.regex.Pattern;

public class CommonConstants {
    public static final int PROGRESS_WAIT_TIME_60_SECONDS = 60000; //milliseconds
    public static final int PROGRESS_WAIT_TIME_30_SECONDS = 30000; //milliseconds
    public static final int RECORDS_NUMBER_MAX = 20;
    //public static final int RECORDS_NUMBER_MAX = 4;
    public final static Random ID_RANDOM_GENERATOR = new Random();
    public final static int ID_RANDOM_RANGE_INTEGER = 10000;
    public final static String ID_RANDOM_RANGE_ALPHANUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public final static int ID_RANDOM_RANGE_ALPHANUM_SIZE = 36;
    
    //Data String Values
    public static final String STRING_VALUE_EMPTY = "";
    public static final int STRING_VALUE_LENGTH_MAX_USER_USERNAME = 25;
    public static final int STRING_VALUE_LENGTH_MAX_USER_PASSWORD = 25;
    public static final int STRING_VALUE_LENGTH_MAX_USER_EMAIL = 100;
    public static final int STRING_VALUE_LENGTH_MAX_USER_FULLNAME = 50;
    public static final int STRING_VALUE_LENGTH_MAX_USER_LOCATION = 100;
    public static final int STRING_VALUE_LENGTH_MAX_USER_SUMMARY = 200;
    public static final int STRING_VALUE_LENGTH_MAX_USER_SECURITY_TOKEN = 20;
    public static final int STRING_VALUE_LENGTH_MAX_TRIP_NAME = 100;
    public static final int STRING_VALUE_LENGTH_MAX_TRIP_LOCATION = 400;
    public static final int STRING_VALUE_LENGTH_MAX_TRIP_NOTE = 1000;
    public static final int STRING_VALUE_LENGTH_MAX_PLACE_LOCATION = 400;
    public static final int STRING_VALUE_LENGTH_MAX_PLACE_NOTE = 200;
    public static final int STRING_VALUE_LENGTH_MAX_PICTURE_NOTE = 200;
    public static final int STRING_VALUE_LENGTH_MAX_COMMENT_NOTE = 200;
    public final static Pattern STRING_VALUE_LETTERSNUMBERS_INVALID_PATTERN = Pattern.compile("[^a-zA-Z0-9]");
    public final static Pattern STRING_VALUE_EMAIL_INVALID_PATTERN = Pattern.compile("[^a-zA-Z0-9\\@\\.\\_\\-]");

    //Date formats:
    //1. Date format how timestamp (date) is stored in mysql, and used when it is pulled 
    //   out of database for API to understand it. By default, dates are stored with  GMT timezone. 
    //   The following format is ISO tandard with GMT timezone
    //public static final String DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL = "'%Y-%m-%d %H:%i:%S GMT'";
    public static final String DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL = "'%Y-%m-%dT%H:%i:%S+0000'";
    
    //2. Date format (default) as it passed around beans and components
    //public static final String DATE_FORMAT_GMT_STANDARD_LONG = "yyyy-MM-dd HH:mm:ss zzz";
    public static final String DATE_FORMAT_GMT_STANDARD_LONG = "yyyy-MM-dd'T'HH:mm:ssZ";
    
    //3. Date formats (locale) as it passed around beans and components
    public static final String DATE_FORMAT_LOCALE_STANDARD_LONG = "yyyy-MM-dd'T'HH:mm:ss zzz";
    
    //4. Date formats for UI display
    //public static final String DATE_FORMAT_LOCALE_UI_WITH_TIME = "EEEE, d MMM yyyy 'at' HH:mm zzz";
    //public static final String DATE_FORMAT_LOCALE_UI_WITHOUT_TIME = "EEEE, d MMM yyyy zzz";
    //public static final String DATE_FORMAT_LOCALE_UI_WITH_TIME = "HH:mm a d MMM yyyy";
    //public static final String DATE_FORMAT_LOCALE_UI_WITH_TIME = "h:mm a d MMM yyyy";
    public static final String DATE_FORMAT_LOCALE_UI_WITH_TIME = "d MMM yyyy 'at' h:mm a";
    public static final String DATE_FORMAT_LOCALE_UI_WITHOUT_TIME = "d MMM yyyy";
    
    //Pictures:
    /* NOTE:
     * 1. Using image format 'PNG' caused the result significantly bigger than the original file.
     *    Therefore, by default we are forcing the JPEG format for now.
     * 2. Picture maximum sizes are for their dimensions in pixels. The actual size in bytes
     *    is: width x height (pixels) x number of bytes per pixel. Various image formats consume
     *    differently. Example: ARGB is 4 bytes per pixel, while RGB is 2 bytes per pixel.
     * 3. Prior to 10/2014, we save/view images with max size = 1024 px. But this became too
     *    big for device with limited heap memory (e.g., Android emulator 16MB) causing OOM error. 
     *    Therefore, post 10/2014 we reduced the max size to 512 px.
     */
    public static final int PICTURE_MAX_SIZE_PIXEL_SAVE = 512;
    public static final int PICTURE_PROFILE_MAX_SIZE_PIXEL_SAVE = 250;
    public static final String PICTURE_FILENAME_PREFIX = "Trekcrumb_";
    public static final String PICTURE_FILENAME_SUFFIX_DETAIL = "_Detail";
    public static final String PICTURE_FILENAME_SUFFIX_THUMBNAIL = "_Thumbnail";
    //public static final String PICTURE_DEFAULT_FORMAT = "PNG";
    //public static final String PICTURE_FILENAME_EXT = ".PNG";
    public static final String PICTURE_DEFAULT_FORMAT = "JPEG";
    public static final String PICTURE_FILENAME_EXT = ".JPG";
    public static final String PICTURE_GENERIC_DESCRIPTION = "Trekcrumb Picture";
    
    //NETWORK
    public static final int NETWORK_TIMEOUT_CONNECTION = Integer.parseInt(CommonProperties.getProperty("network.timeout.connection"));
    public static final int NETWORK_TIMEOUT_READ = Integer.parseInt(CommonProperties.getProperty("network.timeout.read"));

    //WEB SERVICE:
    public static final String WS_HOST = CommonProperties.getProperty("webservice.host");

    //FILE SERVER:
    public static final String FS_SERVER_TYPE = CommonProperties.getProperty("fileserver.type");
    public static final String FS_SERVER_TYPE_SERVERSOCKET = "SERVERSOCKET";
    public static final String FS_SERVER_TYPE_WEBSERVICE = "WEBSERVICE";
    public static final String FS_SERVER_TYPE_GOOGLECLOUDSTORAGE = "GOOGLE_CLOUDSTORAGE";
    public static final String FS_SOCKET_TO_LISTEN_HOST = CommonProperties.getProperty("fileserver.socket.serverToListen.host");
    public static final int    FS_SOCKET_TO_LISTEN_PORT = Integer.parseInt(CommonProperties.getProperty("fileserver.socket.serverToListen.port"));
    public static final String FS_SOCKET_CLIENT_TO_CONNECT_HOST = CommonProperties.getProperty("fileserver.socket.clientToConnect.host");
    public static final int    FS_SOCKET_CLIENT_TO_CONNECT_PORT = Integer.parseInt(CommonProperties.getProperty("fileserver.socket.clientToConnect.port"));
    public static final String FS_WEBSERVICE_HOST = CommonProperties.getProperty("fileserver.webservice.host");
    public static final String FS_SERVER_SERVLET_TYPE = CommonProperties.getProperty("fileserver.servlet.type");
    public static final String FS_SERVER_SERVLET_TYPE_TREKCRUMBFS = "TREKCRUMBFS";
    public static final String FS_SERVER_SERVLET_TYPE_GOOGLECLOUDSTORAGE = "GOOGLE_CLOUDSTORAGE";
    public static final String FS_SERVLET_URL_GETIMAGE_PICTURE = CommonProperties.getProperty("fileserver.servlet.url.getImage.picture");
    public static final String FS_SERVLET_URL_GETIMAGE_PROFILE = CommonProperties.getProperty("fileserver.servlet.url.getImage.profile");
    public static final String FS_SERVLET_HOST = CommonProperties.getProperty("fileserver.servlet.host");
    public static final String FS_SERVLET_URI_PARAM_IMAGE_TYPE = "imageType";
    public static final String FS_SERVLET_URI_PARAM_IMAGE_TYPE_PROFILE = "profile";
    public static final String FS_SERVLET_URI_PARAM_IMAGE_TYPE_PICTURE = "picture";
    public static final String FS_SERVLET_URI_PARAM_IMAGE_NAME = "imageName";
    
    public static final int    FS_THREAD_MAX = Integer.parseInt(CommonProperties.getProperty("fileserver.threadMax"));
    public static final String FS_PICTURE_TREK_DIRECTORY = CommonProperties.getProperty("fileserver.picture.dir");
    public static final String FS_PICTURE_PROFILE_DIRECTORY = CommonProperties.getProperty("fileserver.profile.dir");
    
    //KEYS FOR OBJECTS IN SESSION (WEBAPP) / BUNDLE (ANDROID)
    public static final String KEY_SESSIONOBJ_SERVICE_RESPONSE = "com.trekcrumb.KEY_SESSIONOBJ_SERVICE_RESPONSE";
    public final static String KEY_SESSIONOBJ_MESSAGE_ERROR = "KEY_SESSIONOBJ_MESSAGE_ERROR";
    public final static String KEY_SESSIONOBJ_MESSAGE_INFO = "KEY_SESSIONOBJ_MESSAGE_INFO";
    public static final String KEY_SESSIONOBJ_HOME_SKIP_INIT = "KEY_SESSIONOBJ_HOME_SKIP_INIT";
    public final static String KEY_SESSIONOBJ_INDEX_SELECTED = "com.trekcrumb.KEY_SESSIONOBJ_INDEX_SELECTED";
    public static final String KEY_SESSIONOBJ_IS_TRUE_OR_FALSE = "KEY_SESSIONOBJ_TRUE_OR_FALSE";
    public static final String KEY_SESSIONOBJ_IS_SHOW_DETAIL = "KEY_SESSIONOBJ_IS_SHOW_DETAIL";
    public static final String KEY_SESSIONOBJ_IS_AFTER_DELETE = "KEY_SESSIONOBJ_IS_AFTER_DELETE";
    public static final String KEY_SESSIONOBJ_LIST_CURRENT = "KEY_SESSIONOBJ_LIST_CURRENT";
    public static final String KEY_SESSIONOBJ_PAGINATION_IS_SHOW = "com.trekcrumb.KEY_SESSIONOBJ_PAGINATION_IS_SHOW";
    public static final String KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET = "com.trekcrumb.KEY_SESSIONOBJ_PAGINATION_SEARCH_OFFSET";
    public static final String KEY_SESSIONOBJ_PAGINATION_PAGE_CURRENT = "com.trekcrumb.KEY_SESSIONOBJ_PAGINATION_PAGE_CURRENT";
    public static final String KEY_SESSIONOBJ_PAGINATION_PAGES_TOTAL = "com.trekcrumb.KEY_SESSIONOBJ_PAGINATION_PAGES_TOTAL";
    public static final String KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL = "com.trekcrumb.KEY_SESSIONOBJ_PAGINATION_RECORDS_TOTAL";
    public final static String KEY_SESSIONOBJ_USERSESSION = "com.trekcrumb.KEY_SESSIONOBJ_USERSESSION";
    public final static String KEY_SESSIONOBJ_USER_USERID = "com.trekcrumb.KEY_SESSIONOBJ_USER_USERID";
    public final static String KEY_SESSIONOBJ_USER_USERNAME = "com.trekcrumb.KEY_SESSIONOBJ_USER_USERNAME";
    public final static String KEY_SESSIONOBJ_USER_FULLNAME = "com.trekcrumb.KEY_SESSIONOBJ_USER_FULLNAME";
    public final static String KEY_SESSIONOBJ_USER_DEACTIVATED = "com.trekcrumb.KEY_SESSIONOBJ_USER_DEACTIVATED";
    public final static String KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE = "com.trekcrumb.KEY_SESSIONOBJ_USER_IS_LOGIN_PROFILE";
    public final static String KEY_SESSIONOBJ_USER_SELECTED = "com.trekcrumb.KEY_SESSIONOBJ_USER_SELECTED";
    public final static String KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_SUMMARY = "com.trekcrumb.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_SUMMARY";
    public final static String KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS = "com.trekcrumb.KEY_SESSIONOBJ_TRIPS_LIST_IS_SHOW_STATUS";
    public final static String KEY_SESSIONOBJ_TRIPS_LIST_TYPE = "com.trekcrumb.KEY_BUNDLE_TRIPS_LIST_TYPE";
    public final static String KEY_SESSIONOBJ_TRIPS_LIST_TYPE_PUBLISHED = "com.trekcrumb.KEY_BUNDLE_TRIPS_LIST_TYPE_PUBLISHED";
    public final static String KEY_SESSIONOBJ_TRIPS_LIST_TYPE_UNPUBLISHED = "com.trekcrumb.KEY_BUNDLE_TRIPS_LIST_TYPE_UNPUBLISHED";
    public final static String KEY_SESSIONOBJ_TRIPS_LIST_TYPE_FAVORITES = "com.trekcrumb.KEY_BUNDLE_TRIPS_LIST_TYPE_FAVORITES";
    public final static String KEY_SESSIONOBJ_TRIP_SELECTED = "com.trekcrumb.KEY_SESSIONOBJ_TRIP_SELECTED";
    public final static String KEY_SESSIONOBJ_TRIP_SELECTED_ID = "com.trekcrumb.KEY_SESSIONOBJ_TRIP_SELECTED_ID";
    public final static String KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED = "com.trekcrumb.KEY_SESSIONOBJ_TRIP_IS_USER_LOGIN_OWNED";
    public final static String KEY_SESSIONOBJ_TRIP_PUBLISH_STATUS = "com.trekcrumb.KEY_SESSIONOBJ_TRIP_PUBLISH_STATUS";
    public final static String KEY_SESSIONOBJ_TRIP_SEARCH_CRITERIA = "com.trekcrumb.KEY_SESSIONOBJ_TRIP_SEARCH_CRITERIA";
    public final static String KEY_SESSIONOBJ_PLACE_SELECTED = "com.trekcrumb.KEY_SESSIONOBJ_PLACE_SELECTED";
    public final static String KEY_SESSIONOBJ_PICTURE_SELECTED = "com.trekcrumb.KEY_SESSIONOBJ_PICTURE_SELECTED";
    public final static String KEY_SESSIONOBJ_PICTURE_SELECTED_URL = "com.trekcrumb.KEY_SESSIONOBJ_PICTURE_SELECTED_URL";
    public final static String KEY_SESSIONOBJ_PICTURE_SELECTED_BLOB = "com.trekcrumb.KEY_SESSIONOBJ_PICTURE_SELECTED_BLOB";
    public final static String KEY_SESSIONOBJ_PICTURE_IS_BY_CAMERA = "KEY_SESSIONOBJ_PICTURE_IS_BY_CAMERA";
    public final static String KEY_SESSIONOBJ_LOCATION_IS_GPS_SEARCHING = "com.trekcrumb.KEY_SESSIONOBJ_LOCATION_IS_GPS_SEARCHING";
    public final static String KEY_SESSIONOBJ_LOCATION_CURRENT = "com.trekcrumb.KEY_SESSIONOBJ_LOCATION_CURRENT";
    public final static String KEY_SESSIONOBJ_LOCATION_CURRENT_STRING = "com.trekcrumb.KEY_SESSIONOBJ_LOCATION_CURRENT_STRING";
    public final static String KEY_SESSIONOBJ_SUPPORT_TYPE = "com.trekcrumb.KEY_SESSIONOBJ_SUPPORT_TYPE";
    public final static String KEY_SESSIONOBJ_SUPPORT_TYPE_TERMS = "com.trekcrumb.KEY_SESSIONOBJ_SUPPORT_TYPE_TERMS";
    public final static String KEY_SESSIONOBJ_SUPPORT_TYPE_PRIVACY = "com.trekcrumb.KEY_SESSIONOBJ_SUPPORT_TYPE_PRIVACY";
    public final static String KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ = "com.trekcrumb.KEY_SESSIONOBJ_SUPPORT_TYPE_FAQ";
    public final static String KEY_SESSIONOBJ_SUPPORT_TYPE_CONTACTUS = "com.trekcrumb.KEY_SESSIONOBJ_SUPPORT_TYPE_CONTACTUS";
    public final static String KEY_SESSIONOBJ_SUPPORT_CONTENT_TERMS = "KEY_SESSIONOBJ_SUPPORT_CONTENT_TERMS";
    public final static String KEY_SESSIONOBJ_SUPPORT_CONTENT_PRIVACY = "KEY_SESSIONOBJ_SUPPORT_CONTENT_PRIVACY";
    public final static String KEY_SESSIONOBJ_SUPPORT_CONTENT_FAQ = "KEY_SESSIONOBJ_SUPPORT_CONTENT_FAQ";
    

    
    
    
}
