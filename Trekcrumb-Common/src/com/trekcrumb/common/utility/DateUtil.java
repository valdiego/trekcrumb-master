package com.trekcrumb.common.utility;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.trekcrumb.common.enums.DateFormatEnum;

public class DateUtil {

    /**
     * @return Returns current time as a String with the GMT timezone, and with a format
     *         to follow DateFormatEnum.GMT_STANDARD_LONG
     */
    public static String getCurrentTimeWithGMTTimezone() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        return formatDate(calendar.getTime(), DateFormatEnum.GMT_STANDARD_LONG);
    }
    
    /**
     * @return Returns current time as a String with the device/machine locale timezone, and
     *         with a format to follow DateFormatEnum.LOCALE_STANDARD_LONG
     */
    public static String getCurrentTimeWithLocaleTimezone() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        return formatDate(calendar.getTime(), DateFormatEnum.LOCALE_STANDARD_LONG);
    }
    
    /**
     * Formats a date from Date-type into String-type with specified target formatter to follow.
     * @return A formatted date as String.
     */
    public static String formatDate(
            Date date, 
            DateFormatEnum dateFormatEnumTarget) {
        DateFormat formatter = dateFormatEnumTarget.getFormatter();
        return formatter.format(date);
    }
    
    /**
     * Formats a date from String-type into another String-type but with a different 
     * specified target formatter to follow.
     * @return A formatted date as a String with the new format.
     */
    public static String formatDate(
            String dateOrig, 
            DateFormatEnum dateFormatEnumOrig, 
            DateFormatEnum dateFormatEnumTarget) {
        DateFormat formatterOrig = dateFormatEnumOrig.getFormatter(); 
        Date date = null;
        try {
            date = formatterOrig.parse(dateOrig);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
        DateFormat formatterTarget = dateFormatEnumTarget.getFormatter();
        return formatterTarget.format(date);
    }
    
    /**
     * Formats a date from String-type into Date-type. To do this, the caller must know the
     * format style of the original String-type date.
     *
     * @param dateAsString - The String representation of the Date in question. 
     *                       Example, "2013-07-20T19:48:54-07:00"
     * @param dateAsStringFormatter - The format of the Date in this String input. One must know and pass 
     *                                in the formatStyle, so the method will know how to parse it out and 
     *                                produce a Date. Example, "yyyy-MM-ddTHH:mm:ss"
     * 
     * @return A Date-type date.
     */
    public static Date formatDate(
            String dateAsString, 
            DateFormatEnum dateAsStringFormatter) {
        DateFormat formatter = dateAsStringFormatter.getFormatter(); 
        Date theDate = null;
        try {
            theDate = formatter.parse(dateAsString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return theDate;
    }
    
}
