package com.trekcrumb.common.utility;

import java.util.regex.Matcher;

public class ValidationUtil {
    private ValidationUtil() {}
    
    /**
     * @return 'true' if the input String is qualified as null or empty.
     */
    public static boolean isEmpty(String stringObj) {
        if(stringObj == null
                || stringObj.trim().isEmpty()) {
            return true; 
        }
        if(stringObj.trim().equalsIgnoreCase("null")) {
            return true;
        }
        return false;
    }
    
    /**
     * @param isShouldNotEmpty - If the value is 'true', then the validation fails if input String
     *                           is empty. Otherwise, if input String is empty, the validation will
     *                           completes as success.
     * Throws IllegalArgumentException with a message if the input value is invalid.
     */
    public static void validateUsername(
            String username,
            boolean isShouldNotEmpty) {
        if(isShouldNotEmpty) {
            if(isEmpty(username)) {
                throw new IllegalArgumentException("Username cannot be empty.");
            }
        }
        String trimmedUsername = username.trim();
        if(trimmedUsername.length() < 6
                || trimmedUsername.length() > 25) {
            throw new IllegalArgumentException("Username must be between 6 and 25 chars!");
        }
        Matcher invalidCharMatcher = CommonConstants.STRING_VALUE_LETTERSNUMBERS_INVALID_PATTERN.matcher(trimmedUsername);
        if(invalidCharMatcher.find()) {
            throw new IllegalArgumentException("Username should contains only letters and numbers!");
        }
    }
    
    /**
     * @param isShouldNotEmpty - If the value is 'true', then the validation fails if input String
     *                           is empty. Otherwise, if input String is empty, the validation will
     *                           completes as success.
     * Throws IllegalArgumentException with a message if the input value is invalid.
     */
    public static void validatePassword(
            String password,
            boolean isShouldNotEmpty) {
        if(isShouldNotEmpty) {
            if(isEmpty(password)) {
                throw new IllegalArgumentException("Password cannot be empty.");
            }
        }
        String trimmedPwd = password.trim();
        if(trimmedPwd.length() < 6
                || trimmedPwd.length() > 25) {
            throw new IllegalArgumentException("Password must be between 6 and 25 chars.");
        }
    }    
    
    /**
     * @param isShouldNotEmpty - If the value is 'true', then the validation fails if input String
     *                           is empty. Otherwise, if input String is empty, the validation will
     *                           completes as success.
     * Throws IllegalArgumentException with a message if the input value is invalid.
     */
    public static void validateEmail(
            String email,
            boolean isShouldNotEmpty) {
        if(isShouldNotEmpty) {
            if(isEmpty(email)) {
                throw new IllegalArgumentException("Email cannot be empty.");
            }
        }
        if(!email.contains("@")
                || email.indexOf("@") == 0) {
            throw new IllegalArgumentException("Email format is invalid. Sample valid format: xxx@yyy.zzz");
        }
        if(!email.contains(".")
                || email.lastIndexOf(".") < email.indexOf("@")) {
            throw new IllegalArgumentException("Email format is invalid. Sample valid format: xxx@yyy.zzz");
        }
        
        String trimmedEmail = email.trim();
        Matcher invalidCharMatcher = CommonConstants.STRING_VALUE_EMAIL_INVALID_PATTERN.matcher(trimmedEmail);
        if(invalidCharMatcher.find()) {
            throw new IllegalArgumentException("Email contains invalid character! Sample valid format: xxx@yyy.zzz");
        }
    }    
    
}
