package com.trekcrumb.common.enums;

public enum ServiceStatusEnum implements IEnum{
    STATUS_SUCCESS(1, "STATUS_SUCCESS"),
    STATUS_FAIL(2, "STATUS_FAIL"),
    STATUS_WAITING(3, "STATUS_WAITING")
    ;
    
    private int id;
    private String value;
 
    private ServiceStatusEnum(int id, String value) {
        this.id = id;
        this.value = value;
    }
 
    @Override
    public String toString() {
        return "Service Status: " + value;
    }    

    public int getId() {
        return id;
    }
    
    public String getValue() {
        return value;
    }
 
}
