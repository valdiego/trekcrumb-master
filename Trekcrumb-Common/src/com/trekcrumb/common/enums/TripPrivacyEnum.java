package com.trekcrumb.common.enums;

public enum TripPrivacyEnum implements IEnum {
    PUBLIC(1, "PUBLIC", "Public for the world to see."),
    PRIVATE(2, "PRIVATE", "Private, no one can see."),
    FRIENDS(3, "FRIENDS", "Friends-only, not public.");
    
    private int id;
    private String value;
    private String description;
 
    private TripPrivacyEnum(int id, String value, String description) {
        this.id = id;
        this.value = value;
        this.description = description;
    }
 
    public int getId() {
        return id;
    }
 
    public String getValue() {
        return value;
    }
 
    public String getDescription() {
        return description;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Trip Privacy: ");
        sb.append("[Id = ").append(id);
        sb.append(", value = ").append(value);
        sb.append(", description = ").append(description);
        sb.append(']');
        return sb.toString();
    }
}
