package com.trekcrumb.common.enums;

public enum TripPublishEnum implements IEnum {
    PUBLISH(1, "Published", "On server, published for the world."),
    NOT_PUBLISH(2, "Not Published", "On local device, not on server.");
    
    private int id;
    private String value;
    private String description;
 
    private TripPublishEnum(int id, String value, String description) {
        this.id = id;
        this.value = value;
        this.description = description;
    }
 
    public int getId() {
        return id;
    }
 
    public String getValue() {
        return value;
    }
 
    public String getDescription() {
        return description;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Trip Publish: ");
        sb.append("[Id = ").append(id);
        sb.append(", value = ").append(value);
        sb.append(", description = ").append(description);
        sb.append(']');
        return sb.toString();
    }
}
