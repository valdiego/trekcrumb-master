package com.trekcrumb.common.enums;

public enum FileServerMenuEnum implements IEnum {
    READ_TREK_PICTURE(1, "READ_TREK_PICTURE"),
    WRITE_TREK_PICTURE(2, "WRITE_TREK_PICTURE"),
    DELETE_TREK_PICTURE(3, "DELETE_TREK_PICTURE"),
    
    READ_PROFILE_PICTURE(4, "READ_PROFILE_PICTURE"),
    WRITE_PROFILE_PICTURE(5, "WRITE_PROFILE_PICTURE"),
    DELETE_PROFILE_PICTURE(6, "DELETE_PROFILE_PICTURE")
    ;
    
    private int id;
    private String value;
 
    private FileServerMenuEnum(int id, String value) {
        this.id = id;
        this.value = value;
    }
 
    @Override
    public String toString() {
        return "FileServer Menu: " + value;
    }    

    public int getId() {
        return id;
    }
    
    public String getValue() {
        return value;
    }
 
}
