package com.trekcrumb.common.enums;

/**
 * Interface for the app Enum that dictates the methods any child must implement.  
 *
 */
public interface IEnum {
    public int getId();
    public String getValue();
}
