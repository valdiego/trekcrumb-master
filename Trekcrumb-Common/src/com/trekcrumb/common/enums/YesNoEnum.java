package com.trekcrumb.common.enums;

public enum YesNoEnum implements IEnum {
    NO(0, "NO"),
    YES(1, "YES");
    
    private int id;
    private String value;
 
    private YesNoEnum(int id, String value) {
        this.id = id;
        this.value = value;
    }
    
    public int getId() {
        return id;
    }
 
    public String getValue() {
        return value;
    }
 
    @Override
    public String toString() {
        return getValue();
    }
}
