package com.trekcrumb.common.enums;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.trekcrumb.common.utility.CommonConstants;

public enum DateFormatEnum implements IEnum {
    GMT_STANDARD_LONG(
            1, 
            CommonConstants.DATE_FORMAT_GMT_STANDARD_LONG, 
            getDateFormatter(CommonConstants.DATE_FORMAT_GMT_STANDARD_LONG, TimeZone.getTimeZone("GMT"))),
            
    LOCALE_STANDARD_LONG(
            3, 
            CommonConstants.DATE_FORMAT_LOCALE_STANDARD_LONG, 
            getDateFormatter(CommonConstants.DATE_FORMAT_LOCALE_STANDARD_LONG, TimeZone.getDefault())),
            
    LOCALE_UI_WITH_TIME(
            5, 
            CommonConstants.DATE_FORMAT_LOCALE_UI_WITH_TIME, 
            getDateFormatter(CommonConstants.DATE_FORMAT_LOCALE_UI_WITH_TIME, TimeZone.getDefault())),
            
    LOCALE_UI_WITHOUT_TIME(
            6, 
            CommonConstants.DATE_FORMAT_LOCALE_UI_WITHOUT_TIME, 
            getDateFormatter(CommonConstants.DATE_FORMAT_LOCALE_UI_WITHOUT_TIME, TimeZone.getDefault()));
    
    private int id;
    private String value;
    private SimpleDateFormat formatter;
 
    private DateFormatEnum(int id, String value, SimpleDateFormat formatter) {
        this.id = id;
        this.value = value;
        this.formatter = formatter;
    }
 
    public int getId() {
        return id;
    }
 
    public String getValue() {
        return value;
    }
 
    public SimpleDateFormat getFormatter() {
        return formatter;
    }

    private static SimpleDateFormat getDateFormatter(String formatStyle, TimeZone zone) {
        SimpleDateFormat formatter = new SimpleDateFormat(formatStyle);
        formatter.setTimeZone(zone);
        return formatter;
    }


}
