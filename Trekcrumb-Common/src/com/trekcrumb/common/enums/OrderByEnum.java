package com.trekcrumb.common.enums;

public enum OrderByEnum implements IEnum {
    ORDER_BY_CREATED_DATE(1, " order by CREATED desc"),
    ORDER_BY_CREATED_DATE_ASC(2, " order by CREATED asc"),
    ORDER_BY_UPDATED_DATE(3, " order by UPDATED desc"),
    ORDER_BY_UPDATED_DATE_ASC(4, " order by UPDATED asc");
    
    private int id;
    private String value;
 
    private OrderByEnum(int id, String value) {
        this.id = id;
        this.value = value;
    }
    
    public int getId() {
        return id;
    }
 
    public String getValue() {
        return value;
    }
 
    @Override
    public String toString() {
        return getValue();
    }
}
