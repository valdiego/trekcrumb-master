package com.trekcrumb.common.enums;

public enum TripStatusEnum implements IEnum {
    ACTIVE(1, "ACTIVE"),
    COMPLETED(2, "COMPLETED");

    private int id;
    private String value;
 
    private TripStatusEnum(int id, String value) {
        this.id = id;
        this.value = value;
    }
    
    public int getId() {
        return id;
    }
 
    public String getValue() {
        return value;
    }
 
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Trip Status: ");
        sb.append("[Id = ").append(id);
        sb.append(", value = ").append(value);
        sb.append(']');
        return sb.toString();
    }
}
