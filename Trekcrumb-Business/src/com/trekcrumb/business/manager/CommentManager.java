package com.trekcrumb.business.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.ICommentDAO;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Orchestration service for Comment management that communicate/integrate multiple component layers 
 * (DB, file system, etc.).
 *
 */
public class CommentManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.CommentManager"); 

    public static String create(
            Comment commentToCreate) 
            throws Exception {
        if(commentToCreate == null) {
            throw new IllegalArgumentException("Comment Create error: A complete Favorite bean is required!");
        }
        if(ValidationUtil.isEmpty(commentToCreate.getTripID())
                || ValidationUtil.isEmpty(commentToCreate.getUserID())) {
            throw new IllegalArgumentException("Comment Create error: TripID and UserID are required!");
        }
        if(ValidationUtil.isEmpty(commentToCreate.getNote())) {
            //Ignore attempt to create EMPTY comment:
            return null;
        }

        //Configure input as necessary:
        String commentID = CommonUtil.generateID(commentToCreate.getTripID());
        commentToCreate.setId(commentID);
        commentToCreate.setNote(BusinessUtil.neutralizeStringValue(commentToCreate.getNote(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_COMMENT_NOTE));
        
        try {
            ICommentDAO commentDAOImpl = DAOFactory.getCommentDAOInstance();
            commentDAOImpl.create(commentToCreate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Comment Create error: " + e.getMessage(), e);
            throw e;
        }
        
        return commentID;
    }
    
    public static List<Comment> retrieve(
            Comment commentToRetrieve,
            ServiceTypeEnum retrieveBy,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(commentToRetrieve == null) {
            throw new IllegalArgumentException("Comment Retrieve error: Comment bean is required!");
        }
        
        List<Comment> listOfComments = null;
        try {
            ICommentDAO commentDAOImpl = DAOFactory.getCommentDAOInstance();
            listOfComments = commentDAOImpl.read(commentToRetrieve, retrieveBy, orderBy, offset, numOfRows);
            
            //Remove any sensitive data:
            if(listOfComments != null && listOfComments.size() != 0) {
                for(Comment comment : listOfComments) {
                    comment.setUserID(null);
                }
            }
            
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Comment Retrieve error: " + e.getMessage(), e);
            throw e;
        }

        return listOfComments;
    }
    
    public static int count(Comment commentToCount, ServiceTypeEnum retrieveBy) throws Exception {
        if(commentToCount == null) {
            throw new IllegalArgumentException("Comment Count error: Comment bean is required!");
        }

        int numFound = 0;
        try {
            ICommentDAO commentDAOImpl = DAOFactory.getCommentDAOInstance();
            numFound = commentDAOImpl.count(commentToCount, retrieveBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Comment Count error: " + e.getMessage(), e);
            throw e;
        }
        
        return numFound;
    }
    
    public static int delete(Comment commentToDelete, ServiceTypeEnum deleteBy) throws Exception {
        if(commentToDelete == null) {
            throw new IllegalArgumentException("Comment Delete error: Comment bean is required!");
        }

        int numDeleted = 0;
        try {
            ICommentDAO commentDAOImpl = DAOFactory.getCommentDAOInstance();
            numDeleted = commentDAOImpl.delete(commentToDelete, deleteBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Comment Delete error: " + e.getMessage(), e);
            throw e;
        }
        return numDeleted;
    }

}
