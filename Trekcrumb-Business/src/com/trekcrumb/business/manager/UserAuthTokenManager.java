package com.trekcrumb.business.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.IUserAuthTokenDAO;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Orchestration service for UserAuthToken management that communicate/integrate multiple component layers 
 * (DB, file system, security, etc.).
 *
 */
public class UserAuthTokenManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.UserAuthTokenManager"); 
    
    public static UserAuthToken create(
            final UserAuthToken userAuthTokenInput) 
            throws Exception {
        if(ValidationUtil.isEmpty(userAuthTokenInput.getUserId())
                || ValidationUtil.isEmpty(userAuthTokenInput.getUsername())
                || ValidationUtil.isEmpty(userAuthTokenInput.getDeviceIdentity())) {
            throw new IllegalArgumentException("UserAuthToken Create error: userID, username and device identity are required!");
        }
        UserAuthToken userAuthTokenToCreate = userAuthTokenInput.clone();
        userAuthTokenToCreate.setAuthToken(generateAuthToken(userAuthTokenInput));
        
        IUserAuthTokenDAO userAuthTokenDAO = DAOFactory.getUserAuthTokenDAOInstance();
        try {
            return userAuthTokenDAO.create(userAuthTokenToCreate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Create error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Securely authenticate the UserAuthToken given its unique AuthToken and its User ID.
     * @param userAuthTokenInput - Contains User ID and secure UserAuthToken (String).
     * @throws Exception
     */
    public static User authenticate(
            final UserAuthToken userAuthTokenInput) 
            throws Exception {
        IUserAuthTokenDAO userAuthTokenDAO = DAOFactory.getUserAuthTokenDAOInstance();
        try {
            return userAuthTokenDAO.authenticate(userAuthTokenInput);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Authenticate error: " + e.getMessage(), e);
            throw e;
        }        
    }

    /**
     * Retrieves UserAuthToken either by Device Identity or by User ID.
     * @param userAuthTokenInput - Contains User ID and/or Device Identity. If the latter is
     *                             provided, then it will read UserAuthToken by Device Identity
     *                             which is unique. Otherwise, it reads by User ID which could be
     *                             a list of UserAuthTokens.
     * @return A list of UserAuthTokens.
     * @throws Exception
     */
    public static List<UserAuthToken> retrieve(
            final UserAuthToken userAuthTokenInput,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        IUserAuthTokenDAO userAuthTokenDAO = DAOFactory.getUserAuthTokenDAOInstance();
        try {
            return userAuthTokenDAO.read(userAuthTokenInput, retrieveBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Retrieve error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * @return Number of UserAuthTokens deleted.
     */
    public static int delete(
            final UserAuthToken userAuthTokenInput,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        IUserAuthTokenDAO userAuthTokenDAO = DAOFactory.getUserAuthTokenDAOInstance();
        try {
            return userAuthTokenDAO.delete(userAuthTokenInput, deleteBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Delete error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Deletes UserAuthToken by UserAuthToken ID (default)
     * @param userAuthTokenInput - The UserAuthToken bean to delete including its ID and User ID
     *                             (both required).
     * @return A refreshed list of UserAuthToken after deletion for the given User.
     */
    public static List<UserAuthToken> delete(
            final UserAuthToken userAuthTokenInput) 
            throws Exception {
        List<UserAuthToken> userAuthTokenList = null;
        IUserAuthTokenDAO userAuthTokenDAO = DAOFactory.getUserAuthTokenDAOInstance();
        try {
            userAuthTokenDAO.delete(userAuthTokenInput, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID);
            userAuthTokenList = userAuthTokenDAO.read(userAuthTokenInput, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Delete error: " + e.getMessage(), e);
            throw e;
        }
        
        return userAuthTokenList;
    }
    
    private static String generateAuthToken(UserAuthToken userAuthTokenInput) {
        return CommonUtil.generateID(null);
    }

}
