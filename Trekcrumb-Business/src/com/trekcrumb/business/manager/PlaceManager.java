package com.trekcrumb.business.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.IPlaceDAO;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Orchestration service for Place management that communicate/integrate multiple component layers 
 * (DB, file system, etc.).
 *
 */
public class PlaceManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.PlaceManager"); 

    /**
     * Creates a new Place.
     * 
     * Note:
     * 1. This operation is mostly for adding a new Place to an existing Trip
     * 2. It will set defaults to the new Place (ID, created/updated dates). 
     *    If any client/caller has set these values, it will be overwritten with server's values. This
     *    is done for consistency, no matter who the clients are and how they set these values. 
     * 3. To create a Place that is previously created on user local device, see TripManager.publish()
     *    and PlaceManager.publish()
     */
    public static Place create(
            final Place placeToCreate) 
            throws Exception {
        if(ValidationUtil.isEmpty(placeToCreate.getTripID())
                || ValidationUtil.isEmpty(placeToCreate.getUserID())
                || placeToCreate.getLatitude() == 0
                || placeToCreate.getLongitude() == 0) {
            throw new IllegalArgumentException("Place Create error: TripID, UserID, latitude and longitude values are required!");
        }
        
        //Configure input as necessary:
        //Note: For 'default' location, see Place.getLocation()
        Place placeCreated = placeToCreate.clone();
        placeCreated.setId(CommonUtil.generateID(placeToCreate.getTripID()));
        placeCreated.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
        placeCreated.setUpdated(placeCreated.getCreated());
        placeCreated.setLocation(BusinessUtil.neutralizeStringValue(placeCreated.getLocation(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_LOCATION));
        placeCreated.setNote(BusinessUtil.neutralizeStringValue(placeCreated.getNote(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_NOTE));
        
        //Execute DAO compoments:
        try {
            IPlaceDAO placeDAOImpl = DAOFactory.getPlaceDAOInstance();
            return placeDAOImpl.create(placeCreated);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Place Create error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * @return List of Places found. NULL if nothing found.
     */
    public static List<Place> retrieve(
            Place placeToRetrieve,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        try {
            IPlaceDAO placeDAOImpl = DAOFactory.getPlaceDAOInstance();
           return placeDAOImpl.read(placeToRetrieve, retrieveBy, null);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Place Retrieve error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Updates a Place.
     * 
     * Note:
     * 1. It will set defaults to Place (updated date). 
     *    If any client/caller has set these values, it will be overwritten with server's values. This
     *    is done for consistency, no matter who the clients are and how they set these values. 
     *    
     * @return The updated Place.
     */
    public static Place update(
            Place placeToUpdate) 
            throws Exception{
        if(placeToUpdate == null
                || ValidationUtil.isEmpty(placeToUpdate.getUserID())
                || ValidationUtil.isEmpty(placeToUpdate.getTripID())
                || ValidationUtil.isEmpty(placeToUpdate.getId())) {
            throw new IllegalArgumentException("Place Update error: UserID, TripID and PlaceID are required!");
        }

        //Configure input as necessary:
        placeToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        placeToUpdate.setNote(BusinessUtil.neutralizeStringValue(placeToUpdate.getNote(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_PLACE_NOTE));
        
        //Execute DAO compoments:
        try {
            IPlaceDAO placeDAOImpl = DAOFactory.getPlaceDAOInstance();
            return placeDAOImpl.update(placeToUpdate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Place Update error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static int delete(
            Place placeToDelete, 
            ServiceTypeEnum deleteBy) 
            throws Exception {
        try {
            IPlaceDAO placeDAOImpl = DAOFactory.getPlaceDAOInstance();
            return placeDAOImpl.delete(placeToDelete, deleteBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Place Delete error: " + e.getMessage(), e);
            throw e;
        }
    }


}
