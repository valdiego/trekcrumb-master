package com.trekcrumb.business.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.IFavoriteDAO;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Orchestration service for Favorite management that communicate/integrate multiple component layers 
 * (DB, file system, etc.).
 *
 */
public class FavoriteManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.FavoriteManager"); 

    /**
     * Creates a new Favorite.
     * 
     */
    public static void create(
            final Favorite favoriteToCreate) 
            throws Exception {
        if(favoriteToCreate == null) {
            throw new IllegalArgumentException("Favorite Create error: A complete Favorite bean is required!");
        }
        if(ValidationUtil.isEmpty(favoriteToCreate.getTripID())
                || ValidationUtil.isEmpty(favoriteToCreate.getUserID())) {
            throw new IllegalArgumentException("Favorite Create error: TripID and UserID are required!");
        }

        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            favoriteDAOImpl.create(favoriteToCreate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Create error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Retrieve Favorites given Favorite's criteria.
     * @return List of Favorites found.
     */
    public static List<Favorite> retrieve(
            Favorite favoriteToRetrieve,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(favoriteToRetrieve == null) {
            throw new IllegalArgumentException("Favorite Retrieve error: A complete Favorite bean is required!");
        }
        if(ValidationUtil.isEmpty(favoriteToRetrieve.getTripID())
                && ValidationUtil.isEmpty(favoriteToRetrieve.getUserID())) {
            throw new IllegalArgumentException("Favorite Retrieve error: TripID and/or UserID are required!");
        }
        
        List<Favorite> listOfFavorites = null;
        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            listOfFavorites = favoriteDAOImpl.read(favoriteToRetrieve, orderBy, offset, numOfRows);
            
            //Remove any sensitive data:
            if(listOfFavorites != null && listOfFavorites.size() != 0) {
                for(Favorite favorite : listOfFavorites) {
                    favorite.setUserID(null);
                }
            }
            
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Retrieve error: " + e.getMessage(), e);
            throw e;
        }

        return listOfFavorites;
    }
    
    /**
     * Retrieve Users who favorite a Trip given this Trip's criteria.
     * @return List of Users found.
     */
    public static List<User> retrieveByTrip(
            Trip trip,
            int offset,
            int numOfRows) 
            throws Exception {
        if(trip == null || ValidationUtil.isEmpty(trip.getId())) {
            throw new IllegalArgumentException("Favorite Retrieve by Trip error: TripID is required!");
        }

        List<User> listOfUsers = null;
        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            listOfUsers = favoriteDAOImpl.readByTrip(trip, offset, numOfRows);
            
            //Remove any sensitive data:
            if(listOfUsers != null && listOfUsers.size() != 0) {
                for(User user : listOfUsers) {
                    BusinessUtil.secureUserData(user, false);
                }
            }
            
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Retrieve by Trip error: " + e.getMessage(), e);
            throw e;
        }

        return listOfUsers;
    }
    
    /**
     * Retrieve Trips whom a User marks as his/her favorites given this User's criteria.
     * @return List of Trips found.
     */
    public static List<Trip> retrieveByUser(
            User user,
            int offset,
            int numOfRows) 
            throws Exception {
        if(user == null || ValidationUtil.isEmpty(user.getUsername())) {
            throw new IllegalArgumentException("Favorite Retrieve by User error: Username is required!");
        }

        List<Trip> listOfTrips = null;
        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            listOfTrips = favoriteDAOImpl.readByUser(user, offset, numOfRows);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Retrieve by User error: " + e.getMessage(), e);
            throw e;
        }

        return listOfTrips;
    }    

    public static int count(Favorite favoriteToCount) throws Exception {
        if(favoriteToCount == null) {
            throw new IllegalArgumentException("Favorite Count error: A complete Favorite bean is required!");
        }
        if(ValidationUtil.isEmpty(favoriteToCount.getTripID())
                && ValidationUtil.isEmpty(favoriteToCount.getUserID())) {
            throw new IllegalArgumentException("Favorite Count error: TripID and/or UserID are required!");
        }

        int numFound = 0;
        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            numFound = favoriteDAOImpl.count(favoriteToCount);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Count error: " + e.getMessage(), e);
            throw e;
        }
        return numFound;
    }
    
    public static int countByUser(User user) throws Exception {
        if(user == null || ValidationUtil.isEmpty(user.getUsername())) {
            throw new IllegalArgumentException("Favorite Count by User error: Username is required!");
        }

        int numFound = 0;
        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            numFound = favoriteDAOImpl.countByUser(user);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Count by User error: " + e.getMessage(), e);
            throw e;
        }
        return numFound;
    }

    public static int delete(Favorite favoriteToDelete) throws Exception {
        if(favoriteToDelete == null) {
            throw new IllegalArgumentException("Favorite Delete error: A complete Favorite bean is required!");
        }
        if(ValidationUtil.isEmpty(favoriteToDelete.getTripID())
                && ValidationUtil.isEmpty(favoriteToDelete.getUserID())) {
            throw new IllegalArgumentException("Favorite Delete error: TripID and/or UserID are required!");
        }

        int numDeleted = 0;
        try {
            IFavoriteDAO favoriteDAOImpl = DAOFactory.getFavoriteDAOInstance();
            numDeleted = favoriteDAOImpl.delete(favoriteToDelete);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Favorite Delete error: " + e.getMessage(), e);
            throw e;
        }
        return numDeleted;
    }

}
