package com.trekcrumb.business.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.ITripDAO;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Orchestration service for Trip management that communicate/integrate multiple component layers 
 * (DB, file system, etc.).
 *
 */
public class TripManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.TripManager"); 
    
    /**
     * Creates a fresh, brand new Trip.
     * 
     * Note:
     * 1. It will set defaults to the new Trip ID, created/updated dates and statuses (Active and Published)
     *    If any client/caller has set these values, it will be overwritten with server's values. This
     *    is done for consistency, no matter who the clients are and how they set these values. 
     * 2. To create a Trip that is previously created on user local device, see publish().
     */
    public static Trip create(
            final Trip tripToCreate) 
            throws Exception {
        if(ValidationUtil.isEmpty(tripToCreate.getUserID())
                || ValidationUtil.isEmpty(tripToCreate.getName())
                || tripToCreate.getPrivacy() == null) {
            throw new IllegalArgumentException("Trip Create: ERROR - UserID, Trip name and privacy are required!");
        }
        
        //Configure input as necessary:
        Trip tripCreated = tripToCreate.clone();
        tripCreated.setId(CommonUtil.generateID(null));
        tripCreated.setName(BusinessUtil.neutralizeStringValue(tripCreated.getName(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME));
        tripCreated.setNote(BusinessUtil.neutralizeStringValue(tripCreated.getNote(), false, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NOTE));
        tripCreated.setPublish(TripPublishEnum.PUBLISH);
        tripCreated.setStatus(TripStatusEnum.ACTIVE);
        tripCreated.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
        tripCreated.setUpdated(tripCreated.getCreated());
        
        if(ValidationUtil.isEmpty(tripCreated.getLocation())) {
            if(tripToCreate.getListOfPlaces() != null
                    && tripToCreate.getListOfPlaces().size() > 0) {
                //Note: For 'default' location, see Place.getLocation()
                String location = tripToCreate.getListOfPlaces().get(0).getLocation();
                location = BusinessUtil.neutralizeStringValue(location, true, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION);
                tripCreated.setLocation(location);
            }
        } else {
            tripCreated.setLocation(BusinessUtil.neutralizeStringValue(tripCreated.getLocation(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION));
        }
        
        //Execute DAO compoments:
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.create(tripCreated);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Create: ERROR - " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * @return List of Trips found. NULL if nothing found.
     */
    public static List<Trip> retrieve(
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(tripToRetrieve == null || retrieveBy == null) {
            throw new IllegalArgumentException("Trip Retrieve error: Trip to retrieve and retrieve-by are required!");
        }
        
        List<Trip> tripsFoundList = null;
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            tripsFoundList = tripDAOImpl.read(tripToRetrieve, retrieveBy, whereClause, orderBy, offset, numOfRows);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Retrieve error: " + e.getMessage(), e);
            throw e;
        }
        
        if(tripsFoundList != null && tripsFoundList.size() > 0) {
            if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID) {
                tripsFoundList = processRetrieveTripByTripIDResult(tripToRetrieve, tripsFoundList);
            }
        }
        
        return tripsFoundList;
    }
    
    /**
     * Counts the Trips. A companion method for the retrieve() operation.
     */
    public static int count(
            Trip tripToCount,
            ServiceTypeEnum retrieveBy,
            String whereClause) 
            throws Exception {
        if(tripToCount == null || retrieveBy == null) {
            throw new IllegalArgumentException("Trip Count error: Trip to count and retrieve-by are required!");
        }
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.count(tripToCount, retrieveBy, whereClause);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Count error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static List<Trip> search(
            TripSearchCriteria tripSearchCriteria) 
            throws Exception {
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.search(tripSearchCriteria);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Search error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static int count(
            TripSearchCriteria tripSearchCriteria) 
            throws Exception {
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.count(tripSearchCriteria);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Count error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Updates a Trip.
     * Note:
     * 1. This update does not apply to PUBLISH column since the architecture prevents updating only 
     *    a Trip's publish value. Changing a NOT_PUBLISH to PUBLISH Trip is essentially the same as
     *    create a new Trip on the server, therefore see publish().
     * 2. Updated date: It will set defaults to server's date.
     *    If any client/caller has set these values, it will be overwritten with server's values. This
     *    is done for consistency, no matter who the clients are and how they set these values. 
     * 
     * @param tripToUpdate - The Trip bean containing ONLY the data to be updated. 
     *                       UserID and/or TripID are required based on updateFor parameter.
     * @return The updated Trip.
     */    
    public static Trip update(
            Trip tripToUpdate) 
            throws Exception {
        if(tripToUpdate == null
                || ValidationUtil.isEmpty(tripToUpdate.getUserID())
                || ValidationUtil.isEmpty(tripToUpdate.getId())) {
            throw new IllegalArgumentException("Trip Update error: Trip to update bean must contain User ID and Trip ID!");
        }

        //Configure input as necessary:
        tripToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        tripToUpdate.setName(BusinessUtil.neutralizeStringValue(tripToUpdate.getName(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NAME));
        tripToUpdate.setNote(BusinessUtil.neutralizeStringValue(tripToUpdate.getNote(), false, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_NOTE));
        tripToUpdate.setLocation(BusinessUtil.neutralizeStringValue(tripToUpdate.getLocation(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_TRIP_LOCATION));

        //Execute DAO compoments:
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.update(tripToUpdate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Update error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static int delete(
            Trip tripToDelete, 
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(tripToDelete == null) {
            throw new IllegalArgumentException("Trip Delete error: Trip to delete bean is required!");
        }
        try {
            //Delete trips' picture files when applicable:
            Picture pictureToDelete = new Picture();
            pictureToDelete.setUserID(tripToDelete.getUserID());
            pictureToDelete.setTripID(tripToDelete.getId());
            if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
                PictureManager.delete(pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_USERID);
            } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                PictureManager.delete(pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            }
            
            //Delete trips:
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.delete(tripToDelete, deleteBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Delete error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Creates a Trip that is previously created on user local device.
     * @return The published trip.
     */
    public static Trip publish(
            Trip tripToPublish) 
            throws Exception {
        if(tripToPublish == null
                || ValidationUtil.isEmpty(tripToPublish.getUserID())
                || ValidationUtil.isEmpty(tripToPublish.getId())
                || ValidationUtil.isEmpty(tripToPublish.getName())
                || tripToPublish.getPrivacy() == null
                || tripToPublish.getStatus() == null
                || ValidationUtil.isEmpty(tripToPublish.getCreated())
                || ValidationUtil.isEmpty(tripToPublish.getUpdated())) {
            throw new IllegalArgumentException("Trip Publish error: UserID, TripID, Trip name, status, privacy, created and updated dates are all required!");
        }
        try {
            //Prepare data and defaults:
            Trip tripPublished = tripToPublish.clone();
            tripPublished.setPublish(TripPublishEnum.PUBLISH);

            //Execute:
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.publish(tripPublished);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip Publish error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Retrieves Trip(s) as a sync operation for the given User and a Trip's last updated date.
     */
    public static List<Trip> retrieveSync(
            Trip tripToRetrieve) 
            throws Exception {
        if(tripToRetrieve == null 
                || ValidationUtil.isEmpty(tripToRetrieve.getUserID())) {
            throw new IllegalArgumentException("Trip RetrieveSync error: UserID is required!");
        }
        try {
            ITripDAO tripDAOImpl = DAOFactory.getTripDAOInstance();
            return tripDAOImpl.readSync(tripToRetrieve);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Trip RetrieveSync error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    private static List<Trip> processRetrieveTripByTripIDResult(
            Trip tripToRetrieve, 
            List<Trip> tripsFoundList) {
        Trip tripFound = tripsFoundList.get(0);
        
        //Evaluate if Trip privacy is restricted:
        if(tripFound.getPrivacy() == TripPrivacyEnum.PRIVATE) {
            String userIDRequester = tripToRetrieve.getUserID();
            String userIDOwner = tripFound.getUserID();
            if(!userIDOwner.equalsIgnoreCase(userIDRequester)) {
                LOGGER.error("Trip Retrieve error: Due to trip privacy setting, user has no permission to view the Trip in question!");
                tripsFoundList = null;
                return tripsFoundList;
            }
        }
        
        //Evaluate if Trip contains favorite User beans:
        if(tripFound.getListOfFavoriteUsers() != null) {
            //Secure user data:
            List<User> listOfUsers = tripFound.getListOfFavoriteUsers();
            if(listOfUsers != null && listOfUsers.size() != 0) {
                for(User user : listOfUsers) {
                    BusinessUtil.secureUserData(user, false);
                }
            }
        }
        
        return tripsFoundList;
    }
    
    
}
