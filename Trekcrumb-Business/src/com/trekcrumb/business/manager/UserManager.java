package com.trekcrumb.business.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.IUserDAO;
import com.trekcrumb.business.email.EmailManagerFactory;
import com.trekcrumb.business.email.IEmailManager;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.crypto.Cryptographer;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.ImageFormatNotSupportedException;
import com.trekcrumb.common.exception.UserDuplicateException;
import com.trekcrumb.common.exception.UserNotConfirmedException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.fileserverclient.FileServerClientFactory;
import com.trekcrumb.fileserverclient.IFileServerClient;

/**
 * Orchestration service for User management that communicate/integrate multiple component layers 
 * (DB, file system, security, etc.).
 *
 */
public class UserManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.UserManager"); 
    
    /***
     * Creates a new user given the User bean parameters. The method will do proper
     * validation before it creates a new user.
     * @param userInput - The user bean containing required parameters.
     * @return The newly created User bean.
     * @throws Exception If the following met:
     *         1. The user bean missing required parameters.
     *         2. The user has duplicate email or username in the system.
     *         3. Unexpected error from the back end system.
     */
    public static User userCreate(final User userInput) throws Exception {
        if(userInput == null) {
            throw new IllegalArgumentException("User bean cannot be empty!");
        }
        
        //Validate input:
        ValidationUtil.validateUsername(userInput.getUsername(), true);
        ValidationUtil.validateEmail(userInput.getEmail(), true);
        if(ValidationUtil.isEmpty(userInput.getFullname())) {
            throw new IllegalArgumentException("Fullname is required!");
        }
        User userToCreate = userInput.clone();
        userToCreate = validateAndProcessPassword(userToCreate);
        
        //Configure input as necessary:
        userToCreate.setEmail(userToCreate.getEmail().toLowerCase());
        userToCreate.setUsername(userToCreate.getUsername().toLowerCase());
        userToCreate.setFullname(BusinessUtil.neutralizeStringValue(
                userToCreate.getFullname(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME));
        userToCreate.setActive(YesNoEnum.YES);
        /*
         * NOTE (10/30/2013): Per requirement change, by default new user is confirmed.
         * Implementation of user to confirm via email has been postponed/canceled.
         */
        //userToCreate.setConfirmed(YesNoEnum.NO);
        userToCreate.setConfirmed(YesNoEnum.YES);
        
        //Execute DAO compoments:
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        User userCreated = null;
        try {
            userCreated = userDaoImpl.create(userToCreate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserDuplicateException ude) {
            throw ude;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("Error during User Create: " + e.getMessage(), e);
            throw e;
        }
        
        //Return User:
        if(userCreated != null) {
            createUserAuthToken(userInput, userCreated);
            BusinessUtil.secureUserData(userCreated, true);
        }
        return userCreated;
    }
    
    /**
     * Attempts to complete user confirmation after register. This action basically updates
     * the 'Confirmed' flag to YES.
     * 
     * @return TRUE if confirmation successful. FALSE if it does not need any confirmation.
     * @exception UserNotFoundException is thrown if username/password pair is invalid.
     */
    public static boolean userConfirm(final User userInput) throws Exception {
        if(userInput == null
                || ValidationUtil.isEmpty(userInput.getUsername()) 
                || ValidationUtil.isEmpty(userInput.getPassword())) {
            throw new IllegalArgumentException("User Confirm error: Username and password are required to confirm!");
        }
        
        User userToConfirm = userInput.clone();
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        try {
            List<User> usersFound = 
                    userDaoImpl.read(userToConfirm, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, false, true, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Confirm error: User is not found for given username and password!");
            }
            User userFound = usersFound.get(0);

            //Authenticate Password:
            authenticatePassword(userToConfirm, userFound);
            
            //Update:
            User userUpdate = new User();
            userUpdate.setUserId(userFound.getUserId());
            userUpdate.setConfirmed(YesNoEnum.YES);
            User userUpdated = userDaoImpl.update(userUpdate);
            if(userUpdated != null) {
                return true;
            } else {
                return false;
            }
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("User Confirm error: " + e.getMessage(), e);
            throw e;
        }        
    }

    /**
     * Attempts to authenticate user given the username and password.
     * @return User bean if authenticate is successful.
     *         If user Active is'NO', return User bean with only UserID, updated date and the active status.
     * @exception UserNotFoundException is thrown if user authentication fails.
     * @exception UserNotConfirmedException is thrown if user confirm status is 'NO'.
     */
    public static User userLogin(final User userInput) throws Exception {
        if(userInput == null
                || ValidationUtil.isEmpty(userInput.getUsername()) 
                || ValidationUtil.isEmpty(userInput.getPassword())) {
             throw new IllegalArgumentException("User Login error: Username and password are required!");
        }
        User userFound = null;
        User userToLogin = userInput.clone();
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        try {
            List<User> usersFound = 
                    userDaoImpl.read(userToLogin, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, true, true, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Login error: User is not found for given username and password!");
            }
            userFound = usersFound.get(0);
            
            /*
             * NOTE (10/30/2013): Per requirement change, by default new user is confirmed.
             * Implementation of user to confirm via email has been postponed/canceled.
             */
            /*
            if(userLoggedIn.getConfirmed() != YesNoEnum.YES) {
                throw new UserNotConfirmedException("User Login error: User authentication cannot be done because user has not confirmed registration!");
            }
            */

            //Authenticate password:
            authenticatePassword(userToLogin, userFound);
            
            /*
             * SecurityToken: 
             * 1. If any user has claimed to forget his/her password, a SecurityToken is assigned to his account. 
             *    But if he/she logs in without reset it, we must delete the SecurityToken
             * 2. We cannot do the update logic inside userDaoImpl.read() - which would have been
             *    the most efficient - because it would affect other operations that invoke the same read() method.
             */
            if(!ValidationUtil.isEmpty(userFound.getSecurityToken())) {
                User userToUpdate = new User();
                userToUpdate.setUserId(userFound.getUserId());
                userToUpdate.setSecurityToken("");
                userDaoImpl.update(userToUpdate);
            }
            
            /*
             * Check Active status:
             * 1. Return only necessary User data for reactivation usage when applicable
             */
            if(userFound.getActive() == YesNoEnum.NO) {
                User userDeactivated = new User();
                userDeactivated.setUserId(userFound.getUserId());
                userDeactivated.setUsername(userFound.getUsername());
                userDeactivated.setActive(YesNoEnum.NO);
                userDeactivated.setUpdated(userFound.getUpdated());
                return userDeactivated;
            }
            
            //Return User:
            createUserAuthToken(userInput, userFound);
            BusinessUtil.secureUserData(userFound, true);
            return userFound;

        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(UserNotConfirmedException unce) {
            throw unce;
        } catch(Exception e) {
            LOGGER.error("User Login error: " + e.getMessage(), e);
            throw e;
        }
    }
    
   /**
     * Updates user data. 
     * @param userInput - The User bean containing userID (required) and user data to update.
     * @throws Exception
     */
    public static User userUpdate(final User userInput) throws Exception {
        if(userInput == null 
                || ValidationUtil.isEmpty(userInput.getUserId())) {
            throw new IllegalArgumentException("User Update error: UserID is required!");
        }
        User userToUpdate = userInput.clone();

        //If update password:
        if(userToUpdate.getPassword() != null) {
            if(userToUpdate.getPassword().trim().isEmpty()) {
                userToUpdate.setPassword(null);
            } else {
                userToUpdate = validateAndProcessPassword(userToUpdate);
            }
        }
        
        //If update fullname:
        if(ValidationUtil.isEmpty(userToUpdate.getFullname())) {
            //Prevent empty value:
            userToUpdate.setFullname(null);
        } else {
            userToUpdate.setFullname(BusinessUtil.neutralizeStringValue(userToUpdate.getFullname(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_FULLNAME));
        }
        
        //If update other String contents, neutralize them as necessary:
        userToUpdate.setLocation(BusinessUtil.neutralizeStringValue(userToUpdate.getLocation(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_LOCATION));
        userToUpdate.setSummary(BusinessUtil.neutralizeStringValue(userToUpdate.getSummary(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_USER_SUMMARY));

        //If update email:
        if(userToUpdate.getEmail() != null) {
            if(userToUpdate.getEmail().trim().isEmpty()) {
                userToUpdate.setEmail(null);
            } else {
                ValidationUtil.validateEmail(userToUpdate.getEmail(), false);
                userToUpdate.setEmail(userToUpdate.getEmail().toLowerCase());
            }
        }

        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();

        //If update profile image:
        if(userToUpdate.getProfileImageBytes() != null) {
            //If this failed, exception will be thrown:
            createOrUpdateUserProfileImage(userToUpdate, userDaoImpl);
        }
        
        //Execute DAO compoments:
        User userUpdated = null;
        try {
            userUpdated = userDaoImpl.update(userToUpdate);
            BusinessUtil.secureUserData(userUpdated, true);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserDuplicateException ude) {
            throw ude;
        } catch(Exception e) {
            LOGGER.error("User Update error: " + e.getMessage(), e);
            throw e;
        }
        return userUpdated;
    }
    
    /**
     * De-activate user account. 
     * @param user - The User bean containing userID (required).
     * @return TRUE if update is success.
     * @throws Exception
     */
    public static boolean userDeActivate(final User userInput) throws Exception {
        if(userInput == null 
                || ValidationUtil.isEmpty(userInput.getUserId())) {
            throw new IllegalArgumentException("User Deactivate error: UserID is required!");
        }
        
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        try {
            return userDaoImpl.deactivate(userInput);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("User Deactivate error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Re-activate user account. 
     * @param user - The User bean containing userID (required).
     * @return User bean if success, null otherwise.
     * @throws Exception
     */
    public static User userReActivate(final User userInput) throws Exception {
        if(userInput == null 
                || ValidationUtil.isEmpty(userInput.getUserId())) {
            throw new IllegalArgumentException("User Reactivate error: UserID is required!");
        }
        User userReactivated = null;
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        try {
            userReactivated = userDaoImpl.reactivate(userInput);
            BusinessUtil.secureUserData(userReactivated, true);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("User Reactivate error: " + e.getMessage(), e);
            throw e;
        }
        return userReactivated;
    }
    
    /**
     * Retrieves User profile (NOT a user login!) with given specified parameters.
     * NOTE: Unlike user login, this operation should return no private data but only that is public to see.
     * 
     * @param userToRetrieve - User bean that contains the input parameter to search for.
     * @param retrieveBy - Retrieve by userID or username or email
     * @param isEnrichment - If TRUE then the found User(s) object will be enhanced with populating 
     *                       it with more data (Trips, Favorites, Comments, etc.). Otherwise, it
     *                       returns a basic User data (username, fullname, etc.).
     *                      
     * @return List of Users found.
     * @throws Exception
     */
    public static List<User> userRetrieve(
            User userToRetrieve,
            ServiceTypeEnum retrieveBy,
            boolean isEnrichment,
            int offset,
            int numOfRows) throws Exception {
        if(userToRetrieve == null 
                || retrieveBy == null) {
            throw new IllegalArgumentException("User Retrieve error: User to Retrieve bean and retrieveBy are required!");
        }
        
        List<User> usersFoundList = null;
        try {
            IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
            usersFoundList = 
                    userDaoImpl.read(userToRetrieve, retrieveBy, isEnrichment, false, offset, numOfRows);
            if(usersFoundList == null || usersFoundList.size() == 0 || usersFoundList.get(0) == null) {
                return usersFoundList;
            }

            //Else: Remove any secure/private data
            for(User userFound : usersFoundList) {
                BusinessUtil.secureUserData(userFound, false);
            }
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("User Retrieve error: " + e.getMessage(), e);
            throw e;
        }
        
        return usersFoundList;
    }
    
    public static FileServerResponse userProfileImageUpload(
            final User userWithProfileImageToUpload) 
            throws Exception {
        if(userWithProfileImageToUpload == null
                || ValidationUtil.isEmpty(userWithProfileImageToUpload.getUserId())
                || userWithProfileImageToUpload.getProfileImageBytes() == null) {
            throw new IllegalArgumentException("User Profile Image Upload error: User ID and image bytes are required!");
        }
        
        //Set defaults:
        if(ValidationUtil.isEmpty(userWithProfileImageToUpload.getProfileImageName())) {
            userWithProfileImageToUpload.setProfileImageName(CommonUtil.generatePictureFilename(userWithProfileImageToUpload.getUserId()));
        }
        
        return BusinessUtil.imageUploadToServer(
                FileServerMenuEnum.WRITE_PROFILE_PICTURE,
                userWithProfileImageToUpload.getProfileImageName(),
                userWithProfileImageToUpload.getProfileImageBytes());
    }

    public static FileServerResponse userProfileImageDownload(
            final User userWithProfileImageToDownload) 
            throws Exception{
        if(userWithProfileImageToDownload == null
                || ValidationUtil.isEmpty(userWithProfileImageToDownload.getProfileImageName())) {
            throw new IllegalArgumentException("User Profile Image Download error: Image filename is required!");
        }

        return BusinessUtil.imageDownloadFromServer(
                FileServerMenuEnum.READ_PROFILE_PICTURE, 
                userWithProfileImageToDownload.getProfileImageName());
    }
    
    public static void userPasswordForget(final User userWhoForget) throws Exception {
        if(userWhoForget == null
                || ValidationUtil.isEmpty(userWhoForget.getEmail())) {
             throw new IllegalArgumentException("User Password Forget error: Email is required!");
        }
        
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        try {
            //1. Find the user given his/her email:
            User userToRetrieve = new User();
            userToRetrieve.setEmail(userWhoForget.getEmail());
            List<User> usersFound = 
                    userDaoImpl.read(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, true, true, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Password Forget error: User is not found for given email!");
            }
            
            /*
             * 2. Generate new user SecurityToken
             *    NOTE: By Cryptography design for security, any original salted-hashed passwords 
             *    stored in DB are not retrievable to their original clear-text String. Hence, we need
             *    to ask user to reset his/her own password but must provide SecurityToken to do so.
             */
            String securityToken = CommonUtil.generateSecurityToken();
            User userToUpdate = new User();
            userToUpdate.setUserId(usersFound.get(0).getUserId());
            userToUpdate.setSecurityToken(securityToken);
            userDaoImpl.update(userToUpdate);
            
            //3. Send email
            userWhoForget.setSecurityToken(securityToken);
            IEmailManager emailManager = EmailManagerFactory.getEmailManagerInstance();
            emailManager.mailUserPasswordForget(userWhoForget);

        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("User Password Forget error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static User userPasswordReset(final User userWhoForget) throws Exception {
        if(userWhoForget == null
                || ValidationUtil.isEmpty(userWhoForget.getEmail())
                || ValidationUtil.isEmpty(userWhoForget.getSecurityToken())
                || ValidationUtil.isEmpty(userWhoForget.getPassword())) {
             throw new IllegalArgumentException("User Password Reset error: Email, security token and the new password are required!");
        }
        
        IUserDAO userDaoImpl = DAOFactory.getUserDAOInstance();
        User userLogin = null;
        try {
            //1. Find the user given his/her email:
            User userToRetrieve = new User();
            userToRetrieve.setEmail(userWhoForget.getEmail());
            List<User> usersFound = 
                    userDaoImpl.read(userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, true, true, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Password Reset error: User is not found for given email!");
            }
            userLogin = usersFound.get(0);
            
            //2. Validate user SecurityToken
            if(ValidationUtil.isEmpty(userLogin.getSecurityToken())) {
                throw new IllegalArgumentException("User Password Reset error: The given SecurityToken has been used to reset user password, or has expired!");
            }
            if(!userWhoForget.getSecurityToken().trim().equalsIgnoreCase(userLogin.getSecurityToken().trim())) {
                throw new IllegalArgumentException("User Password Reset error: The given SecurityToken is invalid and does not match the given user account!");
            }
            
            //3. Validate new password:
            User userToUpdate = new User();
            userToUpdate.setUserId(userLogin.getUserId());
            userToUpdate.setPassword(userWhoForget.getPassword());
            userToUpdate.setSecurityToken("");
            userToUpdate = validateAndProcessPassword(userToUpdate);

            //4. Update:
            userDaoImpl.update(userToUpdate);
            
            //5. Validate user active status:
            if(userLogin.getActive() == YesNoEnum.NO) {
                User userDeactivated = new User();
                userDeactivated.setUserId(userLogin.getUserId());
                userDeactivated.setUsername(userLogin.getUsername());
                userDeactivated.setActive(YesNoEnum.NO);
                userDeactivated.setUpdated(userLogin.getUpdated());
                return userDeactivated;
            }
            
            //6. Return User as a logged in user:
            BusinessUtil.secureUserData(userLogin, true);
            return userLogin;    
            
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(UserNotFoundException unfe) {
            throw unfe;
        } catch(Exception e) {
            LOGGER.error("User Password Reset error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static void userContactUs(final User userWhoContactUs) throws Exception {
        IEmailManager emailManager = EmailManagerFactory.getEmailManagerInstance();
        emailManager.mailUserContactUs(userWhoContactUs);
    }
    
    /*
     * Process password including decrypt, validate, salt-n-hash
     */
    private static User validateAndProcessPassword(User userInput) {
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            String pwdDecrypted = cryptoInstance.decrypt(userInput.getPassword());
            ValidationUtil.validatePassword(pwdDecrypted, true);
            
            String pwdSaltedAndHashed = cryptoInstance.saltAndHash(pwdDecrypted);
            userInput.setPassword(pwdSaltedAndHashed);
        } catch(IllegalArgumentException ile) {
            throw ile;
            
        } catch(Exception e) {
            LOGGER.error("validateAndProcessPassword() - ERROR: During password cryptography! " + e.getMessage(), e);
            throw new IllegalArgumentException("User error: Password invalid format, cryptography failure!");
        }
        
        return userInput;
    }
    
    /*
     * To authenticate incoming request password:
     * 1. User in request - Decrypt incoming password first
     * 2. User in DB - Its password must have been salted-n-hashed
     * 3. Then, compare them against each other.
     */
    private static void authenticatePassword(User userInRequest, User userInDB) throws Exception {
        boolean isAuthenticated = false;
        try {
            Cryptographer cryptoInstance = Cryptographer.getInstance();
            String pwdDecrypted = cryptoInstance.decrypt(userInRequest.getPassword());
            
            isAuthenticated = cryptoInstance.verifySaltAndHash(pwdDecrypted, userInDB.getPassword());
        } catch(Exception e) {
            LOGGER.error("Error during password cryptography: " + e.getMessage());
            e.printStackTrace();
            throw new IllegalArgumentException("Password authentication error due to cryptography failure!");
        }
        
        if(!isAuthenticated) {
            throw new UserNotFoundException("User is not found for given username and password!");
        }
    }
    
    private static void createUserAuthToken(User userInput, User userLogin) {
        if(userInput.getUserAuthToken() != null) {
            UserAuthToken userAuthTokenToCreate = userInput.getUserAuthToken();
            userAuthTokenToCreate.setUserId(userLogin.getUserId());
            userAuthTokenToCreate.setUsername(userLogin.getUsername().toLowerCase());
            try {
                UserAuthToken userAuthTokenCreated = UserAuthTokenManager.create(userAuthTokenToCreate);
                userLogin.setUserAuthToken(userAuthTokenCreated);
                
                //Populate the list to include the newly created one:
                List<UserAuthToken> listOfUserAuthTokens = 
                        UserAuthTokenManager.retrieve(userAuthTokenToCreate, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID);
                userLogin.setListOfAuthTokens(listOfUserAuthTokens);
                
            } catch(Exception e) {
                //Log the error, but then ignore
                LOGGER.error("Error during attempt to create new UserAuthToken: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }
    
    private static void createOrUpdateUserProfileImage(User userToEdit, IUserDAO userDaoImpl) throws Exception {
        if(userToEdit.getProfileImageBytes() == null) {
            return;
        }
        
        //Retrieve user:
        List<User> usersFound = 
                userDaoImpl.read(userToEdit, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, false, true, 0, 1);
        if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
            return;
        }
        User userFound = usersFound.get(0);
        
        //Validate if user has existing profile image:
        boolean isHasExistingProfileImage = false;
        String existingImageName = null;
        if(!ValidationUtil.isEmpty(userFound.getProfileImageName())) {
            isHasExistingProfileImage = true;
            existingImageName = userFound.getProfileImageName();
        }
        
        //Create new image on FS
        try {
            FileServerResponse fsResponse = userProfileImageUpload(userToEdit);
            if(!fsResponse.isSuccess()) {
                LOGGER.error("Failed to create new profile image for user [" 
                        +  userToEdit.getUserId() 
                        + "] on FileServer with error: " + fsResponse.getServiceError());
                
                /*
                 * TODO (8/2016): Image format not supported
                 * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
                 * with error: "Invalid argument to native writeImage". The issue is JRE being used by
                 * the server if it is  OpenJDK, it has this bug. Solution is to either change the JRE or use
                 * GIF plug-in, including to support GIF animation.
                 */
                if(fsResponse.getServiceError() != null 
                        && fsResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_FORMAT_NOT_SUPPORTED) {
                    throw new ImageFormatNotSupportedException("User Update error: Image format to create is not supported!");
                }
                
                //Else:
                throw new Exception("User Update error: Failed to create the profile picture file on FileServer with error: " + fsResponse.getServiceError());
            }
            
            //Update data:
            userToEdit.setProfileImageName(fsResponse.getFilename());
            
        } finally {
            //Clean resources:
            userToEdit.setProfileImageBytes(null);
        }
        
        //Delete old image from FS if any
        if(isHasExistingProfileImage) {
            FileServerRequest requestToDelete = new FileServerRequest();
            requestToDelete.setFileMenu(FileServerMenuEnum.DELETE_PROFILE_PICTURE);
            requestToDelete.setFilename(existingImageName);
            IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
            FileServerResponse fsResponse = fsClient.delete(requestToDelete);
            if(!fsResponse.isSuccess()) {
                //Ignore but just log it:
                LOGGER.error("createOrUpdateUserProfileImage() - Failed to delete previous existing image filename [" 
                        +  existingImageName
                        + "] on FileServer with error: " + fsResponse.getServiceError());
            }
        }
    }
    
    
}
