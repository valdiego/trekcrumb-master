package com.trekcrumb.business.manager;

import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.dao.DAOFactory;
import com.trekcrumb.business.dao.IPictureDAO;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceErrorEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.ImageFormatNotSupportedException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.fileserverclient.FileServerClientFactory;
import com.trekcrumb.fileserverclient.IFileServerClient;

/**
 * Orchestration service for Picture management that communicate/integrate multiple component layers 
 * (DB, file system, etc.).
 *
 */
public class PictureManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.manager.PictureManager"); 

    /**
     * Creates a new Picture.
     * 
     * Note: 
     * 1. This operation is mostly for adding a new Picture to an existing Trip
     * 2. It will set defaults to the new Picture ID, filename, created/updated dates, etc. 
     *    If any client/caller has set these values, it will be overwritten with server's values. This
     *    is done for consistency, no matter who the clients are and how they set these values. 
     * 3. To create a Picture that is previously created on user local device, see TripManager.publish()
     *    
     * @return The newly created Picture.
     */
    public static Picture create(
            final Picture pictureToCreate) 
            throws Exception {
        if(ValidationUtil.isEmpty(pictureToCreate.getTripID())
                || ValidationUtil.isEmpty(pictureToCreate.getUserID())
                || pictureToCreate.getImageBytes() == null) {
            throw new IllegalArgumentException("Picture Create error: User ID, Trip ID, image bytes are required!");
        }
        
        //Configure defaults as necessary:
        Picture pictureCreated = pictureToCreate.clone();
        pictureCreated.setId(CommonUtil.generateID(pictureCreated.getTripID()));
        
        String filename = pictureToCreate.getImageName();
        if(ValidationUtil.isEmpty(filename)) {
            filename = CommonUtil.generatePictureFilename(pictureCreated.getTripID());
            pictureCreated.setImageName(filename);
        }
        
        pictureCreated.setNote(BusinessUtil.neutralizeStringValue(pictureCreated.getNote(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE));
        if(pictureCreated.getCoverPicture() == null) {
            pictureCreated.setCoverPicture(YesNoEnum.NO);
        }
        pictureCreated.setCreated(DateUtil.getCurrentTimeWithGMTTimezone());
        pictureCreated.setUpdated(pictureCreated.getCreated());
        
        //1. Save into filesystem:
        FileServerResponse fsResponse = BusinessUtil.imageUploadToServer(
                FileServerMenuEnum.WRITE_TREK_PICTURE,
                pictureCreated.getImageName(),
                pictureCreated.getImageBytes());
        if(!fsResponse.isSuccess()) {
            /*
             * TODO (8/2016): Image format not supported
             * Known issue when image format is GIF, java ImageIO.write() API would throw IIOException 
             * with error: "Invalid argument to native writeImage". The issue is JRE being used by
             * the server if it is  OpenJDK, it has this bug. Solution is to either change the JRE or use
             * GIF plug-in, including to support GIF animation.
             */
            if(fsResponse.getServiceError() != null 
                    && fsResponse.getServiceError().getErrorEnum() == ServiceErrorEnum.PICTURE_SERVICE_IMAGE_FORMAT_NOT_SUPPORTED) {
                throw new ImageFormatNotSupportedException("Picture Create error: Image format to create is not supported!");
            }
            
            //Else:
            throw new Exception("Picture Create error: Failed to create the picture file on FileServer with error: " + fsResponse.getServiceError());
        }
        
        //2. Save into DB:
        try {
            IPictureDAO picDAOImpl = DAOFactory.getPictureDAOInstance();
            picDAOImpl.create(pictureCreated);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Picture Create error: " + e.getMessage(), e);
            throw e;
        }

        //Help save memory resources by setting imageBytes to null (no longer used):
        pictureCreated.setImageBytes(null);
        return pictureCreated;
    }
    
    /**
     * Retrieve pictures given the selection criteria ('retrieveBy').
     * 
     * @param pictureToRetrieve - Contains required data for the chosen criteria (such as Picture ID,
     *                            User ID, etc.) 
     * @param retrieveBy - The selection criteria.
     * @return List of Pictures found. NULL if nothing found.
     *         NOTE: The returned bean will contain image name (and image URL, if any), 
     *         but NOT the image bytes. This is to avoid transfering huge data over the network 
     *         if the list contains many pictures.
     * @throws Exception
     */
    public static List<Picture> retrieve(
            Picture pictureToRetrieve,
            ServiceTypeEnum retrieveBy,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID) {
            //Only for internal use; we do not allow outside to access this operation:
            throw new OperationNotSupportedException("Illegal Access!");
        }
        
        try {
            IPictureDAO picDAOImpl = DAOFactory.getPictureDAOInstance();
            List<Picture> listOfPics = picDAOImpl.read(
                    pictureToRetrieve, retrieveBy, null, orderBy, 
                    offset, numOfRows);
            if(listOfPics == null || listOfPics.size() == 0) {
                return null;
            }
            return listOfPics;
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Picture Retrieve error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static int count(
            Picture pictureToCount,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        if(pictureToCount == null || retrieveBy == null) {
            throw new IllegalArgumentException("Picture Count error: Picture to count and retrieve-by are required!");
        }
        try {
            IPictureDAO dAOImpl = DAOFactory.getPictureDAOInstance();
            return dAOImpl.count(pictureToCount, retrieveBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Picture Count error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    
    /**
     * Updates a Picture.
     * 
     * Note:
     * 1. It will set defaults to Picture (updated date). 
     *    If any client/caller has set these values, it will be overwritten with server's values. This
     *    is done for consistency, no matter who the clients are and how they set these values. 
     *    
     * @return The updated Picture.
     */
    public static Picture update(
            Picture pictureToUpdate) 
            throws Exception{
        if(pictureToUpdate == null
                || ValidationUtil.isEmpty(pictureToUpdate.getUserID())
                || ValidationUtil.isEmpty(pictureToUpdate.getTripID())
                || ValidationUtil.isEmpty(pictureToUpdate.getId())) {
            throw new IllegalArgumentException("Picture Update error: UserID, TripID and PictureID are required!");
        }

        //Configure input as necessary:
        pictureToUpdate.setNote(BusinessUtil.neutralizeStringValue(pictureToUpdate.getNote(), true, CommonConstants.STRING_VALUE_LENGTH_MAX_PICTURE_NOTE));
        pictureToUpdate.setUpdated(DateUtil.getCurrentTimeWithGMTTimezone());
        
        //Execute DAO compoments:
        try {
            IPictureDAO picDAOImpl = DAOFactory.getPictureDAOInstance();
            return picDAOImpl.update(pictureToUpdate);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Picture Update error: " + e.getMessage(), e);
            throw e;
        }
    }
    
    public static int delete(
            Picture pictureToDelete, 
            ServiceTypeEnum deleteBy) 
            throws Exception {
        int numDeleted = 0;
        List<Picture> listOfPicturesToDelete = null;
        try {
            IPictureDAO picDAOImpl = DAOFactory.getPictureDAOInstance();

            //Find out pictures that need to be deleted:
            if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_USERID) {
                listOfPicturesToDelete = 
                        picDAOImpl.read(pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID, null, null, 0, -1);
            } else if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID) {
               listOfPicturesToDelete = 
                        picDAOImpl.read(pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, null, null, 0, -1);
            } else {
                listOfPicturesToDelete = 
                        picDAOImpl.read(pictureToDelete, ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 0, -1);
            }
            if(listOfPicturesToDelete == null || listOfPicturesToDelete.size() == 0) {
                return numDeleted;
            }

            //Delete from DB:
            numDeleted = picDAOImpl.delete(pictureToDelete, deleteBy);
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Picture Delete error: " + e.getMessage(), e);
            throw e;
        }

        //Delete from file repository:
        FileServerRequest fsRequest = new FileServerRequest();
        fsRequest.setFileMenu(FileServerMenuEnum.DELETE_TREK_PICTURE);
        IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
        for(Picture picture : listOfPicturesToDelete) {
            fsRequest.setFilename(picture.getImageName());
            fsClient.delete(fsRequest);
        }

        return numDeleted;
    }
    
    public static FileServerResponse imageUpload(
            final Picture pictureWithImageToUpload) 
            throws Exception {
        if(pictureWithImageToUpload == null
                || ValidationUtil.isEmpty(pictureWithImageToUpload.getImageName())
                || pictureWithImageToUpload.getImageBytes() == null) {
            throw new IllegalArgumentException("Picture Image Upload error: Image filename and content bytes are required!");
        }

        return BusinessUtil.imageUploadToServer(
                FileServerMenuEnum.WRITE_TREK_PICTURE,
                pictureWithImageToUpload.getImageName(),
                pictureWithImageToUpload.getImageBytes());
    }
    
    public static FileServerResponse imageDownload(
            final Picture pictureWithImageToDownload) 
            throws Exception {
        if(pictureWithImageToDownload == null
                || ValidationUtil.isEmpty(pictureWithImageToDownload.getImageName())) {
            throw new IllegalArgumentException("Picture Image Download error: Image filename is required!");
        }
        
        return BusinessUtil.imageDownloadFromServer(
                FileServerMenuEnum.READ_TREK_PICTURE, 
                pictureWithImageToDownload.getImageName());
    }
    


}
