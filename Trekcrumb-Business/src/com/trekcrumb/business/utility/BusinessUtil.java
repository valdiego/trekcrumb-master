package com.trekcrumb.business.utility;

import java.text.Normalizer;
import java.util.List;

import com.trekcrumb.common.bean.FileServerRequest;
import com.trekcrumb.common.bean.FileServerResponse;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.FileServerMenuEnum;
import com.trekcrumb.common.utility.ValidationUtil;
import com.trekcrumb.fileserverclient.FileServerClientFactory;
import com.trekcrumb.fileserverclient.IFileServerClient;

public class BusinessUtil {
    
    /**
     * Securing User bean from any sensitive data.
     */
    public static User secureUserData(User user, boolean isUserLogin) {
        user.setPassword(null);
        user.setSecurityToken(null);
        user.setUsername(user.getUsername().toLowerCase());
        
        if(isUserLogin) {
            user.setEmail(user.getEmail().toLowerCase());
            
            /*
             * If UserAuthToken available: We would return most of the record property EXCEPT, 
             * for security reason, AUTH_TOKEN values. The auth_token values are used for 
             * authentication and we should not return it to the caller. When the caller 
             * asked for authentication where auth_token input param is required, the caller must 
             * have had it in their system (device DB or browser cookie).
             */
            List<UserAuthToken> listOfUserAuthTokens = user.getListOfAuthTokens();
            for(UserAuthToken userAuthToken : listOfUserAuthTokens) {
                userAuthToken.setAuthToken(null);
            }
            
        } else {
            user.setEmail(null);

            /*
             * TODO (2/2016): To remove userID, the impact would be everywhere incl. unit tests.
             * Thus, we have one specific task to do this in our project tasks.
             */
            //user.setUserId(null);
        }
        
        return user;
    }
    
    /**
     * Neutralizes an input String (name, notes, etc.) for any invalidity:
     * 1. For DB, a single quote ' char is invalid, therefore it will be neutralizes into \' to 
     *    prevent DOA invalid closing quote exception
     * 2. For certain String contents, a new line is not allowed thus removed. This applies
     *    only if isRemoveNewLine param is TRUE.
     * 3. For non-ASCII chars, disable it to prevent DOA invalid char exception.
     * 4. For String contents whose length is bigger than maximum allowed, we must truncate it
     *    to prevent DOA maximum length exceeded exception.
     *    
     * @param stringOrig
     * @param isRemoveNewLine: TRUE if any new line should be removed. Otherwise, FALSE to keep it.
     * @paam maxLength: The maximum length of the String allowed and anything bigger will be
     *                  truncated. If maxLength value is <= zero, it will be ignored.
     * @return A new String with all special characters neutralized.
     */
    public static String neutralizeStringValue(
            String stringOrig, 
            boolean isRemoveNewLine,
            int maxLength) {
        if(ValidationUtil.isEmpty(stringOrig)) {
            return stringOrig;
        }
        
        //Single quote:
        //String neutralizedString = stringWithChars.replace("\\", "\\\\");
        String stringNeutralized = stringOrig.replace("'", "\'");
        
        //New line:
        if(isRemoveNewLine) {
            stringNeutralized = stringNeutralized.replace("\r\n", " ");
            stringNeutralized = stringNeutralized.replace("\r", " ");
            stringNeutralized = stringNeutralized.replace("\n", " ");
        }
        
        //Non-ASCII chars:
        if(isStringHasNonASCIIChars(stringNeutralized)) {
            stringNeutralized = neutralizeNonASCIIChars(stringNeutralized);
        }
        
        //Max length:
        if(maxLength > 0 && stringNeutralized.length() > maxLength) {
            stringNeutralized = stringNeutralized.substring(0, maxLength);
        }
        
        return stringNeutralized;
    }
    
    /*
     * Verifies to see if the input String has non-ASCII character.
     * NOTE:
     * 1. In Java, Strings are conceptually encoded as UTF-16 where ASCII character is encoded as the 
     *    values 0 - 127. Any non-ASCII character, which may consist of more than one Java char,
     *    is encoded not to include the numbers 0 - 127 guaranteed.
     * 2. REF: http://stackoverflow.com/questions/3585053/in-java-is-it-possible-to-check-if-a-string-is-only-ascii
     */
    private static boolean isStringHasNonASCIIChars(String stringOrig) {
        boolean isIt = false;
        int stringSize = stringOrig.length();
        for (int i = 0; i < stringSize; i++) {
            int charInQuestion = stringOrig.charAt(i);
            if (charInQuestion > 0x7F) {
                isIt = true;
                break;
            }
        }
        
        return isIt;
    }

    /*
     * Neutralizes String with non-ASCII characters
     * NOTE:
     * 1. Rather than replacing non-ASCII with empty String or '?', we try to replace it with 
     *    ASCII-equivalent when possible. Ex: "swedish umlaut chars of oau" will be mapped to regular ASCII "oau"
     * 2. Use java.text.Normalizer API where it will separate all of the accent marks from the characters. 
     *    Then, we just need to compare each character against being a letter and throw out the ones that aren't.
     * 3. We may use REGEX such as:  String resultString = subjectString.replaceAll("[^\\x00-\\x7F]", "");
     *    BUT using a regular FOR-LOOP is said to be incredibly faster (20-40x)
     * 4. REF: 
     *    -> http://stackoverflow.com/questions/3322152/is-there-a-way-to-get-rid-of-accents-and-convert-a-whole-string-to-regular-lette
     */
    private static String neutralizeNonASCIIChars(String stringOrig) {
        StringBuilder flattenStringBuilder = new StringBuilder(stringOrig.length());
        stringOrig = Normalizer.normalize(stringOrig, Normalizer.Form.NFD);
        for(char charInQuestion : stringOrig.toCharArray()) {
            if (charInQuestion <= '\u007F') {
                flattenStringBuilder.append(charInQuestion);
            }
        }
        
        return flattenStringBuilder.toString();        
    }
    
    public static FileServerResponse imageUploadToServer(
            FileServerMenuEnum fileServerMenu,
            String imageFilename,
            byte[] imageBytes) {
        if(fileServerMenu == null
                || (fileServerMenu != FileServerMenuEnum.WRITE_TREK_PICTURE && fileServerMenu != FileServerMenuEnum.WRITE_PROFILE_PICTURE)) {
            throw new IllegalArgumentException("FileServerMenuEnum must be either WRITE_TREK_PICTURE or WRITE_PROFILE_PICTURE!");
        }
        
        FileServerRequest fsRequest = new FileServerRequest();
        fsRequest.setFileMenu(fileServerMenu);
        fsRequest.setFilename(imageFilename);
        fsRequest.setFileBytes(imageBytes);
        IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
        FileServerResponse fsResponse = fsClient.write(fsRequest);
        if(!fsResponse.isSuccess()) {
            System.err.println("BusinessUtil.imageUploadToServer() - Failed to upload image to server with ERROR: " + fsResponse.getServiceError());
        }
        
        return fsResponse;
    }
    
    public static FileServerResponse imageDownloadFromServer(
            FileServerMenuEnum fileServerMenu,
            String imageFilename) {
        if(fileServerMenu == null
                || (fileServerMenu != FileServerMenuEnum.READ_TREK_PICTURE && fileServerMenu != FileServerMenuEnum.READ_PROFILE_PICTURE)) {
            throw new IllegalArgumentException("FileServerMenuEnum must be either READ_TREK_PICTURE or READ_PROFILE_PICTURE!");
        }

        FileServerRequest fsRequest = new FileServerRequest();
        fsRequest.setFileMenu(fileServerMenu);
        fsRequest.setFilename(imageFilename);
        IFileServerClient fsClient = FileServerClientFactory.getFileServerClientInstance();
        FileServerResponse fsResponse = fsClient.read(fsRequest);
        if(!fsResponse.isSuccess()) {
            System.err.println("BusinessUtil.imageDownloadFromServer() - Failed to upload image to server with ERROR: " + fsResponse.getServiceError());
        }
        
        return fsResponse;
    }
    
    
}
