package com.trekcrumb.business.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BusinessProperties {
    private static final String PROPERTIES_FILE_NAME = "trekcrumb.business.properties";
    
    private static Properties mProperties = null;

    static {
        loadPropertiesFromFile();
    }
    
    /**
     * Disallows instantiation of this static class. 
     */
    private BusinessProperties() {}

    /**
     * Loads the property file.
     *  
     * Note: Initially, loads properties from System. Then, if properties file is found,
     * reload them from the file.
     */
    public static void loadPropertiesFromFile() {
        if (mProperties == null) {
            mProperties = System.getProperties();
        }
        InputStream inputStream = null;
        try {
            ClassLoader classLoader = BusinessProperties.class.getClassLoader();
            inputStream = classLoader.getResourceAsStream(PROPERTIES_FILE_NAME);
            if(inputStream != null) {
                mProperties.load(inputStream);
            }
        } catch(IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    //Ignore
                }
            }
        }
    }
    
    /**
     * Returns requested property value given its key.
     * @exception Exception is thrown if property not found. 
     */
    public static String getProperty(String key) {
        if(mProperties == null) {
            loadPropertiesFromFile();
        }
        
        String value = mProperties.getProperty(key);
        if(value == null) {
            String msg = "Cannot find property from [" + PROPERTIES_FILE_NAME + "] given its key [" + key + "]!";
            throw new IllegalArgumentException(msg);
        }
        return value;
    }        
}
