package com.trekcrumb.business.utility;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.CommonProperties;
import com.trekcrumb.common.utility.ValidationUtil;

public class MailUtil {
    
    public static String readEmailTemplate(String fileName) throws Exception {
        StringBuffer contents = null;
        BufferedReader reader =  null;
        InputStream inputStream = null;
        try {
            contents = new StringBuffer();
            inputStream = MailUtil.class.getClassLoader().getResourceAsStream(fileName);
            if(inputStream == null) {
                throw new IllegalArgumentException("ERROR: Cannot find specified email template file: " + fileName);
            }
            
            reader = new BufferedReader(new InputStreamReader(inputStream));            

            //Read one line at a time using Buffering:
            String line = null; 
            while (( line = reader.readLine()) != null){
                contents.append(line);
                contents.append(System.getProperty("line.separator"));
            }
            
            return contents.toString();
        } finally {
            if(reader != null) {
                reader.close();
            }
            if(inputStream != null) {
                inputStream.close();
            }
            contents = null;
        }
    }
    
    /**
     * Composes email template for User Password Forget.
     * It is used ONLY IF reading template file fails.
     */
    public static String composeEmailTemplate_userPasswordForget() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("We received a request to reset the password for a Trekcrumb account associated with this email address. If you did not make this request, you can safely ignore and delete this email.");
        strBuilder.append("\n");
        strBuilder.append("If you made this request, this is your temporary Security Token: ");
        strBuilder.append(BusinessConstants.EMAIL_TEMPLATE_KEY_SECURITY_TOKEN);
        strBuilder.append("\n");
        strBuilder.append("To reset your password, click the link below or copy-paste it into your browser, and follow the instructions on the web page: ");
        strBuilder.append(CommonProperties.getProperty("webapp.url.user.reset"));
        strBuilder.append("Trekcrumb will never send email to ask you to disclose any of your password, credit cards, or banking accounts. If you receive a suspicious email with a link to update your account information, do not click on the link! Instead, report the email to Trekcrumb for investigation.");
        strBuilder.append("\n");
        strBuilder.append("Thank you from Trekcrumb Team.");
        strBuilder.append("\n\n");
        strBuilder.append("--------------------------------------- \n");
        strBuilder.append("This is an automated email. Please do not reply to this email address.");
        strBuilder.append("This message may contain confidential and privileged information. If you are not the intended addressee, please notify us immediately and delete the message from your computer. ");
        strBuilder.append("Any use, copy or disclosure of the message or any information contained therein to anyone for any purpose other than as permitted is strictly prohibited.");
        strBuilder.append("Trekcrumb does not guarantee and is not liable for the security of any information electronically transmitted or for the proper and complete transmission of the information contained in this communication, or for any delay in its receipt.   ");
        return strBuilder.toString();
    }

    /**
     * Composes email template for User ContactUs to Admin.
     * It is used ONLY IF reading template file fails.
     */
    public static String composeEmailTemplate_userContactUsToAdmin() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("We received a message via Contact Us!\n");
        strBuilder.append("User email: " + BusinessConstants.EMAIL_TEMPLATE_KEY_USER_EMAIL);
        strBuilder.append("\n");
        strBuilder.append("Fullname: " + BusinessConstants.EMAIL_TEMPLATE_KEY_USER_FULLNAME);
        strBuilder.append("\n");
        strBuilder.append("Summary:\n");
        strBuilder.append(BusinessConstants.EMAIL_TEMPLATE_KEY_USER_SUMMARY);
        strBuilder.append("\n\n");
        strBuilder.append("--------------------------------------- \n");
        strBuilder.append("This is an automated email. Please do not reply to this email address.");
        return strBuilder.toString();
    }
    
    /**
     * Composes email template for User ContactUs to User.
     * It is used ONLY IF reading template file fails.
     */
    public static String composeEmailTemplate_userContactUsToUser() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("Thank you for contacting us! Our Trekcrumb team will review your message. Your care and attention are important to us. \n");
        strBuilder.append("Trekcrumb Team");
        strBuilder.append("\n\n");
        strBuilder.append("Your original message is below. \n");
        strBuilder.append("--------------------------------------- \n");
        strBuilder.append(BusinessConstants.EMAIL_TEMPLATE_KEY_USER_SUMMARY);
        strBuilder.append("\n\n");
        strBuilder.append("--------------------------------------- \n");
        strBuilder.append("This is an automated email. Please do not reply to this email address.");
        return strBuilder.toString();
    }
    
    public static MimeMessage generateEmail_userPasswordForget(
            Session mailSession,
            User userWhoForget) 
            throws MessagingException {
        String emailBody = BusinessConstants.EMAIL_TEMPLATE_USER_PASSWORD_FORGET.replace(
                BusinessConstants.EMAIL_TEMPLATE_KEY_SECURITY_TOKEN, 
                userWhoForget.getSecurityToken());
        
        MimeMessage msg = new MimeMessage(mailSession);
        msg.setFrom(BusinessConstants.MAILSERVER_ADDRESS_FROM_DEFAULT);
        msg.setRecipients(Message.RecipientType.TO, userWhoForget.getEmail());
        msg.setSubject(BusinessConstants.EMAIL_SUBJECT_USER_PASSWORD_FORGET);
        msg.setSentDate(new Date());
        msg.setText(emailBody);
        
        return msg;
    }
    
    public static MimeMessage generateEmail_contactUsToAdmin(
            Session mailSession, 
            User userWhoContactUs) 
            throws MessagingException {
        String emailBodyToAdmin = BusinessConstants.EMAIL_TEMPLATE_USER_CONTACT_US_TO_ADMIN.replace(
                BusinessConstants.EMAIL_TEMPLATE_KEY_USER_EMAIL, 
                userWhoContactUs.getEmail());
        
        if(!ValidationUtil.isEmpty(userWhoContactUs.getFullname())) {
            emailBodyToAdmin = emailBodyToAdmin.replace(BusinessConstants.EMAIL_TEMPLATE_KEY_USER_FULLNAME, userWhoContactUs.getFullname());
        } else {
            emailBodyToAdmin = emailBodyToAdmin.replace(BusinessConstants.EMAIL_TEMPLATE_KEY_USER_FULLNAME, "N/A");
        }
        emailBodyToAdmin = emailBodyToAdmin.replace(BusinessConstants.EMAIL_TEMPLATE_KEY_USER_SUMMARY, userWhoContactUs.getSummary());
        MimeMessage msg = new MimeMessage(mailSession);
        msg.setFrom(BusinessConstants.MAILSERVER_ADDRESS_FROM_DEFAULT);
        msg.setRecipients(Message.RecipientType.TO, BusinessConstants.MAILSERVER_ADDRESS_TO_CONTACT_US);

        //Subject
        if(userWhoContactUs.getSummary() != null && userWhoContactUs.getSummary().length() > 25) {
            msg.setSubject(BusinessConstants.EMAIL_SUBJECT_USER_CONTACTUS_TO_ADMIN + userWhoContactUs.getSummary().substring(0, 25));
        } else {
            msg.setSubject(BusinessConstants.EMAIL_SUBJECT_USER_CONTACTUS_TO_ADMIN + userWhoContactUs.getSummary());
        }

        msg.setSentDate(new Date());
        msg.setText(emailBodyToAdmin);

        return msg;
    }
    
    public static MimeMessage generateEmail_contactUsToUser(
            Session mailSession,
            User userWhoContactUs) 
            throws MessagingException {
        String emailBodyToUser = BusinessConstants.EMAIL_TEMPLATE_USER_CONTACT_US_TO_USER.replace(
                BusinessConstants.EMAIL_TEMPLATE_KEY_USER_SUMMARY, userWhoContactUs.getSummary());

        MimeMessage msg = new MimeMessage(mailSession);
        msg.setFrom(BusinessConstants.MAILSERVER_ADDRESS_FROM_DEFAULT);
        msg.setRecipients(Message.RecipientType.TO, userWhoContactUs.getEmail());
        msg.setSubject(BusinessConstants.EMAIL_SUBJECT_USER_CONTACTUS_TO_USER);
        msg.setSentDate(new Date());
        msg.setText(emailBodyToUser);

        return msg;
    }
    
    public static MimeMessage generateEmail_testSuiteResults(
            Session mailSession, 
            String testResultSummary) 
            throws MessagingException {
        MimeMessage msg = new MimeMessage(mailSession);
        msg.setFrom(BusinessConstants.MAILSERVER_ADDRESS_FROM_DEFAULT);
        msg.setRecipients(Message.RecipientType.TO, BusinessConstants.MAILSERVER_ADDRESS_TO_TESTADMIN);
        msg.setSubject(BusinessConstants.EMAIL_SUBJECT_TEST_RESULT);
        msg.setSentDate(new Date());
        msg.setText(testResultSummary);
        
        return msg;
    }


    

}
