package com.trekcrumb.business.utility;

public class BusinessConstants {
    
    /*
     * EMAIL SERVER
     * 1. WARNING: Some of these constants such as EMAIL_SUBJECT may be used by Email admin rule 
     *    setting to filter emails. Be cautious before changing any of the constant values.
     */
    public static final String EMAIL_MANAGER_INSTANCE = BusinessProperties.getProperty("mailserver.manager");    
    public static final String EMAIL_MANAGER_INSTANCE_GMAIL = "GMAIL";
    public static final String EMAIL_MANAGER_INSTANCE_MAILGUN = "MAILGUN";
    public static final String MAILSERVER_USERNAME = BusinessProperties.getProperty("mailserver.username");
    public static final String MAILSERVER_PASSWORD = BusinessProperties.getProperty("mailserver.password");
    public static final String MAILSERVER_ADDRESS_FROM_DEFAULT = "donotreply@trekcrumb.com";
    public static final String MAILSERVER_ADDRESS_TO_CONTACT_US = BusinessProperties.getProperty("mailserver.address.to.contactUs");
    public static final String MAILSERVER_ADDRESS_TO_TESTADMIN = BusinessProperties.getProperty("mailserver.address.to.testAdmin");
    public static final String EMAIL_SUBJECT_USER_PASSWORD_FORGET = "Trekcrumb Notice: You forgot your password";
    public static final String EMAIL_SUBJECT_USER_CONTACTUS_TO_ADMIN = "Trekcrumb Contact Us Notice: ";
    public static final String EMAIL_SUBJECT_USER_CONTACTUS_TO_USER = "Trekcrumb Notice: Thank you for Contacting Us";
    public static final String EMAIL_SUBJECT_USER_CHANGE_ACCOUNT = "Trekcrumb Notice: You changed your account";
    public static final String EMAIL_SUBJECT_TEST_RESULT = "Trekcrumb Alert: Automated Test Suite Results";

    //EMAIL CONTENTS:
    public static final String EMAIL_TEMPLATE_KEY_SECURITY_TOKEN = "${SECURITY_TOKEN}";
    public static final String EMAIL_TEMPLATE_KEY_USER_EMAIL = "${USER_EMAIL}";
    public static final String EMAIL_TEMPLATE_KEY_USER_FULLNAME = "${USER_FULLNAME}";
    public static final String EMAIL_TEMPLATE_KEY_USER_SUMMARY = "${USER_SUMMARY}";
    public static String EMAIL_TEMPLATE_USER_PASSWORD_FORGET = null;
    public static String EMAIL_TEMPLATE_USER_CONTACT_US_TO_ADMIN = null;
    public static String EMAIL_TEMPLATE_USER_CONTACT_US_TO_USER = null;
    public static String EMAIL_TEMPLATE_USER_CHANGE_ACCOUNT = null;
    static {
        if(EMAIL_TEMPLATE_USER_PASSWORD_FORGET == null) {
            try {
                EMAIL_TEMPLATE_USER_PASSWORD_FORGET = MailUtil.readEmailTemplate("resources/userPasswordForget.email");
            } catch(Exception e) {
                System.err.println("BusinessConstants - ERROR: When reading [resources/userPasswordForget.email]! Message: " + e.getMessage());
                e.printStackTrace();
                EMAIL_TEMPLATE_USER_PASSWORD_FORGET = MailUtil.composeEmailTemplate_userPasswordForget();
            }
        }
        
        if(EMAIL_TEMPLATE_USER_CONTACT_US_TO_ADMIN == null) {
            try {
                EMAIL_TEMPLATE_USER_CONTACT_US_TO_ADMIN = MailUtil.readEmailTemplate("resources/userContactUsToAdmin.email");
            } catch(Exception e) {
                System.err.println("BusinessConstants - ERROR: When reading [resources/userContactUsToAdmin.email]! Message: " + e.getMessage());
                e.printStackTrace();
                EMAIL_TEMPLATE_USER_CONTACT_US_TO_ADMIN = MailUtil.composeEmailTemplate_userContactUsToAdmin();
            }
        }

        if(EMAIL_TEMPLATE_USER_CONTACT_US_TO_USER == null) {
            try {
                EMAIL_TEMPLATE_USER_CONTACT_US_TO_USER = MailUtil.readEmailTemplate("resources/userContactUsToUser.email");
            } catch(Exception e) {
                System.err.println("BusinessConstants - ERROR: When reading [resources/userContactUsToUser.email]! Message: " + e.getMessage());
                e.printStackTrace();
                EMAIL_TEMPLATE_USER_CONTACT_US_TO_USER = MailUtil.composeEmailTemplate_userContactUsToUser();
            }
        }
    }
    

}
