package com.trekcrumb.business.email;

import com.trekcrumb.common.bean.User;

public interface IEmailManager {
    
    public void mailUserPasswordForget(User userWhoForget) throws Exception;
    public void mailUserContactUs(User userWhoContactUs) throws Exception;
    public void mailTestResults(String testResultSummary) throws Exception;
}
