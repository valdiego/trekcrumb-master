package com.trekcrumb.business.email;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.mail.smtp.SMTPTransport;
import com.trekcrumb.business.utility.BusinessConstants;
import com.trekcrumb.business.utility.MailUtil;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Manager to send emails with MAILGUN SMTP/Relay server.
 * 1. Must have valid Mailgun account for authentication. 
 * 2. This service emphasizes on "RELAY" to another 3rd party email for the "from/reply-to". No
 *    email storage in the server.
 * 2. Option to use SMTP protocol (Non-SSL) or WebService (preferred, supports TLS and more advance)
 * 3. Works on any remote server
 * 4. Limits emails send per day to: 10000 (maximum free, afterwards pay per email)
 * 
 * Ref:
 * 1. http://www.mailgun.com/
 * 2. https://documentation.mailgun.com/quickstart-sending.html#send-with-smtp-or-api
 * 3. https://gist.github.com/stevehanson/53b8ed909098cb251aa0
 * 4. http://blog.mailgun.com/tls-connection-control/
 * 
 * @author Val Triadi
 * @since 05/2016
 */
public class EmailManagerMailgunImpl implements IEmailManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.email.EmailManagerMailgunImpl"); 
    
    public void mailUserPasswordForget(User userWhoForget) throws Exception {
        if(userWhoForget == null
                || ValidationUtil.isEmpty(userWhoForget.getEmail())
                || ValidationUtil.isEmpty(userWhoForget.getSecurityToken())) {
             throw new IllegalArgumentException("ERROR: Mail for User Password Forget - Email and SecurityToken are required!");
        }

        Session mailSession = getMailSession();
        try {
            MimeMessage msg = MailUtil.generateEmail_userPasswordForget(mailSession, userWhoForget);
            sendEmail(mailSession, msg);
        } catch (MessagingException mex) {
            //Catch to prevent exposing possible user private data from server:
            LOGGER.error("ERROR: Mail for User Password Forget message - " + mex.getMessage(), mex);
            throw new Exception("There is system error when processing 'User Password Forget' message. Please retry.");
        }
    }
    
    public void mailUserContactUs(User userWhoContactUs) throws Exception {
        if(userWhoContactUs == null
                || ValidationUtil.isEmpty(userWhoContactUs.getEmail())
                || ValidationUtil.isEmpty(userWhoContactUs.getSummary())) {
             throw new IllegalArgumentException("ERROR! Mail for Contact Us: User Email and Summary (message) are required!");
        }

        Session mailSession = getMailSession();

        //STEP 1: Send email to app Admin Team
        try {
            MimeMessage msg = MailUtil.generateEmail_contactUsToAdmin(mailSession, userWhoContactUs);
            sendEmail(mailSession, msg);
        } catch (MessagingException mex) {
            //Catch to prevent exposing possible user private data from server:
            LOGGER.error("ERROR: Mail for Contact Us message to Admin Team - " + mex.getMessage(), mex);
            throw new Exception("There is system error when processing 'Contact Us' message. Please retry.");
        }               
        
        //STEP 2: Send confirm email to user
        try {
            MimeMessage msg = MailUtil.generateEmail_contactUsToUser(mailSession, userWhoContactUs);
            sendEmail(mailSession, msg);
        } catch (MessagingException mex) {
            //Catch to prevent exposing possible user private data from server:
            LOGGER.error("ERROR: Mail for Contact Us message to User - " + mex.getMessage(), mex);
            throw new Exception("There is system error when processing 'Contact Us' message. Please retry.");
        }               
    }
    
    public void mailTestResults(String testResultSummary) throws Exception {
        Session mailSession = getMailSession();
        try {
            MimeMessage msg = MailUtil.generateEmail_testSuiteResults(mailSession, testResultSummary);
            sendEmail(mailSession, msg);
        } catch (MessagingException mex) {
            LOGGER.error("ERROR: Mail for Test Results to Admin Team - " + mex.getMessage(), mex);
        }               
    }    
    
    private Session getMailSession() throws Exception {
        /*
         * Setup connection to SMTP server
         * NOTE: Mailgun does NOT support TLS protocol, but SMTPS or a webservice.
         */
        Properties properties = new Properties();
        properties.put("mail.smtps.host","smtp.mailgun.org");
        properties.put("mail.smtps.auth", "true");
        
        /*
         * Get SMTP connection session
         * NOTE: Do NOT use Session.getDefaultInstance() since it would install and reuse 
         * a default Session in JVM for other calls. Hence, security could be an issue if we store
         * authentication object there.
         */
        return Session.getInstance(properties, null);
    }
    
    private void sendEmail(Session mailSession, MimeMessage msg) throws Exception {
        SMTPTransport smtpTransport = null;
        try {
            smtpTransport = (SMTPTransport) mailSession.getTransport("smtps");
            smtpTransport.connect(
                    "smtp.mailgun.com", 
                    BusinessConstants.MAILSERVER_USERNAME, 
                    BusinessConstants.MAILSERVER_PASSWORD);
            smtpTransport.sendMessage(msg, msg.getAllRecipients());
            LOGGER.debug("SMTP send message transport is completed. Server response: " + smtpTransport.getLastServerResponse());
        } finally {
            if(smtpTransport != null) {
                smtpTransport.close();
                smtpTransport = null;
            }
        }
    }
}
