package com.trekcrumb.business.email;

import com.trekcrumb.business.utility.BusinessConstants;
import com.trekcrumb.common.exception.TrekcrumbException;

public final class EmailManagerFactory {
    private EmailManagerFactory() {}
    
    public static IEmailManager getEmailManagerInstance() throws TrekcrumbException {
        String emailManagerImpl = BusinessConstants.EMAIL_MANAGER_INSTANCE;
        if(BusinessConstants.EMAIL_MANAGER_INSTANCE_GMAIL.equalsIgnoreCase(emailManagerImpl)) {
            return new EmailManagerGmailImpl();
                    
        } else if(BusinessConstants.EMAIL_MANAGER_INSTANCE_MAILGUN.equalsIgnoreCase(emailManagerImpl)) {
            return new EmailManagerMailgunImpl();
        }
        
        //Else:
        System.err.println("EmailManagerFactory.getEmailManagerInstance() - ERROR: Null or unknown Email Manager Instance configuration: " + emailManagerImpl);
        throw new TrekcrumbException("There is system error when processing email and message!");
    }
    

}
