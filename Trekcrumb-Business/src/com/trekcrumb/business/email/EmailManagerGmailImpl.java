package com.trekcrumb.business.email;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.business.utility.BusinessConstants;
import com.trekcrumb.business.utility.MailUtil;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Manager to send emails with GMAIL SMTP/Relay server.
 * 1. Must use valid Gmail account
 * 2. Option to use TLS protocol (safer) or SMTPS (SSL)
 * 3. Works only for user (client) local machine. Google may block any access from remote servers
 * 4. Limits emails send per day to ONLY: 99 (maximum)
 * 
 * Ref:
 * 1. https://www.digitalocean.com/community/tutorials/how-to-use-google-s-smtp-server
 * 
 * @author Val Triadi
 * @since 05/2016
 */
public class EmailManagerGmailImpl implements IEmailManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.email.EmailManagerGmailImpl"); 
    
    public void mailUserPasswordForget(User userWhoForget) throws Exception {
        if(userWhoForget == null
                || ValidationUtil.isEmpty(userWhoForget.getEmail())
                || ValidationUtil.isEmpty(userWhoForget.getSecurityToken())) {
             throw new IllegalArgumentException("ERROR: Mail for User Password Forget - Email and SecurityToken are required!");
        }

        Session mailSession = getMailSession();
        try {
            MimeMessage msg = MailUtil.generateEmail_userPasswordForget(mailSession, userWhoForget);
            Transport.send(msg);
        } catch (MessagingException mex) {
            //Catch to prevent exposing possible user private data from server:
            LOGGER.error("ERROR: Mail for User Password Forget message - " + mex.getMessage(), mex);
            throw new Exception("There is system error when processing 'User Password Forget' message. Please retry.");
        }        
    }
    
    public void mailUserContactUs(User userWhoContactUs) throws Exception {
        if(userWhoContactUs == null
                || ValidationUtil.isEmpty(userWhoContactUs.getEmail())
                || ValidationUtil.isEmpty(userWhoContactUs.getSummary())) {
             throw new IllegalArgumentException("ERROR: Mail for Contact Us: Email and Summary (message) are required!");
        }

        Session mailSession = getMailSession();

        //STEP 1: Send email to app Admin Team
        try {
            MimeMessage msg = MailUtil.generateEmail_contactUsToAdmin(mailSession, userWhoContactUs);
            Transport.send(msg);
        } catch (MessagingException mex) {
            //Catch to prevent exposing possible user private data from server:
            LOGGER.error("ERROR: Mail for Contact Us message to Admin Team - " + mex.getMessage(), mex);
            throw new Exception("There is system error when processing 'Contact Us' message. Please retry.");
        }               
        
        //STEP 2: Send confirm email to user
        try {
            MimeMessage msg = MailUtil.generateEmail_contactUsToUser(mailSession, userWhoContactUs);
            Transport.send(msg);
        } catch (MessagingException mex) {
            //Catch to prevent exposing possible user private data from server:
            LOGGER.error("ERROR: Mail for Contact Us message to User - " + mex.getMessage(), mex);
            throw new Exception("There is system error when processing 'Contact Us' message. Please retry.");
        }               
    }
    
    public void mailTestResults(String testResultSummary) throws Exception {
        Session mailSession = getMailSession();
        try {
            MimeMessage msg = MailUtil.generateEmail_testSuiteResults(mailSession, testResultSummary);
            Transport.send(msg);
        } catch (MessagingException mex) {
            LOGGER.error("ERROR: Mail for Test Results to Admin Team - " + mex.getMessage(), mex);
        }               
    }
    
    /*
     * Note for GMAIL server:
     * 1. We chose to use TLS protocol over SSL protocol because the latter is older/obselete 
     *    and has became more vunerable to cyber attack.
     * 2. Do NOT use Session.getDefaultInstance() since it would install and reuse 
     *    a default Session in JVM for other calls. Hence, security could be an issue if we store
     *    authentication object there. Instead, use Session.getInstance()
     * 3. For GMAIL, the default sender "from" address would always be defaulted to the username's
     *    email address, even tho in our code we tried to overwrite it with 
     *    BusinessConstants.MAILSERVER_ADDRESS_FROM_DEFAULT
     */
    private Session getMailSession() throws Exception {
        /*
         * Setup connection to SMTP server
         */
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com"); 
        properties.put("mail.smtp.port", "587");
        
        //Setup authenticator for SMTP server:
        properties.put("mail.smtp.auth", "true");
        Authenticator mailAuthenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(
                        BusinessConstants.MAILSERVER_USERNAME, 
                        BusinessConstants.MAILSERVER_PASSWORD);
            }
        };       
        
        /*
         * Get SMTP connection session
         */
        return Session.getInstance(properties, mailAuthenticator);
    }
    
}
