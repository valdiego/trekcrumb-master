package com.trekcrumb.business.email;

import javax.naming.OperationNotSupportedException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Manager to send emails with Apache's JAMES server.
 * 
 * Ref:
 * 1. https://james.apache.org/server/3/quick-start.html
 * 
 * @author Val Triadi
 * @since 05/2016
 */
public class EmailManagerJamesImpl implements IEmailManager {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.email.EmailManagerJamesImpl"); 
    
    public void mailUserPasswordForget(User userWhoForget) throws Exception {
        throw new OperationNotSupportedException("EmailManagerJamesImpl has NOT been implemented!");
    }
    
    public void mailUserContactUs(User userWhoContactUs) throws Exception {
        if(userWhoContactUs == null
                || ValidationUtil.isEmpty(userWhoContactUs.getEmail())
                || ValidationUtil.isEmpty(userWhoContactUs.getSummary())) {
             throw new IllegalArgumentException("ERROR: Mail for Contact Us: Email and Summary (message) are required!");
        }

        throw new OperationNotSupportedException("EmailManagerJamesImpl has NOT been implemented!");
    }

    public void mailTestResults(String testResultSummary) throws Exception {
        throw new OperationNotSupportedException("EmailManagerJamesImpl has NOT been implemented!");
    }
    
}
