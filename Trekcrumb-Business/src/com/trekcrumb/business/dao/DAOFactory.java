package com.trekcrumb.business.dao;

public final class DAOFactory {
    private DAOFactory() {}
    
    public static IUserDAO getUserDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.UserDAOHibernateImpl();
    }
    
    public static IUserAuthTokenDAO getUserAuthTokenDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.UserAuthTokenDAOHibernateImpl();
    }
    
    public static ITripDAO getTripDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImpl();
    }

    public static IPlaceDAO getPlaceDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.PlaceDAOHibernateImpl();
    }
    
    public static IPictureDAO getPictureDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.PictureDAOHibernateImpl();
    }
    
    public static IFavoriteDAO getFavoriteDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.FavoriteDAOHibernateImpl();
    }

    public static ICommentDAO getCommentDAOInstance() {
        return new com.trekcrumb.business.dao.impl.hibernate.CommentDAOHibernateImpl();
    }

}
