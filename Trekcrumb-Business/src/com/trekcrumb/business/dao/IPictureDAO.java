package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public interface IPictureDAO {
    
    public Picture create(final Picture pictureToCreate) throws Exception;
    
    /**
     * 
     * @param pictureToRetrieve - Contains required parameters for a query.
     * @param retrieveBy - If the value is PICTURE_RETRIEVE_BY_WHERE_CLAUSE, then the whereClause 
     *                     parameter is required.
     * @param whereClause - A complete where clause. If the value is provided, then parameter
     *                      pictureToRetrieve is ignored. WhereClause should NOT include other query syntax
     *                      other than where clauses.
     * @param orderBy - If not provided, then it defaults to ORDER_BY_CREATED_DATE_ASC
     * @return List of picture objects found.
     * @throws Exception
     */
    public List<Picture> read(
            Picture pictureToRetrieve, 
            ServiceTypeEnum retrieveBy,
            String whereClause,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception;

    public int count(
            Picture pictureToCount,
            ServiceTypeEnum retrieveBy) 
            throws Exception ;
    
    public Picture update(Picture pictureToUpdate) throws Exception;
    
    public int delete(
            Picture pictureToDelete, 
            ServiceTypeEnum deleteBy) throws Exception;

}
