package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public interface ITripDAO {
    
    /**
     * Creates a new Trip.
     * 
     * @return The newly created Trip
     */
    public Trip create(Trip tripToCreate) throws Exception;    
    
    /**
     * Retrieves list of Trips from database given the criteria.
     * @param tripToRetrieve -
     *  Contains required data data depending on the retrieveBy param such as:
     *  1. If TRIP_RETRIEVE_BY_USERID, must contain UserID
     *  2. If TRIP_RETRIEVE_BY_TRIPID, must contain UserID and TripID
     *  3. If TRIP_RETRIEVE_BY_WHERE_CLAUSE, this param is ignored.
     *  4. Also, it can specify the trip's STATUS, PRIVACY and PUBLISH values to retrieve.
     *  
     * @param retrieveBy - Required. The valid values are TRIP_RETRIEVE_BY_USERID, TRIP_RETRIEVE_BY_TRIPID
     *                     and TRIP_RETRIEVE_BY_WHERE_CLAUSE
     * @param whereClause - Should only be provided if retrieveBy == TRIP_RETRIEVE_BY_WHERE_CLAUSE.
     *                      Otherwise, this is ignored and should be null.
     * @param orderBy - ORDER_BY_CREATED_DATE descending or ORDER_BY_UPDATED_DATE descending. 
     *                  If not provided (null), the default is by created date descending.
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used.
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 or any negative,
     *                    the method will return ALL records.
     *                    
     * @return List of Trips found, including each's components such as Places, Pictures, etc (enrichments).
     */
    public List<Trip> read(
            Trip tripToRetrieve, 
            ServiceTypeEnum retrieveBy, 
            String whereClause,
            OrderByEnum orderBy,
            int offset, int numOfRows) 
            throws Exception;

    /**
     * Counts the total number of Trips from database given the Trip criteria to find. 
     * This operation is a companion to the retrieve() operation.
     * 
     * @param tripToCount -
     *  Contains required data depending on the retrieveBy param such as:
     *  1. If TRIP_RETRIEVE_BY_USERID, must contain UserID
     *  2. If TRIP_RETRIEVE_BY_TRIPID, must contain UserID and TripID
     *  3. If TRIP_RETRIEVE_BY_WHERE_CLAUSE, this param is ignored.
     *  4. Also, it can specify the trip's STATUS, PRIVACY and PUBLISH values to retrieve.
     *  
     * @param retrieveBy - Required. The valid values are TRIP_RETRIEVE_BY_USERID, TRIP_RETRIEVE_BY_TRIPID
     *                     and TRIP_RETRIEVE_BY_WHERE_CLAUSE
     * @param whereClause - Should only be provided if retrieveBy == TRIP_RETRIEVE_BY_WHERE_CLAUSE.
     *                      Otherwise, this is ignored and should be null.
     * 
     * @return Number of Trips found.
     */
    public int count(
            Trip tripToCount, 
            ServiceTypeEnum retrieveBy, 
            String whereClause) throws Exception;

    /**
     * Searches Trips from database given the search criteria. 
     * 
     * Note: Since this is search operation, it will ONLLY return trips whose privacy is PUBLIC 
     * and publish status is PUBLISHED. To read trips that could be private and/or not-publish, 
     * see retrieve() method.
     * 
     * @param tripSearchCriteria - Contains search filters for trips.
     * @return List of Trips found, enriched with its components data (such as Places, Pictures, etc.)
    */
    public List<Trip> search(TripSearchCriteria tripSearchCriteria) throws Exception;
    
    /**
     * Counts the number of Trips for a given search criteria. This operation is a
     * companion to the search() operation.
     * 
     * @param tripSearchCriteria - Contains search filters for trips.
     * @return Number of Trips found given the specified search criteria
     */
    public int count(TripSearchCriteria tripSearchCriteria) throws Exception;

    /**
     * Updates a Trip.
     * @param tripToUpdate - The Trip bean containing ONLY the data to be updated. 
     *                       UserID and/or TripID are required based on updateFor parameter.
     * 
     * @return The updated Trip.
     */    
    public Trip update(Trip tripToUpdate) throws Exception;

    
    public int delete(Trip tripToDelete, ServiceTypeEnum deleteBy) throws Exception;
    
    /**
     * Creates a Trip that is previously created on user local device.
     * @param tripToPublish
     * @return The published trip.
     */
    public Trip publish(Trip tripToPublish) throws Exception;    
    
    /**
     * Retrieves Trip(s) from server into user local device as part of a sync operation 
     * for the given User and a Trip's last updated date.
     * @param tripToRetrieve - Contains the required data: UserID in question, and the Trip's last
     *                         updated date (if any).
     * @return List of Trips found
     */
    public List<Trip> readSync(Trip tripToRetrieve) throws Exception;
    
    

}
