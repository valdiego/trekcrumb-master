package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public interface ICommentDAO {
    
    public void create(Comment comment) throws Exception;
    
    public List<Comment> read(
            Comment comment,
            ServiceTypeEnum readBy,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception;


    public int count(Comment comment, ServiceTypeEnum countBy) throws Exception;
    
    public int delete(Comment comment, ServiceTypeEnum deleteBy) throws Exception;

}
