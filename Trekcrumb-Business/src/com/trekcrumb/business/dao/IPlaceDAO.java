package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public interface IPlaceDAO {
    
    public Place create(Place placeInput) throws Exception;
    
    public List<Place> read(
            Place placeToRetrieve, 
            ServiceTypeEnum retrieveBy, 
            String whereClause) 
            throws Exception;

    public Place update(Place placeToUpdate) throws Exception;
    
    public int delete(Place placeToDelete, ServiceTypeEnum deleteBy) throws Exception;

    
}
