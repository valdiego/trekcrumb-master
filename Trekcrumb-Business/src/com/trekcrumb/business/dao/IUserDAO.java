package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public interface IUserDAO {
    public User create(User userToCreate) throws Exception;
    
    public List<User> read(
            User userToRead,
            ServiceTypeEnum retrieveBy,
            boolean isEnrichment,
            boolean isUserLogin,
            int offset,
            int numOfRows) 
            throws Exception;
    
    public User update(User userWithUpdates) throws Exception;

    public int delete(
            User userToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception;
            
    public boolean deactivate(User userToDeactivate) throws Exception;
    
    public User reactivate(User userToReactivate) throws Exception;

    
}
