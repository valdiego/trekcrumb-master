package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;

public interface IFavoriteDAO {
    
    public void create(Favorite favorite) throws Exception;
    
    public List<Favorite> read(
            Favorite favorite,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception;

    public List<User> readByTrip(
            Trip trip,
            int offset,
            int numOfRows) 
            throws Exception;
    
    public List<Trip> readByUser(
            User user,
            int offset,
            int numOfRows) 
            throws Exception;

    public int count(Favorite favorite) throws Exception;
    
    public int countByUser(User user) throws Exception;
    
    public int delete(Favorite favorite) throws Exception;

}
