package com.trekcrumb.business.dao.impl.hibernate.usertype;

import com.trekcrumb.common.enums.YesNoEnum;

/**
 * Concrete implementation of BaseEnumUserType for YesNoEnum.
 *
 */
public class YesNoEnumUserType extends BaseEnumUserType<YesNoEnum> {

    @Override
    public Class<YesNoEnum> returnedClass() {
        return YesNoEnum.class;
    }    
}
