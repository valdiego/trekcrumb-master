package com.trekcrumb.business.dao.impl.hibernate.query;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: FAVORITE
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class FavoriteDBQuery {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQuery"); 

    public static void create(
            Session session, 
            Transaction transaction,
            Favorite favoriteToCreate) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Create error: Database session is required!");
        }
        
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        try {
            //FAVORITEID: DB will auto-generate ID and it is NOT_NULL column, we must assign a dummy String:
            favoriteToCreate.setId(CommonConstants.STRING_VALUE_EMPTY);

            session.save(favoriteToCreate);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Create error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
    }
    
    /**
     * Retrieves list of Favorites from database given the criteria.
     * @param favoriteToRetrieve 
     *  1. If tripID is provided, retrieve all Favorites for the given Trip ID
     *  2. If userID is provided, retrieve all Favorites for the given User ID
     *  3. If tripID AND userID are provided, retrieve ONLY favorite with these two matched parameters.
     * @param orderBy - ORDER_BY_CREATED_DATE descending or ORDER_BY_UPDATED_DATE descending. 
     *                  If not provided (null), the default is by created date descending.
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used. 
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 or any negative,
     *                    the method will return ALL records.
     */
    @SuppressWarnings("unchecked")
    public static List<Favorite> read(
            Session session, 
            Transaction transaction,
            Favorite favoriteToRetrieve,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        List<Favorite> listFavoritesFound = null;
        SQLQuery sqlQuery = null;
        if(orderBy == null) {
            //Last created will be the first in line.
            orderBy = OrderByEnum.ORDER_BY_CREATED_DATE;
        }

        try {
            //Compose query:
            StringBuilder queryStrBld = composeQueryForRetrieveOrCount(favoriteToRetrieve, true);
            queryStrBld.append(orderBy.getValue());
            sqlQuery = session.createSQLQuery(queryStrBld.toString());

            //Add pagination on query (limits):
            if(numOfRows > 0) {
                if(numOfRows > CommonConstants.RECORDS_NUMBER_MAX) {
                    numOfRows = CommonConstants.RECORDS_NUMBER_MAX;
                }
                if(offset < 0) {
                    offset = 0;
                }
                sqlQuery.setFirstResult(offset); 
                sqlQuery.setMaxResults(numOfRows); 
            }
            
            //Execute:
            sqlQuery.addEntity(Favorite.class);
            listFavoritesFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Retrieve error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listFavoritesFound;
    }
    
    /**
     * Read Favorite given a TripID, and return a LIST OF USERS who favorite this Trip AND 
     * their status is "ACTIVE".
     * By default, it returns the list in the order of last created first in the list.
     * @param trip - Contains required TripID in question.
     * @return A list of Users
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static List<User> readByTrip(
            Session session, 
            Transaction transaction,
            Trip trip,
            int offset,
            int numOfRows) 
            throws Exception {
        /*
         * IMPORTANT NOTES:
         * 1. We use regular Hibernate SQLQuery to compose SELECT query and to read data. Using this,
         *    we COULD NOT select just some columns from the target entity table (USER) because
         *    Hibernate will expect to read ALL columns as defined as properties (in HBM xml file).
         *    Therefore, we must accept to read all USER columns, and later loop through the result list
         *    to remove any propery values we do not need (or must hide) from the bean manually.
         * 2. SECURITY ISSUE: These User beans may contain sensitive data. We must remove them
         *    as necessary in a 'manager' component level (See TripManager and FavoriteManager).
         *
         * 3. Alternatively, we could have used Hibernate Criteria, Projection and Join to limit
         *    the columns we want to read. But this costly too: To join multi-tables, there must
         *    be foreign-key relationships between them (otherwise: Property xxx not found on yyy exception).
         *    Furthermore, using Criteria with Projection causes it to return PLAIN RAW object list
         *    whom we CANNOT cast to our target bean class. Costly, we have to loop through the 
         *    result list and transform them into the target bean manually.
         */
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Read by Trip error: Database session is required!");
        }
        if(trip == null || ValidationUtil.isEmpty(trip.getId())) {
            throw new IllegalArgumentException("Favorite Read by Trip error: TripID is required!");
        }
        
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        List<User> listUsersFound = null;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            queryStrBld.append("select USER.*"); 
            queryStrBld.append(" from FAVORITE, USER");
            queryStrBld.append(" where FAVORITE.USERID = USER.USERID");
            queryStrBld.append(" and USER.IS_ACTIVE = " + YesNoEnum.YES.getId());
            queryStrBld.append(" and FAVORITE.TRIPID = :tripIdParam");
            queryStrBld.append(" order by FAVORITE.CREATED desc");
            sqlQuery = session.createSQLQuery(queryStrBld.toString());

            //Add pagination on query (limits):
            if(numOfRows > 0) {
                if(numOfRows > CommonConstants.RECORDS_NUMBER_MAX) {
                    numOfRows = CommonConstants.RECORDS_NUMBER_MAX;
                }
                if(offset < 0) {
                    offset = 0;
                }
                sqlQuery.setFirstResult(offset); 
                sqlQuery.setMaxResults(numOfRows); 
            }
            
            //Execute:
            sqlQuery.setString("tripIdParam", trip.getId());
            sqlQuery.addEntity(User.class);
            listUsersFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Retrieve by Trip error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listUsersFound;
    }
    
    /**
     * Read Favorite given a User's username, and return a LIST OF TRIPS whom this user is favoriting
     * AND these trips are not PRIVATE.
     * By default, it returns the list in the order of last created first in the list.
     * @param user - Contains required username in question.
     * @return A list of Trips
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static List<Trip> readByUser(
            Session session, 
            Transaction transaction,
            User user,
            int offset,
            int numOfRows) 
            throws Exception {
        /*
         * IMPORTANT NOTES:
         * 1. We use regular Hibernate SQLQuery to compose SELECT query and to read data. Using this,
         *    we COULD NOT select just some columns from the target entity table (USER) because
         *    Hibernate will expect to read ALL columns as defined as properties (in HBM xml file).
         *    Therefore, we must accept to read all columns, and later loop through the result list
         *    to remove any propery values we do not need (or must hide) from the bean manually.
         * 2. Alternatively, we could have used Hibernate Criteria, Projection and Join to limit
         *    the columns we want to read. But this costly too: To join multi-tables, there must
         *    be foreign-key relationships between them (otherwise: Property xxx not found on yyy exception).
         *    Furthermore, using Criteria with Projection causes it to return PLAIN RAW object list
         *    whom we CANNOT cast to our target bean class. Costly, we have to loop through the 
         *    result list and transform them into the target bean manually.
         */
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Read by User error: Database session is required!");
        }
        if(user == null || ValidationUtil.isEmpty(user.getUsername())) {
            throw new IllegalArgumentException("Favorite Read by User error: Username is required!");
        }
        
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        List<Trip> listTripsFound = null;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            queryStrBld.append("select TRIP.*"); 
            queryStrBld.append(" from FAVORITE, TRIP, USER");
            queryStrBld.append(" where FAVORITE.TRIPID = TRIP.TRIPID and FAVORITE.USERID = USER.USERID");
            queryStrBld.append(" and TRIP.PRIVACY <> " + TripPrivacyEnum.PRIVATE.getId());
            queryStrBld.append(" and USER.USERNAME = :usernameParam");
            queryStrBld.append(" order by FAVORITE.CREATED desc");
            sqlQuery = session.createSQLQuery(queryStrBld.toString());

            //Add pagination on query (limits):
            if(numOfRows > 0) {
                if(numOfRows > CommonConstants.RECORDS_NUMBER_MAX) {
                    numOfRows = CommonConstants.RECORDS_NUMBER_MAX;
                }
                if(offset < 0) {
                    offset = 0;
                }
                sqlQuery.setFirstResult(offset); 
                sqlQuery.setMaxResults(numOfRows); 
            }
            
            //Execute:
            sqlQuery.setString("usernameParam", user.getUsername());
            sqlQuery.addEntity(Trip.class);
            listTripsFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Retrieve by User error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listTripsFound;
    }        
    
    /**
     * Counts Favorites from database given the criteria.
     * @param favoriteToRetrieve 
     *  1. If tripID is provided, count all Favorites for the given Trip ID
     *  2. If userID is provided, count all Favorites for the given User ID
     *  3. If tripID AND userID are provided, count ONLY favorite with these two matched parameters.
     *  
      */
    public static int count(
            Session session, 
            Transaction transaction,
            Favorite favoriteToCount) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Count error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int numFound = 0;
        Query sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld =  composeQueryForRetrieveOrCount(favoriteToCount, false);
            sqlQuery = session.createSQLQuery(queryStrBld.toString());
       
            //Execute:
            numFound = ((BigInteger) sqlQuery.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Count error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return numFound;
    }    
    
    /**
     * Counts ALL Favorites from database given Username.
     * @param User - contains the username in question. 
     */
    public static int countByUser(
            Session session, 
            Transaction transaction,
            User user) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Count by User error: Database session is required!");
        }
        if(user == null || ValidationUtil.isEmpty(user.getUsername())) {
            throw new IllegalArgumentException("Favorite Count by User error: Username is required!");
        }
        
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int numFound = 0;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            queryStrBld.append("select count(*)"); 
            queryStrBld.append(" from FAVORITE, TRIP, USER");
            queryStrBld.append(" where FAVORITE.TRIPID = TRIP.TRIPID and FAVORITE.USERID = USER.USERID");
            queryStrBld.append(" and TRIP.PRIVACY <> " + TripPrivacyEnum.PRIVATE.getId());
            queryStrBld.append(" and USER.USERNAME = :usernameParam");
            sqlQuery = session.createSQLQuery(queryStrBld.toString());

            //Execute:
            sqlQuery.setString("usernameParam", user.getUsername());
            numFound = ((BigInteger) sqlQuery.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Count by User error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }                
        return numFound;
    }    
    
    public static int delete(
            Session session, 
            Transaction transaction,
            Favorite favoriteToDelete) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Favorite Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int deletedItems = -1;
        Query sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            queryStrBld.append("delete"); 
            queryStrBld.append(" from FAVORITE");
            if(!ValidationUtil.isEmpty(favoriteToDelete.getTripID())
                    && !ValidationUtil.isEmpty(favoriteToDelete.getUserID())) {
                queryStrBld.append(" where TRIPID = '" + favoriteToDelete.getTripID() + "' and USERID = '" + favoriteToDelete.getUserID() + "'");
                
            } else if(!ValidationUtil.isEmpty(favoriteToDelete.getTripID())) {
                queryStrBld.append(" where TRIPID = '" + favoriteToDelete.getTripID() + "'");
                
            } else if(!ValidationUtil.isEmpty(favoriteToDelete.getUserID())) {
                queryStrBld.append(" where USERID = '" + favoriteToDelete.getUserID() + "'");
            } else {
                throw new IllegalArgumentException("Favorite DB Query error: either or both TripID and UserID are required!");
            }
            sqlQuery = session.createSQLQuery(queryStrBld.toString());
      
            //Execute:
            deletedItems = sqlQuery.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Favorite Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            sqlQuery = null;
        }        
        return deletedItems;
    }    
    
     
    
    private static StringBuilder composeQueryForRetrieveOrCount(
            Favorite favoriteToRetrieve,
            boolean isRetrieve) {
        StringBuilder queryStrBld = new StringBuilder();
        if(isRetrieve) {
            queryStrBld.append("select FAVORITE.*");
        } else {
            queryStrBld.append("select count(*)"); 
        }
        queryStrBld.append(" from FAVORITE, USER, TRIP");
        queryStrBld.append(" where FAVORITE.TRIPID = TRIP.TRIPID and FAVORITE.USERID = USER.USERID");
        queryStrBld.append(" and USER.IS_ACTIVE = " + YesNoEnum.YES.getId());
        if(!ValidationUtil.isEmpty(favoriteToRetrieve.getTripID())
                && !ValidationUtil.isEmpty(favoriteToRetrieve.getUserID())) {
            queryStrBld.append(" and FAVORITE.TRIPID = '" + favoriteToRetrieve.getTripID() + "' and FAVORITE.USERID = '" + favoriteToRetrieve.getUserID() + "'");
            
        } else if(!ValidationUtil.isEmpty(favoriteToRetrieve.getTripID())) {
            queryStrBld.append(" and FAVORITE.TRIPID = '" + favoriteToRetrieve.getTripID() + "'");
            
        } else if(!ValidationUtil.isEmpty(favoriteToRetrieve.getUserID())) {
            queryStrBld.append(" and FAVORITE.USERID = '" + favoriteToRetrieve.getUserID() + "'");
        } else {
            throw new IllegalArgumentException("Favorite DB Query error: either or both TripID and UserID are required!");
        }
        
        return queryStrBld;
    }
    
}
