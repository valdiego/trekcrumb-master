package com.trekcrumb.business.dao.impl.hibernate.query;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: TRIP
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class TripDBQuery {
    private final static Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.TripDBQuery"); 

    public static void create(
            Session session, 
            Transaction transaction,
            Trip trip) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Create error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        try {
            session.save(trip);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Trip Create: ERROR while writing to DB! " + e.getMessage(), e);
            e.printStackTrace();
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
            
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
    }
    
    /**
     * Retrieves list of Trips from database given the criteria.
     * @param tripToRetrieve -
     *  Contains required data data depending on the retrieveBy param such as:
     *  1. If TRIP_RETRIEVE_BY_USERID, must contain UserID
     *  2. If TRIP_RETRIEVE_BY_TRIPID, must contain TripID
     *  3. If TRIP_RETRIEVE_BY_WHERE_CLAUSE, this param is ignored.
     *  4. Also, it can specify the trip's STATUS, PRIVACY and PUBLISH values to retrieve.
     *  
     * @param retrieveBy - Required. The valid values are TRIP_RETRIEVE_BY_USERID, TRIP_RETRIEVE_BY_TRIPID
     *                     and TRIP_RETRIEVE_BY_WHERE_CLAUSE
     * @param whereClause - Should only be provided if retrieveBy == TRIP_RETRIEVE_BY_WHERE_CLAUSE.
     *                      Otherwise, this is ignored and should be null.
     * @param orderBy - ORDER_BY_CREATED_DATE descending or ORDER_BY_UPDATED_DATE descending. 
     *                  If not provided (null), the default is by created date descending.
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used.
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 or any negative,
     *                    the method will return ALL records.
     */
    @SuppressWarnings("unchecked")
    public static List<Trip> read(
            Session session, 
            Transaction transaction,
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        List<Trip> listTripsFound = null;
        if(orderBy == null) {
            orderBy = OrderByEnum.ORDER_BY_CREATED_DATE;
        }
        
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = composeQueryForRetrieveOrCount(tripToRetrieve, retrieveBy, whereClause, true);
            queryStrBld.append(orderBy.getValue());
            sqlQuery = session.createSQLQuery(queryStrBld.toString());

            //Add pagination on query (limits):
            if(numOfRows > 0) {
                if(numOfRows > CommonConstants.RECORDS_NUMBER_MAX) {
                    numOfRows = CommonConstants.RECORDS_NUMBER_MAX;
                }
                if(offset < 0) {
                    offset = 0;
                }
                sqlQuery.setFirstResult(offset); 
                sqlQuery.setMaxResults(numOfRows); 
            }
            
            //Execute:
            sqlQuery.addEntity(Trip.class);
            listTripsFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(IllegalArgumentException ile) {
            throw ile;
        } catch(Exception e) {
            LOGGER.error("Trip Read error:  " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listTripsFound;
    }
    
    /**
     * Counts the total number of Trips from database given the criteria. This operation is a
     * companion to the retrieve() operation.
     * 
     * @param tripToCount -
     *  Contains required data depending on the retrieveBy param such as:
     *  1. If TRIP_RETRIEVE_BY_USERID, must contain UserID
     *  2. If TRIP_RETRIEVE_BY_TRIPID, must contain UserID and TripID
     *  3. If TRIP_RETRIEVE_BY_WHERE_CLAUSE, this param is ignored.
     *  4. Also, it can specify the trip's STATUS, PRIVACY and PUBLISH values to retrieve.
     *  
     * @param retrieveBy - Required. The valid values are TRIP_RETRIEVE_BY_USERID, TRIP_RETRIEVE_BY_TRIPID
     *                     and TRIP_RETRIEVE_BY_WHERE_CLAUSE
     * @param whereClause - Should only be provided if retrieveBy == TRIP_RETRIEVE_BY_WHERE_CLAUSE.
     *                      Otherwise, this is ignored and should be null.
     */
    public static int count(
            Session session, 
            Transaction transaction,
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Count error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int numOfTrips = -1;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = composeQueryForRetrieveOrCount(tripToRetrieve, retrieveBy, whereClause, false);
            sqlQuery = session.createSQLQuery(queryStrBld.toString());
            
            //Execute:
            numOfTrips = ((BigInteger)sqlQuery.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Trip Count error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return numOfTrips;
    }
    
    /**
     * Searches Trips from database given the search criteria. 
     * 
     * Note: Since this is search operation, it will ONLLY return trips whose privacy is PUBLIC 
     * and publish status is PUBLISHED. To read trips that could be private and/or not-publish, 
     * see retrieve() method.
     * 
     * @param tripSearchCriteria - Contains search filters for trips.
    */
    @SuppressWarnings("unchecked")
    public static List<Trip> search(
            Session session, 
            Transaction transaction,
            TripSearchCriteria tripSearchCriteria) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Search error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        List<Trip> listTripsFound = null;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder nativeQueryStrBld = composeQueryForSearchOrCount(tripSearchCriteria, true);
            sqlQuery = session.createSQLQuery(nativeQueryStrBld.toString());
            
            //Set offset and limit:
            if(tripSearchCriteria.getOffset() >= 0) {
                sqlQuery.setFirstResult(tripSearchCriteria.getOffset()); 
            } else {
                sqlQuery.setFirstResult(0);
            }
            if(tripSearchCriteria.getNumOfRows() > 0
                    && tripSearchCriteria.getNumOfRows() <= CommonConstants.RECORDS_NUMBER_MAX) {
                sqlQuery.setMaxResults(tripSearchCriteria.getNumOfRows()); 
            } else {
                sqlQuery.setMaxResults(CommonConstants.RECORDS_NUMBER_MAX); 
            }

            //Execute:
            sqlQuery.addEntity(Trip.class);
            listTripsFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(IllegalArgumentException ile) {
            throw ile;
        } catch(Exception e) {
            LOGGER.error("Trip Search error:  " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listTripsFound;
    }    
    
    /**
     * Counts the number of Trips for a given search criteria. This operation is a
     * companion to the search() operation. It count ALL records satisfying the
     * criteria, no setting for offset/limit.
     * 
     * @param tripSearchCriteria - Contains search filters for trips.
     * @return Number of Trips found given the specified search criteria
     */
    public static int count(
            Session session, 
            Transaction transaction,
            TripSearchCriteria tripSearchCriteria) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Count error: Database session is required!");
        }
        if(tripSearchCriteria == null){
            throw new IllegalArgumentException("Trip Count error: The search criteria must be provided!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int numOfTrips = -1;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = composeQueryForSearchOrCount(tripSearchCriteria, false);
            sqlQuery = session.createSQLQuery(queryStrBld.toString());
            
            //Execute:
            numOfTrips = ((BigInteger)sqlQuery.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Trip Count error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }
        return numOfTrips;
    }
    
    /**
     * Updates a Trip or Trips depending on the input params.
     * 
     * @param tripToUpdate
     *  The Trip bean containing ONLY the data to be updated. UserID and/or TripID are required based on updateFor parameter.
     *  1. If the property inside Trip bean is null, then this data will not be updated. 
     *  2. For required fields, if the data is empty string, then it will not be updated 
     *     (cannot have empty values); example: Trip Name. 
     *  3. But for non-required fields, if the data is empty string, then it will be updated as empty String; example Trip Note.
     *  4. This update does not apply to PUBLISH column since the architecture prevents updating only 
     *     a Trip's publish value. Changing a NOT_PUBLISH to PUBLISH Trip is essentially the same as
     *     create a new Trip on the server.
     *  
     * @param updateFor 
     *  1. If it is regular Trip update, this param can be null. Both UserID, TripID and updated date are required.
     *  2. If the value is ServiceTypeEnum.TRIP_UPDATE_FOR_ALLSTATUSCOMPLETED, then it is to update ALL 
     *     Trips for the given User to 'completed' status. Thus UserID and updated date are required.
     *       
     * @return Number of updated items.
     *         NOTE: In Hibernate scheme of things, if the row to be updated is not found, the 
     *         Query.executeUpdate() returns ZERO (no throw exception). 
     * @throws Exception
     */
    public static int update(
            Session session, 
            Transaction transaction,
            Trip tripToUpdate,
            ServiceTypeEnum updateFor) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Update error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int updatedItems = -1;
        Query query = null;
        try {
            /*
             * Compose query
             * NOTE: 
             * In order to support string with special chars and new line, we must use "parameterized query" 
             * (i.e., the query input parameters) and populate the values later after session.createQuery(). 
             * If we, instead, set the values directly at StringBuilder.append(), the createQuery() will 
             * remove new line and throw "unexpected token" exception for special chars.
             */
            if(updateFor == ServiceTypeEnum.TRIP_UPDATE_FOR_ALLSTATUSCOMPLETED) {
                /*
                 * NOTE: 
                 * Update all user's active trip to 'completed' status.
                 * Typically by design, this update occurs when user creates a new Trip.
                 */
                if(tripToUpdate == null 
                        || ValidationUtil.isEmpty(tripToUpdate.getUserID())
                        || ValidationUtil.isEmpty(tripToUpdate.getUpdated())) {
                    throw new IllegalArgumentException("Trip Update for all status completed error: Missing UserID or updated date!");
                }
                StringBuilder hqlQueryStrBld = new StringBuilder();
                hqlQueryStrBld.append("update Trip set ");
                hqlQueryStrBld.append("STATUS = " + TripStatusEnum.COMPLETED.getId());
                hqlQueryStrBld.append(", UPDATED = '" + tripToUpdate.getUpdated() + "'");
                hqlQueryStrBld.append(", COMPLETED = '" + tripToUpdate.getUpdated() + "'");
                hqlQueryStrBld.append(" where USERID = :userIdParam");
                hqlQueryStrBld.append(" and STATUS = :statusParam");
                
                query = session.createQuery(hqlQueryStrBld.toString());
                query.setString("userIdParam", tripToUpdate.getUserID());
                query.setInteger("statusParam", TripStatusEnum.ACTIVE.getId());
            
            } else {
                //Regular update:
                if(tripToUpdate == null
                        || ValidationUtil.isEmpty(tripToUpdate.getUserID())
                        || ValidationUtil.isEmpty(tripToUpdate.getId())
                        || ValidationUtil.isEmpty(tripToUpdate.getUpdated())) {
                    throw new IllegalArgumentException("Trip Update error: UserID, TripID and updated date are required!");
                }
                
                StringBuilder hqlQueryStrBld = new StringBuilder();
                hqlQueryStrBld.append("update Trip set ");
                hqlQueryStrBld.append("UPDATED = :updateParam");
                if(!ValidationUtil.isEmpty(tripToUpdate.getName())) {
                    hqlQueryStrBld.append(", NAME = :tripNameParam");
                }
                if(tripToUpdate.getNote() != null) {
                    //It can be empty string:
                    hqlQueryStrBld.append(", NOTE = :tripNoteParam");
                }
                if(!ValidationUtil.isEmpty(tripToUpdate.getLocation())) {
                    hqlQueryStrBld.append(", LOCATION = :tripLocationParam");
                }
                if(tripToUpdate.getPrivacy() != null) {
                    hqlQueryStrBld.append(", PRIVACY = " + tripToUpdate.getPrivacy().getId());
                }
                if(tripToUpdate.getStatus() == TripStatusEnum.COMPLETED) {
                    //Can only update status from Active to Completed, not the other way:
                    hqlQueryStrBld.append(", STATUS = " + tripToUpdate.getStatus().getId());
                    hqlQueryStrBld.append(", COMPLETED = '" + tripToUpdate.getUpdated() + "'");
                }
                hqlQueryStrBld.append(" where TRIPID = :tripIDParam and USERID = :userIDParam");

                query = session.createQuery(hqlQueryStrBld.toString());
                
                //Fill in the values:
                query.setString("updateParam", tripToUpdate.getUpdated());
                query.setString("tripIDParam", tripToUpdate.getId());
                query.setString("userIDParam", tripToUpdate.getUserID());
                if(!ValidationUtil.isEmpty(tripToUpdate.getName())) {
                    query.setString("tripNameParam", tripToUpdate.getName());
                }
                if(tripToUpdate.getNote() != null) {
                    //It can be empty string:
                    query.setString("tripNoteParam", tripToUpdate.getNote().trim());
                }
                if(!ValidationUtil.isEmpty(tripToUpdate.getLocation())) {
                    query.setString("tripLocationParam", tripToUpdate.getLocation());
                }
            }
            
            //Execute:
            updatedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Trip Update error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }
        
        return updatedItems;
    }
    
    public static int delete(
            Session session, 
            Transaction transaction,
            Trip tripToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int deletedItems = -1;
        Query query = null;
        try {
            //Compose query:
            if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
                if(tripToDelete == null
                        || ValidationUtil.isEmpty(tripToDelete.getUserID())){
                    throw new IllegalArgumentException("Trip Delete by User id error: The UserID must be provided.");
                }
                String hqlQueryStr = "delete Trip where USERID = :userIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", tripToDelete.getUserID());
                
            } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                if(tripToDelete == null
                        || ValidationUtil.isEmpty(tripToDelete.getUserID())
                        || ValidationUtil.isEmpty(tripToDelete.getId())){
                    throw new IllegalArgumentException("Trip Delete by Trip id error: The UserID and TripID must be provided.");
                }
                String hqlQueryStr = "delete Trip where USERID = :userIdParam and TRIPID = :tripIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", tripToDelete.getUserID());
                query.setString("tripIdParam", tripToDelete.getId());
                
            } else {
                throw new IllegalArgumentException("Trip Delete error: Unsupported operation type - " + deleteBy);
            }            

            //Execute:
            deletedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Trip Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }        
        return deletedItems;
    }    
    
    private static StringBuilder composeQueryForRetrieveOrCount(
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            boolean isRetrieve) {
        StringBuilder queryStrBld = new StringBuilder();
        if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME) {
            if(isRetrieve) {
                queryStrBld.append("select TRIP.USERID, TRIP.TRIPID, TRIP.NAME, TRIP.NOTE, TRIP.LOCATION"); 
                queryStrBld.append(", TRIP.STATUS, TRIP.PRIVACY, TRIP.PUBLISH");
                queryStrBld.append(", date_format(TRIP.CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
                queryStrBld.append(", date_format(TRIP.UPDATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as UPDATED"); 
                queryStrBld.append(", date_format(TRIP.COMPLETED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as COMPLETED");
                queryStrBld.append(" from USER, TRIP");
            } else {
                queryStrBld.append("select count(*) from TRIP, USER"); 
            }
            
        } else {
            if(isRetrieve) {
                queryStrBld.append("select TRIPID, USERID, NAME, NOTE, LOCATION, STATUS, PRIVACY, PUBLISH");
                queryStrBld.append(", date_format(CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
                queryStrBld.append(", date_format(UPDATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as UPDATED");
                queryStrBld.append(", date_format(COMPLETED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as COMPLETED");
                queryStrBld.append(" from TRIP");
            } else {
                queryStrBld.append("select count(*) from TRIP");
            }
        }
        
        if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME) {
            if(tripToRetrieve == null 
                    || ValidationUtil.isEmpty(tripToRetrieve.getUsername())){
                throw new IllegalArgumentException("Trip Retrieve/Count by Username error: The Username must be provided.");
            }
            queryStrBld.append(" where TRIP.USERID = USER.USERID and USER.USERNAME = '" + tripToRetrieve.getUsername() + "'");
            
        } else if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID) {
            if(tripToRetrieve == null 
                    || ValidationUtil.isEmpty(tripToRetrieve.getUserID())){
                throw new IllegalArgumentException("Trip Retrieve/Count by UserID error: The UserID must be provided.");
            }
            queryStrBld.append(" where USERID = '" + tripToRetrieve.getUserID() + "'");
            
        } else if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID) {
            if(tripToRetrieve == null 
                    || ValidationUtil.isEmpty(tripToRetrieve.getId())) {
                throw new IllegalArgumentException("Trip Retrieve/Count by TripID error: The TripID must be provided.");
            }
            queryStrBld.append(" where TRIPID = '" + tripToRetrieve.getId() + "'");

        } else if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE) {
            if(ValidationUtil.isEmpty(whereClause)){
                throw new IllegalArgumentException("Trip Retrieve/Count by where-clause error: A valid SQL where-clause is required!");
            }
            queryStrBld.append(" ");
            queryStrBld.append(whereClause.trim());

        } else {
            throw new IllegalArgumentException("Trip Retrieve/Count error: Unsupported operation type - " + retrieveBy);
        }
        
        //Limit query further when provided:
        if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERNAME
                || retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID
                || retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID) {
            if(tripToRetrieve.getStatus() != null) {
                queryStrBld.append(" and TRIP.STATUS = " + tripToRetrieve.getStatus().getId());
            }
            if(tripToRetrieve.getPublish() != null) {
                queryStrBld.append(" and TRIP.PUBLISH = " + tripToRetrieve.getPublish().getId());
            }
            
            /*
             * When the request param has privacy set:
             * 1. If PUBLIC, we shall read only public trips
             * 2. Else-if FRIENDS, we shall read only public and friend trips
             * 3. Else: Nothing set or PRIVATE, we will retun all trips including private trips.
             */
            if(tripToRetrieve.getPrivacy() != null) {
                switch(tripToRetrieve.getPrivacy()) {
                    case PUBLIC:
                        //
                        queryStrBld.append(" and TRIP.PRIVACY = 1");
                        break;
                    case FRIENDS:
                        queryStrBld.append(" and (TRIP.PRIVACY = 1 or TRIP.PRIVACY = 3)");
                        break;
                    case PRIVATE:
                        //include ALL
                }        
            }
        }
        
        return queryStrBld;
    }

    private static StringBuilder composeQueryForSearchOrCount(
            TripSearchCriteria tripSearchCriteria, 
            boolean isSearch) {
        StringBuilder queryStrBld = new StringBuilder();
        if(isSearch) {
            queryStrBld.append("select TRIP.USERID, TRIP.TRIPID, TRIP.NAME, TRIP.NOTE, TRIP.LOCATION"); 
            queryStrBld.append(", TRIP.STATUS, TRIP.PRIVACY, TRIP.PUBLISH");
            queryStrBld.append(", date_format(TRIP.CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
            queryStrBld.append(", date_format(TRIP.UPDATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as UPDATED"); 
            queryStrBld.append(", date_format(TRIP.COMPLETED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as COMPLETED"); 
        } else {
            queryStrBld.append("select count(*)"); 
        }
        
        //Defaults for search:
        queryStrBld.append(" from USER, TRIP");
        queryStrBld.append(" where USER.IS_ACTIVE = " + YesNoEnum.YES.getId());
        queryStrBld.append(" and TRIP.PRIVACY = " + TripPrivacyEnum.PUBLIC.getId());
        queryStrBld.append(" and TRIP.PUBLISH = " + TripPublishEnum.PUBLISH.getId());
        queryStrBld.append(" and TRIP.USERID = USER.USERID");
        
        if(!ValidationUtil.isEmpty(tripSearchCriteria.getTripKeyword())) {
            queryStrBld.append(" and (TRIP.NAME LIKE '%" + tripSearchCriteria.getTripKeyword() + "%'");
            queryStrBld.append(" or TRIP.LOCATION LIKE '%" + tripSearchCriteria.getTripKeyword() + "%')");
        }
        if(!ValidationUtil.isEmpty(tripSearchCriteria.getUserKeyword())) {
            queryStrBld.append(" and (USER.USERNAME LIKE '%" + tripSearchCriteria.getUserKeyword() + "%'");
            queryStrBld.append(" or USER.FULLNAME LIKE '%" + tripSearchCriteria.getUserKeyword() + "%')");
        }
        if(tripSearchCriteria.getStatus() != null) {
            queryStrBld.append(" and TRIP.STATUS = " + tripSearchCriteria.getStatus().getId());
        }
        
        if(isSearch) {
            queryStrBld.append(tripSearchCriteria.getOrderBy().getValue());
        }
        
        return queryStrBld;
    }    
    
    
}
