package com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.business.dao.ICommentDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQuery;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public class CommentDAOHibernateImpl 
implements ICommentDAO {
    
    public void create(Comment comment) throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            CommentDBQuery.create(session, transaction, comment);
            transaction.commit();

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    public List<Comment> read(
            Comment comment,
            ServiceTypeEnum readBy,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<Comment> listOfComments = 
                    CommentDBQuery.read(session, null, comment, readBy, orderBy, offset, numOfRows);
            if(listOfComments == null || listOfComments.size() == 0) {
                return null;
            }
            
            return listOfComments;
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }                
    }

    public int count(Comment comment, ServiceTypeEnum countBy) throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return CommentDBQuery.count(session, null, comment, countBy);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        
    }
    
    public int delete(Comment comment, ServiceTypeEnum deleteBy) throws Exception {
        int numDeleted = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            numDeleted = CommentDBQuery.delete(session, transaction, comment, deleteBy);
            transaction.commit();
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
        
        return numDeleted;
    }



}
