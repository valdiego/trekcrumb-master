package com.trekcrumb.business.dao.impl.hibernate.utility;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.TripDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.TripPublishEnum;
import com.trekcrumb.common.exception.TripNotFoundException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

public class HibernateUtility {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility");

    /**
     * Validates to see if the given username and/or email exists in the DB.
     * @return TRUE is either one exists. False if none of them exists.
     */
    public static boolean verifyUsernameOrEmailExist(Session session, User user) throws Exception {
        if(ValidationUtil.isEmpty(user.getUsername()) && ValidationUtil.isEmpty(user.getEmail())) {
            return false;
        }
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Verify if Username/Email Exists error: Database session is required!");
        }

        List<User> usersFound = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            
            if(!ValidationUtil.isEmpty(user.getUsername())) {
                usersFound = UserDBQuery.read(session, transaction, user, ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME, null, 0, 1);
                if(usersFound != null && usersFound.size() > 0) {
                    return true;
                }
            }

            if(!ValidationUtil.isEmpty(user.getEmail())) {
                usersFound = UserDBQuery.read(session, transaction, user, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, null, 0, 1);
                if(usersFound != null && usersFound.size() > 0) {
                    return true;
                }
            }
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("Verify if Username/Email Exists error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if(transaction != null) {
                transaction.commit();
                transaction = null;
            }
        }
        return false;
    }
    
    /**
     * Enriches the User with bits of data here and there as required to process further
     * display/logics by populating user bean with:
     * 1. Trips: Total number of trips, last created trip and last updated trip
     * 2. Pictures: Total number of pictures
     * 3. Favorites
     * 4. Friends
     * 5. More...
     * 
     * @param userToEnrich - Must contain UserID in question
     * @param isUserLogin - Set to 'TRUE' if this is for user login's own data, where it would include user's 
     *                      private data that only user login can see. Otherwise, set to false (default).
     *                      
     */
    public static User userEnrichment(
            Session session, 
            User userToEnrich, 
            boolean isUserLogin,
            boolean isFriend) throws Exception {
       if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("User Enrichment error: Database session is required!");
        }
        if(userToEnrich == null
                || userToEnrich.getUserId() == null) {
            return userToEnrich;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            //Total Trips:
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(userToEnrich.getUserId());
            if(!isUserLogin) {
                //Limit the searched Trip:
                tripToRetrieve.setPrivacy(TripPrivacyEnum.PUBLIC);
                tripToRetrieve.setPublish(TripPublishEnum.PUBLISH);
            }
            userToEnrich.setNumOfTotalTrips(
                    TripDBQuery.count(session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, null));
            
            //Last created trip:
            List<Trip> userTrips = TripDBQuery.read(
                    session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, 
                    null, OrderByEnum.ORDER_BY_CREATED_DATE, 0, 1);
            if(userTrips != null && userTrips.size() > 0) {
                userToEnrich.setLastCreatedTrip(userTrips.get(0));
            }
            
            //Last updated trip:
            userTrips = TripDBQuery.read(
                    session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_USERID, 
                    null, OrderByEnum.ORDER_BY_UPDATED_DATE, 0, 1);
            if(userTrips != null && userTrips.size() > 0) {
                userToEnrich.setLastUpdatedTrip(userTrips.get(0));
            }
            
            //Total pictures:
            Picture picToCount = new Picture();
            picToCount.setUsername(userToEnrich.getUsername());
            if(isUserLogin) {
                picToCount.setPrivacy(TripPrivacyEnum.PRIVATE);
            } else {
                picToCount.setPrivacy(TripPrivacyEnum.PUBLIC);
            }
            userToEnrich.setNumOfTotalPictures(
                    PictureDBQuery.count(session, transaction, picToCount, ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME));
            
            /*
             * Count favorites
             * TODO 8/2017: This count includes all favorited Trips even though some trips may have
             * become "PRIVATE"
             */
            Favorite faveToCount = new Favorite();
            faveToCount.setUserID(userToEnrich.getUserId());
            userToEnrich.setNumOfFavorites(FavoriteDBQuery.count(session, transaction, faveToCount));
            
            /*
             * Count comments
             * TODO 8/2017: This count includes all commented Trips even though some trips may have
             * become "PRIVATE"
             */
            Comment commentToCount = new Comment();
            commentToCount.setUsername(userToEnrich.getUsername());
            userToEnrich.setNumOfComments(CommentDBQuery.count(session, transaction, commentToCount, ServiceTypeEnum.COMMENT_RETRIEVE_BY_USERNAME));
            
            //UserAuthToken
            if(isUserLogin) {
                UserAuthToken authTokenToRead = new UserAuthToken();
                authTokenToRead.setUserId(userToEnrich.getUserId());
                userToEnrich.setListOfAuthTokens(UserAuthTokenDBQuery.read(session, transaction, authTokenToRead, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID));
            }
            
            //TODO: Count total friends
        } catch(IllegalArgumentException iae) {
            throw iae;
        } catch(Exception e) {
            LOGGER.error("User Enrichment error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if(transaction != null) {
                transaction.commit();
                transaction = null;
            }
        }
        return userToEnrich;
    }
    
    /**
     * Enriches the Trip with Trip's places, pictures, comments, etc.
     * Note:
     * 1. This operation calls directly the DB impl classes to retrieve Places, Pictures, etc. 
     *    instead of the managers (PlaceManager, PictureManager, etc.). The reason is to avoid
     *    recreating multiple DB impl instances due to the for-loop.
     * 2. TODO (12/2015): Remove this method if we can enhance Trip DB to retrieve these
     *    enrichment data directly during TripDBQuery.read()
     *    
     * @param retrieveTripBy - Default should be just NULL (ignored), unless it is 
     *                         ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID
     * @return The same List of Trip beans that have been enriched with more data.
     */
    public static List<Trip> tripEnrichment(
            Session session, 
            List<Trip> listOfTripsToPopulate,
            ServiceTypeEnum retrieveTripBy) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Trip Enrichment error: Database session is required!");
        }
        if(listOfTripsToPopulate == null
                || listOfTripsToPopulate.size() == 0) {
            return listOfTripsToPopulate;
        }

        Place placeToRetrieve = new Place();
        Picture pictureToRetrieve = new Picture();
        User userToRetrieve = new User();
        Favorite faveToCount = new Favorite();
        Comment commentToCount = new Comment();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            
            for(Trip trip : listOfTripsToPopulate) {
                //Place:
                placeToRetrieve.setTripID(trip.getId());
                List<Place> placesReadList = PlaceDBQuery.read(
                        session, transaction, placeToRetrieve, ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID, null);
                if(placesReadList != null && placesReadList.size() > 0) {
                    trip.setListOfPlaces(placesReadList);
                }

                //Picture:
                pictureToRetrieve.setTripID(trip.getId());
                List<Picture> picsReadList = PictureDBQuery.read(
                        session, transaction, pictureToRetrieve, ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID, 
                        null, OrderByEnum.ORDER_BY_CREATED_DATE_ASC, 0, -1);
                if(picsReadList != null && picsReadList.size() > 0) {
                    trip.setListOfPictures(picsReadList);
                }
                
                //User:
                userToRetrieve.setUserId(trip.getUserID());
                List<User> listUsersFound = UserDBQuery.read(
                        session, transaction, userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1); 
                if(listUsersFound != null && listUsersFound.size() > 0) {
                    User user = listUsersFound.get(0);
                    trip.setUsername(user.getUsername().toLowerCase());
                    trip.setUserFullname(user.getFullname());
                    trip.setUserImageName(user.getProfileImageName());
                }
                
                /*
                 * Favorites
                 * 1. If a trip has one(s) and the call is ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID,
                 *    then we try to always return the first CommonConstants.RECORDS_NUMBER_MAX 
                 *    so the data is handy and prevent one DB lookup. If ServiceTypeEnum is something else,
                 *    we do not need to provide the handy data.
                 * 
                 * 2. SECURITY ISSUE: These User beans may contain sensitive data. We must remove them
                 *    as necessary in a 'manager' component level (See TripManager).
                 */
                faveToCount.setTripID(trip.getId());
                trip.setNumOfFavorites(FavoriteDBQuery.count(session, transaction, faveToCount));
                if(trip.getNumOfFavorites() > 0
                        && retrieveTripBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID) {
                    trip.setListOfFavoriteUsers(
                            FavoriteDBQuery.readByTrip(session, transaction, trip, 0, CommonConstants.RECORDS_NUMBER_MAX));
                }
                
                /*
                 * Comments
                 * 1. If a trip has one(s) and the call is ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID,
                 *    then we try to always return the first CommonConstants.RECORDS_NUMBER_MAX 
                 *    so the data is handy and prevent one DB lookup. If ServiceTypeEnum is something else,
                 *    we do not need to provide the handy data.
                 */
                commentToCount.setTripID(trip.getId());
                trip.setNumOfComments(
                        CommentDBQuery.count(session, transaction, commentToCount, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID));
                if(trip.getNumOfComments() > 0
                        && retrieveTripBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID) {
                    trip.setListOfComments(
                            CommentDBQuery.read(session, transaction, commentToCount, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID,
                                                null, 0, CommonConstants.RECORDS_NUMBER_MAX));
                }
            }
        } finally {
            if(transaction != null) {
                transaction.commit();
                transaction = null;
            }
        }
        
        return listOfTripsToPopulate;
    }
    
    /**
     * Enriches the Picture with User and Trip's data.
     * Note:
     * 1. This operation calls directly the DB impl classes to retrieve User, Trip, etc. 
     *    instead of the managers (UserManager, TripManager, etc.). The reason is to avoid
     *    recreating multiple DB impl instances due to the for-loop.
     * 2. TODO (12/2015): Picture Enrichment needs refactor to get these data during PictureDBQuery.read()
          for better performance.
     *    Hence, remove this method if we can enhance Picture DB to retrieve these
     *    enrichment data directly during PictureDBQuery.read()
     */
    public static List<Picture> pictureEnrichment(Session session, List<Picture> listOfPicsToPopulate) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Picture Enrichment error: Database session is required!");
        }
        if(listOfPicsToPopulate == null
                || listOfPicsToPopulate.size() == 0) {
            return listOfPicsToPopulate;
        }
        
        User userToRetrieve = new User();
        Trip tripToRetrieve = new Trip();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            
            for(Picture picture : listOfPicsToPopulate) {
                //User:
                userToRetrieve.setUserId(picture.getUserID());
                List<User> listUsersFound = UserDBQuery.read(
                        session, transaction, userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1); 
                if(listUsersFound != null && listUsersFound.size() > 0) {
                    User user = listUsersFound.get(0);
                    picture.setUsername(user.getUsername().toLowerCase());
                    picture.setUserFullname(user.getFullname());
                    picture.setUserImageName(user.getProfileImageName());
                }
                
                //Trip
                tripToRetrieve.setId(picture.getTripID());
                List<Trip> listTripsFound = TripDBQuery.read(
                        session, transaction, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1); 
                if(listTripsFound != null && listTripsFound.size() > 0) {
                    Trip trip = listTripsFound.get(0);
                    picture.setTripName(trip.getName());
                }
            }
        } finally {
            if(transaction != null) {
                transaction.commit();
                transaction = null;
            }
        }
        
        return listOfPicsToPopulate;
    }    
    
    /**
     * Handles potential yet known Hibernate's DB ContrainViolationException due to invalid/non-exist
     * UserID and/or TripID. This exception may occur during operations to entity Trip, Place, 
     * Picture, etc. where DB entries have Foreign-Key contraints to USERID and/or TRIPID columns.
     * 
     * @param cve - The Exception being handled.
     * @param operationName - User-friendly String action/method name. Example: 'Place Create', 
     *        'Picture Create', etc.
     * @param userID - The USERID that may cause the exception because it does not exist or invalid
     * @param tripID - The TRIPID that may cause the exception because it does not exist or invalid
     * 
     * @throws Exception - The 'handled' exception being thrown and expected by upstream components.
     */
    public static void handleConstraintViolationException(
            ConstraintViolationException cve,
            String operationName,
            String userID,
            String tripID) throws Exception {
        String exceptionMsg = cve.getMessage();
        System.err.println(operationName + " - ERROR: Unexpected ConstraintViolationException due to invalid or non-exist UserID [" 
                + userID + "] and/or TripID [" + tripID + "]! Detail: " + exceptionMsg);
        
        if(exceptionMsg != null) {
            if(exceptionMsg.toLowerCase().contains("userid")) {
                throw new UserNotFoundException(operationName + " - ERROR: User NOT found given the user data!");
            } else if(exceptionMsg.toLowerCase().contains("tripid")) {
                throw new TripNotFoundException(operationName + " - ERROR: Trip NOT found given the trip data!");
            }
        }
        
        //Else:
        throw new IllegalArgumentException(operationName + " - ERROR: Either User and/or Trip are not found given input data!");

    }
}
