package com.trekcrumb.business.dao.impl.hibernate.query;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripPrivacyEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: USERCOMMENT
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class CommentDBQuery {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQuery"); 

    public static void create(
            Session session, 
            Transaction transaction,
            Comment commentToCreate) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Comment Create error: Database session is required!");
        }
        
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        try {
            session.save(commentToCreate);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Comment Create error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
    }
    
    /**
     * Retrieves list of Comments from database given the criteria.
     * @param commentToRetrieve 
     * @param ServiceTypeEnum readBy,
     * @param orderBy - ORDER_BY_CREATED_DATE descending or ORDER_BY_UPDATED_DATE descending. 
     *                  If not provided (null), the default is by created date descending.
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used. 
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 or any negative,
     *                    the method will return ALL records.
     */
    @SuppressWarnings("unchecked")
    public static List<Comment> read(
            Session session, 
            Transaction transaction,
            Comment commentToRetrieve,
            ServiceTypeEnum retrieveBy,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Comment Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        List<Comment> listCommentsFound = null;
        SQLQuery sqlQuery = null;
        if(orderBy == null) {
            //First created will be the first in line.
            orderBy = OrderByEnum.ORDER_BY_CREATED_DATE_ASC;
        }

        try {
            //Compose query:
            StringBuilder queryStrBld = composeQueryForRetrieveOrCount(commentToRetrieve, retrieveBy, true);
            queryStrBld.append(orderBy.getValue());
            sqlQuery = session.createSQLQuery(queryStrBld.toString());

            //Add pagination on query (limits):
            if(numOfRows > 0) {
                if(numOfRows > CommonConstants.RECORDS_NUMBER_MAX) {
                    numOfRows = CommonConstants.RECORDS_NUMBER_MAX;
                }
                if(offset < 0) {
                    offset = 0;
                }
                sqlQuery.setFirstResult(offset); 
                sqlQuery.setMaxResults(numOfRows); 
            }
            
            //Execute:
            sqlQuery.addEntity(Comment.class);
            listCommentsFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Comment Read error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listCommentsFound;
    }
    
    public static int count(
            Session session, 
            Transaction transaction,
            Comment commentToRetrieve,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Comment Count error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int numFound = 0;
        Query sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld =  composeQueryForRetrieveOrCount(commentToRetrieve, retrieveBy, false);
            sqlQuery = session.createSQLQuery(queryStrBld.toString());
       
            //Execute:
            numFound = ((BigInteger) sqlQuery.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Comment Count error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return numFound;
    }    
    
    public static int delete(
            Session session, 
            Transaction transaction,
            Comment commentToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Comment Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int deletedItems = -1;
        Query sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            queryStrBld.append("delete"); 
            queryStrBld.append(" from USERCOMMENT");
            switch(deleteBy) {
                case COMMENT_DELETE_BY_COMMENTID: 
                    if(ValidationUtil.isEmpty(commentToDelete.getUserID())
                            || ValidationUtil.isEmpty(commentToDelete.getId())) {
                        throw new IllegalArgumentException("Comment Delete error: Delete by commentID requires userID and commentID!");
                    }
                    queryStrBld.append(" where USERCOMMENTID = '" + commentToDelete.getId() + "' and USERID = '" + commentToDelete.getUserID() + "'");
                    break;
                
                case COMMENT_DELETE_BY_TRIPID: 
                    if(ValidationUtil.isEmpty(commentToDelete.getTripID())) {
                        throw new IllegalArgumentException("Comment Delete error: Delete by tripID requires Trip ID!");
                    }
                    queryStrBld.append(" where TRIPID = '" + commentToDelete.getTripID() + "'");
                    break;
                   
                case COMMENT_DELETE_BY_USERID: 
                    if(ValidationUtil.isEmpty(commentToDelete.getUserID())) {
                        throw new IllegalArgumentException("Comment Delete error: Delete by userID requires User ID!");
                    }
                    queryStrBld.append(" where USERID = '" + commentToDelete.getUserID() + "'");
                    break;
                
                default:
                    throw new IllegalArgumentException("Comment Delete error: Unknown delete by value: " + deleteBy);
            }

            sqlQuery = session.createSQLQuery(queryStrBld.toString());
      
            //Execute:
            deletedItems = sqlQuery.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Comment Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            sqlQuery = null;
        }        
        return deletedItems;
    }    
    
    private static StringBuilder composeQueryForRetrieveOrCount(
            Comment comment,
            ServiceTypeEnum serviceType,
            boolean isRetrieve) {
        StringBuilder queryStrBld = new StringBuilder();
        if(isRetrieve) {
            queryStrBld.append("select USERCOMMENT.USERCOMMENTID as USERCOMMENTID, USERCOMMENT.TRIPID as TRIPID, USERCOMMENT.USERID as USERID, USERCOMMENT.NOTE as NOTE");
            queryStrBld.append(", date_format(USERCOMMENT.CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
            queryStrBld.append(", USER.USERNAME as USERNAME");
            queryStrBld.append(", USER.FULLNAME as USER_FULLNAME");
            queryStrBld.append(", USER.PROFILE_IMAGE_NAME as USER_IMAGENAME");
            queryStrBld.append(", USER.PROFILE_IMAGE_URL as USER_IMAGEURL");
            queryStrBld.append(", TRIP.NAME as TRIP_NAME");
        } else {
            queryStrBld.append("select count(*)"); 
        }
        queryStrBld.append(" from USERCOMMENT, USER, TRIP");
        queryStrBld.append(" where USERCOMMENT.TRIPID = TRIP.TRIPID and USERCOMMENT.USERID = USER.USERID");
        queryStrBld.append(" and USER.IS_ACTIVE = " + YesNoEnum.YES.getId());
        
        switch(serviceType) {
            case COMMENT_RETRIEVE_BY_COMMENTID: 
                if(ValidationUtil.isEmpty(comment.getId())) {
                    throw new IllegalArgumentException("Comment Read/Count error: Requires comment ID!");
                }
                queryStrBld.append(" and USERCOMMENT.USERCOMMENTID = '" + comment.getId() + "'");
                break;
            
            case COMMENT_RETRIEVE_BY_TRIPID: 
                if(ValidationUtil.isEmpty(comment.getTripID())) {
                    throw new IllegalArgumentException("Comment Read/Count error: Requires Trip ID!");
                }
                queryStrBld.append(" and USERCOMMENT.TRIPID = '" + comment.getTripID() + "'");
                break;
               
            case COMMENT_RETRIEVE_BY_USERNAME:
                if(ValidationUtil.isEmpty(comment.getUsername())) {
                    throw new IllegalArgumentException("Comment Read/Count error: Requires Username!");
                }
                //Exclude any PRIVATE Trips:
                queryStrBld.append(" and TRIP.PRIVACY <> " + TripPrivacyEnum.PRIVATE.getId());
                queryStrBld.append(" and USER.USERNAME = '" + comment.getUsername() + "'");
                break;
            
            default:
                throw new IllegalArgumentException("Comment Read/Count error: Unknown service type value: " + serviceType);
        }        
        return queryStrBld;
    }
    
}
