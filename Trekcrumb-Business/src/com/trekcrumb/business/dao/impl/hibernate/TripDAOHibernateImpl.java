package com.trekcrumb.business.dao.impl.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.business.dao.ITripDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.CommentDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.TripDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility;
import com.trekcrumb.common.bean.Comment;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.TripSearchCriteria;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.DateFormatEnum;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.TripStatusEnum;
import com.trekcrumb.common.exception.TripNotFoundException;
import com.trekcrumb.common.exception.UserNotFoundException;
import com.trekcrumb.common.utility.CommonUtil;
import com.trekcrumb.common.utility.DateUtil;
import com.trekcrumb.common.utility.ValidationUtil;

public class TripDAOHibernateImpl 
implements ITripDAO {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.TripDAOHibernateImpl"); 

    /**
     * @see ITripDAO.create()
     */
    @Override
    public Trip create(Trip tripToCreate) throws Exception {
        Trip tripCreated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            /*
             * Verify User exists
             * (Need this to avoid SQL "contraint violation exception")
             */
            User userToRead = new User();
            userToRead.setUserId(tripToCreate.getUserID());
            List<User> userListFound = UserDBQuery.read(
                    session, null, userToRead, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, -1);
            if(userListFound == null || userListFound.size() == 0) {
                LOGGER.error("Trip Create: ERROR UserID [" + tripToCreate.getUserID() + "] is invalid/not exist!");
                throw new UserNotFoundException("Trip Create: CANCELED! The given User data is invalid or does not exist!");
            }

            //Updates user's previous active trips to 'COMPLETED'
            Trip tripToCompleted = new Trip();
            tripToCompleted.setUserID(tripToCreate.getUserID());
            tripToCompleted.setUpdated(tripToCreate.getCreated());
            TripDBQuery.update(session, null, tripToCompleted, ServiceTypeEnum.TRIP_UPDATE_FOR_ALLSTATUSCOMPLETED);
            
            //Create trip and ALL its components:
            tripCreated = tripCreateOnServer(session, tripToCreate, false);
            
            //Enrich Trip with user data before returning:
            User userToRetrieve = new User();
            userToRetrieve.setUserId(tripCreated.getUserID());
            List<User> listUsersFound = UserDBQuery.read(
                    session, null, userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1); 
            if(listUsersFound != null && listUsersFound.size() > 0) {
                User user = listUsersFound.get(0);
                tripCreated.setUsername(user.getUsername());
                tripCreated.setUserFullname(user.getFullname());
                tripCreated.setUserImageName(user.getProfileImageName());
            }
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        
        return tripCreated;
    }
    
    /**
     * @see ITripDAO.read()
     */
    @Override
    public List<Trip> read(
            Trip tripToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        List<Trip> tripsReadList = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            tripsReadList = TripDBQuery.read(session, null, tripToRetrieve, retrieveBy, whereClause, orderBy, offset, numOfRows);
            if(tripsReadList == null || tripsReadList.size() == 0) {
                return null;
            }
            
            //Enrichments:
            tripsReadList = HibernateUtility.tripEnrichment(session, tripsReadList, retrieveBy);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return tripsReadList;
    }
    
    /**
     * @see ITripDAO.count()
     */
    @Override
    public int count(
            Trip tripToCount,
            ServiceTypeEnum retrieveBy,
            String whereClause) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return TripDBQuery.count(session, null, tripToCount, retrieveBy, whereClause);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    /**
     * @see ITripDAO.search()
     */
    public List<Trip> search(
            TripSearchCriteria tripSearchCriteria) 
            throws Exception {
        List<Trip> tripsFoundList = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            tripsFoundList = TripDBQuery.search(session, null, tripSearchCriteria);
            if(tripsFoundList == null || tripsFoundList.size() == 0) {
                return null;
            }
            
            //Enrichments:
            tripsFoundList = HibernateUtility.tripEnrichment(session, tripsFoundList, null);        
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return tripsFoundList;
    }
    
    /**
     * @see ITripDAO.count(TripSearchCriteria)
     */
    @Override
    public int count(
            TripSearchCriteria tripSearchCriteria) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return TripDBQuery.count(session, null, tripSearchCriteria);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    /**
     * @see ITripDAO.update()
     */
    @Override
    public Trip update(
            Trip tripToUpdate) 
            throws Exception {
        Trip tripUpdated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            TripDBQuery.update(session, null, tripToUpdate, ServiceTypeEnum.TRIP_UPDATE);
            
            //Read after update to return all data:
            session.clear();
            
            Trip tripToRetrieve = new Trip();
            tripToRetrieve.setUserID(tripToUpdate.getUserID());
            tripToRetrieve.setId(tripToUpdate.getId());
            List<Trip> listOfTrips = 
                    TripDBQuery.read(session, null, tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            if(listOfTrips == null || listOfTrips.size() == 0 || listOfTrips.get(0) == null) {
                throw new TripNotFoundException("Trip Update error: Trip to update is not found!");
            }
            
            //Enrichments:
            listOfTrips = HibernateUtility.tripEnrichment(
                    session, listOfTrips, ServiceTypeEnum.COMMENT_RETRIEVE_BY_TRIPID);
            tripUpdated = listOfTrips.get(0);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        

        return tripUpdated;
    }
    
    /**
     * @see ITripDAO.delete()
     */
    @Override
    public int delete(
            Trip tripToDelete, 
            ServiceTypeEnum deleteBy) 
            throws Exception {
        int numDeleted = -1;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            
            //Delete its components first:
            //1. Place:
            Place placeToDelete = new Place();
            placeToDelete.setUserID(tripToDelete.getUserID());
            placeToDelete.setTripID(tripToDelete.getId());
            if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
                PlaceDBQuery.delete(session, transaction, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_USERID);
            } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                PlaceDBQuery.delete(session, transaction, placeToDelete, ServiceTypeEnum.PLACE_DELETE_BY_TRIPID);
            }

            //2. Picture:
            Picture pictureToDelete = new Picture();
            pictureToDelete.setUserID(tripToDelete.getUserID());
            pictureToDelete.setTripID(tripToDelete.getId());
            if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
                PictureDBQuery.delete(session, transaction, pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_USERID);
            } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                PictureDBQuery.delete(session, transaction, pictureToDelete, ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID);
            }
            
            //3. Favorites:
            Favorite faveToDelete = new Favorite();
            if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
                faveToDelete.setUserID(tripToDelete.getUserID());
            } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                faveToDelete.setTripID(tripToDelete.getId());
            }
            FavoriteDBQuery.delete(session, transaction, faveToDelete);

            //4. Comments:
            Comment commentToDelete = new Comment();
            if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_USERID) {
                commentToDelete.setUserID(tripToDelete.getUserID());
                CommentDBQuery.delete(session, transaction, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_USERID);
            } else if(deleteBy == ServiceTypeEnum.TRIP_DELETE_BY_TRIPID) {
                commentToDelete.setTripID(tripToDelete.getId());
                CommentDBQuery.delete(session, transaction, commentToDelete, ServiceTypeEnum.COMMENT_DELETE_BY_TRIPID);
            }
            
            //Execute:
            numDeleted = TripDBQuery.delete(session, transaction, tripToDelete, deleteBy);
            
            transaction.commit();
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
        return numDeleted;
    }
    
    /**
     * @see ITripDAO.publish()
     */
    @Override
   public Trip publish(
           Trip tripToPublish) 
           throws Exception {
        /*
         * Business rules:
         * 1. Enable to re-publish trips more than once to anticipate previous publish was failed:
         * 1.1. Check if the same trip has been published. If so, delete it. 
         * 
         * 2. Setting up STATUS properly:
         * 2.1. Check if there is any current published Trip whose status == ACTIVE
         * 2.1.1. If found: Check if current active published Trip CREATED is before the new published Trip:
         * 2.1.1.1. True: Update current published Trip status to COMPLETED
         * 2.1.1.2. Else: Mark the new published Trip status (from whatever it is) to default COMPLETED 
         * 
         * 3. Create (copy) the published Trip.
         */
        Trip tripPublished = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            //Find if the same Trip has been published before:
            List<Trip> tripReadList = TripDBQuery.read(session, null, tripToPublish, ServiceTypeEnum.TRIP_RETRIEVE_BY_TRIPID, null, null, 0, 1);
            if(tripReadList != null && tripReadList.size() > 0) {
                Trip tripPublishedBefore = tripReadList.get(0);
                TripDBQuery.delete(session, null, tripPublishedBefore, ServiceTypeEnum.TRIP_DELETE_BY_TRIPID);
                session.clear();
            }

            //Find any existing ACTIVE trip:
            String whereClause = "where USERID = '" + tripToPublish.getUserID() + "' and STATUS = " + TripStatusEnum.ACTIVE.getId() + "";
            List<Trip> tripActiveFoundList = 
                    TripDBQuery.read(session, null, null, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, 
                                whereClause, OrderByEnum.ORDER_BY_CREATED_DATE, 0, 1);
            if(tripActiveFoundList != null 
                    && tripActiveFoundList.size() > 0 
                    && tripActiveFoundList.get(0) != null) {
                Date tripCurrentActiveCreatedDate = DateUtil.formatDate(tripActiveFoundList.get(0).getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
                Date tripToPublishCreatedDate = DateUtil.formatDate(tripToPublish.getCreated(), DateFormatEnum.GMT_STANDARD_LONG);
                if(tripToPublishCreatedDate.after(tripCurrentActiveCreatedDate)) {
                    //Mark existing ACTIVE trip as completed:
                    Trip tripToCompleted = new Trip();
                    tripToCompleted.setUserID(tripToPublish.getUserID());
                    tripToCompleted.setUpdated(tripToPublish.getUpdated());
                    TripDBQuery.update(session, null, tripToCompleted, ServiceTypeEnum.TRIP_UPDATE_FOR_ALLSTATUSCOMPLETED);
                    session.clear();
                
                } else {
                    if(tripToPublish.getStatus() == TripStatusEnum.ACTIVE) {
                        tripToPublish.setStatus(TripStatusEnum.COMPLETED);
                        tripToPublish.setCompleted(DateUtil.getCurrentTimeWithGMTTimezone());
                    }
                }
            }
            
            tripPublished = tripCreateOnServer(session, tripToPublish, true);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        

        return tripPublished;
    }
    
    /**
     * @see ITripDAO.readSync()
     * @return List of Trips found whose updated dates are after the given input updated date with
     *         order by updated desc.
     */
    @Override
    public List<Trip> readSync(
            Trip tripToRetrieve) 
            throws Exception {
        /*
         * Business rules:
         * 1. This operation is to return a collection of Trips whose updated dates are AFTER the given
         *    updated date to ensure the caller receives only the latest Trips he/she does not currently have.
         * 2. The list, if any, is returned in the order of updated date descending. 
         * 3. If the input updated date is null, then it indicates the user currently does not have 
         *    any Trips collection; thus it will return all Trips records.
         */
         StringBuilder whereClauseBld = new StringBuilder();
         whereClauseBld.append("where");
         whereClauseBld.append(" USERID = '" + tripToRetrieve.getUserID() + "'");
         if(!ValidationUtil.isEmpty(tripToRetrieve.getUpdated())) {
             whereClauseBld.append(" and UPDATED > '" + tripToRetrieve.getUpdated().trim() + "'");
         }

         return read(
                 tripToRetrieve, ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE, 
                 whereClauseBld.toString(), OrderByEnum.ORDER_BY_UPDATED_DATE, 0, -1);
    }    
    
    /*
     * Inserts the new Trip and all its components (Places, Pictures, etc.) into server database.
     */
    private static Trip tripCreateOnServer(
            Session session,
            Trip tripToCreate,
            boolean isPublish) throws Exception {
        String tripID = tripToCreate.getId();
        String createdDate = tripToCreate.getCreated();
        
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            //Insert to DB:
            TripDBQuery.create(session, transaction, tripToCreate);
            
            //Create Place(s):
            if(tripToCreate.getListOfPlaces() != null
                    && tripToCreate.getListOfPlaces().size() > 0) {
                List<Place> listOfPlacesToCreate = tripToCreate.getListOfPlaces();
                if(isPublish) {
                    //Already created on a local device, create a copy as is:
                    for(Place placeToCreate : listOfPlacesToCreate) {
                        if(ValidationUtil.isEmpty(placeToCreate.getId())
                                || ValidationUtil.isEmpty(placeToCreate.getTripID())
                                || ValidationUtil.isEmpty(placeToCreate.getUserID())
                                || ValidationUtil.isEmpty(placeToCreate.getCreated())
                                || ValidationUtil.isEmpty(placeToCreate.getUpdated())
                                || placeToCreate.getLatitude() == 0
                                || placeToCreate.getLongitude() == 0) {
                            LOGGER.error("Trip Publish: ERROR while publishing Trip [" + tripToCreate.getId() + "] => One of its places had EMPTY required data!");
                            continue;
                        }
                        PlaceDBQuery.create(session, transaction, placeToCreate); 
                    }
                    
                } else {
                    //Fresh from raw:
                    List<Place> listOfPlacesCreated = new ArrayList<Place>();
                    for(Place placeToCreate : listOfPlacesToCreate) {
                        placeToCreate.setUserID(tripToCreate.getUserID());
                        placeToCreate.setTripID(tripID);
                        placeToCreate.setId(CommonUtil.generateID(tripID));
                        placeToCreate.setCreated(createdDate);
                        placeToCreate.setUpdated(createdDate);
                        PlaceDBQuery.create(session, transaction, placeToCreate);
                        
                        listOfPlacesCreated.add(placeToCreate);
                    }
                    tripToCreate.setListOfPlaces(listOfPlacesCreated);
                }
            }

            //Create Picture(s):
            if(tripToCreate.getListOfPictures() != null
                    && tripToCreate.getListOfPictures().size() > 0) {
                List<Picture> listOfPicturesToCreate = tripToCreate.getListOfPictures();
                if(isPublish) {
                    //Already created on a local device, create a copy as is:
                    for(Picture picToCreate : listOfPicturesToCreate) {
                        if(ValidationUtil.isEmpty(picToCreate.getId())
                                || ValidationUtil.isEmpty(picToCreate.getTripID())
                                || ValidationUtil.isEmpty(picToCreate.getUserID())
                                || ValidationUtil.isEmpty(picToCreate.getCreated())
                                || ValidationUtil.isEmpty(picToCreate.getUpdated())
                                || ValidationUtil.isEmpty(picToCreate.getImageName())) {
                            LOGGER.error("Trip Publish: ERROR while publishing Trip [" + tripToCreate.getId() + "] => One of its pictures had EMPTY required data!");
                            continue;
                        }
                        PictureDBQuery.create(session, transaction, picToCreate);
                    }
                } else {
                    //Fresh from raw:
                    /*
                     * Note (7/2014): Current solution does not support adding new picture at 
                     * the same time as creating new Trip
                     */
                }
            }
            
            transaction.commit();
            
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;

        } finally {
            transaction = null;
        }

        return tripToCreate;
    }
    

    
    
}
