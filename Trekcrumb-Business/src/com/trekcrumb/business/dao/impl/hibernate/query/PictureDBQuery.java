package com.trekcrumb.business.dao.impl.hibernate.query;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: PICTURE
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class PictureDBQuery {
    private static final Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQuery"); 

    public static void create(
            Session session, 
            Transaction transaction,
            Picture pictureToCreate) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Picture Create error: Database session is required!");
        }
        if(pictureToCreate == null) {
            throw new IllegalArgumentException("Picture Create error: A complete Picture bean to create is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        try {
            session.save(pictureToCreate);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Picture Create error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
    }
    
    /**
     * Retrieves list of Pictures from database given the criteria.
     * @param pictureToRetrieve 
     *  Contains required data data depending on the retrieveBy param such as:
     *  1. If PICTURE_RETRIEVE_BY_PICTUREID, the bean must contain PictureID and TripID
     *  2. If PICTURE_RETRIEVE_BY_TRIPID, the bean must contain TripID
     *  3. If PICTURE_RETRIEVE_BY_USERID, the bean must contain UserID. 
     *     Note: This operation is intended for internal used only (for User/Trip delete, etc.)
     *  4. If PICTURE_RETRIEVE_BY_USERNAME, the bean must contain Username and Trip privacy. 
     *     In addition, it accounts for params offset and numOfRows.
     *  5. If PICTURE_SEARCH, it returns the last CommonConstants.RECORDS_NUMBER_MAX trips that are 
     *     public. 
     *     NOTE: We do not implement full-blown search feature for Picture like we did for Trips/Users.
     *     No search criteria nor offset is used.
     *  6. If PICTURE_RETRIEVE_BY_WHERE_CLAUSE, this param is ignored, but whereClause is required.
     *  
     * @param retrieveBy - Required.
     * @param whereClause - Required only if retrieveBy == PICTURE_RETRIEVE_BY_WHERE_CLAUSE.
     *                      Otherwise, this is ignored.
     * @param orderBy - If not provided (null), the default is by ORDER_BY_CREATED_DATE_ASC.
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used. 
     *                 Only used when retrieveBy == PICTURE_RETRIEVE_BY_USERNAME or PICTURE_RETRIEVE_BY_USERID.
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 or any negative,
     *                    the method will return ALL records.
     *                    Only used when retrieveBy == PICTURE_RETRIEVE_BY_USERNAME or PICTURE_RETRIEVE_BY_USERID.
     */
    @SuppressWarnings("unchecked")
    public static List<Picture> read(
            Session session, 
            Transaction transaction,
            Picture pictureToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Picture Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        List<Picture> listPicsFound = null;
        SQLQuery sqlQuery = null;
        if(orderBy == null) {
            //First created will be the first in line.
            orderBy = OrderByEnum.ORDER_BY_CREATED_DATE_ASC;
        }

        try {
            //Compose query:
            StringBuilder queryStrBld = null;
            if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID) {
                if(pictureToRetrieve == null 
                        || ValidationUtil.isEmpty(pictureToRetrieve.getId())
                        || ValidationUtil.isEmpty(pictureToRetrieve.getTripID())) {
                     throw new IllegalArgumentException("Picture Retrieve by PictureID error: The Trip ID and Picture ID must be provided!");
                 }
                queryStrBld = composeQueryForReadOrCount(true, pictureToRetrieve, retrieveBy);
                queryStrBld.append(" where TRIPID = :tripIdParam and PICTUREID = :picIdParam");
                queryStrBld.append(orderBy.getValue());
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.setString("picIdParam", pictureToRetrieve.getId());
                sqlQuery.setString("tripIdParam", pictureToRetrieve.getTripID());
                
            } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID) {
                if(pictureToRetrieve == null
                        || ValidationUtil.isEmpty(pictureToRetrieve.getTripID())) {
                    throw new IllegalArgumentException("Picture Retrieve by TripID error: The Trip ID must be provided!");
                }
                queryStrBld = composeQueryForReadOrCount(true, pictureToRetrieve, retrieveBy);
                queryStrBld.append(" where TRIPID = :tripIdParam");
                queryStrBld.append(orderBy.getValue());
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.setString("tripIdParam", pictureToRetrieve.getTripID());
            
            } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID) {
                if(pictureToRetrieve == null
                    || ValidationUtil.isEmpty(pictureToRetrieve.getUserID())) {
                        throw new IllegalArgumentException("Picture Retrieve by UserID error: The User ID must be provided!");
                }
                queryStrBld = composeQueryForReadOrCount(true, pictureToRetrieve, retrieveBy);
                queryStrBld.append(" where USERID = :userIdParam");
                queryStrBld.append(orderBy.getValue());
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.setString("userIdParam", pictureToRetrieve.getUserID());
                
            } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME) {
                if(pictureToRetrieve == null
                        || ValidationUtil.isEmpty(pictureToRetrieve.getUsername())
                        || pictureToRetrieve.getPrivacy() == null) {
                    throw new IllegalArgumentException("Picture Retrieve by Username error: The Username and Privacy must be provided!");
                }
                queryStrBld = composeQueryForReadOrCount(true, pictureToRetrieve, retrieveBy);
                queryStrBld.append(orderBy.getValue());
                sqlQuery = session.createSQLQuery(queryStrBld.toString());

                //Add pagination on query (limits):
                if(numOfRows > 0) {
                    if(numOfRows > CommonConstants.RECORDS_NUMBER_MAX) {
                        numOfRows = CommonConstants.RECORDS_NUMBER_MAX;
                    }
                    if(offset < 0) {
                        offset = 0;
                    }
                    sqlQuery.setFirstResult(offset); 
                    sqlQuery.setMaxResults(numOfRows); 
                }
                
            } else if(retrieveBy == ServiceTypeEnum.PICTURE_SEARCH) {
                queryStrBld = composeQueryForReadOrCount(true, pictureToRetrieve, retrieveBy);
                queryStrBld.append(OrderByEnum.ORDER_BY_CREATED_DATE);
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.setMaxResults(CommonConstants.RECORDS_NUMBER_MAX); 

            } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_WHERE_CLAUSE) {
                if(ValidationUtil.isEmpty(whereClause)){
                    throw new IllegalArgumentException("Picture Retrieve by where-clause error: A valid SQL where clause is required.");
                }
                queryStrBld = composeQueryForReadOrCount(true, pictureToRetrieve, retrieveBy);
                queryStrBld.append(" ");
                queryStrBld.append(whereClause.trim());
                queryStrBld.append(orderBy.getValue());
                sqlQuery = session.createSQLQuery(queryStrBld.toString());

            } else {
                throw new IllegalArgumentException("Picture Retrieve error: Unsupported operation type - " + retrieveBy);
            }            
            
            //Execute:
            sqlQuery.addEntity(Picture.class);
            listPicsFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(IllegalArgumentException ile) {
            throw ile;
        } catch(Exception e) {
            LOGGER.error("Picture Retrieve error: Service type ["+ retrieveBy + "] with message: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listPicsFound;
    }    
    
    /**
     * Counts Pictures from database given the criteria.
     * @param pictureToRetrieve - Contains required data data depending on the retrieveBy param such as:
     *  1. If PICTURE_RETRIEVE_BY_TRIPID, the bean must contain TripID
     *  2. If PICTURE_RETRIEVE_BY_USERNAME, the bean must contain Username and Trip privacy. 
     *     In addition, it accounts for params offset and numOfRows.
     *  
     * @param retrieveBy - Required.
     */
    public static int count(
            Session session, 
            Transaction transaction,
            Picture pictureToCount,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Picture Count error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int numFound = 0;
        Query sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID) {
                if(pictureToCount == null
                        || ValidationUtil.isEmpty(pictureToCount.getTripID())) {
                    throw new IllegalArgumentException("Picture Count by TripID error: The Trip ID must be provided!");
                }
                queryStrBld = composeQueryForReadOrCount(false, pictureToCount, retrieveBy);
                queryStrBld.append(" where TRIPID = :tripIdParam");
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.setString("tripIdParam", pictureToCount.getTripID());
            
            } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME) {
                if(pictureToCount == null
                        || ValidationUtil.isEmpty(pictureToCount.getUsername())
                        || pictureToCount.getPrivacy() == null) {
                    throw new IllegalArgumentException("Picture Count by Username error: The Username and Privacy must be provided!");
                }
                queryStrBld = composeQueryForReadOrCount(false, pictureToCount, retrieveBy);
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
    
            } else {
                throw new IllegalArgumentException("Picture Count error: Unsupported operation type - " + retrieveBy);
            }            
                    
            //Execute:
            numFound = ((BigInteger) sqlQuery.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Picture Count error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return numFound;
    }    
    
    /**
     * Updates a Picture.
     * 
     * @param pictureToUpdate
     *  The Picture bean containing ONLY the data to be updated. UserID and PictureID are required.
     *  1. If the property inside Picture bean is null, then this data will not be updated. 
     *  2. For required fields, if the data is empty string, then it will not be updated (cannot have empty values).
     *  3. But for non-required fields, if the data is empty string, then it will be updated as empty String; example Note.
     *  
     * @return Number of updated items.
     *         NOTE: In Hibernate scheme of things, if the row to be updated is not found, the 
     *         Query.executeUpdate() returns ZERO (no throw exception). 
     * @throws Exception
     */
    public static int update(
            Session session, 
            Transaction transaction,
            Picture pictureToUpdate) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Picture Update error: Database session is required!");
        }
        if(pictureToUpdate == null
                || ValidationUtil.isEmpty(pictureToUpdate.getUserID())
                || ValidationUtil.isEmpty(pictureToUpdate.getId())
                || ValidationUtil.isEmpty(pictureToUpdate.getUpdated())) {
            throw new IllegalArgumentException("Picture Update error: User ID, Picture ID and Updated date are required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int updatedItems = -1;
        Query query = null;
        try {
            /*
             * Compose query
             * NOTE: 
             * In order to support string with special chars and new line, we must use "parameterized query" 
             * (i.e., the query input parameters) and populate the values later after session.createQuery(). 
             * If we, instead, set the values directly at StringBuilder.append(), the createQuery() will 
             * remove new line and throw "unexpected token" exception for special chars.
             */
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("update Picture set ");
            hqlQueryStrBld.append("UPDATED = :updateParam");
            if(pictureToUpdate.getNote() != null) {
                //It can be set to empty string:
                hqlQueryStrBld.append(", NOTE = :pictureNoteParam");
            }
            if(pictureToUpdate.getCoverPicture() != null) {
                if(pictureToUpdate.getCoverPicture() == YesNoEnum.YES) {
                    hqlQueryStrBld.append(", IS_COVER_PICTURE = " + YesNoEnum.YES.getId());
                } else {
                    hqlQueryStrBld.append(", IS_COVER_PICTURE = " + YesNoEnum.NO.getId());
                }
            }
            if(pictureToUpdate.getFeatureForPlaceID() != null) {
                //It can be set to empty string:
                hqlQueryStrBld.append(", FEATURE_FOR_PLACEID = '" + pictureToUpdate.getFeatureForPlaceID() + "'");
            }
            hqlQueryStrBld.append(" where PICTUREID = :picIDParam and USERID = :userIDParam");

            query = session.createQuery(hqlQueryStrBld.toString());
            query.setString("updateParam", pictureToUpdate.getUpdated());
            query.setString("picIDParam", pictureToUpdate.getId());
            query.setString("userIDParam", pictureToUpdate.getUserID());
            if(pictureToUpdate.getNote() != null) {
                //It can be set to empty string:
                query.setString("pictureNoteParam", pictureToUpdate.getNote().trim());
            }

            //Execute:
            updatedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Picture Update error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }
        return updatedItems;
    }
    
    public static int delete(
            Session session, 
            Transaction transaction,
            Picture pictureToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Picture Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int deletedItems = -1;
        Query query = null;
        try {
            //Compose query:
            if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_USERID) {
                if(pictureToDelete == null
                        || ValidationUtil.isEmpty(pictureToDelete.getUserID())){
                    throw new IllegalArgumentException("Picture Delete by UserID error: The User ID must be provided!");
                }
                String hqlQueryStr = "delete Picture where USERID = :userIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", pictureToDelete.getUserID());
                
            } else if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_TRIPID) {
                if(pictureToDelete == null
                        || ValidationUtil.isEmpty(pictureToDelete.getUserID())
                        || ValidationUtil.isEmpty(pictureToDelete.getTripID())){
                    throw new IllegalArgumentException("Picture Delete by TripID error: The User ID and Trip ID must be provided!");
                }
                String hqlQueryStr = "delete Picture where USERID = :userIdParam and TRIPID = :tripIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", pictureToDelete.getUserID());
                query.setString("tripIdParam", pictureToDelete.getTripID());
                
            } else if(deleteBy == ServiceTypeEnum.PICTURE_DELETE_BY_PICTUREID) {
                if(pictureToDelete == null
                        || ValidationUtil.isEmpty(pictureToDelete.getUserID())
                        || ValidationUtil.isEmpty(pictureToDelete.getId())){
                    throw new IllegalArgumentException("Picture Delete by PictureID error: The UserID and Picture ID must be provided!");
                }
                String hqlQueryStr = "delete Picture where USERID = :userIdParam and PICTUREID = :picIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", pictureToDelete.getUserID());
                query.setString("picIdParam", pictureToDelete.getId());
                
            } else {
                throw new IllegalArgumentException("Picture Delete error: Unsupported operation type - " + deleteBy);
            }            

            deletedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Picture Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }        
        return deletedItems;
    }    
    
    private static StringBuilder composeQueryForReadOrCount(
            boolean isRead,
            Picture pictureToRetrieve,
            ServiceTypeEnum retrieveBy) {
        StringBuilder queryStrBld = new StringBuilder();
        if(isRead) {
            queryStrBld.append("select PICTURE.PICTUREID, PICTURE.USERID, PICTURE.TRIPID"); 
            queryStrBld.append(", PICTURE.IMAGE_NAME, PICTURE.IMAGE_URL, PICTURE.NOTE");
            queryStrBld.append(", PICTURE.IS_COVER_PICTURE, PICTURE.FEATURE_FOR_PLACEID");
            queryStrBld.append(", date_format(PICTURE.CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
            queryStrBld.append(", date_format(PICTURE.UPDATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as UPDATED");

            if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME
                    || retrieveBy == ServiceTypeEnum.PICTURE_SEARCH) {
                //Enrichments:
                //TODO (12/2015): Picture Enrichment needs refactor to get these data during PictureDBQuery.read()
                //for better performance.
                //Currently this is incomplete. We dont know yet how to store these results into
                //Picture bean. Currently the results are not mapped.
                queryStrBld.append(", USER.USERNAME as USER_USERNAME");
                queryStrBld.append(", USER.FULLNAME as USER_FULLNAME");
                queryStrBld.append(", USER.PROFILE_IMAGE_NAME as USER_PROFILE_IMAGE_NAME");
                queryStrBld.append(", TRIP.NAME as TRIP_NAME");
            }

        } else {
            //Count only:
            queryStrBld.append("select count(*)"); 
        }

        if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID
                || retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_TRIPID
                || retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERID
                || retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_WHERE_CLAUSE) {
            //Str8-forward from table:
            queryStrBld.append(" from PICTURE");
            
        } else if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME) {
            queryStrBld.append(" from USER, TRIP, PICTURE");
            queryStrBld.append(" where PICTURE.USERID = USER.USERID and PICTURE.TRIPID = TRIP.TRIPID");
            queryStrBld.append(" and USER.USERNAME = '" + pictureToRetrieve.getUsername() + "'");
            
            //Restrict privacy:
            switch(pictureToRetrieve.getPrivacy()) {
                case PUBLIC: 
                    queryStrBld.append(" and TRIP.PRIVACY = 1");
                    break;
                case FRIENDS:
                    queryStrBld.append(" and (TRIP.PRIVACY = 1 or TRIP.PRIVACY = 3)");
                    break;
                case PRIVATE:    
                    //No restriction, include ALL.    
            }
            
        } else if(retrieveBy == ServiceTypeEnum.PICTURE_SEARCH) {
            queryStrBld.append(" from USER, TRIP, PICTURE");
            queryStrBld.append(" where PICTURE.USERID = USER.USERID and PICTURE.TRIPID = TRIP.TRIPID");
            queryStrBld.append(" and TRIP.PRIVACY = 1");
        } 
        
        return queryStrBld;
    }    
}
