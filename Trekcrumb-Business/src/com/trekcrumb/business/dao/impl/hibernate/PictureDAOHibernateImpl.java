package com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import com.trekcrumb.business.dao.IPictureDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.PictureDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility;
import com.trekcrumb.common.bean.Picture;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public class PictureDAOHibernateImpl 
implements IPictureDAO {
    
    public Picture create(final Picture pictureToCreate) throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            PictureDBQuery.create(session, transaction, pictureToCreate);
            transaction.commit();
            
        } catch(ConstraintViolationException cve) {
            if (transaction != null) {
                transaction.rollback();
            }
            HibernateUtility.handleConstraintViolationException(
                    cve, "Picture Create", pictureToCreate.getUserID(), pictureToCreate.getTripID());
            
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
            
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
        return pictureToCreate;
    }
    
    public List<Picture> read(
            Picture pictureToRetrieve, 
            ServiceTypeEnum retrieveBy,
            String whereClause,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<Picture> listOfPictures = PictureDBQuery.read(
                    session, null, pictureToRetrieve, retrieveBy, null, orderBy, offset, numOfRows);
            if(listOfPictures == null || listOfPictures.size() == 0) {
                return null;
            }
            
            //Enrichments:
            //TODO (12/2015): Picture Enrichment needs refactor to get these data during PictureDBQuery.read()
            //for better performance
            if(retrieveBy == ServiceTypeEnum.PICTURE_RETRIEVE_BY_USERNAME
                    || retrieveBy == ServiceTypeEnum.PICTURE_SEARCH) {
                listOfPictures = HibernateUtility.pictureEnrichment(session, listOfPictures);
            }
            
            return listOfPictures;
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }

    public int count(
            Picture pictureToCount,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return PictureDBQuery.count(session, null, pictureToCount, retrieveBy);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
 
    public Picture update(Picture pictureToUpdate) throws Exception {
        Picture pictureUpdated = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            
            int numOfUpdated = PictureDBQuery.update(session, transaction, pictureToUpdate);
            if(numOfUpdated > 0) {
                transaction.commit();

                //Return the updated Picture:
                List<Picture> listOfPics = PictureDBQuery.read(
                        session, transaction, pictureToUpdate, 
                        ServiceTypeEnum.PICTURE_RETRIEVE_BY_PICTUREID, null, null, 
                        0, 1);
                if(listOfPics != null && listOfPics.size() > 0) {
                    pictureUpdated = listOfPics.get(0);
                }
            }
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
        return pictureUpdated;        
    }
    
    public int delete(
            Picture pictureToDelete, 
            ServiceTypeEnum deleteBy) throws Exception {
        int numDeleted = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            numDeleted = PictureDBQuery.delete(session, transaction, pictureToDelete, deleteBy);
            transaction.commit();
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
        return numDeleted;
    }


}
