package com.trekcrumb.business.dao.impl.hibernate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.trekcrumb.common.exception.TrekcrumbException;
 
public class HibernateManager {
    private static final Log log = LogFactory.getLog(HibernateManager.class.getName()); 
    
    /*
     * An app should only have ONE SessionFactory, but can have many Sessions for each thread.
     * Ref: http://docs.jboss.org/hibernate/orm/4.1/javadocs/org/hibernate/SessionFactory.html
     */
    private static SessionFactory mSessionFactory;
    
    /**
     * Make it private to enforce singleton.
     */
    private HibernateManager() {}
 
    /**
     * Return a static singleton SessionFactory.
     * If null (has not been initialized), it will build a new one from the hibernate config file. 
     */
    public static SessionFactory getSessionFactory() throws TrekcrumbException {
        if(mSessionFactory == null || mSessionFactory.isClosed()) {
            log.debug("\n*************************************************\n"
            		  + "HIBERNATEMANAGER - INITIATING SESSION FACTORY ...\n"
            		  + "*************************************************\n");
            try {
                //Configures settings from hibernate.cfg.xml
                Configuration configuration = new Configuration();
                configuration.configure(); 
                
                ServiceRegistry serviceRegistry = 
                        new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();        
                mSessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Throwable th) {
                String errMsg = "Error initializing Hibernate SessionFactory: " + th.getMessage();
                log.error(errMsg, th);
                throw new TrekcrumbException(errMsg);
            }            
        }
        return mSessionFactory;
    }
    
    /**
     * @return A new open Session from a singleton SessionFactory.
     */
    public static Session openSession() throws TrekcrumbException {
        try {
            Session session = getSessionFactory().openSession();
            
            /*
             * Set Session attributes:
             * - Flush mode = Manual. Since most tx is for 'read-only' data access. Setting this manual
             *                helps to improve performance. But the client must manually call flush()
             *                when necessary.
             * - Cache mode = Ignore. Since we don't need to have access to 2nd level cache.
             *                (Ref: https://docs.jboss.org/hibernate/orm/3.3/reference/en-US/html/performance.html#performance-cache)
             */
            session.setFlushMode(FlushMode.MANUAL);
            session.setCacheMode(CacheMode.IGNORE);
            return session;
        } catch (Throwable th) {
            String errMsg = "Error while trying to open a new DB Session: " + th.getMessage();
            log.error(errMsg);
            th.printStackTrace();
            throw new TrekcrumbException(errMsg);
        }            
    }    
 
    /**
     * @return A new open StatelessSession from a singleton SessionFactory.
     */
    public static StatelessSession openStatelessSession() throws TrekcrumbException {
        try {
            return getSessionFactory().openStatelessSession();
        } catch (Throwable th) {
            String errMsg = "Error while trying to open a new DB Stateless Session: " + th.getMessage();
            log.error(errMsg);
            th.printStackTrace();
            throw new TrekcrumbException(errMsg);
        }            
    }    

    /**
     * Closes and destroy this SessionFactory and release all resources (caches, connection pools, etc).
     * It will also closes the incoming session/stateless session.
     * 
     * NOTE: Any open Session/StatelessSession must be closed before the factory could be closed.
     */
    public static void shutdownSessionFactory(
            Session session,
            StatelessSession statelessSession) {
        shutdownSessions(session, statelessSession);
        
        if(mSessionFactory != null) {
            log.debug("\n*************************************************\n"
                      + "HIBERNATEMANAGER - SHUTDWON SESSION FACTORY ...\n"
                      + "*************************************************\n");
            try {
                mSessionFactory.close();
            } catch(Exception e) {
                //ignore
                log.error("Ignored error while trying to shutdown entire DB SessionFactory: " + e.getMessage());
            }
        }
    }

    /**
     * Closes any open Session (stateful or stateless), and releases JDBC connection.
     */
    public static void shutdownSessions(
            Session session,
            StatelessSession statelessSession) {
        if(session != null) {
            try {
                session.clear();
                session.disconnect();
                session.close();
                session = null;
            } catch(Exception e) {
                //ignore
                log.error("Ignored error while trying to shutdown DB Session: " + e.getMessage());
            }
        }
        if(statelessSession != null) {
            try {
                statelessSession.close();
                statelessSession = null;
            } catch(Exception e) {
                //ignore
                log.error("Ignored error while trying to shutdown DB StatelessSession: " + e.getMessage());
            }            
        }
    }    

}

