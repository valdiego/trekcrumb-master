package com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import com.trekcrumb.business.dao.IPlaceDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility;
import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public class PlaceDAOHibernateImpl 
implements IPlaceDAO {

    public Place create(Place placeToCreate) throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            PlaceDBQuery.create(session, transaction, placeToCreate);
            transaction.commit();
            
        } catch(ConstraintViolationException cve) {
            if (transaction != null) {
                transaction.rollback();
            }
            HibernateUtility.handleConstraintViolationException(
                    cve, "Place Create", placeToCreate.getUserID(), placeToCreate.getTripID());

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
        return placeToCreate;
    }
    
    public List<Place> read(
            Place placeToRetrieve, 
            ServiceTypeEnum retrieveBy,
            String whereClause) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<Place> listOfPlaces = PlaceDBQuery.read(session, null, placeToRetrieve, retrieveBy, null);
            if(listOfPlaces == null || listOfPlaces.size() == 0) {
                return null;
            }
            return listOfPlaces;
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }

    public Place update(Place placeToUpdate) throws Exception {
        Place placeUpdated = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();

            int numOfUpdated = PlaceDBQuery.update(session, transaction, placeToUpdate);
            if(numOfUpdated > 0) {
                transaction.commit();

                //Return the updated Place:
                List<Place> listOfPlaces = PlaceDBQuery.read(
                        session, transaction, placeToUpdate, ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID, null);
                if(listOfPlaces != null && listOfPlaces.size() > 0) {
                    placeUpdated = listOfPlaces.get(0);
                }
            }
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
        return placeUpdated;
    }
    
    public int delete(Place placeToDelete, ServiceTypeEnum deleteBy) throws Exception {
        int numDeleted = -1;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            numDeleted =  PlaceDBQuery.delete(session, transaction, placeToDelete, deleteBy);
            transaction.commit();
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        

        return numDeleted;
    }


}
