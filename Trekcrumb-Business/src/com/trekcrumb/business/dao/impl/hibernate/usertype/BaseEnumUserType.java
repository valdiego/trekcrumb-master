package com.trekcrumb.business.dao.impl.hibernate.usertype;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;
import org.hibernate.engine.spi.SessionImplementor;

import com.trekcrumb.common.enums.IEnum;

/**
 * Custom implementation of Hibernate UserType.
 * 
 * This class is to enable persisting Enum type into database by mapping (conversioning) both ways:
 * When inserting/updating entity into a table, and when retrieving record from table into entity bean.
 * 
 * This class is abstract to provide basic fundamental methods overriding those from the parent Usertype.
 * Each enum class would require its own UserType. However, with this abstract class they just need
 * to extend this class and MUST override the returnedClass() method. Acceptable enum class must be
 * the child of IEnum.
 * 
 * @param <T>
 */
public abstract class BaseEnumUserType<T extends IEnum> implements UserType {
    
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }
 
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }
 
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable)value;
    }
 
    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return x == y;
    }
 
    @Override
    public int hashCode(Object x) throws HibernateException {
        return x == null ? 0 : x.hashCode();
    }
 
    @Override
    public boolean isMutable() {
        return false;
    }
 
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
 
    @Override
    public int[] sqlTypes() {
        return new int[]{Types.INTEGER};
    }

    /**
     * This is the method that Hibernate will invoke after reading data from DB, 
     * and mapping its resultset to the entity bean.
     * 
     * Note: In this impl, the value that was stored in the DB is the enum ID, and therefore
     * will be mapped with the IEnum.getId().
     */
    @Override
    public Object nullSafeGet(
            ResultSet result, 
            String[] names,
            SessionImplementor sessionImplementor,
            Object owner)
            throws HibernateException, SQLException {
        int id = result.getInt(names[0]);
        if(result.wasNull()) {
            return null;
        }
        for(IEnum value : returnedClass().getEnumConstants()) {
            if(id == value.getId()) {
                return value;
            }
        }
        throw new IllegalStateException("Unknown " + returnedClass().getSimpleName() + " id");
    }
 
    /**
     * This is the method that Hibernate will invoke during insert/update/delete to map fields
     * from entity bean to SQL parameters. 
     * 
     * Note: In this impl, the value to be stored in the DB is IEnum.getId().
     * 
     */
    @Override
    public void nullSafeSet(
            PreparedStatement statement, 
            Object value, 
            int index,
            SessionImplementor sessionImplementor)
            throws HibernateException, SQLException {
        if (value == null) {
            statement.setNull(index, Types.INTEGER);
        } else {
            statement.setInt(index, ((IEnum)value).getId());
        }
    }
 
    /**
     * The abstract method that each child UserType class must override to return 
     * its specific enum class.
     */
    @Override
    public abstract Class<T> returnedClass();
 
}
