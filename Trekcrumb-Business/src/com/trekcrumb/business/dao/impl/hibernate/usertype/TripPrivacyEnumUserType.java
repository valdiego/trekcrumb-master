package com.trekcrumb.business.dao.impl.hibernate.usertype;

import com.trekcrumb.common.enums.TripPrivacyEnum;

/**
 * Concrete implementation of BaseEnumUserType for TripPrivacyEnum.
 *
 */
public class TripPrivacyEnumUserType extends BaseEnumUserType<TripPrivacyEnum> {

    @Override
    public Class<TripPrivacyEnum> returnedClass() {
        return TripPrivacyEnum.class;
    }    
}
