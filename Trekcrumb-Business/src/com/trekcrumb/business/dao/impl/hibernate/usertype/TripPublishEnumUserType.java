package com.trekcrumb.business.dao.impl.hibernate.usertype;

import com.trekcrumb.common.enums.TripPublishEnum;

/**
 * Concrete implementation of BaseEnumUserType for TripPublishEnum.
 *
 */
public class TripPublishEnumUserType extends BaseEnumUserType<TripPublishEnum> {

    @Override
    public Class<TripPublishEnum> returnedClass() {
        return TripPublishEnum.class;
    }    
}
