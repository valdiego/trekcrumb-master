package com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.business.dao.IFavoriteDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.FavoriteDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility;
import com.trekcrumb.common.bean.Favorite;
import com.trekcrumb.common.bean.Trip;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.OrderByEnum;

public class FavoriteDAOHibernateImpl 
implements IFavoriteDAO {
    
    public void create(Favorite favoriteToCreate) throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            FavoriteDBQuery.create(session, transaction, favoriteToCreate);
            transaction.commit();

        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    /**
     * Retrieve Favorites given Favorite's criteria.
     * @return List of Favorites found.
     */
    public List<Favorite> read(
            Favorite favoriteToRetrieve,
            OrderByEnum orderBy,
            int offset,
            int numOfRows) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<Favorite> listOfFavorites = 
                    FavoriteDBQuery.read(session, null, favoriteToRetrieve, orderBy, offset, numOfRows);
            if(listOfFavorites == null || listOfFavorites.size() == 0) {
                return null;
            }
            
            return listOfFavorites;
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }                
    }

    /**
     * Retrieve Users who favorite a Trip given this Trip's criteria.
     * @return List of Users found.
     */
    public List<User> readByTrip(
            Trip trip,
            int offset,
            int numOfRows) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<User> listOUsers = 
                    FavoriteDBQuery.readByTrip(session, null, trip, offset, numOfRows);
            if(listOUsers == null || listOUsers.size() == 0) {
                return null;
            }
            
            return listOUsers;
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }                
    }
    
    /**
     * Retrieve Trips whom a User marks as his/her favorites given this User's criteria.
     * @return List of Trips found.
     */
    public List<Trip> readByUser(
            User user,
            int offset,
            int numOfRows) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<Trip> listOfTrips = 
                    FavoriteDBQuery.readByUser(session, null, user, offset, numOfRows);
            if(listOfTrips == null || listOfTrips.size() == 0) {
                return null;
            }
            
            //Enrichments:
            listOfTrips = HibernateUtility.tripEnrichment(session, listOfTrips, null);        
            return listOfTrips;
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }

    public int count(Favorite favoriteToCount) throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return FavoriteDBQuery.count(session, null, favoriteToCount);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    public int countByUser(User user) throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return FavoriteDBQuery.countByUser(session, null, user);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }

    public int delete(Favorite favoriteToDelete) throws Exception {
        int numDeleted = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            numDeleted = FavoriteDBQuery.delete(session, transaction, favoriteToDelete);
            transaction.commit();
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }
        
        return numDeleted;
    }



}
