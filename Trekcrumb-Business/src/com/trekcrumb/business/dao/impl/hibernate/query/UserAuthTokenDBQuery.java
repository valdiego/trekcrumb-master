package com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: USER_AUTH_TOKEN.
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class UserAuthTokenDBQuery {
    private final static Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQuery");

    public static void create(
            Session session,
            Transaction transaction,
            UserAuthToken userAuthToken) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("UserAuthToken Create error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        try {
            //Put dummy to avoid inserting null:
            userAuthToken.setId(CommonConstants.STRING_VALUE_EMPTY);

            //Execute:
            session.save(userAuthToken);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Create error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
    }
    
    @SuppressWarnings({ "unchecked" })
    public static List<UserAuthToken> read(
            Session session,
            Transaction transaction,
            UserAuthToken userAuthToken,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("UserAuthToken Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        List<UserAuthToken> userAuthTokenList = null;
        SQLQuery sqlQuery = null;
        try {
            //Compose query
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("select USER_AUTH_TOKEN_ID, USERID, USERNAME, DEVICE_IDENTITY, AUTH_TOKEN"); 
            hqlQueryStrBld.append(", date_format(CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
            hqlQueryStrBld.append(" from USER_AUTH_TOKEN");

            if(retrieveBy == ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_USERID) {
                if(ValidationUtil.isEmpty(userAuthToken.getUserId())) {
                    throw new IllegalArgumentException("UserAuthToken Read by userID error: User ID is required!");
                }
                hqlQueryStrBld.append(" where USERID = :userIdParam");
                sqlQuery = session.createSQLQuery(hqlQueryStrBld.toString()).addEntity(UserAuthToken.class);
                sqlQuery.setString("userIdParam", userAuthToken.getUserId());
                
            } else if(retrieveBy == ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY) {
                if(ValidationUtil.isEmpty(userAuthToken.getUserId())
                        || ValidationUtil.isEmpty(userAuthToken.getDeviceIdentity())) {
                    throw new IllegalArgumentException("UserAuthToken Read by DeviceIdentity error: User ID and Device Identity are required!");
                }
                hqlQueryStrBld.append(" where USERID = :userIdParam and DEVICE_IDENTITY = :deviceIdentityParam");
                sqlQuery = session.createSQLQuery(hqlQueryStrBld.toString()).addEntity(UserAuthToken.class);
                sqlQuery.setString("userIdParam", userAuthToken.getUserId());
                sqlQuery.setString("deviceIdentityParam", userAuthToken.getDeviceIdentity());
                
            } else if(retrieveBy == ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN) {
                if(ValidationUtil.isEmpty(userAuthToken.getUserId())
                        || ValidationUtil.isEmpty(userAuthToken.getAuthToken())) {
                    throw new IllegalArgumentException("UserAuthToken Read by AuthToken error: User ID and AuthToken are required!");
                }
                hqlQueryStrBld.append(" where USERID = :userIdParam and AUTH_TOKEN = :authTokenParam");
                sqlQuery = session.createSQLQuery(hqlQueryStrBld.toString()).addEntity(UserAuthToken.class);
                sqlQuery.setString("userIdParam", userAuthToken.getUserId());
                sqlQuery.setString("authTokenParam", userAuthToken.getAuthToken());
            } else {
                throw new IllegalArgumentException("UserAuthToken Read error: Unsupported operation type = " + retrieveBy);
            }            
            
            //Execute:
            userAuthTokenList = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Read error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return userAuthTokenList;
    }
    
    public static int delete(
            Session session,
            Transaction transaction,
            UserAuthToken userAuthToken,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("UserAuthToken Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int deletedItems = -1;
        Query query = null;
        try {
            //Compose query:
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("delete UserAuthToken"); 
            
            if(deleteBy == ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID) {
                if(ValidationUtil.isEmpty(userAuthToken.getUserId())) {
                    throw new IllegalArgumentException("UserAuthToken Delete by User error: User ID is required!");
                }
                hqlQueryStrBld.append(" where USERID = :userIdParam");
                query = session.createQuery(hqlQueryStrBld.toString());
                query.setString("userIdParam", userAuthToken.getUserId());
                
            } else if(deleteBy == ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_AUTHTOKENID) {
                if(ValidationUtil.isEmpty(userAuthToken.getId())) {
                    throw new IllegalArgumentException("UserAuthToken Delete by AuthToken error: userAuthToken ID is required!");
                }
                hqlQueryStrBld.append(" where USER_AUTH_TOKEN_ID = :userAuthTokenIdParam");
                query = session.createQuery(hqlQueryStrBld.toString());
                query.setString("userAuthTokenIdParam", userAuthToken.getId());

            } else {
                throw new IllegalArgumentException("UserAuthToken Delete error: Unsupported operation type = " + deleteBy);
            }
            
            //Execute:
            deletedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }        
        return deletedItems;
    }    
    
    /**
     * Updates userAuthToken entry for the Auth Token.
     * Note: For existing record, UserID and DeviceIdentity cannot be updated (changed).

     * @param userAuthToken contains data of the userAuthToken ID to be updated, and 
     *        the new Auth Token.
     *        
     * @return Number of records updated.
     *         NOTE: In Hibernate scheme of things, if the row to be updated is not found, the 
     *         Query.executeUpdate() returns ZERO (no throw exception). 
     */
    public static int update(
            Session session,
            Transaction transaction,
            UserAuthToken userAuthToken) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("UserAuthToken Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int updatedItems = -1;
        Query query = null;
        try {
            //Compose query:
            StringBuffer strBuffQuery = new StringBuffer();
            strBuffQuery.append("update UserAuthToken set ");
            strBuffQuery.append("AUTH_TOKEN = :authTokenParam ");
            strBuffQuery.append("where USER_AUTH_TOKEN_ID = :userAuthTokenIDParam");
            String hqlQuery = strBuffQuery.toString();
            query = session.createQuery(hqlQuery);
            query.setString("authTokenParam", userAuthToken.getAuthToken());
            query.setString("userAuthTokenIDParam", userAuthToken.getId());

            //Execute:
            updatedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("UserAuthToken Update error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            query = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return updatedItems;
    }
}
