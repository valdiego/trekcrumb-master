package com.trekcrumb.business.dao.impl.hibernate.usertype;

import com.trekcrumb.common.enums.TripStatusEnum;

/**
 * Concrete implementation of BaseEnumUserType for TripStatusEnum.
 *
 */
public class TripStatusEnumUserType extends BaseEnumUserType<TripStatusEnum> {

    @Override
    public Class<TripStatusEnum> returnedClass() {
        return TripStatusEnum.class;
    }    
}
