package com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: USER
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class UserDBQuery {
    private final static Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery");
    private final static String ORDER_BY = " order by UPDATED desc";
    
    public static void create(
            Session session, 
            Transaction transaction,
            User userToCreate) throws Exception {
        if(session == null 
                || !session.isOpen()) {
            throw new IllegalArgumentException("User Create error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        try {
            //USERID: DB will auto-generate ID and it is NOT_NULL column, we must assign a dummy String:
            userToCreate.setUserId(CommonConstants.STRING_VALUE_EMPTY);
            
            session.save(userToCreate);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("User Create error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }
    }
    
    /**
     * Retrieves User(s) from database.
     * 
     * @param userToRead
     * @param retrieveBy
     * @param whereClause
     * @param offset - The starting index of the row. Default is 0 (the start of the record). If 
     *                 numOfRows <= 0, this is ignored and the default is used.
     * @param numOfRows - If value is > 0, then the method will return limited rows from offset
     *                    up to the number of rows specified. If numOfRows <= 0 any negative,
     *                    the method will return all records.
     */
    @SuppressWarnings("unchecked")
    public static List<User> read(
            Session session,
            Transaction transaction,
            User userToRead,
            ServiceTypeEnum retrieveBy,
            String whereClause,
            int offset,
            int numOfRows) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("User Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        List<User> listUsersFound = null;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder queryStrBld = new StringBuilder();
            queryStrBld.append("select USERID, USERNAME, EMAIL, PASSWORD, FULLNAME, SUMMARY, LOCATION, IS_ACTIVE, IS_CONFIRM"); 
            queryStrBld.append(", PROFILE_IMAGE_NAME, PROFILE_IMAGE_URL");
            queryStrBld.append(", SECURITY_TOKEN");
            queryStrBld.append(", date_format(CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
            queryStrBld.append(", date_format(UPDATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as UPDATED"); 
            queryStrBld.append(" from USER");
            
            if(retrieveBy == ServiceTypeEnum.USER_RETRIEVE_BY_USERID) {
                if(userToRead == null || ValidationUtil.isEmpty(userToRead.getUserId())){
                    throw new IllegalArgumentException("User Read by UserID error: The User ID must be provided.");
                }
                queryStrBld.append(" WHERE userId = :userIdParam");
                queryStrBld.append(ORDER_BY);
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.addEntity(User.class);
                sqlQuery.setString("userIdParam", userToRead.getUserId());
                
            } else if(retrieveBy == ServiceTypeEnum.USER_RETRIEVE_BY_USERNAME) {
                if(userToRead == null || ValidationUtil.isEmpty(userToRead.getUsername())){
                    throw new IllegalArgumentException("User Read by username error: The Username must be provided.");
                }
                queryStrBld.append(" WHERE username = :usernameParam");
                queryStrBld.append(ORDER_BY);
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.addEntity(User.class);
                sqlQuery.setString("usernameParam", userToRead.getUsername());

            } else if(retrieveBy == ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL) {
                if(userToRead == null || ValidationUtil.isEmpty(userToRead.getEmail())){
                    throw new IllegalArgumentException("User Read by email error: The Email must be provided.");
                }
                queryStrBld.append(" WHERE email = :emailParam");
                queryStrBld.append(ORDER_BY);
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.addEntity(User.class);
                sqlQuery.setString("emailParam", userToRead.getEmail());

            } else if(retrieveBy == ServiceTypeEnum.TRIP_RETRIEVE_BY_WHERE_CLAUSE) {
                if(userToRead == null || ValidationUtil.isEmpty(whereClause)){
                    throw new IllegalArgumentException("User Read error: A valid SQL where-clause is required!");
                }
                queryStrBld.append(" ");
                queryStrBld.append(whereClause.trim());
                queryStrBld.append(ORDER_BY);
                sqlQuery = session.createSQLQuery(queryStrBld.toString());
                sqlQuery.addEntity(User.class);
                
            } else {
                throw new IllegalArgumentException("User Read error: Unsupported operation type = " + retrieveBy);
            }            
            
            //Limit query results: TODO
            
            //Execute:
            listUsersFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("User Read error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }
        return listUsersFound;
    }    
    
    public static int delete(
            Session session,
            Transaction transaction,
            User userToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("User Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int deletedItems = -1;
        Query query = null;
        try {
            //Compose query:
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("delete User"); 
            if(deleteBy == ServiceTypeEnum.USER_DELETE_BY_USERID) {
                if(ValidationUtil.isEmpty(userToDelete.getUserId())){
                    throw new IllegalArgumentException("User Delete by UserID error: The User ID must be provided!");
                }
                hqlQueryStrBld.append(" where USERID = :userIdParam");
                query = session.createQuery(hqlQueryStrBld.toString());
                query.setString("userIdParam", userToDelete.getUserId());
            } else if(deleteBy == ServiceTypeEnum.USER_DELETE_BY_USERNAME) {
                if(ValidationUtil.isEmpty(userToDelete.getUsername())){
                    throw new IllegalArgumentException("User Delete by Username error: The Username must be provided!");
                }
                hqlQueryStrBld.append(" where USERNAME = :usernameParam");
                query = session.createQuery(hqlQueryStrBld.toString());
                query.setString("usernameParam", userToDelete.getUsername());
            } else {
                throw new IllegalArgumentException("User Delete error: Unsupported operation type = " + deleteBy);
            }
            
            //Execute:
            deletedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("User Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            query = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }
        return deletedItems;
    }
    
    /**
     * Updates the existing user data in DB.
     * Including: email, password and other user properties. If email is to update, the method
     *            will validate that new email does not already exist in the system.
     * Exluding: UserID and Username are NOT editable by calling app.
     *
     * @param userInput - Must contain the user's UserID in question to be updated. 
     *                    The user bean contains only the updated data only. Other data not to
     *                    be updated should be null.
     *                    
     * @return Number of row(s) updated.
     *         NOTE: In Hibernate scheme of things, if the row to be updated is not found, the 
     *         Query.executeUpdate() returns ZERO (no throw exception). 
     */
    public static int update(
            Session session,
            Transaction transaction,
            User userInput) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("User Update error: Database session is required!");
        }
        if(userInput == null || ValidationUtil.isEmpty(userInput.getUserId())) {
            throw new IllegalArgumentException("User Update error: UserID is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }
        
        int updatedItems = -1;
        Query query = null;
        try {
            /*
             * Compose query
             * NOTE: 
             * In order to support string with special chars and new line, we must use "parameterized query" 
             * (i.e., the query input parameters) and populate the values later after session.createQuery(). 
             * If we, instead, set the values directly at StringBuilder.append(), the createQuery() will 
             * remove new line and throw "unexpected token" exception for special chars.
             */
            StringBuffer strBuffQuery = new StringBuffer();
            strBuffQuery.append("update User set ");
            strBuffQuery.append("USERID = '" + userInput.getUserId() + "'");
            if(!ValidationUtil.isEmpty(userInput.getEmail())) {
                strBuffQuery.append(", EMAIL = :emailParam");
            }
            if(!ValidationUtil.isEmpty(userInput.getPassword())) {
                strBuffQuery.append(", PASSWORD = :passwordParam");
            }
            if(!ValidationUtil.isEmpty(userInput.getFullname())) {
                strBuffQuery.append(", FULLNAME = :fullnameParam");
            }
            if(userInput.getSummary() != null) {
                //It can be set to empty string:
                strBuffQuery.append(", SUMMARY = :summaryParam");
            }
            if(userInput.getLocation() != null) {
                //It can be set to empty string:
                strBuffQuery.append(", LOCATION = :locationParam");
            }
            if(userInput.getProfileImageName() != null) {
                strBuffQuery.append(", PROFILE_IMAGE_Name = :ProfileImgNameParam");
            }
            if(userInput.getProfileImageURL() != null) {
                strBuffQuery.append(", PROFILE_IMAGE_URL = :ProfileImgURLParam");
            }
            if(userInput.getActive() != null) {
                strBuffQuery.append(", IS_ACTIVE = :activeParam");
            }
            if(userInput.getConfirmed() != null) {
                strBuffQuery.append(", IS_CONFIRM = :confirmedParam");
            }
            if(userInput.getSecurityToken() != null) {
                //Allow to persist securityToken when it is non-null or empty String:
                strBuffQuery.append(", SECURITY_TOKEN = :securityTokenParam");
            }
            strBuffQuery.append(" where USERID = :userIDParam");

            query = session.createQuery(strBuffQuery.toString());

            //Fill in the values:
            if(!ValidationUtil.isEmpty(userInput.getEmail())) {
                query.setString("emailParam", userInput.getEmail());
            }
            if(!ValidationUtil.isEmpty(userInput.getPassword())) {
                query.setString("passwordParam", userInput.getPassword());
            }
            if(!ValidationUtil.isEmpty(userInput.getFullname())) {
                query.setString("fullnameParam", userInput.getFullname().trim());
            }
            if(userInput.getSummary() != null) {
                //It can be set to empty string:
                query.setString("summaryParam", userInput.getSummary().trim());
            }
            if(userInput.getLocation() != null) {
                //It can be set to empty string:
                query.setString("locationParam", userInput.getLocation().trim());
            }
            if(userInput.getProfileImageName() != null) {
                query.setString("ProfileImgNameParam", userInput.getProfileImageName());
            }
            if(userInput.getProfileImageURL() != null) {
                query.setString("ProfileImgURLParam", userInput.getProfileImageURL());
            }
            if(userInput.getActive() != null) {
                query.setInteger("activeParam", userInput.getActive().getId());
            }
            if(userInput.getConfirmed() != null) {
                query.setInteger("confirmedParam", userInput.getConfirmed().getId());
            }
            if(userInput.getSecurityToken() != null) {
                //Allow to persist securityToken when it is non-null or empty String:
                query.setString("securityTokenParam", userInput.getSecurityToken().trim());
            }
            query.setString("userIDParam", userInput.getUserId());
            
            //Execute:
            updatedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("User Update error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            query = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }
        return updatedItems;
    }
    
    
}
