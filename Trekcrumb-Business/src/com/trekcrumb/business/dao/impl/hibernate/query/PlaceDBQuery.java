package com.trekcrumb.business.dao.impl.hibernate.query;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.common.bean.Place;
import com.trekcrumb.common.enums.OrderByEnum;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.utility.CommonConstants;
import com.trekcrumb.common.utility.ValidationUtil;

/**
 * Provides atomic (single) access operation to create/read/update/delete to database table: PLACE
 * - A database connection (Session) must be provided and managed by calling components.
 * - If a client provides active Transaction in the parameter, the client will be responsible managing
 *   transaction commit/rollback/clean up. If not provided, each method will manage
 *   the transaction locally within the scope of each method.
 */
public class PlaceDBQuery {
    private final static Log LOGGER = LogFactory.getLog("com.trekcrumb.business.dao.impl.hibernate.query.PlaceDBQuery"); 

    public static void create(
            Session session, 
            Transaction transaction,
            Place placeToCreate) throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Place Create error: Database session is required!");
        }
        if(placeToCreate == null) {
            throw new IllegalArgumentException("Place Create error: A complete Place bean to create is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        try {
            session.save(placeToCreate);
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Place Create error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
    }
    
    /**
     * Retrieves Places.
     * By default, we always order by created date ascending: First created will be the first in line.
     */
    @SuppressWarnings("unchecked")
    public static List<Place> read(
            Session session, 
            Transaction transaction,
            Place placeToRetrieve,
            ServiceTypeEnum retrieveBy,
            String whereClause) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Place Read error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        List<Place> listPlacesFound = null;
        SQLQuery sqlQuery = null;
        try {
            //Compose query:
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("select PLACEID, USERID, TRIPID, NOTE, LOCATION, LATITUDE, LONGITUDE"); 
            hqlQueryStrBld.append(", date_format(CREATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as CREATED");
            hqlQueryStrBld.append(", date_format(UPDATED, " + CommonConstants.DATE_FORMAT_DB_RETRIEVE_FROM_MYSQL + ") as UPDATED");
            hqlQueryStrBld.append(" from PLACE");

            if(retrieveBy == ServiceTypeEnum.PLACE_RETRIEVE_BY_TRIPID) {
                if(placeToRetrieve == null
                        || ValidationUtil.isEmpty(placeToRetrieve.getTripID())){
                    throw new IllegalArgumentException("Place Retrieve by TripID error: The TripID must be provided!");
                }
                hqlQueryStrBld.append(" where TRIPID = :tripIdParam");
                hqlQueryStrBld.append(OrderByEnum.ORDER_BY_CREATED_DATE_ASC.getValue());
                sqlQuery = session.createSQLQuery(hqlQueryStrBld.toString());
                sqlQuery.setString("tripIdParam", placeToRetrieve.getTripID());
                
            } else if(retrieveBy == ServiceTypeEnum.PLACE_RETRIEVE_BY_PLACEID) {
                if(placeToRetrieve == null 
                        || ValidationUtil.isEmpty(placeToRetrieve.getId())
                        || ValidationUtil.isEmpty(placeToRetrieve.getTripID())){
                     throw new IllegalArgumentException("Place Retrieve by PlaceID error: The TripID and PlaceID must be provided!");
                 }
                hqlQueryStrBld.append(" where TRIPID = :tripIdParam and PLACEID = :placeIdParam");
                hqlQueryStrBld.append(OrderByEnum.ORDER_BY_CREATED_DATE_ASC.getValue());
                sqlQuery = session.createSQLQuery(hqlQueryStrBld.toString());
                sqlQuery.setString("placeIdParam", placeToRetrieve.getId());
                sqlQuery.setString("tripIdParam", placeToRetrieve.getTripID());

            } else if(retrieveBy == ServiceTypeEnum.PLACE_RETRIEVE_BY_WHERE_CLAUSE) {
                if(ValidationUtil.isEmpty(whereClause)){
                    throw new IllegalArgumentException("Place Retrieve by where-clause error: A valid SQL where clause is required.");
                }
                hqlQueryStrBld.append(" ");
                hqlQueryStrBld.append(whereClause.trim());
                hqlQueryStrBld.append(OrderByEnum.ORDER_BY_CREATED_DATE_ASC.getValue());
                sqlQuery = session.createSQLQuery(hqlQueryStrBld.toString());

            } else {
                throw new IllegalArgumentException("Place Retrieve error: Unsupported operation type - " + retrieveBy);
            }            
            
            //Execute:
            sqlQuery.addEntity(Place.class);
            listPlacesFound = sqlQuery.list();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(IllegalArgumentException ile) {
            throw ile;
        } catch(Exception e) {
            LOGGER.error("Place Retrieve error: " + e.getMessage(), e);
            throw e;
        } finally {
            sqlQuery = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        
        return listPlacesFound;
    }    
    
    /**
     * Updates a Place.
     * 
     * @param placeToUpdate
     *  The Place bean containing ONLY the data to be updated. UserID and PlaceID are required.
     *  1. If the property inside Place bean is null, then this data will not be updated. 
     *  2. For required fields, if the data is empty string, then it will not be updated (cannot have empty values).
     *  3. But for non-required fields, if the data is empty string, then it will be updated as empty String; example Place Note.
     *  4. Place's latitude, longitude and location values cannot be updated.
     *  
     * @return Number of updated items.
     *         NOTE: In Hibernate scheme of things, if the row to be updated is not found, the 
     *         Query.executeUpdate() returns ZERO (no throw exception). 
     * @throws Exception
     */
    public static int update(
            Session session, 
            Transaction transaction,
            Place placeToUpdate) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Place Update error: Database session is required!");
        }
        if(placeToUpdate == null
                || ValidationUtil.isEmpty(placeToUpdate.getUserID())
                || ValidationUtil.isEmpty(placeToUpdate.getId())
                || ValidationUtil.isEmpty(placeToUpdate.getUpdated())) {
            throw new IllegalArgumentException("Place Update error: UserID, PlaceID and updated date are required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int updatedItems = -1;
        Query query = null;
        try {
            /*
             * Compose query
             * NOTE: 
             * In order to support string with special chars and new line, we must use "parameterized query" 
             * (i.e., the query input parameters) and populate the values later after session.createQuery(). 
             * If we, instead, set the values directly at StringBuilder.append(), the createQuery() will 
             * remove new line and throw "unexpected token" exception for special chars.
             */
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("update Place set ");
            hqlQueryStrBld.append("UPDATED = :updateParam");
            if(placeToUpdate.getNote() != null) {
                //It can be empty string:
                hqlQueryStrBld.append(", NOTE = :placeNoteParam");
            }
            hqlQueryStrBld.append(" where PLACEID = :placeIDParam and USERID = :userIDParam");

            query = session.createQuery(hqlQueryStrBld.toString());

            //Fill in the values:
            query.setString("placeIDParam", placeToUpdate.getId());
            query.setString("userIDParam", placeToUpdate.getUserID());
            query.setString("updateParam", placeToUpdate.getUpdated());
            if(placeToUpdate.getNote() != null) {
                //It can be empty string:
                query.setString("placeNoteParam", placeToUpdate.getNote().trim());
            }

            //Execute:
            updatedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Place Update error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }
        
        return updatedItems;
    }
    
    public static int delete(
            Session session, 
            Transaction transaction,
            Place placeToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Place Delete error: Database session is required!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int deletedItems = -1;
        Query query = null;
        try {
            //Compose query:
            if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_USERID) {
                if(placeToDelete == null
                        || ValidationUtil.isEmpty(placeToDelete.getUserID())){
                    throw new IllegalArgumentException("Place Delete by UserID error: The UserID must be provided!");
                }
                String hqlQueryStr = "delete Place where USERID = :userIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", placeToDelete.getUserID());
            } else if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_TRIPID) {
                if(placeToDelete == null
                        || ValidationUtil.isEmpty(placeToDelete.getUserID())
                        || ValidationUtil.isEmpty(placeToDelete.getTripID())){
                    throw new IllegalArgumentException("Place Delete by TripID error: The UserID and TripID must be provided!");
                }
                String hqlQueryStr = "delete Place where USERID = :userIdParam and TRIPID = :tripIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", placeToDelete.getUserID());
                query.setString("tripIdParam", placeToDelete.getTripID());
                
            } else if(deleteBy == ServiceTypeEnum.PLACE_DELETE_BY_PLACEID) {
                if(placeToDelete == null
                        || ValidationUtil.isEmpty(placeToDelete.getUserID())
                        || ValidationUtil.isEmpty(placeToDelete.getId())){
                    throw new IllegalArgumentException("Place Delete by PlaceID error: The UserID and Place ID must be provided!");
                }
                String hqlQueryStr = "delete Place where USERID = :userIdParam and PLACEID = :placeIdParam";
                query = session.createQuery(hqlQueryStr);
                query.setString("userIdParam", placeToDelete.getUserID());
                query.setString("placeIdParam", placeToDelete.getId());
                
            } else {
                throw new IllegalArgumentException("Place Delete error: Unsupported operation type - " + deleteBy);
            }            

            deletedItems = query.executeUpdate();
            session.flush();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Place Delete error: " + e.getMessage(), e);
            if(isTransactionLocal) {
                if (transaction != null) {
                    transaction.rollback();
                }
            }
            throw e;
        } finally {
            if(isTransactionLocal) {
                transaction = null;
            }
            query = null;
        }        
        return deletedItems;
    }    
    
    /**
     * Counts a number of Places for a given TripID.
     * @param placeToCount - A bean that contains the TripID in question.
     */
    public static int count(
            Session session, 
            Transaction transaction,
            Place placeToCount) 
            throws Exception {
        if(session == null || !session.isOpen()) {
            throw new IllegalArgumentException("Place Count error: Database session is required!");
        }
        if(placeToCount == null
                || ValidationUtil.isEmpty(placeToCount.getTripID())){
            throw new IllegalArgumentException("Place Count error: TripID must be provided!");
        }
        boolean isTransactionLocal = false;
        if(transaction == null) {
            transaction = session.beginTransaction();
            isTransactionLocal = true;
        }

        int numOfPlaces = -1;
        Query query = null;
        try {
            //Compose query:
            StringBuilder hqlQueryStrBld = new StringBuilder();
            hqlQueryStrBld.append("select count(*) from Place"); 
            hqlQueryStrBld.append(" where TRIPID = :tripIdParam"); 
            query = session.createQuery(hqlQueryStrBld.toString());
            query.setString("tripIdParam", placeToCount.getTripID());
            
            //Execute:
            numOfPlaces = ((Long)query.uniqueResult()).intValue();
            if(isTransactionLocal) {
                transaction.commit();
            }
        } catch(Exception e) {
            LOGGER.error("Place Count error: " + e.getMessage(), e);
            throw e;
        } finally {
            query = null;
            if(isTransactionLocal) {
                transaction = null;
            }
        }        
        return numOfPlaces;
    }

}
