package com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.business.dao.IUserAuthTokenDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility;
import com.trekcrumb.business.utility.BusinessUtil;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.exception.TrekcrumbException;
import com.trekcrumb.common.exception.UserNotFoundException;

public class UserAuthTokenDAOHibernateImpl
implements IUserAuthTokenDAO {
    
    @Override
    public UserAuthToken create(UserAuthToken userAuthTokenToCreate) throws Exception {
        UserAuthToken userAuthTokenCreated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            List<UserAuthToken> userAuthTokenFoundList = 
                    UserAuthTokenDBQuery.read(session, null, userAuthTokenToCreate, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_DEVICEIDENTITY);
            if(userAuthTokenFoundList == null 
                    || userAuthTokenFoundList.size() == 0) {
                //Create new:
                UserAuthTokenDBQuery.create(session, null, userAuthTokenToCreate);
            } else {
                //Update existing one (given its ID):
                UserAuthToken userAuthTokenFound = userAuthTokenFoundList.get(0);
                userAuthTokenToCreate.setId(userAuthTokenFound.getId());
                UserAuthTokenDBQuery.update(session, null, userAuthTokenToCreate);
            }

            /*
             * NOTE (11/2014): 
             * Without clearing the Session, the re-read same data after update will return old data
             */
            session.clear();

            //Read DB:
            userAuthTokenFoundList = 
                    UserAuthTokenDBQuery.read(session, null, userAuthTokenToCreate, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
            if(userAuthTokenFoundList == null 
                    || userAuthTokenFoundList.size() == 0) {
                throw new TrekcrumbException("UserAuthToken Create error: Failed to retrieve UserAuthToken after creation!");
            }
            userAuthTokenCreated = userAuthTokenFoundList.get(0);
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return userAuthTokenCreated;
    }
    
    @Override
    public List<UserAuthToken> read(
            UserAuthToken userAuthTokenToRead,
            ServiceTypeEnum retrieveBy) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return UserAuthTokenDBQuery.read(session, null, userAuthTokenToRead, retrieveBy);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
    }
    
    @Override
    public int delete(
            UserAuthToken userAuthTokenInput,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        Session session = null;
        try {
            session = HibernateManager.openSession();
            return UserAuthTokenDBQuery.delete(session, null, userAuthTokenInput, deleteBy);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }
    }
    
    @Override
    public int update(UserAuthToken userAuthTokenWithUpdates) throws Exception {
        throw new OperationNotSupportedException("Operation is not supported!");
    }

    @Override
    public User authenticate(UserAuthToken userAuthTokenInput) throws Exception {
        User userAuthenticated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();
            
            Transaction transaction = null;
            try {
                transaction = session.beginTransaction();
                
                List<UserAuthToken> authResult = UserAuthTokenDBQuery.read(
                        session, transaction, userAuthTokenInput, ServiceTypeEnum.USERAUTHTOKEN_RETRIEVE_BY_AUTHTOKEN);
                if(authResult == null || authResult.size() == 0) {
                    throw new UserNotFoundException("UserAuthToken Authenticate error: Failed given the user auth token!");
                }
                
                //Success. Now look for the User and return it:
                User userToRetrieve = new User();
                userToRetrieve.setUserId(userAuthTokenInput.getUserId());
                List<User> listUsersFound = UserDBQuery.read(
                        session, transaction, userToRetrieve, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1);
                if(listUsersFound == null || listUsersFound.size() == 0 || listUsersFound.get(0) == null) {
                    throw new UserNotFoundException("UserAuthToken Authenticate error: After authenticated, failed to find the User given userID!");
                }
                userAuthenticated = listUsersFound.get(0);

                transaction.commit();
            } finally {
                transaction = null;
            }
            
            //Enrichment:
            userAuthenticated = HibernateUtility.userEnrichment(session, userAuthenticated, true, false); 
            BusinessUtil.secureUserData(userAuthenticated, true);
            
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return userAuthenticated;
    }

    


}
