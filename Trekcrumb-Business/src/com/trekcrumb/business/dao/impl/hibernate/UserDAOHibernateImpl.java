package com.trekcrumb.business.dao.impl.hibernate;

import java.util.List;

import javax.naming.OperationNotSupportedException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.trekcrumb.business.dao.IUserDAO;
import com.trekcrumb.business.dao.impl.hibernate.query.UserAuthTokenDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.query.UserDBQuery;
import com.trekcrumb.business.dao.impl.hibernate.utility.HibernateUtility;
import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;
import com.trekcrumb.common.enums.YesNoEnum;
import com.trekcrumb.common.exception.UserDuplicateException;
import com.trekcrumb.common.exception.UserNotFoundException;

public class UserDAOHibernateImpl 
implements IUserDAO {
    
    @Override
    public User create(User userToCreate) throws Exception {
        User userCreated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Validate duplicate:
            if(HibernateUtility.verifyUsernameOrEmailExist(session, userToCreate)) {
                throw new UserDuplicateException("User Create error: Either username or email already exists on the system!");
            }

            //Write DB:
            UserDBQuery.create(session, null, userToCreate);
                
            //Read DB:
            List<User> usersFound = UserDBQuery.read(session, null, userToCreate, ServiceTypeEnum.USER_RETRIEVE_BY_EMAIL, null, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Create error: Fail to find user after it is created in the system for email [" 
                        + userToCreate.getEmail() + "]!");
            }
            userCreated = usersFound.get(0);
            userCreated = HibernateUtility.userEnrichment(session, userCreated, true, false);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return userCreated;
    }
    
    @Override
    public List<User> read(
            User userToRetrieve,
            ServiceTypeEnum retrieveBy,
            boolean isEnrichment,
            boolean isUserLogin,
            int offset,
            int numOfRows) throws Exception {
        List<User> listOfUsersRead = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            if(isUserLogin) {
                /*
                 * If isUserLogin is TRUE, this DB read is for userLogin's own data. 
                 * As such, force to read only 1 record (preventing invalid input numOfRows, if any)
                 */
                numOfRows = 1;
            }
            listOfUsersRead = UserDBQuery.read(session, null, userToRetrieve, retrieveBy, null, offset, numOfRows);
            if(listOfUsersRead == null || listOfUsersRead.size() == 0 || listOfUsersRead.get(0) == null) {
                return null;
            }

            //Enrichments (only if User is active)
            if(isEnrichment) {
                for(User userFound : listOfUsersRead) {
                    if(userFound.getActive() == YesNoEnum.YES) {
                        HibernateUtility.userEnrichment(session, userFound, isUserLogin, false);
                    }
                }
            }
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return listOfUsersRead;
    }
    
    @Override
    public User update(User userWithUpdates) throws Exception {
        User userUpdated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //If update email:
            if(userWithUpdates.getEmail() != null) {
                if(HibernateUtility.verifyUsernameOrEmailExist(session, userWithUpdates)) {
                    throw new UserDuplicateException("User Update error: New Email [" + userWithUpdates.getEmail()  + "] already exist in DB!");
                }
            }

            //Update DB:
            int numOfUpdated  = UserDBQuery.update(session, null, userWithUpdates);
            if(numOfUpdated < 1) {
                return null;
            }
                
            //Read DB after update:
            List<User> usersFound = 
                        UserDBQuery.read(session, null, userWithUpdates, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Update error: User is not found after update!");
            }
            userUpdated = usersFound.get(0);
            userUpdated = HibernateUtility.userEnrichment(session, userUpdated, true, false);
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        

        return userUpdated;
    }
    
    @Override
    public boolean deactivate(User userToDeactivate) throws Exception {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateManager.openSession();
            transaction = session.beginTransaction();
            
            User userToUpdate = new User();
            userToUpdate.setUserId(userToDeactivate.getUserId());
            userToUpdate.setActive(YesNoEnum.NO);
            int numOfUpdated  = UserDBQuery.update(session, transaction, userToUpdate);
            if(numOfUpdated < 1) {
                return false;
            }
            
            UserAuthToken userAuthTokenToDelete = new UserAuthToken();
            userAuthTokenToDelete.setUserId(userToDeactivate.getUserId());
            UserAuthTokenDBQuery.delete(session, transaction, userAuthTokenToDelete, ServiceTypeEnum.USERAUTHTOKEN_DELETE_BY_USERID);
            
            transaction.commit();
            
        } catch(Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw e;
        } finally {
            transaction = null;
            HibernateManager.shutdownSessions(session, null);
        }        
        return true;
    }
    
    @Override
    public User reactivate(User userToReactivate) throws Exception {
        User userActivated = null;
        Session session = null;
        try {
            session = HibernateManager.openSession();

            //Update:
            User userToUpdate = new User();
            userToUpdate.setUserId(userToReactivate.getUserId());
            userToUpdate.setActive(YesNoEnum.YES);
            int numOfUpdated  = UserDBQuery.update(session, null, userToUpdate);
            if(numOfUpdated <= 0) {
                return null;
            }
            
            //Find it:
            List<User> usersFound = 
                    UserDBQuery.read(session, null, userToUpdate, ServiceTypeEnum.USER_RETRIEVE_BY_USERID, null, 0, 1);
            if(usersFound == null || usersFound.size() == 0 || usersFound.get(0) == null) {
                throw new UserNotFoundException("User Reactivate error: User is not found after update!");
            }
            userActivated = usersFound.get(0);
            
            //Enrichments
            userActivated = HibernateUtility.userEnrichment(session, userActivated, true, false);        
        } finally {
            HibernateManager.shutdownSessions(session, null);
        }        
        return userActivated;
    }

    @Override
    public int delete(
            User userToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception {
        throw new OperationNotSupportedException("User Delete via UserDAOHibernateImpl is not supported!");
    }

  
    
}
