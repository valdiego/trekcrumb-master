package com.trekcrumb.business.dao;

import java.util.List;

import com.trekcrumb.common.bean.User;
import com.trekcrumb.common.bean.UserAuthToken;
import com.trekcrumb.common.enums.ServiceTypeEnum;

public interface IUserAuthTokenDAO {
    
    public UserAuthToken create(UserAuthToken userAuthTokenToCreate) throws Exception;
    
    public List<UserAuthToken> read(
            UserAuthToken userAuthTokenToRead,
            ServiceTypeEnum retrieveBy) 
            throws Exception;
    
    public int delete(
            UserAuthToken userAuthTokenToDelete,
            ServiceTypeEnum deleteBy) 
            throws Exception;
    
    public int update(UserAuthToken userAuthTokenWithUpdates) throws Exception;
    
    public User authenticate(UserAuthToken userAuthTokenInput) throws Exception;

    
}
